-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2018 at 07:35 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `grs_data`
--

CREATE TABLE `grs_data` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `no_of_application` int(11) NOT NULL,
  `no_of_online` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `total_disposal` int(11) NOT NULL,
  `total_non_disposal` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `short_desc` text NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grs_data`
--

INSERT INTO `grs_data` (`id`, `section_id`, `month`, `year`, `no_of_application`, `no_of_online`, `total`, `total_disposal`, `total_non_disposal`, `created_at`, `updated_at`, `created_by`, `short_desc`, `comments`) VALUES
(1, 2, 10, 2018, 5, 1, 6, 2, 4, '2018-10-26 03:23:49', '0000-00-00 00:00:00', 8, 'Law', 'Law'),
(2, 2, 9, 2018, 3, 4, 7, 2, 5, '2018-10-26 03:23:49', '0000-00-00 00:00:00', 8, 'Law', 'Law'),
(3, 3, 9, 2018, 3, 4, 7, 6, 1, '2018-10-26 04:40:24', '0000-00-00 00:00:00', 8, 'Law', 'Law'),
(4, 3, 10, 2018, 5, 1, 6, 2, 4, '2018-10-26 03:23:49', '0000-00-00 00:00:00', 8, 'Law', 'Law');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grs_data`
--
ALTER TABLE `grs_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grs_data`
--
ALTER TABLE `grs_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
