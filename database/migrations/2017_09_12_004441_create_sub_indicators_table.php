<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_indicators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('indicator_id');
            $table->string('sub_indicator_title_en', 256);
            $table->text('sub_indicator_title_bn');
            $table->boolean('sub_indicator_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_indicators');
    }
}
