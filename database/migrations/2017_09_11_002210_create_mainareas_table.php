<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainareasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainareas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mainarea_en', 256)->comment('Main Area name in English');
            $table->text('mainarea_bn')->comment('Main Area name in Bangla');
            $table->boolean('status')->comment('1 means active 0 means inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainareas');
    }
}
