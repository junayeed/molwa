-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 03:16 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch_trainers`
--

CREATE TABLE `batch_trainers` (
  `id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `session_topic` varchar(250) CHARACTER SET utf8 NOT NULL,
  `session_start_time` time NOT NULL,
  `session_end_time` time NOT NULL,
  `session_date` date NOT NULL,
  `trainer_type` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `trainer_name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `trainer_office` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `trainer_email` varchar(100) DEFAULT NULL,
  `trainer_cellphone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch_trainers`
--
ALTER TABLE `batch_trainers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch_trainers`
--
ALTER TABLE `batch_trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
