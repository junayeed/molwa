<?php
return  [
    'modules' => [
        'Core/AuditLog',
        'Local/Batch',
        'Local/Employee',
        'Local/Designation',
        'Local/Office',
        'Local/TrainingReport',
        'Local/TrainingCalendar',
        'Local/HatBazarReport',
        'Local/HatBazar',
        'Local/Pourosova',
        'Local/ForwardDairy2',
        'Local/ForeignTraining',
        'Local/Book',
        'Local/BookDistribution',
        'Local/BookRegister',
        'Local/AppAccess'
    ]
]
?>