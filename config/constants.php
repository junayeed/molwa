<?php

return[
    'TEMPLATE_PATH'          =>  $app->make('url')->to('/').'/public',
    'ADMIN_TEMPLATE'         => $app->make('url')->to('/').'/public/admin',
    'PAGE_JS_PATH'           => '/public/js',
    'ASSET_PATH'             => '/public/assets',
    'CAPTCHA_BG_IMAGE_PATH'  => $app->make('url')->to('/').'/public/images/captcha-backgrounds',
    'CAPTCHA_FONTS_PATH'     => $app->make('url')->to('/').'/public/captcha_fonts',
    'TRAINING_PER_DAY_HONORARIUM_1_9'     => 600,
    'TRAINING_PER_DAY_HONORARIUM_10_20'   => 500,
    'TAXABLE_PAY_GRADE'                   => 9,
    'SOURCE_TAX'                          => 0.1,
    'STAMP'                               => 10,
    'MAIN_PRESENTER_HONORARIAM'           => 3500,
    'WORKSHOP_HOST_HONORARIUM'            => 3000,
    'WORKSHOP_MODERATOR'                  => 2500,
    'RAPPORTEUR_HONORARIUM'               => 2000,
    'WORKSHOP_ASSISTANT'                  => 1500,
    'WORKSHOP_PARTICIPANT_HONORARIUM'     => 1000,
    'ALERT_LEVEL_HIGH'                    => 45,
    'ALERT_LEVEL_MEDIUM'                  => 30,
    'ALERT_LEVEL_LOW'                     => 15
    ];
