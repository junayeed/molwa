<div class="modal-header">
    <h3 class="modal-title" id="forwardModalLabel" style="font-size: 2.5rem;">
        Contributer Details
    </h3>
    <button type="button" class="close btn_participant_close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="employee_list">
            <div class="mt-element-list">
                @foreach($contributor_details as $key => $cd)
                    <div class="mt-list-head list-default font-white bg-green-seagreen">
                        <div class="list-head-title-container"><h3 class="list-title">{{$key}}</h3></div>
                    </div>
                    <div class="mt-list-container list-default ext-1 group">
                        @foreach($cd as $ko => $co)
                            @php $tt = array_sum($co);
                                 $curr_month = Date('F, Y');
                                 $target_name = str_replace(", ", "_", $ko);
                            @endphp
                            <a class="list-toggle-container" data-toggle="collapse" href="#{{$target_name}}" aria-expanded="true">
                                <div class="list-toggle @if ( $curr_month == $ko) done @endif uppercase"> {{$ko}}:

                                    {{$tt}}
                                </div>
                            </a>
                            <div class="panel-collapse collapse @if ( $curr_month == $ko) in @endif" id="{{$target_name}}">
                                <ul>
                                    @foreach($co as $k => $c)
                                        <li class="mt-list-item done" style="padding-bottom: 2px !important; padding-top: 2px !important; min-height: 0px !important;">
                                            <div class="list-icon-container" style="padding: 0px !important; height: 0px !important; width: 0px !important; "><i class="icon-check"></i></div>
                                            <div class="list-datetime"> {{$c}}</div>
                                            <div class="list-item-content"><h3 class="uppercase">{{$k}}</h3>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-default btn_participant_close" id="" data-dismiss="modal">
        <i class="fa fa-window-close"> বন্ধ করুন</i>
    </button>
</div>