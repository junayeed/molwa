@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/home">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                  
                    <li>
                        <span>Notification</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Notification
                </h1>
            </div>
            @include('layouts.messages')
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box light">
                        <div class="portlet-title">
                        </div>
                        <div class="portlet-body">
                            <h3>Upazila: {{$notification->getApplicationBasicInfoRow->getUpazilaInfoRow->name}}</h3>
                            <h3>District: {{$notification->getApplicationBasicInfoRow->getDistrictInfoRow->name}}</h3>
                            <h4>Date: {{$notification->date}}</h4>
                            <h4>Application: {{$notification->getApplicationBasicInfoRow->name}}</h4>
                            <h4>Remarks: {{$notification->remarks}}</h4>
                            <a class="btn btn-sm btn-circle btn-view" href="/ApplicationData/{{$notification->application_id}}"><i class="fa fa-eye"></i> Go To Application </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body text-center">
                            <a href="/home" class="btn btn-circle btn-sm btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection