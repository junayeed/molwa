@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="row">
        <div class="col-md-2">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$data['total_applicant']}}">0</span>
                    </div>
                    <div class="desc"> Total Applicants </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat red-pink">
                <div class="visual">
                    <i class="icon-user-female "></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$data['total_gender'][0]->total_female}}">0</span>
                    </div>
                    <div class="desc"> Total Female </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat blue">

                <div class="visual">
                    <i class="icon-user "></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$data['total_gender'][0]->total_male}}">0</span>
                    </div>
                    <div class="desc"> Total Male </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat yellow-soft">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{--<span data-counter="counterup" data-value="{{$data['total_sf_received']}}"></span>--}}
                    </div>
                    <div class="desc"> SF Received </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat green-jungle">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{--<span data-counter="counterup" data-value="{{$data['total_sf_sent']}}"></span>--}}
                    </div>
                    <div class="desc"> SF Sent </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat green-meadow">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{--<span data-counter="counterup" data-value="{{$data['total_closed']}}">0</span>--}}
                    </div>
                    <div class="desc"> Case Closed </div>
                </div>
            </div>
        </div>
    </div>
    <!-- DATA STATS STARTS HERE  -->

    <!-- PERCENTAGE STATS STARTS HERE  -->
    {{--<div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cursor font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">General Stats</span>
                    </div>
                    --}}{{--<div class="actions">
                        <a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                            <i class="fa fa-repeat"></i> Reload </a>
                    </div>--}}{{--
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sf_pending" data-percent="{{ number_format($data['total_sf_pending']/$data['total_applicant']*100, 1) }}">
                                    <span class="bold">{{ number_format($data['total_sf_pending']/$data['total_applicant']*100, 1) }}</span>%
                                </div>SF Pending
                            </div>
                        </div>
                        --}}{{--<div class="margin-bottom-10"> </div>--}}{{--
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sf_received" data-percent="{{ number_format($data['total_sf_received']/$data['total_applicant']*100, 1) }}">
                                    <span class="bold">{{ number_format($data['total_sf_received']/$data['total_applicant']*100, 1) }}</span>%
                                </div>SF Received
                            </div>
                        </div>
                        --}}{{--<div class="margin-bottom-10"> </div>--}}{{--
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sf_sent" data-percent="{{ number_format($data['total_sf_sent']/$data['total_applicant']*100, 1) }}">
                                    <span class="bold">{{ number_format($data['total_sf_sent']/$data['total_applicant']*100, 1) }}</span>%
                                </div>SF Sent
                            </div>
                        </div>
                        --}}{{--<div class="margin-bottom-10"> </div>--}}{{--
                        <div class="col-md-2 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ number_format($data['total_int_order']/$data['total_applicant']*100, 1)}}">
                                    <span class="bold">{{ number_format($data['total_int_order']/$data['total_applicant']*100, 1)}}</span>%
                                </div>Interim Orders
                            </div>
                        </div>
                        <div class="col-md-2 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number case_closed" data-percent="{{number_format($data['total_closed']/$data['total_applicant']*100, 1)}}">
                                    <span class="bold">{{number_format($data['total_closed']/$data['total_applicant']*100, 1)}}</span>%
                                </div>Case Closed
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!-- PERCENTAGE STATS ENDS HERE  -->


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <a id="sample_editable_1_new" class="btn green btn-circle btn-add-new" href="/forwardDairy/create"><i class="fa fa-plus"></i> Add New Case </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th class="table_heading">Serial</th>
                            <th class="table_heading">File No</th>
                            <th class="table_heading">Case No</th>
                            <th class="table_heading">Receive Date</th>
                            <th class="table_heading">Subject</th>
                            <th class="table_heading">Status</th>
                            <th class="table_heading text-center no-print">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($applicant_list as $applicant)
                            {{ $class = '' }}
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td>{{$applicant->applicant_name_en}}</td>
                                <td>{{$applicant->applicant_dob}}</td>
                                <td>{{\Carbon\Carbon::parse($applicant->applicant_dob)->format('d M, Y')}}</td>
                                <td>{{--{{$applicant->case_subject}}--}}</td>
                               {{-- <td>
                                    @if ($applicant->case_status_id == 1)
                                        @php ($class = 'bg-red-flamingo')
                                    @elseif ($applicant->case_status_id == 2)
                                        @php ($class = 'bg-yellow-gold')
                                    @elseif ($applicant->case_status_id == 3)
                                        @php ($class = 'bg-yellow-soft')
                                    @elseif ($applicant->case_status_id == 4)
                                        @php ($class = 'bg-green-jungle')
                                    @elseif ($applicant->case_status_id == 5)
                                        @php ($class = 'bg-green-meadow')
                                    @endif
                                    <label class="label {{$class}}">{{$applicant->case_status}}</label>
                                </td>--}}
                                <td class="text-center no-print">
                                    <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$applicant->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$applicant->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END CONTENT -->

     <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#table_list_view').DataTable({});

            $('.delete-subindicator').on('click', function(){
                var subIndicatorID = $(this).data('id');
                var that        = this;

                bootbox.confirm({
                    message: "Are you sure you want to delete this sub indicator? ",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn btn-danger'
                        }
                    },
                    callback: function (result) {
                        if ( result ){
                            $.ajax({
                                type     : "GET",
                                url      : "{{URL::to('/')}}/deleteSubIndicator",
                                data     : {id: subIndicatorID},
                                dataType : 'JSON',
                                success  : function (res) {
                                    $(that).closest('tr').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                                .search( this.value )
                                .draw();
                    }
                } );
            } );
        } );
    </script>

@endsection