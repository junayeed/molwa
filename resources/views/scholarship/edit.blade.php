@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <h1 class="page-title bold">Edit Case Details</h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/forwardDairy/{{$case_data->id}}">
                {{csrf_field()}}
                {{method_field('PUT')}}

                <div class="row">
                    <!-- CASE DETAILS (LFET COL) START -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet box purple">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cog"></i>
                                            Case Details
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label>File No</label>
                                                <input name="file_no" id="file_no" type="text" class="form-control input-sm " value="{{$case_data->file_no}}">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Case No</label>
                                                <input name="case_no" id="case_no" type="text" class="form-control input-sm" value="{{$case_data->case_no}}">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Case Receive Date</label>
                                                <input id="case_receive_date" name="case_receive_date" class="form-control date-picker input-sm" size="16" placeholder="yyyy/mm/dd" type="text" value="{{\Carbon\Carbon::parse($case_data->case_receive_date)->format('Y/m/d')}}"  />
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Parties Name</label>
                                                <input name="parties_name" id="parties_name" type="text" class="form-control input-sm"  value="{{$case_data->parties_name}}" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Subject</label>
                                                <textarea name="case_subject" id="case_subject" class="form-control input-sm" >{{$case_data->case_subject}}</textarea>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Case Status</label>
                                                <select name="case_status" id="case_status" class="form-control input-sm" >
                                                    <option value="">Select Status</option>
                                                    @foreach($status_list as $id => $status)
                                                        <option value="{{$id}}" {{ $case_data['case_status'] == $id ? 'selected="selected"' : '' }}>{{$status}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Has Ruli Nisi/Ad Interim Order?</label><br>
                                                <input type="checkbox" name="has_interim_order" id="has_interim_order" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CASE DETAILS (LFET COL) END -->

                    <!-- Interim Order (RIGHT COL) START -->
                    <div class="col-md-6">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Rule Nisi / Ad Interim Order
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-7">
                                        <label>Rule Nisi/Ad Interim Order Date</label>
                                        <input id="interim_order_date" name="interim_order_date" class="form-control date-picker input-sm" size="16" placeholder="yyyy/mm/dd" readonly type="text" value="{{\Carbon\Carbon::parse($interim_order_data[0]->interim_order_date)->format('Y/m/d')}}"  />
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label>Deadline</label>
                                        <input id="deadline" name="deadline" class="form-control input-sm" type="text" value="{{$interim_order_data[0]->deadline}}"  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Order Details</label>
                                        <textarea name="order_details" id="order_details" class="form-control input-sm" >{{$interim_order_data[0]->order_details}}</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mt-repeater">
                                            <div data-repeater-list="interim_order_data">
                                                @foreach($interim_order_details as $iod)
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <div class="row mt-repeater-row">
                                                        <div class="col-md-4">
                                                            <label class="small ">Implementing Authority</label>
                                                            <select name="implementing_authority" id="implementing_authority" class="form-control input-sm" >
                                                                <option value="">Select an Authority</option>
                                                                @foreach($imp_authority_list as $id => $imp_authority)
                                                                    <option value="{{$id}}" {{ $iod->implementing_authority == $id ? 'selected="selected"' : '' }}>{{$imp_authority}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <label class="control-label small">Responsibility</label>
                                                            <textarea name="responsibility" id="responsibility" class="form-control input-sm" >{{$iod->responsibility}}</textarea>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add Authority</a>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="form-group col-md-12">
                                    <label>Status*</label><br>
                                    <input type="checkbox" name="sub_indicator_status" id="sub_indicator_status" data-on-text="Active" data-off-text="Inactive" style="height: 30px;" checked>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <!-- Interim Order (RIGHT COL) END -->
                </div>

                <!-- SF DETAILS START -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Statement of Facts (SF)
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mt-repeater">
                                            <div data-repeater-list="sf_data">
                                                @foreach($sf_data as $sf)
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <div class="row mt-repeater-row">
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>Recipient</label>
                                                                <select name="sf_recipient" id="sf_recipient" class="form-control input-sm" >
                                                                    <option value="">Select a Recipient</option>
                                                                    @foreach($recipient_list as $id => $recipient)
                                                                        <option value="{{$id}}" {{ $sf->sf_recipient == $id ? 'selected="selected"' : '' }}>{{$recipient}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>Memo No.</label>
                                                                <input id="sf_issue_memo" name="sf_issue_memo" class="form-control input-sm " size="16" type="text" value="{{$sf->sf_issue_memo}}"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>Date of Issue</label>
                                                                <input id="sf_issue_date" name="sf_issue_date" class="form-control date-picker input-sm" readonly placeholder="yyyy/mm/dd" type="text" value="{{\Carbon\Carbon::parse($sf->sf_issue_date)->format('Y/m/d')}}" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>SF Receiving Date</label>
                                                                <input id="sf_receive_date" name="sf_receive_date" class="form-control date-picker input-sm" readonly placeholder="yyyy/mm/dd" type="text" value="{{\Carbon\Carbon::parse($sf->sf_receive_date)->format('Y/m/d')}}" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>Memo No.</label>
                                                                <input id="sf_sending_memo" name="sf_sending_memo" class="form-control input-sm" size="16" type="text" value="{{$sf->sf_sending_memo}}"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2_15">
                                                            <div class="form-group">
                                                                <label>SF Sending Date</label>
                                                                <input id="sf_sending_date" name="sf_sending_date" class="form-control date-picker input-sm" placeholder="yyyy/mm/dd" type="text" value="{{\Carbon\Carbon::parse($sf->sf_sending_date)->format('Y/m/d')}}" readonly />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs btn-circle green-meadow mt-repeater-add"><i class="fa fa-plus"></i> Add New Recipient</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SF DETAILS END -->

                <!-- JUDGEMENT DETAILS START -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>Judgement Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group col-md-12">
                                            <label>Date of Hearing</label>
                                            <input id="hearing_date" name="hearing_date" class="form-control date-picker input-sm" size="16" placeholder="yyyy/mm/dd" type="text" value="{{(isset($judgement_data->hearing_date)) ? \Carbon\Carbon::parse($judgement_data->hearing_date)->format('Y/m/d') : ''}}"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group col-md-12">
                                            <label>Order/ Judgement</label>
                                            <textarea name="judgement" id="judgement" class="form-control input-sm" >{{(isset($judgement_data->judgement) ? $judgement_data->judgement : "")}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group col-md-12">
                                            <label>C.M.P. No.</label>
                                            <input id="cmp_no" name="cmp_no" class="form-control input-sm" type="text" value="{{(isset($judgement_data->cmp_no) ? $judgement_data->cmp_no : "")}}"  />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group col-md-12">
                                            <label>C.M.P. Order / Judgement</label>
                                            <textarea name="cmp_judgement" id="cmp_judgement" class="form-control input-sm" >{{(isset($judgement_data->cmp_judgement) ? $judgement_data->cmp_judgement : "")}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group col-md-12">
                                            <label>C.P. Order / Judgement</label>
                                            <input id="cp_no" name="cp_no" class="form-control input-sm" type="text" value="{{(isset($judgement_data->cp_no) ? $judgement_data->cp_no : "")}}"  />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group col-md-12">
                                            <label>C.P. Order / Judgement</label>
                                            <textarea name="cp_judgement" id="cp_judgement" class="form-control input-sm" >{{(isset($judgement_data->cp_judgement) ? $judgement_data->cp_judgement : "")}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group col-md-12">
                                            <label>Review Petition No</label>
                                            <input id="review_petition_no" name="review_petition_no" class="form-control input-sm" type="text" value="{{(isset($judgement_data->review_petition_no) ? $judgement_data->review_petition_no : "")}}"  />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group col-md-12">
                                            <label>Order</label>
                                            <textarea id="review_order" name="review_order" class="form-control input-sm">{{(isset($judgement_data->review_order) ? $judgement_data->review_order : "")}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group col-md-12">
                                            <label>Remarks</label>
                                            <textarea id="remarks" name="remarks" class="form-control input-sm">{{(isset($judgement_data->remarks) ? $judgement_data->remarks : "")}}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- JUDGEMENT DETAILS START -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>Case History
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12">
                                            {{--<label>Date of Hearing</label>
                                            <input id="hearing_date" name="hearing_date" class="form-control date-picker input-sm" size="16" placeholder="yyyy/mm/dd" type="text" value=""  />--}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>Case Facts
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-5">
                                            <!-- BEGIN WIDGET THUMB -->
                                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                                <h5 class="widget-thumb-heading">Case is pending for</h5>
                                                <div class="widget-thumb-wrap">
                                                    <i class="widget-thumb-icon bg-yellow-casablanca icon-bulb"></i>
                                                    <div class="widget-thumb-body">
                                                        <span class="widget-thumb-subtitle">DAYS</span>
                                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END WIDGET THUMB -->
                                    </div>
                                </div>
                                <div class="row">
                                    @foreach($sf_data as $sf)
                                    <div class="col-md-5">
                                        <!-- BEGIN WIDGET THUMB -->
                                        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                            <h5 class="widget-thumb-heading">SF is pending at {{$recipient_list[$sf->sf_recipient]}}</h5>
                                            <div class="widget-thumb-wrap">
                                                <i class="widget-thumb-icon bg-red icon-layers"></i>
                                                <div class="widget-thumb-body">
                                                    <span class="widget-thumb-subtitle">DAYS</span>
                                                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($sf->sf_issue_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($sf->sf_issue_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END WIDGET THUMB -->
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> Save &amp; Update</button>
                                <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                                <!-- </div> -->
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
                </div>
            </form>
            <!-- Cancel Modal -->
            <div id="cancel_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="/users" class="btn btn-modal-cancel">Cancel Anyway</a>
                            <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        $(function() {
            $('.date-picker').datepicker({
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "mm/dd/yyyy"
            });

            $("#has_interim_order").bootstrapSwitch();

            var has_interim_order = {{$case_data->has_interim_order}};

            if ( has_interim_order ){
                $("#has_interim_order").bootstrapSwitch('state', true);
            }
            else {
                $("#has_interim_order").bootstrapSwitch('state', false);
            }

            $("#case_details_form").validate(
                {
                    rules:
                        {
                            'case_no'                 : "required",
                            'file_no'                 : "required",
                            'case_receive_date'       : "required",
                            'parties_name'            : "required",
                            'case_subject'            : "required",
                            'case_status'             : "required",
                            // if the has_interim_order is checked
                            'interim_order_date'      : { required: '#has_interim_order:checked' },
                            'deadline'                : { required: '#has_interim_order:checked' },
                            'order_details'           : { required: '#has_interim_order:checked' },
                            //'implementing_authority'  : { required: '#has_interim_order:checked' },
                            //'responsibility'          : { required: '#has_interim_order:checked' },
                            'sf_issue_memo'           : {
                                required:
                                    {
                                        depends: function()
                                        {
                                            return $('#case_status').val() == "2";
                                        }
                                    }
                            }

                        },
                    messages: { },
                    highlight: function (element)
                    { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element)
                    { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label)
                    {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler : function()
                    {
                        return false;
                    }
                });
        });
        /*$('#mainarea').on('change', function(){
            var mainAreaID = $(this).val();

            if ( mainAreaID )
            {
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/')}}/getMainAreaDetails",
                    data: {id: mainAreaID},
                    dataType: 'JSON',
                    success: function (res) {
                        //alert(res[1].length);
                        $('#mainarea_serial').html(res[0][0].id);
                        $('#mainarea_en').html(res[0][0].mainarea_en);
                        $('#mainarea_bn').html(res[0][0].mainarea_bn);
                        $('#created_at').html(res[0][0].created_at);

                        // empty the indicator drodown
                        $("#indicator_id").empty();
                        $("#indicator_id").append("<option value='0'>Select an Indicator</option>");
                        // load the new values for Indicator
                        $.each(res[1], function (index, value) {
                            //console.log(value.id + ' - - ' + value.indicator_title);
                            $("#indicator_id").append("<option value='"+value.id+"'>"+value.indicator_title+"</option>");
                        });
                        $('#serial').html('');
                        $('#indicator_en').html('');
                        $('#indicator_bn').html('');
                        $('#created_at').html('');
                    }
                });
            }
        });

        $('#indicator_id').on('change', function(){
            var indicatorID = $(this).val();

            if ( indicatorID )
            {
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/')}}/getIndicatorDetails",
                    data: {id: indicatorID},
                    dataType: 'JSON',
                    success: function (res) {
                        $('#serial').html(res[0].id);
                        $('#indicator_en').html(res[0].indicator_title);
                        $('#indicator_bn').html(res[0].indicator_title_bn);
                        $('#created_at').html(res[0].created_at);
                    }
                });
            }
        });

        $(function(){
            $("#sub_indicator_status").bootstrapSwitch();

            //var status = {{--{{$sub_indicator->sub_indicator_status}};--}}

            if ( status ){
                $("#sub_indicator_status").bootstrapSwitch('state', true);
            }
            else {
                $("#sub_indicator_status").bootstrapSwitch('state', false);
            }

            //var mainAreaID = {{--{{$sub_indicator->mainarea}}--}};

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getMainAreaDetails",
                data: {id: mainAreaID},
                dataType: 'JSON',
                success: function (res) {
                    //alert(res[1].length);
                    $('#mainarea_serial').html(res[0][0].id);
                    $('#mainarea_en').html(res[0][0].mainarea_en);
                    $('#mainarea_bn').html(res[0][0].mainarea_bn);
                    $('#created_at').html(res[0][0].created_at);
                }
            });

            //var indicatorID = {{--{{$sub_indicator->indicator_id}}--}};

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getIndicatorDetails",
                data: {id: indicatorID},
                dataType: 'JSON',
                success: function (res) {
                    $('#serial').html(res[0].id);
                    $('#indicator_en').html(res[0].indicator_title);
                    $('#indicator_bn').html(res[0].indicator_title_bn);
                    $('#created_at').html(res[0].created_at);
                }
            });
        });*/
    </script>
@endsection