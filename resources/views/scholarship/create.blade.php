@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="application_form" method="POST" action="/Scholarship">
        {{csrf_field()}}
        <div class="row">
            <!-- CASE DETAILS (LFET COL) START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>আবেদনকারীর তথ্য
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="control-label small">বিভাগ</label>
                                        <select name="division" id="division" class="form-control input-sm" >
                                            <option value="">- বিভাগ নির্বাচন করুন -</option>
                                            @foreach($division_list as $id => $division)
                                                <option value="{{$id}}">{{$division}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label small">জেলা</label>
                                        <select name="district" id="district" class="form-control input-sm" >
                                            <option value="">- জেলা  নির্বাচন করুন -</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label small">উপজেলা</label>
                                        <select name="upazilla" id="upazilla" class="form-control input-sm" >
                                            <option value="">- উপজেলা  নির্বাচন করুন -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">জরুরি যোগাযোগের জন্য ফোন</label>
                                        <input id="emergency_phone" name="emergency_phone" class="form-control input-sm" size="16" type="text" value=""  />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">জন্ম তারিখ</label>
                                        <input id="applicant_dob" name="applicant_dob" class="form-control date-picker input-sm" size="16" placeholder="yyyy/mm/dd" type="text" value=""  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">আবেদনকারীর নাম (বাংলা)</label>
                                        <input name="applicant_name_bn" id="applicant_name_bn" type="text" class="form-control input-sm" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">আবেদনকারীর নাম (ইংরেজী ক্যাপিটাল লেটারে)</label>
                                        <input name="applicant_name_en" id="applicant_name_en" type="text" class="form-control input-sm" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">পিতার নাম</label>
                                        <input name="applicant_father_name" id="applicant_father_name" type="text" class="form-control input-sm" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label small ">মাতার নাম</label>
                                        <input name="applicant_mother_name" id="applicant_mother_name" type="text" class="form-control input-sm" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label small">লিঙ্গ</label>
                                        <select name="applicant_gender" id="applicant_gender" class="form-control input-sm">
                                            <option value="">- নির্বাচন করুন -</option>
                                            <option value="1">পুরুষ</option>
                                            <option value="2">মহিলা</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CASE DETAILS (LFET COL) END -->

            <!-- Interim Order (RIGHT COL) START -->
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>আবেদনকারীর সাথে সম্পর্কিত মুক্তিযোদ্ধার তথ্য
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-7">
                                <label class="control-label small">মুক্তিযোদ্ধার সাথে আবেদনকারীর সম্পর্ক</label>
                                <select name="applicant_ff_relation" id="applicant_ff_relation" class="form-control input-sm">
                                    <option value="">- নির্বাচন করুন -</option>
                                    <option value="1">পিতা</option>
                                    <option value="2">মাতা</option>
                                    <option value="3">দাদা</option>
                                    <option value="4">দাদী</option>
                                    <option value="5">নানা</option>
                                    <option value="6">নানী</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-repeater">
                                    <div data-repeater-list="interim_order_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-3">
                                                    <label class="small ">তালিকার ধরন</label>
                                                    <select name="category_type" id="category_type" class="form-control input-sm" >
                                                        <option value="">- নির্বাচন করুন -</option>
                                                        @foreach($ff_type_list as $id => $type)
                                                            <option value="{{$id}}">{{$type}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label small">গেজেট/ক্রমিক/সনদ নং</label>
                                                    <input name="ff_serial_no" id="ff_serial_no" type="text" class="form-control input-sm" >
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add Authority</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Interim Order (RIGHT COL) END -->
        </div>

        <!-- ADDRESS START -->
        <div class="row">
            <!-- PRESENT ADDRESS START -->
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>বর্তমান ঠিকানা
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label small">ঠিকানা</label>
                                <input name="present_address" id="present_address" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">গ্রাম / মহল্লা / ওয়ার্ড</label>
                                <input name="present_village" id="present_village" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">ডাকঘর</label>
                                <input name="present_postoffice" id="present_postoffice" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">জেলা</label>
                                <label class="control-label small">জেলা</label>
                                <select name="present_district" id="present_district" class="form-control input-sm" >
                                    <option value="">- জেলা  নির্বাচন করুন -</option>
                                    @foreach($district_list as $id => $district)
                                        <option value="{{$id}}">{{$district}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">উপজেলা</label>
                                <select name="present_upazilla" id="present_upazilla" class="form-control input-sm" >
                                    <option value="">- উপজেলা  নির্বাচন করুন -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PRESENT ADDRESS END -->

            <!-- PERMANENT ADDRESS START -->
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>স্থায়ী ঠিকানা
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label small">ঠিকানা</label>
                                <input name="permanent_address" id="permanent_address" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">গ্রাম / মহল্লা / ওয়ার্ড</label>
                                <input name="permanent_village" id="permanent_village" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">ডাকঘর</label>
                                <input name="permanent_postoffice" id="permanent_postoffice" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">জেলা</label>
                                <select name="permanent_district" id="permanent_district" class="form-control input-sm" >
                                    <option value="">- জেলা  নির্বাচন করুন -</option>
                                    @foreach($district_list as $id => $district)
                                        <option value="{{$id}}">{{$district}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">উপজেলা</label>
                                <select name="permanent_upazilla" id="permanent_upazilla" class="form-control input-sm" >
                                    <option value="">- উপজেলা  নির্বাচন করুন -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PERMANENT ADDRESS END -->
        </div>
        <!-- ADDRESS END -->

        <!-- ACADEMIC QUALIFICATION START -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>পরীক্ষা সমূহের বিবরণ
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="form-group mt-repeater">
                                    <div data-repeater-list="academic_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-2">
                                                    <label class="small ">পরীক্ষার নাম</label>
                                                    <select name="exam_name" id="exam_name" class="form-control input-sm" >
                                                        <option value="">- নির্বাচন করুন -</option>
                                                        <option value="1">এসএসসি/সমমান</option>
                                                        <option value="2">এইচএসসি/সমমান</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <label class="control-label small text- ">পাশের সন</label>
                                                    <select name="exam_year" id="exam_year" class="form-control input-sm input-xsmall" style="padding: 0px !important;">
                                                        <option value="">- নির্বাচন করুন -</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label small text- ">শিক্ষা প্রতিষ্ঠানের নাম</label>
                                                    <input name="edu_inst" id="edu_inst" type="text" class="form-control input-sm" >
                                                </div>
                                                <div class="col-md-1">
                                                    <label class="control-label small text- ">বোর্ড</label>
                                                    <select name="edu_board" id="edu_board" class="form-control input-sm input-xsmall" style="padding: 0px !important;">
                                                        <option value="">- নির্বাচন করুন -</option>
                                                        @foreach($board_list as $id => $board)
                                                            <option value="{{$id}}">{{$board}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="col-md-2">
                                                    <label class="control-label small ">বিভাগ/গ্রপ</label>
                                                    <select name="edu_group" id="edu_group" class="form-control input-sm"  style="padding: 0px !important;">
                                                        <option value="">- নির্বাচন করুন -</option>
                                                        <option value="1">Science</option>
                                                        <option value="2">Arts</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <label class="control-label small">মোট জিপিএ</label>
                                                    <input name="cgpa" id="cgpa" type="text" class="form-control input-sm" >
                                                </div>
                                                <div class="col-md-1">
                                                    <label class="control-label small" style="font-size: 75%;">৪র্থ বিষয় ব্যতিত</label>
                                                    <input name="cgpa_wo_4th_subject" id="cgpa_wo_4th_subject" type="text" class="form-control input-sm" >
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" id="add_academic_row" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add">
                                        <i class="fa fa-plus"></i>নতুন পরীক্ষা যোগ করুন
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ACADEMIC QUALIFICATION END -->

        <div class="row">
            <!-- CURRENT EDUCATION STATUS START -->
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>আবেদনকারীর বর্তমান শিক্ষাস্তরের বিবরণ  (কেবলমাত্র under graduate ও উচ্চ মাধ্যমিক পর্যায়ের ছাত্র/ছাত্রীদের জন্য প্রযোজ্য
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label small">শিক্ষা প্রতিষ্ঠানের নাম</label>
                                <input name="current_edu_inst" id="current_edu_inst" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">সেশন</label>
                                <input name="current_session" id="current_session" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">বিভাগ/বিষয়</label>
                                <input name="current_subject" id="current_subject" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label small">বর্ষ/শ্রেণী/সেমিস্টার</label>
                                <input name="current_semester" id="current_semester" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">ক্রমিক/আইডি নং</label>
                                <input name="current_id" id="current_id" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CURRENT EDUCATION STATUS END -->

            <!-- CURRENT EDUCATION STATUS START -->
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>অন্য কোন সংস্থা হতে বৃত্তিপ্রাপ্ত হলে
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>অন্য কোন সংস্থা হতে বৃত্তিপ্রাপ্ত?</label><br>
                                <div class="input-group">
                                    <div class="icheck-inline">
                                        <label>
                                            <input type="radio" name="received_scholarship" id="received_scholarship_yes" value="1" class="">হ্যাঁ
                                        </label>
                                        <label>
                                            <input type="radio" name="received_scholarship" id="received_scholarship_no" value="0" checked class="">না
                                        </label>
                                    </div>
                                </div>
                                {{--<input type="checkbox" name="received_scholarship" id="received_scholarship" data-on-text="হ্যাঁ" data-off-text="না" style="height: 30px;">--}}
                            </div>
                        </div>
                        <div class="row other_scholarship">
                            <div class="form-group col-md-12">
                                <label class="control-label small">বৃত্তির ধরন</label>
                                <input name="scholarship_type" id="scholarship_type" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row  other_scholarship">
                            <div class="form-group col-md-6">
                                <label class="control-label small">বৃত্তির সন</label>
                                <input name="scholarship_year" id="scholarship_year" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">মেয়াদ</label>
                                <input name="scholarship_period" id="scholarship_period" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                        <div class="row other_scholarship">
                            <div class="form-group col-md-6">
                                <label class="control-label small">শিক্ষাস্তর</label>
                                <input name="scholarship_edu_level" id="scholarship_edu_level" type="text" class="form-control input-sm" >
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label small">প্রাপ্ত বৃত্তির টাকার পরিমান</label>
                                <input name="scholarship_amount" id="scholarship_amount" type="text" class="form-control input-sm" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CURRENT EDUCATION STATUS END -->
        </div>
        <!-- ADDRESS END -->

        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                        <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/Scholarship" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        var FormValidation = function () {
            var addApplicantForm = function() {
                var form = $('#application_form');
                var error = $('.alert-danger', form);

                $("#application_form").validate(
                {
                    errorElement: 'span', //---- default input error message container
                    errorClass: 'help-block help-block-error', //---- default input error message class
                    focusInvalid: false, //---- do not focus the last invalid input
                    ignore: "",  //---- validate all fields including form hidden input
                    rules:
                        {
                            /** Validate Applicant Data**/
                            'division'                    : "required",
                            'district'                    : "required",
                            'upazilla'                    : "required",
                            'emergency_phone'             : "required",
                            'applicant_dob'               : "required",
                            'applicant_name_bn'           : "required",
                            'applicant_name_en'           : "required",
                            'applicant_name_en'           : "required",
                            'applicant_father_name'       : "required",
                            'applicant_mother_name'       : "required",
                            'applicant_gender'            : "required",
                            'current_edu_inst'            : "required",
                            'current_session'             : "required",
                            'current_subject'             : "required",
                            'current_semester'            : "required",
                            'current_id'                  : "required",
                            'applicant_ff_relation'       : "required",
                            /** Validate Present Address **/
                            'present_address'             : "required",
                            'present_village'             : "required",
                            'present_postoffice'          : "required",
                            'present_district'            : "required",
                            'present_upazilla'            : "required",
                            /** Validate Permanent Address**/
                            'permanent_address'           : "required",
                            'permanent_village'           : "required",
                            'permanent_postoffice'        : "required",
                            'permanent_district'          : "required",
                            'permanent_upazilla'          : "required",
                            'received_scholarship'        : "required",
                            // if the applicant_gender is checked
                            'scholarship_type'            : { required: '#received_scholarship_yes:checked' },
                            'scholarship_year'            : { required: '#received_scholarship_yes:checked' },
                            'scholarship_period'          : { required: '#received_scholarship_yes:checked' },
                            'scholarship_edu_level'       : { required: '#received_scholarship_yes:checked' },
                            'scholarship_amount'          : { required: '#received_scholarship_yes:checked' },
                            /*'interim_order_date'      : { required: '#applicant_gender:checked' },
                            'deadline'                : { required: '#applicant_gender:checked' },
                            'order_details'           : { required: '#applicant_gender:checked' }*/
                            //'implementing_authority'  : { required: '#applicant_gender:checked' },
                            //'responsibility'          : { required: '#applicant_gender:checked' },
                            /*'sf_issue_memo'           : {
                                                            required:
                                                                {
                                                                    depends: function()
                                                                    {
                                                                        return $('#case_status').val() == "2";
                                                                    }
                                                                }
                                                        }*/

                        },
                    messages: { },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        error.show();
                        Metronic.scrollTo(error, -200);
                        error.delay(6000).fadeOut(2000);
                    },
                    highlight: function (element)
                    { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element)
                    { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label)
                    {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    /*submitHandler : function()
                    {
                        return false;
                    }*/
                });
            }
            return {
                //---- main function to initiate the module
                init: function () {
                    addApplicantForm();
                }
            };
        }();

        /** Load District according to Division**/
        $('#division').on('change', function(){
            var division_id = $('#division').val();

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getDistrictList",
                dataType: 'json',
                data: {division_id: division_id},
                success: function (res) {
                    $('#district').empty();
                    $('#district').append('<option value="">- জেলা  নির্বাচন করুন -</option>');
                    $('#upazilla').empty();
                    $('#upazilla').append('<option value="">- উপজেলা নির্বাচন করুন -</option>');
                    $.each(res, function(key, value) {

                        $('#district').append('<option value="'+ key +'">'+ value +'</option>');

                    });
                }
            });
        });

        function getUpazillaList(district_id, destination)
        {
            $.ajax({
                type      : "GET",
                url       : "{{URL::to('/')}}/getUpazillaList",
                dataType  : 'json',
                data      : {district_id: district_id},
                success   : function (res) {
                    $('#'+destination).empty();
                    $('#'+destination).append('<option value="">- উপজেলা নির্বাচন করুন -</option>');
                    $.each(res, function(key, value) {
                        $('#'+destination).append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        }

        /** Load Upazilla according to District**/
        $('#district').on('change', function(){
            var district_id = $('#district').val();

            getUpazillaList(district_id, 'upazilla');
        });

        /** Load Present Upazilla according to Present District**/
        $('#present_district').on('change', function(){
            var district_id = $('#present_district').val();

            getUpazillaList(district_id, 'present_upazilla');
        });

        /** Load Present Upazilla according to Present District**/
        $('#permanent_district').on('change', function(){
            var district_id = $('#permanent_district').val();

            getUpazillaList(district_id, 'permanent_upazilla');
        });


        $(function() {
            FormValidation.init();

            $('.date-picker').datepicker({
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "mm/dd/yyyy"
            });

            //alert($("#received_scholarship").is(":checked"))

            ($("#received_scholarship_yes").is(":checked")) ? $('.other_scholarship').show() : $('.other_scholarship').hide();

            //$("#received_scholarship_yes").on('ifChanged', function(){
            $("#received_scholarship_yes, #received_scholarship_no").on('change', function(){

                if ($("#received_scholarship_yes").is(":checked")) {
                    $('.other_scholarship').show();
                }
                else if ($("#received_scholarship_no").is(":checked")) {
                    $('.other_scholarship').hide();
                }
            });

            /*$("#received_scholarship").bootstrapSwitch();
            //var state = $("#received_scholarship").bootstrapSwitch('state');

            ($("#received_scholarship").bootstrapSwitch('state')) ? $('.other_scholarship').show() : $('.other_scholarship').hide();

            $('#received_scholarship').on('switchChange.bootstrapSwitch', function (e, data) {

                (data) ? $('.other_scholarship').show() : $('.other_scholarship').hide();
            });*/

            $.ajax({
                type      : "GET",
                url       : "{{URL::to('/')}}/getFFDetails",
                dataType  : 'json',
                data      : {},
                success   : function (res) {

                }
            });
        });
    </script>
@endsection