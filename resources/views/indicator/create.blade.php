@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/Indicator">Indicator</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Add New Indicator</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Add New Indicator
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/Indicator">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Choose Main Area
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Choose Main Area</label>
                                        <select name="main_area" id="main_area" class="form-control" required>
                                            <option value="">Select a Main Area</option>
                                            @foreach($main_area_list as $main_area)
                                                <option value="{{$main_area->id}}">{{$main_area->mainarea_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="item-name font-blue-madison">Serial: </label>
                                        <label class="font-blue-madison bold" id="serial"></label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="item-name font-blue-madison">Main Area (EN): </label>
                                        <label class="font-blue-madison bold" id="mainarea_en"></label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="item-name font-blue-madison">Main Area (BN): </label>
                                        <label class="font-blue-madison bold" id="mainarea_bn"></label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="item-name font-blue-madison">Create Date: </label>
                                        <label class="font-blue-madison bold" id="created_at"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>
                                    Indicator Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label>Serial</label>
                                        <input name="serial" id="serial" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Indicator Title (EN)</label>
                                        <input name="indicator_title" id="indicator_title" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Indicator Title (BN)</label>
                                        <input name="indicator_title_bn" id="indicator_title_bn" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Status*</label><br>
                                        <input type="checkbox" name="indicator_status" id="indicator_status" data-on-text="Active" data-off-text="Inactive" style="height: 30px;" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                                <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                                <!-- </div> -->
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
                </div>
            </form>
            <!-- Cancel Modal -->
            <div id="cancel_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="/MainArea" class="btn btn-modal-cancel">Cancel Anyway</a>
                            <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        $('#main_area').on('change', function(){
            var mainAreaID = $(this).val();

            if ( mainAreaID )
            {
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/')}}/getMainAreaDetails",
                    data: {id: mainAreaID},
                    dataType: 'JSON',
                    success: function (res) {
                        $('#serial').html(res[0].id);
                        $('#mainarea_en').html(res[0].mainarea_en);
                        $('#mainarea_bn').html(res[0].mainarea_bn);
                        $('#created_at').html(res[0].created_at);
                    }
                });
            }
        });

        $(function(){
            $("#indicator_status").bootstrapSwitch();
        });

        function getUpazilaInfoList(id) {
            // $('#showloging').show();
            var value = id;
            // alert(value); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getUpazilaInfoList",
                data: {value: value},
                success: function (result) {
                    // alert(result);
                    if (result != '') {
                        $('#upazila').html(result);
                        // $('#showloging').hide();
                    } else {
                        $('#upazila').html('No Recipient Found');
                    }

                }
            }, "json");

        }
    </script>
@endsection