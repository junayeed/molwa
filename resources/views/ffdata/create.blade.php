@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="ff_data_form" method="POST" action="/ffdata"  enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="row">
            <!-- CASE DETAILS (LFET COL) START -->
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>মুক্তিযোদ্ধার প্রমাণক
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>জেলা</label>
                                        <select name="district" id="district" class="form-control input-sm" disabled>
                                            <option value=""> - জেলা বাছাই করুন - </option>
                                            @foreach($district_list as $id => $dist)
                                                <option value="{{$id}}" @if($id == $data['district_id']) selected @endif>{{$dist}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>থানা/উপজেলা</label>
                                        <select name="upazilla" id="upazilla" class="form-control input-sm" disabled>
                                            <option value=""> - থানা/উপজেলা বাছাই করুন - </option>
                                            @foreach($upzilla_list as $id => $up)
                                                <option value="{{$id}}" @if($id == $data['upazilla_id']) selected @endif>{{$up}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>তালিকার ধরন <span class="required" aria-required="true"> * </span></label>
                                        <select name="ff_type_list" id="ff_type_list" class="form-control input-sm" >
                                            <option value=""> - বাছাই করুন - </option>
                                            @foreach($ff_type_list as $id => $ff_type)
                                                <option value="{{$id}}">{{$ff_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label id="ff_label">নম্বর <span class="required" aria-required="true"> * </span></label>
                                        <input name="ff_no" id="ff_no" type="text" class="form-control input-sm" >
                                        <input name="ff_id" id="ff_id" type="hidden" value="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_name_lbl">তালিকার ধরনঃ <span id="ff_type_list_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_no_lbl">ক্রমিক / স্মারক নম্বরঃ <span id="ff_no_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_no_lbl">সনদ নম্বরঃ <span id="ff_certificate_no_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_name_lbl">মুক্তিযোদ্ধার নামঃ <span id="ff_name_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_father_name_lbl">মুক্তিযোদ্ধার পিতা/স্বামীর নামঃ <span id="ff_father_name_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_upazilla_lbl">গ্রাম/মহল্লা/ওয়ার্ড/বাসা নং/রোড নম্বরঃ<span id="ff_village_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_postoffice_lbl">পোষ্ট অফিস/ইউপি/পৌরসভাঃ <span id="ff_postoffice_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_upazilla_lbl">থানা/উপজেলাঃ <span id="ff_upazilla_value" class="ff_data">{{@$ff_details->upazilla_name ? @$ff_details->upazilla_name : '-'}}</span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_district_lbl">জেলাঃ <span id="ff_district_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_gazette_lbl">গেজেটের পৃষ্ঠাঃ <span id="ff_gazette_page_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <span id="ff_gazette_lbl">গেজেটের তারিখঃ <span id="ff_gazette_date_value" class="ff_data"></span></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <a href="javascript:void(0);" id="ff_image" target="_blank">দলিল দেখুন</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CASE DETAILS (LFET COL) END -->

            <!-- Rule Nisi (RIGHT COL) START -->
            <div class="col-md-8" id="rule_nisi_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>মুক্তিযোদ্ধার তথ্য
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label class="control-label col-lg-12">মুক্তিযোদ্ধার ছবি আপলোড করুন (ছবির ফাইলের আকার ১৫০কিলোবাইট)  <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="ff_image" id="ff_image"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>মুক্তিযোদ্ধার মাতার নাম <span class="required" aria-required="true"> * </span></label>
                                        <input id="ff_mother_name" name="ff_mother_name" class="form-control input-sm" size="16" type="text" value=""  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>জাতিয় পরিচয় পত্র নম্বর / জন্ম নিবন্ধন নম্বর <span class="required" aria-required="true"> * </span></label>
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <div class="radio" id="uniform-optionsRadios25">
                                                    <span><input type="radio" name="doc_type" id="doc_type_nid" value="nid"></span>
                                                </div>জাতিয় পরিচয় পত্র নম্বর
                                            </label>
                                            <label class="radio-inline">
                                                <div class="radio" id="uniform-optionsRadios26">
                                                    <span class=""><input type="radio" name="doc_type" id="doc_type_bcr" value="bcr"></span>
                                                </div>জন্ম নিবন্ধন নম্বর
                                            </label>
                                            <label class="radio-inline">
                                                <div class="radio disabled" id="uniform-optionsRadios27">
                                                    <span><input type="radio" name="doc_type" id="doc_type_na" value="n/a"></span>
                                                </div> প্রযোয্য নয়
                                            </label>
                                            <input id="ff_doc_no" name="ff_doc_no" class="form-control input-sm" size="16" type="text" value="" placeholder=""  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>মুক্তিযোদ্ধার জন্ম তারিখ <span class="required" aria-required="true"> * </span></label>
                                        <input id="mask_date" name="ff_dob" class="form-control input-sm" size="16" type="text" value="" data-date-format="dd-mmyyyy"  />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>মুকিতযোদ্ধা জীবিত / মৃত ? <span class="required" aria-required="true"> * </span></label>
                                        <input class="form-control m-input" data-switch="true" type="checkbox" name="ff_status" id="ff_status"
                                               data-handle-width="60" data-on-text="জীবিত" data-off-text="মৃত" data-on-color="success" data-off-color="danger" />
                                        <input type="hidden" name="ff_list_type_id" id="ff_list_type_id" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>মুক্তিযোদ্ধার রক্তের গ্রুপ </label>
                                        <select name="blood_group" id="blood_group" class="form-control input-sm">
                                            <option value=""> - বাছাই করুন - </option>
                                            @foreach($blood_group_list as $bg)
                                                <option value="{{$bg}}">{{$bg}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Rule Nisi (RIGHT COL) END -->

        </div>
        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                        <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">

        var FormValidation = function () {
        var addUserForm = function() {
            var form = $('#ff_data_form');
            var error = $('.alert-danger', form);

            //$.validator.messages.required = '';

            $("#ff_data_form").validate(
            {
                errorElement: 'span', //---- default input error message container
                errorClass: 'help-block help-block-error', //---- default input error message class
                focusInvalid: false, //---- do not focus the last invalid input
                ignore: "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'ff_mother_name'     : "required",
                        //'doc_type'           : "required",
                        'ff_doc_no'          : "required",
                        'ff_dob'             : "required", // FF DOB
                        'ff_type_list'       : "required",
                        'ff_no'              : "required",
                        'ff_image'           : "required"
                    },
                messages: { },
                errorPlacement: function(error, element) {},

                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /*submitHandler : function()
                {
                    return false;
                }*/
            });
        }
        return {
            //---- main function to initiate the module
            init: function () {
                addUserForm();
            }
        };
        }();

        $('input[name="doc_type"]').on('change', function(){
            if($('input[name="doc_type"]:checked').val() === "n/a"){
                $('#ff_doc_no').val('0');
                $('#ff_doc_no').attr('disabled', true);
            }
            else {
                $('#ff_doc_no').val('');
                $('#ff_doc_no').attr('disabled', false);
            }
        });

        $('input:file').change(
            function(e) {
                var ff_image = e.originalEvent.target.files;
                for (var i=0, len=ff_image.length; i<len; i++){
                    var n = ff_image[i].name,
                        s = ff_image[i].size,
                        t = ff_image[i].type;

                    if (s/1024 > 150) {
                        bootbox.alert('মুক্তিযোদ্ধার ছবির আকার ১৫০ কিলোবাইটের বেশী। দয়া করে মধ্যে ছবি আপলোড করুন।');
                        is_valid = false;
                        $(this).trigger('reset.bs.fileinput');
                        //e.preventDefault();
                        //e.stopPropagation()
                    }
                }
            });

        $('#ff_no').on('change', function(){
            var ff_no        = $(this).val();
            var ff_type_list = $('#ff_type_list').val();
            var district_id  = $('#district').val();
            var upazilla_id  = $('#upazilla').val();
            var path         = '';
            if (ff_type_list == 1)       path = 'indian';
            else if (ff_type_list == 25) path = 'lalbarta';
            else if (ff_type_list == 4)  path = 'register';
            else path = 'gadget';

            //alert($('input[name="doc_type"]:checked').val());

            $('.ff_data').html('');
            $('#ff_image').attr("href", "javascript:void(0)");

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getFFDetailsData",
                data: {ff_no: ff_no, ff_type_list: ff_type_list, district_id: district_id, upazilla_id: upazilla_id},
                dataType: 'JSON',
                success: function (res) {
                    if(res != -1) {
                        $('#ff_id').val(res.id);
                        $('#ff_type_list_value').html($("#ff_type_list option:selected").html());
                        $('#ff_no_value').html(res.allotted_number);
                        $('#ff_certificate_no_value').html(res.certificate_no ? res.certificate_no : '-');
                        $('#ff_name_value').html(res.ff_name);
                        $('#ff_father_name_value').html(res.ff_fathers_name);
                        $('#ff_village_value').html(res.village ? res.village : '-');
                        $('#ff_postoffice_value').html(res.postoffice ? res.postoffice : '-');
                        $('#ff_upazilla_value').html(res.upazilla_name ? res.upazilla_name : '-');
                        $('#ff_district_value').html(res.district_name ? res.district_name : '-');
                        $('#ff_gazette_date_value').html(res.gadget_date ? res.gadget_date : '-');
                        $('#ff_gazette_page_value').html(res.gadget_page_no ? res.gadget_page_no : '-');
                        $('#ff_image').attr("href", "http://ff.molwa.gov.bd/public/scanned_image/" + path + '/' + res.image_path);
                        $('#ff_list_type_id').val(res.ff_list_type_id);
                    }
                    else {
                        $('.ff_data').html('');
                        $('#ff_image').attr("href", "javascript:void(0)");
                        bootbox.alert('দুঃক্ষিত, কোন মুক্তিযোদ্ধার তথ্য পাওয়া যায়নি ।');
                    }
                }
            });
        });

        /*$('.btn-save').on('click', function (){

            /!*if ($('#case_status').val() == 3) // if the case status is SF Received
            {
                $("#sf_data_container .validate_sf_receive_date").each(function(){
                    $(this).rules("add", {required: true});
                });
            }*!/

            // if has_interim_order is checked then all the dynamic fileds in Interim Order section is required
            if($('#has_interim_order').is(':checked')) {
                $('.validateImpAuth').each(function () {
                    $(this).rules("add", {required: true});
                });
            }
            //otherwise remove the rules
            else {
                $('.validateImpAuth').each(function () {
                    $(this).rules('remove', 'required');
                });
            }
        });*/

        $(function() {
            FormValidation.init();

            $('.date-picker').datepicker({
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "dd/mm/yyyy"
            });

            //$('#subject_type').select2();

            $("#ff_status").bootstrapSwitch('state', true);
        });

    </script>
@endsection