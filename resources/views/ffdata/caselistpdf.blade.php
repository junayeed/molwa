<html lang="fa">
<body>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        @media print {
            .table_pdf {
                overflow-x: visible !important;
            }
        }
        body {
            font-family: sans;
        }

        th {
            /*background: #0b94ea;
            color: white;*/
            font-size: 15px;
            font-weight: bold;
            border: 1px solid #c9c9c9;
        }

        td {
            font-size: 14px;
            text-align: center;
            border: 1px solid #c9c9c9;
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            margin-top: 60px;
        }

        h1 {
            margin: 20px 10px 10px 10px;
            font-size: 25px;
        }

        h4{
            font-size: 15px;
            margin-bottom: 20px;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }

        .text-center {
            text-align: center
        }

        .header-text{ font-size: 11px; padding-bottom: 4px;}
        .footer-text{ font-size: 9px;}
        .case_subject_bn{font-family: 'nikosh';}
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        {{--<div class="text-center header-text">Ministry of Liberation War Affairs</div>--}}
        <div class="text-center header-text">Case List</div>
    </htmlpageheader>
    <htmlpagefooter name="page-footer">
        <div class="text-center footer-text"> Page: {PAGENO} of {nb}</div>
        <div class="text-center footer-text"> This is an automatic report generated.</div>
        <div class="text-center footer-text"> All rights reserve M/O Liberation War Affairs (MOLWA).</div>

    </htmlpagefooter>


        <table class="table_pdf">
            <thead>
            <tr>
                <th class="table_heading">Serial</th>
                <th class="table_heading">File No</th>
                <th class="table_heading">Case No</th>
                <th class="table_heading">Receive Date</th>
                <th class="table_heading">Subject</th>
                <th class="table_heading">Status</th>
                <th class="table_heading">Rule Nisi</th>
                <th class="table_heading">Interim Order</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($case_list as $case)
                {{ $class = '' }}
                <tr class="odd gradeX">
                    <td>{{$i++}}</td>
                    <td>{{$case->file_no}}</td>
                    <td>{{$case->case_no}}
                        @if ($case->case_type == 'WP')
                            <label class="label bg-red-thunderbird bg-font-red-thunderbird">{{$case->case_type}}</label>
                        @else
                            <label class="label bg-purple-plum bg-font-purple-plum">{{$case->case_type}}</label>
                        @endif
                    </td>
                    <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                    <td class="case_subject_bn">{{str_limit($case->case_subject, $limit = 50, $end = '...')}}</td>
                    <td>
                        @if ($case->case_status_id == 1)
                            @php ($class = 'bg-red-flamingo')
                        @elseif ($case->case_status_id == 2)
                            @php ($class = 'bg-yellow-gold')
                        @elseif ($case->case_status_id == 3)
                            @php ($class = 'bg-yellow-soft')
                        @elseif ($case->case_status_id == 4)
                            @php ($class = 'bg-green-jungle')
                        @elseif ($case->case_status_id == 5)
                            @php ($class = 'bg-green-meadow')
                        @endif
                        <label class="label {{$class}}">{{$case->case_status}}</label>
                    </td>
                    <td class="text-center">
                        @if ($case->has_rule_nisi == 1)
                            @php ($rule_nisi_class = 'bg-red-flamingo')
                            @php ($rule_nisi_text = 'Y')
                        @elseif ($case->has_rule_nisi == 0)
                            @php ($rule_nisi_class = 'bg-green')
                            @php ($rule_nisi_text = 'N')
                        @endif
                        <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                    </td>
                    <td class="text-center">
                        @if ($case->has_interim_order == 1)
                            @php ($interim_order_class = 'bg-red-flamingo')
                            @php ($interim_order_text = 'Y')
                        @elseif ($case->has_interim_order == 0)
                            @php ($interim_order_class = 'bg-green')
                            @php ($interim_order_text = 'N')
                        @endif
                        <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>



    <!-- END CONTENT -->
</body>
</html>