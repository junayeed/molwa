@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green-jungle btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-pencil"></i> EXPORT TO</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">PDF</a></li>
                                <li class="divider"> </li>
                                <li><a href="javascript:;">Excel</a></li>
                            </ul>
                        </div>
                        <a id="sample_editable_1_new" class="btn green btn-circle btn-add-new" href="/ffdata/create"><i class="fa fa-plus"></i> Add New </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th class="table_heading">ক্রমিক</th>
                            <th class="table_heading">ছবি</th>
                            <th class="table_heading">তালিকার ধরন</th>
                            <th class="table_heading">ক্রমিক/স্মারক</th>
                            <th class="table_heading">নাম</th>
                            <th class="table_heading">পিতার নাম</th>
                            <th class="table_heading">মাতার নাম</th>
                            <th class="table_heading">{{--মুক্তিযোদ্ধার--}} অবস্থা</th>
                            <th class="table_heading">দাখিলের তারিখ</th>
                            <th class="table_heading"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($ff_list as $ff)
                            {{ $class = '' }}
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td>
                                    <img src="{{$ff->ff_image_path}}" width="50">
                                </td>
                                <td>{{$ff->list_type_name}}</td>
                                <td>{{$ff->ff_no}}</td>
                                <td>{{$ff->ff_name}}</td>
                                <td>{{$ff->ff_fathers_name}}</td>
                                <td>{{$ff->ff_mother_name}}</td>
                                <td>
                                    @if($ff->ff_status == 1)
                                        <label class="label bg-green-jungle">জীবিত </label>
                                    @else
                                        <label class="bg-red-flamingo">মৃত </label>
                                    @endif
                                </td>
                                <td>{{\Carbon\Carbon::parse($ff->created_at)->format('d M, Y')}}</td>

                                <td class="text\-center no-print">
                                    <a class="btn btn-xs yellow  btn-circle " href="/ffdata/{{$ff->id}}/edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END CONTENT -->

     <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#table_list_view').DataTable({});

            $('.delete-case').on('click', function(){
                var caseID = $(this).data('id');
                var that        = this;

                bootbox.confirm({
                    message: "Are you sure you want to delete this case/pitition? ",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn btn-danger'
                        }
                    },
                    callback: function (result) {
                        if ( result ){
                            $.ajax({
                                type     : "GET",
                                url      : "{{URL::to('/')}}/deleteCase",
                                data     : {caseID: caseID},
                                dataType : 'JSON',
                                success  : function (res) {
                                    $(that).closest('tr').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value ).draw();
                    }
                } );
            } );
        } );
    </script>

@endsection