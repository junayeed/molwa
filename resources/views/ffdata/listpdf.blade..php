<div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green-jungle btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-pencil"></i> EXPORT TO</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">PDF</a></li>
                                <li class="divider"> </li>
                                <li><a href="javascript:;">Excel</a></li>
                            </ul>
                        </div>
                        <a id="sample_editable_1_new" class="btn green btn-circle btn-add-new" href="/forwardDairy/create"><i class="fa fa-plus"></i> Add New </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th class="table_heading">Serial</th>
                            <th class="table_heading">File No</th>
                            <th class="table_heading">Case No</th>
                            <th class="table_heading">Receive Date</th>
                            <th class="table_heading">Subject</th>
                            <th class="table_heading">Status</th>
                            <th class="table_heading">Rule Nisi</th>
                            <th class="table_heading">Interim Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($case_list as $case)
                            {{ $class = '' }}
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td>{{$case->file_no}}</td>
                                <td>{{$case->case_no}} 
                                    @if ($case->case_type == 'WP')
                                        <label class="label bg-red-thunderbird bg-font-red-thunderbird">{{$case->case_type}}</label>
                                    @else
                                        <label class="label bg-purple-plum bg-font-purple-plum">{{$case->case_type}}</label>
                                    @endif
                                </td>
                                <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                                <td>{{str_limit($case->case_subject, $limit = 50, $end = '...')}}</td>
                                <td>
                                    @if ($case->case_status_id == 1)
                                        @php ($class = 'bg-red-flamingo')
                                    @elseif ($case->case_status_id == 2)
                                        @php ($class = 'bg-yellow-gold')
                                    @elseif ($case->case_status_id == 3)
                                        @php ($class = 'bg-yellow-soft')
                                    @elseif ($case->case_status_id == 4)
                                        @php ($class = 'bg-green-jungle')
                                    @elseif ($case->case_status_id == 5)
                                        @php ($class = 'bg-green-meadow')
                                    @endif
                                    <label class="label {{$class}}">{{$case->case_status}}</label>
                                </td>
                                <td class="text-center">
                                    @if ($case->has_rule_nisi == 1)
                                        @php ($rule_nisi_class = 'bg-red-flamingo')
                                        @php ($rule_nisi_text = 'Y')
                                    @elseif ($case->has_rule_nisi == 0)
                                        @php ($rule_nisi_class = 'bg-green')
                                        @php ($rule_nisi_text = 'N')
                                    @endif
                                    <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                                </td>
                                <td class="text-center">
                                    @if ($case->has_interim_order == 1)
                                        @php ($interim_order_class = 'bg-red-flamingo')
                                        @php ($interim_order_text = 'Y')
                                    @elseif ($case->has_interim_order == 0)
                                        @php ($interim_order_class = 'bg-green')
                                        @php ($interim_order_text = 'N')
                                    @endif
                                    <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END CONTENT -->
