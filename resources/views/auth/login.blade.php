@extends('layouts.login')

@section('content')

    <body class="login">
    <!-- BEGIN : LOGIN PAGE 5-1 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-7 bs-reset">
                <div class="login-bg" style="background-image:url(../assets/pages/img/login/bg5.jpg)">
                    {{--<img class="login-logo" src="../assets/pages/img/login/logo.png" />--}}
                </div>
            </div>
            <div class="col-md-4 login-container bs-reset">
                <div class="login-content">
                    {{--<h1>Metronic Admin Login</h1>--}}
                    <!-- BEGIN LOGIN FORM -->
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <h3 class="form-title">Login to your account</h3>
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <span>
			Enter any username and password. </span>
                        </div>
                        <div class="form-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label visible-ie8 visible-ie9">Username</label>
                            <div class="input-icon">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control placeholder-no-fix" required name="email" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input type="password" required placeholder="Password" class="form-control placeholder-no-fix" name="password">
                            </div>
                        </div>
                        <div class="form-actions">
                            {{--<label class="checkbox">
                                <input type="checkbox" name="remember" value="1"/> Remember me </label>--}}
                            <button type="submit" class="btn blue pull-right">
                                Login <i class="m-icon-swapright m-icon-white"></i>
                            </button>
                        </div>
                        {{--<div class="forget-password">
                            <h4>Forgot your password ?</h4>
                            <p>
                                no worries, click <a href="javascript:;" id="forget-password">
                                    here </a>
                                to reset your password.
                            </p>
                        </div>--}}

                    </form>
                    <!-- END LOGIN FORM -->
                    <!-- BEGIN FORGOT PASSWORD FORM -->
                    {{--<form class="forget-form" action="javascript:;" method="post">
                        <h3 class="font-green">Forgot Password ?</h3>
                        <p> Enter your e-mail address below to reset your password. </p>
                        <div class="form-group">
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                        <div class="form-actions">
                            <button type="button" id="back-btn" class="btn grey btn-default">Back</button>
                            <button type="submit" class="btn blue btn-success uppercase pull-right">Submit</button>
                        </div>
                    </form>--}}
                    <!-- END FORGOT PASSWORD FORM -->
                </div>
                <div class="login-footer">
                    <div class="row bs-reset">
                        <div class="col-xs-4 bs-reset">
                            <ul class="login-social">
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-dribbble"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-8 bs-reset">
                            <div class="login-copyright text-right">
                                <p>Copyright &copy; Ministry of Liberation War Affairs 2018</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-1 -->


    <!-- BEGIN COPYRIGHT -->
    {{--<div class="copyright">
        2018 &copy; MOLWA - Ministry of Liberation War Affairs
    </div>--}}
    <!-- END COPYRIGHT -->

    <script>
        var ocean = document.getElementById("ocean"),
            waveWidth = 10,
            waveCount = Math.floor(window.innerWidth / waveWidth),
            docFrag = document.createDocumentFragment();

        for (var i = 0; i < waveCount; i++) {
            var wave = document.createElement("div");
            wave.className += " wave";
            docFrag.appendChild(wave);
            wave.style.left = i * waveWidth + "px";
            wave.style.webkitAnimationDelay = (i / 120) + "s";
        }

        ocean.appendChild(docFrag);
    </script>
@endsection
