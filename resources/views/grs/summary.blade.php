@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn red-thunderbird btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-sitemap"></i> MONTHS</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">সকল</a></li>
                                <li class="divider"> </li>
                                @foreach($month_list as $id => $month)
                                    <li><a href="/exportForwardDairyToPDF">{{$month}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="btn-group">
                            <a class="btn red-thunderbird btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-sitemap"></i> YEARS</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">PDF</a></li>
                                <li class="divider"> </li>
                                <li><a href="javascript:;">Excel</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <a class="btn purple-seance btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-sitemap"></i> SECTIONS</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">সকল</a></li>
                                <li class="divider"> </li>
                                @foreach($section_list as $section)
                                <li><a href="/exportForwardDairyToPDF">{{$section->section_name_bn}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="btn-group">
                            <a class="btn green-jungle btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-pencil"></i> EXPORT TO</a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="/exportForwardDairyToPDF">PDF</a></li>
                                <li class="divider"> </li>
                                <li><a href="javascript:;">Excel</a></li>
                            </ul>
                        </div>
                        {{--<a id="sample_editable_1_new" class="btn green btn-circle btn-add-new" href="/forwardDairy/create"><i class="fa fa-plus"></i> Add New </a>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    মাসঃ {{$month_list[$current_month]}}
                    <table class="table table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th class="table_heading text-center">ক্রমিক</th>
                            <th class="table_heading text-center">শাখা/অধিশাখার নাম</th>
                            <th class="table_heading text-center">পত্র/দরখাস্তযোগে</th>
                            <th class="table_heading text-center">অনলাইনে</th>
                            <th class="table_heading text-center">পূর্বের জের</th>
                            <th class="table_heading text-center">মোট (৩+৪+৫)</th>
                            <th class="table_heading text-center">নিষ্পত্তিকৃত অভিযোগের সংখ্যা</th>
                            <th class="table_heading text-center">নিষ্পত্তিকৃত অভিযোগের সংক্ষিপ্ত বিবরণ</th>
                            <th class="table_heading text-center">অনিষ্পত্তিকৃত অভিযোগের সংখ্যা</th>
                            <th class="table_heading text-center">মন্তব্য</th>
                            <th class="table_heading text-center no-print">Actions</th>
                        </tr>
                        <tr>
                            <th class="table_heading text-center">(১)</th>
                            <th class="table_heading text-center">(২)</th>
                            <th class="table_heading text-center">(৩)</th>
                            <th class="table_heading text-center">(৪)</th>
                            <th class="table_heading text-center">(৫)</th>
                            <th class="table_heading text-center">(৬)</th>
                            <th class="table_heading text-center">(৭)</th>
                            <th class="table_heading text-center">(৮)</th>
                            <th class="table_heading text-center">(৯)</th>
                            <th class="table_heading text-center">(১০)</th>
                            <th class="table_heading text-center no-print"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($grs_data_list as $grs_data)
                            {{ $class = '' }}
                            <tr class="odd gradeX">
                                <td class="text-center">{{$i++}}</td>
                                <td class="text-center">{{$grs_data->section_name_bn}}</td>
                                <td class="text-center">{{$grs_data->no_of_application}}</td>
                                <td class="text-center">{{$grs_data->no_of_online}}</td>
                                <td class="text-center">{{$non_disposal_list[$grs_data->section_id]}}</td>
                                <td class="text-center">{{$grs_data->total}}</td>
                                <td class="text-center">{{$grs_data->total_disposal}}</td>
                                <td class="text-center">{{$grs_data->total_non_disposal}}</td>
                                <td class="text-center">{{$grs_data->short_desc}}</td>
                                <td class="text-center">{{$grs_data->comments}}</td>
                                <td class="text-center no-print">
                                    <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$grs_data->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$grs_data->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END CONTENT -->

     <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#table_list_view').DataTable({});

            $('.delete-case').on('click', function(){
                var caseID = $(this).data('id');
                var that        = this;

                bootbox.confirm({
                    message: "Are you sure you want to delete this case/pitition? ",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn btn-danger'
                        }
                    },
                    callback: function (result) {
                        if ( result ){
                            $.ajax({
                                type     : "GET",
                                url      : "{{URL::to('/')}}/deleteCase",
                                data     : {caseID: caseID},
                                dataType : 'JSON',
                                success  : function (res) {
                                    $(that).closest('tr').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value ).draw();
                    }
                } );
            } );
        } );
    </script>

@endsection