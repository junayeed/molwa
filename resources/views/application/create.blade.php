@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/Application">Application</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Add New Application</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Add New Application
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/Application">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>
                                    Application Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Name of Application</label>
                                        <input name="name" id="name" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Name of Application (BN)</label>
                                        <input name="name_bangla" id="name_bangla" type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Year</label>
                                        <select class="form-control select2" name="year" id="year" required>
                                            <option value="">Please Select</option>
                                            <?php $year=date('Y'); for ($y = $year-5; $y<=$year; $y++) {?>
                                                <option value="{{$y}}" <?php if($y == $year) echo "selected"; ?> >{{$y}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Period</label>
                                        <select class="form-control" name="month" id="month">
                                            <option value="">Please Select</option>
                                            <option value="1">January-June</option>
                                            <option value="2">July-December</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Status*</label><br>
                                        <input type="checkbox" name="status" id="status" data-on-text="Active" data-off-text="Inactive" style="height: 30px;" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                                <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                                <!-- </div> -->
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
                </div>
            </form>
            <!-- Cancel Modal -->
            <div id="cancel_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                            <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        $('#indicator_id').on('change', function(){
            var indicatorID = $(this).val();

            if ( indicatorID )
            {
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/')}}/getIndicatorDetails",
                    data: {id: indicatorID},
                    dataType: 'JSON',
                    success: function (res) {
                        $('#serial').html(res[0].id);
                        $('#indicator_title').html(res[0].indicator_title);
                        $('#indicator_title_bn').html(res[0].indicator_title_bn);
                        $('#created_at').html(res[0].created_at);
                    }
                });
            }
        });

        $(function(){
            $("#status").bootstrapSwitch();
        });
    </script>
@endsection