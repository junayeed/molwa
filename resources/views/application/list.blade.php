@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Application</span>
                    </li>
                </ul>
                <h1 class="page-title bold">List of Application(s)</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="actions">
                                @if(Auth::user()->user_level==4 && Auth::user()->user_type == 0)
                                <a id="sample_editable_1_new" class="btn btn-primary mt-ladda-btn ladda-button mt-progress-demo btn-circle" href="/Application/create"><i class="fa fa-plus"></i> Add Application </a>
                                @endif
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                                <thead>
                                    <tr>
                                        <th class="table_heading">Serial</th>
                                        <th class="table_heading">Name (EN)</th>
                                        <th class="table_heading">Year</th>
                                        <th class="table_heading">Month</th>
                                        <th class="table_heading">Status</th>
                                        <th class="table_heading text-center no-print">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @if(isset($application_list))
                                @foreach($application_list as $rowdata)
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td><a href="/ApplicationData/{{$rowdata->id}}">{{$rowdata->name}}</a></td>
                                        <td>{{$rowdata->year}}</td>
                                        @if($rowdata->month == 1)
                                        <td>January-June</td>
                                        @else
                                        <td>July-December</td>
                                        @endif

                                        @if($rowdata->data_status == 0)
                                        <td style="color: red;"><b>Draft</b></td>
                                        @elseif($rowdata->data_status == 1)
                                        <td style="color: green;"><b>Send to Reviewer</b></td>
                                        @elseif($rowdata->data_status == 2)
                                        <td style="color: red;"><b>Back from Reviewer</b></td>
                                        @elseif($rowdata->data_status == 3)
                                        <td style="color: green;"><b>Submitted to Head Office</b></td>
                                        @elseif($rowdata->data_status == 4)
                                        <td style="color: red;"><b>Back From Head Office</b></td>
                                        @endif


                                        @if($user_level == 'Upazila')
                                        <td class="text-center no-print">
                                            @if(($rowdata->data_status ==0 || $rowdata->data_status ==2) && Auth::user()->user_level == 4 && Auth::user()->user_type == 0)
                                            <a class="btn btn-primary btn-sm mt-ladda-btn ladda-button btn-circle" href="/ApplicationDataCreate/{{$rowdata->id}}"><i class="fa fa-eye"></i> Data </a>
                                            <a class="btn btn-danger btn-sm mt-ladda-btn ladda-button btn-circle" href="/DeleteApplication/{{$rowdata->id}}"><i class="fa fa-trash"></i> Delete</a>
                                            @endif


                                        </td>
                                        @else
                                        <td>
                                            <a class="btn btn-view" href="/ApplicationData/{{$rowdata->id}}"><i class="fa fa-eye"></i> Show</a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        /*$(document).ready(function() {
            $('#table_list_view').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );*/
    </script>

     <script type="text/javascript">
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                                .search( this.value )
                                .draw();
                    }
                } );
            } );
        } );
    </script>

@endsection