@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-2">
            <div class="mt-element-list">
                <div class="mt-list-head list-default font-white bg-dark">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-head-title-container">
                                <h3 class="list-title font-green bold ">SMS Stats</h3>
                                <div class="list-date">{{@$sms_balance}}9999</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="mt-element-list">
                <div class="mt-list-head list-default font-white bg-dark">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-head-title-container">
                                <h3 class="list-title font-green bold">মামলা ব্যবস্থাপনা শীর্ষ অবদানকারী </h3>
                                <div class="list-date">{{$total_cases}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row scroller" style="max-height:350px">
                        @foreach($top_contributors as $tc)
                        <div class="col-md-2">
                            <div class="list-head-summary-container">
                                <div class="list-done margin-bottom-10">
                                    <div class="list-count font-grey bg-grey-mint" style="font-size: 24px; font-weight: bold; color: #00ff1c !important; padding: 11px 9px;">{{sprintf("%03d", $tc->total_entry)}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="list-head-summary-container">
                                <div class="list-done margin-bottom-10">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title" style="margin: 0 0 .2em !important;">{{$tc->name}}</h3>
                                        <div class="list-date">{{$tc->designation}}</div>
                                        <a href="javascript:void(0)" class="view_top_contributer" data-user_id="{{$tc->id}}" data-toggle="modal" data-target="#view_top_contributer_modal"> View Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="mt-element-list">
                <div class="mt-list-head list-default font-white bg-dark">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-head-title-container">
                                <h3 class="list-title font-green bold">Top Contributer for Cheque Entry</h3>
                                <div class="list-date">{{@$total_cheques}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row scroller" style="max-height:350px">
                        @foreach($top_cheque_contributors as $tcc)
                            <div class="col-md-2">
                                <div class="list-head-summary-container">
                                    <div class="list-done margin-bottom-10">
                                        <div class="list-count font-grey bg-grey-mint" style="font-size: 24px; font-weight: bold; color: #00ff1c !important; padding: 11px 9px;">{{sprintf("%03d", $tcc->total_entry)}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="list-head-summary-container">
                                    <div class="list-done margin-bottom-10">
                                        <div class="list-head-title-container">
                                            <h3 class="list-title" style="margin: 0 0 .2em !important;">{{$tcc->name}}</h3>
                                            <div class="list-date">{{$tcc->designation}}</div>
                                            <a href="javascript:void(0)" class="view_hat_bazar_contributer_details" data-user_id="{{$tcc->id}}" data-toggle="modal" data-target="#view_top_contributer_modal"> View Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="mt-element-list">
                <div class="mt-list-head list-default font-white bg-dark">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-head-title-container">
                                <h3 class="list-title font-green bold">আসন্ন প্রশিক্ষণসমূহ</h3>
                                <div class="list-date"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row scroller" style="max-height:350px">
                        @foreach($upcoming_trainings as $tr)
                            <div class="col-md-2">
                                <div class="list-head-summary-container">
                                    <div class="list-done margin-bottom-10">
                                        <div class="list-count font-grey bg-grey-mint" style="font-size: 24px; font-weight: bold; color: #00ff1c !important; padding: 11px 9px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="list-head-summary-container">
                                    <div class="list-done margin-bottom-10">
                                        <div class="list-head-title-container">
                                            <h3 class="list-title" style="margin: 0 0 .2em !important;"><a href="\batch\update\{{$tr->id}}">{{$tr->training_title}}</a></h3>
                                            <div class="list-date">{{\Carbon\Carbon::parse($tr->batch_start_date)->format('d/m/Y')}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


    <div class="modal fade" id="view_top_contributer_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-scrollable " role="document">
            <div class="modal-content" id="top_contributer_details"></div>
        </div>
    </div>

    <script src="{{asset('js/jquery.counterup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.waypoints.min.js')}}" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.view_top_contributer').on('click', function () {
                var user_id = $(this).data('user_id');

                $.ajax({
                    type: "GET",
                    url: "getContributorDetails",
                    data: {user_id: user_id, type: 'case'},
                    dataType: 'html',
                    success: function (res) {
                        $('#top_contributer_details').html(res);
                    }
                });
            });

            $('.view_hat_bazar_contributer_details').on('click', function () {
                var user_id = $(this).data('user_id');

                $.ajax({
                    type: "GET",
                    url: "getContributorDetails",
                    data: {user_id: user_id, type: 'hatbazar'},
                    dataType: 'html',
                    success: function (res) {
                        $('#top_contributer_details').html(res);
                    }
                });
            });
        });
    </script>
@endsection