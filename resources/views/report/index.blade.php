@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/report">Reports</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Reports</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Reports
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/report">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label>Name</label>
                                        <input name="application_name" id="application_name" type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="from_date">From Date</label>
                                        <input id="from_date" name="from_date" class="form-control date-picker" size="16" placeholder="yyyy/mm/dd" type="text" value=""/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="to_date">To Date</label>
                                        <input id="to_date" name="to_date" class="form-control date-picker" size="16" placeholder="yyyy/mm/dd" type="text" value=""/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-4">
                                        <label>Year</label>
                                        <select class="form-control" name="year" id="year">
                                            <option value="">Please Select</option>
                                            <?php
                                                $y = (int) date("Y");
                                                for ($i = $y-5; $i<=$y; $i++) {?>
                                                <option value="{{$i}}">{{$i}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Month</label>
                                        <select class="form-control" name="month" id="month">
                                            <option value="">Please Select</option>
                                            <option value="1">January-June</option>
                                            <option value="2">July-December</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="button" class="btn btn-save" onclick="getReport()"><i class="fa fa-floppy-o"></i> Report </button>
                                <a href="/" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box light">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                            <thead>
                                <tr>
                                    <th class="table_heading">Serial</th>
                                    <th class="table_heading">Name (EN)</th>
                                    <th class="table_heading">Name (BG)</th>
                                    <th class="table_heading">Year</th>
                                    <th class="table_heading">Month</th>
                                    <th class="table_heading text-center no-print">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="application_data">
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
    function getIndicatorInfoRow(id) {
            // $('#showloging').show();
            var main_area_id = id;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getIndicatorInfoRow",
                data: {main_area_id: main_area_id},
                success: function (result) {
                    $('#indicator').html(result);
                }
            }, "json");

        }
    
    function getSubIndicatorInfoRow(id) {
            // $('#showloging').show();
            var indicator_id = id;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getSubIndicatorInfoRow",
                data: {indicator_id: indicator_id},
                success: function (result) {
                    $('#sub_indicator').html(result);
                }
            }, "json");

        }
    
    function getReport() {
            // $('#showloging').show();
            var application_name = document.getElementById('application_name').value;
            var from_date = document.getElementById('from_date').value;
            var to_date = document.getElementById('to_date').value;
            var year = document.getElementById('year').value;
            var month = document.getElementById('month').value;
            // alert(application_name); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getReport",
                data: {application_name: application_name, from_date: from_date, to_date: to_date, year: year, month: month },
                success: function (result) {
                    // alert(result); die();
                    $('#application_data').html(result);
                }
            }, "json");

        }
    
    </script>
@endsection