@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/Application">Application</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Add New Application</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Add New Application
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/report">
                {{csrf_field()}}

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label>Main Area</label>
                                        <select class="form-control" name="main_area" id="main_area" onchange="getIndicatorInfoRow(this.value)">
                                            <option value="">Please Select</option>
                                            @foreach($main_area_list as $rowdata)
                                            <option value="{{$rowdata->id}}">{{$rowdata->mainarea_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Indicator</label>
                                        <select class="form-control" name="indicator" id="indicator" onchange="getSubIndicatorInfoRow(this.value)">
                                        <option value="">Please Select</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Sub-Indicator</label>
                                        <select class="form-control" name="sub_indicator" id="sub_indicator">
                                        <option value="">Please Select</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-4">
                                        <label>Name</label>
                                        <input name="application_name" id="application_name" type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="from_date">From Date</label>
                                        <input id="from_date" name="from_date" class="form-control date-picker" size="16" placeholder="yyyy/mm/dd" type="text" value=""/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="to_date">To Date</label>
                                        <input id="to_date" name="to_date" class="form-control date-picker" size="16" placeholder="yyyy/mm/dd" type="text" value=""/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-4">
                                        <label>Year</label>
                                        <select class="form-control" name="year" id="year">
                                            <option value="">Please Select</option>
                                            <?php for ($y = 2000; $y<=2100; $y++) {?>
                                                <option value="{{$y}}">{{$y}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Month</label>
                                        <select class="form-control" name="month" id="month">
                                            <option value="">Please Select</option>
                                            <option value="1">January-June</option>
                                            <option value="2">July-December</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="button" class="btn btn-save" onclick="getCumulativeReport()"><i class="fa fa-floppy-o"></i> Report </button>
                                <a href="/" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box light">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Main Area</th>
                                    <th>Indicator</th>
                                    <th>Sub-Indicator</th>
                                    <th>Amount in Indicator/Sub-Indicator</th>
                                    <th>Female</th>
                                    <th>Male</th>
                                    <th>Total</th>
                                    <th>Female(%)</th>
                            </thead>
                            <tbody id="application_data">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
    function getIndicatorInfoRow(id) {
            // $('#showloging').show();
            var main_area_id = id;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getIndicatorInfoRow",
                data: {main_area_id: main_area_id},
                success: function (result) {
                    $('#indicator').html(result);
                }
            }, "json");

        }
    
    function getSubIndicatorInfoRow(id) {
            // $('#showloging').show();
            var indicator_id = id;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getSubIndicatorInfoRow",
                data: {indicator_id: indicator_id},
                success: function (result) {
                    $('#sub_indicator').html(result);
                }
            }, "json");

        }
    
    function getCumulativeReport() {
            // $('#showloging').show();
            var main_area = document.getElementById('main_area').value;
            var indicator = document.getElementById('indicator').value;
            var sub_indicator = document.getElementById('sub_indicator').value;
            var application_name = document.getElementById('application_name').value;
            var from_date = document.getElementById('from_date').value;
            var to_date = document.getElementById('to_date').value;
            var year = document.getElementById('year').value;
            var month = document.getElementById('month').value;
            // alert(main_area); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getCumulativeReport",
                data: {main_area: main_area, indicator: indicator, sub_indicator: sub_indicator, application_name: application_name, from_date: from_date, to_date: to_date, year: year, month: month },
                success: function (result) {
                    $('#application_data').html(result);
                }
            }, "json");

        }
    
    </script>
@endsection