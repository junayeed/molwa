<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'/>
    <title>Login - MOLWA</title>
    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.4.1/snap.svg-min.js"></script>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/components.min.css')}}" rel="stylesheet" type="text/css">
    {{--<link href="{{asset('css/login-soft.css')}}" rel="stylesheet" type="text/css">--}}
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />


    <script src="{{asset('js/core.js')}}" type="text/javascript"></script>

    <style>
        .text {
            color: white;
        }
    </style>
</head>

<body>
@yield('content')

</body>
</html>

