<!DOCTYPE html>
<html lang="en" class="ie8 no-js">
<html lang="en" class="ie9 no-js">
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>MOLWA Office</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="MOLWA" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN LAYOUT FIRST STYLES -->
    {{--<link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />--}}
    <!-- END LAYOUT FIRST STYLES -->

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />--}}
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
{{--<link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />


    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/layouts/layout5/css/custom.min.css" rel="stylesheet" type="text/css" />


    <link href="{{ Config::get('constants.CONSTANT_NAME') }}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />

        <link rel="{{asset('shortcut icon')}}" href="{{asset('images/logo/favicon.png')}}"/>
    <script src="{{asset('js/core.js')}}" type="text/javascript"></script>

    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <?php
    $route_full_name = Request::route()->getName();
    $route_array = explode('.', $route_full_name);
    $route_name = $route_array[0];
    //$route_name = Route::currentRouteName();
    ?>
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo">
    <!-- BEGIN CONTAINER -->
    <div class="wrapper">
        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar mega-menu" role="navigation">
                <div class="container-fluid">
                    <div class="clearfix navbar-fixed-top">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                        </button>
                        <!-- End Toggle Button -->
                        <!-- BEGIN LOGO -->
                        <a id="index" class="page-logo" href="index.html">
                            <img src="/assets/layouts/layout5/img/bd_logo.png" alt="Logo"></a>
                        <!-- END LOGO -->

                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="topbar-actions">
                            <!-- BEGIN GROUP INFORMATION -->
                            @if( Auth::user()->user_type != 99 )
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li><a href="/forwardDairy/create">Add New Forwad Dairy</a></li>
                                        <li class="divider"></li>
                                        <li><a href="/Scholarship/create" data-toggle="modal" data-target="#send_sms_modal">Send SMS</a></li>
                                    </ul>
                                </div>
                            @endif
                            <!-- END GROUP INFORMATION -->
                            <!-- BEGIN USER PROFILE -->

                            <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span>Welcome, {{ Auth::user()->name }} </span>
                                    <img src="/assets/layouts/layout5/img/avatar.png" alt=""> </button>
                                    @if( Auth::user()->user_type != 99 )
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="page_user_profile_1.html">
                                                <i class="icon-user"></i> My Profile
                                                <span class="badge badge-danger">1</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="app_calendar.html">
                                                <i class="icon-calendar"></i> My Calendar </a>
                                        </li>
                                        <li class="divider"> </li>

                                        <li>
                                            <a href="/logoutuser">
                                                <i class="icon-key"></i> Log Out </a>
                                        </li>
                                    </ul>
                                    @endif
                            </div>
                            <!-- END USER PROFILE -->

                        </div>
                        <!-- END TOPBAR ACTIONS -->
                    </div>
                    <!-- BEGIN HEADER MENU -->
                    <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown dropdown-fw  @if (@$data['nav_item'] == 'Home') {{'active open selected'}} @endif">
                                <a href="/" class="text-uppercase" style="font-size: 22px !important;">
                                    <i class="icon-home"></i> ড্যাশবোর্ড </a>
                            </li>
                            {{--<li class="dropdown dropdown-fw @if (@$data['nav_item'] == 'ForwardDairy2') {{'active open selected'}} @endif">--}}
                            <li class="dropdown dropdown-fw @if (@$data['nav_item'] == 'ForwardDairy') {{'active open selected'}} @endif">
                                <a href="/forwardDairy" class="text-uppercase" style="font-size: 22px !important;"> <i class="fa fa-cogs"></i>মামলা ব্যবস্থাপনা</a>
                                {{--<ul class="dropdown-menu dropdown-menu-fw">
                                    <li class="@if (@$data['nav_sub_item'] == 'index') {{'active'}} @endif">
                                        <a href="/ForwardDairy2"><i class="icon-bar-chart"></i> রিট </a>
                                    </li>
                                    <li class="@if (@$data['nav_sub_item'] == 'create') {{'active'}} @endif">
                                        <a href="/ForwardDairy2/create"><i class="icon-book-open  "></i> আপিল </a>
                                    </li>
                                    <li class="@if (@$data['nav_sub_item'] == 'search') {{'active'}} @endif">
                                        <a href="/ForwardDairy2"><i class="fa fa-search  "></i> রিভিউ </a>
                                    </li>
                                    <li class="@if (@$data['nav_sub_item'] == 'search') {{'active'}} @endif">
                                        <a href="/ForwardDairy2"><i class="fa fa-search  "></i> এস এফ </a>
                                    </li>
                                    <li class="@if (@$data['nav_sub_item'] == 'search') {{'active'}} @endif">
                                        <a href="/ForwardDairy2"><i class="fa fa-search  "></i> আবেদন নিষ্পত্তি </a>
                                    </li>
                                    <li class="@if (@$data['nav_sub_item'] == 'search') {{'active'}} @endif">
                                        <a href="/ForwardDairy2"><i class="fa fa-search  "></i> রিট নিষ্পত্তি </a>
                                    </li>
                                </ul>--}}
                            </li>
                            <li class="dropdown more-dropdown @if (@$data['nav_item'] == 'HatBazar') {{'active open selected'}} @endif">
                                <a href="javascript:;" class="text-uppercase" style="font-size: 22px !important;"> <i class="fa fa-cogs"></i> হাট বাজার </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="/hatbazar" style="font-size: 20px !important;"><i class="fa fa-users"></i> চেক প্রাপ্তি </a>
                                    </li>
                                    <li>
                                        <a href="/hatbazarreport" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> প্রতিবেদন </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown more-dropdown @if (@$data['nav_item'] == 'Batch' || @$data['nav_item'] == "TrainingReport") {{'active  selected'}} @endif">
                                <a href="javascript:;" class="text-uppercase" style="font-size: 22px !important;"> <i class="fa fa-cogs"></i> প্রশিক্ষণ </a>
                                <ul class="dropdown-menu">
                                    <li><a href="/batch" style="font-size: 20px !important;"><i class="fa fa-users"></i> ব্যাচ </a></li>
                                    <li><a href="/trainingreport" style="font-size: 20px !important;"><i class="fa fa-clone"></i> প্রতিবেদন </a></li>
                                    <li><a href="/trainingcalendar" style="font-size: 20px !important;"><i class="fa fa-calendar-plus-o"></i> প্রশিক্ষণ ক্যালেন্ডার </a></li>
                                </ul>
                            </li>

                            <li class="dropdown more-dropdown @if (@$data['nav_item'] == 'Book' || @$data['nav_item'] == "BookDistribution") {{'active  selected'}} @endif">
                                <a href="javascript:;" class="text-uppercase" style="font-size: 22px !important;"> <i class="fa fa-cogs"></i> বই </a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="/Book" style="font-size: 20px !important;"><i class="fa fa-users"></i> বই </a></li>
                                    <li><a href="/BookDistribution" style="font-size: 20px !important;"><i class="fa fa-clone"></i> বই বিতরণ </a></li>
                                    <li><a href="/BookRegister" style="font-size: 20px !important;"><i class="fa fa-clone"></i> বই রেজিস্টার </a></li>
                                </ul>
                            </li>

                            <li class="dropdown more-dropdown">
                                <a href="javascript:;" class="text-uppercase" style="font-size: 22px !important;"> <i class="fa fa-cogs"></i> সেটিংস </a>
                                <ul class="dropdown-menu">
                                    <li><a href="/users" style="font-size: 20px !important;"><i class="fa fa-users"></i> ব্যবহারকারী </a></li>
                                    <li><a href="/employee" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> কর্মকর্তা / কর্মচারী </a></li>
                                    <li><a href="/designation" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> পদবী </a></li>
                                    <li><a href="#" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> অনুমতি </a></li>
                                    <li><a href="/office" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> দপ্তর </a></li>
                                    <li><a href="/pourosova" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> পৌরসভা </a></li>
                                    <li><a href="/ForeignTraining" style="font-size: 20px !important;"><i class="fa fa-sitemap"></i> বৈদেশিক ভ্রমণ </a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <!-- END HEADER MENU -->
                </div>
                <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
        <div class="container-fluid">
            <div class="page-content">
                <!-- BEGIN PAGE BASE CONTENT -->
                @yield('content')
                <div class="clearfix"></div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- BEGIN FOOTER -->
            <p class="copyright">2019-2023 © ICT Cell @ Ministry of Liberation War Affairs.</p>
            <a href="#index" class="go2top">
                <i class="icon-arrow-up"></i>
            </a>
            <!-- END FOOTER -->
        </div>
    </div>
    <!-- END CONTAINER -->

    <!-- SEND SMS MODAL START HERE -->
    <div class="modal fade" id="send_sms_modal" tabindex="-1" role="dialog" aria-labelledby="sendSMSModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">বার্তা প্রেরণ করুন</h4>
                </div>
                <div class="modal-body" id="send_sms_details">  </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
    <!-- SEND SMS MODAL END HERE -->


    <!--[if lt IE 9]>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/respond.min.js"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>

    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/moment.min.js" type="text/javascript"></script>
    {{--<script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>--}}
    {{--<script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>--}}
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
    {{--<script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-switch/src/js/bootstrap-switch.js" type="text/javascript"></script>--}}
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/select2/js/select2.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL PLUGINS -->

    <script src="{{Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.repeater/src/lib.js" type="text/javascript"></script>
    <script src="{{Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
    <script src="{{Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery.repeater/src/repeater.js" type="text/javascript"></script>

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/scripts/app.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/scripts/ui-general.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/scripts/form-input-mask.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>

    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/scripts/app.min.js" type="text/javascript"></script>

    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/apps/scripts/calendar.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/pages/scripts/components-multi-select.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/pages/scripts/dashboard.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/layouts/layout5/scripts/layout.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

    <!-- END THEME LAYOUT SCRIPTS -->
<script>
    jQuery(document).ready(function($) {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        //UIGeneral.init();
        QuickSidebar.init(); // init quick sidebar

    });
</script>

    <!-- beging:: page plugins  js -->
    @yield("page_plugins")
    <!-- end:: page plugins js -->

    <!-- beging:: page js -->
    @yield("page_js")
    <!-- end:: page js -->


</body>
</html>