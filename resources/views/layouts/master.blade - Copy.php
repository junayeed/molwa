<!DOCTYPE html>
<html lang="en" class="ie8 no-js">
<html lang="en" class="ie9 no-js">
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>GMIS - Gender Information Management System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Preview page of Metronic Admin Theme #2 for managed datatable samples" name="description"/>
    <meta content="" name="author"/>
    <link href="{{asset('css/fonts-plugins-theme-layout.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/icheck/all.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/fileinput.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="{{asset('css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="{{asset('css/plugins.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/blue.min.css')}}" rel="stylesheet" type="text/css" id="style_color"/>

    <link rel="{{asset('shortcut icon')}}" href="{{asset('images/logo/favicon.png')}}"/>
    <script src="{{asset('js/core.js')}}" type="text/javascript"></script>


    <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('js/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/tableExport.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/plugins.js')}}" type="text/javascript"></script>
    <link href="{{asset('js/select2/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('js/select2/select2.min.js')}}"></script>
    <link href="{{asset('css/cubeportfolio.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/css/portfolio.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/daterangepicker.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/bootstrap-multiselect.css')}}" rel="stylesheet"/>
    <!-- <link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet"/> -->

    <!-- Bootstrap Switch-->
    <link href="{{asset('js/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('js/bootstrap-switch/js/bootstrap-switch.js')}}" type="text/javascript"></script>

    <!--Bootbox-->
    <script src="{{asset('js/bootbox/bootbox.min.js')}}" type="text/javascript"></script>

    <!--data table-->
   
   <!--  <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css
    " rel="stylesheet"/>
    <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css
" rel="stylesheet"/>
 -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <?php
    $route_full_name = \Request::route()->getName();
    $route_array = explode('.', $route_full_name);
    $route_name = $route_array[0];
    //$route_name = Route::currentRouteName();
    ?>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="/">
                <img src="{{asset('images/logo/logo.png')}}" alt="logo" class="img-responsive logo-default"/></a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>
        <div class="page-actions">
            <span class="cgrpms">Gender Information Management System</span>
        </div>
        <div class="page-top">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <?php
                            $profile_picture = '/profile_image/default.jpg';
                            if (Auth::user()->getProfileBasicInofRow)
                                $profile_picture = '/profile_image/' . Auth::user()->getProfileBasicInofRow->image; ?>
                            <img alt="" class="img-circle" src="{{asset($profile_picture)}}"/>

                            <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span> 

                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Log Out </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="page-container">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <ul class="page-sidebar-menu page-sidebar-menu-light page-header-fixed" data-keep-expanded="false"
                data-auto-scroll="true" data-slide-speed="200">

                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="title">Configurations</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="/users" class="nav-link"> Users</a>
                        </li>
                        <li class="nav-item">
                            <a href="/MainArea" class="nav-link"> Main Areas</a>
                        </li>
                        <li class="nav-item">
                            <a href="/Indicator" class="nav-link"> Indicators</a>
                        </li>
                        <li class="nav-item">
                            <a href="/SubIndicator" class="nav-link"> Sub Indicators</a>
                        </li>
                        <li class="nav-item">
                            <a href="/Application" class="nav-link"> Application</a>
                        </li>
                    </ul>

                </li>
            </ul>
        </div>
    </div>
    @yield('content')


</div>
<div class="page-footer">
    <div class="page-footer-inner">
        &copy; 2017 <a target="_blank" href="http://coastguard.gov.bd/new/">Coast Guard, Bangladesh</a>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <div class="pull-right owner">
        All Right Reserved By <a href="" target="_blank">Company Name <img
                    src="" alt="" height="12px"></a>
    </div>
</div>

<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="/pcsEntry/create" class="active">
                <span>Add Pcs</span>
                <i class="icon-clock"></i>
            </a>
        </li>
        <li>
            <a href="/reminder" class="active">
                <span>Reminder</span>
                <i class="icon-clock"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>


<script src="{{asset('js/theme-layout.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-multiselect.js')}}" type="text/javascript"></script>
<script src="{{asset('js/page-level.js')}}" type="text/javascript"></script>
<!-- <script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script> -->
<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
<!-- <script src="{{asset('old/js/custom.js')}}"></script> -->
<script src="{{asset('js/bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/components-bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/daterangepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/portfolio-2.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    function menuHighlighter() {
        var linkName = "<?php echo $route_name?>";
        var linkObjList = document.getElementsByClassName("page-sidebar-menu")[0].getElementsByTagName("a");
        var requiredObj;
        var linkParent;

        if (linkName !== "") {
            linkName = "/" + linkName;
            // searching for current link
            for (var i = 0; i < linkObjList.length; i++) {
                if (linkObjList[i].getAttribute("href") === linkName) {
                    requiredObj = linkObjList[i];
                    break;
                }
            }
            
            // will not execute the code block below if no link found for the page
            if (requiredObj) {
                // Highlighting current menus
                var flag = 1;
                do {
                    requiredObj = requiredObj.parentNode;
                    if (requiredObj.nodeName.toLowerCase() === "li") {
                        requiredObj.className += " active open";
                    }
                    else if (requiredObj.getAttribute("class") !== "sub-menu") {
                        flag = 0;
                    }
                }
                while (flag !== 0);
            }
        }
    }
    menuHighlighter();
</script>


</body>
</html>