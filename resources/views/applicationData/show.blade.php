@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/ApplicationData">Application Data</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Add New Application Data</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Add New Application Data
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="" method="POST" action="/ApplicationData">
                {{csrf_field()}}

                <input name="application_id" id="application_id" value="{{$application->id}}" type="text" class="hidden">


                <div class="row" id="sub_indicator_data_table">
                    <div class="col-md-12">
                        <div class="portlet box light">
                            <div class="portlet-title">
                                <div class="actions">
                                    @if(Auth::user()->user_level==4 && Auth::user()->user_type==0 && $application->data_status == 0)
                                        <a id="sample_editable_1_new" class="btn green btn-add-new" href="#submit_modal" data-toggle="modal"> Send To Reviewer <i class="fa fa-arrow-right"></i></a>
                                    @endif
                                </div>
                            </div>
                            <ul class="nav nav-tabs">
                                <?php $first=true; ?>
                                @foreach($main_area_list as $rowdata)
                                    @if($first)
                                        <li class="active">
                                        <?php $first=false; ?>
                                    @else
                                        <li>
                                            @endif
                                            <a href="#{{$rowdata->id}}" data-toggle="tab"> {{$rowdata->mainarea_en}} </a>
                                        </li>
                                        @endforeach
                            </ul>

                            <div class="tab-content">
                                <?php $first=true;?>
                                @foreach($main_area_list as $rowdata)
                                    @if($first)
                                        <div class="tab-pane active row" id="{{$rowdata->id}}">
                                            @else
                                                <div class="tab-pane row" id="{{$rowdata->id}}">
                                                    @endif

                                                    <div class="form-group col-md-12" id="">
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                                            @foreach($rowdata->getIndicatorInfoRow as $indicator)


                                                                <div class="panel panel-default green-dark">
                                                                    <div class="panel-heading" role="tab" id="indicator_{{$indicator->id}}">
                                                                        <h4 class="panel-title">
                                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$indicator->id}}" aria-expanded="true" aria-controls="collapse_{{$indicator->id}}">
                                                                                <i class="more-less glyphicon glyphicon-plus"></i>
                                                                                {{$indicator->indicator_title}}
                                                                            </a>
                                                                        </h4>
                                                                    </div>



                                                                    <div id="collapse_{{$indicator->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="indicator_{{$indicator->id}}">
                                                                        <div class="panel-body">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th class="table_heading">Sl</th>
                                                                                    <th class="table_heading">Sub-Indicator</th>
                                                                                    <th class="table_heading">Amount in Indicator/Sub-Indicator</th>
                                                                                    <th class="table_heading">Female</th>
                                                                                    <th class="table_heading">Male</th>
                                                                                    <th class="table_heading">Total</th>
                                                                                    <th class="table_heading">Female(%)</th>
                                                                                    <th class="table_heading">Remarks</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody class="sub_indicator_data_table" id="">
                                                                                <?php $subList =  \App\Http\Controllers\ApplicationDataController::getSubIndicatorList($indicator->id);?>
                                                                                @if(!empty($subList))
                                                                                    @foreach($subList AS $key=> $list)
                                                                                        <tr>
                                                                                            <td>{{$key+1}}</td>
                                                                                            <td>{{$list->sub_indicator_title_en}}</td>
                                                                                            <td><input type="text" class="form-control" id="amount_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'amount')['amount']}}" placeholder=""></td>
                                                                                            <td><input type="text" class="form-control" id="woman_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'woman')['woman']}}" placeholder=""></td>
                                                                                            <td><input type="text" class="form-control" id="man_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'man')['man']}}" placeholder=""></td>
                                                                                            <td><input type="text" class="form-control" id="total_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'total')['total']}}" placeholder="" readonly></td>
                                                                                            <td><input type="text" class="form-control" id="percent_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'percent')['percent']}}" placeholder="" readonly></td>
                                                                                            <td><input type="text" class="form-control" id="remarks_{{$rowdata->id}}_{{$indicator->id}}_{{$list->id}}" value="{{$indicator->getApplicationData($application->id, $rowdata->id, $indicator->id, $list->id, 'remarks')['remarks']}}" placeholder=""></td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                @endif
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>




                                                                </div>
                                                            @endforeach

                                                        </div><!-- panel-group -->

                                                    </div>

                                                </div>
                                                <?php $first=false;?>
                                                @endforeach
                                        </div>
                            </div>
                        </div>
                    </div>


                    <div class="row ">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-body text-center">
                                    <!-- <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button> -->
                                    <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
                        </div>
                    </div>
            </form>
            <!-- Cancel Modal -->
            <div id="cancel_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="/Application" class="btn btn-modal-cancel">Cancel Anyway</a>
                            <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Submit to Reviewer Modal -->
            <div id="submit_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to Send this Application to Reviewer?</p>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="" method="POST" action="/submitToReviewer">
                                {{csrf_field()}}

                                <input name="application_id" id="" value="{{$application->id}}" type="text" class="hidden">


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet box light">
                                            <div class="form-group col-md-12">
                                                <label>Remarks</label>
                                                <textarea class="form-control" name="remarks" rows="3" id="remarks" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row ">
                                    <div class="col-md-12">
                                        <div class="portlet light">
                                            <div class="portlet-body text-center">
                                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> Send </button>
                                                <button type="button" class="btn btn-cancel" data-dismiss="modal"> Close </button>
                                            </div>
                                        </div>
                                        <!-- END SAMPLE FORM PORTLET-->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        var tbody = document.getElementsById("sub_indicator_data_table_1");

        tbody.addEventListener("input", function(e) {
            if (e.target && e.target.nodeName == "INPUT") {
                var tdIndex = Array.prototype.indexOf.call(e.target.parentNode.parentNode.children, e.target.parentNode);
                var trElem = e.target.parentNode.parentNode;

                //Required inputs
                var input1 = trElem.getElementsByTagName("td")[3].getElementsByTagName("input")[0];
                var input2 = trElem.getElementsByTagName("td")[4].getElementsByTagName("input")[0];
                var input3 = trElem.getElementsByTagName("td")[5].getElementsByTagName("input")[0];
                var input4 = trElem.getElementsByTagName("td")[6].getElementsByTagName("input")[0];
                if(input1.value != '' || input2.value != '')
                {
                    input3.value = Number(input1.value) + Number(input2.value);
                    input4.value = (Number(input1.value)*100/(Number(input1.value) + Number(input2.value))).toFixed(2);
                }


            }
        });
    </script>

    <script type="text/javascript">
        function getSubIndicatorInfo(id,main_area_id) {
            // $('#showloging').show();
            var value = id;
            var application_id = document.getElementById('application_id').value;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getSubIndicatorInfo",
                data: {value: value, application_id: application_id},
                success: function (result) {
                    // alert(result);
                    if (result != '') {
                        $('#sub_indicator_data_table_'+main_area_id).html(result);
                        // $('#showloging').hide();
                    } else {
                        $('#sub_indicator_data_table_'+main_area_id).html('');
                        alert('No Sub-Indicator Found');
                    }

                }
            }, "json");

        }
    </script>

    <script type="text/javascript">
        document.getElementById("sub_indicator_data_table").addEventListener("input", myFunction);

        function myFunction() {
            var field_id = document.activeElement.id;
            var field_value = document.getElementById(field_id).value;
            var application_id = document.getElementById('application_id').value;
            var total='';
            var percent='';
            var field_id_total='';
            var field_id_percent='';


            if ( field_id.indexOf('woman') > -1 ) {
                var field_id_woman = document.activeElement.id;
                var field_id_man = field_id_woman.replace("woman", "man");
                var field_id_total = field_id_woman.replace("woman", "total");
                var field_id_percent = field_id_woman.replace("woman", "percent");

                var woman = document.getElementById(field_id_woman).value;
                var man = document.getElementById(field_id_man).value;
                var total = Number(woman) + Number(man);
                var percent = (Number(woman)*100/(Number(woman) + Number(man))).toFixed(2);

                if(woman == '' && man == '')
                {
                    document.getElementById(field_id_percent).value = '';
                    document.getElementById(field_id_total).value = '';
                }
                else
                {
                    document.getElementById(field_id_percent).value = percent;
                    document.getElementById(field_id_total).value = total;
                }
            }

            else if ( field_id.indexOf('man') > -1 ) {
                var field_id_man = document.activeElement.id;
                var field_id_woman = field_id_man.replace("man", "woman");
                var field_id_total = field_id_man.replace("man", "total");
                var field_id_percent = field_id_man.replace("man", "percent");

                var woman = document.getElementById(field_id_woman).value;
                var man = document.getElementById(field_id_man).value;
                var total = Number(woman) + Number(man);
                var percent = (Number(woman)*100/(Number(woman) + Number(man))).toFixed(2);

                if(woman == '' && man == '')
                {
                    document.getElementById(field_id_percent).value = '';
                    document.getElementById(field_id_total).value = '';
                }
                else
                {
                    document.getElementById(field_id_percent).value = percent;
                    document.getElementById(field_id_total).value = total;
                }
            }


            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/submitApplicationData",
                data: {field_id: field_id, field_value: field_value, application_id: application_id, total: total, percent: percent, field_id_total: field_id_total, field_id_percent: field_id_percent},
                success: function (result) {
                    // alert(result);
                }
            }, "json");
        }
    </script>
@endsection