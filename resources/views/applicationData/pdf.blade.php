
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/home">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/ApplicationData/{{$application->id}}">Application</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Details</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Application: {{$application->name}}
                </h1>
            </div>
            @include('layouts.messages')
            <input name="application_id" id="application_id" value="{{$application->id}}" type="text" class="hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box light">
                        <ul class="nav nav-tabs">
                            <?php $first=true; ?>
                            @foreach($main_area_list as $rowdata)
                            @if($first)
                             <li class="active">
                             <?php $first=false; ?>
                             @else
                             <li>
                             @endif
                                <a href="#{{$rowdata->id}}" data-toggle="tab"> {{$rowdata->mainarea_en}} </a>
                            </li>       
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            <?php $first=true;?>
                            @foreach($main_area_list as $rowdata)
                            @if($first)
                            <div class="tab-pane active row" id="{{$rowdata->id}}">
                            @else
                            <div class="tab-pane row" id="{{$rowdata->id}}">
                            @endif
                                <div class="form-group col-md-6 col-md-offset-3">
                                    <label>Indicator</label>
                                    <select class="form-control" name="indicator_id" id="indicator_id" onchange="getSubIndicatorShowInfo(this.value);">
                                        <option value="">Please Select</option>
                                        @foreach($rowdata->getIndicatorInfoRow as $indicator)
                                        <option value="{{$indicator->id}}">{{$indicator->indicator_title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12" id="">
                                    <?php $first = 1; ?>
                                    @foreach($rowdata->getIndicatorInfoRow as $indicator)
                                    @if($first == 1)
                                    <?php $first = 0; ?>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                                        <thead>
                                            <tr>
                                                <th class="table_heading">Sl</th>
                                                <th class="table_heading">Sub-Indicator</th>
                                                <th class="table_heading">Amount in Indicator/Sub-Indicator</th>
                                                <th class="table_heading">Female</th>
                                                <th class="table_heading">Male</th>
                                                <th class="table_heading">Total</th>
                                                <th class="table_heading">Female(%)</th>
                                                <th class="table_heading">Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody id="sub_indicator_data_table">
                                        
                                        </tbody>
                                    </table>
                                    @endif
                                    @endforeach
                                </div>
                            
                            </div>
                            <?php $first=false;?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body text-center">
                            <a href="/Application" class="btn btn-circle btn-sm btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

