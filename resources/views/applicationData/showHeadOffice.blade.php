@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/ApplicationData">Application Data</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Details Application Data</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Details Application Data
                </h1>
            </div>
            @include('layouts.messages')
            <input name="application_id" id="application_id" value="{{$application->id}}" type="text" class="hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box light">
                    <div class="portlet-title">
                        <div class="actions">
                            @if($application->data_status == 3)
                            <a id="sample_editable_1_new" class="btn red btn-add-new" href="#back_to_reviewer_modal" data-toggle="modal"> Back To Upazila Reviewer <i class="fa fa-arrow-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                        <thead>
                            <tr>
                                <th class="table_heading">Main Area</th>
                                <th class="table_heading">Indicator</th>
                                <th class="table_heading">Sub-Indicator</th>
                                <th class="table_heading">Amount in Indicator/Sub-Indicator</th>
                                <th class="table_heading">Female</th>
                                <th class="table_heading">Male</th>
                                <th class="table_heading">Total</th>
                                <th class="table_heading">Female(%)</th>
                                <th class="table_heading">Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($main_area_list as $main_area)
                            <tr>
                                <td rowspan="{{$main_area->getSubIndicatorCountforMainArea($main_area->id)}}">{{$main_area->mainarea_en}}</td>
                                <?php $firstIndicator = 1; ?>
                                @foreach($main_area->getIndicatorInfoRow as $indicator)
                                    @if($firstIndicator == 1)
                                        <td rowspan="{{$indicator->getSubIndicatorCount($indicator->id)}}">{{$indicator->indicator_title}}</td>
                                        <?php $firstIndicator = 0; $firstSubIndicator = 1; ?>
                                        @foreach($indicator->getSubIndicatorInfoRow as $subIndicator)
                                            @if($firstSubIndicator == 1)
                                                <td>{{$subIndicator->sub_indicator_title_en}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['amount_in_indicator']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['woman']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['man']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['total']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['percent']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['remarks']}}</td>
                                            <?php $firstSubIndicator = 0; ?>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </tr>
                                <?php $firstIndicator = 1; ?>
                                @foreach($main_area->getIndicatorInfoRow as $indicator)
                                    @if($firstIndicator == 1)
                                        <?php $firstSubIndicator = 1; ?>
                                        @foreach($indicator->getSubIndicatorInfoRow as $subIndicator)
                                            @if($firstSubIndicator != 1)
                                                <tr>
                                                <td>{{$subIndicator->sub_indicator_title_en}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['amount_in_indicator']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['woman']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['man']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['total']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['percent']}}</td>
                                                <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['remarks']}}</td>
                                                </tr>
                                            @endif
                                            <?php $firstSubIndicator = 0; ?>
                                        @endforeach
                                    @endif
                                    <?php $firstIndicator = 0; ?>
                                @endforeach
                                
                                <?php $firstIndicator = 1; ?>
                                @foreach($main_area->getIndicatorInfoRow as $indicator)
                                    @if($firstIndicator != 1)
                                        <?php $firstSubIndicator = 1; ?>
                                        @foreach($indicator->getSubIndicatorInfoRow as $subIndicator)
                                            <tr>
                                            @if($firstSubIndicator == 1)
                                            <td rowspan="{{$indicator->getSubIndicatorCount($indicator->id)}}">{{$indicator->indicator_title}}</td>
                                            @endif
                                            <td>{{$subIndicator->sub_indicator_title_en}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['amount_in_indicator']}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['woman']}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['man']}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['total']}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['percent']}}</td>
                                            <td>{{$subIndicator->getApplicationData($application->id, $subIndicator->id)['remarks']}}</td>
                                            </tr>
                                            <?php $firstSubIndicator = 0; ?>
                                        @endforeach
                                    @endif
                                    <?php $firstIndicator = 0;?>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body text-center">
                            <a href="/Application" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- Back To Reviewer Modal -->
            <div id="back_to_reviewer_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to Send this Application to Center?</p>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="" method="POST" action="/backToPreviousUser">
                                {{csrf_field()}}

                                <input name="application_id" id="" value="{{$application->id}}" type="text" class="hidden">


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet box light">
                                            <div class="form-group col-md-12">
                                                <label>Remarks</label>
                                                <textarea class="form-control" name="remarks" rows="3" id="remarks" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row ">
                                    <div class="col-md-12">
                                        <div class="portlet light">
                                            <div class="portlet-body text-center">
                                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> Send </button>
                                                <button type="button" class="btn btn-cancel" data-dismiss="modal"> Close </button>
                                            </div>
                                        </div>
                                        <!-- END SAMPLE FORM PORTLET-->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        function getSubIndicatorShowInfo(id) {
            // $('#showloging').show();
            var value = id;
            var application_id = document.getElementById('application_id').value;
            // alert(application_id); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getSubIndicatorShowInfo",
                data: {value: value, application_id: application_id},
                success: function (result) {
                    // alert(result);
                    if (result != '') {
                        $('#sub_indicator_data_table').html(result);
                        // $('#showloging').hide();
                    } else {
                        $('#sub_indicator_data_table').html('');
                        alert('No Sub-Indicator Found');
                    }

                }
            }, "json");

        }


    </script>
@endsection