@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">

                            <div class="actions">
                                <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/users/create"> Add New User <i class="fa fa-plus"></i></a>
                                <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/createUNO"> Create UNO <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                                <thead>
                                    <tr>
                                        <th class="table_heading">Sl</th>
                                        <th class="table_heading">Name</th>
                                        <th class="table_heading">Email</th>
                                        <th class="table_heading">User Group</th>
                                        <th class="table_heading text-center no-print">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($user_list as $rowdata)
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td>{{$rowdata->name}}</td>
                                        <td>{{$rowdata->email}}</td>
                                        @if($rowdata->user_level != '')
                                        <td>{{$rowdata->getUserGroupInfoRow->name}}</td>
                                        @else
                                        <td></td>
                                        @endif
                                        {{--@if($rowdata->district != '')
                                        <td>{{$rowdata->getDistrictInfoRow->name}}</td>
                                        @else
                                        <td></td>
                                        @endif--}}
                                        <td>{{$rowdata->district_name}}</td>
                                        {{--@if($rowdata->upazila != '')
                                        <td>{{$rowdata->getUpazilaInfoRow->name}}</td>
                                        @else
                                        <td></td>
                                        @endif--}}
                                        <td>{{$rowdata->upazila_name}}</td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-circle btn-warning btn-sm " href="/users/{{$rowdata->id}}/edit"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        /*$(document).ready(function() {
            $('#table_list_view').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'pdf', 'print'
                ]
            } );
        } );*/
    </script>

     <script type="text/javascript">
        /*$(document).ready(function () {
            var t = $('#table_list_view').DataTable({});
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                                .search( this.value )
                                .draw();
                    }
                } );
            } );
        } );*/
    </script>

@endsection