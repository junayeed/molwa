@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <a href="/users">User</a>
                        <i class="fa fa-angle-right"></i>
                    </li>                   
                    <li>
                        <span>Add New User</span>
                    </li>
                </ul>
                <h1 class="page-title bold">Add New User
                </h1>
            </div>
            @include('layouts.messages')
            <form role="form" id="role_create_form" method="POST" action="/users/{{$user->id}}">
                {{csrf_field()}}
                {{method_field('PUT')}}

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body row">
                                <div class="form-group col-md-3 col-md-offset-3">
                                    <label>User Name*</label>
                                    <input name="name" value="{{$user->name}}" type="text" class="form-control calc-maxlength" maxlength="" required>
                                </div>  
                                <div class="form-group col-md-3">
                                    <label>Email*</label>
                                    <input name="email" value="{{$user->email}}" type="text" class="form-control calc-maxlength" maxlength="255" required>
                                </div>
                                <div class="form-group col-md-3 col-md-offset-3">
                                    <label>User Level*</label>
                                    <select name="user_level" class="form-control" required>
                                        <option value="">Please Select</option>
                                        @foreach($user_group as $rowdata)
                                        <option value="{{$rowdata->id}}" <?php if($rowdata->id == $user->user_level) echo "selected"; ?> >{{$rowdata->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>District*</label>
                                    <select name="district_id" class="form-control" onchange="getUpazilaInfoList(this.value)" required>
                                        <option value="">Please Select</option>
                                        @foreach($district_list as $rowdata)
                                        <option value="{{$rowdata->id}}" <?php if($rowdata->id == $user->district_id) echo "selected"; ?> >{{$rowdata->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-md-offset-3">
                                    <label>Upazila*</label>
                                    <select name="upazila_id" id="upazila" class="form-control" required>
                                        <option value="">Please Select</option>
                                        @foreach($upazila_list as $rowdata)
                                        <option value="{{$rowdata->id}}" <?php if($rowdata->id == $user->upazila_id) echo "selected"; ?> >{{$rowdata->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                

                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body text-center">
                                <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> Save &amp; Update</button>
                                <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>
                                <!-- </div> -->
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
                </div>
            </form>
            <!-- Cancel Modal -->
            <div id="cancel_modal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="/users" class="btn btn-modal-cancel">Cancel Anyway</a>
                            <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        function getUpazilaInfoList(id) {
            // $('#showloging').show();
            var value = id;
            // alert(value); die();
            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getUpazilaInfoList",
                data: {value: value},
                success: function (result) {
                    // alert(result);
                    if (result != '') {
                        $('#upazila').html(result);
                        // $('#showloging').hide();
                    } else {
                        $('#upazila').html('No Recipient Found');
                    }

                }
            }, "json");

        }
    </script>
@endsection