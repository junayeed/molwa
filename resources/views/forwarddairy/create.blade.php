@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="case_details_form" method="POST" action="/forwardDairy">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">

                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="font-size: 20px; padding: 5px 20px !important;"> ACTIONS
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="/forwardDairySearch" style="font-size: 16px;"><i class="fa fa-search "></i> Search Case </a></li>
                                    <li class="divider"> </li>
                                    <li><a href="/forwardDairy" style="font-size: 16px;"><i class="fa fa-backward "></i> Back to Case List </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- CASE DETAILS (LFET COL) START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Case Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>eNothi File No</label>
                                        <div class="input-icon">
                                            <i class="" style="margin: 6px 2px 4px 10px !important; font-size: 15px; font-style: normal !important; color: #000 !important;">
                                                48.00.0000.007.
                                            </i>
                                            <input name="file_no" id="file_no" type="text" class="form-control input"  style="padding-left: 125px;" >
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Hard File No</label>
                                        <div class="input-icon">
                                            <i class="" style="margin: 6px 2px 4px 10px !important; font-size: 15px; font-style: normal !important; color: #000 !important;">
                                                48.00.0000.004.
                                            </i>
                                            <input name="hard_file_no" id="hard_file_no" type="text" class="form-control input" style="padding-left: 125px;" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Case Type</label>
                                        <select name="case_type" id="case_type" class="form-control input" >
                                            <option value="">Select Case Type</option>
                                            @foreach($case_type_list as $id => $case_type)
                                                <option value="{{$id}}">{{$case_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Case No</label>
                                        <input name="case_no" id="case_no" type="text" class="form-control input" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Order Date of Court</label>
                                        <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                            <input type="text" class="form-control input" name="writ_petition_order_date" id="writ_petition_order_date" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Case Receive Date</label>
                                        <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                            <input type="text" class="form-control input" name="case_receive_date" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Number of Pages</label>
                                        <input name="page_no" id="page_no" type="text" class="form-control input" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Appendix List</label>
                                        <input name="appendix_list" id="appendix_list" type="text" class="form-control input" >
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Petitioner/ Parties Name</label>
                                        <input name="parties_name" id="parties_name" type="text" class="form-control input" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Subject Type</label>
                                        <select name="subject_type[]" multiple="multiple" id="subject_type" class="form-control input" >
                                            <option value="">Select Case Type</option>
                                            @foreach($case_subject_type_list as $id => $subject_type)
                                                <option value="{{$id}}">{{$subject_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label>Subject</label>
                                        <textarea name="case_subject" id="case_subject" class="form-control input" ></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Case Status</label>
                                        <select name="case_status" id="case_status" class="form-control input" >
                                            <option value="">Select Status</option>
                                            @foreach($status_list as $id => $status)
                                                <option value="{{$id}}">{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label>Has Ruli Nisi?</label><br>
                                        <input type="checkbox" name="has_rule_nisi" id="has_rule_nisi" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Has Interim Order?</label><br>
                                        <input type="checkbox" name="has_interim_order" id="has_interim_order" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Has Supplementary Order?</label><br>
                                        <input type="checkbox" name="has_supplementary_order" id="has_supplementary_order" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Court Contempted?</label><br>
                                        <input type="checkbox" name="has_court_contempted" id="has_court_contempted" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CASE DETAILS (LFET COL) END -->

            <!-- Court Condempted (RIGHT COL) START -->
            <div class="col-md-6" id="court_contempted_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Court Contempted
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Contempt No</label>
                                <input name="contempt_no" id="contempt_no" type="text" class="form-control input" >
                            </div>
                            <div class="form-group col-md-3">
                                <label>Received Date</label>
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="contempt_received_date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Appearance Date</label>
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="appearance_date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-9">
                                <label>Remarks</label>
                                <textarea name="contempt_Remarks" id="contempt_Remarks" class="form-control input" ></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Court Condempted (RIGHT COL) END -->

            <!-- Rule Nisi (RIGHT COL) START -->
            <div class="col-md-6" id="rule_nisi_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Rule Nisi
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Rule Nisi Order Date</label>
                                {{--<input id="rule_nisi_order_date" name="rule_nisi_order_date" class="form-control date-picker input" size="16" placeholder="dd-mm-yyyy" type="text" value=""  />--}}
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="rule_nisi_order_date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-9">
                                <label>Order Details</label>
                                <textarea name="rule_nisi_order_details" id="rule_nisi_order_details" class="form-control input" ></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Rule Nisi (RIGHT COL) END -->

            <!-- Interim Order (RIGHT COL) START -->
            <div class="col-md-6" id="interim_order_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Interim Order
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Interim Order Date</label>
                                {{--<input id="interim_order_date" name="interim_order_date" class="form-control date-picker input" size="16" placeholder="dd-mm-yyyy" type="text" value=""  />--}}
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="interim_order_date"  id="interim_order_date" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important; border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Deadline</label>
                                <input id="deadline" name="deadline" class="form-control input" type="text" value=""  />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Order Details</label>
                                <textarea name="order_details" id="order_details" class="form-control input" ></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-repeater" id="interim_order_repeater">
                                    <div data-repeater-list="interim_order_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-3" id="interim_order_div">
                                                    <label class="small ">Implementing Authority</label>
                                                    <select name="implementing_authority" id="implementing_authority" class="form-control input validateImpAuth imp_authority" >
                                                        <option value="">Select an Authority</option>
                                                        @foreach($imp_authority_list as $id => $imp_authority)
                                                            <option value="{{$id}}">{{$imp_authority}}</option>
                                                        @endforeach
                                                    </select>

                                                    <select name="ministry" id="ministry" class="form-control input margin-top-10 ">
                                                        <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                                        @foreach($ministry_list as $id => $ministry)
                                                            <option value="{{$id}}">{{$ministry}}</option>
                                                        @endforeach
                                                    </select>
                                                    <select name="dc_office" id="dc_office" class="form-control input margin-top-10 ">
                                                        <option value="">জেলা প্রশাসক কার্যালয় বাছাই করুন</option>
                                                        @foreach($district_list as $id => $district)
                                                            <option value="{{$id}}">{{$district}}</option>
                                                        @endforeach
                                                    </select>
                                                    <select name="uno_office" id="uno_office" class="form-control input margin-top-10 " >
                                                        <option value="">ইউ এন ও কার্যালয় বাছাই করুন</option>
                                                        @foreach($upzilla_list as $id => $upzilla)
                                                            <option value="{{$id}}">{{$upzilla}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-8">
                                                    <label class="control-label small">Corresponding Responsibility</label>
                                                    <textarea name="responsibility" id="responsibility" class="form-control input-sm validateImpAuth" ></textarea>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="row mt-repeater-row margin-top-10">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_issue_memo" name="int_issue_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_issue_memo}}" placeholder="Issue Memo no." />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_issue_date" name="int_issue_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_receive_memo" name="int_receive_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_receive_memo}}" placeholder="Receiving Memo no." />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_receive_date" name="int_receive_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_sending_memo" name="int_sending_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_sending_memo}}" placeholder="Sending Memo no." />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input id="int_sending_date" name="int_sending_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                        {{--<div class="form-group col-md-12">
                            <label>Status*</label><br>
                            <input type="checkbox" name="sub_indicator_status" id="sub_indicator_status" data-on-text="Active" data-off-text="Inactive" style="height: 30px;" checked>
                        </div>--}}
                    </div>
                </div>
            </div>
            <!-- Interim Order (RIGHT COL) END -->

            <!-- Supplementary Order (RIGHT COL) START -->
            <div class="col-md-6" id="supplementary_order_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Supplementary Order
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-repeater" id="supplementary_order_repeater">
                                    <div data-repeater-list="supplementary_order_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-3"  id="supplementary_order_details_div">
                                                    <label>Supplementary Order Date</label>
                                                    <input id="supplementary_order_date" name="supplementary_order_date" class="form-control date-picker input" size="16" readonly type="text" value="" readonly />
                                                </div>
                                                <div class="col-md-8">
                                                    <label>Order Details</label>
                                                    <textarea name="supplementary_order_details" id="supplementary_order_details" class="form-control input" ></textarea>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Supplementary Order (RIGHT COL) END -->
        </div>

        <!-- SF DETAILS START -->
        <div class="row" id="sf_details_div">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Statement of Facts (SF)
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12" id="sf_repeater">
                                <div class="form-group mt-repeater" id="sf_data_container">
                                    <div data-repeater-list="sf_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-2" id="sf_details_div">
                                                    <div class="form-group">
                                                        <label>Recipient</label>
                                                        <select name="sf_recipient" id="sf_recipient" class="form-control input" >
                                                            <option value="">Select a Recipient</option>
                                                            @foreach($recipient_list as $id => $recipient)
                                                                <option value="{{$id}}">{{$recipient}}</option>
                                                            @endforeach
                                                        </select>

                                                        <select name="sf_ministry" id="sf_ministry" class="form-control input margin-top-10 ">
                                                            <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                                            @foreach($ministry_list as $id => $ministry)
                                                                <option value="{{$id}}">{{$ministry}}</option>
                                                            @endforeach
                                                        </select>
                                                        <select name="sf_dc_office" id="sf_dc_office" class="form-control input margin-top-10 ">
                                                            <option value="">জেলা প্রশাসক কার্যালয় বাছাই করুন</option>
                                                            @foreach($district_list as $id => $district)
                                                                <option value="{{$id}}">{{$district}}</option>
                                                            @endforeach
                                                        </select>
                                                        <select name="sf_uno_office" id="sf_uno_office" class="form-control input margin-top-10 " >
                                                            <option value="">ইউ এন ও কার্যালয় বাছাই করুন</option>
                                                            @foreach($upzilla_list as $id => $upzilla)
                                                                <option value="{{$id}}">{{$upzilla}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Issue Memo No.</label>
                                                        <input id="sf_issue_memo" name="sf_issue_memo" class="form-control input" size="16" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>Date of Issue</label>
                                                        <input id="sf_issue_date" name="sf_issue_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Receiving Memo No.</label>
                                                        <input id="sf_receive_memo" name="sf_receive_memo" class="form-control input" size="16" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>SF Receiving Date</label>
                                                        <input id="sf_receive_date" name="sf_receive_date" class="form-control date-picker input validate_sf_receive_date" readonly placeholder="dd/mm/yyyy" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Sending Memo No.</label>
                                                        <input id="sf_sending_memo" name="sf_sending_memo" class="form-control input" size="16" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>SF Sending Date</label>
                                                        <input id="sf_sending_date" name="sf_sending_date" class="form-control date-picker input" placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs btn-circle green-meadow mt-repeater-add" id="sf_repeater_button"><i class="fa fa-plus"></i> Add New Recipient</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SF DETAILS END -->

        <!-- AOR STARTS HERE-->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> AOR Details
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="aor_name" name="aor_name" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Cell No.</label>
                                    <input id="aor_cell_no" name="aor_cell_no" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Memo No</label>
                                    <input id="aor_memo_no" name="aor_memo_no" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                        <input type="text" class="form-control input" name="aor_memo_date" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> বিদ্যমান
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- AOR ENDS HERE-->

        <!-- Recipient Ministry START -->
        <div class="row" id="recipient_ministry_div">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> Recipient Ministry
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Recipient</label>
                                    <select name="recipient_ministry" id="recipient_ministry" class="form-control input" >
                                        <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                        @foreach($ministry_list as $id => $ministry)
                                            <option value="{{$id}}">{{$ministry}}</option>
                                        @endforeach
                                        <option value="999">অন্যান্য</option>
                                    </select>
                                    <div id="recipient_others_div">
                                        <input id="recipient_others" name="recipient_others" class="form-control input margin-top-10 " size="16" type="text" value=""  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Memo No.</label>
                                    <input id="memo_no" name="memo_no" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                        <input type="text" class="form-control input" name="issue_date" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <input id="recipient_ministry_remarks" name="recipient_ministry_remarks" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Recipient Ministry END -->

        <!-- JUDGEMENT DETAILS START -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>Judgement Details <input type="checkbox" value="1" name="has_judgement" id="has_judgement">
                        </div>
                    </div>
                    <div class="portlet-body" id="judgement_details_div">
                        <div class="row">
                            <div class="col-md-12" id="hearing_repeater">
                                <div class="form-group mt-repeater" id="hearing_data_container">
                                    <div data-repeater-list="hearing_data">
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="row mt-repeater-row">
                                                <div class="col-md-2" id="hearing_details_div">
                                                    <div class="form-group">
                                                        <label>Hearing Date</label>
                                                        <input id="hearing_date" name="hearing_date" class="form-control date-picker input" size="16" placeholder="dd-mm-yyyy" type="text" value=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Order/ Judgement</label>
                                                        <textarea name="judgement" id="judgement" class="form-control input" ></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-sm-1">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs btn-circle green-meadow mt-repeater-add" id="hearing_repeater_button"><i class="fa fa-plus"></i> Add New Hearing Details</a>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>C.M.P. No.</label>
                                    <input id="cmp_no" name="cmp_no" class="form-control input" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>C.M.P. Order / Judgement</label>
                                    <textarea name="cmp_judgement" id="cmp_judgement" class="form-control input" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>C.P.L.A No</label>
                                    <input id="cp_no" name="cp_no" class="form-control input" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>C.P.L.A Order / Judgement</label>
                                    <textarea name="cp_judgement" id="cp_judgement" class="form-control input" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>Appeal Case No</label>
                                    <input id="appeal_case_no" name="appeal_case_no" class="form-control input" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>Appeal Order / Judgement</label>
                                    <textarea name="appeal_judgement" id="appeal_judgement" class="form-control input" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>Review Petition No</label>
                                    <input id="review_petition_no" name="review_petition_no" class="form-control input" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>Order / Judgement</label>
                                    <textarea id="review_order" name="review_order" class="form-control input"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Final Judgement</label>
                                    <textarea id="final_judgement" name="final_judgement" class="form-control input"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- JUDGEMENT DETAILS START -->

        <div class="row ">
            <div class="col-md-2">
                <button type="submit" class="btn btn-save btn-success font-hg col-md-12"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
            </div>
            <div class="col-md-2">
                <a href="/forwardDairy" class="btn btn-warning font-hg col-md-12"><i class="fa fa-ban"></i> তালিকায় ফেরত যান</a>
            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@section('page_plugins')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
@endsection

    <script type="text/javascript">
        var FormValidation = function () {
        var addUserForm = function() {
            var form = $('#case_details_form');
            var error = $('.alert-danger', form);

            $("#case_details_form").validate(
                {
                    errorElement: 'span', //---- default input error message container
                    errorClass: 'help-block help-block-error', //---- default input error message class
                    focusInvalid: false, //---- do not focus the last invalid input
                    ignore: "",  //---- validate all fields including form hidden input
                    rules:
                        {
                            'case_no'                 : "required",
                            'case_type'               : "required",
                            'file_no'                 : "required",
                            'hard_file_no'            : "required",
                            'case_receive_date'       : "required",
                            'page_no'                 : "required",
                            'appendix_list'           : "required",
                            'parties_name'            : "required",
                            'subject_type'            : "required",
                            'case_subject'            : "required",
                            'case_status'             : "required",
                            // if the has_interim_order is checked
                            'interim_order_date'      : { required: '#has_interim_order:checked' },
                            'deadline'                : { required: '#has_interim_order:checked' },
                            'order_details'           : { required: '#has_interim_order:checked' },
                            // if the has_rule_nisi is checked
                            'rule_nisi_order_date'    : { required: '#has_rule_nisi:checked'},
                            'rule_nisi_order_details' : { required: '#has_rule_nisi:checked'},
                            /*'sf_issue_memo'           : {
                                                            required:{ depend: function() {
                                                                return $('#case_status').val() === '2';
                                                            }}
                                                        }*/

                        },
                    messages: { },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        error.show();
                        Metronic.scrollTo(error, -200);
                        error.delay(6000).fadeOut(2000);
                    },
                    highlight: function (element)
                    { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element)
                    { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label)
                    {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    errorPlacement: function() {
                        return true;
                    }
                    /*submitHandler : function()
                    {
                        return false;
                    }*/
                });
        }
        return {
            //---- main function to initiate the module
            init: function () {
                addUserForm();
            }
        };
        }();

        $('#has_judgement').on('change', function(){
            if($('#has_judgement').is(':checked')) {
                $('#judgement_details_div').show();
            }
            else {
                $('#judgement_details_div').hide();
            }
        });

        $(document).on('change', '#implementing_authority', function() {
            if ($(this).val() == 6 ) { // show the district list for DC office
                $(this).parent().closest('#interim_order_div').find('#dc_office').show();
                $(this).parent().closest('#interim_order_div').find('#uno_office').hide();
                $(this).parent().closest('#interim_order_div').find('#ministry').hide();
            }
            else if ($(this).val() == 7) { // show Upzilla list for UNO office
                $(this).parent().closest('#interim_order_div').find('#uno_office').show();
                $(this).parent().closest('#interim_order_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_div').find('#ministry').hide();
            }
            else if ( $(this).val() == 3 ) { // show Ministry List
                $(this).parent().closest('#interim_order_div').find('#ministry').show();
                $(this).parent().closest('#interim_order_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_div').find('#uno_office').hide();
            }
            else { // hide Ministry, District and Upzilla list
                $(this).parent().closest('#interim_order_div').find('#ministry').hide();
                $(this).parent().closest('#interim_order_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_div').find('#uno_office').hide();
            }
        });

        $(document).on('change', '#sf_recipient', function() {
            if ($(this).val() == 6 ) { // show the district list for DC office
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').show();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
            }
            else if ($(this).val() == 7) { // show Upzilla list for UNO office
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').show();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
            }
            else if ( $(this).val() == 3 ) { // show Ministry List
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').show();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
            }
            else { // hide Ministry, District and Upzilla list
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
            }
        });

        $(document).on('change', '#recipient_ministry', function () {
            if ($(this).val() == 999) {
                $('#recipient_others_div').show();
            }
            else {
                $('#recipient_others_div').hide();
            }
        });

       /* $("#sf_repeater_button").click(function(){
            setTimeout(function(){

                $("#sf_uno_office").select2();

            }, 10);
        });*/

        $('.btn-save').on('click', function (){
            /*if ($('#case_status').val() == 3) // if the case status is SF Received
            {
                $("#sf_data_container .validate_sf_receive_date").each(function(){
                    $(this).rules("add", {required: true});
                });
            }*/

            // if has_interim_order is checked then all the dynamic fileds in Interim Order section is required
            if($('#has_interim_order').is(':checked')) {
                $('.validateImpAuth').each(function () {
                    $(this).rules("add", {required: true});
                });
            }
            //otherwise remove the rules
            else {
                $('.validateImpAuth').each(function () {
                    $(this).rules('remove', 'required');
                });
            }
        });

        $(function() {
            FormValidation.init();
            //$('#sf_uno_office').select2();

            $('#dc_office').hide();
            $('#uno_office').hide();
            $('#ministry').hide();
            $('#recipient_ministry_div').hide();
            $('#judgement_details_div').hide();
            $('#court_contempted_div').hide();
            $('#recipient_others_div').hide();
            $('#supplementary_order_div').hide();

            $('#sf_dc_office').hide();
            //$('#sf_uno_office').next(".select2-container").hide();
            $('#sf_uno_office').hide();
            $('#sf_ministry').hide();

            $('.date-picker').datepicker({
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "dd/mm/yyyy"
            });

            $('#subject_type').select2();

            $("#has_interim_order").bootstrapSwitch('state', true);
            $("#has_rule_nisi").bootstrapSwitch('state', true);
            $("#has_supplementary_order").bootstrapSwitch('state', false);
            $("#has_court_contempted").bootstrapSwitch('state', false);

            var interim_order_repeater = $("#interim_order_repeater").repeater({
                initEmpty:!1,
                show:function() {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) }
            });
            var supplementary_order_repeater = $("#supplementary_order_repeater").repeater({
                initEmpty:!1,
                show:function(){
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) }
            });

            var sf_repeater = $("#sf_repeater").repeater({
                initEmpty:!1,
                show:function() {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) },
                isFirstItemUndeletable: true
            });

            var hearing_repeater = $("#hearing_repeater").repeater({
                initEmpty:!0,
                show:function() {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) },
                isFirstItemUndeletable: false
            });

            $('#has_court_contempted').on('switchChange.bootstrapSwitch', function() {

                if ($("#has_court_contempted").bootstrapSwitch('state')) {
                    $('#court_contempted_div').show();
                }
                else {
                    $('#court_contempted_div').hide();
                }
            });

            $('#has_interim_order').on('switchChange.bootstrapSwitch', function() {

                if ($("#has_interim_order").bootstrapSwitch('state')) {
                    $('#interim_order_div').show();
                }
                else {
                    $('#interim_order_div').hide();
                }
            });

            $('#has_rule_nisi').on('switchChange.bootstrapSwitch', function() {

                if ($("#has_rule_nisi").bootstrapSwitch(('state'))) {
                    $('#rule_nisi_div').show();
                }
                else {
                    $('#rule_nisi_div').hide();
                }
            });

            $('#has_supplementary_order').on('switchChange.bootstrapSwitch', function() {
                if ($("#has_supplementary_order").bootstrapSwitch(('state'))) {
                    $('#supplementary_order_div').show();
                }
                else {
                    $('#supplementary_order_div').hide();
                }
            });

            $('#case_status').on('change', function(){
                if(this.value == 6) { // if the status Necessary action required that is nothing to deal with MOLWA, case send to concern Ministry
                    $('#sf_details_div').hide();
                    $('#recipient_ministry_div').show();
                }
                else {
                    $('#sf_details_div').show();
                    $('#recipient_ministry_div').hide();
                }
            });

            $('#case_no').on('change', function(){
                var case_no = $(this).val();

                if ( !case_no) {
                    return;
                }

                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/')}}/checkDuplicateCaseNo",
                    data: {case_no: case_no},
                    dataType: 'JSON',
                    complete: function (res) {
                        if (res.responseText == 'Duplicate') {
                            bootbox.alert("Case number already exists! Please check the case no. again.");
                        }
                    }
                });
            });
        });

    </script>
@endsection