@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT  -->
    @include('layouts.messages')
    <form role="form" id="case_search_form" method="POST" action="/getSearchResult">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="row">
            <!-- SEARCH START -->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Search
                                </div>
                                <div class="actions">
                                    <div class="btn-group">
                                        <a class="btn green btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="font-size: 20px; padding: 5px 20px !important;"> ACTIONS
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="forwardDairy/create" style="font-size: 16px;"><i class="fa fa-plus"></i> Add New Case </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="forwardDairy" style="font-size: 16px;"><i class="fa fa-search "></i> Back to Case List </a>
                                            </li>
                                            <li class="divider"> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label>File No</label>
                                        <input name="file_no" id="file_no" type="text" class="form-control input" value="@if( isset($searchData['file_no']) ){{$searchData['file_no']}} @endif">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Case Type</label>
                                        <select name="case_type" id="case_type" class="form-control input" >
                                        <option value="">Select Case Type</option>
                                            @foreach($case_type_list as $id => $case_type)
                                                <option value="{{$id}}" {{ isset($searchData['case_type']) &&  $searchData['case_type'] == $id ? 'selected="selected"' : '' }} >{{$case_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label>Case No</label>
                                        <input name="case_no" id="case_no" type="text" class="form-control input" value="@if( isset($searchData['case_no']) ){{$searchData['case_no']}} @endif" >
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label>Case Year</label>
                                        <select name="case_year" id="case_year" class="form-control input">
                                            <option value="">Select Year</option>
                                            @for($y=2000; $y< date('Y')+1; $y++)
                                                <option value="{{$y}}" @if(@$searchData['case_year'] == $y) selected @endif>{{$y}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label>Case Receive Date</label>
                                        <input id="case_receive_date" name="case_receive_date" class="form-control date-picker input" size="16" placeholder="dd/mm/yyyy"
                                               type="text" readonly
                                               value="@if( isset($searchData['case_receive_date']) ){{\Carbon\Carbon::parse($searchData['case_receive_date'])->format('d/m/Y')}} @endif"  />
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Case Status</label>
                                        <select name="case_status" id="case_status" class="form-control input" >
                                            <option value="">Select Status</option>
                                            @foreach($status_list as $id => $status)
                                                <option value="{{$id}}" {{ isset($searchData['case_status']) && $searchData['case_status'] == $id ? 'selected="selected"' : '' }}>{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Subject Type</label>
                                            <select name="subject_type" id="subject_type" class="form-control input" >
                                                <option value="">Select Case Type</option>
                                                @foreach($case_subject_type_list as $id => $subject_type)
                                                    <option value="{{$id}}" {{ isset($searchData['subject_type']) && $searchData['subject_type'] == $id ? 'selected="selected"' : ''}}>{{$subject_type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Petitioner/Parties Name</label>
                                        <input name="parties_name" id="parties_name" type="text" class="form-control input" value="@if(isset($searchData['parties_name']) ){{$searchData['parties_name']}} @endif">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Contempt No</label>
                                        <input name="contempt_no" id="contempt_no" type="text" class="form-control input" value="@if(isset($searchData['contempt_no']) ){{$searchData['contempt_no']}} @endif">
                                    </div>

                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label style="font-size: 13px;">Has Interim Order?</label><br>
                                                <input type="checkbox" name="has_interim_order" id="has_interim_order" data-on-text="Yes" data-off-text="No" style="height: 30px;"
                                                       {{ isset($searchData['has_interim_order']) && $searchData['has_interim_order'] == 'on' ? 'checked="checked"' : ''}}>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Has Ruli Nisi?</label><br>
                                                <input type="checkbox" name="has_rule_nisi" id="has_rule_nisi" data-on-text="Yes" data-off-text="No" style="height: 30px;"
                                                       {{ isset($searchData['has_rule_nisi']) && $searchData['has_rule_nisi'] == 'on' ? 'checked="checked"' : ''}}>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Court Contempted?</label><br>
                                                <input type="checkbox" name="has_court_contempted" id="has_court_contempted" data-on-text="Yes" data-off-text="No" style="height: 30px;"
                                                       {{ isset($searchData['has_court_contempted']) && $searchData['has_court_contempted'] == 'on' ? 'checked="checked"' : ''}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>SF Recipient</label>
                                            <select name="sf_recipient" id="sf_recipient" class="form-control input" >
                                                <option value="">Select a Recipient</option>
                                                @foreach($recipient_list as $id => $recipient)
                                                    <option value="{{$id}}" {{ isset($searchData['sf_recipient']) && $searchData['sf_recipient'] == $id ? 'selected="selected"' : '' }}>{{$recipient}}</option>
                                                @endforeach
                                            </select>

                                            <select name="sf_ministry" id="sf_ministry" class="form-control input-sm margin-top-10 ">
                                                <option value="">মন্ত্রণালয় কার্যালয় বাছাই করুন</option>
                                                @foreach($ministry_list as $id => $ministry)
                                                    <option value="{{$id}}">{{$ministry}}</option>
                                                @endforeach
                                            </select>
                                            <select name="sf_dc_office" id="sf_dc_office" class="form-control input-sm margin-top-10 ">
                                                <option value="">জেলা প্রশাসক কার্যালয় বাছাই করুন</option>
                                                @foreach($district_list as $id => $district)
                                                    <option value="{{$id}}">{{$district}}</option>
                                                @endforeach
                                            </select>
                                            <select name="sf_uno_office" id="sf_uno_office" class="form-control input-sm margin-top-10 " >
                                                <option value="">ইউ এন ও কার্যালয় বাছাই করুন</option>
                                                @foreach($upzilla_list as $id => $upzilla)
                                                    <option value="{{$id}}">{{$upzilla}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success "><i class="fa fa-search "></i> Search</button>
                                        {{--<a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SEARCH END -->
        </div>
        @if ( !empty($case_list) && isset($case_list))

        <div class="row">

                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-body">
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th class="table_heading">Serial</th>
                                {{--<th class="table_heading">File No</th>--}}
                                <th class="table_heading">Case No</th>
                                <th class="table_heading">Receive Date</th>
                                <th class="table_heading">Petitioner/Parties Name</th>
                                <th class="table_heading">Subject Type</th>
                                <th class="table_heading">Status</th>
                                <th class="table_heading">Rule Nisi</th>
                                <th class="table_heading">Interim Order</th>
                                <th class="table_heading">Recipient Name</th>
                                <th class="table_heading text-center no-print">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>

                            @foreach($case_list as $case)
                                {{ $class = '' }}
                                <tr class="odd gradeX">
                                    <td>{{$i++}}</td>
                                    {{--<td>{{substr_replace($case->file_no, '--', 2, 8)}}</td>--}}
                                    <td>{{$case->case_no}}
                                        @if ($case->case_type == 'WP')
                                            <label class="label bg-red-thunderbird bg-font-red-thunderbird">{{$case->case_type}}</label>
                                        @else
                                            <label class="label bg-purple-plum bg-font-purple-plum">{{$case->case_type}}</label>
                                        @endif
                                    </td>
                                    <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                                    <td>{!! $case->parties_name !!}</td>
                                    <td>{{$case->subject_type_bn}}</td>
                                    <td>
                                        @if ($case->case_status_id == 1)
                                            @php ($class = 'bg-red-flamingo')
                                        @elseif ($case->case_status_id == 2)
                                            @php ($class = 'bg-yellow-gold')
                                        @elseif ($case->case_status_id == 3)
                                            @php ($class = 'bg-yellow-soft')
                                        @elseif ($case->case_status_id == 4)
                                            @php ($class = 'bg-green-jungle')
                                        @elseif ($case->case_status_id == 5)
                                            @php ($class = 'bg-green-meadow')
                                        @else
                                            @php ($class = 'bg-grey-mint')
                                        @endif
                                        <label class="label {{$class}}">{{$case->case_status}}</label>
                                    </td>
                                    <td class="text-left">
                                        @if ($case->has_rule_nisi == 1)
                                            @php ($rule_nisi_class = 'bg-red-flamingo')
                                            @php ($rule_nisi_text = 'Y')
                                        @elseif ($case->has_rule_nisi == 0)
                                            @php ($rule_nisi_class = 'bg-green')
                                            @php ($rule_nisi_text = 'N')
                                        @endif
                                        <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                                    </td>
                                    <td class="">
                                        @if ($case->has_interim_order == 1)
                                            @php ($interim_order_class = 'bg-red-flamingo')
                                            @php ($interim_order_text = 'Y')
                                        @elseif ($case->has_interim_order == 0)
                                            @php ($interim_order_class = 'bg-green')
                                            @php ($interim_order_text = 'N')
                                        @endif
                                        <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                                    </td>
                                    <td>
                                        @if ($case->recipient_name)
                                            {{$case->recipient_name}}
                                            @if($case->sf_recipient == 6 || $case->sf_recipient == 7 || $case->sf_recipient == 8)
                                                (
                                                @if ($case->sf_recipient == 6) {{@$district_list[$case->sf_dc_office]}}
                                                @elseif ($case->sf_recipient == 7) {{@$upzilla_list[$case->sf_uno_office]}}
                                                @elseif ($case->sf_recipient == 8) {{@$ministry_list[$case->sf_ministry]}}

                                                @endif
                                                )
                                            @endif
                                        @else
                                            {{@$ministry_list[$case->recipient_ministry]}}
                                        @endif
                                    </td>
                                    <td class="text-center no-print">
                                        <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$case->id}}/edit"><i class="fa fa-edit"></i></a>
                                        {{--<a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$case->id}}"><i class="fa fa-trash"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

            @else No Data Found
        @endif
    </form>
@section('page_plugins')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
@endsection

    <script type="text/javascript">


        var FormValidation = function () {
        var addUserForm = function() {
            var form = $('#profileFrm');
            var error = $('.alert-danger', form);

            $("#case_details_form").validate(
                {
                    errorElement: 'span', //---- default input error message container
                    errorClass: 'help-block help-block-error', //---- default input error message class
                    focusInvalid: false, //---- do not focus the last invalid input
                    ignore: "",  //---- validate all fields including form hidden input
                    rules:
                        {
                            'case_no'                 : "required",
                            'file_no'                 : "required",
                            'case_receive_date'       : "required",
                            'parties_name'            : "required",
                            'case_subject'            : "required",
                            'case_status'             : "required",
                            // if the has_interim_order is checked
                            'interim_order_date'      : { required: '#has_interim_order:checked' },
                            'deadline'                : { required: '#has_interim_order:checked' },
                            'order_details'           : { required: '#has_interim_order:checked' },
                            'sf_issue_memo'           : {
                                                            required:{ depend: function() {
                                                                return $('#case_status').val() === '2';
                                                            }}
                                                        }

                        },
                    messages: { },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        error.show();
                        Metronic.scrollTo(error, -200);
                        error.delay(6000).fadeOut(2000);
                    },
                    highlight: function (element)
                    { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element)
                    { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label)
                    {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    /*submitHandler : function()
                    {
                        return false;
                    }*/
                });
        }
        return {
            //---- main function to initiate the module
            init: function () {
                addUserForm();
            }
        };
        }();

        $('.btn-save').on('click', function (){
            // if has_interim_order is checked then all the dynamic fileds in Interim Order section is required
            if($('#has_interim_order').is(':checked')) {
                $('.validateImpAuth').each(function () {
                    $(this).rules("add", {
                        required: true
                    });
                });
            }
            //otherwise remove the rules
            else {
                $('.validateImpAuth').each(function () {
                    $(this).rules('remove', 'required');
                });
            }
        });

        $('#sf_recipient').on('change', function(){
            if ($(this).val() == 6 ) // show the district list for DC office
            {
                $('#sf_dc_office').show();
                $('#sf_uno_office').hide();
                $('#sf_ministry').hide();
            }
            else if ($(this).val() == 7) // show Upzilla list for UNO office
            {
                $('#sf_uno_office').show();
                $('#sf_dc_office').hide();
                $('#sf_ministry').hide();
            }
            else if ( $(this).val() == 8 ) // show Ministry List
            {
                $('#sf_ministry').show();
                $('#sf_dc_office').hide();
                $('#sf_uno_office').hide();
            }
            else // hide Ministry, District and Upzilla list
            {
                $('#sf_dc_office').hide();
                $('#sf_uno_office').hide();
                $('#sf_ministry').hide();
            }
        });

        $(function() {
            //FormValidation.init();

            $('.date-picker').datepicker({
                autoclose: true,
                isRTL: Metronic.isRTL(),
                format: "dd/mm/yyyy"
            });

            // INITIATE THE SWITCHES
            $("#has_interim_order").bootstrapSwitch();
            $("#has_rule_nisi").bootstrapSwitch();
            $("#has_court_contempted").bootstrapSwitch();

            $('#sf_dc_office').hide();
            $('#sf_uno_office').hide();
            $('#sf_ministry').hide();
        });
    </script>
@endsection