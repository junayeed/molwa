@extends('layouts.master')
@section('content')
    <!-- BEGIN PAGE HEADER-->
    @include('layouts.messages')

    <form role="form" id="role_create_form" method="POST" action="/forwardDairy/{{$case_data->id}}">
        {{csrf_field()}}
        {{method_field('PUT')}}

        <script>
            var interim_order_array   = [];
            var sf_data_array         = [];
            var hd_data_array         = [];

            @if (@$interim_order_details)
                @foreach($interim_order_details as $iod)
                    interim_order_array.push ({!! json_encode($iod) !!});
                @endforeach
            @endif


            @if ( @$sf_data )
                @foreach ( $sf_data as $sf )
                    sf_data_array.push( {!! json_encode($sf) !!})
                @endforeach
            @endif

            @if ( @$hearing_data )
                @foreach ( $hearing_data as $hd )
                hd_data_array.push( {!! json_encode($hd) !!})
            @endforeach
            @endif

        </script>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet">
                    <div class="portlet-title">

                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="font-size: 20px; padding: 5px 20px !important;"> ACTIONS
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="/forwardDairy/create" style="font-size: 16px;"><i class="fa fa-plus "></i> Add New Case </a></li>
                                    <li class="divider"> </li>
                                    <li><a href="/forwardDairySearch" style="font-size: 16px;"><i class="fa fa-print "></i> Print Case Profile </a></li>
                                    <li><a href="/forwardDairySearch" style="font-size: 16px;"><i class="fa fa-rss "></i> Send SMS </a></li>
                                    <li><a href="/forwardDairySearch" style="font-size: 16px;"><i class="fa fa-search "></i> Search Case </a></li>
                                    <li class="divider"> </li>
                                    <li><a href="/forwardDairy" style="font-size: 16px;"><i class="fa fa-backward "></i> Back to Case List </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- CASE DETAILS (LFET COL) START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Case Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>eNothi File No</label>
                                        <div class="input-icon">
                                            <i class="" style="margin: 6px 2px 4px 10px !important; font-size: 15px; font-style: normal !important; color: #000 !important;">
                                                48.00.0000.007.
                                            </i>
                                            <input name="file_no" id="file_no" type="text" class="form-control input" value="{{$case_data->file_no}}"  style="padding-left: 125px !important;">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Hard File No</label>
                                        <div class="input-icon">
                                            <i class="" style="margin: 6px 2px 4px 10px !important; font-size: 15px; font-style: normal !important; color: #000 !important;">
                                                48.00.0000.004.
                                            </i>
                                            <input name="hard_file_no" id="hard_file_no" type="text" class="form-control input" style="padding-left: 125px;" value="{{$case_data->hard_file_no}}" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>Case Type</label>
                                        <select name="case_type" id="case_type" class="form-control input" >
                                            <option value="">Select Case Type</option>
                                            @foreach($case_type_list as $id => $case_type)
                                                <option value="{{$id}}" {{ $case_data['case_type'] == $id ? 'selected="selected"' : '' }} >{{$case_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Case No</label>
                                        <input name="case_no" id="case_no" type="text" class="form-control input" value="{{$case_data->case_no}}">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Order Date of Court</label>
                                        <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                            <input type="text" class="form-control input" name="writ_petition_order_date" id="writ_petition_order_date" value="{{\Carbon\Carbon::parse($case_data->writ_petition_order_date)->format('d/m/Y')}}" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Case Receive Date</label>
                                        <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                            <input type="text" class="form-control input" name="case_receive_date" id="case_receive_date" value="{{\Carbon\Carbon::parse($case_data->case_receive_date)->format('d/m/Y')}}" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label>Number of Pages</label>
                                        <input name="page_no" id="page_no" type="text" class="form-control input" value="{{$case_data->page_no}}">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Appendix List</label>
                                        <input name="appendix_list" id="appendix_list" type="text" class="form-control input" value="{{$case_data->appendix_list}}">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Parties Name</label>
                                        <input name="parties_name" id="parties_name" type="text" class="form-control input"  value="{{$case_data->parties_name}}" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Subject Type</label>
                                        <select name="subject_type[]" id="subject_type" class="form-control input" >
                                            <option value="">Select Case Type</option>
                                            @foreach($case_subject_type_list as $id => $subject_type)
                                                <option value="{{$id}}">{{$subject_type}}</option>
                                                {{--<option value="{{$id}}" {{ $case_data->subject_type == $id ? 'selected="selected"' : '' }}>{{$subject_type}}</option>--}}
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Subject</label>
                                        <textarea name="case_subject" id="case_subject" class="form-control input" >{{$case_data->case_subject}}</textarea>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Case Status</label>
                                        <select name="case_status" id="case_status" class="form-control input" >
                                            <option value="">Select Status</option>
                                            @foreach($status_list as $id => $status)
                                                <option value="{{$id}}" {{ $case_data['case_status'] == $id ? 'selected="selected"' : '' }}>{{$status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <label>Has Ruli Nisi?</label><br>
                                        <input type="checkbox" name="has_rule_nisi" id="has_rule_nisi" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Has Interim Order?</label><br>
                                        <input type="checkbox" name="has_interim_order" id="has_interim_order" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Has Supplementary Order?</label><br>
                                        <input type="checkbox" name="has_supplementary_order" id="has_supplementary_order" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Court Contempted?</label><br>
                                        <input type="checkbox" name="has_court_contempted" id="has_court_contempted" data-on-text="Yes" data-off-text="No" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CASE DETAILS (LFET COL) END -->

            <!-- Court Condempted (RIGHT COL) START -->
            <div class="col-md-6" id="court_contempted_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Court Contempted
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Contempt No</label>
                                <input name="contempt_no" id="contempt_no" type="text" class="form-control input" value="{{@$court_contempted_data[0]->contempt_no}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Received Date</label>
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="contempt_received_date"
                                           value="{{\Carbon\Carbon::parse(@$court_contempted_data[0]->contempt_received_date)->format('d/m/Y')}}"
                                           readonly  placeholder="dd/mm/yyyy">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Appearance Date</label>
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="appearance_date"
                                           value="{{\Carbon\Carbon::parse(@$court_contempted_data[0]->appearance_date)->format('d/m/Y')}}"
                                           readonly placeholder="dd/mm/yyyy">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-9">
                                <label>Remarks</label>
                                <textarea name="contempt_remarks" id="contempt_remarks" class="form-control input" >{{@$court_contempted_data[0]->contempt_remarks}}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Court Condempted (RIGHT COL) END -->

            <!-- Rule Nisi (RIGHT COL) START -->
            <div class="col-md-6" id="rule_nisi_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Rule Nisi
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Rule Nisi Order Date</label>
                                {{--<input id="rule_nisi_order_date" name="rule_nisi_order_date" class="form-control date-picker input" size="16" type="text" value="{{\Carbon\Carbon::parse(@$rule_nisi_data[0]->rule_nisi_order_date)->format('d/m/Y')}}" readonly  />--}}
                                <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                    <input type="text" class="form-control input" name="rule_nisi_order_date" value="{{\Carbon\Carbon::parse(@$rule_nisi_data[0]->rule_nisi_order_date)->format('d/m/Y')}}" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-9">
                                <label>Order Details</label>
                                <textarea name="rule_nisi_order_details" id="rule_nisi_order_details" class="form-control input" >{{@$rule_nisi_data[0]->rule_nisi_order_details}}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Rule Nisi (RIGHT COL) END -->
            <!-- Interim Order (RIGHT COL) START -->
            <div class="col-md-6" id="interim_order_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Interim Order
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Interim Order Date</label>
                                @if (isset($interim_order_data[0]->interim_order_date) && !empty($interim_order_data[0]->interim_order_date))
                                    {{--<input id="interim_order_date" name="interim_order_date" class="form-control date-picker input" size="16" readonly type="text" value="{{\Carbon\Carbon::parse($interim_order_data[0]->interim_order_date)->format('d/m/Y')}}" readonly />--}}
                                    <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                        <input type="text" class="form-control input" name="interim_order_date" value="{{\Carbon\Carbon::parse(@$interim_order_data[0]->interim_order_date)->format('d/m/Y')}}" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                    </div>
                                @else
                                    <input id="interim_order_date" name="interim_order_date" class="form-control date-picker input" size="16" readonly type="text" value="" readonly />
                                @endif
                            </div>
                            <div class="form-group col-md-3">
                                <label>Deadline (No. of days)</label>
                                @if (isset($interim_order_data[0]->deadline) && !empty($interim_order_data[0]->deadline))
                                    <input id="deadline" name="deadline" class="form-control input" type="text" value="{{$interim_order_data[0]->deadline}}"  />
                                @else
                                    <input id="deadline" name="deadline" class="form-control input" type="text" value=""  />
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Order Details</label>
                                @if (isset($interim_order_data[0]->deadline) && !empty($interim_order_data[0]->order_details))
                                    <textarea name="order_details" id="order_details" class="form-control input" >{{$interim_order_data[0]->order_details}}</textarea>
                                @else
                                    <textarea name="order_details" id="order_details" class="form-control input" ></textarea>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-repeater" id="interim_order_repeater">
                                    <div data-repeater-list="interim_order_data">
                                        @if ( isset($interim_order_details[0]) && !empty($interim_order_details[0]))
                                            @foreach($interim_order_details as $iod)
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <div class="row mt-repeater-row">
                                                        <div class="col-md-3"  id="interim_order_details_div">
                                                            <label class="small ">Implementing Authority</label>
                                                            <select name="implementing_authority" id="implementing_authority" class="form-control input" >
                                                                <option value="">Select an Authority</option>
                                                                @foreach($imp_authority_list as $id => $imp_authority)
                                                                    <option value="{{$id}}" {{ $iod->implementing_authority == $id ? 'selected="selected"' : '' }}>{{$imp_authority}}</option>
                                                                @endforeach
                                                            </select>
                                                            <select name='ministry' id="ministry" class="form-control input margin-top-10 ">
                                                                <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                                                @foreach($ministry_list as $id => $ministry)
                                                                    <option value="{{$id}}" {{ $iod->ministry == $id ? 'selected="selected"' : '' }}>{{$ministry}}</option>
                                                                @endforeach
                                                            </select>
                                                            <select name='dc_office' id="dc_office" class="form-control input margin-top-10 ">
                                                                <option value="">জেলা প্রশাসক কার্যালয় বাছাই করুন</option>
                                                                @foreach($district_list as $id => $district)
                                                                    <option value="{{$id}}" {{ $iod->dc_office == $id ? 'selected="selected"' : '' }}>{{$district}}</option>
                                                                @endforeach
                                                            </select>
                                                            <select name='uno_office' id="uno_office" class="form-control input margin-top-10 " >
                                                                <option value="">ইউ এন ও কার্যালয় বাছাই করুন</option>
                                                                @foreach($upzilla_list as $id => $upzilla)
                                                                    <option value="{{$id}}" {{ $iod->uno_office == $id ? 'selected="selected"' : '' }}>{{$upzilla}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label class="control-label small">Responsibility</label>
                                                            <textarea name="responsibility" id="responsibility" class="form-control input" >{{$iod->responsibility}}</textarea>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-repeater-row margin-top-10">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_issue_memo" name="int_issue_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_issue_memo}}" placeholder="Issue Memo no." />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_issue_date" name="int_issue_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse(@$iod->int_issue_date)->format('d/m/Y')}}" readonly />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_receive_memo" name="int_receive_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_receive_memo}}" placeholder="Receiving Memo no." />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_receive_date" name="int_receive_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse(@$iod->int_receive_date)->format('d/m/Y')}}" readonly />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_sending_memo" name="int_sending_memo" class="form-control input" size="16" type="text" value="{{@$iod->int_sending_memo}}" placeholder="Sending Memo no." />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <input id="int_sending_date" name="int_sending_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse(@$iod->int_sending_date)->format('d/m/Y')}}" readonly />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Interim Order (RIGHT COL) END -->

            <!-- Supplementary Order (RIGHT COL) START -->
            <div class="col-md-6" id="supplementary_order_div">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Supplementary Order
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-repeater" id="supplementary_order_repeater">
                                    <div data-repeater-list="supplementary_order_data">
                                        @if ( isset($supplementary_order_data[0]) && !empty($supplementary_order_data[0]))
                                            @foreach($supplementary_order_data as $sod)
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <div class="row mt-repeater-row">
                                                        <div class="col-md-3"  id="supplementary_order_details_div">
                                                            <label>Supplementary Order Date</label>
                                                            <input id="supplementary_order_date" name="supplementary_order_date" class="form-control date-picker input" size="16" readonly type="text" value="{{$sod->supplementary_order_date}}" readonly />
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label>Order Details</label>
                                                            <textarea name="supplementary_order_details" id="supplementary_order_details" class="form-control input" >{{$sod->supplementary_order_details}}</textarea>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @endif
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs green-meadow btn-circle mt-repeater-add"><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Supplementary Order (RIGHT COL) END -->
        </div>



        <!-- SF DETAILS START -->
        <div class="row"  id="sf_details_div">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>
                            Statement of Facts (SF)
                        </div>
                    </div>
                      <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12" id="sf_repeater">
                                <div class="form-group mt-repeater">
                                    <div data-repeater-list="sf_data">
                                        @if ( isset($sf_data) && !empty($sf_data))
                                        @foreach($sf_data as $sf)
                                            <div data-repeater-item class="mt-repeater-item">
                                                <div class="row mt-repeater-row" id="sf_details_div">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>SF Recipient</label>
                                                            <select name="sf_recipient" id="sf_recipient" class="form-control input" >
                                                                <option value="">Select a Recipient</option>
                                                                @foreach($recipient_list as $id => $recipient)
                                                                    <option value="{{$id}}" {{ $sf->sf_recipient == $id ? 'selected="selected"' : '' }}>{{$recipient}}</option>
                                                                @endforeach
                                                            </select>

                                                            <select name="sf_ministry" id="sf_ministry" class="form-control input margin-top-10 ">
                                                                <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                                                @foreach($ministry_list as $id => $ministry)
                                                                    <option value="{{$id}}" {{ $sf->sf_ministry == $id ? 'selected="selected"' : '' }}>{{$ministry}}</option>
                                                                @endforeach
                                                            </select>
                                                            <select name="sf_dc_office" id="sf_dc_office" class="form-control input margin-top-10 ">
                                                                <option value="">জেলা প্রশাসক কার্যালয় বাছাই করুন</option>
                                                                @foreach($district_list as $id => $district)
                                                                    <option value="{{$id}}" {{ $sf->sf_dc_office == $id ? 'selected="selected"' : '' }}>{{$district}}</option>
                                                                @endforeach
                                                            </select>
                                                            <select name="sf_uno_office" id="sf_uno_office" class="form-control input margin-top-10 " >
                                                                <option value="">ইউ এন ও কার্যালয় বাছাই করুন</option>
                                                                @foreach($upzilla_list as $id => $upzilla)
                                                                    <option value="{{$id}}" {{ $sf->sf_uno_office == $id ? 'selected="selected"' : '' }}>{{$upzilla}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Memo No.</label>
                                                            <input id="sf_issue_memo" name="sf_issue_memo" class="form-control input" size="16" type="text" value="{{$sf->sf_issue_memo}}"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label>Date of Issue</label>
                                                            @if ( $sf->sf_issue_date != '0000-00-00')
                                                                <input id="sf_issue_date" name="sf_issue_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse($sf->sf_issue_date)->format('d/m/Y')}}" readonly />
                                                            @else
                                                                <input id="sf_issue_date" name="sf_issue_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>SF Receiving Memo No.</label>
                                                            <input id="sf_receive_memo" name="sf_receive_memo" class="form-control input" size="16" type="text" value="{{$sf->sf_receive_memo}}"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label>SF Receiving Date</label>
                                                            @if ( $sf->sf_receive_date != '0000-00-00')
                                                                <input id="sf_receive_date" name="sf_receive_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse($sf->sf_receive_date)->format('d/m/Y')}}" readonly />
                                                            @else
                                                                <input id="sf_receive_date" name="sf_receive_date" class="form-control date-picker input" readonly placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>SF Sending Memo No.</label>
                                                            <input id="sf_sending_memo" name="sf_sending_memo" class="form-control input" size="16" type="text" value="{{$sf->sf_sending_memo}}"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label>SF Sending Date</label>
                                                            @if ($sf->sf_sending_date != '0000-00-00')
                                                                <input id="sf_sending_date" name="sf_sending_date" class="form-control date-picker input" placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse($sf->sf_sending_date)->format('d/m/Y')}}" readonly />
                                                            @else
                                                                <input id="sf_sending_date" name="sf_sending_date" class="form-control date-picker input" placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs btn-circle green-meadow mt-repeater-add"><i class="fa fa-plus"></i> Add New Recipient</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SF DETAILS END -->

        <!-- AOR STARTS HERE-->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> AOR Details
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="aor_name" name="aor_name" class="form-control input" size="16" type="text" value=""  />
                                    <input id="aor_id" name="aor_id" class="form-control input" size="16" type="hidden" value=""  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Cell No.</label>
                                    <input id="aor_cell_no" name="aor_cell_no" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Memo No</label>
                                    <input id="aor_memo_no" name="aor_memo_no" class="form-control input" size="16" type="text" value=""  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                        <input type="text" class="form-control input" name="aor_memo_date" id="aor_memo_date" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> বিদ্যমান
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th> # </th>
                                    <th> Name </th>
                                    <th> Cell No </th>
                                    <th> Memo No </th>
                                    <th> Memo Date </th>
                                    <th>  </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ( $aor_data )
                                    <?php $i = 1; ?>
                                    @foreach( $aor_data as $aor)
                                    <tr>
                                        <td> {{$i++}} </td>
                                        <td> {{$aor->aor_name}} </td>
                                        <td> {{$aor->aor_cell_no}} </td>
                                        <td> {{$aor->aor_memo_no}} </td>
                                        <td> {{$aor->aor_memo_date}} </td>
                                        <td> <i class="fa fa-edit aor_edit" data-aor_id="{{$aor->id}}"></i> </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- AOR ENDS HERE-->

        <!-- Recipient Ministry START -->
        <div class="row" id="recipient_ministry_div">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i> Recipient Ministry
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Recipient</label>
                                    <select name="recipient_ministry" id="recipient_ministry" class="form-control input" >
                                        <option value="">মন্ত্রণালয় বাছাই করুন</option>
                                        @foreach($ministry_list as $id => $ministry)
                                            <option value="{{$id}}" @if(@$recipient_ministry[0]->recipient_ministry == $id) selected @endif>{{$ministry}}</option>
                                        @endforeach
                                        <option  @if(@$recipient_ministry[0]->recipient_ministry == 999) selected @endif value="999">অন্যান্য</option>
                                    </select>
                                    <div id="recipient_others_div">
                                        <input id="recipient_others" name="recipient_others" class="form-control input margin-top-10 " size="16" type="text" value="{{@$recipient_ministry[0]->recipient_others}}"  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Memo No.</label>
                                    <input id="memo_no" name="memo_no" class="form-control input" size="16" type="text" value="{{@$recipient_ministry[0]->memo_no}}"  />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Date of Issue</label>
                                    <div class="input-group date date-picker input" data-date-format="dd/mm/yyyy" style="padding: 0px 0px !important;">
                                        <input type="text" class="form-control input" name="issue_date" value="{{\Carbon\Carbon::parse(@$recipient_ministry[0]->issue_date)->format('d/m/Y')}}" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button" style="padding: 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <input id="recipient_ministry_remarks" name="recipient_ministry_remarks" class="form-control input" size="16" type="text" value="{{@$recipient_ministry[0]->remarks}}"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Recipient Ministry END -->

        <!-- JUDGEMENT DETAILS START -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>Judgement Details <input type="checkbox" value="1" name="has_judgement" id="has_judgement">
                        </div>
                    </div>
                    <div class="portlet-body" id="judgement_details_div">
                        <div class="row">
                            <div class="col-md-12" id="hearing_repeater">
                                <div class="form-group mt-repeater" id="hearing_data_container">
                                    <div data-repeater-list="hearing_data">
                                        @if ( isset($hearing_data) && !empty($hearing_data))
                                            @foreach($hearing_data as $hd)
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <div class="row mt-repeater-row">
                                                        <div class="col-md-2" id="hearing_details_div">
                                                            <div class="form-group">
                                                                <label>Hearing Date</label>
                                                                @if ($hd->hearing_date != '0000-00-00')
                                                                    <input id="hearing_date" name="hearing_date" class="form-control date-picker input" placeholder="dd/mm/yyyy" type="text" value="{{\Carbon\Carbon::parse($hd->hearing_date)->format('d/m/Y')}}" readonly />
                                                                @else
                                                                    <input id="hearing_date" name="hearing_date" class="form-control date-picker input" placeholder="dd/mm/yyyy" type="text" value="" readonly />
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Order/ Judgement</label>
                                                                <textarea name="judgement" id="judgement" class="form-control input" >{{(isset($hd->hearing_judgement) ? @$hd->hearing_judgement : "")}}</textarea>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-1">
                                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info btn-xs btn-circle green-meadow mt-repeater-add" id="hearing_repeater_button"><i class="fa fa-plus"></i> Add New Hearing Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>C.M.P. No.</label>
                                    <input id="cmp_no" name="cmp_no" class="form-control input" type="text" value="{{(isset($judgement_data[0]->cmp_no) ? @$judgement_data[0]->cmp_no : "")}}"  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>C.M.P. Order / Judgement</label>
                                    <textarea name="cmp_judgement" id="cmp_judgement" class="form-control input" >{{(isset($judgement_data[0]->cmp_judgement) ? @$judgement_data[0]->cmp_judgement : "")}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>C.P.L.A No</label>
                                    <input id="cp_no" name="cp_no" class="form-control input" type="text" value="{{(isset($judgement_data[0]->cp_no) ? @$judgement_data[0]->cp_no : "")}}"  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>C.P.L.A Order / Judgement</label>
                                    <textarea name="cp_judgement" id="cp_judgement" class="form-control input" >{{(isset($judgement_data[0]->cp_judgement) ? @$judgement_data[0]->cp_judgement : "")}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>Appeal Case No</label>
                                    <input id="appeal_case_no" name="appeal_case_no" class="form-control input" type="text" value="{{(isset($judgement_data[0]->appeal_case_no) ? @$judgement_data[0]->appeal_case_no : "")}}"  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>Appeal Order / Judgement</label>
                                    <textarea name="appeal_judgement" id="appeal_judgement" class="form-control input" >{{(isset($judgement_data[0]->appeal_judgement) ? @$judgement_data[0]->appeal_judgement : "")}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group col-md-12">
                                    <label>Review Petition No</label>
                                    <input id="review_petition_no" name="review_petition_no" class="form-control input" type="text" value="{{(isset($judgement_data[0]->review_petition_no) ? @$judgement_data[0]->review_petition_no : "")}}"  />
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group col-md-12">
                                    <label>Order / Judgement</label>
                                    <textarea id="review_order" name="review_order" class="form-control input">{{(isset($judgement_data[0]->review_order) ? @$judgement_data[0]->review_order : "")}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Final Judgement</label>
                                    <textarea id="final_judgement" name="final_judgement" class="form-control input">{{(isset($judgement_data[0]->final_judgement) ? @$judgement_data[0]->final_judgement : "")}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- JUDGEMENT DETAILS END -->

        <!-- HISTORY LOG START -->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>Case History Log
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- HOSTORY STARTS HERE-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <div class="portlet-body" id="chats">
                                        <div class="scroller" style="height: 325px;" data-always-visible="1" data-rail-visible1="1">
                                            <ul class="chats">
                                                <li class="in">
                                                    <img class="avatar" alt="" src="/assets/layouts/layout/img/avatar.png" />
                                                    <div class="message">
                                                        <span class="arrow"> </span>
                                                        <a href="javascript:;" class="name"> {{$created_by[0]->user_name}} ({{$created_by[0]->designation}}) </a>
                                                        <span class="body">has created this case/pitition on <span class="font-red-flamingo bold">{{$created_by[0]->created_at}}</span></span>
                                                    </div>
                                                </li>
                                                @foreach($case_history as $history)
                                                    <li class="out">
                                                        <img class="avatar" alt="" src="/assets/layouts/layout/img/avatar.png" />
                                                        <div class="message">
                                                            <span class="arrow"> </span>
                                                            <a href="javascript:;" class="name"> {{$history['user_name']}} </a>
                                                            <span class="datetime"> at {{$history['updatedTime']}} </span>
                                                            @if ($history['fieldUpdated'] == 'sf_recipient')
                                                                <span class="body">has modified the value of
                                                                    <span class="bold">{{ $history['fieldUpdated'] }}</span> from
                                                                    <span class="font-red-flamingo bold">{{$recipient_list[$history['previousValue']]}}</span> to
                                                                    <span class="font-green-meadow bold">{{$recipient_list[$history['newValue']]}}</span>
                                                                </span>
                                                            @elseif ($history['fieldUpdated'] == 'case_status')
                                                                <span class="body">has modified the value of
                                                                    <span class="bold">{{$history['fieldUpdated']}}</span> from
                                                                    <span class="font-red-flamingo bold">{{$status_list[$history['previousValue']]}}</span> to
                                                                    <span class="font-green-meadow bold">{{$status_list[$history['newValue']]}}</span>
                                                                </span>
                                                            @elseif ($history['fieldUpdated'] == 'case_type')
                                                                <span class="body">has modified the value of
                                                                    <span class="bold">{{$history['fieldUpdated']}}</span> from
                                                                    <span class="font-red-flamingo bold">{{$case_type_list[$history['previousValue']]}}</span> to
                                                                    <span class="font-green-meadow bold">{{$case_type_list[$history['newValue']]}}</span>
                                                                </span>
                                                            @elseif ($history['fieldUpdated'] == 'subject_type')
                                                                <span class="body">has modified the value of
                                                                    <span class="bold">{{$history['fieldUpdated']}}</span> from
                                                                    <span class="font-red-flamingo bold">
                                                                        @foreach(explode(',', $history['previousValue']) as $old_subject)
                                                                            {{$case_subject_type_list[$old_subject]}}{{ ($loop->last ? '' : ', ') }}
                                                                        @endforeach
                                                                    </span> to
                                                                    <span class="font-green-meadow bold">
                                                                        @foreach(explode(',', $history['newValue']) as $new_subject)
                                                                            {{$case_subject_type_list[$new_subject]}}{{ ($loop->last ? '' : ', ') }}
                                                                        @endforeach
                                                                    </span>
                                                                </span>
                                                            @else
                                                                <span class="body">has modified the value of
                                                                    <span class="bold">{{$history['fieldUpdated']}}</span> from
                                                                    @if ( $history['fieldUpdated'] == 'has_rule_nisi' || $history['fieldUpdated'] == 'has_interim_order')
                                                                        <span class="font-red-flamingo bold">
                                                                            {{$history['previousValue'] == 0 ? 'No' : 'Yes' }}
                                                                        </span> to
                                                                        <span class="font-green-meadow bold">
                                                                            {{$history['newValue'] == 0 ? 'No' : 'Yes' }}
                                                                        </span>
                                                                    @else
                                                                        <span class="font-red-flamingo bold">{{$history['previousValue']}}</span> to
                                                                        <span class="font-green-meadow bold">{{$history['newValue']}}</span>
                                                                    @endif
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <!-- HOSTORY ENDS HERE-->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cog"></i>Case Facts
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h5 class="widget-thumb-heading">Days Passed After Case Received</h5>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow-casablanca icon-bulb"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">DAYS</span>
                                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>
                            @if ($sf->sf_issue_date != '0000-00-00')
                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h5 class="widget-thumb-heading">Awaiting for SF</h5>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow-casablanca icon-bulb"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">DAYS</span>
                                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($sf->sf_issue_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>
                            @endif
                            @if ($sf->sf_receive_date != '0000-00-00')
                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h5 class="widget-thumb-heading">Days passed after SF Received</h5>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow-casablanca icon-bulb"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">DAYS</span>
                                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($sf->sf_receive_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if ($sf->sf_sending_date != '0000-00-00')
                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h5 class="widget-thumb-heading">Days passed after SF Sent</h5>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow-casablanca icon-bulb"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">DAYS</span>
                                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($sf->sf_sending_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($case_data->case_receive_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>
                            @endif
                        {{--<div class="row">
                           @foreach($sf_data as $sf)
                                @if (isset($sf->sf_recipient) && ($sf->sf_recipient != 0) )
                                <div class="col-md-5">
                                    <!-- BEGIN WIDGET THUMB -->
                                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                        <h5 class="widget-thumb-heading">SF is pending at {{$recipient_list[$sf->sf_recipient]}}</h5>
                                        <div class="widget-thumb-wrap">
                                            <i class="widget-thumb-icon bg-red icon-layers"></i>
                                            <div class="widget-thumb-body">
                                                <span class="widget-thumb-subtitle">DAYS</span>
                                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{\Carbon\Carbon::parse($sf->sf_issue_date)->diffInDays(\Carbon\Carbon::now())}}">{{\Carbon\Carbon::parse($sf->sf_issue_date)->diffInDays(\Carbon\Carbon::now())}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END WIDGET THUMB -->
                                </div>
                                @endif
                            @endforeach
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- HISTORY LOG END -->

        <div class="row ">
            <div class="col-md-12">
                <button type="submit" class="btn btn-save btn-success font-hg col-md-2"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
                <a href="/forwardDairy" class="btn btn-error font-hg col-md-1"><i class="fa fa-ban"></i> তালিকায় ফেরত যান</a>
            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/users" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@section('page_plugins')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
@endsection

    <script type="text/javascript">
        $('#has_judgement').on('change', function() {
            if ($('#has_judgement').is(':checked')) {
                $('#judgement_details_div').show();
            } else {
                $('#judgement_details_div').hide();
            }
        });

        $(function() {
            $('#dc_office').hide();
            $('#uno_office').hide();
            $('#ministry').hide();

            $('#sf_dc_office').hide();
            $('#sf_uno_office').hide();
            $('#sf_ministry').hide();
            $('#recipient_ministry_div').hide();
            $('#judgement_details_div').hide();
            $('#court_contempted_div').hide();
            $('#recipient_others_div').hide();
            $('#supplementary_order_div').hide();

            $('.date-picker').datepicker({
                autoclose: true,
                defaultDate: '',
                format: "dd/mm/yyyy",
                isRTL: Metronic.isRTL()
            });

            $('#subject_type').select2({
                multiple: true
            });

            var interim_order_repeater = $("#interim_order_repeater").repeater({
                initEmpty:!1,
                show:function(){
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) }
            });

            var supplementary_order_repeater = $("#supplementary_order_repeater").repeater({
                initEmpty:!1,
                show:function(){
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) }
            });

            var sf_repeater = $("#sf_repeater").repeater({
                initEmpty:!1,
                show:function() {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) },
                isFirstItemUndeletable: true
            });

            var hearing_repeater = $("#hearing_repeater").repeater({
                initEmpty:!0,
                show:function() {
                    $(this).slideDown();
                    $('.date-picker').datepicker({
                        autoclose: true,
                        isRTL: Metronic.isRTL(),
                        format: "dd/mm/yyyy"
                    });
                },
                hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) },
                isFirstItemUndeletable: true
            });

            var subject_type     = '{{$case_data['subject_type']}}';
            var subject_type_arr = subject_type.split(',')
            var subject_types    = [];

            $.each(subject_type_arr, function() {
                subject_types.push(this); 
            });

            var case_status = {{$case_data['case_status']}};
            var recipient_ministry_others = "{{@$recipient_ministry[0]->recipient_ministry}}";

            if (case_status === 6) {
                $('#sf_details_div').hide();
                $('#recipient_ministry_div').show();
                if (recipient_ministry_others === "999") {
                    $('#recipient_others_div').show();
                }
            }
            else {
                $('#sf_details_div').show();
                $('#recipient_ministry_div').hide();
                $('#recipient_others_div').hide();
            }

            $('#subject_type').val(subject_types);
            $('#subject_type').trigger("change");

            // Hearing Details Repeater Set data starts here
            if ( hd_data_array.length ) { // SET DATA FOR Interim Order Repeater
                var items = {};
                var i = 0;
                $.each(hd_data_array, function (key, val) {
                    items[key] = {hearing_date: val.hearing_date, judgement: val.hearing_judgement};

                    hearing_repeater.setList(items);
                });
            }
            // IHearing Detailse Repeater Set data ends here

            // INTERIM ORDER DETAILS Repeater Set data starts here
            if ( interim_order_array.length ) { // SET DATA FOR Interim Order Repeater
                var items = {};
                var i = 0;
                $.each(interim_order_array, function (key, val) {
                    /*items[key] = {session_topic: val.session_topic, employee_id: val.employee_id, session_start_time: val.session_start_time,
                        session_end_time: val.session_end_time, trainer_type: val.trainer_type, trainer_name: val.trainer_name,
                        trainer_office: val.trainer_office, trainer_email: val.trainer_email, trainer_cellphone: val.trainer_cellphone,
                        session_date: val.session_date};*/
                    if(val.implementing_authority == 6  ) { // DC OFFICE
                        $("select[name='interim_order_data["+i+"][dc_office]']").show();
                        $("select[name='interim_order_data["+i+"][ministry]']").hide();
                        $("select[name='interim_order_data["+i+"][uno_office]']").hide();
                    }
                    else if (val.implementing_authority == 7) {  // UNO office
                        $("select[name='interim_order_data["+i+"][dc_office]']").hide();
                        $("select[name='interim_order_data["+i+"][ministry]']").hide();
                        $("select[name='interim_order_data["+i+"][uno_office]']").show();
                    }
                    else if ( val.implementing_authority == 3 ) { // Ministry
                        $("select[name='interim_order_data["+i+"][dc_office]']").hide();
                        $("select[name='interim_order_data["+i+"][ministry]']").show();
                        $("select[name='interim_order_data["+i+"][uno_office]']").hide();
                    }
                    else {
                        $("select[name='interim_order_data["+i+"][dc_office]']").hide();
                        $("select[name='interim_order_data["+i+"][ministry]']").hide();
                        $("select[name='interim_order_data["+i+"][uno_office]']").hide();
                    }
                    i++;
                    //interim_order_repeater.setList(items);
                });
            }
            // INTERIM ORDER DETAILS Repeater Set data ends here

            // SF DETAILS Repeater Set data starts here
            if ( sf_data_array.length ) { // SET DATA FOR SF
                var items = {};
                var i = 0;
                $.each(sf_data_array, function (key, val) {

                    if(val.sf_recipient == 6  ) { // DC OFFICE
                        $("select[name='sf_data["+i+"][sf_dc_office]']").show();
                        $("select[name='sf_data["+i+"][sf_ministry]']").hide();
                        $("select[name='sf_data["+i+"][sf_uno_office]']").hide();
                    }
                    else if (val.sf_recipient == 7) {  // UNO office
                        $("select[name='sf_data["+i+"][sf_dc_office]']").hide();
                        $("select[name='sf_data["+i+"][sf_ministry]']").hide();
                        $("select[name='sf_data["+i+"][sf_uno_office]']").show();
                    }
                    else if ( val.sf_recipient == 3 ) { // Ministry
                        $("select[name='sf_data["+i+"][sf_dc_office]']").hide();
                        $("select[name='sf_data["+i+"][sf_ministry]']").show();
                        $("select[name='sf_data["+i+"][sf_uno_office]']").hide();
                    }
                    else {
                        $("select[name='sf_data["+i+"][sf_dc_office]']").hide();
                        $("select[name='sf_data["+i+"][sf_ministry]']").hide();
                        $("select[name='sf_data["+i+"][sf_uno_office]']").hide();
                    }
                    i++;
                    //interim_order_repeater.setList(items);
                });
            }
            // SF DETAILS Repeater Set data ends here

             // HAS RULE NISI SWITCH START
            $("#has_interim_order").bootstrapSwitch();
            $("#has_rule_nisi").bootstrapSwitch();
            $('#has_supplementary_order').bootstrapSwitch();

            var has_rule_nisi            = {{$case_data->has_rule_nisi}};
            var has_interim_order        = {{$case_data->has_interim_order}};
            var has_court_contempted     = {{$case_data->has_court_contempted}};
            var has_judgement            = {{$case_data->has_judgement}};
            var has_supplementary_order  = {{$case_data->has_supplementary_order}};


            if ( has_rule_nisi ){
                $("#has_rule_nisi").bootstrapSwitch('state', true);
                $('#rule_nisi_div').show();
            }
            else {
                $("#has_rule_nisi").bootstrapSwitch('state', false);
                $('#rule_nisi_div').hide();
            }
            // HAS RULE NISi SWITCH END

            if ( has_interim_order ){
                $("#has_interim_order").bootstrapSwitch('state', true);
                $('#interim_order_div').show();
            }
            else {
                $("#has_interim_order").bootstrapSwitch('state', false);
                $('#interim_order_div').hide();
            }

            if ( has_supplementary_order ){
                $("#has_supplementary_order").bootstrapSwitch('state', true);
                $('#supplementary_order_div').show();
            }
            else {
                $("#has_supplementary_order").bootstrapSwitch('state', false);
                $('#supplementary_order_div').hide();
            }

            if ( has_court_contempted ){
                $("#has_court_contempted").bootstrapSwitch('state', true);
                $('#court_contempted_div').show();
            }
            else {
                $("#has_court_contempted").bootstrapSwitch('state', false);
                $('#has_court_contempted').hide();
            }

            if ( has_judgement ) {
                $('#judgement_details_div').show();
                $('#has_judgement').prop( "checked", true );
            }
            else {
                $('#judgement_details_div').hide();
                $('#has_judgement').prop( "checked", false );
            }

            $('#has_court_contempted').on('switchChange.bootstrapSwitch', function() {
                if ($("#has_court_contempted").bootstrapSwitch('state')) {
                    $('#court_contempted_div').show();
                }
                else {
                    $('#court_contempted_div').hide();
                }
            });

            $('#has_interim_order').on('switchChange.bootstrapSwitch', function() {

                if ($("#has_interim_order").bootstrapSwitch('state')) {
                    $('#interim_order_div').show();
                }
                else {
                    $('#interim_order_div').hide();
                }
            });

            $('#has_rule_nisi').on('switchChange.bootstrapSwitch', function() {

                if ($("#has_rule_nisi").bootstrapSwitch(('state'))) {
                    $('#rule_nisi_div').show();
                }
                else {
                    $('#rule_nisi_div').hide();
                }
            });

            $('#has_supplementary_order').on('switchChange.bootstrapSwitch', function() {
                if ($("#has_supplementary_order").bootstrapSwitch(('state'))) {
                    $('#supplementary_order_div').show();
                }
                else {
                    $('#supplementary_order_div').hide();
                }
            });

            $("#case_details_form").validate(
                {
                    rules:
                        {
                            'case_no'                 : "required",
                            'case_type'               : "required",
                            'file_no'                 : "required",
                            'case_receive_date'       : "required",
                            'parties_name'            : "required",
                            'subject_type'            : "required",
                            'case_subject'            : "required",
                            'case_status'             : "required",
                            // if the has_interim_order is checked
                            'interim_order_date'      : { required: '#has_interim_order:checked' },
                            'deadline'                : { required: '#has_interim_order:checked' },
                            'order_details'           : { required: '#has_interim_order:checked' },
                            // if the has_rule_nisi is checked
                            'rule_nisi_order_date'    : { required: '#has_rule_nisi:checked'},
                            'rule_nisi_order_details' : { required: '#has_rule_nisi:checked'},

                            //'implementing_authority'  : { required: '#has_interim_order:checked' },
                            //'responsibility'          : { required: '#has_interim_order:checked' },
                            'sf_issue_memo'           : {
                                required:
                                    {
                                        depends: function()
                                        {
                                            return $('#case_status').val() == "2";
                                        }
                                    }
                            }

                        },
                    messages: { },
                    highlight: function (element)
                    { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element)
                    { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label)
                    {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler : function()
                    {
                        return false;
                    }
                });
        });

        $(document).on('change', '#implementing_authority', function() {
            if ($(this).val() == 6 ) // show the district list for DC office
            {

                $(this).parent().closest('#interim_order_details_div').find('#dc_office').show();
                $(this).parent().closest('#interim_order_details_div').find('#uno_office').hide();
                $(this).parent().closest('#interim_order_details_div').find('#ministry').hide();
            }
            else if ($(this).val() == 7) // show Upzilla list for UNO office
            {
                $(this).parent().closest('#interim_order_details_div').find('#uno_office').show();
                $(this).parent().closest('#interim_order_details_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_details_div').find('#ministry').hide();
            }
            else if ( $(this).val() == 3 ) // show Ministry List
            {
                $(this).parent().closest('#interim_order_details_div').find('#ministry').show();
                $(this).parent().closest('#interim_order_details_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_details_div').find('#uno_office').hide();
            }
            else // hide Ministry, District and Upzilla list
            {
                $(this).parent().closest('#interim_order_details_div').find('#ministry').hide();
                $(this).parent().closest('#interim_order_details_div').find('#dc_office').hide();
                $(this).parent().closest('#interim_order_details_div').find('#uno_office').hide();
            }
        });

        $(document).on('change', '#sf_recipient', function() {
            if ($(this).val() == 6 ) { // show the district list for DC office
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').show();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
            }
            else if ($(this).val() == 7) { // show Upzilla list for UNO office
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').show();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
            }
            else if ( $(this).val() == 3 ) { // show Ministry List
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').show();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
            }
            else { // hide Ministry, District and Upzilla list
                $(this).parent().closest('#sf_details_div').find('#sf_ministry').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_dc_office').hide();
                $(this).parent().closest('#sf_details_div').find('#sf_uno_office').hide();
            }
        });

        $('#case_status').on('change', function(){
            if(this.value == 6) { // if the status Necessary action required that is nothing to deal with MOLWA, case send to concern Ministry
                $('#sf_details_div').hide();
                $('#recipient_ministry_div').show();
            }
            else {
                $('#sf_details_div').show();
                $('#recipient_ministry_div').hide();
            }
        });

        $('.aor_edit').on('click', function () {
            aor_id = $(this).data('aor_id');

            if ( !aor_id) {
                return;
            }

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/getAORDetails",
                data: {aor_id: aor_id},
                dataType: 'JSON',
                success: function (res) {
                    $('#aor_name').val(res['aor_name']);
                    $('#aor_cell_no').val(res['aor_cell_no']);
                    $('#aor_memo_no').val(res['aor_memo_no']);
                    $('#aor_memo_date').val(res['aor_memo_date']);
                    $('#aor_id').val(res['id']);
                }
            });
        });

        $(document).on('change', '#recipient_ministry', function () {
            if ($(this).val() == 999) {
                $('#recipient_others_div').show();
            }
            else {
                $('#recipient_others_div').hide();
            }
        });

        $('#case_no').on('change', function(){
            var case_no = $(this).val();

            if ( !case_no) {
                return;
            }

            $.ajax({
                type: "GET",
                url: "{{URL::to('/')}}/checkDuplicateCaseNo",
                data: {case_no: case_no},
                dataType: 'JSON',
                complete: function (res) {
                    if (res.responseText == 'Duplicate') {
                        bootbox.alert("Case number already exists! Please check the case no. again.");
                    }
                }
            });
        });
    </script>
@endsection