@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT  -->
    @include('layouts.messages')
    <form role="form" id="case_search_form" method="POST" action="/generateFFListCSV">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="row">
            <!-- SEARCH START -->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i>
                                    Search
                                </div>
                            </div>
                            <div class="portlet-body">


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{--<label>Type</label>--}}
                                            <input type="checkbox" value="1" name="ff_list_type[]" > ভারতীয় তালিকা <br>
                                            <input type="checkbox" value="25" name="ff_list_type[]" > লাল মুক্তিবার্তা <br>
                                            <input type="checkbox" value="3" name="ff_list_type[]" > বেসামরিক গেজেট <br>
                                            <input type="checkbox" value="5" name="ff_list_type[]" > শহীদ বেসামরিক গেজেট <br>
                                            <input type="checkbox" value="6" name="ff_list_type[]" > সশস্ত্র বাহিনী শহীদ গেজেট <br>
                                            <input type="checkbox" value="7" name="ff_list_type[]" > শহীদ বিজিবি গেজেট <br>
                                            <input type="checkbox" value="8" name="ff_list_type[]" > শহীদ পুলিশ গেজেট <br>
                                            <input type="checkbox" value="9" name="ff_list_type[]" > যুদ্ধাহত গেজেট <br>
                                            <input type="checkbox" value="10" name="ff_list_type[]" > খেতাবপ্রাপ্ত গেজেট <br>
                                            <input type="checkbox" value="11" name="ff_list_type[]" > মুজিবনগর গেজেট <br>
                                            <input type="checkbox" value="12" name="ff_list_type[]" > বিসিএস ধারণাগত জ্যৈষ্ঠতাপ্রাপ্ত কর্মকর্তা গেজেট <br>
                                            <input type="checkbox" value="13" name="ff_list_type[]" > বিসিএস গেজেট <br>
                                            <input type="checkbox" value="14" name="ff_list_type[]" > সেনাবাহিনী গেজেট <br>
                                            <input type="checkbox" value="15" name="ff_list_type[]" > বিমানবাহিনী গেজেট <br>
                                            <input type="checkbox" value="16" name="ff_list_type[]" > নৌবাহিনী গেজেট <br>
                                            <input type="checkbox" value="17" name="ff_list_type[]" > নৌ-কমান্ডো গেজেট <br>
                                            <input type="checkbox" value="18" name="ff_list_type[]" > বিজিবি গেজেট <br>
                                            <input type="checkbox" value="19" name="ff_list_type[]" > পুলিশ বাহিনী গেজেট <br>
                                            <input type="checkbox" value="20" name="ff_list_type[]" > আনসার বাহিনী গেজেট <br>
                                            <input type="checkbox" value="21" name="ff_list_type[]" > স্বাধীন বাংলা বেতার শব্দ সৈনিক গেজেট <br>
                                            <input type="checkbox" value="22" name="ff_list_type[]" > বীরঙ্গনা গেজেট <br>
                                            <input type="checkbox" value="23" name="ff_list_type[]" > স্বাধীন বাংলা ফুটবল দল গেজেট <br>
                                            <input type="checkbox" value="24" name="ff_list_type[]" > ন্যাপ কমিউনিষ্ট পার্টি ছাত্র ইউনিয়ন বিশেষ গেরিলা <br>
                                            <input type="checkbox" value="31" name="ff_list_type[]" > যুদ্ধাহত পঙ্গু (বর্ডারগার্ড বাংলাদেশ) গেজেট <br>
                                            <input type="checkbox" value="32" name="ff_list_type[]" > যুদ্ধাহত (বর্ডারগার্ড বাংলাদেশ) গেজেট <br>
                                            <input type="checkbox" value="33" name="ff_list_type[]" > বিশ্রামগঞ্জ হাসপাতালে নিয়োজিত/দায়িত্বপালনকারী <br>
                                            <input type="checkbox" value="34" name="ff_list_type[]" > যুদ্ধাহত সেনা গেজেট <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success "><i class="fa fa-search "></i> Search</button>
                                        {{--<a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SEARCH END -->
        </div>

    </form>



@endsection