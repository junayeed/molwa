@extends('layouts.master')
@section('content')
    <!-- PERCENTAGE STATS STARTS HERE  -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="font-size: 20px; padding: 5px 20px !important;"> ACTIONS
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="forwardDairySearch" style="font-size: 16px;"><i class="fa fa-print "></i> Print </a>
                                    <a href="forwardDairy/create" style="font-size: 16px;"><i class="fa fa-plus"></i> Add New Case </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="forwardDairySearch" style="font-size: 16px;"><i class="fa fa-search "></i> Search Case </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="forwardDairy" style="font-size: 16px;"><i class="fa fa-backward "></i> Back to Case List </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                </div>
            </div>
        </div>
    </div>
    <!-- PERCENTAGE STATS ENDS HERE  -->

    <!-- WRIT Pritition List STARTS HERE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>{{@$status_list[@$case_status]}}</div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover writ_petition_table" id="tb_id">
                            <thead>
                            <th class="table_heading">Serial</th>
                            <th class="table_heading">Case No</th>
                            <th class="table_heading">Subject</th>
                            <th class="table_heading">Receive Date</th>
                            <th class="table_heading">Issue Memo and Date</th>
                            <th class="table_heading">Hard File Number</th>
                            <th class="table_heading">Days Passed</th>
                            <th class="table_heading">Rule Nisi</th>
                            <th class="table_heading">Interim Order</th>
                            <th class="table_heading text-center no-print">Actions</th>
                            </thead>
                            <tbody>
                            <?php $total = 0; ?>
                            @if ( !empty( $case_list ) )
                                @foreach($case_list as $key => $cases)
                                    <?php $i = 0; ?>
                                    <tr>
                                        <td colspan="8">
                                            <span class="caption-subject font-blue bold uppercase font-lg ">
                                                @if( $key == 6 )  {{--DC office--}}
                                                    {{@$recipient_list[$key]}}, {{@$district_list[$cases[0]->sf_dc_office]}}
                                                @elseif ( $key == 3 )  {{--Ministry--}}
                                                    {{@$recipient_list[$key]}}, {{@$ministry_list[$cases[0]->sf_ministry]}}
                                                @elseif ( $key == 7 )  {{--UNO Office--}}
                                                    {{@$recipient_list[$key]}}, {{@$upzilla_list[$cases[0]->sf_uno_office]}}
                                                @else
                                                    {{@$recipient_list[$key]}}
                                                @endif
                                            </span>
                                        </td>
                                    </tr>
                                    @foreach(@$cases as $case)
                                        {{ $class = '' }}
                                        <tr class="odd gradeX">
                                            <td>{{\Html::en2bn( ++$i )}}</td>
                                            <td class="@if ($case->awaiting_sf_days >= Config::get('constants.ALERT_LEVEL_HIGH') )high_alert
                                                       @elseif ($case->awaiting_sf_days >= Config::get('constants.ALERT_LEVEL_MEDIUM') && $case->awaiting_sf_days < Config::get('constants.ALERT_LEVEL_HIGH') )medium_alert
                                                       @elseif ($case->awaiting_sf_days >= Config::get('constants.ALERT_LEVEL_LOW') && $case->awaiting_sf_days < Config::get('constants.ALERT_LEVEL_MEDIUM') ) low_alert
                                                       @else nothing @endif bold">
                                                {{@$case->case_no}}
                                            </td>
                                            <td>{{@$case->subject_type_bn}}{{--{{str_limit($case->case_subject, $limit = 50, $end = '...')}}--}}</td>
                                            <td>{{\Carbon\Carbon::parse(@$case->case_receive_date)->format('d M, Y')}}</td>
                                            <td>
                                                @if ($case->case_status_id == 1)
                                                    @php ($class = 'bg-red-flamingo')
                                                @elseif ($case->case_status_id == 2)
                                                    @php ($class = 'bg-yellow-gold')
                                                @elseif ($case->case_status_id == 3)
                                                    @php ($class = 'bg-yellow-soft')
                                                @elseif ($case->case_status_id == 4)
                                                    @php ($class = 'bg-green-jungle')
                                                @elseif ($case->case_status_id == 5)
                                                    @php ($class = 'bg-green-meadow')
                                                @endif
                                                <label class="label {{$class}}">{{$case->sf_issue_memo}} ({{\Carbon\Carbon::parse(@$case->sf_issue_date)->format('d/m/Y')}})</label>
                                            </td>
                                            <td>{{$case->hard_file_no}}</td>
                                            <td>{{$case->awaiting_sf_days}}</td>
                                            <td class="text-left">
                                                @if ($case->has_rule_nisi == 1)
                                                    @php ($rule_nisi_class = 'bg-red-flamingo')
                                                    @php ($rule_nisi_text = 'Yes')
                                                @elseif ($case->has_rule_nisi == 0)
                                                    @php ($rule_nisi_class = 'bg-green')
                                                    @php ($rule_nisi_text = 'No')
                                                @endif
                                                <label class="label {{$rule_nisi_class}}">{{@$rule_nisi_text}}</label>
                                            </td>
                                            <td class="text-cleft">
                                                @if ($case->has_interim_order == 1)
                                                    @php ($interim_order_class = 'bg-red-flamingo')
                                                    @php ($interim_order_text = 'Yes')
                                                @elseif ($case->has_interim_order == 0)
                                                    @php ($interim_order_class = 'bg-green')
                                                    @php ($interim_order_text = 'No')
                                                @endif
                                                <label class="label {{$interim_order_class}}">{{@$interim_order_text}}</label>
                                            </td>
                                            <td class="text-center no-print">
                                                <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{@$case->case_id}}/edit"><i class="fa fa-edit"></i></a>
                                                {{--<a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$case->id}}"><i class="fa fa-trash"></i></a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="8" class="text-center bold"><span class=" font-lg "> উপমোট = {{\Html::en2bn( $i )}} </span></td>
                                        <?php $total += $i; ?>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="8" class="text-center bold"><span class=" font-lg "> মোট = {{\Html::en2bn( $total )}}</span></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- WRIT Pritition List ENDS HERE -->

    <script type="text/javascript">
        $(document).ready(function() {

            var empName = '44';

            $('#tb_id td.high_alert').closest('td').addClass('blink_red');
            $('#tb_id td.medium_alert').closest('td').addClass('blink_orange');
            $('#tb_id td.low_alert').closest('td').addClass('blink_yellow');
            //$('#tb_id td.grn:contains("' + empName + '")').closest('tr').addClass('blink');
        } );
    </script>

@endsection