@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="row">
        <div class="col-md-2">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_case']}}">0</span></div>
                    <div class="desc bold"> Total Cases </div>
                </div>
                <a class="more" href="/showCategoryResult">&nbsp; {{--View Details<i class="m-icon-swapright m-icon-white"></i>--}}</a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat red-thunderbird">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_court_contempted']}}">0</span></div>
                    <div class="desc bold">Court Contempt</div>
                </div>
                <a class="more" id="court_contempted_table"> View Details<i class="m-icon-swapright m-icon-white"></i></a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat red-flamingo">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_pending']}}">0</span></div>
                    <div class="desc bold"> Pending at MOLWA </div>
                </div>
                <a class="more" href="/showCategoryResult?case_status=1">{{-- View Details<i class="m-icon-swapright m-icon-white"></i>--}}&nbsp;</a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat yellow-crusta">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_sf_pending']}}">0</span></div>
                    <div class="desc bold"> Awating for SF </div>
                </div>
                @if ( @$data['total_sf_pending'] )
                <a class="more" href="/showCategoryResult?case_status=2"> View Details<i class="m-icon-swapright m-icon-white"></i></a>
                @else
                    <a class="more" href="javascript:void(0);">&nbsp;</a>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat yellow-soft">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_sf_received']}}"></span></div>
                    <div class="desc bold"> SF Received </div>
                </div>
                @if ( @$data['total_sf_received'] )
                 <a class="more" href="/showCategoryResult?case_status=3"> View Details<i class="m-icon-swapright m-icon-white"></i></a>
                @else
                    <a class="more" href="javascript:void(0);">&nbsp;</a>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat green-jungle">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_sf_sent']}}"></span></div>
                    <div class="desc bold"> SF Sent </div>
                </div>
                @if ( @$data['total_sf_sent'] )
                <a class="more" href="/showCategoryResult?case_status=4"> View Details<i class="m-icon-swapright m-icon-white"></i></a>
                @else
                    <a class="more" href="javascript:void(0);">&nbsp;</a>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat green-meadow">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_closed']}}">0</span></div>
                    <div class="desc bold"> Case Closed </div>
                </div>
                <a class="more" href="/showCategoryResult?case_status=5">&nbsp; {{--View Details<i class="m-icon-swapright m-icon-white"></i>--}}</a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="dashboard-stat yellow-saffron">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number"><span data-counter="counterup" data-value="{{@$data['total_corr_ministry']}}">0</span></div>
                    <div class="desc bold"> Sent to Corrosponding Ministry/Division </div>
                </div>
                <a class="more" href="/showCategoryResult?case_status=5">&nbsp; {{--View Details<i class="m-icon-swapright m-icon-white"></i>--}}</a>
            </div>
        </div>
    </div>
    <!-- DATA STATS STARTS HERE  -->

    <!-- PERCENTAGE STATS STARTS HERE  -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cursor font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">General Stats</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="font-size: 20px; padding: 5px 20px !important;"> ACTIONS
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="forwardDairy/create" style="font-size: 16px;"><i class="fa fa-plus"></i> Add New Case </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="forwardDairySearch" style="font-size: 16px;"><i class="fa fa-search "></i> Search Case </a>
                                </li>
                                <li class="divider"> </li>
                                {{--<li>
                                    <a href="javascript:;"> Pending
                                        <span class="badge badge-danger"> 4 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Completed
                                        <span class="badge badge-success"> 12 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Overdue
                                        <span class="badge badge-warning"> 9 </span>
                                    </a>
                                </li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number molwa_pending" data-percent="{{ number_format(Html::divisionNumber($data['total_court_contempted'], @$data['total_case'])*100, 1) }}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_court_contempted'], $data['total_case'])*100, 1) }}</span>%
                                </div>Court Contempted
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number molwa_pending" data-percent="{{ number_format(Html::divisionNumber($data['total_pending'], $data['total_case'])*100, 1) }}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_pending'], $data['total_case'])*100, 1) }}</span>%
                                </div>Pending at MOLWA
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number sf_pending" data-percent="{{ number_format(Html::divisionNumber($data['total_sf_pending'], $data['total_case'])*100, 1) }}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_sf_pending'], $data['total_case'])*100, 1) }}</span>%
                                </div>SF Pending
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number sf_received" data-percent="{{ number_format(Html::divisionNumber($data['total_sf_received'], $data['total_case'])*100, 1) }}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_sf_received'], $data['total_case'])*100, 1) }}</span>%
                                </div>SF Received
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number sf_sent" data-percent="{{ number_format(Html::divisionNumber($data['total_sf_sent'], $data['total_case'])*100, 1) }}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_sf_sent'], $data['total_case'])*100, 1) }}</span>%
                                </div>SF Sent
                            </div>
                        </div>
                        <div class="margin-bottom-10"> </div>
                        <div class="col-md-1 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ number_format(Html::divisionNumber($data['total_int_order'], $data['total_case'])*100, 1)}}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_int_order'], $data['total_case'])*100, 1)}}</span>%
                                </div>Interim Orders
                            </div>
                        </div>
                        <div class="col-md-1 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ number_format(Html::divisionNumber($data['total_rule_nisi'], $data['total_case'])*100, 1)}}">
                                    <span class="bold">{{ number_format(Html::divisionNumber($data['total_rule_nisi'], $data['total_case'])*100, 1)}}</span>%
                                </div>Rule Nisi
                            </div>
                        </div>
                        <div class="col-md-1 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number case_closed" data-percent="{{number_format(Html::divisionNumber($data['total_closed'], $data['total_case'])*100, 1)}}">
                                    <span class="bold">{{number_format(Html::divisionNumber($data['total_closed'], $data['total_case'])*100, 1)}}</span>%
                                </div>Case Closed
                            </div>
                        </div>
                        <div class="col-md-1 margin-bottom-10">
                            <div class="easy-pie-chart">
                                <div class="number case_closed" data-percent="{{number_format(Html::divisionNumber($data['total_corr_ministry'], $data['total_case'])*100, 1)}}">
                                    <span class="bold">{{number_format(Html::divisionNumber($data['total_corr_ministry'], $data['total_case'])*100, 1)}}</span>%
                                </div>Corrosponding Ministry/Division
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PERCENTAGE STATS ENDS HERE  -->

    <!-- Court Contempted List STARTS HERE -->
    <div class="row"id="court_contemptedn_list_table">
        <div class="col-md-12">
            <div class="portlet box red-thunderbird">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Court Contempted List </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <th class="table_heading">Serial</th>
                                <th class="table_heading">Hard File No</th>
                                <th class="table_heading">Contempt No (Case No)</th>
                                <th class="table_heading">Receive Date</th>
                                <th class="table_heading">Subject</th>
                                <th class="table_heading">Status</th>
                                <th class="table_heading">Rule Nisi</th>
                                <th class="table_heading">Interim Order</th>
                                <th class="table_heading">Contempted Received Date</th>
                                <th class="table_heading">Appearance Date</th>
                                <th class="table_heading text-center no-print">Actions</th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if( $court_contempted_list)
                                @foreach($court_contempted_list as $year => $case_list)
                                    <tr class="even gradeX">
                                        <td colspan="10"><span class="caption-subject font-red-thunderbird bold font-hg">{{$year}}</span></td>
                                    </tr>
                                    @foreach($case_list as $case)
                                    {{ $class = '' }}
                                    <tr class="even gradeX">
                                        <td>{{$i++}}</td>
                                        <td>{{$case->hard_file_no}}</td>
                                        <td>{{$case->contempt_no}} ({{$case->case_no}})</td>
                                        <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                                        <td>{{$case->subject_type_bn}}{{--{{str_limit($case->case_subject, $limit = 50, $end = '...')}}--}}</td>
                                        <td>
                                            @if ($case->case_status_id == 1)
                                                @php ($class = 'bg-red-flamingo')
                                            @elseif ($case->case_status_id == 2)
                                                @php ($class = 'bg-yellow-gold')
                                            @elseif ($case->case_status_id == 3)
                                                @php ($class = 'bg-yellow-soft')
                                            @elseif ($case->case_status_id == 4)
                                                @php ($class = 'bg-green-jungle')
                                            @elseif ($case->case_status_id == 5)
                                                @php ($class = 'bg-green-meadow')
                                            @elseif ($case->case_status_id == 6)
                                                @php ($class = 'bg-yellow-saffron')
                                            @endif
                                            <label class="label {{$class}}">{{$case->case_status}}</label>
                                        </td>
                                        <td class="text-left">
                                            @if ($case->has_rule_nisi == 1)
                                                @php ($rule_nisi_class = 'bg-red-flamingo')
                                                @php ($rule_nisi_text = 'Yes')
                                            @elseif ($case->has_rule_nisi == 0)
                                                @php ($rule_nisi_class = 'bg-green')
                                                @php ($rule_nisi_text = 'No')
                                            @endif
                                            <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                                        </td>
                                        <td class="text-cleft">
                                            @if ($case->has_interim_order == 1)
                                                @php ($interim_order_class = 'bg-red-flamingo')
                                                @php ($interim_order_text = 'Yes')
                                            @elseif ($case->has_interim_order == 0)
                                                @php ($interim_order_class = 'bg-green')
                                                @php ($interim_order_text = 'No')
                                            @endif
                                            <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                                        </td>
                                        <td>{{\Carbon\Carbon::parse($case->contempt_received_date)->format('d M, Y')}}</td>
                                        <td>{{\Carbon\Carbon::parse($case->appearance_date)->format('d M, Y')}}</td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$case->id}}/edit"><i class="fa fa-edit"></i></a>
                                            {{--<a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$case->id}}"><i class="fa fa-trash"></i></a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                    @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Court Contempted List ENDS HERE -->

    <!-- WRIT Pritition List STARTS HERE -->
    <div class="row" id="writ_petition_list_table">
        <div class="col-md-12">
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Writ Petition List </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover writ_petition_table">
                            <thead>
                            <th class="table_heading">Serial</th>
                            <th class="table_heading">Hard File No</th>
                            <th class="table_heading">Case No</th>
                            <th class="table_heading">Receive Date</th>
                            <th class="table_heading">Subject</th>
                            <th class="table_heading">Status</th>
                            <th class="table_heading">Rule Nisi</th>
                            <th class="table_heading">Interim Order</th>
                            <th class="table_heading text-center no-print">Actions</th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if ( $writ_petition_list )
                                @foreach($writ_petition_list as $year => $case_list)
                                    <tr class="odd gradeX">
                                        <td  colspan="10"><span class="caption-subject font-purple bold font-hg">{{$year}}</span></td>
                                    </tr>
                                    @foreach($case_list as $case)
                                    {{ $class = '' }}
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td>{{$case->hard_file_no}}</td>
                                        <td>{{$case->case_no}}</td>
                                        <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                                        <td>{{$case->subject_type_bn}}{{--{{str_limit($case->case_subject, $limit = 50, $end = '...')}}--}}</td>
                                        <td>
                                            @if ($case->case_status_id == 1)
                                                @php ($class = 'bg-red-flamingo')
                                            @elseif ($case->case_status_id == 2)
                                                @php ($class = 'bg-yellow-gold')
                                            @elseif ($case->case_status_id == 3)
                                                @php ($class = 'bg-yellow-soft')
                                            @elseif ($case->case_status_id == 4)
                                                @php ($class = 'bg-green-jungle')
                                            @elseif ($case->case_status_id == 5)
                                                @php ($class = 'bg-green-meadow')
                                            @elseif ($case->case_status_id == 6)
                                                @php ($class = 'bg-yellow-saffron')
                                            @endif
                                            <label class="label {{$class}}">{{$case->case_status}}</label>
                                        </td>
                                        <td class="text-left">
                                            @if ($case->has_rule_nisi == 1)
                                                @php ($rule_nisi_class = 'bg-red-flamingo')
                                                @php ($rule_nisi_text = 'Yes')
                                            @elseif ($case->has_rule_nisi == 0)
                                                @php ($rule_nisi_class = 'bg-green')
                                                @php ($rule_nisi_text = 'No')
                                            @endif
                                            <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                                        </td>
                                        <td class="text-cleft">
                                            @if ($case->has_interim_order == 1)
                                                @php ($interim_order_class = 'bg-red-flamingo')
                                                @php ($interim_order_text = 'Yes')
                                            @elseif ($case->has_interim_order == 0)
                                                @php ($interim_order_class = 'bg-green')
                                                @php ($interim_order_text = 'No')
                                            @endif
                                            <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                                        </td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$case->id}}/edit"><i class="fa fa-edit"></i></a>
                                            {{--<a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$case->id}}"><i class="fa fa-trash"></i></a>--}}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- WRIT Pritition List ENDS HERE -->

    <!-- Civil Suits List STARTS HERE -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Civil Suit List ({{count($civil_suit_list)}})</div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover writ_petition_table">
                            <thead>
                            <th class="table_heading">Serial</th>
                            <th class="table_heading">Hard File No</th>
                            <th class="table_heading">Case No</th>
                            <th class="table_heading">Receive Date</th>
                            <th class="table_heading">Subject</th>
                            <th class="table_heading">Status</th>
                            <th class="table_heading">Rule Nisi</th>
                            <th class="table_heading">Interim Order</th>
                            <th class="table_heading text-center no-print">Actions</th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if ( $civil_suit_list )
                                @foreach($civil_suit_list as $case)
                                    {{ $class = '' }}
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td>{{$case->hard_file_no}}</td>
                                        <td>{{$case->case_no}}</td>
                                        <td>{{\Carbon\Carbon::parse($case->case_receive_date)->format('d M, Y')}}</td>
                                        <td>{{$case->subject_type_bn}}{{--{{str_limit($case->case_subject, $limit = 50, $end = '...')}}--}}</td>
                                        <td>
                                            @if ($case->case_status_id == 1)
                                                @php ($class = 'bg-red-flamingo')
                                            @elseif ($case->case_status_id == 2)
                                                @php ($class = 'bg-yellow-gold')
                                            @elseif ($case->case_status_id == 3)
                                                @php ($class = 'bg-yellow-soft')
                                            @elseif ($case->case_status_id == 4)
                                                @php ($class = 'bg-green-jungle')
                                            @elseif ($case->case_status_id == 5)
                                                @php ($class = 'bg-green-meadow')
                                            @endif
                                            <label class="label {{$class}}">{{$case->case_status}}</label>
                                        </td>
                                        <td class="text-left">
                                            @if ($case->has_rule_nisi == 1)
                                                @php ($rule_nisi_class = 'bg-red-flamingo')
                                                @php ($rule_nisi_text = 'Yes')
                                            @elseif ($case->has_rule_nisi == 0)
                                                @php ($rule_nisi_class = 'bg-green')
                                                @php ($rule_nisi_text = 'No')
                                            @endif
                                            <label class="label {{$rule_nisi_class}}">{{$rule_nisi_text}}</label>
                                        </td>
                                        <td class="text-cleft">
                                            @if ($case->has_interim_order == 1)
                                                @php ($interim_order_class = 'bg-red-flamingo')
                                                @php ($interim_order_text = 'Yes')
                                            @elseif ($case->has_interim_order == 0)
                                                @php ($interim_order_class = 'bg-green')
                                                @php ($interim_order_text = 'No')
                                            @endif
                                            <label class="label {{$interim_order_class}}">{{$interim_order_text}}</label>
                                        </td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-xs green-meadow btn-circle " href="/forwardDairy/{{$case->id}}/edit"><i class="fa fa-edit"></i></a>
                                            {{--<a class="btn btn-xs red delete-case btn-circle " href="javascript:void(0);" data-id="{{$case->id}}"><i class="fa fa-trash"></i></a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Civil Suits List ENDS HERE -->

    <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#table_list_view').DataTable({});

            $('.delete-case').on('click', function(){
                var caseID = $(this).data('id');
                var that        = this;

                bootbox.confirm({
                    message: "Are you sure you want to delete this case/pitition? ",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn btn-danger'
                        }
                    },
                    callback: function (result) {
                        if ( result ){
                            $.ajax({
                                type     : "GET",
                                url      : "{{URL::to('/')}}/deleteCase",
                                data     : {caseID: caseID},
                                dataType : 'JSON',
                                success  : function (res) {
                                    $(that).closest('tr').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value ).draw();
                    }
                } );
            } );
        } );

        $(document).on('click', '#court_contempted_table', function() {
            pos = $('#court_contemptedn_list_table').height() ;

            $('html,body').animate({
                scrollTop: 710
            }, 'slow');
        });
    </script>

@endsection