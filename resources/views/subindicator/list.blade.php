@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar m-heading-1 border-red m-bordered">
                <ul class="page-breadcrumb pull-right bold">
                    <li>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Sub Indicator</span>
                    </li>
                </ul>
                <h1 class="page-title bold">List of Sub Indicator(s)</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="actions">
                                <a id="sample_editable_1_new" class="btn green btn-circle btn-add-new" href="/SubIndicator/create"><i class="fa fa-plus"></i> Add Sub Indicator </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                                <thead>
                                    <tr>
                                        <th class="table_heading">Serial</th>
                                        <th class="table_heading">Indicator</th>
                                        <th class="table_heading">Title (EN)</th>
                                        <th class="table_heading">Title (BG)</th>
                                        <th class="table_heading">Amount</th>
                                        <th class="table_heading">Status</th>
                                        <th class="table_heading text-center no-print">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($sub_indicator_list as $sub_indicator)
                                    <tr class="odd gradeX">
                                        <td>{{$sub_indicator->serial}}</td>
                                        <td>{{$sub_indicator->indicator_title}}</td>
                                        <td>{{$sub_indicator->sub_indicator_title_en}}</td>
                                        <td>{{$sub_indicator->sub_indicator_title_bn}}</td>
                                        <td>{{$sub_indicator->amount}}</td>
                                        <td>
                                            @if ($sub_indicator->sub_indicator_status == 1)
                                                <label class="label label-success">Active</label>
                                            @else
                                                <label class="label label-warning">Inactive</label>
                                            @endif
                                        </td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-sm btn-warning btn-circle btn-edit" href="/SubIndicator/{{$sub_indicator->id}}/edit"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-circle btn-danger delete-subindicator" href="javascript:void(0);" data-id="{{$sub_indicator->id}}"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <script type="text/javascript">
        /*$(document).ready(function() {
            $('#table_list_view').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );*/
    </script>

     <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#table_list_view').DataTable({});

            $('.delete-subindicator').on('click', function(){
                var subIndicatorID = $(this).data('id');
                var that        = this;

                bootbox.confirm({
                    message: "Are you sure you want to delete this sub indicator? ",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn btn-danger'
                        }
                    },
                    callback: function (result) {
                        if ( result ){
                            $.ajax({
                                type     : "GET",
                                url      : "{{URL::to('/')}}/deleteSubIndicator",
                                data     : {id: subIndicatorID},
                                dataType : 'JSON',
                                success  : function (res) {
                                    $(that).closest('tr').fadeOut('slow');
                                }
                            });
                        }
                    }
                });
            });
        });
        $(document).ready(function() {            
            $('#table_list_view tfoot th').each( function () {
                var title = $(this).text();
                if(title !== 'Actions')
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );       
            var table = $('#table_list_view').DataTable();       
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                                .search( this.value )
                                .draw();
                    }
                } );
            } );
        } );
    </script>

@endsection