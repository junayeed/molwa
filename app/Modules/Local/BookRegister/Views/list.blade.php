@extends('layouts.master')
@section('content')
    <form method="POST" action="/batch">
    {{csrf_field()}}
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light">
                    <!-- TOP RIGHT MENU STARTS HERE -->
                    <div class="portlet-title">
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                   data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="/Book/add" class="font-lg">
                                            <i class="fa fa-plus font-green-jungle"></i> নতুন বই নিবন্ধন করুন
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/BookDistribution/add" class="font-lg">
                                            <i class="fa fa-plus font-green-jungle"></i> নতুন বই বিতরণ
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- TOP RIGHT MENU ENDS HERE -->
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                        <div id="m_calendar" class="fc fc-unthemed fc-ltr">
                            <div class="fc-view-container" style="">
                                <div class="fc-view fc-listWeek-view fc-list-view fc-widget-content" style="">
                                    <div class="fc-scroller" style="overflow: hidden auto; height: auto;">
                                        <table class="fc-list-table" style="font-size: 1.5em !important;">
                                            <tbody>
                                            @if($books)
                                                @foreach($books as $book)
                                                <tr class="fc-list-heading sub-container">
                                                    <td class="fc-widget-header">
                                                        <a class="fc-list-heading-main exploder"><span class="fa fa-plus"></span> [{{Html::en2bn($book->book_id)}}] {{@$book->book_title_bn}}</a>
                                                        <br>{{$book->writer}}
                                                    </td>
                                                </tr>
                                                <tr class="explode hide">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <table class="table table-condensed table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th><span class="font-lg">তারিখ</span></th>
                                                                            <th><span class="font-lg">জমা</span></th>
                                                                            <th><span class="font-lg">বিতরণ</span></th>
                                                                            <th><span class="font-lg">বিবরণ</span></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @if($book->book_register)
                                                                        <?php $total_distributed = 0; $total_deposited = 0; ?>
                                                                        @foreach($book->book_register as $r)
                                                                        <tr>
                                                                            <td class="font-hg"><span class="font-lg">{{Html::en2bn(\Carbon\Carbon::parse(@$r['created_at'])->format('d/m/Y') )}}</td>
                                                                            <td align="center"><span class="font-green font-lg text-center">{{Html::en2bn($r['book_deposited'])}}</span></td>
                                                                            <td align="center"><span class="font-red-sunglo font-lg text-center">{{Html::en2bn($r['book_distributed'])}}</span></td>
                                                                            <?php $total_deposited += $r['book_deposited']; $total_distributed += $r['book_distributed']; ?>
                                                                            <td>
                                                                                @if ( $r['book_distribution_id'] == 0)
                                                                                    <span class="font-lg">{{$r['details']}}</span>
                                                                                @else
                                                                                    <a class="" href="/BookDistribution/update/{{$r['book_distribution_id']}}"><span class="font-lg">{{$r['details']}}</span> </a>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                        <tr>
                                                                            <td colspan="2" align="right"><span class="font-lg">মোট = {{Html::en2bn($total_deposited-$total_distributed)}}</span></td>
                                                                            <td colspan="2"></td>
                                                                        </tr>
                                                                    @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else
                                                <tr class="fc-list-item m-fc-event--danger">
                                                    <td class="fc-list-item-time fc-widget-content font-lg text-danger ">কোন তথ্য পাওয়া যায়নি</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </form>
    <!-- END CONTENT -->


    <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details"></div>
        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/BookRegister.js" type="text/javascript"></script>
@endsection