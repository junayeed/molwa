<?php
namespace App\Modules\Local\BookRegister\Controllers;
use App\Http\Controllers\Controller;
//use App\Modules\Local\Book\Models\BookModel;
use App\Modules\Local\Book\Models\BookModel;
use App\Modules\Local\BookRegister\Models\BookRegisterModel;
use App\Modules\Local\Employee\Models\EmployeeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

class BookRegisterController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $books                = array();
        //$search_data          = $request->only();
        $search_query         = DB::table('books')->select('books.id AS book_id', 'books.book_title_bn', 'books.writer', 'publishers.publisher_bn')
                                                 ->leftJoin('publishers', 'books.publisher_id', '=', 'publishers.id');
        $publishers_list      = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');

        $books = $search_query->get()->toArray();

        if ( $books) {
            foreach($books as $book) {
                $book->book_register = $this->getBookRegisterDetails($book->book_id);
            }
        }
        //echo "<pre>"; print_r($books); die;
        //dd($books);
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;


        return view('Local/BookRegister::list', ['books' => $books, 'publisher_list' => $publishers_list]);
    }

    public function getBookRegisterDetails($book_id) {
        return BookRegisterModel::select('book_distribution_id', 'book_deposited', 'book_distributed', 'created_at', 'details')->where('book_id', $book_id)->get();
    }

    public function add() {
        $book_data                  = (object)[];
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $book_data->financial_year = \Common::getCurrentFinancialYear();
        //$book_data->batch_no       = BatchModel::where('financial_year', '=', $batch_data->financial_year)->max('batch_no')+1;
        $batch_status_list          = ['Upcoming' => 'আসন্ন', 'In Progress' => 'চলমান', 'Completed' => 'সমাপ্ত'];
        $publishers_list            = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');
        $book_list                  = DB::table('books')->select('id', 'book_title_bn')->get()->pluck('book_title_bn', 'id');
        $language_list              = ['Bangla' => 'বাংলা', 'English' => 'ইংরেজি'];
        $source_list                = ['Purchased' => 'ক্রয়কৃত', 'Donation' => 'দান'];
        $trainer_list               = EmployeeModel::select(DB::raw("CONCAT(employee.name_bn, ', ', D.name_bn) AS display_name"), 'employee.id')
                                                ->leftJoin('employee_profiles as ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                ->leftJoin('designations AS D', 'D.id', '=', 'ep.designation')
                                                ->where('employee_status', '=', 'Active')
                                                ->where('pay_grade', '<', 10)
                                                ->orderBy('D.weight', 'ASC')
                                                ->get()->pluck('display_name', 'id');

        //echo "<pre>"; print_r($book_data); die;
        return view('Local/BookDistribution::form', ['data' => $data, 'batch_status_list' => $batch_status_list, 'publishers_list' => $publishers_list,
                                                'source_list' => $source_list, 'language_list' => $language_list, 'book_list' => $book_list,
                                                'trainer_list' => $trainer_list, 'book_data' => $book_data]);
    }

    public function update($id) {
        $book_data                  = BookDistributionModel::find($id);
        $book_dis_data              = DB::table('book_registry')->select('book_registry.book_id', 'books.book_title_bn', 'books.writer', 'book_registry.book_distributed', 'books.no_copy')
                                        ->leftjoin('books', 'books.id', '=', 'book_registry.book_id')
                                        ->where('book_distribution_id', $id)
                                        ->get();
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        //$auditLogData               = \Common::getAuditLogData('Batch', $id);
        $publishers_list            = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');
        $language_list              = ['Bangla' => 'বাংলা', 'English' => 'ইংরেজি'];
        $source_list                = ['Purchased' => 'ক্রয়কৃত', 'Donation' => 'দান'];
        $book_list                  = DB::table('books')->select('id', 'book_title_bn')->get()->pluck('book_title_bn', 'id');

        //dd($book_dis_data);
        if ( \Request::route()->getName() == "bookdistribution.book_distribution_letter_pdf" ) {
            $book_dis_letter_html =  view('Local/BookDistribution::bookdistribution_letter_pdf', ['book_data' => $book_data, 'book_dis_data' => $book_dis_data/*, 'emp_stats' => $emp_stats[0],
                    'designation_list' => $designation_list->toArray(),
                'emp_class_list' => $emp_class_list,
                'district_list' => $district_list->toArray(),
                'degree_list' => $degree_list->toArray(),
                'country_list' => $country_list,
                'emp_training_history' => $emp_training_history,
                'emp_ft_details' => $emp_ft_details*/]);

            \Common::pdfCreate($book_dis_letter_html, "Book Distribution Letter");
            return;
        }

        return view('Local/BookDistribution::form', ['book_data' => $book_data, 'data' => $data, 'publishers_list' => $publishers_list,
                                                          'language_list' => $language_list, 'source_list' => $source_list, 'book_list' => $book_list,
                                                          'book_dis_data' => $book_dis_data]);
    }

    public function save(Request $request) {
        //dd($request);
        $book_data                           = $request->only(['book_distribution_subject', 'book_distribution_ref', 'book_distribution_date', 'book_distribution_inst_name', 'financial_year', 'created_by',
                                                               'book_receiver_name', 'book_receiver_cell', 'book_distributor_cell', 'book_distributor_name']);
        $book_data['financial_year']         = \Common::getCurrentFinancialYear();
        $book_data['created_by']             = Auth::user()->id;
        $book_data['book_distribution_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $book_data['book_distribution_date']) ));
        $field_list                          = array('book_distribution_subject', 'book_distribution_ref', 'book_distribution_date', 'book_distribution_inst_name',
                                                     'financial_year', 'created_by', 'book_receiver_name', 'book_receiver_cell', 'book_distributor_cell',
                                                     'book_distributor_name');

        if( $request->id ) { // Update
            $book_distribution_id = $request->id;
            $book                 = BookDistributionModel::find($request->id);
            $old_data             = $book->toArray();
            $book->update($book_data);

            \Common::AuditLog('Book', $request->id, $old_data, $book_data, 'Updated', $field_list);
        }
        else { // Create
            $book                  = BookDistributionModel::create($book_data);
            $book_distribution_id  = $book->id;

            \Common::AuditLog('Book Distribution', $book_distribution_id, '', '', 'Create', '');
        }

        $this->saveBookDistributionDetails($request, $book_distribution_id);

        return redirect('/BookDistribution');
    }

    public function saveBookDistributionDetails($book_distribution, $book_distribution_id) {
        DB::table('book_registry')->where('book_distribution_id', $book_distribution_id)->delete();

        if($book_distribution['books_data']) {
            foreach($book_distribution['books_data'] as $book) {
                $book_distribution =  array();
                $book_distribution['book_distribution_id']  = $book_distribution_id;
                $book_distribution['book_id']               = $book['book_id'];
                $book_distribution['book_distributed']      = $book['dist_book_copy'];
                $book_distribution['details']               = "বিতরণকৃত";
                $book_distribution['created_by']            = Auth::user()->id;
                $book_distribution['created_at']            = time();

                DB::table('book_registry')->insert($book_distribution);
            }
        }

    }

    public function getBookDetails(Request $request) {
        $book_id         = $request->input('book_id');

        $book_details = DB::table('books')->select('publishers.publisher_bn', 'books.writer', 'books.no_copy')
                                          ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                                          ->where('books.id', $book_id)
                                          ->get();

        echo json_encode($book_details[0]);
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/Book');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
