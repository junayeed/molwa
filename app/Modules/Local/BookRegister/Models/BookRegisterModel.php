<?php
namespace App\Modules\Local\BookRegister\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class BookRegisterModel extends Model
{
    protected $table = 'book_registry';

    protected $fillable = ['book_id', 'book_distribution_id', 'book_deposited', 'book_distributed', 'details', 'created_by', 'created_at'];
}
