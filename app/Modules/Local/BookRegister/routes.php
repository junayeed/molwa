<?php

Route::group(['prefix' => 'BookRegister', 'namespace' => 'App\Modules\Local\BookRegister\Controllers', 'middleware' => ['web']], function () {
    Route::post('/',                                   ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@index']);
    Route::get('/',                                    ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@index']);
    Route::get('/show/{id}',                           ['as' => 'bookregister.show',   'uses' => 'BookRegisterController@show']);
    Route::get('/add',                                 ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@add']);
    Route::get('/update/{id}',                         ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@update']);
    Route::post('/save',                               ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@save']);
    Route::get('/delete/{id}',                         ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@delete']);
    Route::get('/getBookDetails/',                     ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@getBookDetails']);
    Route::get('/removeBatchParticipant/',             ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@removeBatchParticipant']);
    Route::get('/sendSMS/',                            ['as' => 'bookregister.index',  'uses' => 'BookRegisterController@sendSMS2BatchParticipans']);
});




?>