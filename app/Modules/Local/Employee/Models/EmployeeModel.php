<?php
namespace App\Modules\Local\Employee\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeeModel extends Model
{
    protected $table = 'employee';

    /*protected $fillable = ['name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office',
                           'profile_image', 'employee_class', 'employee_status', 'gender', 'pay_grade'];*/
    protected $fillable = ['emp_profile_id', 'name_en', 'name_bn', 'cadre_no', 'email', 'cellphone', 'profile_image',
                           'employee_status', 'gender', 'dob', 'nid', 'retirement_date', 'prl_date', 'service_joining_date',
                           'molwa_joining_date', 'home_district', 'education', 'room_no', 'intercom', 'telephone'];
}
