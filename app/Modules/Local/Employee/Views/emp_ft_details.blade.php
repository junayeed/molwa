<div class="table-scrollable">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th> স্মারক </th>
            <th class="hidden-xs"> তারিখ </th>
            <th>দেশ</th>
            <th>ভ্রমণ শুরু ও শেষের তারিখ</th>
        </tr>
        </thead>
        <tbody>
        @if ( @$emp_ft_details )
            @foreach(@$emp_ft_details AS $f_year => $value)
                <?php $serial = 1; ?>
                <tr>
                    <td class="highlight" colspan="4" style="font-size: 18px;"><u>অর্থ বছরঃ {{\Html::en2bn($f_year)}}</u></td>
                </tr>
                <tr>
                    <td style="font-size: 14px;"><u>ক্রমিক</u></td>
                    <td style="font-size: 14px;"><u>স্মারক ও তারিখ</u></td>
                    <td style="font-size: 14px;"><u>দেশ</u></td>
                    <td style="font-size: 14px;"><u>ভ্রমণ শুরু ও শেষের তারিখ</u></td>
                </tr>
                @foreach($value AS $key => $emp)
                    <tr>
                        <td width="5%">{{  \Html::en2bn($serial++) }}।</td>
                        <td width="25%">{{ \Html::en2bn( $emp->memo_no ) }},  {{ \Html::en2bn( $emp->memo_date ) }}</td>
                        <td width="30%">
                            @foreach($country_list AS $id => $country)
                                @if( in_array($id, explode(',', $emp->country) ) )
                                    <span class="font-lg">{{$country}}</span>
                                @endif
                            @endforeach
                        </td>
                        <td width="35%">{{ \Html::en2bn( Carbon\Carbon::parse($emp->start_date)->format('d/m/Y' ) ) }} - {{ \Html::en2bn( Carbon\Carbon::parse($emp->end_date)->format('d/m/Y' ) ) }}</td>
                    </tr>
                @endforeach
                <tr><td style="height: 10px;"></td></tr>
            @endforeach
        @else
            <tr>
                <td colspan="4"> দুঃখিত! কোন তথ্য পাওয়া যায় নি।  </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>