<div style="font-size: 24px; text-align: center; padding: 10px; "> {{@$emp_data->name_bn}} </div>
<div style="font-size: 20px; text-align: center;"> {{$designation_list[@$emp_data->designation]}} </div>

<div style="float: right; width: 98%; border: 0px solid yellowgreen;">
    <div style="font-size: 20px; text-align: center; padding: 5px; ">প্রশিক্ষণের তথ্য</div>
    <table style="border: 1px solid #a2a2a2; width: 100%; ">

        @if($emp_training_history)
            @foreach($emp_training_history AS $key => $history)
                <?php $tr_serial = 1; $total_tr_hrs = 0;?>
                <tr>
                    <td colspan="4">অর্থ বছর: {{Html::en2bn($key)}}</td>
                </tr>
                    @foreach($history AS $h)
                        <tr>
                            <td>{{ Html::en2bn($tr_serial++) }}।</td>
                            <td>{{$h->training_title}}</td>
                            <td>
                                {{Html::en2bn( number_format($h->training_hrs, 1))}}ঘন্টা x {{Html::en2bn( number_format($h->training_days, 0))}}দিন = {{Html::en2bn( number_format($h->training_hrs*$h->training_days, 1) )}}
                                <?php $total_tr_hrs += $h->training_hrs*$h->training_days?>
                            </td>
                        </tr>
                @endforeach
                    <tr>
                        <td colspan="2" align="right">মোট = </td>
                        <td align="right">{{ Html::en2bn( number_format(@$total_tr_hrs, 1) )}}</td>
                    </tr>
            @endforeach
        @else
            <tr><td colspan="3"> দুঃখিত! কোন তথ্য পাওয়া যায় নি।  </td></tr>
        @endif
    </table>
</div>

{{--<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

This is text that follows the clear:both.--}}
</div>