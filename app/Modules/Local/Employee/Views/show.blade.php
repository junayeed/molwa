    <table id="table_list_view">
        <thead>
        <tr style="border: 1px solid black;">
            <td>ক্রমিক</td>
            <td>নাম</td>
            <td>পদবি</td>
            <td>দপ্তর</td>
            <td>ইমেইল</td>
            <td>ফোন</td>

        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($emp_data as $emp)
            <tr class="odd gradeX">
                <td>{{$i++}}</td>
                <td>{{$emp->name_bn}}</td>
                <td>{{$emp->designation}}</td>
                <td>{{$emp->office_name_bn}}</td>
                <td>{{$emp->email}}</td>
                <td>{{$emp->cellphone}}</td>

            </tr>
        @endforeach

        </tbody>
    </table>

