<table style="width: 100%;" >
    <thead>
    <tr>
        <td width="5%"  style="font-size: 14px;">ক্রমিক</td>
        <td width="35%" style="font-size: 14px;">নাম ও পদবি</td>
        <td width="25%" style="font-size: 14px;">ইমেইল</td>
        <td width="25%" style="font-size: 14px;">ফোন</td>
        <td width="10%" style="font-size: 14px;">রুম</td>

    </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    @foreach($emp_data as $emp)
        <tr>
            <td style="border-bottom-width: 1px;border-bottom-style: solid; border-bottom-color: red; border-collapse: collapse;">{{\Html::en2bn( $i++ )}}।</td>
            <td>{{$emp->name_bn}}<br>{{$emp->designation}}</td>
            <td>{{$emp->email}}<br></td>
            <td>মোবাইলঃ {{\Html::en2bn( $emp->cellphone )}}<br>ফোনঃ {{\Html::en2bn( $emp->telephone )}}<br>ইন্টারকমঃ {{\Html::en2bn( $emp->intercom )}}</td>
            <td>{{\Html::en2bn( $emp->room_no )}}</td>

        </tr>
    @endforeach

    </tbody>
</table>

