@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="add_employee_form" method="POST" action="/employee/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$emp_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- TOP RIGHT MENU STARTS HERE -->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                               data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0)" class="foreign_training_view font-lg" data-employee_id="{{@$emp_data->id}}" data-toggle="modal" data-target="#foreign_training_modal">
                                        <i class="fa fa-plane font-green-jungle"></i> বৈদেশিক ভ্রমণ/ প্রশিক্ষণ এর তথ্য
                                    </a>
                                </li>
                                <li>
                                    <a href="/employee/employee_ft_pdf/{{@$emp_data->id}}" class="font-lg">
                                        <i class="fa fa-print font-green-jungle"></i> বৈদেশিক ভ্রমণের তথ্য প্রিন্ট করুন
                                    </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="/employee/employee_pdf/{{@$emp_data->id}}" class="font-lg"> <i class="fa fa-file-pdf-o font-green-jungle"></i> প্রোফাইল প্রিন্ট করুন  </a>
                                </li>
                                <li>
                                    <a href="/employee/employee_training_pdf/{{@$emp_data->id}}" class="font-lg"> <i class="fa fa-calendar-check-o font-green-jungle"></i> প্রশিক্ষণের তথ্য প্রিন্ট করুন </a>
                                </li>
                                <li>
                                    <a href="/employee" class="font-lg"> <i class="fa fa-close font-red-flamingo"></i> বন্ধ করুন </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TOP RIGHT MENU ENDS HERE -->

            <!-- EMPLOYEE DETAILS START HERE -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-sidebar">
                            <div class="portlet light bordered">
                                <!-- SIDEBAR USERPIC -->
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 250px; height: 250px;">
                                        @if ( @$emp_data->profile_image )
                                        <img src="/{{@$emp_data->profile_image}}" alt="" />
                                        @else
                                        <img src="./images/no_image.png" alt="" />
                                        @endif
                                    </div>
                                    <div>
                                    <span class="btn red btn-outline btn-file">
                                        <span class="fileinput-new btn btn-success btn-sm"> Select image </span>
                                        <span class="fileinput-exists btn btn-warning  btn-sm"> Change </span>
                                        <input type="file" name="profile_image" class="form-control m-input">
                                    </span>
                                        <a href="javascript:;" class="btn btn-danger btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                <!-- STAT -->
                                <div class="row list-separated profile-stat">
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> {{Html::en2bn(@$emp_stats->financial_year)}} </div>
                                        <div class="uppercase profile-stat-text" style="font-size: 24px !important; font-weight: normal !important; "> অর্থ বছর </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> {{ Html::en2bn( number_format(@$emp_stats->total_training_hrs, 1) )}} </div>
                                        <div class="uppercase profile-stat-text" style="font-size: 24px !important; font-weight: normal !important; "> ঘন্টা </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> {{ Html::en2bn( @$emp_stats->total_training )}} </div>
                                        <div class="uppercase profile-stat-text" style="font-size: 24px !important; font-weight: normal !important; "> ব্যাচ </div>
                                    </div>
                                </div>
                                <div class="row text-left">
                                    <div class="sparkline-chart">
                                        <div class="number" id="sparkline_bar5"></div>
                                        {{--<a class="title" href="javascript:;"> Network
                                            <i class="icon-arrow-right"></i>
                                        </a>--}}
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET MAIN -->
                        </div>
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> কর্মকর্তা / কর্মচারীর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">নাম</label>
                                            <input name="name_bn" id="name_bn" type="text" class="form-control input" value="{{@$emp_data->name_bn}}">

                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">Name</label>
                                            <input name="name_en" id="name_en" type="text" class="form-control input" value="{{@$emp_data->name_en}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">পদবী</label>
                                            <select name="designation" id="designation" class="form-control input" >
                                                <option value=""> - পদবী বাছাই করুন - </option>
                                                @foreach($designation_list as $id => $designation)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->designation) selected @endif>{{$designation}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ক্যাডার নম্বর</label>
                                            <input name="cadre_no" id="cadre_no" type="text" class="form-control input"  value="{{@$emp_data->cadre_no}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">বেতন গ্রেড</label>
                                            <input name="pay_grade" id="pay_grade" type="number" class="form-control input" value="{{@$emp_data->pay_grade}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">শ্রেণী</label>
                                            <select name="employee_class" id="employee_class" class="form-control input" >
                                                <option value=""> - বাছাই করুন - </option>
                                                @foreach($emp_class_list as $id => $emp_class)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->employee_class) selected @endif>{{$emp_class}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">দপ্তর</label>
                                            <select name="office" id="office" class="form-control input" >
                                                <option value=""> - দপ্তর বাছাই করুন - </option>
                                                @foreach($office_list as $id => $office)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->office) selected @endif>{{$office}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">ই-মেইল</label>
                                            <input name="email" id="email" type="text" class="form-control input" value="{{@$emp_data->email}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">মোবাইল নম্বর</label>
                                            <input name="cellphone" id="cellphone" type="text" class="form-control input" value="{{@$emp_data->cellphone}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">জাতীয় পরিচয়পত্র নম্বর</label>
                                            <input name="nid" id="nid" type="text" class="form-control input"  value="{{@$emp_data->nid}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">জন্ম তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control input" name="dob" id="dob"
                                                       value="@if (@$emp_data->dob !='0000-00-00'){{\Carbon\Carbon::parse(@$emp_data->dob)->format('d-m-Y')}} @endif" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 9px 9px 9px 9px !important; border: 1px solid #BEBEBE !important; ">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">"পিআরএল" এর তারিখ</label>
                                            <input name="prl_date" id="prl_date" type="text" class="form-control input"
                                                   value="@if (@$emp_data->prl_date !='0000-00-00'){{\Carbon\Carbon::parse(@$emp_data->prl_date)->format('d-m-Y')}} @endif" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">অবসর গ্রহণের তারিখ</label>
                                            <input name="retirement_date" id="retirement_date" type="text" class="form-control input"
                                                   value="@if (@$emp_data->retirement_date !='0000-00-00'){{\Carbon\Carbon::parse(@$emp_data->retirement_date)->format('d-m-Y')}} @endif" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">চাকুরিতে যোগদানের তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control input" name="service_joining_date" id="service_joining_date"
                                                       value="@if (@$emp_data->service_joining_date !='0000-00-00'){{\Carbon\Carbon::parse(@$emp_data->service_joining_date)->format('d-m-Y')}} @endif" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 10px 9px 9px 9px !important; border: 1px solid #BEBEBE !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">মু্বিমে যোগদানের তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control input" name="molwa_joining_date" id="molwa_joining_date"
                                                       value="@if (@$emp_data->molwa_joining_date !='0000-00-00'){{\Carbon\Carbon::parse(@$emp_data->molwa_joining_date)->format('d-m-Y')}} @endif" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 10px 9px 9px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">লিঙ্গ</label>
                                            <select name="gender" id="gender" class="form-control input" >
                                                <option value=""> - বাছাই করুন - </option>
                                                <option value="Female" @if( "Female" == @$emp_data->gender) selected @endif> মহিলা </option>
                                                <option value="Male" @if( "Male" == @$emp_data->gender) selected @endif> পুরুষ </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">নিজ জেলা</label>
                                            <select name="home_district" id="home_district" class="form-control input" >
                                                <option value=""> - জেলা বাছাই করুন - </option>
                                                @foreach($district_list as $id => $district)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->home_district) selected @endif>{{$district}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">শিক্ষাগত যোগ্যতা</label>
                                            <select name="education" id="education" class="form-control input" >
                                                <option value=""> - বাছাই করুন - </option>
                                                @foreach($degree_list as $id => $degree)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->education) selected @endif>{{$degree}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">রুম নম্বর</label>
                                            <input name="room_no" id="room_no" type="text" class="form-control input" value="{{@$emp_data->room_no}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ইন্টারকম</label>
                                            <input name="intercom" id="intercom" type="number" class="form-control input" value="{{@$emp_data->intercom}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">টেলিফোন</label>
                                            <input name="telephone" id="telephone" type="number" class="form-control input" value="{{@$emp_data->telephone}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">অবস্থা</label>
                                            <select name="employee_status" id="employee_status" class="form-control input" >
                                                <option value=""> - অবস্থা বাছাই করুন - </option>
                                                @foreach($emp_status_list as $id => $emp_status)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->employee_status) selected @endif>{{$emp_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-save btn-success font-hg col-md-3"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END HERE -->

            <!-- RIGHT PANEL START HERE -->
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <!-- TRAINING HISTORY START HERE -->
                        <div class="portlet light bordered" style="padding: 12px 5px 0px !important;">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-edit font-dark"></i>
                                    <span class="caption-subject font-dark bold uppercase" style="font-size: 18px !important;">প্রশিক্ষণ ইতিহাস</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="scroller" style="height: 400px;" data-always-visible="1" data-rail-visible="0">
                                    <ul class="feeds">
                                        @if(@$emp_training_history)
                                            @foreach($emp_training_history AS $key => $history)
                                                <label class="font-lg">অর্থ বছর: {{Html::en2bn($key)}}</label>
                                                @foreach($history AS $h)
                                                    <li>
                                                        <div class="col1">
                                                            <div class="cont">
                                                                <div class="cont-col1">
                                                                    <div class="label label-sm label-info">
                                                                        <i class="fa fa-check "></i>
                                                                    </div>
                                                                </div>
                                                                <div class="cont-col2">
                                                                    <div class="desc">{{$h->training_title}}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col2">
                                                            <div class="date">
                                                                {{Html::en2bn( number_format($h->training_hrs, 1))}}ঘন্টা x {{Html::en2bn( number_format($h->training_days, 1))}}দিন = {{Html::en2bn( number_format($h->training_hrs*$h->training_days, 1) )}}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- TRAINING HISTORY END HERE -->

                        <!-- AUDIT LOG START HERE -->
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i> লগ
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">

                                    <div class="col-md-12" data-scrollable="true" data-height="300" style="height: 300px; overflow: hidden;">
                                        @if ( !empty($audit_log) )
                                            @foreach($audit_log as $al)
                                                @if ($al->action == 'Create')
                                                    @php ($class = 'info')
                                                @elseif ($al->action == 'Updated')
                                                    @php ($class = 'warning')
                                                @endif

                                                <div class="m-timeline-3__items">
                                                    <div class="m-timeline-3__item m-timeline-3__item--{{$class}}">
                                                        <span class="m-timeline-3__item-time text-center text-{{$class}} " style="font-size: 0.95rem; width:4.5rem;">{{\Carbon\Carbon::parse(@$al->created_at)->format('d-m-Y h:i a')}}</span>
                                                        <div class="m-timeline-3__item-desc">
                                                            @if ($al->action == 'Create')
                                                                <span class="m-timeline-3__item-text ">
                                                        <span class="text-info">New Batch created </span>
                                                    </span>
                                                                <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link">By User</a>
                                                    </span>
                                                            @elseif ($al->action == 'Updated')
                                                                <span class="m-timeline-3__item-text ">
                                                        Value of <span class="text-info">{{$al->field_updated}}</span> has been updated from

                                                        <span class="text-danger">{{@$al->previous_value}}</span><br> to
                                                        <span class="text-success">{{@$al->current_value}}</span>
                                                    </span>
                                                                <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link">By User</a>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- TRAINING HISTORY END HERE -->
                    </div>
                </div>
            </div>
            <!-- RIGHT PANEL END HERE-->
        </div>
        <!-- FOREIGN TRAINING MODAL START HERE -->
        <div class="modal fade" id="foreign_training_modal" tabindex="-1" role="dialog" aria-labelledby="foreignTrainingModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">বৈদেশিক প্রশিক্ষণের বিস্তারিত তথ্য </h4>
                    </div>
                    <div class="modal-body" id="foreign_training_details">  </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FOREIGN TRAINING MODAL END HERE -->
    </form>

    <!-- END CONTENT -->

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/metronic/demo/default/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{{url(asset("/theme"))}}/metronic/demo/default/custom/crud/forms/widgets/nouislider.js" type="text/javascript"></script>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/employee.js" type="text/javascript"></script>
@endsection

    <script>
        var months_val = [];
        @if(@$emp_training_history)
            @foreach($emp_training_history AS $key => $history)
                @if ($key == $financial_year)
                    @foreach($history AS $h)
                        @if (@$h->training_hrs*@$h->training_days)
                            months_val.push({{ (@$h->training_hrs*@$h->training_days) }});
                        @else
                            months_val.push(10);
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
    </script>
