<div style="font-size: 24px; text-align: center; padding: 10px; "> {{@$emp_data->name_bn}} </div>
<div style="font-size: 20px; text-align: center;"> {{$designation_list[@$emp_data->designation]}} </div>
<p></p>
<div style="float: left; width: 20%; border: 0px solid red;">
    <table>
        <tbody>
            <tr>
                <td><img src="{{@$emp_data->profile_image}}" width="110" ></td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table style="border: 1px solid #a2a2a2; width: 100%; ">
        <tr>
            <td style="border-bottom: 1px solid #a2a2a2;" width="60%"> অর্থ বছর</td>
            <td style="border-bottom: 1px solid #a2a2a2;" width="40%"> জনঘন্টা </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #F2F5F8;">২০২০-২০২১</td>
            <td style="border-bottom: 1px solid #F2F5F8;">{{ Html::en2bn( number_format(@$emp_stats->total_training_hrs, 1) )}}</td>
        </tr>
        <tr><td style="height: 50px;"></td></tr>
        <tr>
            <td style="border-bottom: 1px solid #F2F5F8;">রুম নম্বর</td>
            <td style="border-bottom: 1px solid #F2F5F8;">{{ Html::en2bn( @$emp_data->room_no )}}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #F2F5F8;">ইন্টারকম</td>
            <td style="border-bottom: 1px solid #F2F5F8;">{{ Html::en2bn( @$emp_data->intercom )}}</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #F2F5F8;">টেলিফোন</td>
            <td style="border-bottom: 1px solid #F2F5F8;">{{ Html::en2bn( @$emp_data->telephone )}}</td>
        </tr>
    </table>
</div>
<div style="float: right; width: 79%; border: 0px solid yellowgreen;">
        <div style="font-size: 20px; text-align: center; padding: 5px; ">প্রোফাইল তথ্য</div>
        <table style="border: 1px solid #a2a2a2; width: 100%; ">
            <tr>
                <td colspan="2"><u>নাম (বাংলা)</u></td>
                <td colspan="2"><u>Name</u></td>
            </tr>
            <tr>
                <td colspan="2">{{@$emp_data->name_bn}}</td>
                <td colspan="2">{{@$emp_data->name_en}}</td>
            </tr>
            <tr><td style="height: 10px;"></td></tr>
            <tr>
                <td><u>পদবী</u></td>
                <td><u>ক্যাডার নম্বর</u></td>
                <td><u>বেতন গ্রেড</u></td>
                <td><u>শ্রেণী</u></td>
            </tr>
            <tr>
                <td>{{$designation_list[@$emp_data->designation]}}</td>
                <td>{{ \Html::en2bn(@$emp_data->cadre_no) }}</td>
                <td>{{ \Html::en2bn(@$emp_data->pay_grade) }}</td>
                <td>{{$emp_class_list[@$emp_data->employee_class]}}</td>
            </tr>
            <tr><td style="height: 10px;"></td></tr>
            <tr>
                <td colspan="2"><u>ই-মেইল</u></td>
                <td><u>মোবাইল নম্বর</u></td>
                <td><u>জাতীয় পরিচয়পত্র নম্বর</u></td>
            </tr>
            <tr>
                <td colspan="2">{{@$emp_data->email}}</td>
                <td>{{ \Html::en2bn(@$emp_data->cellphone) }}</td>
                <td>{{ \Html::en2bn(@$emp_data->nid) }}</td>
            </tr>
            <tr><td style="height: 10px;"></td></tr>
            <tr>
                <td><u>জন্ম তারিখ</u></td>
                <td><u>"পিআরএল" এর তারিখ</u></td>
                <td><u>অবসর গ্রহণের তারিখ</u></td>
                <td></td>
            </tr>
            <tr>
                <td>{{ \Html::en2bn( \Carbon\Carbon::parse(@$emp_data->dob)->format('d-m-Y' ) ) }}</td>
                <td>{{ \Html::en2bn( \Carbon\Carbon::parse(@$emp_data->prl_date)->format('d-m-Y' ) ) }}</td>
                <td>{{ \Html::en2bn( \Carbon\Carbon::parse(@$emp_data->retirement_date)->format('d-m-Y' ) ) }}</td>
                <td></td>
            </tr>
            <tr><td style="height: 10px;"></td></tr>
            <tr>
                <td><u>চাকুরিতে যোগদানের তারিখ</u></td>
                <td><u>মু্বিমে যোগদানের তারিখ</u></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>{{ \Html::en2bn( \Carbon\Carbon::parse(@$emp_data->service_joining_date)->format('d-m-Y' ) ) }}</td>
                <td>{{ \Html::en2bn( \Carbon\Carbon::parse(@$emp_data->molwa_joining_date)->format('d-m-Y' ) ) }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr><td style="height: 10px;"></td></tr>
            <tr>
                <td><u>লিঙ্গ</u></td>
                <td><u>নিজ জেলা</u></td>
                <td><u>শিক্ষাগত যোগ্যতা</u></td>
                <td></td>
            </tr>
            <tr>
                <td>{{ @$emp_data->gender }}</td>
                <td>{{ @$district_list[@$emp_data->home_district] }}</td>
                <td>{{ @$degree_list[@$emp_data->education] }}</td>
                <td></td>
            </tr>
        </table>
        <p></p>
        <div style="font-size: 20px; text-align: center; padding: 5px; ">প্রশিক্ষণের তথ্য</div>
        <table style="border: 1px solid #a2a2a2; width: 100%; ">
            <?php $tr_serial = 1; $total_tr_hrs = 0;?>
            @if($emp_training_history)
                @foreach($emp_training_history AS $key => $history)
                    <?php $tr_serial = 1; $total_tr_hrs = 0;?>
                    <tr>
                        <td colspan="4">অর্থ বছর: {{Html::en2bn($key)}}</td>
                    </tr>
                    @foreach($history AS $h)
                        <tr>
                            <td>{{ Html::en2bn($tr_serial++) }}।</td>
                            <td>{{$h->training_title}}</td>
                            <td>
                                {{Html::en2bn( number_format($h->training_hrs, 1))}}ঘন্টা x {{Html::en2bn( number_format($h->training_days, 0))}}দিন = {{Html::en2bn( number_format($h->training_hrs*$h->training_days, 1) )}}
                                <?php $total_tr_hrs += $h->training_hrs*$h->training_days?>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2" align="right">মোট = </td>
                        <td align="right">{{ Html::en2bn( number_format(@$total_tr_hrs, 1) )}}</td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="3"> দুঃখিত! কোন তথ্য পাওয়া যায় নি।  </td></tr>
            @endif
        </table>

        <p></p>
        <div style="font-size: 20px; text-align: center; padding: 5px; "> বৈদেশিক ভ্রমণের তথ্য </div>
        <table style="border: 1px solid #a2a2a2; width: 100%; ">
            @if ( @$emp_ft_details )
                @foreach(@$emp_ft_details AS $f_year => $value)
                    <?php $serial = 1; ?>
                    <tr>
                        <td class="highlight" colspan="4" style="font-size: 18px;"><u>অর্থ বছরঃ {{\Html::en2bn($f_year)}}</u></td>
                    </tr>
                    <tr>
                        <td style="font-size: 14px;"><u>ক্রমিক</u></td>
                        <td style="font-size: 14px;"><u>স্মারক ও তারিখ</u></td>
                        <td style="font-size: 14px;"><u>দেশ</u></td>
                        <td style="font-size: 14px;"><u>ভ্রমণ শুরু ও শেষের তারিখ</u></td>
                    </tr>
                    @foreach($value AS $key => $emp)
                        <tr>
                            <td width="5%">{{  \Html::en2bn($serial++) }}।
                            <td width="25%">{{ \Html::en2bn( $emp->memo_no ) }},  {{ \Html::en2bn( $emp->memo_date ) }}</td>
                            <td width="30%">
                                @foreach($country_list AS $id => $country)
                                    @if( in_array($id, explode(',', $emp->country) ) )
                                        <span class="font-lg">{{$country}}</span>
                                    @endif
                                @endforeach
                            </td>
                            <td width="35%">{{ \Html::en2bn( Carbon\Carbon::parse($emp->start_date)->format('d/m/Y' ) ) }} - {{ \Html::en2bn( Carbon\Carbon::parse($emp->end_date)->format('d/m/Y' ) ) }}</td>
                        </tr>
                    @endforeach
                        <tr><td style="height: 10px;"></td></tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4"> দুঃখিত! কোন তথ্য পাওয়া যায় নি।  </td>
                </tr>
            @endif
        </table>
    </div>

    {{--<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

    This is text that follows the clear:both.--}}
</div>