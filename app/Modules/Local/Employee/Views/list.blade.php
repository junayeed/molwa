@extends('layouts.master')
@section('content')
    <!-- STATS STARTS HERE  -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cursor font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">তথ্য / উপাত্ত</span>
                    </div>
                    <div class="actions">
                        {{--<a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                            <i class="fa fa-repeat"></i> Reload </a>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number molwa_pending" data-percent="{{ count(@$emp_data) }}">
                                    <span class="bold">{{ \Html::en2bn( count(@$emp_data) ) }}</span>&nbsp;
                                </div>কর্মকর্তা / কর্মচারী
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number female " data-percent="{{ @$emp_stats['Female'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_stats['Female'] ) }}</span>&nbsp;
                                </div> মহিলা
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number male " data-percent="{{ @$emp_stats['Male'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_stats['Male'] ) }}</span>&nbsp;
                                </div> পুরুষ
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ @$emp_class_stats['1'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_class_stats['1'] ) }}</span>
                                </div>১ম শ্রেণী
                            </div>
                        </div><div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ @$emp_class_stats['2'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_class_stats['2'] ) }}</span>
                                </div>২য় শ্রেণী
                            </div>
                        </div><div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number interim_order" data-percent="{{ @$emp_class_stats['3'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_class_stats['3'] ) }}</span>
                                </div>৩য় শ্রেণী
                            </div>
                        </div><div class="col-md-1">
                            <div class="easy-pie-chart">
                                <div class="number case_closed" data-percent="{{ @$emp_class_stats['4'] }}">
                                    <span class="bold">{{ \Html::en2bn( @$emp_class_stats['4'] ) }}</span>
                                </div>৪র্থ শ্রেণী
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- STATS ENDS HERE  -->

    <!-- SEARCH FILTERS STARTS HERE  -->
    <div class="row">
        <form method="POST" action="/employee">
            {{csrf_field()}}
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-search font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">অনুসন্ধান</span>
                    </div>
                    <div class="actions">
                        <button type="submit" class="btn btn-sm btn-circle red">
                            <i class="fa fa-search"></i> অনুসন্ধান করুন
                        </button>
                        {{--<input type="submit" class="btn btn-sm btn-circle red" name="employeePDFBtn" value="pdf">--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>পদবী</label>
                            <select name="designation" id="designation" class="form-control input" >
                                <option value=""> - পদবী বাছাই করুন - </option>
                                @foreach($designation_list as $id => $designation)
                                    <option value="{{$id}}" @if( $id == @$search_data['designation']) selected @endif>{{$designation}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>দপ্তর</label>
                            <select name="office" id="office" class="form-control input" >
                                <option value=""> - দপ্তর বাছাই করুন - </option>
                                @foreach($office_list as $id => $office)
                                    <option value="{{$id}}" @if( $id == @$search_data['office']) selected @endif>{{$office}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>লিঙ্গ</label>
                            <select name="gender" id="gender" class="form-control input" >
                                <option value=""> - বাছাই করুন - </option>
                                <option value="Female" @if( "Female" == @$search_data['gender']) selected @endif> মহিলা </option>
                                <option value="Male" @if( "Male" == @$search_data['gender']) selected @endif> পুরুষ </option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>শ্রেণী</label>
                            <select name="employee_class" id="employee_class" class="form-control input" >
                                <option value=""> - বাছাই করুন - </option>
                                @foreach($emp_class_list as $id => $emp_class)
                                    <option value="{{$id}}" @if( $id == @$search_data['employee_class']) selected @endif>{{$emp_class}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>অবস্থা</label>
                            <select name="employee_status" id="employee_status" class="form-control input" >
                                <option value=""> - অবস্থা বাছাই করুন - </option>
                                @foreach($emp_status_list as $id => $emp_status)
                                    <option value="{{$id}}" @if( $id == @$search_data['employee_status']) selected @endif>{{$emp_status}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!-- SEARCH FILTERS ENDS HERE  -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">

                            <div class="actions">
                                <div class="btn-group">
                                    <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                       data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a id="sample_editable_1_new" href="/employee/add" class="font-lg"><i class="fa fa-plus font-green-jungle"></i> নতুন কর্মকর্তা / কর্মচারী </a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            {{--<input type="submit" class="btn btn-sm btn-circle red" name="employeePDFBtn" value="pdf">--}}
                                        </li>
                                        <li>
                                            <a href="/employee?employeePDFBtn=1" class="font-lg"> <i class="fa fa-file-excel-o font-green-jungle"></i> কর্মকর্তা/কর্মচারী বিস্তারিত প্রতিবেদন </a>
                                        </li>
                                        <li>
                                            <a href="/employee?employeeContactBtn=1" class="font-lg"> <i class="fa fa-clone font-green-jungle"></i> কর্মকর্তা যোগাযোগ</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="/employee" class="font-lg"> <i class="fa fa-list font-green-jungle"></i> তালিকা ভিউ </a>
                                        </li>
                                        <li>
                                            <a href="/employee" class="font-lg"> <i class="fa fa-th font-green-jungle"></i> অফিস ভিউ </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body" id="list_view">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                                <thead>
                                <tr>
                                    <th class="table_heading">ক্রমিক</th>
                                    <th class="table_heading">ছবি</th>
                                    <th class="table_heading">নাম</th>
                                    <th class="table_heading">পদবি</th>
                                    <th class="table_heading">দপ্তর</th>
                                    <th class="table_heading">ইমেইল</th>
                                    <th class="table_heading">ফোন</th>
                                    <th class="table_heading text-center no-print">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($emp_data as $emp)
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td><img src="{{$emp->profile_image}}" alt="" height="50"></td>
                                        <td>{{$emp->name_bn}}</td>
                                        <td>{{$emp->designation}}</td>
                                        <td>{{$emp->office_name_bn}}</td>
                                        <td>{{$emp->email}}</td>
                                        <td>{{$emp->cellphone}}</td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-circle btn-warning btn-sm " href="/employee/update/{{$emp->id}}"><i class="fa fa-edit"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection