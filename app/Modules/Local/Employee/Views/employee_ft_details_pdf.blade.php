<div style="font-size: 24px; text-align: center; padding: 10px; "> {{@$emp_data->name_bn}} </div>
<div style="font-size: 20px; text-align: center;"> {{$designation_list[@$emp_data->designation]}} </div>
<p></p>

<div style="float: right; width: 95%; border: 0px solid yellowgreen;">
        <div style="font-size: 20px; text-align: center; padding: 5px; "> বৈদেশিক ভ্রমণের তথ্য </div>
        <table style="border: 1px solid #a2a2a2; width: 100%; ">
            @foreach(@$emp_ft_details AS $f_year => $value)
                <?php $serial = 1; ?>
                <tr>
                    <td class="highlight" colspan="4" style="font-size: 18px;"><u>অর্থ বছরঃ {{\Html::en2bn($f_year)}}</u></td>
                </tr>
                <tr>
                    <td style="font-size: 14px;"><u>ক্রমিক</u></td>
                    <td style="font-size: 14px;"><u>স্মারক ও তারিখ</u></td>
                    <td style="font-size: 14px;"><u>দেশ</u></td>
                    <td style="font-size: 14px;"><u>ভ্রমণ শুরু ও শেষের তারিখ</u></td>
                </tr>
                @foreach($value AS $key => $emp)
                    <tr>
                        <td width="5%">{{  \Html::en2bn($serial++) }}।</td>
                        <td width="25%">{{ \Html::en2bn( $emp->memo_no ) }},  {{ \Html::en2bn( $emp->memo_date ) }}</td>
                        <td width="30%">
                            @foreach($country_list AS $id => $country)
                                @if( in_array($id, explode(',', $emp->country) ) )
                                    <span class="font-lg">{{$country}}</span>
                                @endif
                            @endforeach
                        </td>
                        <td width="35%">{{ \Html::en2bn( Carbon\Carbon::parse($emp->start_date)->format('d/m/Y' ) ) }} - {{ \Html::en2bn( Carbon\Carbon::parse($emp->end_date)->format('d/m/Y' ) ) }}</td>
                    </tr>
                @endforeach
                    <tr><td style="height: 10px;"></td></tr>
            @endforeach
        </table>
    </div>

    {{--<div style="clear: both; margin: 0pt; padding: 0pt; "></div>

    This is text that follows the clear:both.--}}
</div>