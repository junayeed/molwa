<?php
namespace App\Modules\Local\Employee\Controllers;

use App\Modules\Local\Employee\Models\EmployeeModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Modules\Local\Designation\Models\DesignationModel;
use App\Models\EmployeeProfile;
use App\Models\Office;

class EmployeeController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $search_data                = $request->only(['designation', 'office', 'gender', 'employee_class', 'employee_status']);
        $emp_query                  = EmployeeModel::select('employee.id AS id', 'employee.name_bn', 'employee.email',
                                                                     'employee.cellphone', 'ep.office', 'designations.name_bn AS designation',
                                                                     'offices.office_name_bn', 'employee.profile_image', 'employee.name_en',
                                                                     'employee.telephone', 'employee.intercom', 'employee.room_no')
                                                   ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                   ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
                                                   ->leftJoin('offices', 'offices.id', '=', 'ep.office')
                                                   ->where(['employee_status' => 'Active'])
                                                   ->orderBy('ep.designation', 'ASC');

        if ( isset($search_data['designation']) && !empty($search_data['designation']) )          $emp_query->where('ep.designation', '=', $search_data['designation']);
        if ( isset($search_data['office']) && !empty($search_data['office']) )                    $emp_query->where('ep.office', '=', $search_data['office']);
        if ( isset($search_data['employee_class']) && !empty($search_data['employee_class']) )    $emp_query->where('ep.employee_class', '=', $search_data['employee_class']);
        if ( isset($search_data['employee_status']) && !empty($search_data['employee_status']) )  $emp_query->where('employee_status', '=', $search_data['employee_status']);
        if ( isset($search_data['gender']) && !empty($search_data['gender']) )                    $emp_query->where('gender', '=', $search_data['gender']);


        $emp_data                   = $emp_query->get()->all();
        $emp_stats                  = EmployeeModel::select(DB::raw('COUNT(gender) AS total_gender'), 'gender')
                                                   ->where(['employee_status' => 'Active'])
                                                   ->groupBy('gender')
                                                   ->get()->pluck('total_gender', 'gender');
        $emp_class_stats            = EmployeeModel::select(DB::raw('COUNT(ep.employee_class) AS total_emp_class'), 'ep.employee_class')
                                                   ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                   ->where(['employee_status' => 'Active'])
                                                   ->groupBy('ep.employee_class')
                                                   ->get()->pluck('total_emp_class', 'employee_class');
        $designation_list           = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list             = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list            = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        //echo "<pre>"; print_r($emp_data); die;

        if ( $request->has('employeePDFBtn') ){

            $html =  view('Local/Employee::show', ['emp_data' => $emp_data, 'office_list' => $office_list, 'emp_class_list' => $emp_class_list,
                                       'emp_status_list' => $emp_status_list]);
            \Common::pdfCreate($html, "Employee List"); return;
        }
        else if ( $request->has('employeeContactBtn') ){

            $html =  view('Local/Employee::employee_contact_pdf', ['emp_data' => $emp_data, 'office_list' => $office_list, 'emp_class_list' => $emp_class_list,
                'emp_status_list' => $emp_status_list]);
            \Common::pdfCreate($html, "Employee Contact List"); return;
        }

        //echo "<pre>"; print_r($emp_class_stats); die;

        return view('Local/Employee::list', ['emp_data' => $emp_data, 'data' => $data, 'emp_stats' => $emp_stats,
                                                   'emp_class_stats' => $emp_class_stats, 'designation_list' => $designation_list,
                                                   'emp_status_list' => $emp_status_list, 'office_list' => $office_list,
                                                   'emp_class_list' => $emp_class_list, 'search_data' => $search_data,
                                                   'emp_training_history' => '']);
    }

    function add() {

        $designation_list           = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list             = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list            = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];
        $district_list              = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        $degree_list                = DB::table('education')->select('id', 'name')->pluck('name', 'id');

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Employee::form', ['data' => $data, 'designation_list' => $designation_list, 'emp_status_list' => $emp_status_list,
                                                   'office_list' => $office_list, 'emp_class_list' => $emp_class_list, 'district_list' => $district_list,
                                                  'degree_list' => $degree_list]);
    }

    function update($id) {
        $emp_training_history         = array();
        $financial_year               = \Common::getCurrentFinancialYear();
        $emp_data                     = EmployeeModel::select('employee.*', 'ep.office', 'ep.designation', 'ep.employee_class', 'ep.pay_grade')
                                                     ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                     ->where(['employee.id' => $id])
                                                     ->get()->first();
        $emp_stats                    = DB::table('batches AS B')
                                         ->select(DB::raw('COUNT(BP.batch_id) AS total_training'), 'B.financial_year',
                                                  DB::raw('SUM( (TIME_TO_SEC(TIMEDIFF(batch_end_time, batch_start_time))/3600*(TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1) ) ) AS total_training_hrs'),
                                                  DB::raw('TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1 AS training_days'))
                                         ->leftJoin('batch_participants AS BP', 'BP.batch_id', '=', 'B.id')
                                         ->where(['batch_status' => 'Completed', 'BP.employee_id' => $emp_data->id, 'financial_year' => $financial_year])
                                         ->get();
        /*$emp_month_stats              = DB::table('batches AS B')
                                          ->select(DB::raw('COUNT(BP.batch_id) AS total_training'), DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(batch_end_time, batch_start_time))/3600) AS total_training_hrs'))
                                          ->leftJoin('batch_participants AS BP', 'BP.batch_id', '=', 'B.id')
                                          ->where(['batch_status' => 'Completed', 'BP.employee_id' => $emp_data->id, 'financial_year' => $financial_year])
                                          ->get();*/
        $training_history             = EmployeeModel::select('B.batch_no', 'B.training_title', DB::raw('DATE_FORMAT(batch_start_date, "%M") AS training_month'),
                                                               DB::raw('TIME_TO_SEC(TIMEDIFF(batch_end_time, batch_start_time))/3600 AS training_hrs'),
                                                               DB::raw('TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1 AS training_days'),
                                                               'batch_start_date', 'batch_end_date', 'financial_year')
                                                     ->leftJoin('batch_participants AS BP', 'BP.employee_id', '=', 'employee.id')
                                                     ->leftJoin('batches AS B', 'B.id', '=', 'BP.batch_id')
                                                     ->where(['employee.id' => $id, 'B.batch_status' => 'Completed'])
                                                     ->orderBy('financial_year', 'DESC')
                                                     ->orderByRaw("FIELD(training_month, 'July', 'August', 'September', 'October', 'November', 'December', 'January', 'February', 'March', 'April', 'May', 'June')")
                                                     ->get();

        if ( $training_history ) {
            foreach($training_history AS $tr) {
                $emp_training_history[$tr->financial_year][] = $tr;
            }
        }
        //echo "<pre>"; print_r($emp_training_history); die;
        $auditLogData                 = \Common::getAuditLogData('Employee', $id);

        $action                       = app('request')->route()->getAction();

        $controller                   = class_basename($action['controller']);
        list($controller, $action)    = explode('@', $controller);
        $designation_list             = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                  = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list               = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list              = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];
        $district_list                = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        $degree_list                  = DB::table('education')->select('id', 'name')->pluck('name', 'id');
        $country_list                 = DB::table('country')->select('id', 'nicename_bn')->pluck('nicename_bn', 'id');
        $data['nav_item']             = str_replace("Controller", "", $controller);
        $data['nav_sub_item']         = $action;
        $emp_ft_details               = $this->getEmployeeForeignTrainingDetails($id, 'array');

        if ( \Request::route()->getName() == "employee.employee_pdf" ) {
            $employee_details_html =  view('Local/Employee::employee_details_pdf', ['emp_data' => $emp_data, 'emp_stats' => $emp_stats[0],
                                                                                         'designation_list' => $designation_list->toArray(),
                                                                                         'emp_class_list' => $emp_class_list,
                                                                                         'district_list' => $district_list->toArray(),
                                                                                         'degree_list' => $degree_list->toArray(),
                                                                                         'country_list' => $country_list,
                                                                                         'emp_training_history' => $emp_training_history,
                                                                                         'emp_ft_details' => $emp_ft_details]);

            \Common::pdfCreate($employee_details_html, "Employee Details");
            return;
        }
        else if ( \Request::route()->getName() == "employee.employee_ft_pdf" ) {
            $employee_ft_details_html =  view('Local/Employee::employee_ft_details_pdf', ['emp_data' => $emp_data,
                                                                                               'designation_list' => $designation_list->toArray(),
                                                                                               'country_list' => $country_list,
                                                                                               'emp_ft_details' => $emp_ft_details]);

            \Common::pdfCreate($employee_ft_details_html, "Employee Foreign Details");
            return;
        }
        else if ( \Request::route()->getName() == "employee.employee_training_pdf" ) {
            $employee_ft_details_html =  view('Local/Employee::employee_training_details_pdf', ['emp_data' => $emp_data,
                                                                                                     'designation_list' => $designation_list->toArray(),
                                                                                                     'emp_training_history' => $emp_training_history]);

            \Common::pdfCreate($employee_ft_details_html, "Employee Training Details");
            return;
        }

        return view('Local/Employee::form', ['emp_data' => $emp_data, 'data' => $data, 'designation_list' => $designation_list,
                                                  'office_list' => $office_list, 'emp_class_list' => $emp_class_list, 'audit_log' => $auditLogData,
                                                  'emp_status_list' => $emp_status_list, 'emp_stats' => $emp_stats[0], 'emp_training_history' => $emp_training_history,
                                                  'district_list' => $district_list, 'degree_list' => $degree_list, 'financial_year' => $financial_year]);
    }

    function save(Request $request) {
        $employee_data          = $request->only(['emp_profile_id', 'name_en', 'name_bn', 'cadre_no', 'email', 'cellphone',
                                                  'profile_image', 'employee_status', 'gender', 'dob', 'nid', 'retirement_date',
                                                  'prl_date', 'service_joining_date', 'molwa_joining_date', 'home_district',
                                                  'education', 'room_no', 'intercom', 'telephone']);
        $employee_profile_data  = $request->only(['designation', 'office', 'employee_class', 'pay_grade']);
        $field_list             = array('name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office',
                                        'employee_class', 'employee_status', 'gender', 'pay_grade', 'dob', 'nid', 'retirement_date',
                                        'prl_date', 'service_joining_date', 'molwa_joining_date', 'home_district', 'education', 'room_no', 'intercom', 'telephone');
        //echo "<pre>"; print_r($employee_data);die;
        $employee_data['dob']                   = date_format(date_create($employee_data['dob']), 'Y-m-d');
        $employee_data['retirement_date']       = date_format(date_create($employee_data['retirement_date']), 'Y-m-d');
        $employee_data['prl_date']              = date_format(date_create($employee_data['prl_date']), 'Y-m-d');
        $employee_data['service_joining_date']  = date_format(date_create($employee_data['service_joining_date']), 'Y-m-d');
        $employee_data['molwa_joining_date']    = date_format(date_create($employee_data['molwa_joining_date']), 'Y-m-d');

        if( $request->id ) {
            $employee      = EmployeeModel::find($request->id); // get employee data from Employee Table
            $employee_data['emp_profile_id'] = $this->checkEmployeeProfileData($employee->emp_profile_id, $employee_profile_data);
            if ( empty($employee_data['emp_profile_id']) ) {
                $employee_data['emp_profile_id'] = $employee->emp_profile_id;
            }

            if ( !$request->hasFile('profile_image') ) {
                $employee_data['profile_image'] = $employee->profile_image;
            }

            $old_data      = $employee->toArray();
            $employee->update($employee_data);
            $this->uploadImage($request, $request->id);

            \Common::AuditLog('Employee', $request->id, $old_data, $employee_data, 'Updated', $field_list);
        }
        else {
            $employee_data['emp_profile_id'] = $this->saveEmployeeProfileData($employee_profile_data);
            $employee_id                     = EmployeeModel::create($employee_data)->id;

            $this->uploadImage($request, $employee_id);
            \Common::getAuditLogData('Employee', $employee_id, '', '', 'Create', '');
        }

        return redirect('/employee');
    }

    public function checkEmployeeProfileData($emp_profile_id, $employee_profile_data){
        DB::enableQueryLog();
        $employee_profile = EmployeeProfile::select('designation', 'office', 'employee_class', 'pay_grade')->where(['id' => $emp_profile_id, 'status' => 'Active'])->get()->toArray();

        //dd(DB::getQueryLog());
        //echo "<pre>"; print_r($employee_profile); print_r($employee_profile_data);die;
        $r = array_diff($employee_profile[0], $employee_profile_data);

        if ( !empty($r) ) { // if any difference found create a new row in employee profile table
            EmployeeProfile::whereId($emp_profile_id)->update(['status' => 'Inactive']);

            return EmployeeProfile::create($employee_profile_data)->id;
        }
        else {
            //EmployeeProfile::update(['status' => 'Inactive'], []);
        }
    }

    public function saveEmployeeProfileData($employee_profile_data) {
        return EmployeeProfile::create($employee_profile_data)->id;
    }

    /**
     * @param $request
     * @param $trainee_id
     * @param $division_id
     * @return string
     */
    public function uploadImage($request, $employee_id){
        if ($request->hasFile('profile_image')) {
            if ($request->file('profile_image')->isValid()) {

                $file        = $request->file('profile_image');
                $image_name  = $employee_id . '.' . $file->getClientOriginalExtension();
                $image_path  = "images/profile_picture/" . $employee_id . "/";
                $path        = public_path("images/profile_picture/" . $employee_id . "/");
                $request->file('profile_image')->move($path, $image_name);

                $employee    = EmployeeModel::find($employee_id);

                $employee->update(['profile_image' => $image_path . $image_name]);
            }
        }
    }

    public function getEmployeeForeignTrainingDetails($employee_id, $return_type = '') {
        //$employee_id    = $request->input('employee_id');
        //DB::enableQueryLog();
        $emp_ft_details = array();
        $emp_ft_data    = DB::table('foreign_training_emp AS FTE')
                            ->select('memo_no', 'memo_date', 'country', 'FY.financial_year', 'start_date', 'end_date')
                            ->leftJoin('foreign_trainings AS FT', 'FT.id', '=', 'FTE.ft_id')
                            ->leftJoin('financial_years AS FY', 'FY.id', '=', 'FT.financial_year')
                            ->where('FTE.employee_id', $employee_id)
                            ->orderBy('FT.id', 'DESC')
                            ->get()->all();
        $country_list       = DB::table('country')->select('id', 'nicename_bn')->pluck('nicename_bn', 'id');
        $designation_list   = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $emp_data           = EmployeeModel::find($employee_id);
        //dd(DB::getQueryLog());

        if ( !empty($emp_ft_data) ) {
            foreach($emp_ft_data AS $emp) {
                $emp_ft_details[$emp->financial_year][] = $emp;
            }
        }

        if ( $return_type == 'array') {
            return @$emp_ft_details;
        }

        return view('Local/Employee::emp_ft_details', ['emp_ft_details' => @$emp_ft_details, 'country_list' => $country_list,
                                                            'designation_list' => $designation_list, 'emp_data' => $emp_data]);
    }
}
