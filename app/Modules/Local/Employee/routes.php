<?php

Route::group(['prefix' => 'employee', 'namespace' => 'App\Modules\Local\Employee\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',                                     ['as' => 'employee.index',                  'uses' => 'EmployeeController@index']);
    Route::post('/',                                    ['as' => 'employee.index',                  'uses' => 'EmployeeController@index']);
    Route::get('/show/{id}',                            ['as' => 'employee.show',                   'uses' => 'EmployeeController@show']);
    Route::get('/add',                                  ['as' => 'employee.index',                  'uses' => 'EmployeeController@add']);
    Route::get('/update/{id}',                          ['as' => 'employee.index',                  'uses' => 'EmployeeController@update']);
    Route::get('/employee_pdf/{id}',                    ['as' => 'employee.employee_pdf',           'uses' => 'EmployeeController@update']);
    Route::get('/employee_ft_pdf/{id}',                 ['as' => 'employee.employee_ft_pdf',        'uses' => 'EmployeeController@update']);
    Route::get('/employee_training_pdf/{id}',           ['as' => 'employee.employee_training_pdf',  'uses' => 'EmployeeController@update']);
    Route::post('/save',                                ['as' => 'employee.index',                  'uses' => 'EmployeeController@save']);
    Route::get('/delete/{id}',                          ['as' => 'employee.index',                  'uses' => 'EmployeeController@delete']);
    Route::get('/getEmployeeForeignTrainingDetails/{emp_id}',   ['as' => 'employee.index',          'uses' => 'EmployeeController@getEmployeeForeignTrainingDetails']);
});
?>