<?php
namespace App\Modules\Local\HatBazarReport\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class HatBazarReportModel extends Model
{
    protected $table = 'hat_bazar';

    protected $fillable = ['office_type', 'division', 'district', 'upazilla', 'bengali_year', 'cheque_amount', 'cheque_number',
        'cheque_date', 'cheque_bank', 'deposite_ac_number', 'deposite_date', 'deposite_book_no', 'deposite_inst_no'];
}
