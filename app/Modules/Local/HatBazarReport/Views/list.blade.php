@extends('layouts.master')
@section('content')
    <!-- PERCENTAGE STATS STARTS HERE  -->
    {{--<div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cursor font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">General Stats</span>
                    </div>
                    <div class="actions">
                        --}}{{--<a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                            <i class="fa fa-repeat"></i> Reload </a>--}}{{--
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">

                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!-- PERCENTAGE STATS ENDS HERE  -->
    <!-- PERCENTAGE STATS STARTS HERE  -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cursor font-purple"></i>
                        <span class="caption-subject font-purple bold uppercase">অনুসন্ধান</span>
                    </div>
                    <div class="actions">
                        {{--<a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                            <i class="fa fa-repeat"></i> Reload </a>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label class="font-lg">অফিসের ধরন</label>
                            <select name="office_type" id="office_type" class="form-control input" >
                                <option value=""> - অফিস বাছাই করুন - </option>
                                @foreach($office_list as $id => $office)
                                    <option value="{{$id}}" {{--@if( $id == @$hb_data->office_type) selected @endif--}}>{{$office}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2" id="division_div">
                            <label class="font-lg">বিভাগ</label>
                            <select name="division" id="division" class="form-control input" >
                                <option value=""> - বিভাগ বাছাই করুন - </option>
                                @foreach($division_list as $id => $division)
                                    <option value="{{$id}}" {{--@if( $id == @$hb_data->division) selected @endif--}}>{{$division}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2" id="district_div">
                            <label class="font-lg">জেলা</label>
                            <select name="district" id="district" class="form-control input" >
                                <option value=""> - জেলা বাছাই করুন - </option>
                                @foreach($district_list as $id => $district)
                                    <option value="{{$id}}" {{--@if( $id == @$hb_data->division) selected @endif--}}>{{$district}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2" id="upazilla_div">
                            <label class="font-lg">উপজেলা</label>
                            <select name="upazilla" id="upazilla" class="form-control input" >
                                <option value=""> - উপজেলা বাছাই করুন - </option>
                                @foreach($upazila_list as $id => $up)
                                    <option value="{{$id}}" {{--@if( $id == @$hb_data->division) selected @endif--}}>{{$up}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PERCENTAGE STATS ENDS HERE  -->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">

                    <div class="actions">
                        {{--<a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/employee/add"><i class="fa fa-plus"></i> নতুন কর্মকর্তা / কর্মচারী </a>--}}
                        {{--<a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/createUNO"> Create UNO <i class="fa fa-plus"></i></a>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                        <thead>
                        <tr>
                            <th class="table_heading text-center">ক্রমিক</th>
                            <th class="table_heading text-center">অফিসের ধরন</th>
                            @foreach($bengali_year_list as $yr)
                                <th class="table_heading text-center">{{$yr}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($hb_data as $hb_key => $hb)
                            <tr class="odd gradeX">
                                <td>{{Html::en2bn($i++)}}</td>
                                <td>{{$hb_key}}</td>
                                @foreach($bengali_year_list as $yr)
                                    @if( isset($hb[$yr]) )
                                    <td class="text-right"><span class="font-lg">{{ Html::en2bn( number_format($hb[$yr], 1) ) }}</span></td>
                                    @else <td class="text-right bg-red-flamingo-opacity "><span class="font-lg">০.০</span></td>
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>


    <!-- END CONTENT -->
@endsection