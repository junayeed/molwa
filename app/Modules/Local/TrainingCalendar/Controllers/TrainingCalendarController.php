<?php
namespace App\Modules\Local\TrainingCalendar\Controllers;

use App\Modules\Local\Employee\Models\EmployeeModel;
use App\Modules\Local\Batch\Models\BatchModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Modules\Local\Designation\Models\DesignationModel;
use App\Models\Office;

class TrainingCalendarController extends Controller
{
    function __construct(){
	}

    function index() {
        $training_data    = BatchModel::select('id', 'training_title', 'batch_start_date', 'batch_end_date',
                                               'batch_start_time', 'batch_status')
                                      ->get()->all();


        return view('Local/TrainingCalendar::list', ['training_data' => $training_data]);
    }

    function add() {

        $designation_list           = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list             = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list              = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Employee::form', ['data' => $data, 'designation_list' => $designation_list, 'emp_status_list' => $emp_status_list,
                                                   'office_list' => $office_list, 'emp_class_list' => $emp_class_list]);
    }

    function update($id) {
        $financial_year               = \Common::getCurrentFinancialYear();
        $emp_data                     = EmployeeModel::find($id);
        $emp_stats                    = DB::table('batches AS B')
                                         ->select(DB::raw('COUNT(BP.batch_id) AS total_training'), DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(batch_end_time, batch_start_time))/3600) AS total_training_hrs'))
                                         ->leftJoin('batch_participants AS BP', 'BP.batch_id', '=', 'B.id')
                                         ->where(['batch_status' => 'Completed', 'BP.employee_id' => $emp_data->id, 'financial_year' => $financial_year])
                                         ->get();

        $auditLogData                 = \Common::getAuditLogData('Batch', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        list($controller, $action)    = explode('@', $controller);
        $designation_list             = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                  = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list               = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list              = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];
        $data['nav_item']             = str_replace("Controller", "", $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Employee::form', ['emp_data' => $emp_data, 'data' => $data, 'designation_list' => $designation_list,
                                                   'office_list' => $office_list, 'emp_class_list' => $emp_class_list, 'audit_log' => $auditLogData,
                                                   'emp_status_list' => $emp_status_list, 'emp_stats' => $emp_stats[0]]);
    }

    function save(Request $request) {
        $employee_data     = $request->only(['name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office', 'employee_class', 'employee_status', 'gender', 'pay_grade']);
        $field_list        = array('name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office', 'employee_class', 'employee_status', 'gender', 'pay_grade');

        if( $request->id ) {
            $employee      = EmployeeModel::find($request->id);
            $old_data      = $employee->toArray();
            $employee->update($employee_data);
            $this->uploadImage($request, $request->id);

            \Common::AuditLog('Employee', $request->id, $old_data, $employee_data, 'Updated', $field_list);
        }
        else {
            $employee_id = EmployeeModel::create($employee_data)->id;
            $this->uploadImage($request, $employee_id);
            \Common::getAuditLogData('Employee', $employee_id, '', '', 'Create', '');
        }

        return redirect('/employee');
    }

    /**
     * @param $request
     * @param $trainee_id
     * @param $division_id
     * @return string
     */
    public function uploadImage($request, $employee_id){
        if ($request->hasFile('profile_image')) {
            if ($request->file('profile_image')->isValid()) {

                $file        = $request->file('profile_image');
                $image_name  = $employee_id . '.' . $file->getClientOriginalExtension();
                $image_path  = "images/profile_picture/" . $employee_id . "/";
                $path        = public_path("images/profile_picture/" . $employee_id . "/");
                $request->file('profile_image')->move($path, $image_name);

                $employee    = EmployeeModel::find($employee_id);

                $employee->update(['profile_image' => $image_path . $image_name]);

                //return $image_path . $image_name;
            }
        }
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/batch');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
