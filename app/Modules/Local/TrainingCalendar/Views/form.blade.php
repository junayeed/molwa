@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="add_employee_form" method="POST" action="/employee/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$emp_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-sidebar">
                            <div class="portlet light bordered">
                                <!-- SIDEBAR USERPIC -->
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 250px; height: 250px;">
                                        @if ( @$emp_data->profile_image )
                                        <img src="/{{@$emp_data->profile_image}}" alt="" />
                                        @else
                                        <img src="./images/no_image.png" alt="" />
                                        @endif
                                    </div>
                                    <div>
                                    <span class="btn red btn-outline btn-file">
                                        <span class="fileinput-new btn btn-success btn-sm"> Select image </span>
                                        <span class="fileinput-exists btn btn-warning  btn-sm"> Change </span>
                                        <input type="file" name="profile_image" class="form-control m-input">
                                    </span>
                                        <a href="javascript:;" class="btn btn-danger btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                                <!-- STAT -->
                                <div class="row list-separated profile-stat">
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> {{ Html::en2bn( number_format(@$emp_stats->total_training_hrs, 1) )}} </div>
                                        <div class="uppercase profile-stat-text" style="font-size: 18px !important; font-weight: normal !important; "> ঘন্টা </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> {{ Html::en2bn( @$emp_stats->total_training )}} </div>
                                        <div class="uppercase profile-stat-text" style="font-size: 18px !important; font-weight: normal !important; "> সেশন </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> 61 </div>
                                        <div class="uppercase profile-stat-text"> Uploads </div>
                                    </div>
                                </div>

                            </div>
                            <!-- END PORTLET MAIN -->
                        </div>
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> কর্মকর্তা / কর্মচারীর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>নাম</label>
                                            <input name="name_bn" id="name_bn" type="text" class="form-control input" value="{{@$emp_data->name_bn}}">

                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Name</label>
                                            <input name="name_en" id="name_en" type="text" class="form-control input" value="{{@$emp_data->name_en}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>ক্যাডার নম্বর</label>
                                            <input name="cadre_no" id="cadre_no" type="text" class="form-control input"  value="{{@$emp_data->cadre_no}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>শ্রেণী</label>
                                            <select name="employee_class" id="employee_class" class="form-control input" >
                                                <option value=""> - বাছাই করুন - </option>
                                                @foreach($emp_class_list as $id => $emp_class)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->employee_class) selected @endif>{{$emp_class}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>পদবী</label>
                                            <select name="designation" id="designation" class="form-control input" >
                                                <option value=""> - পদবী বাছাই করুন - </option>
                                                @foreach($designation_list as $id => $designation)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->designation) selected @endif>{{$designation}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>দপ্তর</label>
                                            <select name="office" id="office" class="form-control input" >
                                                <option value=""> - দপ্তর বাছাই করুন - </option>
                                                @foreach($office_list as $id => $office)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->office) selected @endif>{{$office}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>ই-মেইল</label>
                                            <input name="email" id="email" type="text" class="form-control input" value="{{@$emp_data->email}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>মোবাইল নম্বর</label>
                                            <input name="cellphone" id="cellphone" type="text" class="form-control input" value="{{@$emp_data->cellphone}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>লিঙ্গ</label>
                                            <select name="gender" id="gender" class="form-control input" >
                                                <option value=""> - বাছাই করুন - </option>
                                                <option value="Female" @if( "Female" == @$emp_data->gender) selected @endif> মহিলা </option>
                                                <option value="Male" @if( "Male" == @$emp_data->gender) selected @endif> পুরুষ </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>বেতন গ্রেড</label>
                                            <input name="pay_grade" id="pay_grade" type="number" class="form-control input" value="{{@$emp_data->pay_grade}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>অবস্থা</label>
                                            <select name="employee_status" id="employee_status" class="form-control input" >
                                                <option value=""> - অবস্থা বাছাই করুন - </option>
                                                @foreach($emp_status_list as $id => $emp_status)
                                                    <option value="{{$id}}" @if( $id == @$emp_data->employee_status) selected @endif>{{$emp_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i> লগ
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AUDIT LOG END -->
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                        <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>

                    </div>
                </div>

            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/metronic/demo/default/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{{url(asset("/theme"))}}/metronic/demo/default/custom/crud/forms/widgets/nouislider.js" type="text/javascript"></script>

@endsection

@section('page_js')
    <script src="{{url(asset("/js"))}}/batch.js" type="text/javascript"></script>

@endsection