@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light calendar bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <i class="icon-calendar font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">প্রশিক্ষণ ক্যালেন্ডার</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="training_calendar"> </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@endsection

<script>
    var training_data = [];
    var bg_color;
    @foreach($training_data as $tr)
        @if ($tr->batch_status == "In Progress")
            bg_color = "#34bfa3";
        @elseif ($tr->batch_status == "Completed")
            bg_color = "#f4516c";
        @elseif ($tr->batch_status == "Upcoming")
            bg_color = '#adb5ca';
        @endif
    var params = {title: "{{$tr->training_title}}", backgroundColor: bg_color, start: "{{$tr->batch_start_date}}", end: "{{$tr->batch_end_date}}", url: "/batch/update/{{$tr->id}}"};

    training_data.push(params);
    @endforeach
</script>

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/TrainingCalendar.js" type="text/javascript"></script>
@endsection


