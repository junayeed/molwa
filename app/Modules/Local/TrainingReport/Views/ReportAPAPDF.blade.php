<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td style="border-top: 1px solid black; " align="center">মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়</td>
    </tr>
    <tr>
        <td style="border-top: 1px solid black;" align="center">এপিএ ভিত্তিক প্রতিবেদন</td>
    </tr>
    <tr>
        <td style="border-top: 1px solid black;" align="center">সময়কালঃ {{Html::en2bn( @$financial_year ) }}</td>
    </tr>
</table>
<p></p>


<!-- 2.2.1 starts here -->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
        <tr>
            <td colspan="6" style="padding-bottom: 11px;">[২.২] মন্ত্রণালয়ের সকল প্রকল্প সুষ্ঠুভাবে বাস্তবায়নের লক্ষ্যে 'প্রকল্প ব্যবস্থাপনা ও ই-জিপি ' সংক্রান্ত প্রশিক্ষিত জনবল প্রস্তুতকরণ।</td>
        </tr>
        <tr>
            <td colspan="6" style="padding-bottom: 11px; padding-left: 11px;">[২.২.১] প্রকল্প ব্যবস্থাপনা সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল</td>
        </tr>
        <tr>
            <td colspan="6" style="padding-bottom: 11px; padding-left: 11px;">লক্ষ্যমাত্রা (সংখ্যা (জন)): ৩০ জন</td>
        </tr>

    </thead>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
    <tr>
        <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
        <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
        <td width="30%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
        <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
        <td width="15%" style="border: 1px solid black;" align="center">সময়</td>
        <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা</td>
    </tr>
    </thead>
    <tbody>
        <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
        @if ( isset($report_data['2.2.1']) )
            @foreach($report_data['2.2.1'] as $data)
                <?php $flag = false; $monthly_total = 0; ?>
                @foreach($data as $d)
                    <tr class="odd gradeX">
                        @if ( !$flag )
                            <td rowspan="{{count($data)}}" style="font-size: 21px ;position: relative; right: 1px ; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center ;">
                                {{Html::dateEN2BN($d->training_month)}}
                            </td>
                            <?php $flag = true; ?>
                        @endif
                        <td class="text-center" style="font-size: 14px;"  align="center">{{Html::en2bn($d->batch_no)}}</td>
                        <td style="font-size: 17px;">{{$d->training_title}}</td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                            ({{Html::en2bn( $d->total_training_days) }} দিন)
                        </td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                        </td>
                        <td style="font-size: 17px ;"  align="center">
                            <?php $monthly_total += $d->batch_size; ?>
                            {{Html::en2bn($d->batch_size)}}
                        </td>
                    </tr>
                @endforeach
                @php
                    @$grand_total_2_2_1 += @$monthly_total;
                @endphp
                <tr>
                    <td colspan="5" style="text-align: right ; font-size: 21px;"  align="right">{{Html::dateEN2BN($d->training_month)}} = </td>
                    <td style="font-size: 21px;" align="center">{{Html::en2bn( number_format($monthly_total) )}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5" style="text-align: right ;font-size: 24px;" align="right"> মোট অর্জন = </td>
                <td style="font-size: 22px;" align="center">
                    {{Html::en2bn( number_format(@$grand_total_2_2_1) )}}
                </td>
            </tr>
        @endif
    </tbody>
</table>
<!-- 2.2.1 ends here -->
<p></p>
<p></p>
<!-- 2.2.2 starts here -->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
    <tr>
        <td colspan="6" style="padding-bottom: 11px;">[২.২] মন্ত্রণালয়ের সকল প্রকল্প সুষ্ঠুভাবে বাস্তবায়নের লক্ষ্যে 'প্রকল্প ব্যবস্থাপনা ও ই-জিপি ' সংক্রান্ত প্রশিক্ষিত জনবল প্রস্তুতকরণ।</td>
    </tr>
    <tr>
        <td colspan="6" style="padding-bottom: 11px; padding-left: 11px;">[২.২.২] ই-জিপি সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল</td>
    </tr>
    <tr>
        <td colspan="6" style="padding-bottom: 11px; padding-left: 11px;">লক্ষ্যমাত্রা (সংখ্যা (জন)): ১৫ জন</td>
    </tr>
    </thead>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
        <tr>
            <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
            <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
            <td width="30%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
            <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
            <td width="15%" style="border: 1px solid black;" align="center">সময়</td>
            <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা</td>
        </tr>
    </thead>
    <tbody>
    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
    @if ( isset($report_data['2.2.2']) )
        @foreach($report_data['2.2.2'] as $data)
            <?php $flag = false; $monthly_total = 0; ?>
            @foreach($data as $d)
                <tr class="odd gradeX">
                    @if ( !$flag )
                        <td rowspan="{{count($data)}}" style="font-size: 21px ;position: relative; right: 1px ; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center ;">
                            {{Html::dateEN2BN($d->training_month)}}
                        </td>
                        <?php $flag = true; ?>
                    @endif
                    <td class="text-center" style="font-size: 14px;"align="center">{{Html::en2bn($d->batch_no)}}</td>
                    <td style="font-size: 17px ;">{{$d->training_title}}</td>
                    <td style="font-size: 17px ;">
                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                    </td>
                    <td style="font-size: 17px ;">
                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                    </td>
                    <td style="font-size: 17px ;"align="center">
                        <?php $monthly_total += $d->batch_size; ?>
                        {{Html::en2bn($d->batch_size)}}
                    </td>

                </tr>
            @endforeach
            @php
                @$grand_total_2_2_2 += @$monthly_total;
            @endphp
            <tr>
                <td colspan="5" style="text-align: right ;font-size: 21px ;"align="right">{{Html::dateEN2BN($d->training_month)}} = </td>
                <td style="font-size: 21px ;"align="center">{{Html::en2bn( number_format($monthly_total) )}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" style="text-align: right ;font-size: 24px ;"align="right"> মোট অর্জন = </td>
            <td style="font-size: 22px ;" align="center">
                {{Html::en2bn( number_format(@$grand_total_2_2_2) )}}
            </td>
        </tr>
    @endif
    </tbody>
</table>
<!-- 2.2.2 ends here -->

<p></p><p></p>
<!-- 2.5.1 START HERE-->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
    <tr>
        <td colspan="7" style="padding-bottom: 11px;">[২.৫] ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">[২.৫.১] ১ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">সংখ্যা (জন): ৬০</td>
    </tr>
    </thead>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
    <tr>
        <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
        <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
        <td width="30%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
        <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
        <td width="15%" style="border: 1px solid black;" align="center">সময়</td>
        <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা</td>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; $flag = false; $grand_total_2_5_1 = 0; $temp_grand_total = 0;?>
    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
    @if ( isset($report_data['2.5.1']) )
        @foreach($report_data['2.5.1'] as $data)
            <?php $flag = false; $monthly_total = 0; ?>
            @foreach($data as $d)
                <tr class="odd gradeX">
                    @if ( !$flag )
                        <td rowspan="{{count($data)}}" style="font-size: 20px; position: relative; right: 0px; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center;">
                            {{Html::dateEN2BN($d->training_month)}}
                        </td>
                        <?php $flag = true; ?>
                    @endif
                    <td class="text-center" style="font-size: 14px;">{{Html::en2bn($d->batch_no)}}</td>
                    <td style="font-size: 17px;">{{$d->training_title}}</td>
                    <td style="font-size: 17px;">
                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                    </td>
                    <td style="font-size: 17px;">
                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                    </td>
                    <td style="font-size: 17px;">
                        <?php $monthly_total += $d->batch_size; ?>
                        {{Html::en2bn($d->batch_size)}}
                    </td>

                </tr>
            @endforeach
            @php
                @$grand_total_2_5_1 += @$monthly_total;
            @endphp
            <tr>
                <td colspan="5" style="text-align: right;font-size: 20px;">{{Html::dateEN2BN($d->training_month)}} = </td>
                <td style="font-size: 20px;">{{Html::en2bn( number_format($monthly_total) )}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" style="text-align: right;font-size: 24px;"> সর্বমোট = </td>
            <td style="font-size: 22px;">
                {{Html::en2bn( number_format(@$grand_total_2_5_1) )}}
            </td>
        </tr>
    @endif
    <tr>
        <td colspan="5" style="text-align: right ;font-size: 24px ;" align="right"> মোট অর্জন = </td>
        <td style="font-size: 22px ;" align="center">
            {{Html::en2bn( number_format($grand_total_2_5_1) )}}
        </td>
    </tr>
    </tbody>
</table>
<!-- 2.5.1 END HERE-->

<p></p><p></p>
<!-- 2.5.2 START HERE-->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
        <tr>
            <td colspan="7" style="padding-bottom: 11px;">[২.৫] ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</td>
        </tr>
        <tr>
            <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">[২.৫.২] ১০ম গ্রেড ও তদুর্ধ্ব প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ</td>
        </tr>
        <tr>
            <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">লক্ষ্যমাত্রা (জনঘন্টা): ২০</td>
        </tr>
    </thead>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
        <tr>
            <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
            <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
            <td width="25%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
            <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
            <td width="15%" style="border: 1px solid black;" align="center">সময়</td>
            <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা (১ম গ্রেড - ১০ম গ্রেড)</td>
            <td width="10%" style="border: 1px solid black;" align="center">জনঘন্টা</td>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; $flag = false; $grand_total_2_5_2 = 0; $temp_grand_total = 0;?>
        @if ( isset($report_data['2.5.2']) )
            @foreach($report_data['2.5.2'] as $data)
                <?php $flag = false; $monthly_total = 0; ?>
                @foreach($data as $d)
                    <tr class="odd gradeX">
                        @if ( !$flag )
                            <td rowspan="{{count($data)}}" style="font-size: 21px ;position: relative; right: 1px ; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center ;">
                                {{Html::dateEN2BN($d->training_month)}}
                            </td>
                            <?php $flag = true; ?>
                        @endif
                        <td class="text-center" style="font-size: 14px ;" align="center">{{Html::en2bn($d->batch_no)}}</td>
                        <td style="font-size: 17px ;">{{$d->training_title}}</td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                            ({{Html::en2bn( $d->total_training_days) }} দিন)
                        </td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                        </td>
                        <td style="font-size: 17px ;">{{Html::en2bn($d->batch_participants_1_10)}} ({{Html::en2bn($d->batch_size)}})</td>
                        <td style="font-size: 17px ;">
                            ({{Html::en2bn($d->total_training_hrs)}} x {{Html::en2bn($d->total_training_days)}} x {{Html::en2bn($d->batch_participants_1_10)}}) / {{Html::en2bn($d->total_employee_1_10)}} =
                            @php
                                $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_1_10) / $d->total_employee_1_10;
                            @endphp
                            {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_1_10) / $d->total_employee_1_10, 2) ) }}
                        </td>
                    </tr>
                @endforeach
                @php
                    $grand_total_2_5_2 += $monthly_total;
                @endphp
                <tr>
                    <td colspan="6" style="text-align: right ;font-size: 21px ;" align="right">{{Html::dateEN2BN($d->training_month)}} = </td>
                    <td style="font-size: 21px ;" align="center">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td colspan="6" style="text-align: right ;font-size: 24px ;" align="right"> মোট অর্জন = </td>
            <td style="font-size: 22px ;" align="center">
                {{Html::en2bn( number_format($grand_total_2_5_2, 2) )}}
            </td>
        </tr>
    </tbody>
</table>
<!-- 2.5.2 END HERE-->
<p></p><p></p>
<!-- 2.5.3 START HERE-->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
    <tr>
        <td colspan="7" style="padding-bottom: 11px;">[২.৫] ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">[২.৫.৩] ১১তম - ২০তম গ্রেডের প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">লক্ষ্যমাত্রা (জনঘন্টা): ২০</td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
        <tr>
            <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
            <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
            <td width="25%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
            <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
            <td width="15%" style="border: 1px solid black;" align="center">সময়</td>
            <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা (১১ম গ্রেড - ২০ম গ্রেড)</td>
            <td width="10%" style="border: 1px solid black;" align="center">জনঘন্টা</td>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; $flag = false; $grand_total_2_5_3 = 0; $temp_grand_total = 0;?>
        @if ( isset($report_data['2.5.2']) )
            @foreach($report_data['2.5.2'] as $data)
                <?php $flag = false; $monthly_total = 0; ?>
                @foreach($data as $d)

                    <tr class="odd gradeX">
                        @if ( !$flag )
                            <td rowspan="{{count($data)}}" style="font-size: 21px ;position: relative; right: 1px ; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center ;">
                                {{Html::dateEN2BN($d->training_month)}}
                            </td>
                            <?php $flag = true; ?>
                        @endif
                        <td class="text-center" style="font-size: 14px ;" align="center">{{Html::en2bn($d->batch_no)}}</td>
                        <td style="font-size: 17px ;">{{$d->training_title}}</td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                            ({{Html::en2bn( $d->total_training_days) }} দিন)
                        </td>
                        <td style="font-size: 17px ;">
                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                        </td>
                        <td style="font-size: 17px ;"align="center">{{Html::en2bn($d->batch_participants_11_20)}} ({{Html::en2bn($d->batch_size)}})</td>
                        <td style="font-size: 17px ;">
                            ({{Html::en2bn($d->total_training_hrs)}} x {{Html::en2bn($d->total_training_days)}} x {{Html::en2bn($d->batch_participants_11_20)}}) / {{Html::en2bn($d->total_employee_11_20)}} =
                            @php
                                $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_11_20) / $d->total_employee_11_20;
                            @endphp
                            {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_11_20) / $d->total_employee_11_20, 2) ) }}
                        </td>
                    </tr>
                @endforeach
                @php
                    @$grand_total_2_5_3 += @$monthly_total;
                @endphp
                <tr>
                    <td colspan="6" style="text-align: right ;font-size: 21px ;"align="right">{{Html::dateEN2BN($d->training_month)}} = </td>
                    <td style="font-size: 21px ;"align="center">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td colspan="6" style="text-align: right ;font-size: 24px ;"align="right">  মোট অর্জন = </td>
            <td style="font-size: 22px ;"align="center">
                {{Html::en2bn( number_format(@$grand_total_2_5_3, 2) )}}
            </td>
        </tr>
    </tbody>
</table>
<!-- 2.5.3 END HERE-->
<p></p><p></p>
<!-- 2.5.4 START HERE-->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <thead>
    <tr>
        <td colspan="7" style="padding-bottom: 11px;">[২.৫] ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">[২.৫.৪] সমসাময়িক বিষয় নিয়ে লার্নিং সেশন আয়োজিত</td>
    </tr>
    <tr>
        <td colspan="7" style="padding-bottom: 11px; padding-left: 11px;">লক্ষ্যমাত্রা (সংখ্যা): ৬ টি</td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="1">
    <thead>
    <tr>
        <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
        <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ নম্বর</td>
        <td width="25%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
        <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; $flag = false; $temp_grand_total = 0; $grand_total_2_5_4 = 0;?>
    @if ( isset($report_data['2.5.4']) )
        @foreach($report_data['2.5.4'] as $data)
            <?php $flag = false; $monthly_total = 0; ?>
            @foreach($data as $d)
                <tr class="odd gradeX">
                    @if ( !$flag )
                        <td rowspan="{{count($data)}}" style="font-size: 20px ;position: relative; right: 0px ; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center ;">
                            {{Html::dateEN2BN($d->training_month)}}
                        </td>
                        <?php $flag = true; ?>
                    @endif
                    <td class="text-center" style="font-size: 14px ;" align="center">{{Html::en2bn($d->batch_no)}}</td>
                    <td style="font-size: 17px ;">{{$d->training_title}}</td>
                    <td style="font-size: 17px ;">
                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                    </td>
                </tr>
                @php
                    @$grand_total_2_5_4++;
                @endphp
            @endforeach
        @endforeach
    @endif
    <tr>
        <td colspan="3" style="text-align: right ;font-size: 24px ;"align="right">  মোট অর্জন  = </td>
        <td style="font-size: 22px ;"align="center">
            {{Html::en2bn( number_format(@$grand_total_2_5_4) )}}
        </td>
    </tr>
    </tbody>
</table>
<!-- 2.5.4 END HERE-->