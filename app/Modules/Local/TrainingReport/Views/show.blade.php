<table cellspacing="0" cellpadding="0" width="100%">

    <tr>
        <td style="border-top: 1px solid black;" align="center">{{ \Html::en2bn( @$total_emp )  }}</td>
        <td style="border-top: 1px solid black;" align="center">{{ \Html::en2bn( number_format(@$quarter_wise_summary[1], 2) ) }}</td>
        <td style="border-top: 1px solid black;" align="center">{{ \Html::en2bn( number_format(@$quarter_wise_summary[2], 2) ) }}</td>
        <td style="border-top: 1px solid black;" align="center">{{ \Html::en2bn( number_format(@$quarter_wise_summary[3], 2) ) }}</td>
        <td style="border-top: 1px solid black;" align="center">{{ \Html::en2bn( number_format(@$quarter_wise_summary[4], 2) ) }}</td>
    </tr>
    <tr>
        <td style="border-top: 1px solid black;" align="center">কর্মকর্তা / কর্মচার</td>
        <td style="border-top: 1px solid black;" align="center">১ম কোরার্টার</td>
        <td style="border-top: 1px solid black;" align="center">২য় কোরার্টার </td>
        <td style="border-top: 1px solid black;" align="center">৩য় কোরার্টার</td>
        <td style="border-top: 1px solid black;" align="center">৪র্থ কোরার্টার</td>
    </tr>
</table>
<p></p>

<table cellspacing="0" cellpadding="0" width="100%">
    <thead>
    <tr>
        <td width="10%" style="border: 1px solid black;" align="center">মাস, বছর</td>
        <td width="5%" style="border: 1px solid black;" align="center">ব্যাচ</td>
        <td width="20%" style="border: 1px solid black;" align="center">প্রশিক্ষণের বিষয়</td>
        <td width="15%" style="border: 1px solid black;" align="center">তারিখ</td>
        <td width="10%" style="border: 1px solid black;" align="center">সময়</td>
        <td width="10%" style="border: 1px solid black;" align="center">প্রশিক্ষণার্থীর সংখ্যা</td>
        <td width="10%" style="border: 1px solid black;" align="center">মোট ঘন্টা</td>
        <td width="10%" style="border: 1px solid black;" align="center">জনঘন্টা</td>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
    @foreach($report_data as $data)
        <?php $flag = false; $monthly_total = 0; ?>
        @foreach($data as $d)
            <tr class="odd gradeX">
                @if ( !$flag )
                    <td rowspan="{{count($data)}}" style="font-size: 20px;position: relative; right: 0px ; transform: rotate(-180deg);writing-mode: vertical-rl; height: 10px;">
                        {{Html::dateEN2BN($d->training_month)}}
                    </td>
                    <?php $flag = true; ?>
                @endif
                <td align="center" style="font-size: 14px ;">{{Html::en2bn($d->batch_no)}}</td>
                <td style="font-size: 17px ;">{{$d->training_title}}</td>
                <td style="font-size: 17px ;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}} ({{Html::en2bn( $d->total_training_days) }} দিন)</td>
                <td style="font-size: 17px ;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)</td>
                <td align="center" style="font-size: 17px ;">{{Html::en2bn($d->batch_size)}}</td>
                <td align="center" style="font-size: 17px ;">{{Html::en2bn($d->batch_size)}} x {{Html::en2bn( $d->total_training_days) }} দিন x {{Html::en2bn( $d->total_training_hrs) }} ঘন্টা = {{Html::en2bn($d->total_training_hrs*$d->total_training_days*$d->batch_size)}}</td>
                <td align="center" style="font-size: 17px ;">
                    @php
                        $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_size) / $total_emp;
                    @endphp
                    {{Html::en2bn($d->total_training_hrs*$d->total_training_days*$d->batch_size)}}%{{Html::en2bn($total_emp)}} =
                    {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_size) /$total_emp, 2) ) }}
                </td>
            </tr>
        @endforeach
        @php
            $grand_total += $monthly_total;
        @endphp
        <tr>
            <td colspan="7" style="text-align: right ;font-size: 20px ;">{{Html::dateEN2BN($d->training_month)}} =&nbsp;</td>
            <td align="center" style="font-size: 20px ;">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
        </tr>
    @endforeach
    @foreach($future_report_data as $data)
        <?php $flag = false; $temp_monthly_total = 0; ?>
        @foreach($data as $d)
            <tr class="odd gradeX">
                @if ( !$flag )
                    <td rowspan="{{count($data)}}" style="color: #7f90a4; font-size: 20px ;position: relative; right: 0px ; transform: rotate(-180deg);writing-mode: vertical-rl; height: 10px;">
                        {{Html::dateEN2BN($d->training_month)}}
                    </td>
                    <?php $flag = true; ?>
                @endif
                <td class="text-center" style="color: #7f90a4; font-size: 14px ;">{{Html::en2bn($d->batch_no)}}</td>
                <td style="color: #7f90a4; font-size: 17px ;">{{$d->training_title}}</td>
                <td style="color: #7f90a4; font-size: 17px ;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}} ({{Html::en2bn( $d->total_training_days) }} দিন)</td>
                <td style="color: #7f90a4; font-size: 17px ;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)</td>
                <td style="color: #7f90a4; font-size: 17px ;">{{Html::en2bn($d->batch_size)}}</td>
                <td style="color: #7f90a4; font-size: 17px ;">
                    @php
                        $temp_monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_size) / $total_emp;
                    @endphp
                    ({{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_size) /$total_emp, 2) ) }})
                </td>
            </tr>
        @endforeach
        @php
            $temp_grand_total += $temp_monthly_total;
        @endphp
        <tr>
            <td colspan="7" style="color: #7f90a4; text-align: right ;font-size: 20px ;">{{Html::dateEN2BN($d->training_month)}} = </td>
            <td style="color: #7f90a4; font-size: 20px ;">({{Html::en2bn( number_format($temp_monthly_total, 2) )}})</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="7" style="text-align: right ;font-size: 24px ;"> সর্বমোট =&nbsp; </td>
        <td style="font-size: 22px ;">
            {{Html::en2bn( number_format($grand_total, 2) )}}
            <label style="color: #7f90a4;display: block;font-size: 18px ">+ ({{Html::en2bn( number_format($temp_grand_total, 2) )}})</label>
            <label style="color: #7f90a4;display: block;font-size: 18px ">≈ {{Html::en2bn( number_format($grand_total+$temp_grand_total, 2) )}}</label>
        </td>
    </tr>

    </tbody>
</table>
