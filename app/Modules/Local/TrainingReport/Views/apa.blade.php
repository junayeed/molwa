@extends('layouts.master')
@section('content')

    <form method="POST" action="/trainingreport">
        {{csrf_field()}}

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">

                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                   data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a id="sample_editable_1_new" href="/trainingreport?trainingReportPDFBtn=1" class="font-lg">
                                            <i class="fa fa-file-pdf-o font-green-jungle"></i> প্রতিবেদন </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/trainingreport" class="font-lg"> <i class="fa fa-list font-green-jungle"></i> তালিকা ভিউ </a>
                                    </li>
                                    <li>
                                        <a href="/trainingreport" class="font-lg"> <i class="fa fa-print font-green-jungle"></i> প্রিন্ট তালিকা ভিউ </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/trainingreport/apa?financial_year={{ @$search_data['financial_year'] }}" class="font-lg apa_view"> <i class="fa fa-th font-green-jungle"></i> এপিএ ভিউ </a>
                                    </li>
                                    <li>
                                        <a href="/trainingreport/apa?trainingAPAReportPrntBtn=1&financial_year={{ @$search_data['financial_year'] }}" class="font-lg apa_view"> <i class="fa fa-print font-green-jungle"></i> প্রিন্ট এপিএ প্রতিবেদন </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label>অর্থ বছর</label>
                                <select name="financial_year" id="financial_year" class="form-control input" onchange="this.form.submit();">
                                    <option value=""> - অর্থ বছর বাছাই করুন - </option>
                                    @foreach($financial_year_list as $id => $fin_yr)
                                        <option value="{{$id}}"
                                                @if ( $search_data['financial_year']))
                                                    @if ( $id == @$search_data['financial_year']) selected @endif
                                                @else
                                                    @if ( $id == $current_financial_yr) selected @endif
                                                @endif>{{Html::en2bn($fin_yr)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- APA Performance Index 2.2.1 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.২] >> কর্মসম্পাদন সূচকঃ [২.২.১] </div>
                                                <div class="title font-red-haze"> প্রকল্প ব্যবস্থাপনা সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> সংখ্যা (জন) </div>
                                                <div class="number font-red-haze"> ৩০ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue">অর্জন সংখ্যা (জন) )</div>
                                                <div class="number font-blue" id="apa_2_2_1_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ নম্বর</th>
                                        <th class="table_heading col-md-3">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading col-md-3">তারিখ</th>
                                        <th class="table_heading col-md-2">সময়</th>
                                        <th class="table_heading col-md-1">প্রশিক্ষণার্থীর সংখ্যা</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.2.1']) )
                                        @foreach($report_data['2.2.1'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)
                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        <?php $monthly_total += $d->batch_size; ?>
                                                        {{Html::en2bn($d->batch_size)}}
                                                    </td>

                                                </tr>
                                            @endforeach
                                            @php
                                                @$grand_total_2_2_1 += @$monthly_total;
                                            @endphp
                                            <tr>
                                                <td colspan="5" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                                <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total) )}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                            <td style="font-size: 22px !important;">
                                                {{Html::en2bn( number_format(@$grand_total_2_2_1) )}}
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <!-- APA TABLE VIEW END HERE-->
                            </div>
                        </div>
                        <!-- APA Performance Index 2.2.1 END HERE-->
                        <!-- APA Performance Index 2.2.2 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.২] >> কর্মসম্পাদন সূচকঃ [২.২.২] </div>
                                                <div class="title font-red-haze"> ই-জিপি সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> সংখ্যা (জন) </div>
                                                <div class="number font-red-haze"> ১৫ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue">অর্জন সংখ্যা (জন) )</div>
                                                <div class="number font-blue" id="apa_2_2_2_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ নম্বর</th>
                                        <th class="table_heading col-md-3">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading col-md-3">তারিখ</th>
                                        <th class="table_heading col-md-2">সময়</th>
                                        <th class="table_heading col-md-1">প্রশিক্ষণার্থীর সংখ্যা</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.2.2']) )
                                        @foreach($report_data['2.2.2'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)
                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        <?php $monthly_total += $d->batch_size; ?>
                                                        {{Html::en2bn($d->batch_size)}}
                                                    </td>

                                                </tr>
                                            @endforeach
                                            @php
                                                @$grand_total_2_2_2 += @$monthly_total;
                                            @endphp
                                            <tr>
                                                <td colspan="5" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                                <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total) )}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                            <td style="font-size: 22px !important;">
                                                {{Html::en2bn( number_format(@$grand_total_2_2_2) )}}
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <!-- APA TABLE VIEW END HERE-->
                            </div>
                        </div>
                        <!-- APA Performance Index 2.2.2 END HERE-->

                        <!-- APA Performance Index 2.5.1 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.৫] >> কর্মসম্পাদন সূচকঃ [২.৫.১] </div>
                                                <div class="title font-red-haze"> ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> সংখ্যা (জন) </div>
                                                <div class="number font-red-haze"> ৬০ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue">অর্জন সংখ্যা (জন) )</div>
                                                <div class="number font-blue" id="apa_2_5_1_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ নম্বর</th>
                                        <th class="table_heading col-md-3">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading col-md-3">তারিখ</th>
                                        <th class="table_heading col-md-2">সময়</th>
                                        <th class="table_heading col-md-1">প্রশিক্ষণার্থীর সংখ্যা</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.5.1']) )
                                        @foreach($report_data['2.5.1'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)
                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        <?php $monthly_total += $d->batch_size; ?>
                                                        {{Html::en2bn($d->batch_size)}}
                                                    </td>

                                                </tr>
                                            @endforeach
                                            @php
                                                @$grand_total_2_5_1 += @$monthly_total;
                                            @endphp
                                            <tr>
                                                <td colspan="5" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                                <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total) )}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                            <td style="font-size: 22px !important;">
                                                {{Html::en2bn( number_format(@$grand_total_2_5_1) )}}
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <!-- APA TABLE VIEW END HERE-->
                            </div>
                        </div>
                        <!-- APA Performance Index 2.5.1 END HERE-->


                        <!-- APA Performance Index 2.5.2 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.৫] >> কর্মসম্পাদন সূচকঃ [২.৫.২] </div>
                                                <div class="title font-red-haze"> ১০ম গ্রেড ও তদুর্ধ্ব প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> লক্ষ্যমাত্রা (জনঘন্টা) </div>
                                                <div class="number font-red-haze"> ২০ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue"> অর্জন (জনঘন্টা) </div>
                                                <div class="number font-blue" id="apa_2_5_2_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ নম্বর</th>
                                        <th class="table_heading col-md-3">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading col-md-3">তারিখ</th>
                                        <th class="table_heading col-md-2">সময়</th>
                                        <th class="table_heading col-md-1">প্রশিক্ষণার্থীর সংখ্যা (১ম গ্রেড - ১০ম গ্রেড)</th>
                                        <th class="table_heading col-md-2">জনঘন্টা</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total_2_5_2 = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.5.2']) )
                                        @foreach($report_data['2.5.2'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)

                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                                    </td>
                                                    <td style="font-size: 17px !important;">{{Html::en2bn($d->batch_participants_1_10)}} ({{Html::en2bn($d->batch_size)}})</td>
                                                    <td style="font-size: 17px !important;">
                                                        ({{Html::en2bn($d->total_training_hrs)}} x {{Html::en2bn($d->total_training_days)}} x {{Html::en2bn($d->batch_participants_1_10)}}) / {{Html::en2bn($d->total_employee_1_10)}} =
                                                        @php
                                                            $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_1_10) / $d->total_employee_1_10;
                                                        @endphp
                                                        {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_1_10) / $d->total_employee_1_10, 2) ) }}
                                                    </td>
                                                </tr>

                                            @endforeach
                                            @php
                                                $grand_total_2_5_2 += $monthly_total;
                                            @endphp
                                            <tr>
                                                <td colspan="6" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                                <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                        <tr>
                                            <td colspan="6" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                            <td style="font-size: 22px !important;">
                                                {{Html::en2bn( number_format($grand_total_2_5_2, 2) )}}
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- APA TABLE VIEW END HERE-->

                            </div>
                        </div>
                        <!-- APA Performance Index 2.5.2 END HERE-->

                        <!-- APA Performance Index 2.5.3 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.৫] >> কর্মসম্পাদন সূচকঃ [২.৫.৩] </div>
                                                <div class="title font-red-haze"> ১১তম - ২০তম গ্রেডের প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> লক্ষ্যমাত্রা (জনঘন্টা) </div>
                                                <div class="number font-red-haze"> ২০ </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue"> অর্জন (জনঘন্টা) </div>
                                                <div class="number font-blue" id="apa_2_5_3_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ নম্বর</th>
                                        <th class="table_heading col-md-3">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading col-md-3">তারিখ</th>
                                        <th class="table_heading col-md-2">সময়</th>
                                        <th class="table_heading col-md-1">প্রশিক্ষণার্থীর সংখ্যা (১১ম গ্রেড - ২০ম গ্রেড)</th>
                                        <th class="table_heading col-md-2">জনঘন্টা</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total_2_5_3 = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.5.2']) )
                                        @foreach($report_data['2.5.2'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)

                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                                    </td>
                                                    <td style="font-size: 17px !important;">{{Html::en2bn($d->batch_participants_11_20)}} ({{Html::en2bn($d->batch_size)}})</td>
                                                    <td style="font-size: 17px !important;">
                                                        ({{Html::en2bn($d->total_training_hrs)}} x {{Html::en2bn($d->total_training_days)}} x {{Html::en2bn($d->batch_participants_11_20)}}) / {{Html::en2bn($d->total_employee_11_20)}} =
                                                        @php
                                                            $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_11_20) / $d->total_employee_11_20;
                                                        @endphp
                                                        {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_participants_11_20) / $d->total_employee_11_20, 2) ) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @php
                                                @$grand_total_2_5_3 += @$monthly_total;
                                            @endphp
                                            <tr>
                                                <td colspan="6" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                                <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                        <tr>
                                            <td colspan="6" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                            <td style="font-size: 22px !important;">
                                                {{Html::en2bn( number_format(@$grand_total_2_5_3, 2) )}}
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- APA TABLE VIEW END HERE-->

                            </div>
                        </div>
                        <!-- APA Performance Index 2.5.3 END HERE-->

                        <!-- APA Performance Index 2.5.4 START HERE-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> কার্যক্রমঃ [২.৫] >> কর্মসম্পাদন সূচকঃ [২.৫.৪] </div>
                                                <div class="title font-red-haze"> সমসাময়িক বিষয় নিয়ে লার্নিং সেশন আয়োজিত </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-left">
                                            <div class="stat-number">
                                                <div class="title font-red-haze"> লক্ষ্যমাত্রা (সংখ্যা) </div>
                                                <div class="number font-red-haze"> ৬ টি </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="stat-right">
                                            <div class="stat-number">
                                                <div class="title font-blue"> অর্জন (সংখ্যা) </div>
                                                <div class="number font-blue" id="apa_2_5_4_acheivement"> ০ </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 margin-top-10">
                                <!-- APA TABLE VIEW START HERE-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="apa_list_view">
                                    <thead>
                                    <tr>
                                        <th class="table_heading col-md-half">মাস, বছর</th>
                                        <th class="table_heading col-md-half">ব্যাচ</th>
                                        <th class="table_heading">প্রশিক্ষণের বিষয়</th>
                                        <th class="table_heading">তারিখ</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 1; $flag = false; $grand_total_2_5_4 = 0; $temp_grand_total = 0;?>
                                    @if ( isset($report_data['2.5.4']) )
                                        @foreach($report_data['2.5.4'] as $data)
                                            <?php $flag = false; $monthly_total = 0; ?>
                                            @foreach($data as $d)
                                                <tr class="odd gradeX">
                                                    @if ( !$flag )
                                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; text-align: center !important;">
                                                            {{Html::dateEN2BN($d->training_month)}}
                                                        </td>
                                                        <?php $flag = true; ?>
                                                    @endif
                                                    <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                                    <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                                    <td style="font-size: 17px !important;">
                                                        {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                                        ({{Html::en2bn( $d->total_training_days) }} দিন)
                                                    </td>
                                                </tr>
                                                <?php $grand_total_2_5_4++; ?>
                                            @endforeach

                                        @endforeach
                                    @endif

                                </table>
                                <!-- APA TABLE VIEW END HERE-->

                            </div>
                        </div>
                        <!-- APA Performance Index 2.5.4 END HERE-->
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </form>
    <!-- END CONTENT -->
@endsection

@section('page_js')
    <script src="{{url(asset("/js"))}}/TrainingReport.js" type="text/javascript"></script>

@endsection

<script>

    var apa_2_2_1_acheivement_val = "{!! Html::en2bn( number_format(@$grand_total_2_2_1) ) !!}";
    var apa_2_2_2_acheivement_val = "{!! Html::en2bn( number_format(@$grand_total_2_2_2) ) !!}";
    var apa_2_5_1_acheivement_val = "{!! Html::en2bn( number_format(@$grand_total_2_5_1) ) !!}";
    var apa_2_5_2_acheivement_val = "{!! Html::en2bn( number_format(@$grand_total_2_5_2, 2) ) !!}";
    var apa_2_5_3_acheivement_val = "{!! Html::en2bn( number_format(@$grand_total_2_5_3, 2) ) !!}";
    var apa_2_5_4_acheivement_val = "{!! Html::en2bn( number_format($grand_total_2_5_4))  !!}";

</script>