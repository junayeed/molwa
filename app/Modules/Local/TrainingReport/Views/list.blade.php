@extends('layouts.master')
@section('content')

    <form method="POST" action="/trainingreport">
        {{csrf_field()}}
        <!-- PERCENTAGE STATS STARTS HERE  -->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-cursor font-purple"></i>
                            <span class="caption-subject font-purple bold uppercase">General Stats</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-1">
                                <div class="easy-pie-chart">
                                    <div class="number molwa_pending" data-percent="{{ @$total_emp }}">
                                        <span class="bold">{{ \Html::en2bn( @$total_emp )  }}</span>&nbsp;
                                    </div>কর্মকর্তা / কর্মচারী
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="easy-pie-chart">
                                    <div class="number female" data-percent="{{ @$emp_stats['Female'] }}">
                                        <span class="bold">{{ \Html::en2bn( @$emp_stats['Female'] ) }}</span>&nbsp;
                                    </div> মহিলা
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="easy-pie-chart">
                                    <div class="number male" data-percent="{{ @$emp_stats['Male'] }}">
                                        <span class="bold">{{ \Html::en2bn( @$emp_stats['Male'] ) }}</span>&nbsp;
                                    </div> পুরুষ
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="easy-pie-chart">
                                    <div class="number purple" data-percent="25">
                                        <span class="bold">{{ \Html::en2bn( number_format(@$quarter_wise_summary[1], 2) ) }}</span>
                                    </div>১ম কোরার্টার
                                </div>
                            </div>
                            <div class="col-md-1 margin-bottom-10">
                                <div class="easy-pie-chart">
                                    <div class="number purple" data-percent="50">
                                        <span class="bold">{{ \Html::en2bn( number_format(@$quarter_wise_summary[2], 2) ) }}</span>
                                    </div>২য় কোরার্টার
                                </div>
                            </div>
                            <div class="col-md-1 margin-bottom-10">
                                <div class="easy-pie-chart">
                                    <div class="number purple" data-percent="75">
                                        <span class="bold">{{ \Html::en2bn( number_format(@$quarter_wise_summary[3], 2) ) }}</span>
                                    </div>৩য় কোরার্টার
                                </div>
                            </div>
                            <div class="col-md-1 margin-bottom-10">
                                <div class="easy-pie-chart">
                                    <div class="number purple" data-percent="100">
                                        <span class="bold">{{ \Html::en2bn( number_format(@$quarter_wise_summary[4], 2) ) }}</span>
                                    </div>৪র্থ কোরার্টার
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PERCENTAGE STATS ENDS HERE  -->
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">

                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                   data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a id="sample_editable_1_new" href="/trainingreport?trainingReportPDFBtn=1" class="font-lg">
                                            <i class="fa fa-file-pdf-o font-green-jungle"></i> প্রতিবেদন </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/trainingreport" class="font-lg"> <i class="fa fa-list font-green-jungle"></i> তালিকা ভিউ </a>
                                    </li>
                                    <li>
                                        <a href="/trainingreport" class="font-lg"> <i class="fa fa-print font-green-jungle"></i> প্রিন্ট তালিকা ভিউ </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/trainingreport/apa?financial_year={{ @$search_data['financial_year'] }}" class="font-lg apa_view"> <i class="fa fa-th font-green-jungle"></i> এপিএ ভিউ </a>
                                    </li>
                                    <li>
                                        <a href="/trainingreport/apa?trainingAPAReportPrntBtn=1&financial_year={{ @$search_data['financial_year'] }}" class="font-lg apa_view"> <i class="fa fa-print font-green-jungle"></i> প্রিন্ট এপিএ প্রতিবেদন </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label>অর্থ বছর</label>
                                <select name="financial_year" id="financial_year" class="form-control input" onchange="this.form.submit();">
                                    <option value=""> - অর্থ বছর বাছাই করুন - </option>
                                    @foreach($financial_year_list as $id => $fin_yr)
                                        <option value="{{$id}}"
                                                @if ( $search_data['financial_year']))
                                                    @if ( $id == @$search_data['financial_year']) selected @endif
                                                @else
                                                    @if ( $id == $current_financial_yr) selected @endif
                                                @endif>{{Html::en2bn($fin_yr)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- LIST TABLE STARS HERE -->
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                            <thead>
                                <tr>
                                    <th class="table_heading">মাস, বছর</th>
                                    <th class="table_heading">ব্যাচ</th>
                                    <th class="table_heading">প্রশিক্ষণের বিষয়</th>
                                    <th class="table_heading">তারিখ</th>
                                    <th class="table_heading">সময়</th>
                                    <th class="table_heading">প্রশিক্ষণার্থীর সংখ্যা</th>
                                    <th class="table_heading">জনঘন্টা</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; $flag = false; $grand_total = 0; $temp_grand_total = 0;?>
                            @foreach($report_data as $data)
                                <?php $flag = false; $monthly_total = 0; ?>
                                @foreach($data as $d)
                                    <tr class="odd gradeX">
                                        @if ( !$flag )
                                        <td rowspan="{{count($data)}}" style="font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; height: 10px;">
                                            {{Html::dateEN2BN($d->training_month)}}
                                        </td>
                                        <?php $flag = true; ?>
                                        @endif
                                        <td class="text-center" style="font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                        <td style="font-size: 17px !important;">{{$d->training_title}}</td>
                                        <td style="font-size: 17px !important;">
                                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}}
                                            ({{Html::en2bn( $d->total_training_days) }} দিন)
                                        </td>
                                        <td style="font-size: 17px !important;">
                                            {{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)
                                        </td>
                                        <td style="font-size: 17px !important;">{{Html::en2bn($d->batch_size)}}</td>
                                        <td style="font-size: 17px !important;">
                                            @php
                                                $monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_size) / $total_emp;
                                            @endphp
                                            {{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_size) /$total_emp, 2) ) }}
                                        </td>
                                    </tr>
                                @endforeach
                                @php
                                    $grand_total += $monthly_total;
                                @endphp
                                <tr>
                                    <td colspan="6" style="text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                    <td style="font-size: 20px !important;">{{Html::en2bn( number_format($monthly_total, 2) )}}</td>
                                </tr>
                            @endforeach
                            @foreach($future_report_data as $data)
                                <?php $flag = false; $temp_monthly_total = 0; ?>
                                @foreach($data as $d)
                                    <tr class="odd gradeX">
                                        @if ( !$flag )
                                            <td rowspan="{{count($data)}}" style="color: #7f90a4; font-size: 20px !important;position: relative; right: 0px !important; transform: rotate(-180deg);writing-mode: vertical-rl; height: 10px;">
                                                {{Html::dateEN2BN($d->training_month)}}
                                            </td>
                                            <?php $flag = true; ?>
                                        @endif
                                        <td class="text-center" style="color: #7f90a4; font-size: 14px !important;">{{Html::en2bn($d->batch_no)}}</td>
                                        <td style="color: #7f90a4; font-size: 17px !important;">{{$d->training_title}}</td>
                                        <td style="color: #7f90a4; font-size: 17px !important;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_date)->format('d/m/Y') )}} ({{Html::en2bn( $d->total_training_days) }} দিন)</td>
                                        <td style="color: #7f90a4; font-size: 17px !important;">{{Html::en2bn( \Carbon\Carbon::parse($d->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($d->batch_end_time)->format('h:i') ) }} ({{Html::en2bn( $d->total_training_hrs) }} ঘন্টা)</td>
                                        <td style="color: #7f90a4; font-size: 17px !important;">{{Html::en2bn($d->batch_size)}}</td>
                                        <td style="color: #7f90a4; font-size: 17px !important;">
                                            @php
                                                $temp_monthly_total += ($d->total_training_hrs*$d->total_training_days*$d->batch_size) / $total_emp;
                                            @endphp
                                            ({{Html::en2bn( number_format( ($d->total_training_hrs*$d->total_training_days*$d->batch_size) /$total_emp, 2) ) }})
                                        </td>
                                    </tr>
                                @endforeach
                                @php
                                    $temp_grand_total += $temp_monthly_total;
                                @endphp
                                <tr>
                                    <td colspan="6" style="color: #7f90a4; text-align: right !important;font-size: 20px !important;">{{Html::dateEN2BN($d->training_month)}} = </td>
                                    <td style="color: #7f90a4; font-size: 20px !important;">({{Html::en2bn( number_format($temp_monthly_total, 2) )}})</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="6" style="text-align: right !important;font-size: 24px !important;"> সর্বমোট = </td>
                                <td style="font-size: 22px !important;">
                                    {{Html::en2bn( number_format($grand_total, 2) )}}
                                    <label style="color: #7f90a4;display: block;font-size: 18px ">+ ({{Html::en2bn( number_format($temp_grand_total, 2) )}})</label>
                                    <label style="color: #7f90a4;display: block;font-size: 18px ">≈ {{Html::en2bn( number_format($grand_total+$temp_grand_total, 2) )}}</label>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                        <!-- LIST TABLE VIEW END HERE-->

                        {{--<div class="row number-stats margin-bottom-30">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-left">
                                    <div class="stat-number">
                                        <div class="title font-red-haze font-lg"> লক্ষ্যমাত্রা (জনঘন্টা) </div>
                                        <div class="number font-red-haze"> ৩০ </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="stat-right">
                                    <div class="stat-number">
                                        <div class="title font-blue"> অর্জন (জনঘন্টা) </div>
                                        <div class="number font-blue"> ০ </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </form>
    <!-- END CONTENT -->
@endsection

@section('page_js')
    <script src="{{url(asset("/js"))}}/TrainingReport.js" type="text/javascript"></script>

@endsection