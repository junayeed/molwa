<?php

Route::group(['prefix' => 'trainingreport', 'namespace' => 'App\Modules\Local\TrainingReport\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',             ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@index']);
    Route::post('/',            ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@index']);
    Route::get('/apa',          ['as' => 'trainingreport.show',        'uses' => 'TrainingReportController@apaReport']);


    /*Route::get('/add',          ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@add']);
    Route::get('/update/{id}',  ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@update']);
    Route::post('/save',        ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@save']);
    Route::get('/delete/{id}',  ['as' => 'trainingreport.index',       'uses' => 'TrainingReportController@delete']);*/
});
?>