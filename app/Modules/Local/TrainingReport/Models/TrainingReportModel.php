<?php
namespace App\Modules\Local\TrainingReport\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class TrainingReportModel extends Model
{
    protected $table = 'employee';

    protected $fillable = ['name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office',
                           'profile_image', 'employee_class', 'employee_status', 'gender', 'pay_grade'];
}
