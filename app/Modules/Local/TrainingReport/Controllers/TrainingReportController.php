<?php
namespace App\Modules\Local\TrainingReport\Controllers;

use App\Modules\Local\Employee\Models\EmployeeModel;
use App\Modules\Local\Batch\Models\BatchModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Modules\Local\Designation\Models\DesignationModel;
use App\Models\Office;

class TrainingReportController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $report_data                = array();
        $future_report_data         = array();
        $search_data                = $request->only(['financial_year']);
        $current_financial_yr       = \Common::getCurrentFinancialYear();
        $search_query               = BatchModel::select('batch_no', 'training_title', 'batch_size', 'batch_start_time',
                                                         'batch_end_time', 'batch_start_date', 'batch_end_date', 'include_weekend',
                                                         DB::raw('DATE_FORMAT(batch_start_date, "%M, %Y") AS training_month'),
                                                         DB::raw('TIMESTAMPDIFF(HOUR, batch_start_time, batch_end_time) AS total_training_hrs'),
                                                         DB::raw('TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1 AS total_training_days')
                                                         )
                                                   ->where(['batch_status' => 'Completed'])
                                                   ->orderBy('batch_start_date', 'ASC');

        if ( isset($search_data['financial_year']) && !empty($search_data['financial_year']) ) {
            $search_query->where('batches.financial_year', '=', $search_data['financial_year']);
        }
        else {
            $search_query->where('batches.financial_year', '=', $current_financial_yr);
        }

        $rpt_data = $search_query->get();

        $future_query              = BatchModel::select('batch_no', 'training_title', 'batch_size', 'batch_start_time',
                                                                  'batch_end_time', 'batch_start_date', 'batch_end_date', 'include_weekend',
                                                                  DB::raw('DATE_FORMAT(batch_start_date, "%M, %Y") AS training_month'),
                                                                  DB::raw('TIMESTAMPDIFF(HOUR, batch_start_time, batch_end_time) AS total_training_hrs'),
                                                                  DB::raw('TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1 AS total_training_days')
                                                        )
                                                 ->whereRaw('((`batch_status` = "Upcoming") or (`batch_status` = "In Progress"))')
                                                 ->orderBy('batch_start_date', 'ASC');
        if ( isset($search_data['financial_year']) && !empty($search_data['financial_year']) ) {
            $future_query->where('batches.financial_year', '=', $search_data['financial_year']);
        }
        else {
            $future_query->where(['batches.financial_year' => $current_financial_yr]);
        }

        $future_rpt_data = $future_query->get();

        $emp_stats                  = DB::table('employee')->select(DB::raw('COUNT(gender) AS total_gender'), 'gender')->where(['employee_status' => 'Active'])->groupBy('gender')->get()->pluck('total_gender', 'gender');
        $total_emp                  = DB::table('employee')->select(DB::raw('COUNT(id) AS total_employee'))->where(['employee_status' => 'Active'])->get();
        $financial_year_list        = DB::table('financial_years')->select('financial_year')->get()->pluck('financial_year', 'financial_year');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        if ( $rpt_data ) {
            foreach($rpt_data as $d) {
                if ( !$d->include_weekend ) {
                    $weekends = \Common::calculateWeekEnd($d->batch_start_date, $d->batch_end_date);
                    $d->total_training_days = $d->total_training_days-$weekends;
                }
                $report_data[$d->training_month][] = $d;
            }
        }

        if ( $future_rpt_data ) {
            foreach($future_rpt_data as $f) {
                if ( !$f->include_weekend ) {
                    $weekends = \Common::calculateWeekEnd($f->batch_start_date, $f->batch_end_date);
                    $f->total_training_days = $f->total_training_days - $weekends;
                }
                $future_report_data[$f->training_month][] = $f;
            }
        }

        $quarter_wise_summary    = $this->calculateQuarterWiseSummary($report_data, $total_emp[0]->total_employee);
        $total_employee_1_10     = $this->totalEmployee(1, 10);  // get the total number of employee of Garde 1 to 10
        $total_employee_11_20    = $this->totalEmployee(11, 20);  // get the total number of employee of Garde 11 to 20

        if ( $request->has('trainingReportPDFBtn') ){
            $html =  view('Local/TrainingReport::show', ['report_data' => $report_data, 'future_report_data' => $future_report_data,
                                                               'total_emp' => $total_emp[0]->total_employee, 'quarter_wise_summary' => $quarter_wise_summary]);
            \Common::pdfCreate($html, "Training Report");
            return;
        }


        //\Common::dumpVar(DB::getQueryLog());
        return view('Local/TrainingReport::list', ['report_data' => $report_data, 'data' => $data, 'emp_stats' => $emp_stats,
                                                        'total_emp' => $total_emp[0]->total_employee, 'future_report_data' => $future_report_data,
                                                        'quarter_wise_summary' => $quarter_wise_summary, 'financial_year_list' => $financial_year_list,
                                                        'current_financial_yr' => $current_financial_yr, 'search_data' => $search_data]);
    }



    function apaReport(Request $request) {
        DB::enableQueryLog();

        $report_data                = array();
        $future_report_data         = array();
        $search_data                = $request->only(['financial_year']);
        $current_financial_yr       = \Common::getCurrentFinancialYear();
        $search_query               = BatchModel::select('batch_no', 'training_title', 'batch_size', 'batch_start_time',
                                                                 'batch_end_time', 'batch_start_date', 'batch_end_date', 'include_weekend',
                                                                 'id', 'apa_activities', 'apa_performance_index', 'total_employee_1_10', 'total_employee_11_20',
                                                                 DB::raw('DATE_FORMAT(batch_start_date, "%M, %Y") AS training_month'),
                                                                 DB::raw('TIMESTAMPDIFF(HOUR, batch_start_time, batch_end_time) AS total_training_hrs'),
                                                                 DB::raw('TIMESTAMPDIFF(DAY, batch_start_date, batch_end_date)+1 AS total_training_days')
                                                        )
                                                ->where(['batch_status' => 'Completed'])
                                                ->orderBy('batch_start_date', 'ASC')
                                                ->orderBy('apa_performance_index', 'ASC');

        if ( isset($search_data['financial_year']) && !empty($search_data['financial_year']) ) {
            $search_query->where('batches.financial_year', '=', $search_data['financial_year']);
            $financial_year = $search_data['financial_year'];
        }
        else {
            $search_query->where('batches.financial_year', '=', $current_financial_yr);
            $financial_year = $current_financial_yr;
        }

        $rpt_data = $search_query->get();

        $emp_stats                  = DB::table('employee')->select(DB::raw('COUNT(gender) AS total_gender'), 'gender')->where(['employee_status' => 'Active'])->groupBy('gender')->get()->pluck('total_gender', 'gender');
        $total_emp                  = DB::table('employee')->select(DB::raw('COUNT(id) AS total_employee'))->where(['employee_status' => 'Active'])->get();
        $financial_year_list        = DB::table('financial_years')->select('financial_year')->get()->pluck('financial_year', 'financial_year');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;


        if ( $rpt_data ) {
            foreach($rpt_data as $d) {
                //if ( !$d->include_weekend ) {
                    $weekends = \Common::calculateWeekEnd($d->batch_start_date, $d->batch_end_date);
                    $d->total_training_days      = $d->total_training_days-$weekends;
                    $d->batch_participants_1_10  = $this->getBatchWiseParticipants($d->id, 1, 10);
                    $d->batch_participants_11_20 = $this->getBatchWiseParticipants($d->id, 11, 20);
                //}
                $report_data[$d->apa_performance_index][$d->training_month][] = $d;
            }
        }
        //print_r(DB::getQueryLog());
        //echo "<pre>"; print_r($report_data); die;

        //$quarter_wise_summary    = $this->calculateQuarterWiseSummary($report_data, $total_emp[0]->total_employee);
        $total_employee_1_10     = $this->totalEmployee(1, 10);  // get the total number of employee of Garde 1 to 10
        $total_employee_11_20    = $this->totalEmployee(11, 20);  // get the total number of employee of Garde 11 to 20

        if ( $request->has('trainingAPAReportPrntBtn') ){
            $html =  view('Local/TrainingReport::ReportAPAPDF', ['report_data' => $report_data, 'data' => $data, 'emp_stats' => $emp_stats,
                                                                      'total_emp' => $total_emp[0]->total_employee, 'future_report_data' => $future_report_data,
                                                                      'financial_year' => $financial_year,
                                                                      'current_financial_yr' => $current_financial_yr, 'search_data' => $search_data,
                                                                      'total_employee_1_10' => $total_employee_1_10, 'total_employee_11_20' => $total_employee_11_20]);
            \Common::pdfCreate($html, "APA Training Report");
            return;
        }

        //\Common::dumpVar(DB::getQueryLog());
        return view('Local/TrainingReport::apa', ['report_data' => $report_data, 'data' => $data, 'emp_stats' => $emp_stats,
                                                       'total_emp' => $total_emp[0]->total_employee, 'future_report_data' => $future_report_data,
                                                       'financial_year_list' => $financial_year_list,
                                                       'current_financial_yr' => $current_financial_yr, 'search_data' => $search_data,
                                                       'total_employee_1_10' => $total_employee_1_10, 'total_employee_11_20' => $total_employee_11_20]);
    }

    function getBatchWiseParticipants($batch_id, $pay_grade_start, $pay_grade_end) {

        $data = DB::table('batch_participants AS BP')->select(DB::raw('COUNT(BP.id) AS batch_participants'))
            ->leftJoin('employee_profiles AS EP', 'EP.id', '=', 'BP.emp_profile_id')
            ->where('BP.batch_id', '=', $batch_id)
            ->where('pay_grade', '>=', $pay_grade_start)
            ->where('pay_grade', '<=', $pay_grade_end)
            ->get();

        return $data[0]->batch_participants;
    }

    public static function totalEmployee($pay_grade_start, $pay_grade_end) {
        $data = DB::table('employee AS E')->select(DB::raw('COUNT(E.id) AS total_employee'))
                          ->leftJoin('employee_profiles AS EP', 'EP.id', '=', 'E.emp_profile_id')
                          ->where(['employee_status' => 'Active'])
                          ->where('pay_grade', '>=', $pay_grade_start)
                          ->where('pay_grade', '<=', $pay_grade_end)
                          ->get();

        return $data[0]->total_employee;
    }

    function calculateQuarterWiseSummary($report_data, $total_employee) {
        $quarterly_summary = array(0, 0, 0, 0, 0);
        $calendar = [1 => ['July', 'August', 'September'],
                     2 => ['October', 'November', 'December'],
                     3 => ['January', 'February', 'March'],
                     4 => ['April', 'May', 'June']];

        foreach($report_data as $key => $rd){
            foreach($rd as $r) {
                list($month, $year) = explode(', ', $key);

                foreach ($calendar as $ckey => $cval) {
                    if (in_array($month, $cval)) {
                        $quarterly_summary[$ckey] += ($r['batch_size']*$r['total_training_hrs'] * $r['total_training_days']) / $total_employee;
                    }

                }
            }
        }

        return $quarterly_summary;
    }

    function add() {

        $designation_list           = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list             = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list              = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Employee::form', ['data' => $data, 'designation_list' => $designation_list, 'emp_status_list' => $emp_status_list,
                                                   'office_list' => $office_list, 'emp_class_list' => $emp_class_list]);
    }

    function update($id) {
        $financial_year               = \Common::getCurrentFinancialYear();
        $emp_data                     = EmployeeModel::find($id);
        $emp_stats                    = DB::table('batches AS B')
                                         ->select(DB::raw('COUNT(BP.batch_id) AS total_training'), DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(batch_end_time, batch_start_time))/3600) AS total_training_hrs'))
                                         ->leftJoin('batch_participants AS BP', 'BP.batch_id', '=', 'B.id')
                                         ->where(['batch_status' => 'Completed', 'BP.employee_id' => $emp_data->id, 'financial_year' => $financial_year])
                                         ->get();

        $auditLogData                 = \Common::getAuditLogData('Batch', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        list($controller, $action)    = explode('@', $controller);
        $designation_list             = DesignationModel::select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list                  = Office::select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $emp_class_list               = ["1" => "১ম শ্রেণী", "2" => "২য় শ্রেণী", "3" => "৩য় শ্রেণী", "4" => "৪র্থ শ্রেণী"];
        $emp_status_list              = ['Active' => 'সক্রিয়', 'Inactive' => 'নিষ্ক্রিয়'];
        $data['nav_item']             = str_replace("Controller", "", $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Employee::form', ['emp_data' => $emp_data, 'data' => $data, 'designation_list' => $designation_list,
                                                   'office_list' => $office_list, 'emp_class_list' => $emp_class_list, 'audit_log' => $auditLogData,
                                                   'emp_status_list' => $emp_status_list, 'emp_stats' => $emp_stats[0]]);
    }

    function save(Request $request) {
        $employee_data     = $request->only(['name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office', 'employee_class', 'employee_status', 'gender', 'pay_grade']);
        $field_list        = array('name_en', 'name_bn', 'cadre_no', 'designation', 'email', 'cellphone', 'office', 'employee_class', 'employee_status', 'gender', 'pay_grade');

        if( $request->id ) {
            $employee      = EmployeeModel::find($request->id);
            $old_data      = $employee->toArray();
            $employee->update($employee_data);
            $this->uploadImage($request, $request->id);

            \Common::AuditLog('Employee', $request->id, $old_data, $employee_data, 'Updated', $field_list);
        }
        else {
            $employee_id = EmployeeModel::create($employee_data)->id;
            $this->uploadImage($request, $employee_id);
            \Common::getAuditLogData('Employee', $employee_id, '', '', 'Create', '');
        }

        return redirect('/employee');
    }
}
