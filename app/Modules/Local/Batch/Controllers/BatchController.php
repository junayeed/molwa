<?php
namespace App\Modules\Local\Batch\Controllers;
use App\Modules\Local\Batch\Models\BatchModel;
use App\Http\Controllers\Controller;
use App\Modules\Local\Employee\Models\EmployeeModel;
use App\Modules\Local\TraingReport\Models\TrainingReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

class BatchController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $batch                = array();
        $search_data          = $request->only(['financial_year']);
        $current_financial_yr = \Common::getCurrentFinancialYear();
        $search_query         = BatchModel::select('batches.id', DB::raw('count(batch_participants.batch_id) AS no_participants'), 'batch_no',
                                                   'training_title', 'batch_size', 'batch_start_date', 'batch_end_date', 'financial_year', 'include_weekend',
                                                   'batch_start_time', 'batch_end_time', 'batch_status', 'training_venue')
                                          ->leftJoin('batch_participants', 'batch_participants.batch_id', '=', 'batches.id')
                                          ->groupBy('batches.id')
                                          ->orderByRaw("FIELD(batch_status, \"Upcoming\", \"In Progress\", \"Completed\")")
                                          ->orderBy('batch_start_date', 'DESC');

        if ( isset($search_data['financial_year']) && !empty($search_data['financial_year']) ) {
            $search_query->where('batches.financial_year', '=', $search_data['financial_year']);
        }
        else {
            $search_query->where('batches.financial_year', '=', $current_financial_yr);
        }

        $batch_data = $search_query->get();

        if ( $batch_data ){
            foreach( $batch_data as $b){
                $batch[$b->batch_status][] = $b;
            }
        }

        $venue_list                 = DB::table('venues')->select('id', 'venue_bn')->where('status', 'Active')->get()->pluck('venue_bn', 'id');
        $financial_year_list        = DB::table('financial_years')->select('financial_year')->get()->pluck('financial_year', 'financial_year');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Batch::list', ['data' => $data, 'batch_data' => $batch, 'venue_list' => $venue_list,
                                               'financial_year_list' => $financial_year_list, 'current_financial_yr' => $current_financial_yr,
                                               'search_data' => $search_data]);
    }

    public function saveBatchParticipant(Request $request){
        $data = $request->only(['batch_id', 'employee_id', 'emp_profile_id']);
        $id = DB::table('batch_participants')->insertGetId($data);
    }

    public function removeBatchParticipant(Request $request){
        $data = $request->only(['batch_id', 'employee_id']);

        DB::table('batch_participants')->where('batch_id', '=', $data['batch_id'])->where('employee_id', '=', $data['employee_id'])->delete();
    }


    public function getParticipantDetails(Request $request) {
        $batch_id         = $request->input('batch_id');
        $designation      = $request->input('designation');
        $employee_class   = $request->input('employee_class');
        $office           = $request->input('office');

        $batch_data     = BatchModel::find($batch_id);
        $employee_query = EmployeeModel::select('employee.name_bn', 'profile_image', 'email', 'ep.employee_class', 'emp_profile_id',
                                               'designations.name_bn AS designation', 'office_name_bn', 'employee.id AS employee_id')
                                      ->where('employee_status', 'Active')
                                      ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
                                      ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
                                      ->leftJoin('offices', 'offices.id', '=', 'ep.office')
                                      ->orderBy('designations.weight', 'ASC');
        if ( isset($designation) && !empty($designation) )          $employee_query->where('ep.designation', '=', $designation);
        if ( isset($employee_class) && !empty($employee_class) )    $employee_query->where('ep.employee_class', '=', $employee_class);
        if ( isset($office) && !empty($office) )                    $employee_query->where('ep.office', '=', $office);

        $employee_data    = $employee_query->get()->all();

        $participant_list = DB::table('batch_participants')->select('id', 'employee_id')->where('batch_id', $batch_id)->get()->pluck('employee_id', 'id')->toArray();
        $designation_list = DB::table('designations')->select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list      = DB::table('offices')->select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $class_list_bn    = \Html::$bn_class;
        $class_list_en    = \Html::$en_class;


        if ( $employee_data ) {
            foreach($employee_data AS $emp){
                $employee_list[$emp->employee_class][] = $emp;
            }
        }

        return view('Local/Batch::participants', ['batch_data' => $batch_data, 'employee_list' => @$employee_list,
                                                        'participant_list' => $participant_list, 'designation_list' => $designation_list,
                                                        'class_list_bn' => $class_list_bn, 'class_list_en' => $class_list_en,
                                                        'office_list' => $office_list, 'srch_designation' => $designation, 'srch_employee_class' => $employee_class,
                                                        'srch_office' => $office]);
    }

    public function add() {
        $batch_data                 = (object)[];
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $batch_data->financial_year = \Common::getCurrentFinancialYear();
        $batch_data->batch_no       = BatchModel::where('financial_year', '=', $batch_data->financial_year)->max('batch_no')+1;
        $batch_status_list          = ['Upcoming' => 'আসন্ন', 'In Progress' => 'চলমান', 'Completed' => 'সমাপ্ত'];
        $venue_list                 = DB::table('venues')->select('id', 'venue_bn')->where('status', 'Active')->get()->pluck('venue_bn', 'id');
        $designation_list           = DB::table('designations')->select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $training_type_list         = ['Internal Training' => 'অভ্যন্তরীণ প্রশিক্ষণ', 'Workshop' => 'কর্মশালা'];
        $trainer_list               = EmployeeModel::select(DB::raw("CONCAT(employee.name_bn, ', ', D.name_bn) AS display_name"), 'employee.id')
                                                ->leftJoin('employee_profiles as ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                ->leftJoin('designations AS D', 'D.id', '=', 'ep.designation')
                                                ->where('employee_status', '=', 'Active')
                                                ->where('pay_grade', '<', 10)
                                                ->orderBy('D.weight', 'ASC')
                                                ->get()->pluck('display_name', 'id');

        return view('Local/Batch::form', ['data' => $data, 'batch_status_list' => $batch_status_list, 'venue_list' => $venue_list,
                                                'training_type_list' => $training_type_list, 'designation_list' => $designation_list,
                                                'trainer_list' => $trainer_list, 'batch_data' => $batch_data]);
    }

    public function update($id) {
        $batch_data                 = BatchModel::find($id);
        $batch_status_list          = ['Upcoming' => 'আসন্ন', 'In Progress' => 'চলমান', 'Completed' => 'সমাপ্ত'];
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $auditLogData               = \Common::getAuditLogData('Batch', $id);
        $venue_list                 = DB::table('venues')->select('id', 'venue_bn')->where('status', 'Active')->get()->pluck('venue_bn', 'id');
        $training_type_list         = ['Internal Training' => 'অভ্যন্তরীণ প্রশিক্ষণ', 'Workshop' => 'কর্মশালা'];
        $batch_trainer_data         = DB::table('batch_trainers')->where(['batch_id' => $batch_data->id])->get();
        $trainer_list               = EmployeeModel::select(DB::raw("CONCAT(employee.name_bn, ', ', D.name_bn) AS display_name"), 'employee.id')
                                                   ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                   ->leftJoin('designations AS D', 'D.id', '=', 'ep.designation')
                                                   ->where('employee_status', '=', 'Active')
                                                   ->where('pay_grade', '<', 10)
                                                   ->orderBy('D.weight', 'ASC')
                                                   ->get()->pluck('display_name', 'id');
        //\Common::dumpVar($batch_trainer_data, 1);
        return view('Local/Batch::form', ['batch_data' => $batch_data, 'data' => $data, 'audit_log' => $auditLogData,
                                                'batch_status_list' => $batch_status_list, 'venue_list' => $venue_list,
                                                'training_type_list' => $training_type_list, 'trainer_list' => $trainer_list,
                                                'batch_trainer_data' => $batch_trainer_data]);
    }

    public function save(Request $request) {
        $batch_data                      = $request->only(['batch_no', 'training_title', 'batch_size', 'batch_start_date',
                                                           'batch_end_date', 'include_weekend', 'batch_start_time',
                                                           'batch_end_time', 'batch_status',
                                                           'training_type', 'training_venue', 'financial_year', 'apa_activities', 'apa_performance_index']);
        $batch_data['batch_start_date']       = date('Y-m-d', strtotime(str_replace('/', '-', $batch_data['batch_start_date']) ));
        $batch_data['batch_end_date']         = date('Y-m-d', strtotime(str_replace('/', '-', $batch_data['batch_end_date'])));
        $batch_data['include_weekend']        = isset($batch_data['include_weekend']) ? 1 : 0;
        $batch_data['financial_year']         = \Common::getCurrentFinancialYear();
        $batch_data['total_employee_1_10']    = \App\Modules\Local\TrainingReport\Controllers\TrainingReportController::totalEmployee(1,10);
        $batch_data['total_employee_11_20']   = \App\Modules\Local\TrainingReport\Controllers\TrainingReportController::totalEmployee(11,20);
        $field_list                           = array('batch_no', 'training_title', 'batch_size', 'batch_start_date', 'batch_end_date',
                                                      'batch_start_time', 'batch_end_time', 'include_weekend', 'batch_status',
                                                      'training_type', 'training_venue', 'financial_year', 'apa_activities',
                                                      'apa_performance_index');

        //echo "<pre>"; print_r($request->has('honarariumBtn'));die;

        if ( $request->has('attandanceBtn') ){
            $batch_id       = $request->id;
            $batch_data     = BatchModel::find($request->id);
            $parcipant_data = DB::table('batch_participants')->select('employee.name_bn AS participant_name', 'designations.name_bn AS designation', 'office_name_bn')
                                ->leftJoin('employee', 'employee.id', '=', 'batch_participants.employee_id')
                                ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'batch_participants.emp_profile_id')
                                ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
                                ->leftJoin('offices AS of', 'of.id', '=', 'ep.office')
                                ->where('batch_id', $batch_id)
                                ->orderBy('designations.weight', 'ASC')->get();

            \Common::generateAttandanceSheet($batch_data, $parcipant_data);
            return ;
        }
        else if ( $request->has('honarariumBtn') ){
            $batch_id = $request->id;
            $batch_data = BatchModel::find($request->id);
            $parcipant_data = DB::table('batch_participants')->select('employee.name_bn AS participant_name', 'designations.name_bn AS designation', 'pay_grade', 'office_name_bn')
                                ->leftJoin('employee', 'employee.id', '=', 'batch_participants.employee_id')
                                ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'batch_participants.emp_profile_id')
                                ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
                                ->leftJoin('offices AS of', 'of.id', '=', 'ep.office')
                                ->where('batch_id', $batch_id)
                                ->orderBy('designations.weight', 'ASC')->get();

            \Common::generateHonarariumSheet($batch_data, $parcipant_data);
            return ;
        }
        else if ( $request->has('scheduleBtn') ) {
            $batch_id     = $request->id;
            $batch_data   = BatchModel::find($request->id);
            $trainer_data = DB::table('batch_trainers')
                                ->select('employee.name_bn AS trainer', 'session_topic', 'session_date', 'session_start_time',
                                         'session_end_time', 'trainer_type', 'employee_id', 'trainer_name')
                                ->leftJoin('employee', 'employee.id', '=', 'batch_trainers.employee_id')
                                ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'batch_participants.emp_profile_id')
                                ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
                                ->where('batch_id', $batch_id)
                                ->orderBy('session_date', 'ASC')
                                ->orderBy('session_start_time', 'ASC')
                                ->orderBy('designations.weight', 'ASC')
                                ->get();

            //echo "<pre>"; print_r($parcipant_data);die;

            \Common::generateBatchSchedule($batch_data, $trainer_data);
            return ;
        }

        if( $request->id ) { // Update the Batch Data
            $batch         = BatchModel::find($request->id);
            $old_data      = $batch->toArray();
            $batch->update($batch_data);

            $this->saveTrainerData($request, $request->id);
            $this->uploadTrainingProof($batch->id, $request->batch_no, $request, $batch_data['financial_year']);

            \Common::AuditLog('Batch', $request->id, $old_data, $batch_data, 'Updated', $field_list);
        }
        else { // Create a new Batch Data
            $batch = BatchModel::create($batch_data);
            \Common::AuditLog('Batch', $batch->id, '', '', 'Create', '');

            $this->saveTrainerData($request, $batch->id);
            $this->uploadTrainingProof($batch->id, $batch->batch_no, $request, $batch_data['financial_year']);
        }

        return redirect('/batch');
    }

    public function saveTrainerData($request, $batch_id) {
        if ( $request->input('batch_session_data') ) {
            if (!empty(DB::table('batch_trainers')->where('batch_id', $batch_id)->first())) {
                DB::table('batch_trainers')->where('batch_id', $batch_id)->delete();
            }

            foreach ($request->input('batch_session_data') as $item) {
                $trainer_data = array();
                $trainer_data['batch_id']            = $batch_id;
                $trainer_data['session_topic']       = $item['session_topic'];
                $trainer_data['session_start_time']  = $item['session_start_time'];
                $trainer_data['session_end_time']    = $item['session_end_time'];
                $trainer_data['session_date']        = date('Y-m-d', strtotime(str_replace('/', '-', $item['session_date'])));
                $trainer_data['trainer_type']        = isset($item['trainer_type'][0]) ? 1 : 0;
                if ($trainer_data['trainer_type'] == 1) {
                    $trainer_data['employee_id']     = $item['employee_id'];
                } else {
                    $trainer_data['trainer_name']    = $item['trainer_name'];
                }

                DB::table('batch_trainers')->insert($trainer_data);
            }
        }
    }

    public function uploadTrainingProof($batch_id, $batch_no, $request, $fy) {
        if ($request->hasFile('training_letter_file')) {
            if ($request->file('training_letter_file')->isValid()) {
                $file        = $request->file('training_letter_file');
                $image_name  = 'batch-' . $batch_no . '-training-letter' . '.' . $file->getClientOriginalExtension();
                $image_path  = "images/batch/" . $fy . '/' . $batch_no . "/";
                $path        = public_path("images/batch/" . $fy . '/' . $batch_no . "/");
                $request->file('training_letter_file')->move($path, $image_name);

                $batch    = BatchModel::find($batch_id);
                $batch->update(['training_letter_file' => $image_path . $image_name]);
            }
        }

        if ($request->hasFile('attandance_sheet_file')) {
            if ($request->file('attandance_sheet_file')->isValid()) {
                $file        = $request->file('attandance_sheet_file');
                $image_name  = 'batch-' . $batch_no . '-attandance-sheet' . '.' . $file->getClientOriginalExtension();
                $image_path  = "images/batch/" . $fy . '/' . $batch_no . "/";
                $path        = public_path("images/batch/" . $fy . '/' . $batch_no . "/");
                $request->file('attandance_sheet_file')->move($path, $image_name);

                $batch    = BatchModel::find($batch_id);
                $batch->update(['attandance_sheet_file' => $image_path . $image_name]);
            }
        }

        if ($request->hasFile('honorarium_sheet_file')) {
            if ($request->file('honorarium_sheet_file')->isValid()) {
                $file        = $request->file('honorarium_sheet_file');
                $image_name  = 'batch-' . $batch_no . '-honorarium-sheet' . '.' . $file->getClientOriginalExtension();
                $image_path  = "images/batch/" . $fy . '/' . $batch_no . "/";
                $path        = public_path("images/batch/" . $fy . '/' . $batch_no . "/");
                $request->file('honorarium_sheet_file')->move($path, $image_name);

                $batch    = BatchModel::find($batch_id);
                $batch->update(['honorarium_sheet_file' => $image_path . $image_name]);
            }
        }
    }

    public function sendSMS2BatchParticipans(Request $request) {
        $batch_id = $request->input('batch_id');
        $sms_text = $request->input('sms_text');

        $participants = DB::table('batch_participants AS BP')
                          ->select('cellphone', 'E.id', 'E.name_bn')
                          ->leftJoin('employee AS E', 'E.id', '=', 'BP.employee_id')
                          ->where('BP.batch_id', $batch_id)
                          ->get()->all();

        //echo "<pre>"; print_r($participants);die;

        if ( $participants ) {
            foreach($participants AS $participant) {
                //\Common::sendSMS($participant->cellphone, $sms_text);
                $reply[$participant->cellphone] = \Common::sendSMS($participant->id, "8801730448801", $sms_text);

                //$reply[$participant->name_bn] = \Common::sendSMS($participant->id, $participant->cellphone, $sms_text);
            }
        }

        $data = array('reply' => $reply, 'total' => count($reply));

        //echo "<pre>"; print_r($reply); die;
        echo json_encode($data); die;
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/batch');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
