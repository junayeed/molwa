<?php
namespace App\Modules\Local\Batch\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class BatchModel extends Model
{
    protected $table = 'batches';

    protected $fillable = ['batch_no', 'training_title', 'batch_size', 'batch_start_date', 'batch_end_date', 'include_weekend',
                           'batch_start_time', 'batch_end_time', 'batch_status', 'training_type', 'training_venue',
                           'financial_year', 'training_letter_file', 'attandance_sheet_file', 'honorarium_sheet_file',
                           'apa_activities', 'apa_performance_index', 'total_employee_1_10', 'total_employee_11_20'];
}
