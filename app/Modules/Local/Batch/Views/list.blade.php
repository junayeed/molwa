@extends('layouts.master')
@section('content')
    <form method="POST" action="/batch">
    {{csrf_field()}}
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="actions">
                            <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/batch/add"><i class="fa fa-plus"></i> নতুন ব্যাচ তৈরী করুন </a>
                            {{--<a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/createUNO"> Create UNO <i class="fa fa-plus"></i></a>--}}
                        </div>
                    </div>
                    <div class="portlet-body">

                            <div class="row">
                                <div class="col-md-2">
                                    <label>অর্থ বছর</label>
                                    <select name="financial_year" id="financial_year" class="form-control input" onchange="this.form.submit();">
                                        <option value=""> - অর্থ বছর বাছাই করুন - </option>
                                        @foreach($financial_year_list as $id => $fn_year)
                                            <option value="{{$id}}"
                                                    @if ( $search_data['financial_year']))
                                                        @if ( $id == @$search_data['financial_year']) selected @endif
                                                    @else
                                                        @if ( $id == $current_financial_yr) selected @endif
                                                    @endif>{{Html::en2bn($fn_year)}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                        <div id="m_calendar" class="fc fc-unthemed fc-ltr">
                            <div class="fc-view-container" style="">
                                <div class="fc-view fc-listWeek-view fc-list-view fc-widget-content" style="">
                                    <div class="fc-scroller" style="overflow: hidden auto; height: auto;">
                                        <table class="fc-list-table" style="font-size: 1.5em !important;">
                                            <tbody>
                                            @if( !empty($batch_data) )
                                            @foreach($batch_data as $id => $batch)
                                                <tr class="fc-list-heading">
                                                    <td class="fc-widget-header" colspan="6">
                                                        <a class="fc-list-heading-main">
                                                            @if( $id == 'Upcoming')আসন্ন প্রশিক্ষণসমূহ
                                                            @elseif ( $id == 'In Progress') চলমান প্রশিক্ষণসমূহ
                                                            @elseif ( $id == 'Completed' ) সমাপ্ত প্রশিক্ষণসমূহ
                                                            @endif
                                                        </a>
                                                    </td>
                                                </tr>

                                                    @foreach($batch as $b)
                                                        <tr class="fc-list-item @if($id == 'Completed') m-fc-event--danger @elseif($id == 'In Progress')m-fc-event--success @endif">
                                                            <td class="fc-list-item-time fc-widget-content font-lg">
                                                                <a><i class="fa fa-calendar-check-o"></i> {{Html::en2bn( \Carbon\Carbon::parse($b->batch_start_date)->format('d/m/Y') )}} - {{Html::en2bn( \Carbon\Carbon::parse($b->batch_end_date)->format('d/m/Y') )}}</a>
                                                                <div class="fc-description"><i class="fa fa-clock-o"></i> {{Html::en2bn( \Carbon\Carbon::parse($b->batch_start_time)->format('h:i') )}} - {{Html::en2bn( \Carbon\Carbon::parse($b->batch_end_time)->format('h:i') )}}</div>
                                                                <div class="fc-description"><i class="fa fa-location-arrow"></i> {{$venue_list[$b->training_venue]}}</div>
                                                            </td>
                                                            <td class="fc-list-item-marker fc-widget-content">
                                                                <span class="fc-event-dot" style="width: 17px !important; height: 17px !important;"></span>
                                                            </td>
                                                            <td class="fc-list-item-title fc-widget-content font-lg">
                                                                <a>ব্যাচ নং - {{Html::en2bn( $b->batch_no )}}</a>
                                                                <div class="fc-description">প্রশিক্ষণের বিষয়ঃ {{$b->training_title}}</div>
                                                            </td>
                                                            <td class="fc-list-item-title fc-widget-content font-lg text-center">
                                                                <a>ব্যাচের আকার</a>
                                                                <div class="fc-description"><label class="label label-info "> {{Html::en2bn( $b->batch_size )}} জন </label></div>
                                                            </td>
                                                            <td class="fc-list-item-title fc-widget-content font-lg text-center">
                                                                <a>প্রশিক্ষণার্থীর সংখ্যা</a>
                                                                <div class="fc-description"><label class="label label-info "> {{Html::en2bn( $b->no_participants )}} জন </label></div>
                                                            </td>
                                                            <td class="text-center no-print">
                                                                <a href="javascript:void(0)" class="add_participants" data-batch_id="{{$b->id}}" data-toggle="modal" data-target="#participant_modal"><i class="fa fa-users"></i></a> &nbsp;&nbsp;
                                                                <a class="" href="/batch/update/{{$b->id}}"><i class="fa fa-edit"></i> </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                            @endforeach
                                            @else
                                                <tr class="fc-list-item m-fc-event--danger">
                                                    <td class="fc-list-item-time fc-widget-content font-lg text-danger ">কোন তথ্য পাওয়া যায়নি</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </form>
    <!-- END CONTENT -->


    <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details"></div>
        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/batch.js" type="text/javascript"></script>
@endsection