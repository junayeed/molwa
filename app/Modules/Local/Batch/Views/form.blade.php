@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')

    <form role="form" id="batch_form" method="POST" action="/batch/save"  enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" value="{{@$batch_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- TOP RIGHT MENU STARTS HERE -->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                               data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0)" class="font-lg" data-batch_id="{{@$batch_data->id}}" data-toggle="modal" data-target="#sms_modal">
                                        <i class="fa fa-mobile font-green-jungle"></i> বার্তা প্রেরণ করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="/batch/add" class="font-lg">
                                        <i class="fa fa-plus font-green-jungle"></i> নতুন ব্যাচ তৈরী করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="/batch" class="font-lg"> <i class="fa fa-close font-red-flamingo"></i> বন্ধ করুন </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TOP RIGHT MENU ENDS HERE -->
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i>  ব্যাচ এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">অর্থ বছর</label>
                                            <input name="financial_year" id="financial_year" type="text" class="form-control input" value="{{@$batch_data->financial_year}}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ব্যাচ নম্বর</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"> ব্যাচ নং - </span>
                                                <input name="batch_no" id="batch_no" type="text" class="form-control input" value="{{ @$batch_data->batch_no}}">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ব্যাচ এর আকার</label>
                                            <input name="batch_size" id="batch_size" type="number" class="form-control input" value="{{@$batch_data->batch_size}}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">প্রশিক্ষণ এর স্থান</label>
                                            <select name="training_venue" id="training_venue" class="form-control input" >
                                                <option value=""> - স্থান বাছাই করুন - </option>
                                                @foreach($venue_list as $id => $venue)
                                                    <option value="{{$id}}" @if( $id == @$batch_data->training_venue) selected @endif>{{$venue}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label class="font-lg">প্রশিক্ষণ শিরোনাম</label>
                                            <input name="training_title" id="training_title" type="text" class="form-control input" value="{{@$batch_data->training_title}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">প্রশিক্ষণের তারিখ</label>
                                            <div class="input-group input date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control" name="batch_start_date" id="batch_start_date" readonly value="{{\Carbon\Carbon::parse(@$batch_data->batch_start_date)->format('d/m/Y')}}">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control" name="batch_end_date" id="batch_end_date" readonly value="{{\Carbon\Carbon::parse(@$batch_data->batch_end_date)->format('d/m/Y')}}">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label class="font-lg">সময়কাল</label><br>
                                            <label class="badge badge-info" style="font-size: 21px !important; padding: 0px 12px; !important; height: 27px !important;" id="batch_days_duration"></label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">সপ্তাহান্তিক অবকাশ</label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                <label>
                                                    <input type="checkbox" class="form-control" name="include_weekend" id="include_weekend" @if( @$batch_data->include_weekend ) checked @endif />
                                                </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">প্রশিক্ষণের সময়</label>
                                            <div class="input-icon">
                                                <i class="fa fa-clock-o"></i>
                                                <input type="text" class="form-control timepicker timepicker-default" name="batch_start_time" id="batch_start_time" value="{{@$batch_data->batch_start_time}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">প্রশিক্ষণের সময়</label>
                                            <div class="input-icon">
                                                <i class="fa fa-clock-o"></i>
                                                <input type="text" class="form-control timepicker timepicker-default" name="batch_end_time" id="batch_end_time" readonly value="{{@$batch_data->batch_end_time}}">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">দৈর্ঘ্যকাল (ঘন্টা)</label><br>
                                            <label class="badge badge-info" style="font-size: 24px !important; padding: 0px 15px; !important; height: 27px !important;" id="batch_time_length"></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">অবস্থা</label>
                                            <select name="batch_status" id="batch_status" class="form-control input" >
                                                <option value=""> - অবস্থা বাছাই করুন - </option>
                                                @foreach($batch_status_list as $id => $batch_status)
                                                    <option value="{{$id}}" @if($id == @$batch_data->batch_status) selected @endif>{{$batch_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">প্রশিক্ষণের ধরন</label>
                                            <select name="training_type" id="training_type" class="form-control input" >
                                                <option value=""> - ধরন বাছাই করুন - </option>
                                                @foreach($training_type_list as $id => $training_type)
                                                    <option value="{{$id}}" @if($id == @$batch_data->training_type) selected @endif>{{$training_type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">এপিএ কার্যক্রম</label>
                                            <select name="apa_activities" id="apa_activities" class="form-control input" >
                                                <option value=""> - কার্যক্রম বাছাই করুন - </option>
                                                <option value="2.2" @if(@$batch_data->apa_activities == "2.2") selected @endif>[২.২] মন্ত্রণালয়ের সকল প্রকল্প সুষ্ঠুভাবে বাস্তবায়নের লক্ষ্যে 'প্রকল্প ব্যবস্থাপনা ও ই-জিপি' সংক্রান্ত প্রশিক্ষিত জনবল প্রস্তুতকরণ</option>
                                                <option value="2.5" @if(@$batch_data->apa_activities == "2.5") selected @endif>[২.৫] ডিজিটাল বাংলাদেশ বিনির্মাণের লক্ষ্যে কর্মচারীদের কারিগরি দক্ষতা বৃদ্ধি ও সমসাময়িক অন্যান্য প্রশিক্ষণ প্রদান</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">এপিএ কর্মসম্পাদন সূচক</label>
                                            <select name="apa_performance_index" id="apa_performance_index" class="form-control input" >
                                                <option value=""> - কর্মসম্পাদন সূচক বাছাই করুন - </option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-mortar-board"></i>  প্রশিক্ষক এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-2"><label class="font-lg">তারিখ</label></div>
                                        <div class="form-group col-md-3"><label class="font-lg">বিষয়</label></div>
                                        <div class="col-md-1"></div>
                                        <div class="form-group col-md-3"><label class="font-lg">প্রশিক্ষক</label></div>
                                        <div class="col-md-1 form-group" style="width: 9% !important; padding-left: 2px !important; padding-right: 2px !important;"><label class="font-lg">শুরুর সময়</label></div>
                                        <div class="col-md-1 form-group" style="width: 9% !important; padding-left: 2px !important; padding-right: 2px !important;"><label class="font-lg">শেষের সময়</label></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="batch_session_repeater">
                                                <div data-repeater-list="batch_session_data">
                                                    <div data-repeater-item class="form-group row " id="batch_session_details_div" style="margin-bottom: 0px !important;">
                                                        <div class="col-md-2">
                                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy" data-date-start-date="+0d">
                                                                <input type="text" class="form-control input-sm" name="session_date" readonly>
                                                                <span class="input-group-btn">
                                                                        <button class="btn default" type="button" style="padding: 7px 9px !important;">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm" id="session_topic" required name="session_topic" placeholder="">
                                                        </div>
                                                        <div class="col-md-1" style="padding-left: 1px !important; padding-right: 1px !important;" id="trainer_type_div">
                                                            <input type="checkbox" class="trainer_type" name="trainer_type" value="1" checked>
                                                            <span id="trainer_type_lbl">নিজ অফিস</span>
                                                        </div>
                                                        <div class="col-md-3" id="own_office_div">
                                                            <div id="own_office">
                                                                <select name="employee_id" id="employee_id" class="form-control input-sm">
                                                                    <option value=""> - প্রশিক্ষক বাছাই করুন - </option>
                                                                    @foreach($trainer_list as $id => $trainer)
                                                                        <option value="{{$id}}" @if($id == @$trainer) selected @endif>{{$trainer}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div id="other_office">
                                                                <input type="text" class="input-sm form-control" name="trainer_name" id="trainer_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 form-group" style="width: 9% !important; padding-left: 2px !important; padding-right: 2px !important;">
                                                            <div class="input-icon">
                                                                <i class="fa fa-clock-o"></i>
                                                                <input type="text" class="input-sm form-control timepicker timepicker-default session_start_time" name="session_start_time" id="session_start_time" value="{{@$batch_data->batch_start_time}}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 form-group" style="width: 9% !important; padding-left: 2px !important; padding-right: 2px !important;">
                                                            <div class="input-icon">
                                                                <i class="fa fa-clock-o"></i>
                                                                <input type="text" class="input-sm form-control timepicker timepicker-default session_end_time" name="session_end_time" id="session_end_time" value="{{@$batch_data->batch_start_time}}" readonly>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-1" style="width: 6% !important; padding-top: 6px; padding-left: 4px !important; padding-right: 2px !important;">
                                                            <div data-repeater-delete="" class="text-danger">
                                                                <i class="fa fa-2x fa-remove"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <div class="col-md-4 ">
                                                        <div href="javascript:;" data-repeater-create="" class="add_new_session">
                                                            <label class="badge badge-info"><i class="la la-plus"></i> আরও যোগ করুন </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-save btn-success font-hg col-md-2"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
                        {{--<button class="btn btn-cancel"><i class="fa fa-reply-all"></i> ব্যাচের তালিকায় ফেরৎ আসুন</button>--}}
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-edit font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase" style="font-size: 22px !important;">প্রয়োজনীয় দলিল</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="note note-success" style="padding: 5px 30px 55px 15px !important;">
                                <button type="submit" class="btn grey-mint btn-outline col-md-12 btn-no-border bg-hover-blue " style="font-size: 26px !important;" name="attandanceBtn" value="1">
                                    <i class="fa fa-list-ol "></i> হাজিরা শীট
                                </button>
                            </div>
                            <div class="note note-warning" style="padding: 5px 30px 55px 15px !important;">
                                <button type="submit" class="btn grey-mint btn-outline col-md-12 btn-no-border bg-hover-yellow "  style="font-size: 26px !important;" name="honarariumBtn"  value="1">
                                    <i class="fa fa-money"></i> সম্মানী শীট
                                </button>
                            </div>
                            <div class="note note-info" style="padding: 5px 30px 55px 15px !important;">
                                <button type="submit" class="btn grey-mint btn-outline col-md-12 btn-no-border bg-hover-purple-plum "  style="font-size: 26px !important;" name="scheduleBtn"  value="1">
                                    <i class="fa fa-hourglass-2"></i> প্রশিক্ষণের সময়সূচী
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-mouse-pointer "></i>
                                <span class="caption-subject font-dark bold uppercase" style="font-size: 22px !important;">প্রমাণক</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row margin-bottom-20 margin-left-40 ">
                                @if (@$batch_data->training_letter_file)
                                <div class="col-md-3 text-center">
                                    <a href="#attachment_view_modal" class="attachment_view" data-toggle="modal" data-ref="{{@$batch_data->training_letter_file}}"
                                       data-modal_title="প্রশিক্ষণের পত্র">
                                        <i class="fa fa-file-pdf-o fa-2x font-red-sunglo "></i>
                                        <label class="badge badge-warning  margin-top-10 text-dark font-lg">প্রশিক্ষণের পত্র</label>
                                    </a>
                                </div>
                                @endif
                                @if (@$batch_data->attandance_sheet_file)
                                <div class="col-md-3 text-center">
                                    <a href="#attachment_view_modal"  class="attachment_view" data-toggle="modal" data-ref="{{@$batch_data->attandance_sheet_file}}"
                                       data-modal_title="প্রশিক্ষণের হাজীরা">
                                        <i class="fa fa-file-pdf-o fa-2x font-red-sunglo"></i>
                                        <label class="badge badge-warning margin-top-10 text-dark">&nbsp;&nbsp;হাজীরা শীট&nbsp;&nbsp;</label>
                                    </a>
                                </div>
                                @endif
                                @if (@$batch_data->honorarium_sheet_file)
                                <div class="col-md-3 text-center">
                                    <a href="#attachment_view_modal"  class="attachment_view" data-toggle="modal" data-ref="{{@$batch_data->honorarium_sheet_file}}"
                                       data-modal_title="প্রশিক্ষণের সম্মানী">
                                        <i class="fa fa-file-pdf-o fa-2x font-red-sunglo"></i>
                                        <label class="badge badge-warning  margin-top-10 text-dark ">&nbsp;&nbsp;সম্মানী শীট&nbsp;&nbsp;</label>
                                    </a>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> প্রশিক্ষণের পত্র বাছাই করুন </span>
                                                <span class="fileinput-exists"> পরিবর্তন করুন </span>
                                                <input type="file" name="training_letter_file">
                                            </span>
                                            <span class="fileinput-filename"> </span> &nbsp;
                                            <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> হাজীরা শীট বাছাই করুন </span>
                                                <span class="fileinput-exists"> পরিবর্তন করুন </span>
                                                <input type="file" name="attandance_sheet_file">
                                            </span>
                                            <span class="fileinput-filename"> </span> &nbsp;
                                            <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> সম্মানী শীট বাছাই করুন </span>
                                                <span class="fileinput-exists"> পরিবর্তন করুন </span>
                                                <input type="file" name="honorarium_sheet_file">
                                            </span>
                                            <span class="fileinput-filename"></span> &nbsp;
                                            <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cog"></i>
                                <span class="caption-subject font-dark bold uppercase"  style="font-size: 22px !important;">লগ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
            <!-- AUDIT LOG END -->
        </div>
    </form>

    <!-- Attachment View Modal STARTS -->
    <div id="attachment_view_modal" class="modal fade in">
        <div class="modal-dialog modal-lg modal-scroll"  style="max-height:700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="attachment_view_modal_title"></h4>
                </div>
                <div class="modal-body" id="attachment_view">
                    <iframe id="attachment_iframe" src="" width="100%" height="70%" frameborder="0" ></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Attachment View Modal ENDS -->

    <!-- SMS Modal STARTS -->
    <div class="modal fade" id="sms_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"> বার্তা </h4>
                </div>
                <div class="modal-body" id="">

                    <div class="row">
                        <div class="col-md-5">
                            <textarea  class="form-control input" style="height: 180px;" id="sms_text"></textarea>
                        </div>
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sms_sending" data-percent="0">
                                    <span class="bold sms_sent_number" style="top: 25px; position: relative; left: 25px; font-size: 40px;">0</span>&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible="1">
                                <ul class="feeds">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn red-flamingo btn-outline font-lg" data-dismiss="modal">বন্ধ করুন</button>
                    <button type="button" class="btn green-haze btn-outline font-lg" id="send_sms_btn">বার্তা প্রেরণ করুন</button>
                </div>
            </div>
        </div>
    </div>

    <!-- SMS Modal ENDS -->
@endsection

@section('page_plugins')


@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/batch.js" type="text/javascript"></script>
@endsection

<script>
    var trainer_array       = [];

    @if (@$batch_trainer_data)
    @foreach($batch_trainer_data as $trainer)
        @php $trainer->session_date = \Carbon\Carbon::parse(@$trainer->session_date)->format('d/m/Y'); @endphp
        trainer_array.push ({!! json_encode($trainer) !!});
    @endforeach
    @endif

    var apa_performance_index_val = "{!! @$batch_data->apa_performance_index !!}";

</script>