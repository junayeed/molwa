<div class="modal-header">
    <h3 class="modal-title" id="forwardModalLabel" style="font-size: 2.5rem;">
        প্রশিক্ষণার্থী
    </h3>
    <button type="button" class="close btn_participant_close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="employee_list">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title" style="padding: 0px !important;">
                    <div class="caption col-md-12">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase" style="font-size: 2.5rem;">ব্যাচ নং - {{Html::en2bn( $batch_data->batch_no )}}</span>
                        <div class="caption-desc font-grey-cascade bold" style="font-size: 1.75rem;"> বিষয়ঃ {{$batch_data->training_title}} </div>
                    </div>
                </div>
                <div class="portlet-title" style="padding: 0px !important;">
                    <div class="caption col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="list-head-title-container">
                                    <select name="designation" id="designation" class="form-control input" data-batch_id="{{@$batch_data->id}}">
                                        <option value=""> - পদবী বাছাই করুন - </option>
                                        @foreach($designation_list as $id => $designation)
                                            <option value="{{$id}}" @if ($id == @$srch_designation) selected @endif>{{$designation}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="list-head-title-container">
                                    <select name="office" id="office" class="form-control input" data-batch_id="{{@$batch_data->id}}">
                                        <option value=""> - দপ্তর বাছাই করুন - </option>
                                        @foreach($office_list as $id => $office)
                                            <option value="{{$id}}" @if($id == @$srch_office) selected @endif>{{$office}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="list-head-title-container">
                                    <select name="employee_class" id="employee_class" class="form-control input" data-batch_id="{{@$batch_data->id}}">
                                        <option value=""> - শ্রেনী বাছাই করুন - </option>
                                        @foreach($class_list_bn as $id => $class)
                                            <option value="{{$class_list_en[$id]}}" @if($class_list_en[$id] == @$srch_employee_class) selected @endif>{{$class}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-element-list">
                        {{--<div class="mt-list-head list-default ext-1 font-dark bg-yellow-crusta">

                        </div>--}}
                        {{--<div class="mt-list-head list-default ext-1 font-dark bg-yellow-crusta">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title"></h3>
                                        <div class="list-date"></div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="list-head-summary-container">
                                        <div class="list-pending">
                                            <div class="list-count">3</div>
                                            <div class="list-label">Pending</div>
                                        </div>
                                        <div class="list-done">
                                            <div class="list-count last">2</div>
                                            <div class="list-label">Completed</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}

                        @if ($employee_list)
                            @foreach($employee_list AS $emp_class => $employee)
                            <div class="mt-list-head list-news ext-1 font-white bg-grey-gallery">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">{{Html::formatEmployeeClass($emp_class)}}</h3>
                                </div>
                                <div class="list-count pull-right bg-red font-lg" style="padding: 9px !important; font-size: 24px !important;">{{Html::en2bn( count($employee) )}}</div>
                            </div>
                            <div class="mt-list-container list-news ext-1">
                                <ul>
                                    @foreach( $employee AS $emp)
                                    <li class="mt-list-item" style="padding: 9px !important;">
                                        <div class="list-icon-container">
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="employee_id" value="{{$emp->employee_id}}"
                                                               data-batch_id="{{$batch_data->id}}" @if( in_array($emp->employee_id, $participant_list) ) checked @endif
                                                               data-emp_profile_id="{{$emp->emp_profile_id}}">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt="" src="{{$emp->profile_image}}" style="width: 50px;" />
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-lg" style="margin-bottom: 6px !important;"> {{$emp->name_bn}} </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">{{$emp->designation}}, {{$emp->office_name_bn}}</a>
                                            </h3>
                                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum gravida mauris, a tincidunt dolor porttitor eu. </p>--}}
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        @else
                            দুঃখিত! কোন তথ্য পাওয়া যায়নি ।
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-default btn_participant_close" id="" data-dismiss="modal">
        <i class="fa fa-window-close"> বন্ধ করুন</i>
    </button>
</div>
<script>
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        increaseArea: '40%' // optional
    });
</script>
