<?php

Route::group(['prefix' => 'batch', 'namespace' => 'App\Modules\Local\Batch\Controllers', 'middleware' => ['web']], function () {
    Route::post('/',                           ['as' => 'batch.index',  'uses' => 'BatchController@index']);
    Route::get('/',                            ['as' => 'batch.index',  'uses' => 'BatchController@index']);
    Route::get('/show/{id}',                   ['as' => 'batch.show',   'uses' => 'BatchController@show']);
    Route::get('/add',                         ['as' => 'batch.index',  'uses' => 'BatchController@add']);
    Route::get('/update/{id}',                 ['as' => 'batch.index',  'uses' => 'BatchController@update']);
    Route::post('/save',                       ['as' => 'batch.index',  'uses' => 'BatchController@save']);
    Route::get('/delete/{id}',                 ['as' => 'batch.index',  'uses' => 'BatchController@delete']);
    Route::get('/getParticipantDetails/',      ['as' => 'batch.index',  'uses' => 'BatchController@getParticipantDetails']);
    Route::get('/saveBatchParticipant/',       ['as' => 'batch.index',  'uses' => 'BatchController@saveBatchParticipant']);
    Route::get('/removeBatchParticipant/',     ['as' => 'batch.index',  'uses' => 'BatchController@removeBatchParticipant']);
    Route::get('/sendSMS/',                    ['as' => 'batch.index',  'uses' => 'BatchController@sendSMS2BatchParticipans']);
});




?>