<div class="modal-header">
    <h3 class="modal-title" id="forwardModalLabel" style="font-size: 2.5rem;">
        ক্ষুদেবার্তা প্রেরণ
    </h3>
    <button type="button" class="close btn_participant_close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12" id="employee_list">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title" style="padding: 0px !important;">
                    <div class="caption col-md-12">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase" style="font-size: 2.5rem;">প্রতিষ্ঠানের নাম: {{$book_dis_data->book_distribution_inst_name}}</span>
                        <div class="caption-desc font-grey-cascade bold" style="font-size: 1.75rem;"> বিষয়: {{$book_dis_data->book_distribution_subject}} </div>
                        <div class="caption-desc font-grey-cascade bold" style="font-size: 1.75rem;"> বই গ্রহণকারীর নাম: {{$book_dis_data->book_receiver_name}} </div>
                        <div class="caption-desc font-grey-cascade bold" style="font-size: 1.75rem;"> বই গ্রহণকারীর মোবাইল নম্বর: {{$book_dis_data->book_receiver_cell}} </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <textarea cols="80" rows="8" class="form-control input-lg">আঁপনার প্রতিষ্ঠানের জন্য বরাদ্দকৃত {{Html::en2bn($total_book_distributed->total_book_distributed)}}টি বই সরবরাহের জন্য প্রস্তুত আছে। দয়া করে মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়ে যোগাযোগ করুন। </textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-success" >
        <i class="fa fa-window-close"> ক্ষুদেবার্তা প্রেরণ করুন</i>
    </button>
    <button type="button" class="btn btn-sm btn-default btn_participant_close" id="" data-dismiss="modal">
        <i class="fa fa-window-close"> বন্ধ করুন</i>
    </button>
</div>