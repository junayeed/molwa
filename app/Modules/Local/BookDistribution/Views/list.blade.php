@extends('layouts.master')
@section('content')
    <form method="POST" action="/batch">
    {{csrf_field()}}
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <!-- TOP RIGHT MENU STARTS HERE -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="actions">
                                <div class="btn-group">
                                    <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                       data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="/Book/add" class="font-lg">
                                                <i class="fa fa-plus font-green-jungle"></i> নতুন বই নিবন্ধন করুন
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/BookDistribution/add" class="font-lg">
                                                <i class="fa fa-plus font-green-jungle"></i> নতুন বই বিতরণ
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- TOP RIGHT MENU ENDS HERE -->
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                        <div id="m_calendar" class="fc fc-unthemed fc-ltr">
                            <div class="fc-view-container" style="">
                                <div class="fc-view fc-listWeek-view fc-list-view fc-widget-content" style="">
                                    <div class="fc-scroller" style="overflow: hidden auto; height: auto;">
                                        <table class="fc-list-table" style="font-size: 1.5em !important;">
                                            <tbody>
                                                <tr class="fc-list-heading">
                                                    <td class="fc-widget-header" colspan="6">
                                                        <a class="fc-list-heading-main">বই এর তালিকা</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    {{--<td>ক্রমিক</td>--}}
                                                    <td>প্রতিষ্ঠানের নাম</td>
                                                    <td>স্মারক</td>
                                                    <td>তারিখ</td>
                                                    <td>বই এর সংখ্যা</td>
                                                    <td>অবস্থা</td>
                                                    <td>Action</td>
                                                </tr>

                                            @if($books)
                                                @foreach($books as $book)
                                                <tr class="fc-list-item" >
                                                    <td class="fc-list-item-time fc-widget-content font-lg">
                                                        <a href="/BookDistribution/update/{{$book->id}}">{{$book->book_distribution_inst_name}}</a>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{$book->book_distribution_ref }}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{Html::en2bn(\Carbon\Carbon::parse($book->book_distribution_date)->format('d/m/Y') )}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{$book->total_book_distributed}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">
                                                            @if ($book->distribution_status == "Delivered")
                                                                <label class="label bg-green-dark-opacity ">বিতরণকৃত</label>
                                                            @elseif ($book->distribution_status == "Not Delivered")
                                                                <label class="label bg-red-flamingo">বিতরণ করা হয়নি</label>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td class="text-left no-print">
                                                        <a class="" href="/BookDistribution/update/{{$book->id}}"><i class="fa fa-edit"></i></a> &nbsp;
                                                        <a class="" href="/BookDistribution/book_distribution_letter_pdf/{{$book->id}}"><i class="fa fa-file"></i></a>
                                                        <a href="javascript:void(0)" class="book_sms" data-book_id="{{$book->id}}" data-toggle="modal" data-target="#open_book_sms_modal"><i class="fa fa-send-o"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else
                                                <tr class="fc-list-item m-fc-event--danger">
                                                    <td class="fc-list-item-time fc-widget-content font-lg text-danger ">কোন তথ্য পাওয়া যায়নি</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </form>
    <!-- END CONTENT -->


    <div class="modal fade" id="open_book_sms_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="open_book_sms_box"></div>
        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/BookDistribution.js" type="text/javascript"></script>
@endsection

<script>
    var book_dis_array = [];

</script>