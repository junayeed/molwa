@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')

    <form role="form" id="batch_form" method="POST" action="/BookDistribution/save"  enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" value="{{@$book_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- TOP RIGHT MENU STARTS HERE -->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                               data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="/BookDistribution/book_distribution_letter_pdf/{{@$book_data->id}}" class="font-lg" >
                                        <i class="fa fa-mobile font-green-jungle"></i> বই বিতরণ পত্র তৈরী করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="font-lg" data-batch_id="{{@$book_data->id}}" data-toggle="modal" data-target="#sms_modal">
                                        <i class="fa fa-mobile font-green-jungle"></i> বার্তা প্রেরণ করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="/Book/add" class="font-lg">
                                        <i class="fa fa-plus font-green-jungle"></i> নতুন বই সংযোজন করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="/Book" class="font-lg"> <i class="fa fa-close font-red-flamingo"></i> বন্ধ করুন </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TOP RIGHT MENU ENDS HERE -->
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> বিতরণ এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">বিষয়</label>
                                            <input name="book_distribution_subject" id="book_distribution_subject" type="text" class="form-control input-lg " value="{{@$book_data->book_distribution_subject}}" >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">প্রতিষ্ঠান/লাইব্রেরির নাম</label>
                                            <input name="book_distribution_inst_name" id="book_distribution_inst_name" type="text" class="form-control input-lg" value="{{@$book_data->book_distribution_inst_name}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">স্মারক নম্বর</label>
                                            <input name="book_distribution_ref" id="book_distribution_ref" type="text" class="form-control input-lg" value="{{@$book_data->book_distribution_ref}}" >
                                        </div>
                                        <div class="form-group col-md-2">
                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                <label class="font-lg">তারিখ</label>
                                                <input type="text" class="form-control input-lg" name="book_distribution_date"  id="book_distribution_date" value="{{@$book_data->book_distribution_date}}" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 15px 12px !important; margin-top: 29px  !important; border: 1px solid #BEBEBE !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                         </div>
                                        <div class="form-group col-md-2">
                                            <div class="input-group">
                                                <label class="font-lg">অবস্থা </label>
                                                <select name="distribution_status" id="distribution_status" class="form-control input-lg">
                                                    <option value=""> - বই বাছাই করুন - </option>
                                                    @foreach($book_dist_status_list as $id => $status)
                                                        <option value="{{$id}}" @if($id == @$book_data->distribution_status) selected @endif>{{$status}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">বই গ্রহণকারীর নাম</label>
                                            <input name="book_receiver_name" id="book_receiver_name" type="text" class="form-control input-lg " value="{{@$book_data->book_receiver_name}}" >
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">বই গ্রহণকারীর মোবাইল নম্বর</label>
                                            <input name="book_receiver_cell" id="book_receiver_cell" type="text" class="form-control input-lg" value="{{@$book_data->book_receiver_cell}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">বই সরবরাহকারীর নাম</label>
                                            <input name="book_distributor_name" id="book_distributor_name" type="text" class="form-control input-lg " value="{{@$book_data->book_distributor_name}}" >
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">বই সরবরাহকারীর মোবাইল নম্বর</label>
                                            <input name="book_distributor_cell" id="book_distributor_cell" type="text" class="form-control input-lg" value="{{@$book_data->book_distributor_cell}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-book"></i> বই এর বিস্তারিত তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-3"><label class="font-lg">বই এর নাম</label></div>
                                        <div class="form-group col-md-3"><label class="font-lg">লেখক</label></div>
                                        <div class="form-group col-md-3"><label class="font-lg">প্রকাশক</label></div>
                                        <div class="form-group col-md-1"><label class="font-lg">বই এর সংখ্যা</label></div>
                                        <div class="form-group col-md-1"><label class="font-lg">বিতরণ</label></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="books_repeater">
                                                <div data-repeater-list="books_data">
                                                    <div data-repeater-item class="form-group row " id="books_details_div" style="margin-bottom: 0px !important;">
                                                        <div class="col-md-3 margin-bottom-10" id="book_name">
                                                            <select name="book_id" id="book_id" class="form-control input book_distribution">
                                                                <option value=""> - বই বাছাই করুন - </option>
                                                                @foreach($book_list as $id => $book)
                                                                    <option value="{{$id}}" @if($id == @$book) selected @endif>[{{$id}}] - {{$book}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 font-lg" id="writer_div"></div>
                                                        <div class="col-md-3 font-lg" id="publisher_div"></div>
                                                        <div class="col-md-1 font-lg" id="no_copy_div"></div>
                                                        <div class="col-md-1 font-lg" id="dist_book_copy_div">
                                                            <input type="text" name="dist_book_copy" id="dist_book_copy" value="" class="input form-control no_book_copy">
                                                        </div>
                                                        <div class="col-md-1" style="width: 6% !important; padding-top: 6px; padding-left: 4px !important; padding-right: 2px !important;">
                                                            <div data-repeater-delete="" class="text-danger">
                                                                <i class="fa fa-2x fa-remove remove_book"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <div class="col-md-4 margin-top-10">
                                                        <div href="javascript:;" data-repeater-create="" class="add_new_book_distribution">
                                                            <label class="badge badge-info"><i class="la la-plus"></i> আরও যোগ করুন </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-save btn-success font-hg col-md-2"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
                        {{--<button class="btn btn-cancel"><i class="fa fa-reply-all"></i> ব্যাচের তালিকায় ফেরৎ আসুন</button>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> মোট বিতরণকৃত বই
                                    </div>
                                </div>
                                <div class="portlet-body font-lg text-center" id="total_book_distribution">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
        </div>
    </form>

    <!-- Attachment View Modal STARTS -->
    <div id="attachment_view_modal" class="modal fade in">
        <div class="modal-dialog modal-lg modal-scroll"  style="max-height:700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="attachment_view_modal_title"></h4>
                </div>
                <div class="modal-body" id="attachment_view">
                    <iframe id="attachment_iframe" src="" width="100%" height="70%" frameborder="0" ></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Attachment View Modal ENDS -->

    <!-- SMS Modal STARTS -->
    <div class="modal fade" id="sms_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"> বার্তা </h4>
                </div>
                <div class="modal-body" id="">

                    <div class="row">
                        <div class="col-md-5">
                            <textarea  class="form-control input" style="height: 180px;" id="sms_text"></textarea>
                        </div>
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sms_sending" data-percent="0">
                                    <span class="bold sms_sent_number" style="top: 25px; position: relative; left: 25px; font-size: 40px;">0</span>&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible="1">
                                <ul class="feeds">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn red-flamingo btn-outline font-lg" data-dismiss="modal">বন্ধ করুন</button>
                    <button type="button" class="btn green-haze btn-outline font-lg" id="send_sms_btn">বার্তা প্রেরণ করুন</button>
                </div>
            </div>
        </div>
    </div>

    <!-- SMS Modal ENDS -->
@endsection

@section('page_plugins')


@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/BookDistribution.js" type="text/javascript"></script>
@endsection

<script>
    var book_dis_array = [];

    @if (@$book_dis_data)
        @foreach($book_dis_data as $book)
            {{--@php $trainer->session_date = \Carbon\Carbon::parse(@$trainer->session_date)->format('d/m/Y'); @endphp--}}
            book_dis_array.push ({!! json_encode($book) !!});
        @endforeach
    @endif

    var apa_performance_index_val = "{!! @$book_data->apa_performance_index !!}";

</script>