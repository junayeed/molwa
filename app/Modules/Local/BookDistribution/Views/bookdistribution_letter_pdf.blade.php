<div>
    <table align="center" style="width: 100%; border: 0px solid red;">
        <tbody>
        <tr>
            <td align="center">গণপ্রজাতন্ত্রী বাংলাদেশ সরকার</td>
        </tr>
        <tr>
            <td align="center" style="font-size: 20px; ">মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়</td>
        </tr>
        <tr>
            <td align="center">পরিবহন পুল ভবন, সচিবালয় সংযোগ সড়ক,ঢাকা</td>
        </tr>
        <tr>
            <td align="center">(ইতিহাস সংরক্ষণ, গবেষণা ও প্রকাশনা অধিশাখা)</td>
        </tr>
        <tr>
            <td align="center">molwa.gov.bd</td>
        </tr>
        </tbody>
    </table>
</div>
<p></p>
<table style="width: 100%; border: 0px solid red;">
    <tbody>
        <tr>
            <td width="70%">স্মারক নম্বর: {{$book_data->book_distribution_ref}}</td>
            <td width="30%" align="right">তারিখ: {{Html::en2bn(\Carbon\Carbon::parse($book_data->book_distribution_date)->format('d/m/Y'))}}</td>
        </tr>
        <tr>
            <td colspan="2">বিষয়: {{$book_data->book_distribution_subject}}</td>
        </tr>
        <tr>
            <td colspan="2">প্রতিষ্ঠান/লাইব্রেরির নাম: {{$book_data->book_distribution_inst_name}}</td>
        </tr>
    </tbody>
</table>
<p></p>
<table style="width: 100%;" border="1">
    <tbody>
        <tr>
            <td colspan="4" style="text-align: center;border: 0px solid red; font-size: 20px; "><u>বিতরণকৃত বইয়ের তালিকা</u></td>
        </tr>
        <tr>
            <td style="border: 1px solid red;">ক্রমিক</td>
            <td style="border: 1px solid red;">বইয়ের নাম</td>
            <td style="border: 1px solid red;">লেখক/সম্পাদকের নাম</td>
            <td style="border: 1px solid red;">বইয়ের সংখ্যা</td>
        </tr>
        <?php $serial = 1; $total_book = 0; ?>
        @foreach($book_dis_data as $dis_data)
            <?php $total_book += $dis_data->book_distributed; ?>
            <tr>
                <td style="border: 1px solid red;">{{Html::en2bn($serial++)}}</td>
                <td style="border: 1px solid red;">{{@$dis_data->book_title_bn}}</td>
                <td style="border: 1px solid red;">{{@$dis_data->writer}}</td>
                <td align="center" style="border: 1px solid red;">{{Html::en2bn(@$dis_data->book_distributed)}}</td>
            </tr>
        @endforeach

        <tr>
            <td colspan="3" align="right" style="border: 1px solid red;">মোট = </td>
            <td align="center" style="border: 1px solid red;">{{Html::en2bn($total_book)}}</td>
        </tr>
    </tbody>
</table>
<p></p>
<table style="width: 100%; border: 0px solid red;">
    <tbody>
        <tr>
            <td colspan="4" style="border: 0px solid red; font-size: 16px;">তালিকায় বর্ণিত {{Html::en2bn($total_book)}}কপি বই বুঝিয়া পাইলাম।</td>
        </tr>
        <tr>
            <td width="17%" height="50px" style="vertical-align: bottom;">বই গ্রহণকারীর স্বাক্ষর:</td>
            <td></td>
            <td width="18%" height="50px" style="vertical-align: bottom;">বই সরবরাহকারীর স্বাক্ষর:</td>
            <td></td>
        </tr>
        <tr>
            <td height="50px" style="vertical-align: bottom;">বই গ্রহণকারীর নাম:</td>
            <td style="vertical-align: bottom;">{{$book_data->book_receiver_name}}</td>
            <td height="50px" style="vertical-align: bottom;">বই সরবরাহকারীর নাম:</td>
            <td style="vertical-align: bottom;">{{$book_data->book_distributor_name}}</td>
        </tr>
        <tr>
            <td height="50px" style="vertical-align: bottom;">মোবাইল নম্বর:</td>
            <td style="vertical-align: bottom;">{{$book_data->book_receiver_cell}}</td>
            <td height="50px" style="vertical-align: bottom;">মোবাইল নম্বর:</td>
            <td style="vertical-align: bottom;">{{$book_data->book_distributor_cell}}</td>
        </tr>
    </tbody>
</table>

