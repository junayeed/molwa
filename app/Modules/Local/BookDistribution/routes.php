<?php

Route::group(['prefix' => 'BookDistribution', 'namespace' => 'App\Modules\Local\BookDistribution\Controllers', 'middleware' => ['web']], function () {
    Route::post('/',                                   ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@index']);
    Route::get('/',                                    ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@index']);
    Route::get('/show/{id}',                           ['as' => 'bookdistribution.show',   'uses' => 'BookDistributionController@show']);
    Route::get('/add',                                 ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@add']);
    Route::get('/update/{id}',                         ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@update']);
    Route::post('/save',                               ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@save']);
    Route::get('/delete/{id}',                         ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@delete']);
    Route::get('/getBookDetails/',                     ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@getBookDetails']);
    Route::get('/removeBatchParticipant/',             ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@removeBatchParticipant']);
    Route::get('/sendSMS/',                            ['as' => 'bookdistribution.index',  'uses' => 'BookDistributionController@sendSMS2BatchParticipans']);


    Route::get('/book_distribution_letter_pdf/{id}',   ['as' => 'bookdistribution.book_distribution_letter_pdf',   'uses' => 'BookDistributionController@update']);
    Route::get('/showSendBookSMS/',                ['as' => 'bookdistribution.index',                          'uses' => 'BookDistributionController@showSendBookSMS']);
});




?>