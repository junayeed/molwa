<?php
namespace App\Modules\Local\BookDistribution\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class BookDistributionModel extends Model
{
    protected $table = 'book_distribution';

    protected $fillable = ['book_distribution_subject', 'book_distribution_ref', 'book_distribution_date', 'book_distribution_inst_name', 'financial_year', 'created_by',
                           'book_receiver_name', 'book_receiver_cell', 'book_distributor_cell', 'book_distributor_name', 'distribution_status'];
}
