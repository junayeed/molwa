<?php
namespace App\Modules\Local\ForeignTraining\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ForeignTrainingModel extends Model
{
    protected $table     = 'foreign_trainings';
    //public $timestamps   = false;

    protected $fillable  = ['memo_no', 'memo_date', 'financial_year', 'country', 'start_date', 'end_date', 'go_attachment', 'financial_source'];
}
