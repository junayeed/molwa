<?php

Route::group(['prefix' => 'ForeignTraining', 'namespace' => 'App\Modules\Local\ForeignTraining\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',                                ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@index']);
    Route::post('/',                               ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@index']);
    Route::get('/show/{id}',                       ['as' => 'ForeignTraining.show',        'uses' => 'ForeignTrainingController@show']);
    Route::get('/add',                             ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@add']);
    Route::get('/update/{id}',                     ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@update']);
    Route::post('/save',                           ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@save']);
    Route::get('/delete/{id}',                     ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@delete']);
    Route::get('/getParticipantList/',             ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@getParticipantList']);
    Route::get('/getParticipantDetails/{emp_ids}', ['as' => 'ForeignTraining.index',       'uses' => 'ForeignTrainingController@getParticipantDetails']);
});
?>