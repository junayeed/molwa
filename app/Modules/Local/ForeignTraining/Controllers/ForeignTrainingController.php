<?php
namespace App\Modules\Local\ForeignTraining\Controllers;

use App\Modules\Local\Batch\Models\BatchModel;
use App\Modules\Local\Employee\Models\EmployeeModel;
use App\Modules\Local\ForeignTraining\Models\ForeignTrainingModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Designation;


class ForeignTrainingController extends Controller
{
    function __construct(){

	}

    function index(Request $request) {
        $search_data                = $request->only(['country']);DB::enableQueryLog();

        //echo "<pre>"; print_r($search_data);die;

        $ft_query   = ForeignTrainingModel::select('foreign_trainings.id', 'memo_no', 'memo_date', 'foreign_trainings.financial_year',
                                                           'start_date', 'country', 'go_attachment', 'financial_source',
                                                           'end_date', DB::raw("COUNT(FTE.employee_id) AS total_employee"), 'foreign_trainings.financial_year')
                                          ->leftJoin('foreign_training_emp AS FTE', 'FTE.ft_id', '=', 'foreign_trainings.id')
                                          ->leftJoin('financial_years AS FY', 'FY.id', '=', 'foreign_trainings.financial_year')
                                          ->groupBy('memo_no', 'foreign_trainings.financial_year')
                                          ->orderBy('foreign_trainings.financial_year', 'DESC');

        if ( isset($search_data['country']) && !empty($search_data['country']) )          $ft_query->where('foreign_trainings.country', 'LIKE', '%,'.$search_data['country'].'%')->orwhere('foreign_trainings.country', 'LIKE', '%'.$search_data['country'].',%')->orwhere('foreign_trainings.country', 'LIKE', $search_data['country']);

        $data                       = $ft_query->get()->all();//->toArray();//dd(DB::getQueryLog());
        $ft_employee_list           = DB::table('foreign_training_emp')->get()->all();
        $country_list               = DB::table('country')->select('id', 'nicename_bn')->whereNotNull('nicename_bn')->pluck('nicename_bn', 'id');
        $fy_list                    = DB::table('financial_years')->select('id', 'financial_year')->pluck('financial_year', 'id');
        $designation_list           = DB::table('designations')->select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');

        if ( @$data ) {
            foreach($data AS $d) {
                $ft_data[$d->financial_year][] = $d;
            }
        }
        //echo "<pre>"; print_r($ft_data); die;
        if ($ft_employee_list) {
            foreach ($ft_employee_list as $emp) {
                $ft_employee[$emp->ft_id][] = $emp->employee_id;
            }
        }

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;


        return view('Local/ForeignTraining::list', ['ft_data' => $ft_data, 'data' => $data, 'country_list' => $country_list,
                                                         'fy_list' => $fy_list, 'designation_list' => $designation_list,
                                                         'ft_employee' => @$ft_employee]);
    }

    function add() {
        $country_list               = DB::table('country')->select('id', 'nicename_bn', 'iso')->whereNotNull('nicename_bn')->get()->all();
        $fy_list                    = DB::table('financial_years')->select('id', 'financial_year')->pluck('financial_year', 'id');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/ForeignTraining::form', ['data' => $data, 'country_list' => $country_list, 'fy_list' => $fy_list]);
    }

    function update($id) {
        $ft_data                      = ForeignTrainingModel::find($id);
        $ft_emp_list                  = implode(',', DB::table('foreign_training_emp AS FTE')->select('employee_id')->where('FTE.ft_id', $id)->pluck('employee_id', 'employee_id')->toArray() );
        $country_list                 = DB::table('country')->select('id', 'nicename_bn', 'iso')->whereNotNull('nicename_bn')->get()->all();//->pluck('nicename_bn', 'id');
        $fy_list                      = DB::table('financial_years')->select('id', 'financial_year')->pluck('financial_year', 'id');
        //$auditLogData                 = \Common::getAuditLogData('Designation', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        $data['nav_item']             = str_replace("Controller", "", $controller);
        list($controller, $action)    = explode('@', $controller);
        $data['nav_sub_item']         = $action;

        //echo "<pre>"; print_r($country_list); die;

        return view('Local/ForeignTraining::form', ['ft_data' => $ft_data, 'country_list' => $country_list, 'fy_list' => $fy_list, 'emp_ids' => $ft_emp_list]);
    }

    function save(Request $request) {

        $ft_data                   = $request->only(['memo_no', 'memo_date', 'financial_year', 'country', 'start_date', 'end_date', 'go_attachment', 'financial_source']);
        $ft_data['country']        = implode(',', $ft_data['country']);
        $ft_data['memo_date']      = date_format(date_create($ft_data['memo_date']), 'Y-m-d');
        $ft_data['start_date']     = date_format(date_create($ft_data['start_date']), 'Y-m-d');
        $ft_data['end_date']       = date_format(date_create($ft_data['end_date']), 'Y-m-d');
        $ft_data['go_attachment']  = $this->uploadForeignTrainingGO($ft_data['financial_year'], $request);
        $employees                 = explode(',', $request->input('employees') );

        $field_list        = array('memo_no', 'memo_date', 'financial_year', 'country', 'start_date', 'end_date', 'go_attachment');

        if( $request->id ) {
            $foreign_training       = ForeignTrainingModel::find($request->id);
            $old_data               = $foreign_training->toArray();
            if ( !isset($ft_data['go_attachment']) ) {
                $ft_data['go_attachment'] = $foreign_training['go_attachment'];
            }

            $foreign_training->update($ft_data);

            \Common::AuditLog('ForeignTraining', $request->id, $old_data, $ft_data, 'Updated', $field_list);
        }
        else {
            $ft_id = ForeignTrainingModel::create($ft_data)->id;
            foreach ($employees as $emp) {
                $data['ft_id']          = $ft_id;
                $data['employee_id']    = $emp;
                DB::table('foreign_training_emp')->insert($data);
            }
            \Common::getAuditLogData('ForeignTraining', $ft_id, '', '', 'Create', '');
        }

        return redirect('/ForeignTraining');
    }

    public function uploadForeignTrainingGO($financial_year, $request) {
        if ($request->hasFile('go_attachment')) {
            if ($request->file('go_attachment')->isValid()) {
                $file        = $request->file('go_attachment');
                //echo "<pre>"; print_r($file->getClientOriginalName());die;
                $image_name  = $file->getClientOriginalName();
                $path        = public_path("images/foreign_trainings/" . $financial_year . "/");

                $request->file('go_attachment')->move($path, $image_name);

                return "images/foreign_trainings/" . $financial_year . "/" . $image_name;
            }
        }
        else {
            return;
        }
    }

    public function getParticipantDetails($emp_ids) {
        //$employees = explode(',', $request->input('employees'));
        $employees = explode(',', $emp_ids);

        $employee_list = EmployeeModel::select('employee.name_bn', 'cadre_no', 'molwa_joining_date', 'prl_date', 'profile_image', 'designations.name_bn AS designation')
                                      ->leftJoin('employee_profiles AS EP', 'EP.id', '=', 'employee.emp_profile_id')
                                      ->leftJoin('designations', 'designations.id', '=', 'EP.designation')
                                      ->where('employee_status', '=', 'Active')
                                      ->whereIn('employee.id', $employees)
                                      ->orderBy('designations.weight', 'ASC')
                                      ->get()->all();


        return view('Local/ForeignTraining::participant_details', ['employee_list' => @$employee_list]);
    }

    public function getParticipantList(Request $request) {
        $designation      = $request->input('designation');
        $employee_class   = $request->input('employee_class');
        $office           = $request->input('office');

        $employee_query = EmployeeModel::select('employee.name_bn', 'profile_image', 'email', 'ep.employee_class', 'emp_profile_id',
            'designations.name_bn AS designation', 'office_name_bn', 'employee.id AS employee_id')
            ->where('employee_status', 'Active')
            ->where('ep.pay_grade', '<=', 10)
            ->leftJoin('employee_profiles AS ep', 'ep.id', '=', 'employee.emp_profile_id')
            ->leftJoin('designations', 'designations.id', '=', 'ep.designation')
            ->leftJoin('offices', 'offices.id', '=', 'ep.office')
            ->orderBy('designations.weight', 'ASC');
        if ( isset($designation) && !empty($designation) )          $employee_query->where('ep.designation', '=', $designation);
        if ( isset($employee_class) && !empty($employee_class) )    $employee_query->where('ep.employee_class', '=', $employee_class);
        if ( isset($office) && !empty($office) )                    $employee_query->where('ep.office', '=', $office);

        $employee_data    = $employee_query->get()->all();

        //$participant_list = DB::table('batch_participants')->select('id', 'employee_id')->where('batch_id', $batch_id)->get()->pluck('employee_id', 'id')->toArray();
        $designation_list = DB::table('designations')->select('id', 'name_bn')->orderBy('weight', 'ASC')->get()->pluck('name_bn', 'id');
        $office_list      = DB::table('offices')->select('id', 'office_name_bn')->get()->pluck('office_name_bn', 'id');
        $class_list_bn    = \Html::$bn_class;
        $class_list_en    = \Html::$en_class;


        if ( $employee_data ) {
            foreach($employee_data AS $emp){
                $employee_list[$emp->employee_class][] = $emp;
            }
        }

        return view('Local/ForeignTraining::participants', ['employee_list' => @$employee_list, 'designation_list' => $designation_list,
                                                                 'class_list_bn' => $class_list_bn, 'class_list_en' => $class_list_en,
                                                                 'office_list' => $office_list, 'srch_designation' => $designation,
                                                                 'srch_employee_class' => $employee_class, 'srch_office' => $office]);
    }
}
