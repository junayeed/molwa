<div class="container-fluid">
    <div class="row">
    @if ($employee_list)
        @foreach($employee_list AS $employee)
            <div class="col-md-3">
                <div class="portlet light profile-sidebar-portlet8">
                    <div class="profile-userpic">
                        <img src="/{{$employee->profile_image}}" class="img-responsive" alt="" style="width: 75% !important;"> </div>
                    <div class="profile-usertitle" style="margin-top: 5px !important;">
                        <div class="profile-usertitle-name" style="font-size: 16px !important;"> {{$employee->name_bn}} </div>
                        <div class="profile-usertitle-job"> {{$employee->designation}} </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif
    </div>
</div>