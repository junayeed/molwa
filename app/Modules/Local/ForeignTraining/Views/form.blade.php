@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="office_form" method="POST" action="/ForeignTraining/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$ft_data->id}}">
        <input type="hidden" name="employees" id="employees" value="{{@$emp_ids}}">
        {{csrf_field()}}
        <div class="row">
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> বৈদেশিক ভ্রমন/ প্রশিক্ষণ এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">স্মারক নম্বর</label>
                                            <input name="memo_no" id="memo_no" type="text" class="form-control input" value="{{@$ft_data->memo_no}}">
                                        </div>
                                        <div class="form-group col-md-4 ">
                                            <label class="font-lg">তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control input" name="memo_date" id="memo_date"
                                                       value="@if (@$ft_data->memo_date !='0000-00-00'){{\Carbon\Carbon::parse(@$ft_data->memo_date)->format('d-m-Y')}} @endif" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 6px 9px 6px 9px !important;  border: 1px solid #BEBEBE !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">অর্থ বছর</label>
                                            <select class="form-control input" id="financial_year" name="financial_year">
                                                @foreach($fy_list as $id => $fy)
                                                    <option value="{{$id}}" @if($id == @$ft_data->financial_year) selected @endif>{{$fy}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">ভ্রমণ শুরু ও শেষের তারিখ</label>
                                            <div class="input-group input date-picker input-daterange" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" name="start_date" id="start_date" readonly value="@if (@$ft_data->start_date !='0000-00-00'){{\Carbon\Carbon::parse(@$ft_data->start_date)->format('d-m-Y')}} @else  @endif">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control" name="end_date" id="end_date" readonly value="@if (@$ft_data->end_date !='0000-00-00'){{\Carbon\Carbon::parse(@$ft_data->end_date)->format('d-m-Y')}} @else  @endif">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">সময়কাল</label><br>
                                            <label class="badge badge-info" style="font-size: 24px !important; padding: 0px 15px; !important; height: 27px !important;" id="batch_days_duration"></label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">আর্থিক উৎস</label>
                                            <select class="form-control input" id="financial_source" name="financial_source">
                                                <option value="GoB" @if("GoB" == @$ft_data->financial_source) selected @endif>জিওবি</option>
                                                <option value="Project" @if("Project" == @$ft_data->financial_source) selected @endif>প্রকল্প</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label class="font-lg">জিও (GO)</label>
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new"> জিও বাছাই করুন </span>
                                                        <span class="fileinput-exists"> পরিবর্তন করুন </span>
                                                        <input type="file" name="go_attachment">
                                                    </span>
                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                </div>
                                            </div>
                                        </div>
                                        @if (@$ft_data->go_attachment)
                                            <div class="col-md-3 text-center">
                                                <a href="#attachment_view_modal"  class="attachment_view" data-toggle="modal" data-ref="{{@$ft_data->go_attachment}}"
                                                   data-modal_title="প্রশিক্ষণের সম্মানী">
                                                    <i class="fa fa-file-pdf-o font-red-sunglo"></i>
                                                    <label class="badge badge-warning  margin-top-10 text-dark ">&nbsp;&nbsp;জিও&nbsp;&nbsp;</label>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">দেশ বাছাই করুন</label>
                                            <div class="col-md-12">
                                                <select multiple="multiple" class="multi-select font-lg" id="country" name="country[]">
                                                    @foreach($country_list as $id => $country)
                                                        <option value="{{@$country->id}}" @if( in_array(@$country->id, explode(',', @$ft_data->country) ) ) selected @endif>{{$country->nicename_bn}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="mt-element-list">
                            <div class="mt-list-head list-todo font-white bg-red" style="min-height: 41px; padding: 1px !important;">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">কর্মকর্তা/ কর্মচারী
                                        {{--<i class="fa fa-check"></i> 15--}}
                                    </h3>
                                </div>
                                <a href="javascript:;">
                                    <div class="list-count pull-right bg-red-mint" style="padding: 9px;">
                                        <a href="javascript:void(0)" class="add_participants" data-batch_id="" data-toggle="modal" data-target="#participant_modal"><i class="fa fa-users"></i></a>
                                    </div>
                                </a>
                            </div>
                            <div class="mt-list-container list-todo" id="foreign_training_participants">
                                {{--{@$ft_data->emp_details}}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MAP STARTS HERE -->
        <div class="row">
            <div class="col-md-12">
                <div id="container">

                </div>
            </div>
        </div>
        <!-- MAP ENDS HERE -->

        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button class="dt-button buttons-pdf buttons-html5 btn green btn-outline" tabindex="0" aria-controls="sample_1"><i class="fa fa-floppy-o"></i> <span> SAVE</span></button>
                        <a href="/ForeignTraining" class="btn btn-cancel"><i class="fa fa-ban"></i> Cancel</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg " role="document">
                <div class="modal-content" id="participant_details"></div>
            </div>
        </div>

        <!-- Attachment View Modal STARTS -->
        <div id="attachment_view_modal" class="modal fade in">
            <div class="modal-dialog modal-lg modal-scroll" style="max-height:700px important;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="attachment_view_modal_title"></h4>
                    </div>
                    <div class="modal-body" id="attachment_view">
                        <iframe id="attachment_iframe" src="" width="100%" height="70%" frameborder="0" ></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Attachment View Modal ENDS -->
    </form>
@endsection

@section('page_plugins')
    <script src="http://code.highcharts.com/maps/highmaps.js"></script>
    <script src="http://code.highcharts.com/maps/modules/data.js"></script>
    <script src="http://code.highcharts.com/mapdata/custom/world.js"></script>
@endsection

@section('page_js')

    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/Foreigntraining.js" type="text/javascript"></script>


    <script>
        var selected_country_iso = [];
        var country_list         = {!! json_encode($country_list) !!};

        @foreach($country_list as $id => $country)
        @if( in_array($country->id, explode(',', @$ft_data->country) ) )
        selected_country_iso.push("{{$country->iso}}")
        @endif
        @endforeach
    </script>
@endsection