@extends('layouts.master')
@section('content')

    <!-- SEARCH FILTERS STARTS HERE  -->
    <div class="row">
        <form method="POST" action="/ForeignTraining">
            {{csrf_field()}}
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search font-purple fa-2x"></i>
                            <span class="caption-subject font-purple bold uppercase">অনুসন্ধান</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label>পদবী</label>
                                <select name="designation" id="designation" class="form-control input" >
                                    <option value=""> - পদবী বাছাই করুন - </option>
                                    @foreach($designation_list as $id => $designation)
                                        <option value="{{$id}}" @if( $id == @$search_data['designation']) selected @endif>{{$designation}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>দেশ</label>
                                <select name="country" class="form-control input" >
                                    <option value=""> - দেশ বাছাই করুন - </option>
                                    @foreach($country_list as $id => $country)
                                        <option value="{{$id}}" @if( $id == @$search_data['employee_class']) selected @endif>{{$country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-circle purple ">
                                    <i class="fa fa-search"></i> অনুসন্ধান করুন
                                </button>
                            </div>
                            {{--<input type="submit" class="btn btn-sm btn-circle red" name="employeePDFBtn" value="pdf">--}}
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- SEARCH FILTERS ENDS HERE  -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="actions">
                        <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new font-lg " href="/ForeignTraining/add"><i class="fa fa-plus"></i> নতুন যোগ করুন </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                        <thead>
                        <tr>
                            <th class="table_heading">ক্রমিক</th>
                            <th class="table_heading">স্মারক নম্বর ও তারিখ</th>
                            <th class="table_heading">ভ্রমণ শুরু ও শেষের তারিখ</th>
                            <th class="table_heading">দেশ</th>
                            <th class="table_heading">কর্মকর্তা/ কর্মচারীর সংখ্যা</th>
                            <th class="table_heading text-center no-print">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @if (@$ft_data)
                            @foreach($ft_data as $year => $f)
                                <th>
                                    <td colspan="6" class="text-left">
                                        <span class="font-hg">{{\Html::en2bn($fy_list[$year])}}</span>
                                    </td>
                                </th>
                                @foreach( $f AS $ft)
                                    <tr class="odd gradeX">
                                        <td><span class="font-lg">{{\Html::en2bn( $i++)}}</span></td>
                                        <td><span class="font-lg">{{\Html::en2bn( $ft->memo_no )}}, {{\Html::en2bn( \Carbon\Carbon::parse(@$ft->memo_date)->format('d/m/Y') )}}</span></td>
                                        <td><span class="font-lg">{{\Html::en2bn( \Carbon\Carbon::parse(@$ft->start_date)->format('d/m/Y') )}} - {{\Html::en2bn( \Carbon\Carbon::parse(@$ft->end_date)->format('d/m/Y') )}}</span></td>
                                        <td>
                                            @foreach($country_list AS $id => $country)
                                                @if( in_array($id, explode(',', $ft->country) ) )
                                                    <span class="font-lg ">{{$country}}</span>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <span class="font-lg">{{\Html::en2bn( $ft->total_employee )}}</span>
                                            <a href="javascript:void(0)" class="participant_list" data-employees="{{implode(',', $ft_employee[$ft->id])}}" data-toggle="modal" data-target="#participant_modal"><i class="fa fa-users fa-2x"></i></a>
                                        </td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-circle btn-warning btn-sm " href="/ForeignTraining/update/{{$ft->id}}"><i class="fa fa-edit"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @else
                            <tr class="odd gradeX">
                                <td colspan="6"> কোন তথ্য পাওয়া যায়নি।</td>
                            </tr>
                        @endif


                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">বৈদেশিক প্রশিক্ষ </h4>
                </div>
                <div class="modal-body" id="foreign_training_participants_list"></div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_plugins')
    <script src="http://code.highcharts.com/maps/highmaps.js"></script>
    <script src="http://code.highcharts.com/maps/modules/data.js"></script>
    <script src="http://code.highcharts.com/mapdata/custom/world.js"></script>
@endsection
@section('page_js')
    <script src="{{url(asset("/js"))}}/Foreigntraining.js" type="text/javascript"></script>
@endsection