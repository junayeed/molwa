<?php

Route::group(['prefix' => 'hatbazar', 'namespace' => 'App\Modules\Local\HatBazar\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',                            ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@index']);
    Route::post('/',                           ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@index']);
    Route::get('/show/{id}',                   ['as' => 'hatbazar.show',   'uses' => 'HatBazarController@show']);
    Route::get('/add',                         ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@add']);
    Route::get('/update/{id}',                 ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@update']);
    Route::post('/save',                       ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@save']);
    Route::get('/delete/{id}',                 ['as' => 'hatbazar.index',  'uses' => 'HatBazarController@delete']);

});




?>