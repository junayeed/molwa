<?php
namespace App\Modules\Local\HatBazar\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class HatBazarModel extends Model
{
    protected $table = 'hat_bazar';

    protected $fillable = ['office_type', 'division', 'district', 'upazilla', 'dc_office', 'pourosova', 'bengali_year',
                           'cheque_amount', 'cheque_number', 'cheque_date', 'cheque_bank', 'deposite_ac_number',
                           'deposite_date', 'deposite_book_no', 'deposite_inst_no', 'city_corporation', 'enter_by'];
}
