@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <form method="POST" action="/hatbazar">
            {{csrf_field()}}
            <div class="row">
                <div class="form-group col-md-2">
                    <label class="font-lg">অফিসের ধরন</label>
                    <select name="office_type" id="office_type" class="form-control input" >
                        <option value=""> - অফিস বাছাই করুন - </option>
                        @foreach($office_list as $id => $office)
                            <option value="{{$id}}" @if( $id == @$search_data['office_type']) selected @endif>{{$office}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2" id="division_div">
                    <label class="font-lg">বিভাগ</label>
                    <select name="division" id="division" class="form-control input" >
                        <option value=""> - বিভাগ বাছাই করুন - </option>
                        @foreach($division_list as $id => $division)
                            <option value="{{$id}}"  @if( $id == @$search_data['division']) selected @endif>{{$division}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2" id="district_div">
                    <label class="font-lg">জেলা</label>
                    <select name="district" id="district" class="form-control input" >
                        <option value=""> - জেলা বাছাই করুন - </option>
                        @foreach($district_list as $id => $district)
                            <option value="{{$id}}" @if( $id == @$search_data['district']) selected @endif>{{$district}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2" id="upazilla_div">
                    <label class="font-lg">উপজেলা</label>
                    <select name="upazilla" id="upazilla" class="form-control input" >
                        <option value=""> - উপজেলা বাছাই করুন - </option>
                        @foreach($upazila_list as $id => $up)
                            <option value="{{$id}}" @if( $id == @$search_data['upazilla']) selected @endif>{{$up}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-sm btn-circle red">
                    <i class="fa fa-search"></i> অনুসন্ধান করুন
                </button>
            </div>
        </form>
        <div class="row">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">

                    <div class="actions">
                        <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/hatbazar/add"><i class="fa fa-plus"></i> নতুন এন্ট্রি করুন </a>
                        {{--<a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/createUNO"> Create UNO <i class="fa fa-plus"></i></a>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                        <thead>
                        <tr>
                            <th class="table_heading">ক্রমিক</th>
                            <th class="table_heading">অফিসের ধরন</th>
                            <th class="table_heading">উপজেলা/পৌরসভা</th>
                            <th class="table_heading">বাংলা সন</th>
                            <th class="table_heading">টাকার পরিমাণ</th>
                            <th class="table_heading">চেক নম্বর</th>
                            <th class="table_heading">ব্যাংক</th>
                            <th class="table_heading">চেক এর তারিখ</th>
                            <th class="table_heading">হিসাব নম্বর ও ব্যাংক</th>
                            <th class="table_heading">জমার তারিখ</th>
                            {{--<th class="table_heading">ব্যাংক</th>--}}
                            <th class="table_heading text-center no-print">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $i           = 1;
                            $grand_total = 0;
                            $sub_total   = 0;
                        ?>
                        @foreach($hb_data as $id_div => $division)
                            @foreach($division AS $id_dis => $district)
                                <?php
                                $sub_total   = 0;
                                ?>
                                <tr >
                                    <td colspan="10" style="text-align: left !important;">{{$division_list[$id_div]}} >> {{$district_list[$id_dis]}}</td>
                                </tr>
                                @foreach($district AS $hb)
                                    <tr class="odd gradeX">
                                        <td>{{\Html::en2bn($i++)}}।</td>
                                        @if ( $hb->office_type == 1)
                                            <td><span class="label label-sm label-success ">{{$office_list[$hb->office_type]}}</span></td>
                                            <td><span class="label label-sm label-success">{{$hb->upa_bn_name}}</span></td>
                                        @elseif ( $hb->office_type == 2)
                                            <td><span class="label label-sm label-danger">{{$office_list[$hb->office_type]}}, </span></td>
                                            <td><span class="label label-sm label-danger">{{$hb->dc_office_name_bn}}</span></td>
                                        @elseif ( $hb->office_type == 3)
                                            <td><span class="label label-sm label-warning">{{$office_list[$hb->office_type]}}</span></td>
                                            <td><span class="label label-sm label-warning">{{$hb->pourosova_name_bn}}</span></td>
                                        @elseif ( $hb->office_type == 4)
                                            <td><span class="label label-sm label-primary">{{$office_list[$hb->office_type]}}</span></td>
                                        <td><span class="label label-sm label-primary">{{$hb->cc_name_bn}}</span></td>
                                        @endif
                                        <td>{{$hb->bengali_year_bn}}</td>
                                        <td>{{Html::en2bn( number_format($hb->cheque_amount, 2) )}}<?php $grand_total += $hb->cheque_amount; $sub_total += $hb->cheque_amount; ?></td>
                                        <td>{{$hb->cheque_number}}</td>
                                        <td>{{$hb->bank_bn}}</td>
                                        <td>{{Html::en2bn( \Carbon\Carbon::parse(@$hb->cheque_date)->format('d/m/Y') )}}</td>
                                        <td>
                                            {{$hb->deposite_ac_number}}
                                            @if($hb->deposite_ac_number == "0200001211052"), অগ্রণী ব্যাংক লিঃ
                                            @elseif ($hb->deposite_ac_number == "4426334191554"), সোনালী ব্যাংক লিঃ
                                            @endif
                                        </td>
                                        <td>{{Html::en2bn( \Carbon\Carbon::parse(@$hb->deposite_date)->format('d/m/Y') )}}</td>
                                        {{--<td>{{$hb->deposite_date}}</td>--}}
                                        <td class="text-center no-print">
                                            <a class="btn btn-circle btn-warning btn-sm " href="/hatbazar/update/{{$hb->id}}"><i class="fa fa-edit"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4"> <h5 class="text-bold text-right">উপমোট = </h5> </td>
                                    <td><h5 class="text-bold">{{\Html::en2bn( number_format($sub_total, 2) )}}</h5></td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>
                            <td colspan="4"> <h4 class="text-bold text-right">সর্বমোট = </h4> </td>
                            <td> <h4 class="text-bold">{{\Html::en2bn( number_format($grand_total, 2) )}}</h4></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->


    <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details"></div>
        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
@endsection