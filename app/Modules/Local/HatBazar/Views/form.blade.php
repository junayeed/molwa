@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="hatbazar_form" method="POST" action="/hatbazar/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="{{@$hb_data->id}}">
        <input type="hidden" name="district_val" id="district_val" value="{{ @$hb_data->district }}">
        <input type="hidden" name="upazilla_val" id="upazilla_val" value="{{ @$hb_data->upazilla }}">
        <input type="hidden" name="pourosova_val" id="pourosova_val" value="{{ @$hb_data->pourosova }}">

        {{csrf_field()}}
        <div class="row">
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i>  প্রেরকের এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">অফিসের ধরন</label>
                                            <select name="office_type" id="office_type" class="form-control input" >
                                                <option value=""> - অফিস বাছাই করুন - </option>
                                                @foreach($office_list as $id => $office)
                                                    <option value="{{$id}}" @if( $id == @$hb_data->office_type) selected @endif>{{$office}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="division_div">
                                            <label class="font-lg">বিভাগ</label>
                                            <select name="division" id="division" class="form-control input" >
                                                <option value=""> - বিভাগ বাছাই করুন - </option>
                                                @foreach($division_list as $id => $division)
                                                    <option value="{{$id}}" @if( $id == @$hb_data->division) selected @endif>{{$division}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="district_div">
                                            <label class="font-lg">জেলা</label>
                                            <select name="district" id="district" class="form-control input" >
                                                <option value=""> - জেলা বাছাই করুন - </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="upazilla_div">
                                            <label class="font-lg">উপজেলা</label>
                                            <select name="upazilla" id="upazilla" class="form-control input" >
                                                <option value=""> - উপজেলা বাছাই করুন - </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="pourosova_div">
                                            <label class="font-lg">পৌরসভা অফিস</label>
                                            <select name="pourosova" id="pourosova" class="form-control input" >
                                                <option value=""> - পৌরসভা বাছাই করুন - </option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="city_corporation_div">
                                            <label class="font-lg">সিটি কর্পোরেশন</label>
                                            <select name="city_corporation" id="city_corporation" class="form-control input" >
                                                <option value=""> - সিটি কর্পোরেশন বাছাই করুন - </option>
                                                @foreach($city_corporation_list as $id => $cc)
                                                    <option value="{{$id}}" @if( $id == @$hb_data->city_corporation) selected @endif>{{$cc}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3" id="dc_office_div">
                                            <label class="font-lg">জেলা প্রশাসক অফিস</label>
                                            <select name="dc_office" id="dc_office" class="form-control input" >
                                                <option value=""> - জেলা প্রশাসক অফিস বাছাই করুন - </option>
                                                @foreach($dc_office_list as $id => $dc)
                                                    <option value="{{$id}}" @if( $id == @$hb_data->dc_office) selected @endif>{{$dc}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box red-haze ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-money"></i>  চেক এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">বাংলা সন</label>
                                            <select name="bengali_year" id="bengali_year" class="form-control input" >
                                                <option value=""> - সন বাছাই করুন - </option>
                                                @foreach($bengali_year_list as $id => $year)
                                                    <option value="{{$id}}" @if($id == @$hb_data->bengali_year) selected @endif>{{$year}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">টাকার পরিমাণ</label>
                                            <input name="cheque_amount" id="cheque_amount" type="text" class="form-control input" value="{{@$hb_data->cheque_amount}}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">চেক নম্বর</label>
                                            <input name="cheque_number" id="cheque_number" type="text" class="form-control input" value="{{@$hb_data->cheque_number}}">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="font-lg">চেক এর তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control input" name="cheque_date" id="cheque_date"
                                                       value="{{\Carbon\Carbon::parse(@$hb_data->cheque_date)->format('d/m/Y')}}" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; margin-left: 0px !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ব্যাংক</label>
                                            <select name="cheque_bank" id="cheque_bank" class="form-control input" >
                                                <option value=""> - ব্যাংক বাছাই করুন - </option>
                                                @foreach($bank_list as $id => $bank)
                                                    <option value="{{$id}}" @if($id == @$hb_data->cheque_bank) selected @endif>{{$bank}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-book"></i>  চেক জমার বিবরণ
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label class="font-lg">হিসাব নম্বর</label>
                                            <select name="deposite_ac_number" id="deposite_ac_number" class="form-control input" >
                                                <option value=""> - ব্যাংক বাছাই করুন - </option>
                                                <option value="0200001211052" @if("0200001211052" == @$hb_data->deposite_ac_number) selected @endif>মুক্তিযোদ্ধাদের কল্যাণে হাট-বাজার, 0200001211052, অগ্রণী ব্যাংক লিঃ</option>
                                                <option value="4426334191554" @if("4426334191554" == @$hb_data->deposite_ac_number) selected @endif>মুক্তিযোদ্ধাদের কল্যাণে হাট-বাজার, 4426334191554, সোনালী ব্যাংক লিঃ</option>                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="font-lg">জমার তারিখ</label>
                                            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                <input type="text" class="form-control input" name="deposite_date" id="deposite_date"
                                                       value="{{\Carbon\Carbon::parse(@$hb_data->deposite_date)->format('d/m/Y')}}" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; margin-left: 0px !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">বই নম্বর</label>
                                            <input name="deposite_book_no" id="deposite_book_no" type="text" class="form-control input" value="{{@$hb_data->deposite_book_no}}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">ক্রমিক নম্বর</label>
                                            <input name="deposite_inst_no" id="deposite_inst_no" type="text" class="form-control input" value="{{@$hb_data->deposite_inst_no}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            {{--<div class="col-md-3">
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-edit font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase" style="font-size: 22px !important;">প্রয়োজনীয় দলিল</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-edit font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase" style="font-size: 22px !important;">প্রমাণক</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> প্রশিক্ষণের পত্র বাছাই করুন </span>
                                                <span class="fileinput-exists"> পরিবর্তন করুন </span>
                                                <input type="file" name="training_letter_file">
                                            </span>
                                            <span class="fileinput-filename"> </span> &nbsp;
                                            <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cog"></i>
                                <span class="caption-subject font-dark bold uppercase"  style="font-size: 22px !important;">লগ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>--}}
            <!-- AUDIT LOG END -->
        </div>

        <div class="row ">
            <div class="col-md-8">
                <button type="submit" class="btn btn-save btn-success font-hg col-md-2"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
            </div>
        </div>
    </form>
        <!-- END CONTENT -->

@endsection

@section('page_plugins')


@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/hatbazar.js" type="text/javascript"></script>

@endsection

