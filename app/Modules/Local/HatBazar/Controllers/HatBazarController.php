<?php
namespace App\Modules\Local\HatBazar\Controllers;
use App\Modules\Local\HatBazar\Models\HatBazarModel;
use App\Modules\Local\HatBazar\Models\Model;
use App\Http\Controllers\Controller;
use App\Modules\Local\Employee\Models\EmployeeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

class HatBazarController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $hb_a              = array();
        $search_data       = $request->only(['office_type', 'division', 'district', 'upazilla']);
        $hb_query          = HatBazarModel::select('hat_bazar.*', 'by.bengali_year_bn', 'b.bank_bn', 'dis.dis_bn_name',
                                                   'up.upa_bn_name', 'dc_office_name_bn', 'cc_name_bn', 'pourosova_name_bn')
                                          ->leftJoin('bengali_year AS by', 'by.id', '=', 'hat_bazar.bengali_year')
                                          ->leftJoin('banks AS b', 'b.id', '=', 'hat_bazar.cheque_bank')
                                          ->leftJoin('district_tbl AS dis', 'dis.id', '=', 'hat_bazar.district')
                                          ->leftJoin('upazila_tbl AS up', 'up.id', '=', 'hat_bazar.upazilla')
                                          ->leftJoin('dc_offices AS DC', 'DC.id', '=', 'hat_bazar.dc_office')
                                          ->leftJoin('city_corporation AS CC', 'CC.id', '=', 'hat_bazar.city_corporation')
                                          ->leftJoin('pourosova AS PO', 'PO.id', '=', 'hat_bazar.pourosova')
                                          ->orderByRaw('created_at', 'DESC');

        if ( isset($search_data['office_type']) && !empty($search_data['office_type']) )    $hb_query->where('hat_bazar.office_type', '=', $search_data['office_type']);
        if ( isset($search_data['division']) && !empty($search_data['division']) )          $hb_query->where('hat_bazar.division', '=', $search_data['division']);
        if ( isset($search_data['district']) && !empty($search_data['district']) )          $hb_query->where('hat_bazar.district', '=', $search_data['district']);
        if ( isset($search_data['upazilla']) && !empty($search_data['upazilla']) )          $hb_query->where('hat_bazar.upazilla', '=', $search_data['upazilla']);

        $hb_data  = $hb_query->get()->all();

        if ($hb_data) {
            foreach($hb_data as $hb) {
                $hb_a[$hb->division][$hb->district][] = $hb;
            }
        }

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $office_list                = ['1' => 'উপজেলা নির্বাহী অফিস', '2' => 'জেলা প্রশাসক অফিস', '3' => 'পৌরসভা অফিস', '4' => 'সিটি কর্পোরেশন অফিস'];
        $division_list              = DB::table('division_tbl')->pluck('div_bn_name', 'id');
        $district_list              = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $upazila_list               = DB::table('upazila_tbl')->pluck('upa_bn_name', 'id');
        $bengali_year_list          = DB::table('bengali_year')->pluck('bengali_year_bn', 'id');
        $bank_list                  = DB::table('banks')->pluck('bank_bn', 'id');
        $city_corporation_list      = DB::table('city_corporation')->pluck('cc_name_bn', 'id');
        $dc_office_list             = DB::table('dc_offices')->pluck('dc_office_name_bn', 'id');
        //echo "<pre>"; print_r($hb_a); die;


        return view('Local/HatBazar::list', ['data' => $data, 'hb_data' => $hb_a, 'office_list' => $office_list,
                                                  'division_list' => $division_list->toArray(), 'district_list' => $district_list->toArray(),
                                                  'upazila_list' => $upazila_list, 'bank_list' => $bank_list, 'bengali_year_list' => $bengali_year_list,
                                                  'city_corporation_list' => $city_corporation_list, 'dc_office_list' => $dc_office_list,
                                                  'search_data' => $search_data]);
    }

    public function saveBatchParticipant(Request $request){
        $data = $request->only(['batch_id', 'employee_id', 'emp_profile_id']);
        $id = DB::table('batch_participants')->insertGetId($data);
    }

    public function removeBatchParticipant(Request $request){
        $data = $request->only(['batch_id', 'employee_id']);

        DB::table('batch_participants')->where('batch_id', '=', $data['batch_id'])->where('employee_id', '=', $data['employee_id'])->delete();
    }

    function add() {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        $office_list                = ['1' => 'উপজেলা নির্বাহী অফিস', '2' => 'জেলা প্রশাসক অফিস', '3' => 'পৌরসভা অফিস', '4' => 'সিটি কর্পোরেশন অফিস'];
        $division_list              = DB::table('division_tbl')->pluck('div_bn_name', 'id');
        $bengali_year_list          = DB::table('bengali_year')->pluck('bengali_year_bn', 'id');
        $bank_list                  = DB::table('banks')->pluck('bank_bn', 'id');
        $city_corporation_list      = DB::table('city_corporation')->pluck('cc_name_bn', 'id');
        $dc_office_list             = DB::table('dc_offices')->pluck('dc_office_name_bn', 'id');

        return view('Local/HatBazar::form', ['data' => $data, 'office_list' => $office_list, 'division_list' => $division_list,
                                                  'dc_office_list' => $dc_office_list, 'bank_list' => $bank_list,
                                                  'bengali_year_list' => $bengali_year_list, 'city_corporation_list' => $city_corporation_list]);
    }

    function update($id) {
        $hb_data                    = HatBazarModel::find($id);
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $auditLogData               = \Common::getAuditLogData('HatBazar', $id);
        $office_list                = ['1' => 'উপজেলা নির্বাহী অফিস', '2' => 'জেলা প্রশাসক অফিস', '3' => 'পৌরসভা অফিস', '4' => 'সিটি কর্পোরেশন অফিস'];
        $division_list              = DB::table('division_tbl')->pluck('div_bn_name', 'id');
        $bengali_year_list          = DB::table('bengali_year')->pluck('bengali_year_bn', 'id');
        $bank_list                  = DB::table('banks')->pluck('bank_bn', 'id');
        $city_corporation_list      = DB::table('city_corporation')->pluck('cc_name_bn', 'id');
        $dc_office_list             = DB::table('dc_offices')->pluck('dc_office_name_bn', 'id');

        return view('Local/HatBazar::form', ['hb_data' => $hb_data, 'data' => $data, 'audit_log' => $auditLogData,
                                                  'office_list' => $office_list, 'division_list' => $division_list,
                                                  'bank_list' => $bank_list, 'bengali_year_list' => $bengali_year_list,
                                                  'dc_office_list' => $dc_office_list, 'city_corporation_list' => $city_corporation_list]);
    }

    function save(Request $request) {
        $hb_data                     = $request->only(['office_type', 'division', 'district', 'upazilla', 'dc_office', 'pourosova',
                                                       'bengali_year', 'cheque_amount', 'cheque_number', 'cheque_date', 'cheque_bank',
                                                       'deposite_ac_number', 'deposite_date', 'deposite_book_no', 'deposite_inst_no',
                                                       'city_corporation', 'enter_by']);
        $hb_data['cheque_date']      = date('Y-m-d', strtotime(str_replace('/', '-', $hb_data['cheque_date']) ));
        $hb_data['deposite_date']    = date('Y-m-d', strtotime(str_replace('/', '-', $hb_data['deposite_date'])));
        $hb_data['financial_year']   = \Common::getCurrentFinancialYear();
        $hb_data['enter_by']         = Auth::user()->id;
        $field_list                  = array('office_type', 'division', 'district', 'upazilla', 'dc_office', 'pourosova',
                                             'bengali_year', 'cheque_amount', 'cheque_number', 'cheque_date', 'cheque_bank',
                                             'deposite_ac_number', 'deposite_date', 'deposite_book_no', 'deposite_inst_no', 'city_corporation');

        if ($hb_data['office_type'] == 2) { // if office_type is DC office
            $hb_data['district'] = $hb_data['dc_office'];
            $hb_data['division'] = \Common::getDistrictDivisionID($hb_data['dc_office']);
        }
        else if ($hb_data['office_type'] == 4) { // if office_type is City Corp.
            $hb_data['district'] = \Common::getCCDistrictID($hb_data['city_corporation']);
            $hb_data['division'] = \Common::getDistrictDivisionID($hb_data['district']);
        }
        if( $request->id ) { // Update the Hat Bazar Data
            $hb         = HatBazarModel::find($request->id);
            $old_data   = $hb->toArray();
            $hb->update($hb_data);

            //\Common::AuditLog('HatBazar', $request->id, $old_data, $hb, 'Updated', $field_list);
        }
        else { // Create a new Batch Data
            $batch = HatBazarModel::create($hb_data);
            \Common::AuditLog('HatBazar', $batch->id, '', '', 'Create', '');
        }

        return redirect('/hatbazar');
    }

    function saveTrainerData($request, $batch_id) {
        if ( $request->input('batch_session_data') ) {
            if (!empty(DB::table('batch_trainers')->where('batch_id', $batch_id)->first())) {
                DB::table('batch_trainers')->where('batch_id', $batch_id)->delete();
            }

            foreach ($request->input('batch_session_data') as $item) {
                $trainer_data = array();
                $trainer_data['batch_id']            = $batch_id;
                $trainer_data['session_topic']       = $item['session_topic'];
                $trainer_data['session_start_time']  = $item['session_start_time'];
                $trainer_data['session_end_time']    = $item['session_end_time'];
                $trainer_data['session_date']        = date('Y-m-d', strtotime(str_replace('/', '-', $item['session_date'])));
                $trainer_data['trainer_type']        = isset($item['trainer_type'][0]) ? 1 : 0;
                if ($trainer_data['trainer_type'] == 1) {
                    $trainer_data['employee_id']     = $item['employee_id'];
                } else {
                    $trainer_data['trainer_name']    = $item['trainer_name'];
                }

                DB::table('batch_trainers')->insert($trainer_data);
            }
        }
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/batch');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
