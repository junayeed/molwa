<?php
namespace App\Modules\Local\ForwardDairy2\Controllers;

use App\Modules\Local\ForwardDairy2\Models\ForwardDairy2Model;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Designation;


class ForwardDairy2Controller extends Controller
{
    function __construct(){
	}

    function index() {
        $writ_list                  = ForwardDairy2Model::select('writ_petitions.id', 'writ_no', 'writ_order_date', 'writ_complainant',
                                                                         'writ_receive_date', 'writ_filing_date', 'ministry_name_bn',
                                                                         'subject_type_bn', 'writ_defendant')
                                                    ->leftJoin('ministry_tbl AS MT', 'MT.id', '=', 'writ_petitions.writ_ministry')
                                                    ->leftJoin('subject_types AS ST', 'ST.id', '=', 'writ_petitions.writ_type')
                                                    ->get()->all();
        $writ_type_list             = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $ministry_list              = DB::table('ministry_tbl')->where('status', 'Active')->pluck('ministry_name_bn', 'id');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/ForwardDairy2::form', ['data' => $data, 'writ_type_list' => $writ_type_list, 'ministry_list' => $ministry_list, 'writ_list' => $writ_list]);
    }

    function add() {
        $district_list              = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Pourosova::form', ['data' => $data, 'district_list' => $district_list]);
    }

    function update($id) {
        $pourosova_data               = PourosovaModel::find($id);
        $district_list                = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        //$auditLogData                 = \Common::getAuditLogData('Designation', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        $data['nav_item']             = str_replace("Controller", "", $controller);
        list($controller, $action)    = explode('@', $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Pourosova::form', ['pourosova_data' => $pourosova_data, 'data' => $data, 'district_list' => $district_list]);
    }

    function save(Request $request) {
        $writ_data  = $request->only(['writ_no', 'writ_order_date', 'writ_receive_date', 'writ_filing_date',
                                      'writ_complainant', 'writ_defendant', 'writ_ministry', 'writ_type', 'hard_file_no',
                                      'efile_no']);
        $field_list = array('writ_no', 'writ_order_date', 'writ_receive_date', 'writ_filing_date', 'writ_complainant',
                            'writ_defendant', 'writ_ministry', 'writ_type', 'hard_file_no', 'efile_no');

        $writ_data['writ_order_date']    = date('Y-m-d', strtotime($writ_data['writ_order_date']));
        $writ_data['writ_receive_date']  = date('Y-m-d', strtotime($writ_data['writ_receive_date']));
        $writ_data['writ_filing_date']   = date('Y-m-d', strtotime($writ_data['writ_filing_date']));

        //echo "<pre>"; print_r($writ_data);die;

        if( $request->id ) {
            $writ      = ForwardDairy2Model::find($request->id);
            $old_data  = $writ->toArray();
            $writ->update($writ_data);

            $this->saveWritOrder($request->id, $request);

            \Common::AuditLog('ForwardDairy2', $request->id, $old_data, $writ_data, 'Updated', $field_list);
        }
        else {
            $writ_id = ForwardDairy2Model::create($writ_data)->id;

            $this->saveWritOrder($writ_id, $request);

            \Common::getAuditLogData('ForwardDairy2', $writ_id, '', '', 'Create', '');
        }

        return redirect('/ForwardDairy2');
    }

    public function saveWritOrder($writ_id, $request) {
        $writ_order_data  = $request->only(['cmp_submission_date', 'power_of_attorney', 'order_certified_copy', 'copy_of_petition',
                                            'delayed_exp', 'sf_issue_date', 'sf_issue_memo', 'sf_receiving_date', 'sf_receiving_memo',
                                            'sf_sending_date', 'sf_sending_memo' ]);

        $writ_order_data['writ_id']               = $writ_id;
        $writ_order_data['cmp_submission_date']   = date('Y-m-d', strtotime($writ_order_data['cmp_submission_date']));
        $writ_order_data['sf_issue_date']         = date('Y-m-d', strtotime($writ_order_data['sf_issue_date']));
        $writ_order_data['sf_receiving_date']     = date('Y-m-d', strtotime($writ_order_data['sf_receiving_date']));
        $writ_order_data['sf_sending_date']       = date('Y-m-d', strtotime($writ_order_data['sf_sending_date']));
        $writ_order_data['power_of_attorney']     = isset($writ_order_data['power_of_attorney'])    ? 1 : 0;
        $writ_order_data['order_certified_copy']  = isset($writ_order_data['order_certified_copy']) ? 1 : 0;
        $writ_order_data['copy_of_petition']      = isset($writ_order_data['copy_of_petition'])     ? 1 : 0;
        $writ_order_data['delayed_exp']           = isset($writ_order_data['delayed_exp'])          ? 1 : 0;
        
        if (DB::table('writ_orders')->where('writ_id', $writ_id)->get()->first()) {
            DB::table('writ_orders')->update($writ_order_data, ['writ_id', $writ_id]);
        }
        else {
            DB::table('writ_orders')->insert($writ_order_data);
        }

    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/pourosova');
    }

    function show($id){
        $writ                       = ForwardDairy2Model::find($id);
        $writ_order                 = DB::table('writ_orders AS WO')->select('WO.*', 'WP.writ_no')->leftJoin('writ_petitions AS WP', 'WP.id', '=', 'WO.writ_id')->where('writ_id', $id)->get()->first();
        $writ_list                  = ForwardDairy2Model::select('writ_petitions.id', 'writ_no', 'writ_order_date', 'writ_complainant',
                                                                         'writ_receive_date', 'writ_filing_date', 'ministry_name_bn',
                                                                         'subject_type_bn', 'writ_defendant')
                                                        ->leftJoin('ministry_tbl AS MT', 'MT.id', '=', 'writ_petitions.writ_ministry')
                                                        ->leftJoin('subject_types AS ST', 'ST.id', '=', 'writ_petitions.writ_type')
                                                        ->get()->all();

        $writ_type_list             = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $ministry_list              = DB::table('ministry_tbl')->where('status', 'Active')->pluck('ministry_name_bn', 'id');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        return view('Local/ForwardDairy2::form', ['data' => $data, 'writ_type_list' => $writ_type_list, 'ministry_list' => $ministry_list,
                                                       'writ' => $writ, 'writ_list' => $writ_list, 'writ_order' => $writ_order]);

    }
}
