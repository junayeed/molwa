@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="office_form" method="POST" action="/ForwardDairy2/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$writ->id}}">
        {{csrf_field()}}
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Left Tabs </div>
                {{--<div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                </div>--}}
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="nav nav-tabs tabs-left">
                            <li class="active">
                                <a href="#tab_writ" data-toggle="tab"> রিট </a>
                            </li>
                            <li>
                                <a href="#tab_order" data-toggle="tab"> অর্ডার </a>
                            </li>
                            <li>
                                <a href="#tab_rule_nishi" data-toggle="tab"> রুল নিশি </a>
                            </li>
                            <li>
                                <a href="#tab_settelment" data-toggle="tab"> আবেদন নিষ্পত্তি </a>
                            </li>
                            <li>
                                <a href="#tab_contempt" data-toggle="tab"> কন্টেম্পট </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="tab-content">
                            <!-- WRIT TAB starts here -->
                            <div class="tab-pane active" id="tab_writ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>রিট নম্বর</label>
                                                <input name="writ_no" id="writ_no" type="text" class="form-control input" value="{{@$writ->writ_no}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>আদেশের তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="writ_order_date" id="writ_order_date"
                                                           value="@if(@$writ->writ_order_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ->writ_order_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>প্রাপ্তির তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="writ_receive_date" id="writ_receive_date"
                                                           value="@if(@$writ->writ_receive_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ->writ_receive_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>ফাইলিং তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="writ_filing_date" id="writ_filing_date"
                                                           value="@if(@$writ->writ_filing_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ->writ_filing_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>হার্ড ফাইল নম্বর</label>
                                                <div class="input-icon">
                                                    <i class="" style="margin: 8px 2px 4px 10px !important; font-size: 13px; font-style: normal !important; color: #555 !important;">
                                                        48.00.0000.004.
                                                    </i>
                                                    <input name="hard_file_no" id="hard_file_no" type="text" class="form-control input" style="padding-left: 115px;" value="{{@$writ->hard_file_no}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>ই-ফাইল নম্বর</label>
                                                <div class="input-icon">
                                                    <i class="" style="margin: 8px 2px 4px 10px !important; font-size: 13px; font-style: normal !important; color: #555 !important;">
                                                        48.00.0000.007.
                                                    </i>
                                                    <input name="efile_no" id="efile_no" type="text" class="form-control input"  style="padding-left: 115px;" value="{{@$writ->efile_no}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>বাদী</label>
                                                <textarea name="writ_complainant" id="writ_complainant" type="text" class="form-control input" >{{@$writ->writ_complainant}}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>বিবাদী</label>
                                                <textarea name="writ_defendant" id="writ_defendant" type="text" class="form-control input" >{{@$writ->writ_defendant}}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>মন্ত্রণালয়/বিভাগ</label>
                                                <select name="writ_ministry" id="writ_ministry" class="form-control input" >
                                                    <option value=""> - বাছাই করুন - </option>
                                                    @foreach($ministry_list as $id => $mn)
                                                        <option value="{{$id}}" @if( $id == @$writ->writ_ministry) selected @endif>{{$mn}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>টাইপ</label>
                                                <select name="writ_type" id="writ_type" class="form-control input" >
                                                    <option value=""> - বাছাই করুন - </option>
                                                    @foreach($writ_type_list as $id => $type)
                                                        <option value="{{$id}}" @if( $id == @$writ->writ_type) selected @endif>{{$type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                                <p></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <th class="table_heading">ক্রমিক</th>
                                                <th class="table_heading">রিট নম্বর</th>
                                                <th class="table_heading">আদেশের তারিখ</th>
                                                <th class="table_heading">প্রাপ্তির তারিখ</th>
                                                <th class="table_heading">ফাইলিং তারিখ</th>
                                                <th class="table_heading">বাদী</th>
                                                <th class="table_heading">বিবাদী</th>
                                                <th class="table_heading">মন্ত্রণালয়/বিভাগ</th>
                                                <th class="table_heading">টাইপ</th>
                                                <th class="table_heading text-center no-print">Actions</th>
                                            </tr>
                                            </thead>
                                            <?php $i = 1; ?>
                                            @foreach($writ_list as $writ)
                                                <tr class="odd gradeX">
                                                    <td>{{Html::en2bn( $i++ )}}</td>
                                                    <td>{{$writ->writ_no}}</td>
                                                    <td>{{Html::en2bn( \Carbon\Carbon::parse(@$writ->writ_order_date)->format('d-m-Y') )}}</td>
                                                    <td>{{Html::en2bn( \Carbon\Carbon::parse(@$writ->writ_receive_date)->format('d-m-Y') )}}</td>
                                                    <td>{{Html::en2bn( \Carbon\Carbon::parse(@$writ->writ_filing_date)->format('d-m-Y') )}}</td>
                                                    <td>{{$writ->writ_complainant}}</td>
                                                    <td>{{$writ->writ_defendant}}</td>
                                                    <td>{{$writ->ministry_name_bn}}</td>
                                                    <td>{{$writ->subject_type_bn}}</td>
                                                    <td><a class="" href="/ForwardDairy2/show/{{$writ->id}}"><i class="fa fa-edit"></i> </a></td>
                                                </tr>
                                            @endforeach
                                            <tbody>

                                        </table>

                                    </div>
                                    </div>
                                </div>
                            </div>
                            <!-- WRIT TAB ends here -->

                            <!--WRIT - ORDER TAB starts here   -->
                            <div class="tab-pane fade" id="tab_order">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>রিট নম্বর <span class="writ_no">{{@$writ_order->writ_no}}</span></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>সি.এম.পি. দাখিলের তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="cmp_submission_date" id="cmp_submission_date"
                                                           value="@if(@$writ_order->cmp_submission_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ_order->cmp_submission_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                    <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <div class="icheck-inline">
                                                        <label> ওকালতনামা প্রস্তুত <input name="power_of_attorney" id="power_of_attorney" @if(@$writ_order->power_of_attorney) checked @endif type="checkbox" class="icheck" data-checkbox="icheckbox_square-green"></label>
                                                        <label> আদেশের সার্টিফাইড কপি সংগ্রহ <input name="order_certified_copy" id="order_certified_copy" @if(@$writ_order->order_certified_copy) checked @endif  type="checkbox" class="icheck" data-checkbox="icheckbox_square-green"></label>
                                                        <label> আর্জির কপি সংগ্রহ <input name="copy_of_petition" id="copy_of_petition" @if(@$writ_order->copy_of_petition) checked @endif type="checkbox" class="icheck" data-checkbox="icheckbox_square-green"></label>
                                                        <label> বিলম্বজনিত ব্যাখ্যা(প্রযোজ্যক্ষেত্রে) সংগ্রহ <input name="delayed_exp" id="delayed_exp" @if(@$writ_order->delayed_exp) checked @endif type="checkbox" class="icheck" data-checkbox="icheckbox_square-green"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. প্রেরণের তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="sf_issue_date" id="sf_issue_date"
                                                           value="@if(@$writ_order->sf_issue_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ_order->sf_issue_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. প্রেরণের স্মারক</label>
                                                <input name="sf_issue_memo" id="sf_issue_memo" type="text" class="form-control input" value="{{@$writ_order->sf_issue_memo}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. প্রাপ্তির তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="sf_receiving_date" id="sf_receiving_date"
                                                           value="@if(@$writ_order->sf_receiving_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ_order->sf_receiving_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. প্রাপ্তির স্মারক</label>
                                                <input name="sf_receiving_memo" id="sf_receiving_memo" type="text" class="form-control input" value="{{@$writ_order->sf_receiving_memo}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. আদালতে প্রেরণের তারিখ</label>
                                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                    <input type="text" class="form-control input" name="sf_sending_date" id="sf_sending_date"
                                                           value="@if(@$writ_order->sf_sending_date != '0000-00-00'){{\Carbon\Carbon::parse(@$writ_order->sf_sending_date)->format('d-m-Y')}} @endif" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button" style="padding: 3px 9px 9px 9px !important; border: 1px #BEBEBE solid !important;">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>এস.এফ. আদালতে প্রেরণের স্মারক</label>
                                                <input name="sf_sending_memo" id="sf_sending_memo" type="text" class="form-control input" value="{{@$writ_order->sf_sending_memo}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--WRIT - ORDER TAB ends here   -->

                            <!--WRIT - RULE NISI TAB starts here   -->
                            <div class="tab-pane fade" id="tab_rule_nishi">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>রিট নম্বর <span class="writ_no">{{@$writ->writ_no}}</span></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--WRIT - RULE NISI TAB ends here   -->
                            <div class="tab-pane fade" id="tab_settelment">
                                <div class="row">
                                    আবেদন নিষ্পত্তি
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab_contempt">
                                <div class="row">
                                    কন্টেম্পট
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/forwarddairy2.js" type="text/javascript"></script>
@endsection