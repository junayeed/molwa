<?php
namespace App\Modules\Local\ForwardDairy2\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ForwardDairy2Model extends Model
{
    protected $table = 'writ_petitions';

    protected $fillable = ['writ_no', 'writ_order_date', 'writ_receive_date', 'writ_filing_date', 'writ_complainant',
                           'writ_defendant', 'writ_ministry', 'writ_type', 'hard_file_no', 'efile_no'];
}
