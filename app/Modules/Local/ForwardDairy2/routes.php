<?php

Route::group(['prefix' => 'ForwardDairy2', 'namespace' => 'App\Modules\Local\ForwardDairy2\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',             ['as' => 'ForwardDairy2.index',       'uses' => 'ForwardDairy2Controller@index']);
    Route::get('/show/{id}',    ['as' => 'ForwardDairy2.show',        'uses' => 'ForwardDairy2Controller@show']);
    Route::get('/add',          ['as' => 'ForwardDairy2.index',       'uses' => 'ForwardDairy2Controller@add']);
    Route::get('/update/{id}',  ['as' => 'ForwardDairy2.index',       'uses' => 'ForwardDairy2Controller@update']);
    Route::post('/save',        ['as' => 'ForwardDairy2.index',       'uses' => 'ForwardDairy2Controller@save']);
    Route::get('/delete/{id}',  ['as' => 'ForwardDairy2.index',       'uses' => 'ForwardDairy2Controller@delete']);
});
?>