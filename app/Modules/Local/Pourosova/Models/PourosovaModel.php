<?php
namespace App\Modules\Local\Pourosova\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class PourosovaModel extends Model
{
    protected $table = 'pourosova';

    protected $fillable = ['pourosova_name_en', 'pourosova_name_bn', 'district_id'];
}
