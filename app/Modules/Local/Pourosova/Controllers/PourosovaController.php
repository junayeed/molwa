<?php
namespace App\Modules\Local\Pourosova\Controllers;

use App\Modules\Local\Pourosova\Models\PourosovaModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Designation;


class PourosovaController extends Controller
{
    function __construct(){
	}

    function index() {
        $pourosova_data             = PourosovaModel::select('pourosova.id', 'pourosova_name_en', 'pourosova_name_bn', 'district_id', 'dis_bn_name')
                                                    ->leftJoin('district_tbl AS DT', 'DT.id', '=', 'pourosova.district_id')->get()->all();
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Pourosova::list', ['pourosova_data' => $pourosova_data, 'data' => $data]);
    }

    function add() {
        $district_list              = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Pourosova::form', ['data' => $data, 'district_list' => $district_list]);
    }

    function update($id) {
        $pourosova_data               = PourosovaModel::find($id);
        $district_list                = DB::table('district_tbl')->select('id', 'dis_bn_name')->pluck('dis_bn_name', 'id');
        //$auditLogData                 = \Common::getAuditLogData('Designation', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        $data['nav_item']             = str_replace("Controller", "", $controller);
        list($controller, $action)    = explode('@', $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Pourosova::form', ['pourosova_data' => $pourosova_data, 'data' => $data, 'district_list' => $district_list]);
    }

    function save(Request $request) {
        $pourosova_data  = $request->only(['pourosova_name_bn', 'pourosova_name_en', 'district_id']);
        $field_list        = array('pourosova_name_bn', 'pourosova_name_en', 'district_id');

        if( $request->id ) {
            $pourosova      = PourosovaModel::find($request->id);
            $old_data      = $pourosova->toArray();
            $pourosova->update($pourosova_data);

            \Common::AuditLog('Pourosova', $request->id, $old_data, $pourosova_data, 'Updated', $field_list);
        }
        else {
            $pourosova_id = PourosovaModel::create($pourosova_data)->id;

            \Common::getAuditLogData('Pourosova', $pourosova_id, '', '', 'Create', '');
        }

        return redirect('/pourosova');
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/pourosova');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Pourosova::show', ['data' => $asset]);

    }
}
