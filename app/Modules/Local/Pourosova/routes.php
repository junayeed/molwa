<?php

Route::group(['prefix' => 'pourosova', 'namespace' => 'App\Modules\Local\Pourosova\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',             ['as' => 'pourosova.index',       'uses' => 'PourosovaController@index']);
    Route::get('/show/{id}',    ['as' => 'pourosova.show',        'uses' => 'PourosovaController@show']);
    Route::get('/add',          ['as' => 'pourosova.index',       'uses' => 'PourosovaController@add']);
    Route::get('/update/{id}',  ['as' => 'pourosova.index',       'uses' => 'PourosovaController@update']);
    Route::post('/save',        ['as' => 'pourosova.index',       'uses' => 'PourosovaController@save']);
    Route::get('/delete/{id}',  ['as' => 'pourosova.index',       'uses' => 'PourosovaController@delete']);
});
?>