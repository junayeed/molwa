@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="office_form" method="POST" action="/pourosova/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$pourosova_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> পৌরসভার তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>পৌরসভার নাম (বাংলা)</label>
                                            <input name="pourosova_name_bn" id="pourosova_name_bn" type="text" class="form-control input" value="{{@$pourosova_data->pourosova_name_bn}}">

                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>পৌরসভার নাম (ইংরেজী)</label>
                                            <input name="pourosova_name_en" id="pourosova_name_en" type="text" class="form-control input" value="{{@$pourosova_data->pourosova_name_en}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>জেলা</label>
                                            <select name="district_id" id="district_id" class="form-control input">
                                                <option value=""> - জেলা বাছাই করুণ - </option>
                                                @foreach($district_list AS $id=>$dis)
                                                    <option value="{{$id}}"  @if($id == @$pourosova_data->district_id) selected @endif>{{$dis}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i> লগ
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AUDIT LOG END -->
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                        <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>

                    </div>
                </div>

            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

@endsection

@section('page_js')
    <script src="{{url(asset("/js"))}}/office.js" type="text/javascript"></script>
@endsection