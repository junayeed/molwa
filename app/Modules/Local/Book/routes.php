<?php

Route::group(['prefix' => 'Book', 'namespace' => 'App\Modules\Local\Book\Controllers', 'middleware' => ['web']], function () {
    Route::post('/',                           ['as' => 'book.index',  'uses' => 'BookController@index']);
    Route::get('/',                            ['as' => 'book.index',  'uses' => 'BookController@index']);
    Route::get('/show/{id}',                   ['as' => 'book.show',   'uses' => 'BookController@show']);
    Route::get('/add',                         ['as' => 'book.index',  'uses' => 'BookController@add']);
    Route::get('/update/{id}',                 ['as' => 'book.index',  'uses' => 'BookController@update']);
    Route::post('/save',                       ['as' => 'book.index',  'uses' => 'BookController@save']);
    Route::get('/delete/{id}',                 ['as' => 'book.index',  'uses' => 'BookController@delete']);
    Route::get('/getParticipantDetails/',      ['as' => 'book.index',  'uses' => 'BookController@getParticipantDetails']);
    Route::get('/saveBatchParticipant/',       ['as' => 'book.index',  'uses' => 'BookController@saveBatchParticipant']);
    Route::get('/removeBatchParticipant/',     ['as' => 'book.index',  'uses' => 'BookController@removeBatchParticipant']);
    Route::get('/sendSMS/',                    ['as' => 'book.index',  'uses' => 'BookController@sendSMS2BatchParticipans']);
});




?>