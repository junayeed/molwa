<?php
namespace App\Modules\Local\Book\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Local\Book\Models\BookModel;
use App\Models\BookRegistry;
use App\Modules\Local\Employee\Models\EmployeeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    function __construct(){
	}

    function index(Request $request) {
        $books                = array();
        //$search_data          = $request->only();
        $search_query         = BookModel::select();
        $publishers_list      = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');

        $books = $search_query->get();

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Book::list', ['books' => $books, 'publisher_list' => $publishers_list]);
    }

    public function add() {
        $book_data                 = (object)[];
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $book_data->financial_year = \Common::getCurrentFinancialYear();
        //$book_data->batch_no       = BatchModel::where('financial_year', '=', $batch_data->financial_year)->max('batch_no')+1;
        $batch_status_list          = ['Upcoming' => 'আসন্ন', 'In Progress' => 'চলমান', 'Completed' => 'সমাপ্ত'];
        $publishers_list            = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');
        $language_list              = ['Bangla' => 'বাংলা', 'English' => 'ইংরেজি'];
        $source_list                = ['Purchased' => 'ক্রয়কৃত', 'Donation' => 'দান'];
        $trainer_list               = EmployeeModel::select(DB::raw("CONCAT(employee.name_bn, ', ', D.name_bn) AS display_name"), 'employee.id')
                                                ->leftJoin('employee_profiles as ep', 'ep.id', '=', 'employee.emp_profile_id')
                                                ->leftJoin('designations AS D', 'D.id', '=', 'ep.designation')
                                                ->where('employee_status', '=', 'Active')
                                                ->where('pay_grade', '<', 10)
                                                ->orderBy('D.weight', 'ASC')
                                                ->get()->pluck('display_name', 'id');

        //echo "<pre>"; print_r($publishers_list); die;
        return view('Local/Book::form', ['data' => $data, 'batch_status_list' => $batch_status_list, 'publishers_list' => $publishers_list,
                                                'source_list' => $source_list, 'language_list' => $language_list,
                                                'trainer_list' => $trainer_list, 'batch_data' => $book_data]);
    }

    public function update($id) {
        $book_data                  = BookModel::find($id);
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        //$auditLogData               = \Common::getAuditLogData('Batch', $id);
        $publishers_list            = DB::table('publishers')->select('id', 'publisher_bn')->where('status', 'Active')->get()->pluck('publisher_bn', 'id');
        $language_list              = ['Bangla' => 'বাংলা', 'English' => 'ইংরেজি'];
        $source_list                = ['Purchased' => 'ক্রয়কৃত', 'Donation' => 'দান'];

        return view('Local/Book::form', ['book_data' => $book_data, 'data' => $data, 'publishers_list' => $publishers_list,
                                              'language_list' => $language_list, 'source_list' => $source_list]);
    }

    public function save(Request $request) {
        $book_data                            = $request->only(['book_title_en', 'book_title_bn', 'isbn', 'writer', 'publisher_id', 'editor', 'accession_no',
                                                                'book_summary', 'publish_year', 'price', 'total_page', 'source', 'volume', 'edition', 'no_copy', 'threshhold', 'book_language']);
        $field_list                           = array('book_title_en', 'book_title_bn', 'isbn', 'writer', 'publisher_id', 'editor', 'accession_no',
                                                      'book_summary', 'publish_year', 'price', 'total_page', 'source', 'volume', 'edition', 'no_copy', 'threshhold');


        if( $request->id ) { // Update
            //echo "<pre>"; print_r($book_data);die;
            $book         = BookModel::find($request->id);
            $old_data     = $book->toArray();
            $book->update($book_data);

            $this->updateBookRegistry($request, $request->id);

            \Common::AuditLog('Book', $request->id, $old_data, $book_data, 'Updated', $field_list);
        }
        else { // Create
            $book = BookModel::create($book_data);
            $this->updateBookRegistry($request, $book->id);

            \Common::AuditLog('Book', $book->id, '', '', 'Create', '');
        }

        return redirect('/Book');
    }

    public function updateBookRegistry($request, $book_id) {
        DB::enableQueryLog();
        $book_data['book_id']           = $book_id;
        $book_data['book_deposited']    = $request->no_copy;
        $book_data['details']           = "বই নির্বাচন কমিটির সুপারিশে ক্রয়কৃত";
        $book_data['created_by']        = Auth::user()->id;

        BookRegistry::create($book_data);
        dd(DB::getQueryLog());
    }
}
