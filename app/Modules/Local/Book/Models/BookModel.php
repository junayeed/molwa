<?php
namespace App\Modules\Local\Book\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class BookModel extends Model
{
    protected $table = 'books';

    protected $fillable = ['book_title_en', 'book_title_bn', 'isbn', 'writer', 'publisher_id', 'editor', 'accession_no',
                           'book_summary', 'publish_year', 'price', 'total_page', 'source', 'volume', 'edition', 'no_copy',
                           'threshhold', 'book_language'];
}
