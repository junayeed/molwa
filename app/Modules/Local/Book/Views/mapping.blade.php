@extends('layouts.master')


@section('content')

    {{Layout::BoxStart("ট্রেড ম্যাপিং", '<a href="/trade" class="btn btn-sm btn-primary mb-1">ট্রেডের তালিকা</a>')}}
    <form method="POST" action="/trade/savemapping">
        {{ csrf_field() }}
        <div class="row">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {{$trade_data->trade_name_bn}}
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div id="trade_tree" class="tree-demo" data-trade_id="{{$trade_data->id}}">
                    </div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>

   {{Layout::BoxEnd()}}

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/metronic/demo/default/custom/components/base/treeview.js" type="text/javascript"></script>
@endsection
@section('page_js')
    <script src="{{url(asset("/js"))}}/trade.js" type="text/javascript"></script>

    <script>
        $(document).ready(function (){

            $('#trade_tree').on('click', function(e){

                var arr      = $('#trade_tree').jstree('get_selected')
                var trade_id = $('#trade_tree').data('trade_id');

                arr.push()
                $.ajax({
                    type:"POST",
                    data:{"trade_id" : trade_id, "ids" : arr, "_token": "{{ csrf_token() }}"},
                    url:"/trade/savemapping",
                    success: function(data) {
                        $('.text').text(JSON.stringify(data));
                    },
                    dataType: 'jsonp',
                });

                console.log($('#trade_tree').jstree('get_selected'))
                e.stopPropagation();
            });

            $('#trade_tree').on('select_node.jstree', function (e, data) {
                if (data.node.children.length > 0) {
                    //$('#trade_tree').jstree(true).deselect_node(data.node);
                    //$('#trade_tree').jstree(true).toggle_node(data.node);
                }
            })

            $("#trade_tree").jstree( {
                    plugins:["wholerow", "checkbox", "types"], core: {
                        themes: {
                            responsive: !1
                        }
                        , data:[ <?=$data?>]
                    }
                    , types: {
                        default: {
                            icon: "fa fa-file m--font-warning"
                        }
                        , file: {
                            icon: "fa fa-file  m--font-warning"
                        }
                    }
                }
            )

        });

    </script>

@endsection

