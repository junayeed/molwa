@extends('layouts.master')
@section('content')
    <form method="POST" action="/batch">
    {{csrf_field()}}
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <!-- TOP RIGHT MENU STARTS HERE -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="actions">
                                <div class="btn-group">
                                    <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                                       data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="/Book/add" class="font-lg">
                                                <i class="fa fa-plus font-green-jungle"></i> নতুন বই নিবন্ধন করুন
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/BookDistribution" class="font-lg">
                                                <i class="fa fa-plus font-green-jungle"></i> বই বিতরণ
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- TOP RIGHT MENU ENDS HERE -->
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">&nbsp;</div>
                        </div>
                        <div id="m_calendar" class="fc fc-unthemed fc-ltr">
                            <div class="fc-view-container" style="">
                                <div class="fc-view fc-listWeek-view fc-list-view fc-widget-content" style="">
                                    <div class="fc-scroller" style="overflow: hidden auto; height: auto;">
                                        <table class="fc-list-table" style="font-size: 1.5em !important;">
                                            <tbody>
                                                <tr class="fc-list-heading">
                                                    <td class="fc-widget-header" colspan="6">
                                                        <a class="fc-list-heading-main">বিতরণের তালিকা</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    {{--<td>ক্রমিক</td>--}}
                                                    <td>বই এর নাম</td>
                                                    <td>ISBN</td>
                                                    <td>লেখক</td>
                                                    <td>প্রকাশক</td>
                                                    <td>বই এর সংখ্যা</td>
                                                </tr>

                                            @if($books)
                                                @foreach($books as $book)
                                                <tr class="fc-list-item" >
                                                    <td class="fc-list-item-time fc-widget-content font-lg">
                                                        <a href="/Book/update/{{$book->id}}">{{$book->book_title_bn}}</a>
                                                        <div class="fc-description">সালঃ {{Html::en2bn( $book->publish_year )}}</div>
                                                        <div class="fc-description">{{$book->writer}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">

                                                        <div class="fc-description">{{$book->isbn}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{$book->writer}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{$publisher_list[$book->publisher_id]}}</div>
                                                    </td>
                                                    <td class="fc-list-item-title fc-widget-content font-lg">
                                                        <div class="fc-description">{{$book->no_copy}}</div>
                                                    </td>
                                                    <td class="text-center no-print">
                                                        <a class="" href="/Book/update/{{$book->id}}"><i class="fa fa-edit"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else
                                                <tr class="fc-list-item m-fc-event--danger">
                                                    <td class="fc-list-item-time fc-widget-content font-lg text-danger ">কোন তথ্য পাওয়া যায়নি</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </form>
    <!-- END CONTENT -->


    <div class="modal fade" id="participant_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details"></div>
        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/batch.js" type="text/javascript"></script>
@endsection