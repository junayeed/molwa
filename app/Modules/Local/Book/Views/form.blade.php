@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')

    <form role="form" id="batch_form" method="POST" action="/Book/save"  enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" value="{{@$book_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- TOP RIGHT MENU STARTS HERE -->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown"
                               data-hover="dropdown" data-close-others="true" style="font-size: 18px;"> মেন্যু
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="/Book/add" class="font-lg">
                                        <i class="fa fa-plus font-green-jungle"></i> নতুন বই নিবন্ধন করুন
                                    </a>
                                </li>
                                <li>
                                    <a href="/Book" class="font-lg"> <i class="fa fa-close font-red-flamingo"></i> বন্ধ করুন </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TOP RIGHT MENU ENDS HERE -->
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i>  বই এর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">বই এর নাম (বাংলা)</label>
                                            <input name="book_title_bn" id="book_title_bn" type="text" class="form-control input-lg " value="{{@$book_data->book_title_bn}}" >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="font-lg">Book Title</label>
                                            <input name="book_title_en" id="book_title_en" type="text" class="form-control input-lg" value="{{@$book_data->book_title_en}}" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">ISBN</label>
                                            <input name="isbn" id="isbn" type="text" class="form-control input-lg" value="{{ @$book_data->isbn}}">
                                         </div>
                                        <div class="form-group col-md-3">
                                            <label class="font-lg">Accession No</label>
                                            <input name="accession_no" id="accession_no" type="number" class="form-control input-lg" value="{{@$book_data->accession_no}}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">প্রকাশের সাল</label>
                                            <input name="publish_year" id="publish_year" type="number" class="form-control input-lg" value="{{@$book_data->publish_year}}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">মূল্য</label>
                                            <input name="price" id="price" type="number" class="form-control input-lg" value="{{@$book_data->batch_size}}">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label class="font-lg">মোট পৃষ্ঠা</label>
                                            <input name="total_page" id="total_page" type="number" class="form-control input-lg" value="{{@$book_data->total_page}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">লেখক</label>
                                            <input name="writer" id="writer" type="text" class="form-control input-lg" value="{{@$book_data->writer}}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">সম্পাদক</label>
                                            <input name="editor" id="editor" type="text" class="form-control input-lg" value="{{@$book_data->editor}}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="font-lg">প্রকাশক</label>
                                            <select name="publisher_id" id="publisher_id" class="form-control input-lg" >
                                                <option value=""> - প্রকাশক বাছাই করুন - </option>
                                                @foreach($publishers_list as $id => $publisher)
                                                    <option value="{{$id}}" @if($id == @$book_data->publisher_id) selected @endif>{{$publisher}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-lg">সারসংক্ষেপ</label>
                                                <textarea name="book_summary" id="book_summary" class="form-control input-lg" rows="4" style="height: 21.2%;">{{@$book_data->book_summary}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="font-lg">খণ্ড</label>
                                                    <input name="volume" id="volume" type="number" class="form-control input-lg" value="{{@$book_data->volume}}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-lg">সংস্করণ</label>
                                                    <input name="edition" id="edition" type="number" class="form-control input-lg" value="{{@$book_data->edition}}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-lg">সংখ্যা</label>
                                                    <input name="no_copy" id="no_copy" type="number" class="form-control input-lg" value="{{@$book_data->no_copy}}">
                                                </div>
                                            </div>
                                            <div class="row margin-top-15">
                                                <div class="col-md-4">
                                                    <label class="font-lg">প্রান্তিক সীমা</label>
                                                    <input name="threshhold" id="threshhold" type="number" class="form-control input-lg" value="{{@$book_data->threshhold}}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-lg">ভাষা</label>
                                                    <select name="book_language" id="book_language" class="form-control input-lg" >
                                                        <option value=""> - ভাষা বাছাই করুন - </option>
                                                        @foreach ($language_list as $id => $language)
                                                        <option value="{{$id}}" @if($id == @$book_data->book_language) selected @endif>{{$language}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="font-lg">উৎস</label>
                                                    <select name="source" id="source" class="form-control input-lg" >
                                                        <option value=""> - উৎস বাছাই করুন - </option>
                                                        @foreach ($source_list as $id => $source)
                                                        <option value="{{$id}}" @if($id == @$book_data->source) selected @endif>{{$source}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-save btn-success font-hg col-md-2"><i class="fa fa-floppy-o"></i> সংরক্ষণ করুন</button>
                        {{--<button class="btn btn-cancel"><i class="fa fa-reply-all"></i> ব্যাচের তালিকায় ফেরৎ আসুন</button>--}}
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-edit font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase" style="font-size: 22px !important;">বই এর প্রচ্ছদ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 417px; height: 429px;">
                                    @if ( @$emp_data->profile_image )
                                        <img src="/{{@$emp_data->profile_image}}" alt="" />
                                    @else
                                        <img src="../images/book_no_image.png" alt="" style="width: 400px; height: 400px;" />
                                    @endif
                                </div>
                                <div>
                                    <span class="btn red btn-outline btn-file">
                                        <span class="fileinput-new btn btn-success btn-sm">  বই এর প্রচ্ছদ বাছাই করুন </span>
                                        <span class="fileinput-exists btn btn-warning  btn-sm"> পরিবর্তন করুন </span>
                                        <input type="file" name="profile_image" class="form-control m-input">
                                    </span>
                                    <a href="javascript:;" class="btn btn-danger btn-sm red fileinput-exists" data-dismiss="fileinput"> মুছে ফেলুন </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="row">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cog"></i>
                                <span class="caption-subject font-dark bold uppercase"  style="font-size: 22px !important;">লগ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
            <!-- AUDIT LOG END -->
        </div>
    </form>

    <!-- Attachment View Modal STARTS -->
    <div id="attachment_view_modal" class="modal fade in">
        <div class="modal-dialog modal-lg modal-scroll"  style="max-height:700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="attachment_view_modal_title"></h4>
                </div>
                <div class="modal-body" id="attachment_view">
                    <iframe id="attachment_iframe" src="" width="100%" height="70%" frameborder="0" ></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Attachment View Modal ENDS -->

    <!-- SMS Modal STARTS -->
    <div class="modal fade" id="sms_modal" tabindex="-1" role="dialog" aria-labelledby="forwardModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content" id="participant_details">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"> বার্তা </h4>
                </div>
                <div class="modal-body" id="">

                    <div class="row">
                        <div class="col-md-5">
                            <textarea  class="form-control input" style="height: 180px;" id="sms_text"></textarea>
                        </div>
                        <div class="col-md-2">
                            <div class="easy-pie-chart">
                                <div class="number sms_sending" data-percent="0">
                                    <span class="bold sms_sent_number" style="top: 25px; position: relative; left: 25px; font-size: 40px;">0</span>&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible="1">
                                <ul class="feeds">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn red-flamingo btn-outline font-lg" data-dismiss="modal">বন্ধ করুন</button>
                    <button type="button" class="btn green-haze btn-outline font-lg" id="send_sms_btn">বার্তা প্রেরণ করুন</button>
                </div>
            </div>
        </div>
    </div>

    <!-- SMS Modal ENDS -->
@endsection

@section('page_plugins')


@endsection

@section('page_js')
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{ Config::get('constants.CONSTANT_NAME') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url(asset("/js"))}}/batch.js" type="text/javascript"></script>
@endsection

<script>
    var trainer_array       = [];

    @if (@$batch_trainer_data)
    @foreach($batch_trainer_data as $trainer)
        @php $trainer->session_date = \Carbon\Carbon::parse(@$trainer->session_date)->format('d/m/Y'); @endphp
        trainer_array.push ({!! json_encode($trainer) !!});
    @endforeach
    @endif

    var apa_performance_index_val = "{!! @$book_data->apa_performance_index !!}";

</script>