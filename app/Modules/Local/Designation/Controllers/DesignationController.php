<?php
namespace App\Modules\Local\Designation\Controllers;

use App\Modules\Local\Designation\Models\DesignationModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Designation;
use App\Models\Office;

class DesignationController extends Controller
{
    function __construct(){
	}

    function index() {
        $designation_data           = DesignationModel::select()->orderBy('weight', 'ASC')->get()->all();
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Designation::list', ['designation_data' => $designation_data, 'data' => $data]);
    }

    function add() {

        $designation_list           = Designation::select('id', 'name_bn', 'weight', 'status')->orderBy('weight', 'ASC')->get();
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Designation::form', ['data' => $data, 'designation_list' => $designation_list]);
    }

    function update($id) {
        $designation_data             = DesignationModel::find($id);
        $designation_list             = Designation::select('id', 'name_bn', 'weight', 'status')->orderBy('weight', 'ASC')->get();
        $auditLogData                 = \Common::getAuditLogData('Designation', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        $data['nav_item']             = str_replace("Controller", "", $controller);
        list($controller, $action)    = explode('@', $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Designation::form', ['designation_data' => $designation_data, 'data' => $data, 'designation_list' => $designation_list]);
    }

    function save(Request $request) {
        $designation_data  = $request->only(['name_en', 'name_bn', 'weight', 'status']);
        $field_list        = array('name_en', 'name_bn', 'weight', 'status');

        if( $request->id ) {
            $designation      = DesignationModel::find($request->id);
            $old_data      = $designation->toArray();
            $designation->update($designation_data);

            \Common::AuditLog('Designation', $request->id, $old_data, $designation_data, 'Updated', $field_list);
        }
        else {
            $designation_id = DesignationModel::create($designation_data)->id;

            \Common::getAuditLogData('Designation', $designation_id, '', '', 'Create', '');
        }

        return redirect('/designation');
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/batch');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
