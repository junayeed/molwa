<?php

Route::group(['prefix' => 'designation', 'namespace' => 'App\Modules\Local\Designation\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',             ['as' => 'designation.index',       'uses' => 'DesignationController@index']);
    Route::get('/show/{id}',    ['as' => 'designation.show',        'uses' => 'DesignationController@show']);
    Route::get('/add',          ['as' => 'designation.index',       'uses' => 'DesignationController@add']);
    Route::get('/update/{id}',  ['as' => 'designation.index',       'uses' => 'DesignationController@update']);
    Route::post('/save',        ['as' => 'designation.index',       'uses' => 'DesignationController@save']);
    Route::get('/delete/{id}',  ['as' => 'designation.index',       'uses' => 'DesignationController@delete']);
});




?>