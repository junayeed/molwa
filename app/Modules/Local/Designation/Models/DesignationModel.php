<?php
namespace App\Modules\Local\Designation\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class DesignationModel extends Model
{
    protected $table = 'designations';

    protected $fillable = ['name_en', 'name_bn', 'weight', 'status'];
}
