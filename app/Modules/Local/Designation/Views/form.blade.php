@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    @include('layouts.messages')
    <form role="form" id="designation_form" method="POST" action="/designation/save"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{@$designation_data->id}}">
        {{csrf_field()}}
        <div class="row">
            <!-- EMPLOYEE DETAILS START -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cog"></i> কর্মকর্তা / কর্মচারীর তথ্য
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>পদবি (বাংলা)</label>
                                            <input name="name_bn" id="name_bn" type="text" class="form-control input" value="{{@$designation_data->name_bn}}">

                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>পদবি (ইংরেজী)</label>
                                            <input name="name_en" id="name_en" type="text" class="form-control input" value="{{@$designation_data->name_en}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>ওয়েট</label>
                                            <input name="weight" id="weight" type="number" class="form-control input" value="{{@$designation_data->weight}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>অবস্থা</label>
                                            <select name="status" id="status" class="form-control input">
                                                <option value=""> - বাছাই করুণ - </option>
                                                <option value="Active" @if(@$designation_data->status == 'Active') selected @endif>সক্রিয়</option>
                                                <option value="Inactive" @if(@$designation_data->status == 'Inactive') selected @endif>নিষ্ক্রিয়</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- EMPLOYEE DETAILS END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple scr ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-share-alt"></i> লগ
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row scroller"  style="height: 250px;">
                                    <ul class="feeds">
                                    @foreach($designation_list as $d)
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc sbold"> {{$d->name_bn}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date">
                                                    <span class="label label-sm label-warning "> {{$d->weight}}</span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AUDIT LOG END -->
            <!-- AUDIT LOG START -->
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cog"></i> লগ
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AUDIT LOG END -->
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body text-center">
                        <button type="submit" class="btn btn-save"><i class="fa fa-floppy-o"></i> save</button>
                        <a href="#cancel_modal" class="btn btn-cancel" data-toggle="modal"><i class="fa fa-ban"></i> cancel</a>

                    </div>
                </div>

            </div>
        </div>
    </form>
    <!-- Cancel Modal -->
    <div id="cancel_modal" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="cancel-modal-text">Are you sure you want to cancel?</p>
                </div>
                <div class="modal-footer">
                    <a href="/SubIndicator" class="btn btn-modal-cancel">Cancel Anyway</a>
                    <button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection

@section('page_plugins')
    <script src="{{url(asset("/theme"))}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

@endsection

@section('page_js')
    <script src="{{url(asset("/js"))}}/designation.js" type="text/javascript"></script>
@endsection