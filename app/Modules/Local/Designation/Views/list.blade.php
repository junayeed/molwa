@extends('layouts.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">

                            <div class="actions">
                                <a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/designation/add"><i class="fa fa-plus"></i> নতুন পদবি </a>
                                {{--<a id="sample_editable_1_new" class="btn btn-circle green btn-add-new" href="/createUNO"> Create UNO <i class="fa fa-plus"></i></a>--}}
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_list_view">
                                <thead>
                                <tr>
                                    <th class="table_heading">ক্রমিক</th>
                                    <th class="table_heading">পদবি (বাংলা)</th>
                                    <th class="table_heading">পদবি (ইংরেজী)</th>
                                    <th class="table_heading">ওয়েট</th>
                                    <th class="table_heading">অবস্থা</th>
                                    <th class="table_heading text-center no-print">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($designation_data as $designation)
                                    <tr class="odd gradeX">
                                        <td>{{$i++}}</td>
                                        <td>{{$designation->name_bn}}</td>
                                        <td>{{$designation->name_en}}</td>
                                        <td>{{$designation->weight}}</td>
                                        <td>
                                            @if($designation->status == 'Active') সক্রিয়
                                            @else নিষ্ক্রিয়
                                            @endif
                                        </td>
                                        <td class="text-center no-print">
                                            <a class="btn btn-circle btn-warning btn-sm " href="/designation/update/{{$designation->id}}"><i class="fa fa-edit"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection