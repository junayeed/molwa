<?php

Route::group(['prefix' => 'office', 'namespace' => 'App\Modules\Local\Office\Controllers', 'middleware' => ['web']], function () {
    Route::get('/',             ['as' => 'office.index',       'uses' => 'OfficeController@index']);
    Route::get('/show/{id}',    ['as' => 'office.show',        'uses' => 'OfficeController@show']);
    Route::get('/add',          ['as' => 'office.index',       'uses' => 'OfficeController@add']);
    Route::get('/update/{id}',  ['as' => 'office.index',       'uses' => 'OfficeController@update']);
    Route::post('/save',        ['as' => 'office.index',       'uses' => 'OfficeController@save']);
    Route::get('/delete/{id}',  ['as' => 'office.index',       'uses' => 'OfficeController@delete']);
});




?>