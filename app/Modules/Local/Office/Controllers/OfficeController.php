<?php
namespace App\Modules\Local\Office\Controllers;

use App\Modules\Local\Office\Models\OfficeModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Designation;


class OfficeController extends Controller
{
    function __construct(){
	}

    function index() {
        $office_data                = OfficeModel::select()->get()->all();
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Office::list', ['office_data' => $office_data, 'data' => $data]);
    }

    function add() {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('Local/Office::form', ['data' => $data]);
    }

    function update($id) {
        $office_data                  = OfficeModel::find($id);
        $auditLogData                 = \Common::getAuditLogData('Designation', $id);
        $action                       = app('request')->route()->getAction();
        $controller                   = class_basename($action['controller']);
        $data['nav_item']             = str_replace("Controller", "", $controller);
        list($controller, $action)    = explode('@', $controller);
        $data['nav_sub_item']         = $action;

        return view('Local/Office::form', ['office_data' => $office_data, 'data' => $data]);
    }

    function save(Request $request) {
        $office_data  = $request->only(['office_name_en', 'office_name_bn', 'office_status']);
        $field_list        = array('office_name_en', 'office_name_bn', 'office_status');

        if( $request->id ) {
            $office      = OfficeModel::find($request->id);
            $old_data      = $office->toArray();
            $office->update($office_data);

            \Common::AuditLog('Office', $request->id, $old_data, $office_data, 'Updated', $field_list);
        }
        else {
            $office_id = OfficeModel::create($office_data)->id;

            \Common::getAuditLogData('Office', $office_id, '', '', 'Create', '');
        }

        return redirect('/office');
    }

    function delete($id) {
        BatchModel::find($id) -> delete();
        return redirect('/batch');
    }

    function show($id){
        $asset = BatchModel::find($id);
        return view('Local/Batch::show', ['data' => $asset]);

    }
}
