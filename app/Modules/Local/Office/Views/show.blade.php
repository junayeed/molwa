@extends('layouts.master')

@section('page_css')
    <link href="{{url(asset("/theme"))}}/metronic/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{url(asset("/css"))}}/image_slider.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!--Theme page used:
theme/default/dist/default/components/widgets/general.html
theme/default/dist/default/crud/datatables/basic/basic.html

-->

    {{Layout::BoxStart($data->name, '<a href="/asset/update/'.$data->id.'" class="btn btn-sm btn-warning m-btn m-btn--custom m-btn--icon"> <i class="la la-edit"></i> Edit Asset</a>')}}


     <!--Begin::Section-->
     <div class="m-portlet m-portlet--border-bottom-accent">
         <div class="m-portlet__body m-portlet__body--no-padding">
             <div class="row m-row--no-padding m-row--col-separator-xl">
                 <div class="col-md-12 col-lg-12 col-xl-4">

                     <!--begin:: Widgets/Stats2-1 -->
                     <div class="m-widget1">
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Item Tag</h3>
                                     <span class="m-widget1__desc">CA-V-732001</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Installation</h3>
                                     <span class="m-widget1__desc">Weekly Customer Orders</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Maximo PM</h3>
                                     <span class="m-widget1__desc">System bugs and issues</span>
                                 </div>

                             </div>
                         </div>
                     </div>

                     <!--end:: Widgets/Stats2-1 -->
                 </div>
                 <div class="col-md-12 col-lg-12 col-xl-4">

                     <!--begin:: Widgets/Stats2-2 -->
                     <div class="m-widget1">
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Item Description</h3>
                                     <span class="m-widget1__desc">Awerage IPO Margin</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Location</h3>
                                     <span class="m-widget1__desc">Yearly Expenses</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Maximo Location</h3>
                                     <span class="m-widget1__desc">Overall Regional Logistics</span>
                                 </div>

                             </div>
                         </div>
                     </div>

                     <!--begin:: Widgets/Stats2-2 -->
                 </div>
                 <div class="col-md-12 col-lg-12 col-xl-4">

                     <!--begin:: Widgets/Stats2-3 -->
                     <div class="m-widget1">
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Owner</h3>
                                     <span class="m-widget1__desc">Awerage Weekly Orders</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title">Next inspection year</h3>
                                     <span class="m-widget1__desc">2019</span>
                                 </div>

                             </div>
                         </div>
                         <div class="m-widget1__item">
                             <div class="row m-row--no-padding align-items-center">
                                 <div class="col">
                                     <h3 class="m-widget1__title"></h3>
                                     <span class="m-widget1__desc"></span>
                                 </div>

                             </div>
                         </div>
                     </div>

                     <!--begin:: Widgets/Stats2-3 -->
                 </div>
             </div>
         </div>
     </div>

     <!--End::Section-->

     <!--Begin::Section-->
     <div class="row">
         <div class="col-xl-6">

             <!--begin:: Widgets/Diagram -->
             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 Diagrams
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools"></div>
                 </div>
                 <div class="m-portlet__body">
                     <img class="embed-responsive" src="{{asset('/images')}}/NII.png">

                 </div>

             </div>

             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 Photos
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools"></div>
                 </div>
                 <div class="m-portlet__body">

                     <div class="container">
                         <div class="row blog">
                             <div class="col-md-12">
                                 <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                                     <ol class="carousel-indicators">
                                         <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                                         <li data-target="#blogCarousel" data-slide-to="1"></li>
                                     </ol>

                                     <!-- Carousel items -->
                                     <div class="carousel-inner">

                                         <div class="carousel-item active">
                                             <div class="row">
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>
                                             </div>
                                             <!--.row-->
                                         </div>
                                         <!--.item-->

                                         <div class="carousel-item">
                                             <div class="row">
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>
                                                 <div class="col-md-4">
                                                     <a href="#">
                                                         <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                                     </a>
                                                 </div>

                                             </div>
                                             <!--.row-->
                                         </div>
                                         <!--.item-->

                                     </div>
                                     <!--.carousel-inner-->
                                 </div>
                                 <!--.Carousel-->

                             </div>
                         </div>
                     </div>

                 </div>
             </div>

             <!--end:: Widgets/Diagram -->
         </div>
         <div class="col-xl-6">

             <!--begin:: Widgets/Inspection Due -->
             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 Inspection Due
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools">
                         <a href="#" class="btn btn-sm btn-primary m-btn m-btn--custom m-btn--icon">
							<span>
								<i class="la la-plus"></i> Add
							</span>
                         </a>

                     </div>
                 </div>
                 <div class="m-portlet__body row">
                     <table class="table table-striped- table-bordered table-hover" id="m_table_1">
                         <thead>
                             <tr>
                                 <th>ID</th>
                                 <th>Inspection Type</th>
                                 <th>Due Date</th>
                                 <th>Inspector</th>
                             </tr>
                         </thead>
                         <tbody>
                         <tr>
                             <td>1</td>
                             <td>61715-075</td>
                             <td>2/12/2018</td>
                             <td nowrap>Bash</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>63629-4697</td>
                             <td>8/6/2017</td>
                             <td nowrap>Naveed</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>68084-123</td>
                             <td>5/26/2016</td>
                             <td nowrap>SAM</td>
                         </tr>
                         </tbody>
                     </table>
                 </div>
             </div>

             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 Complete Inspections
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools">
                         <a href="#" class="btn btn-sm btn-success m-btn m-btn--custom m-btn--icon">
							<span>
								<i class="la la-plus"></i> Add
							</span>
                         </a>
                     </div>
                 </div>
                 <div class="m-portlet__body">
                     <table class="table table-striped- table-bordered table-hover" id="m_table_1">
                         <thead>
                         <tr>
                             <th>ID</th>
                             <th>Inspection Type</th>
                             <th>Due Date</th>
                             <th>Inspector</th>
                         </tr>
                         </thead>
                         <tbody>
                         <tr>
                             <td>1</td>
                             <td>61715-075</td>
                             <td>2/12/2018</td>
                             <td nowrap>SAM</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>63629-4697</td>
                             <td>8/6/2017</td>
                             <td nowrap>Bash</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>68084-123</td>
                             <td>5/26/2016</td>
                             <td nowrap>Naveed</td>
                         </tr>
                         </tbody>
                     </table>

                 </div>
             </div>

             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 RBIs
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools">
                         <a href="#" class="btn btn-sm btn-warning m-btn m-btn--custom m-btn--icon">
							<span>
								<i class="la la-plus"></i> Add
							</span>
                         </a>
                     </div>
                 </div>
                 <div class="m-portlet__body">
                     <table class="table table-striped- table-bordered table-hover" id="m_table_1">
                         <thead>
                         <tr>
                             <th>ID</th>
                             <th>Inspection Type</th>
                             <th>Due Date</th>
                             <th>Inspector</th>
                         </tr>
                         </thead>
                         <tbody>
                         <tr>
                             <td>1</td>
                             <td>61715-075</td>
                             <td>2/12/2018</td>
                             <td nowrap>Naveed</td>
                         </tr>
                         <tr>
                             <td>2</td>
                             <td>63629-4697</td>
                             <td>8/6/2017</td>
                             <td nowrap>Bash</td>
                         </tr>
                         <tr>
                             <td>3</td>
                             <td>68084-123</td>
                             <td>5/26/2016</td>
                             <td nowrap>Bash</td>
                         </tr>
                         </tbody>
                     </table>

                 </div>
             </div>

             <div class="m-portlet m-portlet--border-bottom-accent">
                 <div class="m-portlet__head">
                     <div class="m-portlet__head-caption">
                         <div class="m-portlet__head-title">
                             <h3 class="m-portlet__head-text">
                                 MCDRs
                             </h3>
                         </div>
                     </div>
                     <div class="m-portlet__head-tools">
                         <a href="#" class="btn btn-sm btn-warning m-btn m-btn--custom m-btn--icon">
							<span>
								<i class="la la-plus"></i> Add
							</span>
                         </a>

                     </div>
                 </div>
                 <div class="m-portlet__body">

                     <table class="table table-striped- table-bordered table-hover" id="m_table_1">
                         <thead>
                         <tr>
                             <th>ID</th>
                             <th>Inspection Type</th>
                             <th>Due Date</th>
                             <th>Inspector</th>
                         </tr>
                         </thead>
                         <tbody>
                         <tr>
                             <td>1</td>
                             <td>61715-075</td>
                             <td>2/12/2018</td>
                             <td nowrap>Bash</td>
                         </tr>

                         </tbody>
                     </table>

                 </div>
             </div>

             <!--end:: Widgets/Inspection Due -->
         </div>
     </div>

     <!--End::Section-->



    {{Layout::BoxEnd()}}


@endsection

@section('page_plugins')
@endsection

@section('page_js')

    <script src="{{url(asset("/theme"))}}/metronic/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

    <script type="javascript/text">
        $(document).ready(function(){
            $("form").validate();

            $('#blogCarousel').carousel({
				interval: 5000
		});
        });

    </script>
@endsection
