<?php
namespace App\Modules\Local\Office\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class OfficeModel extends Model
{
    protected $table = 'offices';

    protected $fillable = ['office_name_en', 'office_name_bn', 'office_status'];
}
