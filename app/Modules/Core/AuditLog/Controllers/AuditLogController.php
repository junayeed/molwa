<?php
namespace App\Modules\Core\User_Management\Controllers;
use App\Modules\Core\User_Management\Models\UserModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Validator;


class UserController extends Controller
{
    function __construct(){
	}

    function index() {
        $data = UserModel::all();
        return view('Core/User_Management::list', ['data' => $data]);
    }
    function add() {
        $data = (object) array(
            'id'    => '',
            'name' => '',
            'email'  => '',
            'password' => ''
        );
        return view('Core/User_Management::form', ['data' => $data]);
    }
    function  update($id) {
        $data = UserModel::find($id);
        return view('Core/User_Management::form', ['data' => $data]);
    }

    function save(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/user/add')
                        ->withErrors($validator)
                        ->withInput();
        }

        if( $request -> id) {

            $user = UserModel::find($request -> id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            if(!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            }
            $user->save();

        } else {

            UserModel::create([
                'name' => $request -> get('name'),
                'email' => $request -> get('email'),
                'password' => Hash::make($request -> get('password'))
            ]);
        }


        return redirect('/user');
    }
    function delete($id) {
        UserModel::find($id) -> delete();
        return redirect('/user');
    }
}
