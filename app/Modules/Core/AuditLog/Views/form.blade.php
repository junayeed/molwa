@extends('layouts.master')

@section('page_css')
@endsection

@section('content')


    {{Layout::BoxStart("User", '<a href="/user" class="btn btn-sm btn-primary mb-1">List Users</a>')}}
        <form method="POST" id="frmUser" action="/user/save">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$data -> id}}" />
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input type="text" class="required form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{$data -> name}}" autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                <div class="col-md-6">
                    <input type="email" class="required form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{$data -> email}}" />

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input type="text" class="required form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" value="">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
    {{Layout::BoxEnd()}}

@endsection

@section('page_plugins')
@endsection

@section('page_js')
<script type="javascript/text">
    $(document).ready(function(){
        $("form").validate();
    });
</script>
@endsection
