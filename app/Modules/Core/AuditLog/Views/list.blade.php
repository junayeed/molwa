@extends('layouts.master')

@section('page_css')
@endsection

@section('content')
    {{Layout::BoxStart("Users", '<a href="/user/add" class="btn btn-sm btn-primary mb-1">Add Users</a>')}}
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>

                    <td>
                        <a href="/user/update/{{$row->id}}" class="btn btn-sm btn-info">Edit</a>
                        <a href="/user/delete/{{$row->id}}" class="btn btn-sm btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    {{Layout::BoxEnd()}}

@endsection

@section('page_plugins')
@endsection

@section('page_js')
@endsection
