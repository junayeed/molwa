<?php

namespace App\Modules\Core\AuditLog\Models;
use Illuminate\Database\Eloquent\Model;

class AuditLogModel extends Model
{
    protected $table = 'audit_logs';

    protected $fillable = [ 'row_id', 'module_name', 'previous_value', 'current_value', 'action', 'action_by', 'field_updated' ];

}
