<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class CourtContempt extends Model
{
    //use Notifiable;
    protected $table = "court_contempted";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'contempt_no', 'contempt_received_date', 'appearance_date', 'contempt_remarks'];
}