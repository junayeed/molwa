<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class StatementFacts extends Model
{
    //use Notifiable;
    protected $table = "sf_details";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'sf_recipient', 'sf_ministry', 'sf_dc_office','sf_uno_office','sf_issue_memo', 'sf_issue_date', 'sf_receive_memo', 'sf_receive_date',
                           'sf_sending_memo', 'sf_sending_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    */

    /*public function statementFacts()
    {
        return $this->belongsTo(ForwardDairy::class, 'case_id', 'id');
    }*/
}
