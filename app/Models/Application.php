<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
	protected $table = 'applications';

    protected $guarded = ['id'];

    public function mainArea()
    {
        return $this->belongsTo('App\Models\MainArea', 'main_area', 'id');
    }

    public function getUpazilaInfoRow()
    {
        return $this->belongsTo('App\Models\Upazila', 'upazila_id', 'id');
    }

    public function getDistrictInfoRow()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');
    }
}
