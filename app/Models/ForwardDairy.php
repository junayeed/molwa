<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class ForwardDairy extends Model
{
    //use Notifiable;
    protected $table = "cases";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_no', 'case_no', 'case_type', 'case_receive_date', 'parties_name', 'subject_type',
                           'case_subject', 'case_status', 'has_interim_order', 'has_rule_nisi', 'created_by', 'hard_file_no',
                           'page_no', 'appendix_list', 'has_court_contempted', 'writ_petition_order_date', 'case_year', 'has_judgement',
                           'has_supplementary_order'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDistrictInfoRow()
    {
        return $this->belongsTo('App\Models\District', 'district', 'id');
    }
    public function getUpazilaInfoRow()
    {
        return $this->belongsTo('App\Models\Upazila', 'upazila', 'id');
    }
    public function getUserGroupInfoRow()
    {
        return $this->belongsTo('App\Models\UserGroup', 'user_level', 'id');
    }*/

    public function data()
    {
        return $this->hasMany(InterimOrder::class);
    }
}
