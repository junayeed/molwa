<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $guarded = ['id'];

    public function getUserBasicInfoRow()
    {
        return $this->belongsTo('App\User', 'from_id', 'id');
    }

    public function getApplicationBasicInfoRow()
    {
        return $this->belongsTo('App\Models\Application', 'application_id', 'id');
    }
}
