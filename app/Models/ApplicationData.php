<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ApplicationData extends Model
{
    protected $table = 'application_datas';

    protected $guraded = ['id'];

    public function getSubIndicatorInfoRow(){
    	return $this->belongsTo('App\Models\SubIndicator', 'sub_indicator_id', 'id');
    }

    

}
