<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class FFData extends Model
{
    //use Notifiable;
    protected $table = "ff_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ff_id', 'ff_no', 'ff_list_type_id', 'ff_mother_name', 'ff_dob', 'ff_image_path', 'ff_status', 'doc_type', 'ff_doc_no', 'created_by', 'blood_group'];



    public function data()
    {
        return $this->hasMany(InterimOrder::class);
    }
}
