<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Indicator;
use App\Models\SubIndicator;

class MainArea extends Model
{
    //use Notifiable;
    protected $table = "mainareas";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['mainarea_en', 'mainarea_bn', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDistrictInfoRow()
    {
        return $this->belongsTo('App\Models\District', 'district', 'id');
    }
    public function getUpazilaInfoRow()
    {
        return $this->belongsTo('App\Models\Upazila', 'upazila', 'id');
    }
    public function getUserGroupInfoRow()
    {
        return $this->belongsTo('App\Models\UserGroup', 'user_level', 'id');
    }*/

    public function getIndicatorInfoRow()
    {
        return $this->hasMany('App\Models\Indicator', 'main_area', 'id');
    }

    public function getSubIndicatorCountforMainArea($id)
    {
        $indicator = Indicator::where('main_area', $id)->select('id')->get();
        $indicator = $indicator->toArray();

        $count = SubIndicator::whereIn('indicator_id', $indicator)->count();

        return $count;
    }
}
