<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class SubIndicator extends Model
{
    //use Notifiable;
    protected $table = "sub_indicators";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['indicator_id', 'serial', 'sub_indicator_title_en', 'sub_indicator_title_bn', 'sub_indicator_status', 'amount', 'mainarea'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDistrictInfoRow()
    {
        return $this->belongsTo('App\Models\District', 'district', 'id');
    }
    public function getUpazilaInfoRow()
    {
        return $this->belongsTo('App\Models\Upazila', 'upazila', 'id');
    }
    public function getUserGroupInfoRow()
    {
        return $this->belongsTo('App\Models\UserGroup', 'user_level', 'id');
    }*/
    public function getApplicationData($application_id, $sub_indicator_id)
    {
        $count = ApplicationData::where('application_id', $application_id)->where('sub_indicator_id', $sub_indicator_id)->first();

        return $count;
    }

    public function getIndicatorInfoRow(){
        return $this->belongsTo('App\Models\Indicator', 'indicator_id', 'id');
    }
    
}
