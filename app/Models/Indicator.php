<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SubIndicator;
use App\Models\ApplicationData;

class Indicator extends Model
{
    //use Notifiable;
    protected $table = "indicators";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['main_area', 'serial', 'indicator_title', 'indicator_title_bn', 'indicator_status'];

    public function mainarea()
    {
        return $this->belongsTo('App\Models\MainArea', 'main_area', 'id');
    }

    public function getSubIndicatorInfoRow()
    {
        return $this->hasMany('App\Models\SubIndicator', 'indicator_id', 'id');
    }
    public function getSubIndicatorCount($id)
    {
        $count = SubIndicator::where('indicator_id', $id)->count();

        return $count;
    }
    public function getApplicationData($application_id, $main_area_id, $indicator_id, $sub_indicator_id, $column_name)
    {
        $application_data = ApplicationData::where('application_id', $application_id)->where('main_area_id', $main_area_id)->where('indicator_id', $indicator_id)->where('sub_indicator_id', $sub_indicator_id)->select($column_name)->first();
        

        return $application_data;
    }
}
