<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class AOR extends Model
{
    //use Notifiable;
    protected $table = "aor_details";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'aor_name', 'aor_cell_no', 'aor_memo_no', 'aor_memo_date'];
}
