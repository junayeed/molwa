<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class SupplementaryOrder extends Model
{
    //use Notifiable;
    protected $table = "supplementary_orders";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'supplementary_order_date', 'supplementary_order_details'];
}