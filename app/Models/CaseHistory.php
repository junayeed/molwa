<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SubIndicator;
use App\Models\ApplicationData;

class CaseHistory extends Model
{
    //use Notifiable;
    protected $table = "case_update_history";

    public $timestamps  = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['caseID', 'fieldUpdated', 'previousValue', 'newValue', 'updatedTime', 'updatedBy'];

}
