<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class InterimOrderDetails extends Model
{
    //use Notifiable;
    protected $table = "interim_orders_details";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'interim_order_id', 'implementing_authority', 'ministry', 'dc_office', 'uno_office', 'responsibility', 'int_issue_memo', 'int_issue_date', 'int_receive_memo', 'int_receive_date', 'int_sending_memo', 'int_sending_date'];
}
