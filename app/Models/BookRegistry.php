<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class BookRegistry extends Model
{
    //use Notifiable;
    protected $table = "book_registry";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['book_id', 'book_deposite', 'book_distributed', 'details', 'comments', 'created_by'];
}
