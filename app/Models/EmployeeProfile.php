<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeProfile extends Model
{
    protected $table = "employee_profiles";

    protected $fillable = ['designation', 'office', 'employee_class', 'pay_grade'];
}
