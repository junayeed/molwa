<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class InterimOrder extends Model
{
    //use Notifiable;
    protected $table = "interim_orders";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'interim_order_date', 'order_details', 'deadline'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    */

    public function forwardDairy()
    {
        return $this->belongsTo(ForwardDairy::class, 'case_id', 'id');
    }
}
