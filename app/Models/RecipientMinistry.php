<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class RecipientMinistry extends Model
{
    //use Notifiable;
    protected $table = "recipient_ministry";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'recipient_ministry', 'memo_no', 'issue_date', 'remarks', 'recipient_others'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    */

    public function forwardDairy()
    {
        return $this->belongsTo(ForwardDairy::class, 'case_id', 'id');
    }
}
