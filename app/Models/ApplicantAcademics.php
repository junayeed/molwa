<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class ApplicantAcademics extends Model
{
    //use Notifiable;
    protected $table = "applicant_academics";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['applicant_id', 'exam_name', 'exam_year', 'edu_inst', 'edu_board', 'edu_group', 'cgpa', 'cgpa_wo_4th_subject'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDistrictInfoRow()
    {
        return $this->belongsTo('App\Models\District', 'district', 'id');
    }
    public function getUpazilaInfoRow()
    {
        return $this->belongsTo('App\Models\Upazila', 'upazila', 'id');
    }
    public function getUserGroupInfoRow()
    {
        return $this->belongsTo('App\Models\UserGroup', 'user_level', 'id');
    }*/

    public function data()
    {
        return $this->hasMany(InterimOrder::class);
    }

    public function getApplicationData($application_id, $sub_indicator_id)
    {
        $count = ApplicationData::where('application_id', $application_id)->where('sub_indicator_id', $sub_indicator_id)->first();

        return $count;
    }

    public function getIndicatorInfoRow(){
        return $this->belongsTo('App\Models\Indicator', 'indicator_id', 'id');
    }
    
}
