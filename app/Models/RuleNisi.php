<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class RuleNisi extends Model
{
    //use Notifiable;
    protected $table = "rule_nisi_orders";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'rule_nisi_order_date', 'rule_nisi_order_details'];
}