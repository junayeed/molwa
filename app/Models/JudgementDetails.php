<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationData;


class JudgementDetails extends Model
{
    //use Notifiable;
    protected $table = "judgement_details";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['case_id', 'cmp_no', 'cmp_judgement', 'cp_no', 'cp_judgement', 'appeal_case_no', 'appeal_judgement', 'review_petition_no', 'review_order', 'final_judgement'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];

    */
}
