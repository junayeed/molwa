<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indicator;
use App\Models\MainArea;
use App\Models\SubIndicator;
use App\Models\Application;
use App\Models\ApplicationData;
use App\Models\UserGroup;
use Illuminate\Support\Facades\Input;
use Auth;

class CumulativeReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_area_list = MainArea::get();

        return view('report.indexCumulative', compact('main_area_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getIndicatorInfoRow(Request $request)
    {
        $main_area_id = $request->input('main_area_id');

        $indicator_list = Indicator::where('main_area', $main_area_id)->get();

        echo '<option value="">Please Select</option>';
        if($indicator_list)
        {
            foreach($indicator_list as $rowdata)
            {
                echo '<option value="'.$rowdata->id.'">'.$rowdata->indicator_title.'</option>';
            }
        }
        else
        {
            
        }
    }
    public function getSubIndicatorInfoRow(Request $request)
    {
        $indicator_id = $request->input('indicator_id');

        $sub_indicator_list = SubIndicator::where('indicator_id', $indicator_id)->get();

        echo '<option value="">Please Select</option>';
        if($sub_indicator_list)
        {
            foreach($sub_indicator_list as $rowdata)
            {
                echo '<option value="'.$rowdata->id.'">'.$rowdata->sub_indicator_title_en.'</option>';
            }
        }
        else
        {

        }
    }

    public function getCumulativeReport(Request $request)
    {
        $main_area = $request->input('main_area');
        $indicator = $request->input('indicator');
        $sub_indicator = $request->input('sub_indicator');
        $application_name = $request->input('application_name');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $year = $request->input('year');
        $month = $request->input('month');

        $q = Application::query();

        if (Input::has('application_name')) {
            $q->where('name', 'LIKE', '%'.Input::get('application_name').'%');
        }
        if (Input::has('from_date')) {
            $q->where('date', '>=', Input::get('from_date'));
        }
        if (Input::has('to_date')) {
            $q->where('date', '<=', Input::get('to_date'));
        }
        if (Input::has('year')) {
            $q->where('year', Input::get('year'));
        }
        if (Input::has('month')) {
            $q->where('month', Input::get('month'));
        }


        $user_group = UserGroup::find(Auth::user()->user_level);

        $user_level = $user_group->name;
        if($user_level == 'Upazila')
        {
            $application = $q->where('district_id', Auth::user()->district_id)->where('upazila_id', Auth::user()->upazila_id)->select('id')->get();
        }
        else if($user_level == 'Head Office')
        {
            $application = $q->select('id')->get();
        }

        $application = $application->toArray();

        $q = ApplicationData::query();

        if(Input::has('sub_indicator')){
            $q->where('sub_indicator_id', Input::get('sub_indicator'));
        }
        else if(Input::has('indicator')){
            $sub_indicator_list = SubIndicator::where('indicator_id', Input::get('indicator'))->select('id')->get();
            $sub_indicator_list = $sub_indicator_list->toArray();

            $q->whereIn('sub_indicator_id', $sub_indicator_list);
        }
        else if(Input::has('main_area')){
            $indicator_list = Indicator::where('main_area', Input::get('main_area'))->select('id')->get();
            $indicator_list = $indicator_list->toArray();

            $sub_indicator_list = SubIndicator::whereIn('indicator_id', $indicator_list)->select('id')->get();
            $sub_indicator_list = $sub_indicator_list->toArray();

            $q->whereIn('sub_indicator_id', $sub_indicator_list);
        }

        $application_data = $q->selectRaw('*, sum(amount_in_indicator) as amount_in_indicator, sum(woman) as woman, sum(man) as man, sum(total) as total')->groupBy('sub_indicator_id')->get();


        foreach($application_data as $rowdata)
        {
            $woman = $rowdata->woman; 
            $total = $rowdata->total; 
            $percent=round((($woman*100)/$total),2);
            $i=1;

            echo '<tr>
                    <td>'.$i++.'</td>
                    <td>'.$rowdata->getSubIndicatorInfoRow->getIndicatorInfoRow->mainarea->mainarea_en.'</td>
                    <td>'.$rowdata->getSubIndicatorInfoRow->getIndicatorInfoRow->indicator_title.'</td>
                    <td>'.$rowdata->getSubIndicatorInfoRow->sub_indicator_title_en.'</td>
                    <td>'.$rowdata->amount_in_indicator.'</td>
                    <td>'.$rowdata->woman.'</td>
                    <td>'.$rowdata->man.'</td>
                    <td>'.$rowdata->total.'</td>
                    <td>'.$percent.'</td>
                </tr>';
        }
    }
}
