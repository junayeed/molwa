<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\MainArea;
use App\User;

class MainAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_area_list = MainArea::get();

        return view('mainarea.list', compact('main_area_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mainarea.create');
        //return view('mainarea.create', compact('district_list', 'user_group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $main_area_data = $request->all();

        $main_area_data['status'] = isset($main_area_data['status']) ? 1 : 0;

        $main_area = MainArea::create($main_area_data);

        //echo "<pre>"; print_r($response);die;
        $main_area->save();

        return redirect('MainArea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_area = MainArea::find($id);

        if (is_null($main_area)) {
            abort(404, 'Not Found');
        }

        return view('mainarea.edit', compact('main_area'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $main_area_data = $request->all();

        $main_area_data['status'] = isset($main_area_data['status']) ? 1 : 0;

        $response = MainArea::find($id)->update($main_area_data);

        return redirect('MainArea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function deleteMainarea(Request $request)
    {
        $id = $request->input('id');

        MainArea::find($id)->delete();

        echo '1';
        die;
    }
}