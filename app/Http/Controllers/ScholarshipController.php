<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Scholarship;
use App\Models\ApplicantAddress;
use App\Models\ApplicantAcademics;
use App\Models\Upazila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;


define('ACTIVE', 1);

class ScholarshipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $applicant_list                   = DB::table('applicants')->select('applicants.id', 'applicants.applicant_name_en', 'applicants.applicant_name_bn', 'applicants.applicant_dob')
                                           //->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                           ->get();

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        // get data for stats
        $data['total_applicant']     = DB::table('applicants')->count();
        $data['total_gender']        = DB::table('applicants')->select(DB::raw('SUM(applicant_gender=1) as total_male'),
                                                                       DB::raw('SUM(applicant_gender=2) as total_female'))
                                                              ->get();
        //echo "<pre>"; print_r($data); die;
        $data['total_sf_pending']    = DB::table('cases')->where('case_status', 2)->count();  // 2 => Awating for SF
        $data['total_sf_received']   = DB::table('cases')->where('case_status', 3)->count();  // 3 => SF Received
        $data['total_sf_sent']       = DB::table('cases')->where('case_status', 4)->count();  // 4 => SF Sent
        $data['total_closed']        = DB::table('cases')->where('case_status', 5)->count();  // 5 => Closed
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;

        return view('scholarship.list', compact('applicant_list', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $division_list              = DB::table('division_tbl')->pluck('div_bn_name', 'id');
        $district_list              = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $category_list              = DB::table('category_list')->pluck('category_name', 'id');
        $board_list                 = DB::table('education_board')->pluck('board_name', 'id');
        $ff_type_list               = DB::table('ff_list_type')->pluck('name', 'id');
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        //echo "<pre>"; print_r(Auth::user()->id);die;

        return view('scholarship.create', compact('division_list', 'district_list', 'category_list', 'data', 'board_list', 'ff_type_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get and save applicant data
        $applicant_data                      = $request->only(['division', 'district', 'upazilla', 'emergency_phone', 'applicant_dob', 'applicant_name_bn', 'applicant_name_en',
                                                               'applicant_father_name', 'applicant_mother_name', 'applicant_gender', 'current_edu_inst', 'current_session',
                                                               'current_subject', 'current_semester', 'current_id', 'created_by']);
        $applicant_data['applicant_dob']     = date("Y-m-d", strtotime($applicant_data['applicant_dob']));
        $applicant_data['created_by']        = Auth::user()->id; // get the logged in user ID

        $applicant     = Scholarship::create($applicant_data);
        $applicant_id  = $applicant->id;

        // get and save applicant address data
        $app_address                 = $request->only(['present_address', 'present_village', 'present_postoffice', 'present_district',
                                                       'present_upazilla', 'permanent_address', 'permanent_village', 'permanent_postoffice',
                                                       'permanent_district', 'permanent_upazilla']);
        $app_address['applicant_id'] = $applicant_id;
        ApplicantAddress::create($app_address);

        // Get and Save academic data
        //echo "<pre>"; print_r($academic_data);die;

        // save interim order details
        foreach($request->input('academic_data') as $academic)
        {
            $academic_data['exam_name']           = $academic['exam_name'];
            $academic_data['exam_year']           = $academic['exam_year'];
            $academic_data['edu_inst']            = $academic['edu_inst'];
            $academic_data['edu_board']           = $academic['edu_board'];
            $academic_data['edu_group']           = $academic['edu_group'];
            $academic_data['cgpa']                = $academic['cgpa'];
            $academic_data['cgpa_wo_4th_subject'] = $academic['cgpa_wo_4th_subject'];
            $academic_data['applicant_id']        = $applicant_id;

            ApplicantAcademics::create($academic_data);
        }

        return redirect('Scholarship');
    }

    public function getDistrictList(Request $request)
    {
        $division_id = $request->input('division_id');

        $district_list = District::where('dis_division_id', $division_id)->pluck('dis_bn_name', 'id');

        echo json_encode($district_list);
        die;
    }

    public function getUpazillaList(Request $request)
    {
        $district_id = $request->input('district_id');

        $upazilla_list = Upazila::where('upa_district_id', $district_id)->pluck('upa_bn_name', 'id');

        echo json_encode($upazilla_list);
        die;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data              = ForwardDairy::find($id);
        $interim_order_data     = DB::table('interim_orders')->where(['case_id' => $case_data['id']])->get();
        $interim_order_details  = DB::table('interim_orders_details')->where(['case_id' => $case_data['id']])->get();
        $sf_data                = DB::table('sf_details')->where(['case_id' => $case_data['id']])->get();
        $judgement_data         = DB::table('judgement_details')->where(['case_id' => $case_data['id']])->get();
        $list                   = array(1 => 'MOLWA(Budget)', 2 => 'MOLWA(Gadget)', 3 => 'JAMUKA', 4 => 'JAMUKOTRA', 5 => 'SONGSOD', 6 => 'DC(Various)', 7 => 'UNO(Various)');
        $status_list            = DB::table('case_status')->pluck('case_status', 'id');
        $imp_authority_list     = $list;
        $recipient_list         = $list;

        //$main_area_list = MainArea::where('status', ACTIVE)->get();
        //$indicator_list = Indicator::where('indicator_status', ACTIVE)->where('main_area', $sub_indicator->mainarea)->get();
        //echo "<pre>"; print_r($sf_data); die;

        if (is_null($case_data)) {
            abort(404, 'Not Found');
        }

        return view('forwarddairy.edit', compact('case_data', 'interim_order_data', 'sf_data', 'imp_authority_list', 'recipient_list', 'status_list', 'interim_order_details', 'judgement_data'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub_indicator_data = $request->all();

        $sub_indicator_data['sub_indicator_status'] = isset($sub_indicator_data['sub_indicator_status']) ? 1 : 0;

        $response = SubIndicator::find($id)->update($sub_indicator_data);

        return redirect('SubIndicator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function getFFDetails(Request $request)
    {
        $url = "http://ff.molwa.gov.bd/api.php?method=get_data&token=1087167911de6e553733c31b2378617a034df9e8&ff_list_type_id=4&certificate_no=118100";
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        echo json_encode($content); die;

    }
}