<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;

define('ACTIVE', 1);

class GrsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $current_month   = date('m');
        $previous_month  = ($current_month == 1) ? 12 : $current_month-1;
        $current_year    = date('Y');
        $previous_year   = ($current_month == 1) ? $current_year - 1 : $current_year;

        $grs_data_list = DB::table('grs_data')
                                ->select('*', 'sections.section_name_bn')
                                ->join('sections', 'sections.id', '=', 'grs_data.section_id')
                                ->where(['grs_data.month' => $current_month])
                                ->get();

        $non_disposal_list = $this->getPreviousMonthNonDisposalList($previous_month);
        //echo "<pre>"; print_r($non_disposal);die;

        $section_list = DB::table('sections')->select('id', 'section_name_bn')->get();
        $month_list   = array('1' => 'জানুয়ারী', '2' => 'ফেব্রুয়ারী', '3' => 'মার্চ', '4' => 'এপ্রিল', '5' => 'মে', '6' => 'জুন',
                              '7' => 'জুলাই', '8' => 'অগাষ্ট', '9' => 'সেপ্টেম্বর', '10' => 'অক্টোবর', '11' => 'নভেম্বর', '12' => 'ডিসেম্বর',);

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;

        return view('grs.summary', compact('grs_data_list', 'data', 'section_list', 'month_list', 'current_month', 'non_disposal_list'));
    }

    public function getPreviousMonthNonDisposalList($previous_month){
        $previous_month_non_disposal = DB::table('grs_data')->select('total_non_disposal', 'section_id')->where(['month' => $previous_month])->get()->toArray();

        if ($previous_month_non_disposal){
            foreach($previous_month_non_disposal as $item){
                $non_disposal_list[$item->section_id] = $item->total_non_disposal;
            }

            return $non_disposal_list;
        }

        return array();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_area_list = MainArea::where('status', ACTIVE)->get();
        $indicator_list = Indicator::where('indicator_status', ACTIVE)->get();

        return view('subindicator.create', compact('indicator_list', 'main_area_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sub_indicator_data = $request->all();

        $sub_indicator_data['sub_indicator_status'] = isset($sub_indicator_data['sub_indicator_status']) ? 1 : 0;

        $sub_indicator = SubIndicator::create($sub_indicator_data);

        $sub_indicator->save();

        return redirect('SubIndicator');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_indicator      = SubIndicator::find($id);
        /*$indicator      = DB::table('indicators')
                              ->select('*', 'indicators.id AS id', 'mainareas.id AS mainarea_id')
                              ->join('mainareas', 'indicators.main_area', '=', 'mainareas.id')
                              ->where(['indicators.id' => $id])
                              ->get();*/

        $main_area_list = MainArea::where('status', ACTIVE)->get();
        $indicator_list = Indicator::where('indicator_status', ACTIVE)->where('main_area', $sub_indicator->mainarea)->get();

        //echo "<pre>"; print_r($sub_indicator); die;

        if (is_null($sub_indicator)) {
            abort(404, 'Not Found');
        }

        return view('subindicator.edit', compact('sub_indicator', 'indicator_list', 'main_area_list'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub_indicator_data = $request->all();

        $sub_indicator_data['sub_indicator_status'] = isset($sub_indicator_data['sub_indicator_status']) ? 1 : 0;

        $response = SubIndicator::find($id)->update($sub_indicator_data);

        return redirect('SubIndicator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function getIndicatorDetails(Request $request)
    {
        $id = $request->input('id');

        $indicator_details = Indicator::where('id', $id)->get();

        echo json_encode($indicator_details);
        die;

    }

    public function deleteSubIndicator(Request $request)
    {
        $id = $request->input('id');

        SubIndicator::find($id)->delete();

        echo '1';
        die;
    }
}