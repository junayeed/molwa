<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\FFData;
use mPDF;

define('ACTIVE', 1);

class FFDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $login_id = Auth::user()->id;
        $ff_list                   = DB::table('ff_data AS FF')->select('FF.id', 'ff_id', 'ff_no', 'FF.ff_list_type_id', 'ff_mother_name', 'FFL.ff_name', 'FFL.ff_fathers_name',
                                                                  'ff_dob', 'ff_image_path','ff_status', 'doc_type', 'FLT.name AS list_type_name',
                                                                  'ff_doc_no', 'created_by', 'created_at')->where('created_by', '=', $login_id)
                                           ->leftJoin('ff_list_type AS FLT', 'FLT.id', '=', 'FF.ff_list_type_id')
                                           ->leftJoin('ff_list AS FFL', 'FFL.id', '=', 'FF.ff_id')
            /*->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
            ->orderBy(DB::raw("cast(replace(cases.file_no, '.', '') as unsigned)"))*/
                                           ->get();

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        // get data for stats
        /*$data['total_case']          = DB::table('cases')->count();
        $data['total_int_order']     = DB::table('cases')->where('has_interim_order', 1)->count();
        $data['total_closed']        = DB::table('cases')->where('case_status', 5)->count();  // 5 => Closed*/
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;

        return view('ffdata.list', compact('ff_list', 'data'));
    }

    public function search()
    {
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $imp_authority_list          = $list; //MainArea::where('status', ACTIVE)->get();
        $recipient_list              = $list; //Indicator::where('indicator_status', ACTIVE)->get();
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $upzilla_list                = $this->getUpzillaList();
        

        return view('forwarddairy.search', compact('data', 'status_list', 'case_type_list', 'imp_authority_list', 'recipient_list', 'ministry_list', 'district_list', 'upzilla_list', 'case_subject_type_list'));
    }

    public function getSearchResult(Request $request){
        $searchData = $request->only(['file_no', 'case_no', 'case_receive_date', 'parties_name', 'case_status', 'case_type', 'subject_type',
                                      'has_interim_order', 'has_rule_nisi', 'sf_recipient', 'sf_ministry', 'sf_dc_office', 'sf_uno_office']);

        $query = ForwardDairy::select('cases.id', 'cases.file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.case_type', 
                                      'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                      'cases.has_interim_order', 'recipients.recipient_name', 'sf_details.sf_recipient', 'case_type.short_name AS case_type',
                                      'sf_details.sf_ministry', 'sf_details.sf_dc_office', 'sf_details.sf_uno_office', 'subject_types.subject_type_bn')
                                      ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                      ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                      ->leftJoin('sf_details', 'cases.id', '=', 'sf_details.case_id')
                                      ->leftJoin('recipients', 'recipients.id', '=', 'sf_details.sf_recipient')
                                      ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type');

        if ( isset($searchData['file_no']) && !empty($searchData['file_no']) )                       $query->where('cases.file_no', 'LIKE', '%' . $searchData['file_no'] . '%');
        if ( isset($searchData['case_type']) && !empty($searchData['case_type']) )                   $query->where('cases.case_type', '=', $searchData['case_type']);
        if ( isset($searchData['case_no']) && !empty($searchData['case_no']) )                       $query->where('cases.case_no', 'LIKE', '%' . $searchData['case_no'] . '%');
        if ( isset($searchData['parties_name']) && !empty($searchData['parties_name']) )             $query->where('cases.parties_name', 'LIKE', '%' . $searchData['parties_name'] . '%');
        if ( isset($searchData['case_status']) && !empty($searchData['case_status']) )               $query->where('cases.case_status', '=', $searchData['case_status']);
        if ( isset($searchData['case_receive_date']) && !empty($searchData['case_receive_date']) )   $query->where('cases.case_receive_date', '=', date("Y-m-d", strtotime($searchData['case_receive_date'])));
        if ( $searchData['has_interim_order'] == 'on')                                               $query->where('cases.has_interim_order', '=', '1');
        if ( $searchData['has_rule_nisi'] == 'on')                                                   $query->where('cases.has_rule_nisi', '=', '1');
        if ( isset($searchData['sf_recipient']) && !empty($searchData['sf_recipient']))              $query->where('sf_details.sf_recipient', '=', $searchData['sf_recipient']);
        if ( isset($searchData['sf_ministry']) && !empty($searchData['sf_ministry']))                $query->where('sf_details.sf_ministry', '=', $searchData['sf_ministry']);
        if ( isset($searchData['sf_dc_office']) && !empty($searchData['sf_dc_office']))              $query->where('sf_details.sf_dc_office', '=', $searchData['sf_dc_office']);
        if ( isset($searchData['sf_uno_office']) && !empty($searchData['sf_uno_office']))            $query->where('sf_details.sf_uno_office', '=', $searchData['sf_uno_office']);
        if ( isset($searchData['subject_type']) && !empty($searchData['subject_type']))              $query->where('subject_types.id', '=', $searchData['subject_type']);

        $case_list                   = $query->get();
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $imp_authority_list          = $list;
        $recipient_list              = $list;
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $upzilla_list                = $this->getUpzillaList();
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;


        return view('forwarddairy.search', compact('case_list', 'status_list', 'imp_authority_list', 'recipient_list', 'data', 'searchData', 'ministry_list', 
                                                   'district_list', 'upzilla_list', 'case_type_list', 'case_subject_type_list'));
    }

    public function getUpzillaList(){
        $upazilla_list               = DB::table('upazila_tbl')->select('upazila_tbl.id', DB::raw("CONCAT(upa_bn_name, ', ' ,dis_bn_name) as upazilla_name"))
                                           ->leftJoin('district_tbl', 'district_tbl.id', '=', 'upazila_tbl.upa_district_id')
                                           ->get()->pluck('upazilla_name', 'id');

        return $upazilla_list;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_names')->pluck('district_name', 'id');
        $ff_type_list                = DB::table('ff_list_type')->pluck('name', 'id');
        $blood_group_list            = array('A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-');
        $upzilla_list                = DB::table('upazilla_names')->pluck('upazilla_name', 'id');
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        $data['upazilla_id']         = Auth::user()->upazila_id;
        $data['district_id']         = Auth::user()->district_id;

        //echo "<pre>"; print_r($blood_group_list); die;


        return view('ffdata.create', compact('ff_type_list', 'blood_group_list', 'status_list', 'ministry_list', 'district_list', 'upzilla_list', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ff_data                 = $request->only(['ff_id', 'ff_no', 'ff_list_type_id', 'ff_mother_name', 'ff_dob', 'ff_image_path', 'ff_status', 'doc_type', 'ff_doc_no', 'created_by', 'blood_group']);
        $ff_data['ff_dob']       = date("Y-m-d", strtotime(str_replace('/', '-', $ff_data['ff_dob'])));
        $ff_data['created_by']   = Auth::user()->id;
        $ff_data['ff_no']        = self::bn2en($ff_data['ff_no']);
        $ff_data['ff_doc_no']    = self::bn2en($ff_data['ff_doc_no']);
        $ff_data['ff_status']    = $ff_data['ff_status'] == 'on' ? 1 : 0;

        if ($ff_data['doc_type'] == 'n/a'){
            $ff_data['ff_doc_no'] = '';
        }

        if ($request->hasFile('ff_image')) {
            $first_digit = substr($ff_data['ff_id'], 0, 1);
            if ($request->file('ff_image')->isValid()) {
                $file        = $request->file('ff_image');
                $image_name  = $ff_data['ff_id'] . '.' . $file->getClientOriginalExtension();
                $image_path  = "/ff_images/". $first_digit . "/";
                $path        = public_path($image_path);
                $request->file('ff_image')->move($path, $image_name);

                $ff_data['ff_image_path'] = $image_path . $image_name;
            }
        }

        //echo "<pre>";print_r($ff_data); die;

        // Save FF details
        $ff     = FFData::create($ff_data);

        return redirect('ffdata');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //echo "herer";
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id  Case ID
     * @return \Illuminate\Http\Response
     */
    public function edit($ffID)
    {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $ff_data                    = FFData::find($ffID);
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_names')->pluck('district_name', 'id');
        $ff_type_list                = DB::table('ff_list_type')->pluck('name', 'id');
        $blood_group_list            = array('A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-');
        $upzilla_list                = DB::table('upazilla_names')->pluck('upazilla_name', 'id');
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        $data['upazilla_id']         = Auth::user()->upazila_id;
        $data['district_id']         = Auth::user()->district_id;

        /*echo "<pre>";
        print_r($case_data['file_no']);
        print_r(explode(".", $case_data['hard_file_no']));
        die();
        dd($case_data);*/

        $ff_details       = DB::table('ff_list AS FF')->select('FF.id', 'allotted_number', 'ff_name', 'ff_fathers_name', 'image_path',
                                                               'village', 'postoffice', 'FF.district_id', 'certificate_no', 'ff_list_type_id',
                                                               'FF.upazilla_id', 'gadget_page_no', 'gadget_date', 'is_published', 'is_valid',
                                                               'DIS.district_name', 'UP.upazilla_name', 'FLT.name AS ff_list_type')
                                                      ->leftJoin('district_names AS DIS', 'DIS.id', '=', 'FF.district_id')
                                                      ->leftJoin('upazilla_names AS UP', 'UP.id', '=', 'FF.upazilla_id')
                                                      ->leftJoin('ff_list_type AS FLT', 'FLT.id', '=', 'FF.ff_list_type_id')
                                                      ->where('FF.id', '=', $ff_data->ff_id)
                                                      ->get()->first();

        //echo "<pre>"; print_r($ff_details);die();
        if (is_null($ff_data)) {
            abort(404, 'Not Found');
        }

        return view('ffdata.edit', compact('ff_data',   'blood_group_list', 'ff_details', 'ff_type_list', 'ministry_list', 'district_list', 'upzilla_list', 'data'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ffID)
    {
        $ffD                     = $ffID;
        $ff_data                 = $request->only(['ff_id', 'ff_no', 'ff_list_type_id', 'ff_mother_name', 'ff_dob', 'ff_image_path', 'ff_status', 'doc_type', 'ff_doc_no', 'created_by', 'blood_group']);
        $ff_data['ff_dob']       = date("Y-m-d", strtotime(str_replace('/', '-', $ff_data['ff_dob'])));
        $ff_data['created_by']   = Auth::user()->id;
        $ff_data['ff_no']        = self::bn2en($ff_data['ff_no']);
        $ff_data['ff_doc_no']    = self::bn2en($ff_data['ff_doc_no']);
        $ff_data['ff_status']    = $ff_data['ff_status'] == 'on' ? 1 : 0;

        if ($ff_data['doc_type'] == 'n/a'){
            $ff_data['ff_doc_no'] = '';
        }

        if ($request->hasFile('ff_image')) {
            $first_digit = substr($ff_data['ff_id'], 0, 1);
            if ($request->file('ff_image')->isValid()) {
                $file        = $request->file('ff_image');
                $image_name  = $ff_data['ff_id'] . '.' . $file->getClientOriginalExtension();
                $image_path  = "/ff_images/". $first_digit . "/";
                $path        = public_path($image_path);
                $request->file('ff_image')->move($path, $image_name);

                $ff_data['ff_image_path'] = $image_path . $image_name;
            }
        }

        $ffOldData = FFData::find($ffID)->toArray();

        // get different / changes value by comparing with posted value and old database value.
        $diffValueArr = array_diff_assoc($ff_data, $ffOldData);

        $updateTime = date('Y-m-d H:i');

        foreach($diffValueArr as $diffKey => $diffVal){
            $newValue = $diffVal;
            if(isset($ffOldData[$diffKey]))
                $oldValue = $ffOldData[$diffKey];
            else $oldValue = '';

            $updateHistory['ffID']        = $ffID;
            $updateHistory['fieldUpdated']  = $diffKey;
            $updateHistory['previousValue'] = $oldValue;
            $updateHistory['newValue']      = $newValue;
            $updateHistory['updatedBy']     = Auth::user()->id;
            $updateHistory['updatedTime']   = $updateTime;
            //CaseHistory::create($updateHistory);
            //echo "<pre>";print_r($updateHistory);

            // unset $updateHistory and re-initiate for use again.
            unset($updateHistory);
            $updateHistory = array();
        }
        //die();

        // Update FF details
        $ff    = FFData::find($ffID)->update($ff_data);

        return redirect('ffdata');
    }

    public function getFFDetailsData(Request $request)
    {
        DB::enableQueryLog();
        $ff_no        = self::bn2en($request->input('ff_no'));
        $ff_type_list = $request->input('ff_type_list');
        $district_id  = $request->input('district_id');
        $upazilla_id  = $request->input('upazilla_id');
        $query        = DB::table('ff_list AS FF')->select('FF.id', 'allotted_number', 'ff_name', 'ff_fathers_name', 'image_path', 'ff.postoffice', 'FF.village',
                                                     'village', 'postoffice', 'FF.district_id', 'certificate_no', 'ff_list_type_id',
                                                     'FF.upazilla_id', 'gadget_page_no', 'gadget_date', 'is_published', 'is_valid',
                                                     'DIS.district_name', 'UP.upazilla_name');

        $query->leftJoin('district_names AS DIS', 'DIS.id', '=', 'FF.district_id');
        $query->leftJoin('upazilla_names AS UP', 'UP.id', '=', 'FF.upazilla_id');

        if ($ff_type_list == 1 || $ff_type_list == 25 || $ff_type_list == 26  || $ff_type_list == 27) { // indian list && lalmuktibarta
            $query->where('allotted_number', '=', $ff_no)->where('ff_list_type_id', '=', $ff_type_list);
        }
        else if ($ff_type_list == 4 || $ff_type_list == 2) {
            $query->where('certificate_no', '=', $ff_no)->where('ff_list_type_id', '=', $ff_type_list);
        }
        else { // otherwise Gazette
            $query->where('allotted_number', '=', $ff_no)->where('ff_list_type_id', '=', $ff_type_list);//->where('upazilla_id', '=', $upazilla_id);
        }

        $ff_data =  $query->where('FF.district_id', '=', $district_id)->where('FF.upazilla_id', '=', $upazilla_id)->where('is_published', '=', 1)->get();

/*
        $lastQuery = DB::getQueryLog();
        echo "<pre>"; print_r($lastQuery); die;*/

        /*$url = "http://ff.molwa.gov.bd/api/ff_list_api/?method=post&token=1087167911de6e553733c31b2378617a034df9e8&ff_list_type_id=4&certificate_no=118100";
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);*/

        if ( !empty($ff_data[0]) ) {
            echo json_encode($ff_data[0]);
            die;
        }
        else {
            echo '-1';
            die;
        }
    }

    public function en2bn($number) {

        $n = str_replace(self::$en, self::$bn, $number);
        return $n;
    }

    public function bn2en($number) {

        $n = str_replace(self::$bn, self::$en, $number);
        return $n;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function checkDuplicateCaseNo(Request $request)
    {
        $case_no = $request->input('case_no');

        DB::enableQueryLog();

        

        //$case_list = ForwardDairy::select('case_no')->where( DB::raw('case_no'), '=', "'" . $case_no . "'")->get();
        $case_details = DB::raw("SELECT case_no FROM cases where case_no = '". $case_no ."'");

        $last = DB::getQueryLog();

        //print_r(DB::getQueryLog());
        
        print_r($case_details);
    

        if ( $case_details )
        {
            echo 'Duplicate';
        }
        else 
        {
            echo 'Unique';
        }
        
    }

    public function exportForwardDairyToPDF()
    {
        $case_list                   = DB::table('cases')->select('cases.id', 'cases.file_no', 'cases.case_no', 'cases.case_receive_date',
                                                                  'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                                  'cases.has_interim_order', 'case_type.short_name AS case_type')
                                           ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                           ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                           ->get();

        $pdf = mPDF::loadView('forwarddairy.caselistpdf', compact('case_list'), [], [
            'format' => 'A4-L'
        ]);

        return $pdf->download('caselistpdf.pdf');


    }


    public function generateFFListCSV(Request $request) {
        $ff_list = $request->input('ff_list_type');

        //DB::enableQueryLog();

        $ff_list_type = DB::connection('mysqlFF')->table('ff_list AS FF')
                                                 ->select('ff_name', 'ff_fathers_name', 'village', 'postoffice', 'UP.upazilla_name', 'D.district_name')
                                                 ->leftJoin('district_names AS D', 'FF.district_id', '=', 'D.id')
                                                 ->leftJoin('upazilla_names AS UP', 'FF.upazilla_id', '=', 'UP.id')
                                                 ->leftJoin('ff_list_type AS FLT', 'FLT.id', '=', 'ff_list_type_id')
                                                 ->whereIn('ff_list_type_id', $ff_list)
                                                 ->where('is_valid', 0)
                                                 ->where('FF.district_id', 2)
                                                 ->get();

        //print_r(DB::getQueryLog());die;

    }



    public function generateFFList() {
        $ff_list_type = DB::connection('mysqlFF')->table('ff_list_type')->select('id', 'name')->get();

        //echo "<pre>"; print_r($ff_list_type); die;



        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;

        return view('forwarddairy.ff_list', compact('ff_list_type', 'data'));
    }
}