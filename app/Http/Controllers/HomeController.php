<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Application;
use App\Models\Notification;
use App\Models\ForwardDairy;
use App\Modules\Local\HatBazar\Models\HatBazarModel;
use App\Modules\Local\Batch\Models\BatchModel;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$applicationCount   = Application::where('status', 1)->get()->count();
        DB::enableQueryLog();
        $districtWiseAppCnt = Application::select(DB::raw('COUNT(district_id) AS district_count'), 'district_tbl.name')
                                         ->leftJoin('district_tbl', 'applications.district_id', '=', 'district_tbl.id')
                                         ->where('status', 1)
                                         ->groupBy('district_id')->get();

        $notifications = Notification::where('from_id', Auth::user()->id)->orWhere('to_id', Auth::user()->id)->get();*/

        //dd(DB::getQueryLog());

        //echo '<pre>';  print_r($districtWiseAppCnt); die;

        //return view('home', compact('applicationCount', 'districtWiseAppCnt', 'notifications'));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://bulksms.teletalk.com.bd/link_sms_send.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "op=BALANCE&user=molwa&pass=XGXNeLryKP");


        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $sms_return = curl_exec($ch);
        curl_close ($ch);

        //echo "<pre>"; print_r($server_output); die;

        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $sms_balance                = $this->getSMSBalance($sms_return);

        DB::enableQueryLog();
        $total_cases      = ForwardDairy::select(DB::raw('count(id) AS total_cases'))->get();
        $total_cheques    = HatBazarModel::select(DB::raw('count(id) AS total_cheques'))->get();
        $top_contributors = ForwardDairy::select(DB::raw('users.id, count(created_by) AS total_entry, date_format(cases.created_at, "%d/%m/%Y") AS entry_date, users.name, users.designation'))
                                        ->leftJoin('users', 'users.id', '=', 'cases.created_by')
                                        ->groupBy('created_by')->orderBy('total_entry', 'DESC')->get();
        $top_cheque_contributors = HatBazarModel::select(DB::raw('users.id, count(enter_by) AS total_entry, date_format(hat_bazar.created_at, "%d/%m/%Y") AS entry_date, users.name, users.designation'))
                                        ->leftJoin('users', 'users.id', '=', 'hat_bazar.enter_by')
                                        ->groupBy('enter_by')->orderBy('total_entry', 'DESC')->get();
        $upcoming_trainings      = BatchModel::select('id', 'training_title', 'batch_start_date', 'batch_end_date')
                                             ->where('batch_status', '=', 'Upcoming')->limit(3)->get();

        //echo "<pre>"; print_r($upcoming_trainings); die;
        return view('home', ['data' => $data, 'sms_balance' => $sms_balance, 'top_contributors' => $top_contributors,
                                  'total_cases' => $total_cases[0]->total_cases, 'top_cheque_contributors' => $top_cheque_contributors,
                                  'total_cheques' => $total_cheques[0]->total_cheques, 'upcoming_trainings' => $upcoming_trainings]);
    }

    public function getContributorDetails(Request $request) {
        $user_id           = $request->input('user_id');
        $contributor_type  = $request->input('type');

        if ( $contributor_type == 'case') {
            $list = ForwardDairy::select(DB::raw('users.id, count(created_by) AS total_entry, date_format(cases.created_at, "%d/%m/%Y") AS entry_date, date_format(cases.created_at, "%M, %Y") as month_year, users.name, users.designation'))
                ->leftJoin('users', 'users.id', '=', 'cases.created_by')
                ->where('users.id', '=', $user_id)
                ->groupBy('entry_date')
                ->orderBy('cases.created_at', 'DESC')->get();
        }
        else if ($contributor_type == 'hatbazar') {
            $list = HatBazarModel::select(DB::raw('users.id, count(enter_by) AS total_entry, date_format(hat_bazar.created_at, "%d/%m/%Y") AS entry_date, date_format(hat_bazar.created_at, "%M, %Y") as month_year, users.name'))
                ->leftJoin('users', 'users.id', '=', 'hat_bazar.enter_by')
                ->where('users.id', '=', $user_id)
                ->groupBy('entry_date')
                ->orderBy('hat_bazar.created_at', 'DESC')->get();
        }
        if ( $list ) {
            foreach($list as $l) {
                $contributor_details[$l->name][$l->month_year][$l->entry_date] = $l->total_entry;
            }
        }

        //echo "<pre>"; print_r($contributor_details);
        return view('contributer_details', ['contributor_details' => $contributor_details]);
    }

    public function getSMSBalance($text) {
        preg_match_all('/|<reply>AVAILABLE CREDIT: (.*), (.*)<\/reply>|Uis/', $text, $output_array);

        return @$output_array[1][1]; // return total SMS balance
    }

    public function logoutuser(){
        Auth::logout();
        return redirect('/login');
    }
}
