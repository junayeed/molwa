<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Indicator;
use App\Models\SubIndicator;
use App\Models\MainArea;
use App\User;

define('ACTIVE', 1);

class SubIndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sub_indicator_list = DB::table('sub_indicators')
                                ->select('*', 'sub_indicators.id AS id')
                                ->join('indicators', 'indicators.id', '=', 'sub_indicators.indicator_id')
                                //->where(['indicators.id' => $id])
                                ->get();

        return view('subindicator.list', compact('sub_indicator_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_area_list = MainArea::where('status', ACTIVE)->get();
        $indicator_list = Indicator::where('indicator_status', ACTIVE)->get();

        return view('subindicator.create', compact('indicator_list', 'main_area_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sub_indicator_data = $request->all();

        $sub_indicator_data['sub_indicator_status'] = isset($sub_indicator_data['sub_indicator_status']) ? 1 : 0;

        $sub_indicator = SubIndicator::create($sub_indicator_data);

        $sub_indicator->save();

        return redirect('SubIndicator');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_indicator      = SubIndicator::find($id);
        /*$indicator      = DB::table('indicators')
                              ->select('*', 'indicators.id AS id', 'mainareas.id AS mainarea_id')
                              ->join('mainareas', 'indicators.main_area', '=', 'mainareas.id')
                              ->where(['indicators.id' => $id])
                              ->get();*/

        $main_area_list = MainArea::where('status', ACTIVE)->get();
        $indicator_list = Indicator::where('indicator_status', ACTIVE)->where('main_area', $sub_indicator->mainarea)->get();

        //echo "<pre>"; print_r($sub_indicator); die;

        if (is_null($sub_indicator)) {
            abort(404, 'Not Found');
        }

        return view('subindicator.edit', compact('sub_indicator', 'indicator_list', 'main_area_list'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub_indicator_data = $request->all();

        $sub_indicator_data['sub_indicator_status'] = isset($sub_indicator_data['sub_indicator_status']) ? 1 : 0;

        $response = SubIndicator::find($id)->update($sub_indicator_data);

        return redirect('SubIndicator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function getIndicatorDetails(Request $request)
    {
        $id = $request->input('id');

        $indicator_details = Indicator::where('id', $id)->get();

        echo json_encode($indicator_details);
        die;

    }

    public function deleteSubIndicator(Request $request)
    {
        $id = $request->input('id');

        SubIndicator::find($id)->delete();

        echo '1';
        die;
    }
}