<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Indicator;
use App\Models\ForwardDairy;
use App\Models\RuleNisi;
use App\Models\CourtContempt;
use App\Models\RecipientMinistry;
use App\Models\InterimOrder;
use App\Models\InterimOrderDetails;
use App\Models\StatementFacts;
use App\Models\JudgementDetails;
use App\Models\HearingDetails;
use App\Models\CaseHistory;
use App\Models\AOR;
use App\User;
use Illuminate\Support\Facades\Auth;
use mPDF;
use Illuminate\Database\QueryException;
use App\Models\SupplementaryOrder;

define('ACTIVE', 1);

class ForwardDairyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        // Get WRIT PETITION List
        $writ_petitions         = DB::table('cases')->select('cases.id', 'cases.file_no', 'cases.hard_file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.subject_type',
                                                                 'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                                 'cases.has_interim_order', 'case_type.short_name AS case_type', 'subject_types.subject_type_bn', 'cases.case_year')
                                                       ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                                       ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                                       ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
                                                       ->where('cases.case_type', 1)  // case_type == 1 is WRIT PETITION
                                                       ->where('cases.has_court_contempted', 0)
                                                       ->orderBy('cases.case_year', 'DESC')
                                                       ->orderBy(DB::raw("cast(replace(cases.file_no, '.', '') as unsigned)"))
                                                       ->get();
        if ( $writ_petitions) {
            foreach($writ_petitions as $wp) {
                $writ_petition_list[$wp->case_year][] = $wp;
            }
        }

        //echo "<pre>"; print_r($writ_petition_list); die;
        // Get Court Contempted List
        $court_contempted         = DB::table('cases')->select('cases.id', 'cases.file_no', 'cases.hard_file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.subject_type',
                                                                    'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                                    'cases.has_interim_order', 'case_type.short_name AS case_type', 'subject_types.subject_type_bn',
                                                                    'court_contempted.contempt_no' ,'court_contempted.contempt_received_date', 'court_contempted.appearance_date',
                                                                    'cases.case_year')
                                                            ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                                            ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                                            ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
                                                            ->leftJoin('court_contempted', 'cases.id', '=', 'court_contempted.case_id')
                                                            ->where('cases.has_court_contempted', 1)
                                                            ->orderBy(DB::raw("cast(replace(cases.file_no, '.', '') as unsigned)"))
                                                            ->get();
        if ( $court_contempted ) {
            foreach($court_contempted as $cc) {
                $court_contempted_list[$cc->case_year][] = $cc;
            }
        }
        // Get Civil Suit List
        $civil_suit_list         = DB::table('cases')->select('cases.id', 'cases.file_no', 'cases.hard_file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.subject_type',
                                                              'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                              'cases.has_interim_order', 'case_type.short_name AS case_type', 'subject_types.subject_type_bn', 'cases.case_year')
                                                    ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                                    ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                                    ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
                                                    ->where('cases.case_type', 2)  // case_type == 1 is WRIT PETITION
                                                    ->where('cases.has_court_contempted', 0)
                                                    ->orderBy(DB::raw("cast(replace(cases.file_no, '.', '') as unsigned)"))
                                                    ->get();


        // get data for stats
        $data['total_case']               = DB::table('cases')->count();
        $data['total_court_contempted']   = DB::table('cases')->where('has_court_contempted', 1)->count();
        $data['total_int_order']          = DB::table('cases')->where('has_interim_order', 1)->count();
        $data['total_rule_nisi']          = DB::table('cases')->where('has_rule_nisi', 1)->count();
        $data['total_pending']            = DB::table('cases')->where('case_status', 1)->count();  // 1 => Pending at MOLWA
        $data['total_sf_pending']         = DB::table('cases')->where('case_status', 2)->count();  // 2 => Awating for SF
        $data['total_sf_received']        = DB::table('cases')->where('case_status', 3)->count();  // 3 => SF Received
        $data['total_sf_sent']            = DB::table('cases')->where('case_status', 4)->count();  // 4 => SF Sent
        $data['total_closed']             = DB::table('cases')->where('case_status', 5)->count();  // 5 => Closed
        $data['total_corr_ministry']      = DB::table('cases')->where('case_status', 6)->count();  // 6 => Writ sent to corrosponding MInistry
        $data['nav_item']                 = str_replace("Controller", "", $controller);
        $data['nav_sub_item']             = $action;

        return view('forwarddairy.list', compact('writ_petition_list', 'court_contempted_list', 'civil_suit_list', 'data'));
    }

    public function search()
    {
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $imp_authority_list          = $list;
        $recipient_list              = $list;
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $upzilla_list                = $this->getUpzillaList();
        

        return view('forwarddairy.search', compact('data', 'status_list', 'case_type_list', 'imp_authority_list', 'recipient_list', 'ministry_list', 'district_list', 'upzilla_list', 'case_subject_type_list'));
    }

    public function showCategoryResult(Request $request)
    {
        $case_status                 = $request->input('case_status');
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $imp_authority_list          = $list;
        $recipient_list              = $list;
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $upzilla_list                = $this->getUpzillaList();

        $query                       = DB::table('cases')->select('cases.id as case_id', 'cases.file_no', 'cases.hard_file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.subject_type',
                                                                 'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                                 'cases.has_interim_order', 'case_type.short_name AS case_type', 'subject_types.subject_type_bn', 'sf_details.*',
                                                                 DB::raw('DATEDIFF(CURDATE(), sf_issue_date) AS awaiting_sf_days') )
                                                        ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                                        ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                                        ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
                                                        ->leftJoin('sf_details', 'sf_details.case_id', '=', 'cases.id')
                                                        ->where('cases.case_status', $case_status)
                                                        ->orderBy('sf_details.sf_recipient');

        $cases = $query->get();

        if ( $list) {
            foreach($cases as $case) {
                $case_list[$case->sf_recipient][] = $case;
            }
        }

        //echo "<pre>"; print_r($case_list); die;

        return view('forwarddairy.case_status_list', compact('data', 'case_list', 'status_list', 'case_type_list', 'imp_authority_list',
                                                                          'recipient_list', 'ministry_list', 'district_list', 'upzilla_list',
                                                                          'case_subject_type_list', 'case_status'));
    }

    public function getSearchResult(Request $request){
        $searchData = $request->only(['file_no', 'case_no', 'case_receive_date', 'parties_name', 'case_status', 'case_type', 'subject_type',
                                      'has_interim_order', 'has_rule_nisi', 'sf_recipient', 'sf_ministry', 'sf_dc_office', 'sf_uno_office',
                                      'has_court_contempted', 'contempt_no', 'case_year']);
        
        DB::enableQueryLog();

        $keyword = $searchData['parties_name'];

        $query = ForwardDairy::select('cases.id', 'cases.file_no', 'cases.hard_file_no', 'cases.case_no', 'cases.case_receive_date', 'cases.case_type', 
                                      'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi', 'case_year',
                                      'cases.has_interim_order', 'recipients.recipient_name', 'sf_details.sf_recipient', 'case_type.short_name AS case_type',
                                      'sf_details.sf_ministry', 'sf_details.sf_dc_office', 'sf_details.sf_uno_office', 'subject_types.subject_type_bn',
                                      'parties_name', 'recipient_ministry')
                                      ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                      ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                      ->leftJoin('sf_details', 'cases.id', '=', 'sf_details.case_id')
                                      ->leftJoin('recipient_ministry', 'recipient_ministry.case_id', '=', 'cases.id')
                                      ->leftJoin('recipients', 'recipients.id', '=', 'sf_details.sf_recipient')
                                      ->leftJoin('subject_types', 'subject_types.id', '=', 'cases.subject_type')
                                      ->leftJoin('court_contempted', 'court_contempted.case_id', '=', 'cases.id');

        if ( isset($searchData['file_no']) && !empty($searchData['file_no']) )                       $query->where('cases.file_no', 'LIKE', '%' . $searchData['file_no'] . '%');
        if ( isset($searchData['case_type']) && !empty($searchData['case_type']) )                   $query->where('cases.case_type', '=', $searchData['case_type']);
        if ( isset($searchData['case_no']) && !empty($searchData['case_no']) )                       $query->where('cases.case_no', 'LIKE', '%' . $searchData['case_no'] . '%');
        if ( isset($searchData['case_year']) && !empty($searchData['case_year']) )                   $query->where('cases.case_year', '=', $searchData['case_year']);
        if ( isset($searchData['contempt_no']) && !empty($searchData['contempt_no']) )               $query->where('court_contempted.contempt_no', 'LIKE', '%' . $searchData['contempt_no'] . '%');
        if ( isset($searchData['parties_name']) && !empty($searchData['parties_name']) )             $query->where('cases.parties_name', 'LIKE', '%' . $searchData['parties_name'] . '%');
        if ( isset($searchData['case_status']) && !empty($searchData['case_status']) )               $query->where('cases.case_status', '=', $searchData['case_status']);
        if ( isset($searchData['case_receive_date']) && !empty($searchData['case_receive_date']) )   $query->where('cases.case_receive_date', '=', date("Y-m-d", strtotime($searchData['case_receive_date'])));
        if ( $searchData['has_interim_order'] == 'on')                                               $query->where('cases.has_interim_order', '=', '1');
        if ( $searchData['has_rule_nisi'] == 'on')                                                   $query->where('cases.has_rule_nisi', '=', '1');
        if ( $searchData['has_court_contempted'] == 'on')                                            $query->where('cases.has_court_contempted', '=', '1');
        if ( isset($searchData['sf_recipient']) && !empty($searchData['sf_recipient']))              $query->where('sf_details.sf_recipient', '=', $searchData['sf_recipient']);
        if ( isset($searchData['sf_ministry']) && !empty($searchData['sf_ministry']))                $query->where('sf_details.sf_ministry', '=', $searchData['sf_ministry']);
        if ( isset($searchData['sf_dc_office']) && !empty($searchData['sf_dc_office']))              $query->where('sf_details.sf_dc_office', '=', $searchData['sf_dc_office']);
        if ( isset($searchData['sf_uno_office']) && !empty($searchData['sf_uno_office']))            $query->where('sf_details.sf_uno_office', '=', $searchData['sf_uno_office']);
        if ( isset($searchData['subject_type']) && !empty($searchData['subject_type']))              $query->where('subject_types.id', '=', $searchData['subject_type']);

        $case_list                   = $query->get()->map(function ($row) use ($keyword) {
            $row->parties_name = preg_replace('/(' . $keyword . ')/i', "<b>$1</b>", $row->parties_name);
            return $row;});
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $imp_authority_list          = $list;
        $recipient_list              = $list;
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $upzilla_list                = $this->getUpzillaList();
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;

        $lastQuery = DB::getQueryLog();
        //echo "<pre>"; print_r($lastQuery); die;

        return view('forwarddairy.search', compact('case_list', 'status_list', 'imp_authority_list', 'recipient_list', 'data', 'searchData', 'ministry_list', 
                                                   'district_list', 'upzilla_list', 'case_type_list', 'case_subject_type_list'));
    }

    public function getUpzillaList(){
        $upazilla_list               = DB::table('upazila_tbl')->select('upazila_tbl.id', DB::raw("CONCAT(upa_bn_name, ', ' ,dis_bn_name) as upazilla_name"))
                                           ->leftJoin('district_tbl', 'district_tbl.id', '=', 'upazila_tbl.upa_district_id')
                                           ->get()->pluck('upazilla_name', 'id');

        return $upazilla_list;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action                      = app('request')->route()->getAction();
        $controller                  = class_basename($action['controller']);
        list($controller, $action)   = explode('@', $controller);
        $list                        = DB::table('recipients')->pluck('recipient_name', 'id');
        $status_list                 = DB::table('case_status')->pluck('case_status', 'id');
        $case_type_list              = DB::table('case_type')->pluck('case_type', 'id');
        $ministry_list               = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list               = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $case_subject_type_list      = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $upzilla_list                = $this->getUpzillaList();//DB::table('upazila_tbl')->pluck('upa_bn_name', 'id');
        $imp_authority_list          = $list;
        $recipient_list              = $list;
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        //echo "<pre>"; print_r($list); die;


        return view('forwarddairy.create', compact('recipient_list', 'imp_authority_list', 'status_list', 'case_subject_type_list',
                                                                 'case_type_list', 'ministry_list', 'district_list', 'upzilla_list', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $case_data                          = $request->only(['file_no', 'case_no', 'case_type', 'case_receive_date', 'parties_name',
                                                              'subject_type', 'has_judgement', 'case_subject', 'case_status', 'has_interim_order',
                                                              'has_rule_nisi', 'created_by', 'hard_file_no', 'page_no', 'appendix_list', 'has_court_contempted',
                                                              'has_supplementary_order', 'writ_petition_order_date', 'case_year']);
        $case_data['writ_petition_order_date']     = date('Y-m-d', strtotime(str_replace('/', '-', $case_data['writ_petition_order_date']) ));
        $case_data['case_receive_date']            = date('Y-m-d', strtotime(str_replace('/', '-', $case_data['case_receive_date']) ));
        $case_data['created_by']                   = Auth::user()->id;
        $case_data['has_interim_order']            = isset($case_data['has_interim_order'])        ? 1 : 0;
        $case_data['has_rule_nisi']                = isset($case_data['has_rule_nisi'])            ? 1 : 0;
        $case_data['has_judgement']                = ($case_data['has_judgement'] == 1)            ? 1 : 0;
        $case_data['has_court_contempted']         = isset($case_data['has_court_contempted'])     ? 1 : 0;
        $case_data['has_supplementary_order']      = isset($case_data['has_supplementary_order'])  ? 1 : 0;
        $case_data['subject_type']                 = implode(',', $case_data['subject_type']);
        $case_data['file_no']                      = isset($case_data['file_no'])      ? '48.00.0000.007.' . $case_data['file_no']      : '';
        $case_data['hard_file_no']                 = isset($case_data['hard_file_no']) ? '48.00.0000.004.' . $case_data['hard_file_no'] : '';
        $case_data['case_year']                    = explode("/", $case_data['case_no'])[1];
        //echo "<pre>"; print_r($case_year); die;
        //dd($case_data);

        // Save Case Details
        $case     = ForwardDairy::create($case_data);
        $case_id  = $case->id;

        /*** Save Recipient Ministry ***/
        if ($case_data['case_status'] == 6) { //if case_status is Writ Send To Corresponding Ministry/Divisionthen save the Ministry Recipient into DB
            $ministry_recipient_data                = $request->only(['recipient_ministry', 'memo_no', 'issue_date', 'recipient_ministry_remarks', 'recipient_others']);
            $ministry_recipient_data['case_id']     = $case_id;
            $ministry_recipient_data['issue_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $ministry_recipient_data['issue_date']) ));
            $ministry_recipient_data['remarks']     = $ministry_recipient_data['recipient_ministry_remarks'];

            RecipientMinistry::create($ministry_recipient_data);
        }

        /***  Save Court Contempted Data   ***/
        $court_contempt_data = $request->only(['contempt_no', 'contempt_received_date', 'appearance_date', 'contempt_remarks']);

        if ($court_contempt_data) {
            $court_contempt_data['case_id']                 = $case_id;
            $court_contempt_data['contempt_received_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $court_contempt_data['contempt_received_date'])));
            $court_contempt_data['appearance_date']         = date('Y-m-d', strtotime(str_replace('/', '-', $court_contempt_data['appearance_date'])));

            $court_contempt = CourtContempt::create($court_contempt_data);
        }
        /***  Save Court Contempted Data   ***/

        /***  Save Save Rule Nisi Data   ***/
        $rule_nisi_data                             = $request->only(['rule_nisi_order_date', 'rule_nisi_order_details']);
        $rule_nisi_data['case_id']                  = $case_id;
        $rule_nisi_data['rule_nisi_order_details']  = isset($rule_nisi_data['rule_nisi_order_details']) ? $rule_nisi_data['rule_nisi_order_details'] : '';
        $rule_nisi_data['rule_nisi_order_date']     = isset($rule_nisi_data['rule_nisi_order_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $rule_nisi_data['rule_nisi_order_date']) )) : '0000-00-00';

        $rule_nisi = RuleNisi::create($rule_nisi_data);

        $rule_nisi_id = $rule_nisi->id;
        /***  Save Save Rule Nisi Data   ***/

        /*** Save interim order data   ***/
        $int_order_data                       = $request->only(['interim_order_date', 'order_details', 'deadline']);
        $int_order_data['order_details']      = isset($int_order_data['order_details']) ? $int_order_data['order_details'] : '';
        $int_order_data['deadline']           = isset($int_order_data['deadline']) ? $int_order_data['deadline'] : '';
        $int_order_data['case_id']            = $case_id;
        $int_order_data['interim_order_date'] = isset($int_order_data['interim_order_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $int_order_data['interim_order_date']) )) : '0000-00-00';
        //$interim_order_data[]                 = $request->only(['implementing_authority', 'ministry', 'dc_office', 'uno_office', 'responsibility']);

        $interim_order = InterimOrder::create($int_order_data);

        $interim_order_id = $interim_order->id;
        /*** Save interim order data   ***/


        /*** save interim order details  ***/
        if ( $request->has('interim_order_data') ) {
            foreach ($request->input('interim_order_data') as $iod) {
                $interim_order_details['implementing_authority']  = isset($iod['implementing_authority']) ? $iod['implementing_authority'] : 0;
                $interim_order_details['ministry']                = isset($iod['ministry']) ? $iod['ministry']                             : 0;
                $interim_order_details['dc_office']               = isset($iod['dc_office']) ? $iod['dc_office']                           : 0;
                $interim_order_details['uno_office']              = isset($iod['uno_office']) ? $iod['uno_office']                         : 0;
                $interim_order_details['responsibility']          = isset($iod['responsibility']) ? $iod['responsibility']                 : '';
                $interim_order_details['int_issue_memo']          = isset($iod['int_issue_memo']) ? $iod['int_issue_memo']                 : '';
                $interim_order_details['int_receive_memo']        = isset($iod['int_receive_memo']) ? $iod['int_receive_memo']             : '';
                $interim_order_details['int_sending_memo']        = isset($iod['int_sending_memo']) ? $iod['int_sending_memo']             : '';
                $interim_order_details['int_issue_date']          = isset($iod['int_issue_date'])  ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_issue_date'] ) ))     : '';
                $interim_order_details['int_receive_date']        = isset($iod['int_receive_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_receive_date'] ) ))  : '';
                $interim_order_details['int_sending_date']        = isset($iod['int_sending_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_sending_date'] ) ))  : '';
                $interim_order_details['case_id']                 = $case_id;
                $interim_order_details['interim_order_id']        = $interim_order_id;

                InterimOrderDetails::create($interim_order_details);
            }
        }
        /*** save interim order details  ***/

        /**** Save Supllementary Order ***/
        if ($request->has('supplementary_order_data')) {
            foreach ($request->input('supplementary_order_data') as $sod) {
                $supplementary_data['case_id']                      = $case_id;
                $supplementary_data['supplementary_order_date']     = date('Y-m-d', strtotime(str_replace('/', '-', $sod['supplementary_order_date'])));
                $supplementary_data['supplementary_order_details']  = $sod['supplementary_order_details'];

                SupplementaryOrder::create($supplementary_data);
            }
        }
        /**** Save Supllementary Order ***/

        /***   save SF details   ***/
        $sf_details[]                      = $request->only(['sf_recipient', 'sf_ministry', 'sf_dc_office', 'sf_uno_office',
                                                             'sf_issue_memo', 'sf_receive_memo', 'sf_sending_memo', 'sf_issue_date',
                                                             'sf_receive_date', 'sf_sending_date']);
        if ( $request->has('sf_data') ) {
            foreach ( $request->input('sf_data') as $sd) {
                $sf_data['sf_recipient']     = isset($sd['sf_recipient']) ? $sd['sf_recipient']       : 0;
                $sf_data['sf_ministry']      = isset($sd['sf_ministry']) ? $sd['sf_ministry']         : 0;
                $sf_data['sf_dc_office']     = isset($sd['sf_dc_office']) ? $sd['sf_dc_office']       : 0;
                $sf_data['sf_uno_office']    = isset($sd['sf_uno_office']) ? $sd['sf_uno_office']     : 0;
                $sf_data['sf_issue_memo']    = isset($sd['sf_issue_memo']) ? $sd['sf_issue_memo']     : '';
                $sf_data['sf_receive_memo']  = isset($sd['sf_receive_memo']) ? $sd['sf_receive_memo'] : '';
                $sf_data['sf_sending_memo']  = isset($sd['sf_sending_memo']) ? $sd['sf_sending_memo'] : '';
                $sf_data['sf_issue_date']    = isset($sd['sf_issue_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_issue_date']))) : '0000-00-00';
                $sf_data['sf_receive_date']  = isset($sd['sf_receive_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_receive_date']))) : '0000-00-00';
                $sf_data['sf_sending_date']  = isset($sd['sf_sending_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_sending_date']))) : '0000-00-00';
                $sf_data['case_id'] = $case_id;

                StatementFacts::create($sf_data);
            }
        }
        /***   save SF details   ***/

        /***  save AOR data ***/
        $aor_data                   = $request->only(['aor_name', 'aor_cell_no', 'aor_memo_no', 'aor_memo_date']);
        $aor_data['aor_memo_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $aor_data['aor_memo_date']) ));
        $aor_data['case_id']        = $case_id;

        try {
            AOR::create($aor_data);
        }
        catch (QueryException $e) {}
        /***  save AOR data ***/

        /*** save judgement details data   ***/

        if ( $case_data['has_judgement'] == 1 ) {
            $judgement_data = $request->only(['cmp_no', 'cmp_judgement', 'cp_no', 'cp_judgement', 'appeal_case_no',
                                              'appeal_judgement', 'review_petition_no', 'review_order', 'final_judgement']); //dd($judgement_data);
            $judgement_data['case_id'] = $case_id;
            //$judgement_data['hearing_date']         = isset($judgement_data['hearing_date'])       ? date("Y-m-d", strtotime($judgement_data['hearing_date'])) : '0000-00-00';
            //$judgement_data['judgement']            = isset($judgement_data['judgement'])          ? $judgement_data['judgement']          : '';
            $judgement_data['cmp_no']              = isset($judgement_data['cmp_no'])             ? $judgement_data['cmp_no']             : '';
            $judgement_data['cmp_judgement']       = isset($judgement_data['cmp_judgement'])      ? $judgement_data['cmp_judgement']      : '';
            $judgement_data['cp_no']               = isset($judgement_data['cp_no'])              ? $judgement_data['cp_no']              : ''; // cp_no is cpla_no
            $judgement_data['cp_judgement']        = isset($judgement_data['cp_judgement'])       ? $judgement_data['cp_judgement']       : ''; // co_judgement is cpla_judgement
            $judgement_data['appeal_case_no']      = isset($judgement_data['appeal_case_no'])     ? $judgement_data['appeal_case_no']     : '';
            $judgement_data['appeal_judgement']    = isset($judgement_data['appeal_judgement'])   ? $judgement_data['appeal_judgement']   : '';
            $judgement_data['review_petition_no']  = isset($judgement_data['review_petition_no']) ? $judgement_data['review_petition_no'] : '';
            $judgement_data['review_order']        = isset($judgement_data['review_order'])       ? $judgement_data['review_order']       : '';
            $judgement_data['final_judgement']     = isset($judgement_data['final_judgement'])    ? $judgement_data['final_judgement']    : '';

            JudgementDetails::create($judgement_data);
        }
        /*** save judgement details data   ***/

        return redirect('forwardDairy');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //echo "herer";
    }

    /**
     * Split the file number with . and return last 2 segments
     * Example: 48.00.0000.007.799.005.2017 -- returns 799.005.2017
     * Example: 48.00.0000.007.99.124.17 -- returns 99.124.17
     * Example: 48.00.0000.007.7899.012.17 -- returns 7899.012.17
     * Example: 48.00.0000.007.78.032.2017 -- returns 78.032.2017
     *
     * @param $file_no
     * @return mixed
     */
    public function splitFileNumber($file_no) {
        $file_no_array = explode('.', $file_no);
        return $file_no_array[4] . '.' . $file_no_array[5] . '.' . $file_no_array[6];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id  Case ID
     * @return \Illuminate\Http\Response
     */
    public function edit($caseID) {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);
        $data['nav_item']           = str_replace("Controller", "", $controller);
        $data['nav_sub_item']       = $action;
        $sf_data                    = array();
        $case_data                  = ForwardDairy::find($caseID);  //dd($case_data);
        $court_contempted_data      = CourtContempt::where(['case_id' => $case_data['id']])->get();
        $interim_order_data         = InterimOrder::where(['case_id' => $case_data['id']])->get(); //DB::table('interim_orders')->where(['case_id' => $case_data['id']])->get();
        $interim_order_details      = InterimOrderDetails::where(['case_id' => $case_data['id']])->get();
        $sf_data                    = StatementFacts::where(['case_id' => $case_data['id']])->get();
        $judgement_data             = JudgementDetails::where(['case_id' => $case_data['id']])->get();
        $hearing_data               = HearingDetails::where(['case_id' => $case_data['id']])->get();
        $rule_nisi_data             = RuleNisi::where(['case_id' => $case_data['id']])->get();
        $supplementary_order_data   = SupplementaryOrder::where(['case_id' => $case_data['id']])->get();
        $recipient_ministry         = RecipientMinistry::where(['case_id' => $case_data['id']])->get();
        $aor_data                   = AOR::where(['case_id' => $case_data['id']])->get();
        $list                       = DB::table('recipients')->pluck('recipient_name', 'id');
        $status_list                = DB::table('case_status')->pluck('case_status', 'id');
        $ministry_list              = DB::table('ministry_tbl')->where('status', '=', 'Active')->get()->pluck('ministry_name_bn', 'id');
        $district_list              = DB::table('district_tbl')->pluck('dis_bn_name', 'id');
        $case_type_list             = DB::table('case_type')->pluck('case_type', 'id');
        $case_subject_type_list     = DB::table('subject_types')->pluck('subject_type_bn', 'id');
        $upzilla_list               = $this->getUpzillaList();//DB::table('upazila_tbl')->pluck('upa_bn_name', 'id');
        $imp_authority_list         = $list;
        $recipient_list             = $list;

        if ( !empty ($case_data['file_no']) ) {
            $case_data['file_no'] = str_replace('48.00.0000.007.', '', $case_data['file_no']);
        }

        if ( !empty ($case_data['hard_file_no']) ) {
            $case_data['hard_file_no'] = str_replace('48.00.0000.004.', '', $case_data['hard_file_no']);
        }

        $case_history               = CaseHistory::select('fieldUpdated', 'previousValue', 'newValue', 'updatedBy', 'updatedTime', 'users.name AS user_name')
                                                 ->leftjoin('users', 'users.id', '=', 'case_update_history.updatedBy')
                                                 ->where(['caseID' => $caseID])
                                                 ->orderBy('updatedTime', 'DESC')
                                                 ->orderBy('updatedBy')
                                                 ->get()->toArray();
        $created_by                 = ForwardDairy::select('cases.created_at', 'cases.created_by', 'users.name AS user_name', 'users.designation')
                                                  ->leftjoin('users', 'users.id', '=', 'cases.created_by')
                                                  ->where(['cases.id' => $caseID])
                                                  ->get();



        if (is_null($case_data)) {
            abort(404, 'Not Found');
        }

        return view('forwarddairy.edit', compact('case_data', 'case_type_list', 'case_subject_type_list', 'court_contempted_data',
                                                              'status_list', 'recipient_list', 'ministry_list', 'district_list', 'upzilla_list',
                                                              'rule_nisi_data', 'created_by', 'case_history', 'imp_authority_list', 'interim_order_details',
                                                              'interim_order_data', 'sf_data', 'judgement_data', 'hearing_data', 'data', 'recipient_ministry',
                                                              'aor_data', 'supplementary_order_data'));

        /*return view('forwarddairy.edit', compact('case_data', 'interim_order_data', 'sf_data', 'imp_authority_list',
                                                 , 'rule_nisi_data', 'interim_order_details',
                                                 ));*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $caseID                             = $id;
        $case_data                          = $request->only(['file_no', 'case_no', 'case_type', 'case_receive_date', 'parties_name', 'subject_type', 'case_subject',
                                                              'case_status', 'has_rule_nisi', 'has_interim_order', 'hard_file_no', 'page_no', 'appendix_list',
                                                              'has_court_contempted', 'writ_petition_order_date', 'case_year', 'has_judgement', 'has_supplementary_order']); //dd($case_data);
        
        $case_data['writ_petition_order_date']     = date('Y-m-d', strtotime(str_replace('/', '-', $case_data['writ_petition_order_date']) )); 
        $case_data['case_receive_date']            = date('Y-m-d', strtotime(str_replace('/', '-', $case_data['case_receive_date'] ) ));
        $case_data['has_interim_order']            = isset($case_data['has_interim_order'])         ? 1 : 0;
        $case_data['has_rule_nisi']                = isset($case_data['has_rule_nisi'])             ? 1 : 0;
        $case_data['has_court_contempted']         = isset($case_data['has_court_contempted'])      ? 1 : 0;
        $case_data['has_supplementary_order']      = isset($case_data['has_supplementary_order'])   ? 1 : 0;
        $case_data['has_judgement']                = ($case_data['has_judgement'] == 1)             ? 1 : 0;
        $case_data['subject_type']                 = implode(',', $case_data['subject_type']);
        $case_data['file_no']                      = isset($case_data['file_no']) ? '48.00.0000.007.' . $case_data['file_no'] : '';
        $case_data['hard_file_no']                 = isset($case_data['hard_file_no']) ? '48.00.0000.004.' . $case_data['hard_file_no'] : '';
        $case_data['case_year']                    = explode("/", $case_data['case_no'])[1];
        //dd($case_data);

        $caseOldData = ForwardDairy::find($caseID)->toArray();


        // get different / changes value by comparing with posted value and old database value.
        $diffValueArr = array_diff_assoc($case_data,$caseOldData);

        $updateTime = date('Y-m-d H:i');

        foreach($diffValueArr as $diffKey => $diffVal){
            $newValue = $diffVal;
            if(isset($caseOldData[$diffKey]))
                $oldValue = $caseOldData[$diffKey];
            else $oldValue = '';

            $updateHistory['caseID']        = $caseID;
            $updateHistory['fieldUpdated']  = $diffKey;
            $updateHistory['previousValue'] = $oldValue;
            $updateHistory['newValue']      = $newValue;
            $updateHistory['updatedBy']     = Auth::user()->id;
            $updateHistory['updatedTime']   = $updateTime;
            CaseHistory::create($updateHistory);

            // unset $updateHistory and re-initiate for use again.
            unset($updateHistory);
            $updateHistory = array();
        }

        // Update case details
        $case    = ForwardDairy::find($caseID)->update($case_data);

        /*** Save Recipient Ministry ***/
        if ($case_data['case_status'] == 6) {
            $ministry_recipient_data                = $request->only(['recipient_ministry', 'memo_no', 'issue_date', 'recipient_ministry_remarks', 'recipient_others']);

            if ($ministry_recipient_data['recipient_ministry'] != 999) {
                $ministry_recipient_data['recipient_others'] = NULL;
            }

            $ministry_recipient_data['issue_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $ministry_recipient_data['issue_date']) ));
            $ministry_recipient_data['remarks']     = $ministry_recipient_data['recipient_ministry_remarks'];
            //echo "<pre>"; print_r($ministry_recipient_data); die;

            RecipientMinistry::updateOrCreate(['case_id' => $caseID], $ministry_recipient_data);
        }

        // if the has_interim_order is checked then update the interim data
        if ($case_data['has_interim_order']) {
            $this->updateInterimOrder($request, $caseID);
        }
        // if the has_rule_nisi is checked then update the interim data
        if ($case_data['has_rule_nisi']) {
            $this->updateRuleNisi($request, $caseID);
        }

        // if the has_supplementary_order is checked then update the interim data
        if ($case_data['has_supplementary_order']) {
            $this->updateSupplementaryOrderDetails($request, $caseID);
        }

        $this->updateSFDetails($request, $caseID);
        $this->updateCourtContempted($request, $caseID);
        $this->updateAORDetails($request, $caseID);
        $this->updateJudgementDetails($request, $caseID);

        return redirect('forwardDairy');
    }

    private function updateSupplementaryOrderDetails($request, $caseID) {
        SupplementaryOrder::where(['case_id' => $caseID])->delete();

        // save supplementary order details
        if ( $request->has('supplementary_order_data') ) {
            foreach ( $request->input('supplementary_order_data') as $sod) {
                $sup_order_details['supplementary_order_details']  = isset($sod['supplementary_order_details']) ? $sod['supplementary_order_details']                 : '';
                $sup_order_details['supplementary_order_date']     = isset($sod['supplementary_order_date'])  ? date('Y-m-d', strtotime(str_replace('/', '-', $sod['supplementary_order_date'] ) ))     : '';
                $sup_order_details['case_id']                      = $caseID;

                SupplementaryOrder::create($sup_order_details);

                //$this->logCaseHistory($caseID, $interim_order_details, $interim_order->toArray());
            }
        }
    }

    private function updateAORDetails($request, $caseID) {
        $aor_id = $request->input('aor_id');

        if ($aor_id) {
            $aor_data = $request->only(['aor_name', 'aor_cell_no', 'aor_memo_no', 'aor_memo_date']);
            $aor_data['aor_memo_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $aor_data['aor_memo_date']) ));

            $cc = AOR::where(['id' => $aor_id])->first();

            AOR::updateOrCreate(['id' => $aor_id], $aor_data);
            $this->logCaseHistory($caseID, $aor_data, $cc->toArray());
        }
    }

    private function updateCourtContempted($request, $caseID) {
        /***  Save Court Contempted Data   ***/
        $court_contempt_data = $request->only(['contempt_no', 'contempt_received_date', 'appearance_date', 'contempt_remarks']);
        $court_contempt_data['case_id']                 = $caseID;
        $court_contempt_data['contempt_received_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $court_contempt_data['contempt_received_date'])));
        $court_contempt_data['appearance_date']         = date('Y-m-d', strtotime(str_replace('/', '-', $court_contempt_data['appearance_date'])));

        $cc = CourtContempt::where(['case_id' => $caseID])->first();

        if ( !empty($cc) ) {
            $this->logCaseHistory($caseID, $court_contempt_data, $cc->toArray());
            $cc->update($court_contempt_data);
        }
        else {
            // now update the record
            CourtContempt::Insert($court_contempt_data);
        }
    }

    private function logCaseHistory($caseID, $newData, $oldData)
    {
        // get different / changes value by comparing with posted value and old database value.
        $diffValueArr = array_diff_assoc($newData, $oldData);

        $updateTime = date('Y-m-d H:i');

        foreach($diffValueArr as $diffKey => $diffVal){
            $newValue = $diffVal;
            if(isset($oldData[$diffKey]))
                $oldValue = $oldData[$diffKey];
            else $oldValue = '';

            $updateHistory['caseID']        = $caseID;
            $updateHistory['fieldUpdated']  = $diffKey;
            $updateHistory['previousValue'] = $oldValue;
            $updateHistory['newValue']      = $newValue;
            $updateHistory['updatedBy']     = Auth::user()->id;
            $updateHistory['updatedTime']   = $updateTime;
            CaseHistory::create($updateHistory);

            // unset $updateHistory and re-initiate for use again.
            unset($updateHistory);
            $updateHistory = array();
        }
    }

    private function updateRuleNisi($request, $caseID)
    {
        //echo "<pre>"; print_r($request->only(['rule_nisi_order_date', 'rule_nisi_order_details'])); die;
        $rule_nisi_data                            = $request->only(['rule_nisi_order_date', 'rule_nisi_order_details']);
        $rule_nisi_data['case_id']                 = $caseID;
        $rule_nisi_data['rule_nisi_order_date']    = isset($rule_nisi_data['rule_nisi_order_date'])     ? date('Y-m-d', strtotime(str_replace('/', '-', $rule_nisi_data['rule_nisi_order_date'] ) )) : '0000-00-00';
        $rule_nisi_data['rule_nisi_order_details'] = isset($rule_nisi_data['rule_nisi_order_details'])  ? $rule_nisi_data['rule_nisi_order_details'] : '';

        // retrive the case's rule nisi
        $rule_nisi = RuleNisi::where(['case_id' => $caseID])->first();

        if ( !empty($rule_nisi) ) {
            $this->logCaseHistory($caseID, $rule_nisi_data, $rule_nisi->toArray());
            $rule_nisi->update($rule_nisi_data);
        }
        else {
            // now update the record
            RuleNisi::Insert($rule_nisi_data);
        }
    }

    private function updateInterimOrder($request, $caseID)
    {
        $int_order_data                        = $request->only(['interim_order_date', 'order_details', 'deadline']);
        //$interim_order_data[]                  = $request->only(['implementing_authority', 'ministry', 'dc_office', 'uno_office', 'responsibility']);
        $int_order_data['case_id']             = $caseID;
        $int_order_data['interim_order_date']  = date('Y-m-d', strtotime(str_replace('/', '-', $int_order_data['interim_order_date'] ) ));

        // retrive the case's interim order
        $interim_order = InterimOrder::where(['case_id' => $caseID])->first();
        // now update the record
        if ( !empty($interim_order) ) {
            $interim_order->update($int_order_data);
        }
        else {
            $interim_order = InterimOrder::create($int_order_data);
        }

        $interim_order_id = $interim_order->id;

        /**** DELETE ALL THE INTERIM ORDER DETAILS FROM THE TABLE ****/
        InterimOrderDetails::where(['case_id' => $caseID, 'interim_order_id' => $interim_order_id])->delete();
        //dd( $interim_order_data); die;
        // save interim order details
        if ( $request->has('interim_order_data') ) {
            foreach ( $request->input('interim_order_data') as $iod) {
                $interim_order_details['implementing_authority'] = isset($iod['implementing_authority']) ? $iod['implementing_authority'] : 0;
                $interim_order_details['ministry']               = isset($iod['ministry']) ? $iod['ministry']                             : 0;
                $interim_order_details['dc_office']              = isset($iod['dc_office']) ? $iod['dc_office']                           : 0;
                $interim_order_details['uno_office']             = isset($iod['uno_office']) ? $iod['uno_office']                         : 0;
                $interim_order_details['responsibility']         = isset($iod['responsibility']) ? $iod['responsibility']                 : '';
                $interim_order_details['int_issue_memo']         = isset($iod['int_issue_memo']) ? $iod['int_issue_memo']                 : '';
                $interim_order_details['int_receive_memo']       = isset($iod['int_receive_memo']) ? $iod['int_receive_memo']             : '';
                $interim_order_details['int_sending_memo']       = isset($iod['int_sending_memo']) ? $iod['int_sending_memo']             : '';
                $interim_order_details['int_issue_date']         = isset($iod['int_issue_date'])  ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_issue_date'] ) ))     : '';
                $interim_order_details['int_receive_date']       = isset($iod['int_receive_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_receive_date'] ) ))  : '';
                $interim_order_details['int_sending_date']       = isset($iod['int_sending_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $iod['int_sending_date'] ) ))  : '';
                $interim_order_details['case_id']                = $caseID;
                $interim_order_details['interim_order_id']       = $interim_order_id;

                InterimOrderDetails::create($interim_order_details);

                $this->logCaseHistory($caseID, $interim_order_details, $interim_order->toArray());
            }
        }
    }

    private function updateSFDetails($request, $caseID)
    {
        // retrive the case's interim order
        $sf_details = StatementFacts::where(['case_id' => $caseID])->first();
        //$sf_data[]  = $request->only(['sf_recipient', 'sf_ministry', 'sf_dc_office', 'sf_uno_office', 'sf_issue_memo', 'sf_issue_date', 'sf_receive_memo', 'sf_receive_date', 'sf_sending_memo', 'sf_sending_date']);

        StatementFacts::where(['case_id' => $caseID])->delete();

        //$sf_data = $request->input('sf_data');

        // save sf details
        if ( $request->has('sf_data') ) {
            foreach ( $request->input('sf_data') as $sd) {
                $sf_data['sf_recipient']     = isset($sd['sf_recipient'])    ? $sd['sf_recipient']     : 0;
                $sf_data['sf_ministry']      = isset($sd['sf_ministry'])     ? $sd['sf_ministry']      : 0;
                $sf_data['sf_dc_office']     = isset($sd['sf_dc_office'])    ? $sd['sf_dc_office']     : 0;
                $sf_data['sf_uno_office']    = isset($sd['sf_uno_office'])   ? $sd['sf_uno_office']    : 0;
                $sf_data['sf_issue_memo']    = isset($sd['sf_issue_memo'])   ? $sd['sf_issue_memo']    : 0;
                $sf_data['sf_receive_memo']  = isset($sd['sf_receive_memo']) ? $sd['sf_receive_memo']  : '';
                $sf_data['sf_sending_memo']  = isset($sd['sf_sending_memo']) ? $sd['sf_sending_memo']  : 0;
                $sf_data['sf_issue_date']    = isset($sd['sf_issue_date'])   ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_issue_date']))) : '0000-00-00';
                $sf_data['sf_receive_date']  = isset($sd['sf_receive_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_receive_date']))) : '0000-00-00';
                $sf_data['sf_sending_date']  = isset($sd['sf_sending_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $sd['sf_sending_date']))) : '0000-00-00';
                $sf_data['case_id']          = $caseID;

                StatementFacts::create($sf_data);

                $this->logCaseHistory($caseID, $sf_data, $sf_details->toArray());
            }
        }
    }

    private function updateJudgementDetails($request, $caseID)
    {
        $judgement_data = $request->only(['cmp_no', 'cmp_judgement', 'cp_no', 'cp_judgement', 'appeal_case_no',
                                          'appeal_judgement', 'review_petition_no', 'review_order', 'final_judgement']); //dd($request);
        $judgement_data['case_id'] = $caseID;
        //$judgement_data['hearing_date']         = isset($judgement_data['hearing_date'])       ? date("Y-m-d", strtotime($judgement_data['hearing_date'])) : '0000-00-00';
        //$judgement_data['judgement']            = isset($judgement_data['judgement'])          ? $judgement_data['judgement']          : '';
        $judgement_data['cmp_no']              = isset($judgement_data['cmp_no'])             ? $judgement_data['cmp_no']             : '';
        $judgement_data['cmp_judgement']       = isset($judgement_data['cmp_judgement'])      ? $judgement_data['cmp_judgement']      : '';
        $judgement_data['cp_no']               = isset($judgement_data['cp_no'])              ? $judgement_data['cp_no']              : ''; // cp_no is cpla_no
        $judgement_data['cp_judgement']        = isset($judgement_data['cp_judgement'])       ? $judgement_data['cp_judgement']       : ''; // co_judgement is cpla_judgement
        $judgement_data['appeal_case_no']      = isset($judgement_data['appeal_case_no'])     ? $judgement_data['appeal_case_no']     : '';
        $judgement_data['appeal_judgement']    = isset($judgement_data['appeal_judgement'])   ? $judgement_data['appeal_judgement']   : '';
        $judgement_data['review_petition_no']  = isset($judgement_data['review_petition_no']) ? $judgement_data['review_petition_no'] : '';
        $judgement_data['review_order']        = isset($judgement_data['review_order'])       ? $judgement_data['review_order']       : '';
        $judgement_data['final_judgement']     = isset($judgement_data['final_judgement'])    ? $judgement_data['final_judgement']    : '';

        JudgementDetails::where(['case_id' => $caseID])->delete();

        JudgementDetails::create($judgement_data);

        if ( $request->has('hearing_data') ) {
            HearingDetails::where(['case_id' => $caseID])->delete();

            foreach ( $request->input('hearing_data') as $hd) {
                $hd_data['case_id']            = $caseID;
                $hd_data['hearing_date']       = isset($hd['hearing_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $hd['hearing_date']))) : '0000-00-00';
                $hd_data['hearing_judgement']  = isset($hd['judgement'])    ? $hd['judgement'] : '';

                HearingDetails::create($hd_data);
            }
        }

        //$this->logCaseHistory($caseID, $judgement_data, $interim_order->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function deleteCase(Request $request)
    {
        $caseID = $request->input('caseID');

        //echo 'Case ::: ' . $caseID;

        /*RuleNisi::find($caseID)->delete();
        InterimOrder::find($caseID)->delete();
        InterimOrderDetails::find($caseID)->delete();
        StatementFacts::find($caseID)->delete();
        JudgementDetails::find($caseID)->delete();*/

        ForwardDairy::find($caseID)->delete();

        echo '1';
        die;
    }

    public function getAORDetails(Request $request) {
        $aor_id = $request->input('aor_id');

        $aor_data = AOR::where(['id' => $aor_id])->get();

        echo json_encode($aor_data[0]);
    }

    public function checkDuplicateCaseNo(Request $request) 
    {
        $case_no = $request->input('case_no');

        DB::enableQueryLog();

        $case_details = ForwardDairy::select('case_no')->where(['case_no' => $case_no])->get();

        $last = DB::getQueryLog();


        if ( @$case_details[0]->case_no )
        {
            echo 'Duplicate';
        }
        else 
        {
            echo 'Unique';
        }
        
    }

    public function exportForwardDairyToPDF()
    {
        $case_list                   = DB::table('cases')->select('cases.id', 'cases.file_no', 'cases.case_no', 'cases.case_receive_date',
                                                                  'cases.case_subject', 'case_status.id as case_status_id','case_status.case_status', 'cases.has_rule_nisi',
                                                                  'cases.has_interim_order', 'case_type.short_name AS case_type')
                                           ->leftJoin('case_status', 'cases.case_status', '=', 'case_status.id')
                                           ->leftJoin('case_type', 'cases.case_type', '=', 'case_type.id')
                                           ->get();

        $pdf = mPDF::loadView('forwarddairy.caselistpdf', compact('case_list'), [], [
            'format' => 'A4-L'
        ]);

        return $pdf->download('caselistpdf.pdf');


    }
}