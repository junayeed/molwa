<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Upazila;
use App\Models\District;
use App\Models\UserGroup;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action                     = app('request')->route()->getAction();
        $controller                 = class_basename($action['controller']);
        list($controller, $action)  = explode('@', $controller);

        $user_list = User::select('users.*', 'district_tbl.name AS district_name', 'upazila_tbl.name AS upazila_name')
                         ->leftJoin('district_tbl', 'users.district_id', '=', 'district_tbl.id')
                         ->leftJoin('upazila_tbl', 'users.upazila_id', '=', 'upazila_tbl.id')
                         ->get();
        $data['nav_item']            = str_replace("Controller", "", $controller);
        $data['nav_sub_item']        = $action;
        //echo "<pre>"; print_r($user_list); die;
        return view('user.list', compact('user_list', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $district_list = District::get();
        $user_group = UserGroup::get();

        return view('user.create', compact('district_list', 'user_group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = User::create($request->all());

        $response->password = bcrypt($request->password);
        $response->save();

        return redirect('users');
    }

    public function createUNO(){
        $upazilla_list = DB::table('upazilla_names AS UP')->select('upazilla_name_en', 'UP.id AS upazila_id', 'DIS.id AS district_id')
                                                          ->leftJoin('district_names AS DIS', 'DIS.id', '=', 'UP.district_id')->get()->toArray();
        //echo '<pre>'; print_r($upazilla_list);die();
        //$this->deleteUNOUser();

        foreach($upazilla_list AS $up) {
            if ( $up->upazilla_name_en && $up->upazila_id ) {
                $user_data['name']         = 'uno-' . str_replace(' ', '_', strtolower($up->upazilla_name_en));
                $user_data['email']        = 'uno-' . str_replace(' ', '_', strtolower($up->upazilla_name_en));
                $user_data['user_level']   = 1;
                $user_data['user_type']    = 99;
                $user_data['designation']  = 'UNO';
                $user_data['district_id']  = $up->district_id;
                $user_data['upazila_id']   = $up->upazila_id;
                $user_data['password']     = bcrypt('uno'.$up->upazila_id);

                User::create($user_data);
            }
        }
    }

    public function deleteUNOUser() {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            abort(404, 'Not Found');
        }

        $district_list = District::get();
        $upazila_list = Upazila::where('upa_district_id', $user->district_id)->get();
        $user_group = UserGroup::get();

        return view('user.edit', compact('user', 'district_list', 'user_group', 'upazila_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo "<pre>"; print_r($request->all()); die;
        $response = User::find($id)->update($request->all());

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUpazilaInfoList(Request $request)
    {
        $district_id = $request->input('value');

        $upazila_list = Upazila::where('upa_district_id', $district_id)->get();
        // print_r($basic_info); exit;
        if ($upazila_list):
            echo '<option value="">Please Select</option>';
            foreach ( $upazila_list as $value ):
                echo '<option value=' . $value->id . '>' . $value->name . '</option>';
            endforeach;
        else :
            echo '';

        endif;
    }
}
