<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indicator;
use App\Models\MainArea;
use App\Models\SubIndicator;
use App\Models\Application;
use App\Models\ApplicationData;
use App\Models\UserGroup;
use Illuminate\Support\Facades\Input;
use Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getReport(Request $request)
    {
        $application_name = $request->input('application_name');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $year = $request->input('year');
        $month = $request->input('month');



        $user_group = UserGroup::find(Auth::user()->user_level);

        $user_level = $user_group->name;

        $q = Application::query();

        if (Input::has('application_name')) {
            $q->where('name', 'LIKE', '%'.Input::get('application_name').'%');
        }
        if (Input::has('from_date')) {
            $q->where('date', '>=', Input::get('from_date'));
        }
        if (Input::has('to_date')) {
            $q->where('date', '<=', Input::get('to_date'));
        }
        if (Input::has('year')) {
            $q->where('year', Input::get('year'));
        }
        if (Input::has('month')) {
            $q->where('month', Input::get('month'));
        }


        if($user_level == 'Upazila')
        {

            $application = $q->where('district_id', Auth::user()->district_id)
                             ->where('upazila_id', Auth::user()->upazila_id)
                            // ->where('data_status', 3)
                             ->where('status', 1)
                             ->get();
        }
        else if($user_level == 'Head Office')
        {
            $application = $q->where('data_status', 3)
                             ->where('status', 1)
                             ->get();
        }

        $i = 1;
        foreach($application as $rowdata){
            echo '<tr class="odd gradeX">
                    <td>'.$i++.'</td>
                    <td>'.$rowdata->name.'</td>
                    <td>'.$rowdata->name_bangla.'</td>
                    <td>'.$rowdata->year.'</td>';
                    if($rowdata->month == 1)
                    {
                        echo '<td>January-June</td>';
                    }
                    else{
                        echo '<td>July-December</td>';
                    }
                    if($user_level == 'Upazila')
                    {
                        echo '<td class="text-center no-print">                       
                        <a class="btn btn-view" href="/ApplicationData/'.$rowdata->id.'"><i class="fa fa-eye"></i> Show</a>
                        </td>';
                    }
                    else
                    {
                        echo '<td>
                        <a class="btn btn-view" href="/ApplicationData/'.$rowdata->id.'"><i class="fa fa-eye"></i> Show</a>
                    </td>';
                    }
                echo '</tr>';
        }
    }
    
}
