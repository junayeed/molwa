<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\FFData;
use mPDF;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportFFDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $inputFileName = 'Barguna.xlsx';

        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        //dd($sheetData);
        $rowCount = 0;

        foreach ($sheetData AS $rows => $row) {
            //$num = $rows;
            // skipping the first row of the excel sheet
            if ($rowCount == 0) {
                $rowCount++;
                continue;
            }
            //foreach ($row AS $key => $value)
            //{
                //echo "Key = " . $key . "::: Value = " . $value . "<br>";
                $ffIDArray = array($row['B'], $row['C'], $row['D'], $row['E'], $row['G'], $row['H'], $row['I']);

                $ffListTypeArray = $this->getFFListType($ffIDArray);
                //echo "<pre>";print_r($ffListTypeArray);

                foreach($ffListTypeArray AS $listType) {
                    if ( $listType == 1 ) { $indian = $row['P']; }
                    else if ( $listType == 2 ) { $bamush = $row['T']; }
                    else if ( $listType == 3 ) { $gazette_civil = $row['R']; }
                    else if ($listType == 4 ) { $certificate = $row['S']; }
                    else if ($listType == 5 ) { $gazette_civil_m = $row['U']; }
                    else if ($listType == 6 ) { $gazette_armed_force_m = $row['V']; }
                    else if ($listType == 7 ) { $gazette_BGD_m = $row['W']; }
                    else if ($listType == 8 ) { $gazette_police_m = $row['X']; }
                    else if ($listType == 9 ) { $gazette_wounded = $row['Y']; }
                    else if ($listType == 10 ) { $gazette_titled = $row['Z']; }
                    else if ($listType == 11 ) { $gazette_mujibnagar = $row['AA']; }
                    else if ($listType == 12 ) { $gazette_BCS_SR = $row['AB']; }
                    else if ($listType == 13 ) { $gazette_BCS = $row['AC']; }
                    else if ($listType == 13 ) { $gazette_BCS = $row['AC']; }
                    else if ($listType == 14 ) { $gazette_army = $row['AD']; }
                    else if ($listType == 15 ) { $gazette_air = $row['AE']; }
                    else if ($listType == 16 ) { $gazette_navy = $row['AF']; }
                    else if ($listType == 17 ) { $gazette_navy_COM = $row['AG']; }
                    else if ($listType == 18 ) { $gazette_BGB = $row['AH']; }
                    else if ($listType == 19 ) { $gazette_police = $row['AI']; }
                    else if ($listType == 20 ) { $gazette_anser = $row['AJ']; }
                    else if ($listType == 21 ) { $gazette_sound = $row['AK']; }
                    else if ($listType == 22 ) { $gazette_birangona = $row['AL']; }
                    else if ($listType == 23 ) { $gazette_football = $row['AM']; }
                    else if ($listType == 24 ) { $gazette_nap = $row['AN']; }
                    else if ($listType == 25 ) { $lal = $row['Q']; }
                    else if ($listType == 26 ) { $lal_sp = $row['AO']; }
                    else if ($listType == 27 ) { $indian_armed_force = $row['AP']; }
                    else if ($listType == 28 ) { $indian_padma = $row['AR']; }
                    else if ($listType == 29 ) { $indian_meghna = $row['AS']; }
                    else if ($listType == 31 ) { $war_crippled_border_guard = $row['AT']; }
                    else if ($listType == 32 ) { $Warlike_border_guard = $row['AU']; }
                    else if ($listType == 33 ) { $indian_sector = $row['AV']; }
                    else if ($listType == 34 ) { $bisharmgonj = $row['AW']; }
                    else if ($listType == 35 ) { $warwound_army = $row['AX']; }
                }

                //echo 'বেসামরিক গেজেট = ' . @$row['R'] . ' - লালমুক্তিবার্তা = ' . @$row['Q'] . ' - ভারতীয় তালিকা = ' . @$row['P'] . ' - সাময়িক সনদ = ' . @$row['S'] . '<br>';
                list($ff_name, $ff_father_name, $ff_village, $ff_postoffice, $ff_upazilla, $ff_district) = array($row['J'], $row['K'], $row['L'], $row['M'], $row['N'], $row['O']);

            $ff_id = DB::table('new_ff_details')->insertGetId(['ff_name' => $ff_name, 'ff_father_name' => $ff_father_name, 'village' => $ff_village,
                                                               'postoffice' => $ff_postoffice, 'upazilla' => 1, 'district' => 1, 'upazilla_text' => 'has',
                                                               'district_text' => 'aad']
                                                              );
           // }
            $rowCount++;
            if ($rowCount == 3) break;
        }

        //return view('ffdata.list', compact('ff_list', 'data'));
    }

    public function getFFListType($ffIDArray) {
        if ( !empty($ffIDArray) ) {
            foreach ($ffIDArray AS $ffID) {
                if ( $ffID ) {
                    $listType = DB::table('ff_list')->where('id', '=', $ffID)->select('ff_list_type_id')->get()->toArray();

                    $ffListTypeArray[] = $listType[0]->ff_list_type_id;
                }
            }

            return $ffListTypeArray;
        }
    }

}