<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Indicator;
use App\Models\MainArea;
use App\User;

define('ACTIVE', 1);

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $indicator_list = Indicator::get();

        return view('indicator.list', compact('indicator_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_area_list = MainArea::where('status', ACTIVE)->get();

        return view('indicator.create', compact('main_area_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $indicator_data = $request->all();

        $indicator_data['indicator_status'] = isset($indicator_data['indicator_status']) ? 1 : 0;

        $indicator = Indicator::create($indicator_data);

        $indicator->save();

        return redirect('Indicator');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indicator      = Indicator::find($id);
        /*$indicator      = DB::table('indicators')
                              ->select('*', 'indicators.id AS id', 'mainareas.id AS mainarea_id')
                              ->join('mainareas', 'indicators.main_area', '=', 'mainareas.id')
                              ->where(['indicators.id' => $id])
                              ->get();*/

        $main_area_list = MainArea::where('status', ACTIVE)->get();

        //echo "<pre>"; print_r($main_area_list); die;

        if (is_null($indicator)) {
            abort(404, 'Not Found');
        }

        return view('indicator.edit', compact('indicator', 'main_area_list'));
    }

    public function delete($id)
    {
        print_r($id); die;
        MainArea::find($id)->delete();
        return redirect('MainArea');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indicator_data = $request->all();

        $indicator_data['indicator_status'] = isset($indicator_data['indicator_status']) ? 1 : 0;

        $response = Indicator::find($id)->update($indicator_data);

        return redirect('Indicator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        print_r($id); die;
    }

    public function getMainAreaDetails(Request $request)
    {
        $id = $request->input('id');

        $main_area_details = MainArea::where('id', $id)->get();
        $indicator_list    = Indicator::select('id', 'indicator_title')->where('main_area', $id)->get();

        echo json_encode( array($main_area_details, $indicator_list) );
        die;

    }

    public function deleteIndicator(Request $request)
    {
        $id = $request->input('id');

        Indicator::find($id)->delete();

        echo '1';
        die;
    }
}
