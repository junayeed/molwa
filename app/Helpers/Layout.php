<?php
class Layout {

    public static function BoxStart($heading, $action = '') {

        echo '<!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                        '. $heading .'
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    '.$action.'
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                    <div class="m-section__content">';

    }

    public static function BoxEnd() {
        echo  '
            </div>
                <!--end::Section-->
            </div>
        </div>

        <!--end::Portlet-->
        ';
    }

}
?>
