<?php

use App\Modules\Local\Batch\Models\BatchModel;
class Html {

    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    public static $bn_class = array("১ম শ্রেণী", "২য় শ্রেণী", "৩য় শ্রেণী", "৪র্থ শ্রেণী");
    public static $en_class = array("1", "2", "3", "4");

    public static $month_bn_class = array("জানুয়ারি", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
    public static $month_en_class = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    public static function getActiveBatchData() {
        $batch = BatchModel::where('batch_status', 'Active')->get()->first();

        return $batch;
    }

    public static function en2bn($number) {
        $n = str_replace(self::$en, self::$bn, $number);
        return $n;
    }

    public static function dateEN2BN($str) {

        list($month, $year) = explode(", ", $str);

        $bn_year = str_replace(self::$en, self::$bn, $year);
        $bn_month = str_replace(self::$month_en_class, self::$month_bn_class, $month);

        return $bn_month . ', ' . $bn_year;
    }

    public static function formatEmployeeClass($number) {
        $cls_bn = str_replace(self::$en_class, self::$bn_class, $number);
        return $cls_bn;
    }

    public static function divisionNumber($numerator, $denominator) {
        return $denominator == 0 ? 0 : ($numerator/$denominator);
    }

    /*
    *   This function will create Select field
    */
    public static function Select($name, $options, $default = '', $class = 'form-control custom-select', $extraAttr = '') {

        $attributes = array(
            'class' => $class
        );
        if($extraAttr != '') {
            $attributes = array_merge($attributes, $extraAttr);
        }
        return Form::select(  $name,   $options,  $default,   $attributes  );
    }

    /*
    * This function will create Text field
    */
    public static function Text($name, $default = '', $id = '', $class = 'form-control m-input', $extraAttr = '') {
        $attributes = array(
            'class' => $class
        );
        if($extraAttr != '') {
            $attributes = array_merge($attributes, $extraAttr);
        }
        if($id != '') {
            $attributes['id'] = $id;
        }

        return Form::text($name, $default, $attributes);
    }

    /*
    * This field will create Date picker field
    */

    public static function Date($name, $default = '', $id = '', $class = 'form-control m-input date-picker', $extraAttr = '') {
        $attributes = array(
            'class' => $class
        );
        if($extraAttr != '') {
            $attributes = array_merge($attributes, $extraAttr);
        }
        if($id != '') {
            $attributes['id'] = $id;
        }
        $attributes['readonly '] = 'readonly';
        return Form::text($name, $default, $attributes);
        
    }

    public static function File($name, $default = '', $id = '', $class = 'custom-file-input', $extraAttr = '') {
        $attributes = array(
            'class' => $class
        );
        if($extraAttr != '') {
            $attributes = array_merge($attributes, $extraAttr);
        }
        if($id != '') {
            $attributes['id'] = $id;
        }
        $attributes['readonly '] = 'readonly';

        return'
            <div class="custom-file">'.
                Form::file($name, $attributes)
                .'<label class="custom-file-label" for="customFile">Choose file</label>
            </div>';
    }
    
    /*
    * This field will create Textarea
    */
    public static function Textarea($name, $default = '', $id = '', $class = 'form-control m-input', $extraAttr = '') {
        $attributes = array(
            'class' => $class
        );
        if($extraAttr != '') {
            $attributes = array_merge($attributes, $extraAttr);
        }
        if($id != '') {
            $attributes['id'] = $id;
        }
        return Form::textarea($name, $default, $attributes);
    }

    /*
    *   This function will Create Yes/No select box
    */
    public static function YesNoSelect($name, $default = '', $class = 'form-control custom-select', $extraAttr = '') {
        $options = array('Yes' => 'Yes', 'No' => 'No');
        return self::Select( $name, $options, $default = '', $class, $extraAttr);
    }

    /*
    *   This function will Create Radios fields
    */
    public static function Radios($name, $options, $default = '', $layout = 'inline') {

        if($layout !== 'inline') {
            $layout = 'list';
        }
        $html = '<div class="m-radio-'.$layout.'">';

        foreach($options as $k => $v) {
            $checked = $default == $k ? true : false;
            $html .= '<label class="m-radio">
                        '.Form::radio($name, $k, $checked) . ' ' . $v . '
                        <span></span>
                    </label>';
        }

        $html .= '</div>';
        return $html;
    }

    public static function Checkbox($name, $value ='', $checked = false, $label = '') {

        $html = '<label class="m-checkbox">'.
                        Form::checkbox($name, $value, $checked) .' ' . $label . '
                    <span></span>
                </label>'; 

        return $html;      
    }

    /*
    *   This function will Create Checkbox fields
    */
    public static function Checkboxes($name, $options, $default = '', $layout = 'inline') {

        if($layout !== 'inline') {
            $layout = 'list';
        }
        $html = '<div class="m-checkbox-'.$layout.'">';

        foreach($options as $k => $v) {
            $checked = $default == $k ? true : false;
            $html .= '<label class="m-checkbox">
                        '.Form::checkbox($name, $k, $checked) . ' ' . $v . '
                        <span></span>
                    </label>';
        }

        $html .= '</div>';
        return $html;
    }



}

