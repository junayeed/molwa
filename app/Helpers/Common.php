<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/6/2019
 * Time: 6:26 PM
 */

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Modules\Core\AuditLog\Models\AuditLogModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

Class Common {
    public static $weekends = array("Fri", "Sat");

    public static function dumpVar($data) {
        echo "<pre>"; print_r($data);
        die('---Die here');
    }

    public static function AuditLog($module_name, $row_id, $previous_val_arr, $current_val_arr, $action, $filed_list) {
        $AuditLog['row_id']         = $row_id;
        $AuditLog['module_name']    = $module_name;
        $AuditLog['action_by']      = Auth::user()->id;
        $AuditLog['action']         = $action;
        $AuditLog['field_updated']  = '';
        $AuditLog['previous_value'] = '';
        $AuditLog['current_value']  = '';

        if ($action != 'Updated') {
            AuditLogModel::create($AuditLog);
            unset($AuditLog);
            $AuditLog = array();
            return;
        }
        else {
            $diffValueArr = array_diff_assoc($current_val_arr, $previous_val_arr);

            foreach ($diffValueArr as $diffKey => $diffVal) {
                if (in_array($diffKey, $filed_list)) {
                    $newValue = $diffVal;
                    if (isset($previous_val_arr[$diffKey]))
                        $oldValue = $previous_val_arr[$diffKey];
                    else $oldValue = '';

                    $AuditLog['row_id']         = $row_id;
                    $AuditLog['module_name']    = $module_name;
                    $AuditLog['action_by']      = Auth::user()->id;
                    $AuditLog['action']         = $action;
                    $AuditLog['field_updated']  = $diffKey;
                    $AuditLog['previous_value'] = $oldValue;
                    $AuditLog['current_value']  = $newValue;

                    AuditLogModel::create($AuditLog);

                    // unset $AuditLog and re-initiate for use again.
                    unset($AuditLog);
                    $AuditLog = array();
                }
            }
        }
    }

    public static function getAuditLogData($module_name, $row_id){
        $auditData = AuditLogModel::select('*')->where([ ['module_name', $module_name], ['row_id', $row_id] ])->orderBy('created_at', 'DESC')->get()->all();

        return $auditData;
    }

    public static function getCurrentFinancialYear() {
        $financial_year = (date('m')<='06') ?
                             date('Y',strtotime('-1 year')) .'-'. date('Y') :
                             date('Y') . '-' . date('Y',strtotime('+1 year'));

        return $financial_year;
    }

    /**
     * Generate the Attandance Sheet
     *
     * @param $batch_data
     * @param $trainer_data
     */
    public static function generateAttandanceSheet($batch_data, $parcipant_data)
    {
        $spreadsheet = new Spreadsheet();

        $styleArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ]
        ];

        $spreadsheet->getDefaultStyle()->getFont()->setName('Nikosh');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(14);

        $start            = new \Carbon\Carbon($batch_data->batch_start_date);
        $end              = new \Carbon\Carbon($batch_data->batch_end_date);
        $batch_start_time = new \Carbon\Carbon($batch_data->batch_start_time);
        $batch_end_time   = new \Carbon\Carbon($batch_data->batch_end_time);

        /*** Page Header ***/
        $spreadsheet->getActiveSheet()->setCellValue('C3', 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার');
        $spreadsheet->getActiveSheet()->setCellValue('C4', 'মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়');
        $spreadsheet->getActiveSheet()->setCellValue('C5', 'প্রশিক্ষণ নং ' . \Html::en2bn($batch_data->batch_no) . ': ' . $batch_data->training_title);
        $spreadsheet->getActiveSheet()->setCellValue('C6', 'তারিখঃ ' . \Html::en2bn($start->format('d/m/Y')) . ' - ' . \Html::en2bn($end->format('d/m/Y')) . '; সময়: ' . \Html::en2bn($batch_start_time->format('h:i')) . ' - ' . \Html::en2bn($batch_end_time->format('h:i')) );
        $spreadsheet->getActiveSheet()->setCellValue('C7', 'হাজিরা শীট');

        /*** Page Header ***/

        /*** Cell Header***/
        $spreadsheet->getActiveSheet()->setCellValue('C8', '(জেষ্ঠ্যতার ক্রমানুসারে নহে)');
        $spreadsheet->getActiveSheet()->mergeCells('C8:D8');
        $spreadsheet->getActiveSheet()->setCellValue('C9', 'ক্রমিক');
        $spreadsheet->getActiveSheet()->setCellValue('D9', 'নাম ও পদবী');
        $spreadsheet->getActiveSheet()->setCellValue('E9', 'স্বাক্ষর');
        $spreadsheet->getActiveSheet()->mergeCells('C9:C10');
        $spreadsheet->getActiveSheet()->mergeCells('D9:D10');
        $spreadsheet->getActiveSheet()->getStyle('C9:E9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C10:E10')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C3:E9')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(8, 10);

        $start   = new \Carbon\Carbon($batch_data->batch_start_date);
        $end     = new \Carbon\Carbon($batch_data->batch_end_date);
        $no_days = $start->diff($end)->days;

        $index_array = ['E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(23);


        for($i = 0; $i <= $no_days; $i++) {
            $temp = new \Carbon\Carbon($batch_data->batch_start_date);
            $date = $temp->addDays($i);
            $date_array[] = $date->format('d/m/Y');
            //echo "<pre>"; print_r($date_array);
            $spreadsheet->getActiveSheet()->setCellValue($index_array[$i].'10', \Html::en2bn($date->format('d/m/Y') ) );
            $spreadsheet->getActiveSheet()->getColumnDimension($index_array[$i])->setWidth(15);
            $spreadsheet->getActiveSheet()->getStyle($index_array[$i].'10')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle($index_array[$i].'10')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        }

        $spreadsheet->getActiveSheet()->mergeCells('E9:'. $index_array[$i-1].'9');
        $spreadsheet->getActiveSheet()->getStyle('E9:'. $index_array[$i-1].'9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('E9:'. $index_array[$i-1].'9')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->mergeCells('C3:'.$index_array[$i-1].'3');
        $spreadsheet->getActiveSheet()->mergeCells('C4:'.$index_array[$i-1].'4');
        $spreadsheet->getActiveSheet()->mergeCells('C5:'.$index_array[$i-1].'5');
        $spreadsheet->getActiveSheet()->mergeCells('C6:'.$index_array[$i-1].'6');
        $spreadsheet->getActiveSheet()->mergeCells('C7:'.$index_array[$i-1].'7');
        /*** Cell Header ***/

        $cellNo       = 11;
        $serial_no    = 1;
        //$day_diff = floor(abs(strtotime($batch_data->batch_end_date) - strtotime($batch_data->batch_start_date))/ 86400 ) + 1;

        /*** Content ***/
        foreach($parcipant_data as $key => $parcipant) {
            $spreadsheet->getActiveSheet()->setCellValue('C' . $cellNo, Html::en2bn(@$serial_no) . '.' );
            //$spreadsheet->getActiveSheet()->setCellValue('D' . $cellNo, @$parcipant->participant_name . ', ' . @$parcipant->designation . ' ('.@$parcipant->office_name_bn.')');
            $spreadsheet->getActiveSheet()->setCellValue('D' . $cellNo, @$parcipant->participant_name . "\n" . @$parcipant->designation);
            $spreadsheet->getActiveSheet()->setCellValue('E' . $cellNo, '');
            $spreadsheet->getActiveSheet()->getStyle('D' . $cellNo)->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getRowDimension($cellNo)->setRowHeight(40);

            $cellNo++;
            $serial_no++;
        }
        $spreadsheet->getActiveSheet()->getStyle('C11' . ':' . $index_array[$i-1] . ($cellNo-1) )->applyFromArray($styleArray);
        /*** Content ***/

        $spreadsheet->getActiveSheet()->getHeaderFooter()
            ->setOddFooter('&L' . 'প্রশিক্ষণ নং - ' . \Html::en2bn($batch_data->batch_no) . ' হাজীরা শীট' . '&Cহাজীরা শীটটি স্বংয়ক্রিয়ভাবে প্রস্তুতকৃত' .  '&Rপৃষ্ঠা &P / &N');

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('হাজিরা শীট');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats- officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Attandance_sheet.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');

        $writer->save('php://output');
        //exit;
    }

    public static function generateBatchSchedule($batch_data, $trainer_data)
    {
        $spreadsheet = new Spreadsheet();

        $styleArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ]
        ];

        $spreadsheet->getDefaultStyle()->getFont()->setName('Nikosh');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(14);

        $start            = new \Carbon\Carbon($batch_data->batch_start_date);
        $end              = new \Carbon\Carbon($batch_data->batch_end_date);
        $batch_start_time = new \Carbon\Carbon($batch_data->batch_start_time);
        $batch_end_time   = new \Carbon\Carbon($batch_data->batch_end_time);

        /*** Page Header ***/
        $spreadsheet->getActiveSheet()->setCellValue('B3', 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার');
        $spreadsheet->getActiveSheet()->setCellValue('B4', 'মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়');
        $spreadsheet->getActiveSheet()->setCellValue('B5', 'ব্যাচ ' . \Html::en2bn($batch_data->batch_no) . ': ' . $batch_data->training_title);
        $spreadsheet->getActiveSheet()->setCellValue('B6', 'তারিখঃ ' . \Html::en2bn($start->format('d/m/Y')) . ' - ' . \Html::en2bn($end->format('d/m/Y')) . '; সময়: ' . \Html::en2bn($batch_start_time->format('h:i')) . ' - ' . \Html::en2bn($batch_end_time->format('h:i')) );
        $spreadsheet->getActiveSheet()->setCellValue('B7', 'প্রশিক্ষণ সিডিউল');
        $spreadsheet->getActiveSheet()->mergeCells('B3:H3');
        $spreadsheet->getActiveSheet()->mergeCells('B4:H4');
        $spreadsheet->getActiveSheet()->mergeCells('B5:H5');
        $spreadsheet->getActiveSheet()->mergeCells('B6:H6');
        $spreadsheet->getActiveSheet()->mergeCells('B7:H7');

        /*** Page Header ***/

        /*** Cell Header***/
        $spreadsheet->getActiveSheet()->setCellValue('B8', '(জেষ্ঠ্যতার ক্রমানুসারে নহে)');
        $spreadsheet->getActiveSheet()->mergeCells('B8:C8');
        $spreadsheet->getActiveSheet()->setCellValue('B9', 'ক্রমিক');
        $spreadsheet->getActiveSheet()->setCellValue('C9', 'বিষয়');
        $spreadsheet->getActiveSheet()->setCellValue('D9', 'প্রশিক্ষক');
        $spreadsheet->getActiveSheet()->setCellValue('E9', 'তারিখ');
        $spreadsheet->getActiveSheet()->setCellValue('F9', 'শুরু');
        $spreadsheet->getActiveSheet()->setCellValue('G9', 'শেষ');
        $spreadsheet->getActiveSheet()->setCellValue('H9', 'স্বাক্ষর');
        $spreadsheet->getActiveSheet()->getStyle('B3:H9')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle('B9:H9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(8, 10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);

        /*** Cell Header ***/

        $cellNo       = 10;
        $serial_no    = 1;

        /*** Content ***/
        foreach($trainer_data as $key => $trainer) {
            $spreadsheet->getActiveSheet()->setCellValue('B' . $cellNo, Html::en2bn(@$serial_no) . '.' );
            $spreadsheet->getActiveSheet()->setCellValue('C' . $cellNo, @$trainer->session_topic);
            if ($trainer->trainer_type) {
                $spreadsheet->getActiveSheet()->setCellValue('D' . $cellNo, @$trainer->trainer);
            }
            else {
                $spreadsheet->getActiveSheet()->setCellValue('D' . $cellNo, @$trainer->trainer_name);
            }
            $spreadsheet->getActiveSheet()->setCellValue('E' . $cellNo, \Html::en2bn( \Carbon\Carbon::parse(@$trainer->session_date)->format('d/m/Y') )  );
            $spreadsheet->getActiveSheet()->setCellValue('F' . $cellNo, \Html::en2bn( \Carbon\Carbon::parse(@$trainer->session_start_time)->format('h:i') ) );
            $spreadsheet->getActiveSheet()->setCellValue('G' . $cellNo, \Html::en2bn( \Carbon\Carbon::parse(@$trainer->session_end_time)->format('h:i') ) );
            $spreadsheet->getActiveSheet()->getStyle('C' . $cellNo)->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getStyle('D' . $cellNo)->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getRowDimension($cellNo)->setRowHeight(40);

            $cellNo++;
            $serial_no++;
        }
        $spreadsheet->getActiveSheet()->getStyle('B10' . ':' . 'H' . ($cellNo-1) )->applyFromArray($styleArray);
        /*$spreadsheet->getActiveSheet()->getStyle('D11' . ':' . $index_array[$i-1] . ($cellNo-1) )->getAlignment()->setHorizontal('center');*/
        /*** Content ***/

        $spreadsheet->getActiveSheet()->getHeaderFooter()
            ->setOddFooter('&L' . 'প্রশিক্ষণ নং - ' . \Html::en2bn($batch_data->batch_no) . ' প্রশিক্ষণ সিডিউল' . '&Cপ্রশিক্ষণ সিডিউল স্বংয়ক্রিয়ভাবে প্রস্তুতকৃত' .  '&Rপৃষ্ঠা &P / &N');


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('প্রশিক্ষণ সিডিউল');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats- officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="batch_schedule.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');

        $writer->save('php://output');
        //exit;
    }

    public static function generateHonarariumSheet($batch_data, $parcipant_data)
    {
        $spreadsheet = new Spreadsheet();

        $styleArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ]
        ];

        $spreadsheet->getDefaultStyle()->getFont()->setName('Nikosh');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(14);

        $start            = new \Carbon\Carbon($batch_data->batch_start_date);
        $end              = new \Carbon\Carbon($batch_data->batch_end_date);
        $batch_start_time = new \Carbon\Carbon($batch_data->batch_start_time);
        $batch_end_time   = new \Carbon\Carbon($batch_data->batch_end_time);

        /*** Page Header ***/
        $spreadsheet->getActiveSheet()->setCellValue('B3', 'গণপ্রজাতন্ত্রী বাংলাদেশ সরকার');
        $spreadsheet->getActiveSheet()->setCellValue('B4', 'মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়');
        $spreadsheet->getActiveSheet()->setCellValue('B5', 'প্রশিক্ষণ নং ' . \Html::en2bn($batch_data->batch_no) . ': ' . $batch_data->training_title);
        $spreadsheet->getActiveSheet()->setCellValue('B6', 'তারিখঃ ' . \Html::en2bn($start->format('d/m/Y')) . ' - ' . \Html::en2bn($end->format('d/m/Y')) . '; সময়: ' . \Html::en2bn($batch_start_time->format('h:i')) . ' - ' . \Html::en2bn($batch_end_time->format('h:i')) );
        $spreadsheet->getActiveSheet()->setCellValue('B7', 'সম্মানী শীট');

        /*** Page Header ***/

        /*** Cell Header***/
        $spreadsheet->getActiveSheet()->setCellValue('B8', '(জেষ্ঠ্যতার ক্রমানুসারে নহে)');
        $spreadsheet->getActiveSheet()->mergeCells('B8:C8');
        $spreadsheet->getActiveSheet()->setCellValue('B9', 'ক্রমিক');
        $spreadsheet->getActiveSheet()->setCellValue('C9', 'নাম ও পদবী');
        $spreadsheet->getActiveSheet()->setCellValue('D9', 'সম্মানীর পরিমান');
        //$spreadsheet->getActiveSheet()->setCellValue('E9', 'উৎস কর (১০%)');
        //$spreadsheet->getActiveSheet()->setCellValue('F9', 'স্ট্যাম্প');
        $spreadsheet->getActiveSheet()->setCellValue('E9', 'নীট প্রদেয়');
        $spreadsheet->getActiveSheet()->setCellValue('F9', 'স্বাক্ষর');
        $spreadsheet->getActiveSheet()->getStyle('D9')->getAlignment()->setWrapText(true);
        //$spreadsheet->getActiveSheet()->getStyle('E9')->getAlignment()->setWrapText(true);
        //$spreadsheet->getActiveSheet()->getStyle('F9')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('G9')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->mergeCells('B9:B10');
        $spreadsheet->getActiveSheet()->mergeCells('C9:C10');
        $spreadsheet->getActiveSheet()->mergeCells('D9:D10');
        $spreadsheet->getActiveSheet()->mergeCells('E9:E10');
        //$spreadsheet->getActiveSheet()->mergeCells('F9:F10');
        $spreadsheet->getActiveSheet()->mergeCells('G9:G10');
        $spreadsheet->getActiveSheet()->getStyle('B9:D9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('B10:B10')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C9:C9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C10:C10')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('D9:D9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('D10:D10')->applyFromArray($styleArray);
        //$spreadsheet->getActiveSheet()->getStyle('E9:E9')->applyFromArray($styleArray);
        //$spreadsheet->getActiveSheet()->getStyle('E10:E10')->applyFromArray($styleArray);
        //$spreadsheet->getActiveSheet()->getStyle('F9:F9')->applyFromArray($styleArray);
        //$spreadsheet->getActiveSheet()->getStyle('F10:F10')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('B3:D9')->getAlignment()->setHorizontal('center');

        $start = new \Carbon\Carbon($batch_data->batch_start_date);
        $end = new \Carbon\Carbon($batch_data->batch_end_date);
        $no_days = $start->diff($end)->days+1;

        $index_array = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'];
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(23);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(8);
        //$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(4);
        //$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(6);


        for($i = 0; $i < $no_days; $i++)
        {
            $temp = new \Carbon\Carbon($batch_data->batch_start_date);
            $date = $temp->addDays($i);
            $date_array[] = $date->format('d/m/Y');
            //echo "<pre>"; print_r($date_array);
            $spreadsheet->getActiveSheet()->setCellValue($index_array[$i].'10', \Html::en2bn($date->format('d/m/Y') ) );
            $spreadsheet->getActiveSheet()->getColumnDimension($index_array[$i])->setWidth(20);
            $spreadsheet->getActiveSheet()->getStyle($index_array[$i].'10')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle($index_array[$i].'10')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        }

        $spreadsheet->getActiveSheet()->mergeCells('F9:'. $index_array[$i-1].'9');
        $spreadsheet->getActiveSheet()->getStyle('D9:'. $index_array[$i-1].'9')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('D9:'. $index_array[$i-1].'9')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->mergeCells('B3:'.$index_array[$i-1].'3');
        $spreadsheet->getActiveSheet()->mergeCells('B4:'.$index_array[$i-1].'4');
        $spreadsheet->getActiveSheet()->mergeCells('B5:'.$index_array[$i-1].'5');
        $spreadsheet->getActiveSheet()->mergeCells('B6:'.$index_array[$i-1].'6');
        $spreadsheet->getActiveSheet()->mergeCells('B7:'.$index_array[$i-1].'7');


        /*** Cell Header ***/

        $cellNo       = 11;
        $serial_no    = 1;
        $day_diff = floor(abs(strtotime($batch_data->batch_end_date) - strtotime($batch_data->batch_start_date))/ 86400 ) + 1;

        /*** Content ***/
        foreach($parcipant_data as $key => $parcipant) {
            $spreadsheet->getActiveSheet()->setCellValue('B' . $cellNo, Html::en2bn(@$serial_no) . '.' );
            $spreadsheet->getActiveSheet()->setCellValue('C' . $cellNo, @$parcipant->participant_name . "\n" . @$parcipant->designation);
            $daily_allowance = ($parcipant->pay_grade <= config('constants.TAXABLE_PAY_GRADE')) ? config('constants.TRAINING_PER_DAY_HONORARIUM_1_9') : config('constants.TRAINING_PER_DAY_HONORARIUM_10_20');
            $tax             = ($parcipant->pay_grade <= config('constants.TAXABLE_PAY_GRADE')) ? $daily_allowance * config('constants.SOURCE_TAX') : 0;
            $stamp_fee       = config('constants.STAMP');

            $spreadsheet->getActiveSheet()->setCellValue('D' . $cellNo, \Html::en2bn($daily_allowance) . '/-' );
            //$spreadsheet->getActiveSheet()->setCellValue('E' . $cellNo, \Html::en2bn($tax) . '/-' );
            //$spreadsheet->getActiveSheet()->setCellValue('F' . $cellNo, \Html::en2bn($stamp_fee) . '/-' );
            //$spreadsheet->getActiveSheet()->setCellValue('G' . $cellNo, \Html::en2bn( (($daily_allowance - $tax)*$no_days )-$stamp_fee) . '/-' );
            $spreadsheet->getActiveSheet()->setCellValue('E' . $cellNo, \Html::en2bn( ($daily_allowance*$no_days )) . '/-' );
            $spreadsheet->getActiveSheet()->getStyle('D' . $cellNo.':F' . $cellNo)->getAlignment()->setHorizontal('center');
            $spreadsheet->getActiveSheet()->getStyle('C' . $cellNo)->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getRowDimension($cellNo)->setRowHeight(40);

            $cellNo++;
            $serial_no++;
        }
        $spreadsheet->getActiveSheet()->getStyle('B11' . ':' . $index_array[$i-1] . ($cellNo-1) )->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('D11' . ':' . $index_array[$i-1] . ($cellNo-1) )->getAlignment()->setHorizontal('center');
        /*** Content ***/

        $spreadsheet->getActiveSheet()->getHeaderFooter()
            ->setOddFooter('&L' . 'প্রশিক্ষণ নং  - ' . \Html::en2bn($batch_data->batch_no) . ' সম্মানী শীট' . '&Cসম্মানী শীটটি স্বংয়ক্রিয়ভাবে প্রস্তুতকৃত' .  '&Rপৃষ্ঠা &P / &N');

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('সম্মানী শীট');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats- officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Honararium_sheet.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');

        $writer->save('php://output');
        //exit;
    }

    public static function pdfCreate($html, $report_name){
        //$html =  self::showTraineePDF();
        $page_settings = array('mode' => 'utf-8', 'format' => 'A4', 'font_size' => 18, 'default_font' => "'kalpurush', Kalpurush",
                               'margin_left'=> 15, 'margin_right'=> 15, 'margin_top'=> 10, 'margin_bottom'=> 5, 'margin_header'=> 5,
                               'margin_footer'=> 4, 'orientation' => 'P');

        $mpdf = new \Mpdf\Mpdf($page_settings);

        $mpdf->showWatermarkImage = true;
        $mpdf->useSubstitutions = false;
        $mpdf->simpleTables     = true;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("MOLWA Office");
        $mpdf->SetSubject("MOLWA");
        $mpdf->SetAuthor("MOLWA");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->SetDisplayMode('fullpage');
        //$mpdf->watermark_font = 'DejaVuSansCondensed';
        //$mpdf->SetWatermarkText('IGA');
        //$mpdf->showWatermarkText = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        //$stylesheet = file_get_contents('back-end/pdf_download.css');
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        //$mpdf->WriteHTML($stylesheet, 1);

        $mpdf->SetHTMLHeader('');
        $mpdf->SetHTMLFooter('<p style="text-align: center; font-size: 10px; font-family: Arial, Helvetica, sans-serif">Copy-right, ICT Cell@Ministry of Liberation War Affairs. 2021-2022</p>');

        $mpdf->defaultfooterfontsize  = 16;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline      = 1;

        $mpdf->SetCompression(true);

        //$mpdf->Write($data);
        $mpdf->WriteHTML($html);
        $mpdf->Output("$report_name.pdf", 'D');
    }

    public static function sendSMS($employee_id, $cellphone, $sms_text) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://bulksms.teletalk.com.bd/link_sms_send.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "op=SMS&user=molwa&pass=XGXNeLryKP&mobile=$cellphone&charset=ASCII&sms=$sms_text");

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close ($ch);

        if ( str_contains($server_output, 'SUCCESS') ) {
            \Common::storeNotificationData($employee_id, $cellphone,'SMS', $sms_text, $server_output, 'Success');
            return 'SUCCESS';
        }
        else if ( str_contains($server_output, 'FAILED') ) {
            \Common::storeNotificationData($employee_id, $cellphone,'SMS', $sms_text, $server_output, 'Failure');
            return 'FAILED';
        }
    }

    public static function storeNotificationData($employee_id, $receiver_credential, $notification_type, $send_text, $server_output, $response_type) {
        $data['receiver']             = $employee_id;
        $data['receiver_credential']  = $receiver_credential;
        $data['notification_type']    = $notification_type;
        $data['send_text']            = $send_text;
        $data['response']             = $server_output;
        $data['response_type']        = $response_type;

        DB::table('notifications')->insert($data);
    }

    public static function getDistrictDivisionID($districtID) {
        return DB::table('district_tbl AS DIS')
                 ->select('DIS.dis_division_id')
                 ->where('DIS.id', $districtID)->get()->all()[0]->dis_division_id;
    }

    public static function getCCDistrictID($ccID) {
        return DB::table('city_corporation AS CC')
            ->select('CC.district')
            ->where('CC.id', $ccID)->get()->all()[0]->district;
    }

    public static function calculateWeekEnd($start_date, $end_date) {
        $weekend = 0;
        while( $start_date <= $end_date) {
            //echo "Start Date = " . $start_date . " ::: End Date = " . $end_date . "\n";

            $day = date("D", strtotime($start_date));

            if ( in_array($day, self::$weekends)) {
                $weekend++;
            }

            $start_date = date('Y-m-d', strtotime($start_date . ' + 1 day'));
        }

        return $weekend;

    }
}