<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('getContributorDetails', array('uses' => 'HomeController@getContributorDetails'));

Route::resource('forwardDairy',    'ForwardDairyController');
Route::get('getAORDetails',        array('uses' => 'ForwardDairyController@getAORDetails'));
Route::any('forwardDairySearch',   array('uses' => 'ForwardDairyController@search'));
Route::any('getSearchResult',      array('uses' => 'ForwardDairyController@getSearchResult'));
Route::any('showCategoryResult',   array('uses' => 'ForwardDairyController@showCategoryResult'));


Route::resource('ffdata',          'FFDataController');
Route::resource('Scholarship',     'ScholarshipController');
Route::resource('Grs',             'GrsController');

Route::get('logoutuser',   'HomeController@logoutuser');

Route::get('exportForwardDairyToPDF', 'ForwardDairyController@exportForwardDairyToPDF');

//Route::get('forwardDairy/search', 'ForwardDairyController@search');






Route::any('generateFFList', array('uses' => 'ForwardDairyController@generateFFList'));
Route::any('generateFFListCSV', array('uses' => 'ForwardDairyController@generateFFListCSV'));




Route::resource('users',           'UserController');
Route::get('/createUNO',       'UserController@createUNO');

/******* AJAX Calls ******/
Route::get('getIndicatorDetails',    'SubIndicatorController@getIndicatorDetails');
Route::get('getMainAreaDetails',     'IndicatorController@getMainAreaDetails');
Route::get('deleteIndicator',        'IndicatorController@deleteIndicator');
Route::get('checkDuplicateCaseNo',    'ForwardDairyController@checkDuplicateCaseNo');
Route::get('deleteCase',             'ForwardDairyController@deleteCase');


Route::get('deleteMainarea',         'MainAreaController@deleteMainarea');
Route::get('deleteMainarea',         'MainAreaController@deleteMainarea');
Route::get('getDistrictList',        'ScholarshipController@getDistrictList');
Route::get('getUpazillaList',        'ScholarshipController@getUpazillaList');
Route::get('getFFDetailsData',       'FFDataController@getFFDetailsData');


Route::resource('notification', 'NotificationController');


/*Route::get('/getDistrictList',            '.\app\Modules\HatBazar\HatBazarController@getDistrictList');
Route::get('/getUpazillaList',            'Aapp\Modules\HatBazar\HatBazarController@getUpazillaList');*/

Route::get('/getDistrictList', function (Request $request) {
    $division         = $request->input('division');
    $district_data    = DB::table('district_tbl')->select('id', 'dis_bn_name')
        ->where(['dis_division_id' => $division])
        ->get()->pluck('dis_bn_name', 'id');

    return response()->json($district_data);
});

Route::get('/getUpazillaList', function (Request $request) {
    $district         = $request->input('district');
    $office_type      = $request->input('office_type');

    if ($office_type == 1) {
        $upazilla_data = DB::table('upazila_tbl')->select('id', 'upa_bn_name')
            ->where(['upa_district_id' => $district])
            ->get()->pluck('upa_bn_name', 'id');

        return response()->json($upazilla_data);
    }
    else if ($office_type == 3){
        $pourosova_data = DB::table('pourosova')->select('id', 'pourosova_name_bn')
            ->where(['district_id' => $district])
            ->get()->pluck('pourosova_name_bn', 'id');

        return response()->json($pourosova_data);
    }


});