$(document).ready(function () {
    TrainingCalendar.initCalendar();

    $.each(training_data, function (key, val){
        var start_date = separateDate(val.start);
        var end_date   = separateDate(val.end);
        val.start =  new Date(start_date[0], start_date[1], start_date[2]);
        val.end   =  new Date(end_date[0], end_date[1], end_date[2]);

        //console.log('key = ' + key + '  Val = ' + val.backgroundColor);
    });
    //training_data.push
});

function separateDate(batch_start_date) {
    return batch_start_date.split('-');
}

var TrainingCalendar = function() {
    return {
        initCalendar: function () {

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#training_calendar').width() <= 400) {
                $('#training_calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#training_calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,year,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,year,month,agendaWeek,agendaDay'
                    };
                }
            }


            $('#training_calendar').fullCalendar('destroy'); // destroy the training_calendar
            $('#training_calendar').fullCalendar({ //re-initialize the training_calendar
                disableDragging: true,
                header: h,
                editable: false,
                events: training_data
            });
        }
    };
}();