$('.confirm_url').click(function(){return confirm("are you sure?");});

$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});

function validateSendForm()
{
    var checkboxs=document.getElementsByName("recipient_to_users[]");
    var okay=false;
    for(var i=0,l=checkboxs.length;i<l;i++)
    {
        if(checkboxs[i].checked)
        {
            okay=true;
            break;
        }
    }
    if(okay) {
    	// return true;
    }
    else {
    	alert("Please select recipient");
    	return false;
    }

	if (okay && confirm("Are you sure?")){
	 	return true;
	} else {
		return false;
	}
  
}

function validate_selectReceivedDemandsToMerge()
{
    var checkboxs=document.getElementsByName("selected_letters[]");
    var okay=false;
    for(var i=0,l=checkboxs.length;i<l;i++)
    {
        if(checkboxs[i].checked)
        {
            okay=true;
            break;
        }
    }
    if(okay) {
    	// return true;
    }
    else {
    	alert("Please select first!");
    	return false;
    }

	if (okay && confirm("Are you sure?")){
	 	return true;
	} else {
		return false;
	}
  
}

function Clickheretoprint(elem) {
        Popup(jQuery(elem).html());
    }

function Popup(data) {
    var mywindow = window.open('', '');
    mywindow.document.write('<html><title>Report</title>');
    mywindow.document.write('<link rel="stylesheet" type="text/css" href="{{ URL::asset("css/bootstrap.min.css") }}">');
    mywindow.document.write('<head></head><body><style type="text/css">');
    mywindow.document.write('table{margin:10px; font-size: 11px}');
    mywindow.document.write('table thead, tr, th, table tbody, tr, td { margin:10px; font-size: 11px;}');
    mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:10px;margin-bottom:0px } </style></body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.print();
}