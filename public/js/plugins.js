/*********************************************************/
/****** 7.datatables.bootstrap.js ************************/
/*********************************************************/

/* Set the defaults for DataTables initialisation */
$.extend(true, $.fn.dataTable.defaults, {
    "dom": "<'row'<'col-md-6 col-sm-6'l><'col-md-6 col-sm-6'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-5'i><'col-md-7 col-sm-7'p>>", // default layout with horizobtal scrollable datatable
    //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // datatable layout without  horizobtal scroll(used when bootstrap dropdowns used in the datatable cells)
    "language": {
        "lengthMenu": " _MENU_ records ",
        "paginate": {
            "previous": 'Prev',
            "next": 'Next',
            "page": "Page",
            "pageOf": "of"
        }
    },
    "pagingType": "bootstrap_number"
});

/* Default class modification */
$.extend($.fn.dataTableExt.oStdClasses, {
    "sWrapper": "dataTables_wrapper",
    "sFilterInput": "form-control input-sm input-inline",
    "sLengthSelect": "form-control input-sm input-xsmall input-inline"
});

// In 1.10 we use the pagination renderers to draw the Bootstrap paging,
// rather than  custom plug-in
$.fn.dataTable.defaults.renderer = 'bootstrap';
$.fn.dataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
    var api = new $.fn.dataTable.Api(settings);
    var classes = settings.oClasses;
    var lang = settings.oLanguage.oPaginate;
    var btnDisplay, btnClass;

    var attach = function (container, buttons) {
        var i, ien, node, button;
        var clickHandler = function (e) {
            e.preventDefault();
            if (e.data.action !== 'ellipsis') {
                api.page(e.data.action).draw(false);
            }
        };

        for (i = 0, ien = buttons.length; i < ien; i++) {
            button = buttons[i];

            if ($.isArray(button)) {
                attach(container, button);
            } else {
                btnDisplay = '';
                btnClass = '';

                switch (button) {
                    case 'ellipsis':
                        btnDisplay = '&hellip;';
                        btnClass = 'disabled';
                        break;

                    case 'first':
                        btnDisplay = lang.sFirst;
                        btnClass = button + (page > 0 ?
                                '' : ' disabled');
                        break;

                    case 'previous':
                        btnDisplay = lang.sPrevious;
                        btnClass = button + (page > 0 ?
                                '' : ' disabled');
                        break;

                    case 'next':
                        btnDisplay = lang.sNext;
                        btnClass = button + (page < pages - 1 ?
                                '' : ' disabled');
                        break;

                    case 'last':
                        btnDisplay = lang.sLast;
                        btnClass = button + (page < pages - 1 ?
                                '' : ' disabled');
                        break;

                    default:
                        btnDisplay = button + 1;
                        btnClass = page === button ?
                            'active' : '';
                        break;
                }

                if (btnDisplay) {
                    node = $('<li>', {
                        'class': classes.sPageButton + ' ' + btnClass,
                        'aria-controls': settings.sTableId,
                        'tabindex': settings.iTabIndex,
                        'id': idx === 0 && typeof button === 'string' ?
                            settings.sTableId + '_' + button : null
                    })
                        .append($('<a>', {
                                'href': '#'
                            })
                                .html(btnDisplay)
                        )
                        .appendTo(container);

                    settings.oApi._fnBindAction(
                        node, {
                            action: button
                        }, clickHandler
                    );
                }
            }
        }
    };

    attach(
        $(host).empty().html('<ul class="pagination"/>').children('ul'),
        buttons
    );
}

/***
 Custom Pagination
 ***/

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* Bootstrap style full number pagination control */
$.extend($.fn.dataTableExt.oPagination, {
    "bootstrap_full_number": {
        "fnInit": function (oSettings, nPaging, fnDraw) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function (e) {
                e.preventDefault();
                if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                    fnDraw(oSettings);
                }
            };

            $(nPaging).append(
                '<ul class="pagination">' +
                '<li class="prev disabled"><a href="#" title="' + oLang.sFirst + '"><i class="fa fa-angle-double-left"></i></a></li>' +
                '<li class="prev disabled"><a href="#" title="' + oLang.sPrevious + '"><i class="fa fa-angle-left"></i></a></li>' +
                '<li class="next disabled"><a href="#" title="' + oLang.sNext + '"><i class="fa fa-angle-right"></i></a></li>' +
                '<li class="next disabled"><a href="#" title="' + oLang.sLast + '"><i class="fa fa-angle-double-right"></i></a></li>' +
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind('click.DT', {
                action: "first"
            }, fnClickHandler);
            $(els[1]).bind('click.DT', {
                action: "previous"
            }, fnClickHandler);
            $(els[2]).bind('click.DT', {
                action: "next"
            }, fnClickHandler);
            $(els[3]).bind('click.DT', {
                action: "last"
            }, fnClickHandler);
        },

        "fnUpdate": function (oSettings, fnDraw) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

            if (oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            } else if (oPaging.iPage <= iHalf) {
                iStart = 1;
                iEnd = iListLength;
            } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }


            for (i = 0, iLen = an.length; i < iLen; i++) {
                if (oPaging.iTotalPages <= 0) {
                    $('.pagination', an[i]).css('visibility', 'hidden');
                } else {
                    $('.pagination', an[i]).css('visibility', 'visible');
                }

                // Remove the middle elements
                $('li:gt(1)', an[i]).filter(':not(.next)').remove();

                // Add the new list items and their event handlers
                for (j = iStart; j <= iEnd; j++) {
                    sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                    $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                        .insertBefore($('li.next:first', an[i])[0])
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                            fnDraw(oSettings);
                        });
                }

                // Add / remove disabled classes from the static elements
                if (oPaging.iPage === 0) {
                    $('li.prev', an[i]).addClass('disabled');
                } else {
                    $('li.prev', an[i]).removeClass('disabled');
                }

                if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                    $('li.next', an[i]).addClass('disabled');
                } else {
                    $('li.next', an[i]).removeClass('disabled');
                }
            }
        }
    }
});

$.extend($.fn.dataTableExt.oPagination, {
    "bootstrap_number": {
        "fnInit": function (oSettings, nPaging, fnDraw) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function (e) {
                e.preventDefault();
                if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                    fnDraw(oSettings);
                }
            };

            $(nPaging).append(
                '<ul class="pagination">' +
                '<li class="prev disabled"><a href="#" title="' + oLang.sPrevious + '"><i class="fa fa-angle-left"></i></a></li>' +
                '<li class="next disabled"><a href="#" title="' + oLang.sNext + '"><i class="fa fa-angle-right"></i></a></li>' +
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind('click.DT', {
                action: "previous"
            }, fnClickHandler);
            $(els[1]).bind('click.DT', {
                action: "next"
            }, fnClickHandler);
        },

        "fnUpdate": function (oSettings, fnDraw) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

            if (oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            } else if (oPaging.iPage <= iHalf) {
                iStart = 1;
                iEnd = iListLength;
            } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for (i = 0, iLen = an.length; i < iLen; i++) {
                if (oPaging.iTotalPages <= 0) {
                    $('.pagination', an[i]).css('visibility', 'hidden');
                } else {
                    $('.pagination', an[i]).css('visibility', 'visible');
                }

                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(.next)').remove();

                // Add the new list items and their event handlers
                for (j = iStart; j <= iEnd; j++) {
                    sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                    $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                        .insertBefore($('li.next:first', an[i])[0])
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                            fnDraw(oSettings);
                        });
                }

                // Add / remove disabled classes from the static elements
                if (oPaging.iPage === 0) {
                    $('li.prev', an[i]).addClass('disabled');
                } else {
                    $('li.prev', an[i]).removeClass('disabled');
                }

                if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                    $('li.next', an[i]).addClass('disabled');
                } else {
                    $('li.next', an[i]).removeClass('disabled');
                }
            }
        }
    }
});


/* Bootstrap style full number pagination control */
$.extend($.fn.dataTableExt.oPagination, {
    "bootstrap_extended": {
        "fnInit": function (oSettings, nPaging, fnDraw) {
            var oLang = oSettings.oLanguage.oPaginate;
            var oPaging = oSettings.oInstance.fnPagingInfo();

            var fnClickHandler = function (e) {
                e.preventDefault();
                if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                    fnDraw(oSettings);
                }
            };

            $(nPaging).append(
                '<div class="pagination-panel"> ' + (oLang.page ? oLang.page : '') + ' ' +
                '<a href="#" class="btn btn-sm default prev disabled"><i class="fa fa-angle-left"></i></a>' +
                '<input type="text" class="pagination-panel-input form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;">' +
                '<a href="#" class="btn btn-sm default next disabled"><i class="fa fa-angle-right"></i></a> ' +
                (oLang.pageOf ? oLang.pageOf + ' <span class="pagination-panel-total"></span>' : '') +
                '</div>'
            );

            var els = $('a', nPaging);

            $(els[0]).bind('click.DT', {
                action: "previous"
            }, fnClickHandler);
            $(els[1]).bind('click.DT', {
                action: "next"
            }, fnClickHandler);

            $('.pagination-panel-input', nPaging).bind('change.DT', function (e) {
                var oPaging = oSettings.oInstance.fnPagingInfo();
                e.preventDefault();
                var page = parseInt($(this).val());
                if (page > 0 && page <= oPaging.iTotalPages) {
                    if (oSettings.oApi._fnPageChange(oSettings, page - 1)) {
                        fnDraw(oSettings);
                    }
                } else {
                    $(this).val(oPaging.iPage + 1);
                }
            });

            $('.pagination-panel-input', nPaging).bind('keypress.DT', function (e) {
                var oPaging = oSettings.oInstance.fnPagingInfo();
                if (e.which == 13) {
                    var page = parseInt($(this).val());
                    if (page > 0 && page <= oSettings.oInstance.fnPagingInfo().iTotalPages) {
                        if (oSettings.oApi._fnPageChange(oSettings, page - 1)) {
                            fnDraw(oSettings);
                        }
                    } else {
                        $(this).val(oPaging.iPage + 1);
                    }
                    e.preventDefault();
                }
            });
        },

        "fnUpdate": function (oSettings, fnDraw) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

            if (oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            } else if (oPaging.iPage <= iHalf) {
                iStart = 1;
                iEnd = iListLength;
            } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for (i = 0, iLen = an.length; i < iLen; i++) {
                var wrapper = $(an[i]).parents(".dataTables_wrapper");

                if (oPaging.iTotal <= 0) {
                    $('.dataTables_paginate, .dataTables_length', wrapper).hide();
                } else {
                    $('.dataTables_paginate, .dataTables_length', wrapper).show();
                }

                if (oPaging.iTotalPages <= 0) {
                    $('.dataTables_paginate, .dataTables_length .seperator', wrapper).hide();
                } else {
                    $('.dataTables_paginate, .dataTables_length .seperator', wrapper).show();
                }

                $('.pagination-panel-total', an[i]).html(oPaging.iTotalPages);
                $('.pagination-panel-input', an[i]).val(oPaging.iPage + 1);

                // Remove the middle elements
                $('li:gt(1)', an[i]).filter(':not(.next)').remove();

                // Add the new list items and their event handlers
                for (j = iStart; j <= iEnd; j++) {
                    sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                    $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                        .insertBefore($('li.next:first', an[i])[0])
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                            fnDraw(oSettings);
                        });
                }

                // Add / remove disabled classes from the static elements
                if (oPaging.iPage === 0) {
                    $('a.prev', an[i]).addClass('disabled');
                } else {
                    $('a.prev', an[i]).removeClass('disabled');
                }

                if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                    $('a.next', an[i]).addClass('disabled');
                } else {
                    $('a.next', an[i]).removeClass('disabled');
                }
            }
        }
    }
});


/* ==========================================================
 *
 * bootstrap-maxlength.js v 1.6.0
 * Copyright 2015 Maurizio Napoleoni @mimonap
 * Licensed under MIT License
 * URL: https://github.com/mimo84/bootstrap-maxlength/blob/master/LICENSE
 *
 * ========================================================== */

!function (a) {
    "use strict";
    a.event.special.destroyed || (a.event.special.destroyed = {
        remove: function (a) {
            a.handler && a.handler()
        }
    }), a.fn.extend({
        maxlength: function (b, c) {
            function d(a) {
                var c = a.val();
                c = b.twoCharLinebreak ? c.replace(/\r(?!\n)|\n(?!\r)/g, "\r\n") : c.replace(new RegExp("\r?\n", "g"), "\n");
                var d = 0;
                return d = b.utf8 ? f(c) : c.length
            }

            function e(a, c) {
                var d = a.val(), e = 0;
                b.twoCharLinebreak && (d = d.replace(/\r(?!\n)|\n(?!\r)/g, "\r\n"), "\n" === d.substr(d.length - 1) && d.length % 2 === 1 && (e = 1)), a.val(d.substr(0, c - e))
            }

            function f(a) {
                for (var b = 0, c = 0; c < a.length; c++) {
                    var d = a.charCodeAt(c);
                    128 > d ? b++ : b += d > 127 && 2048 > d ? 2 : 3
                }
                return b
            }

            function g(a, c, e) {
                var f = !0;
                return !b.alwaysShow && e - d(a) > c && (f = !1), f
            }

            function h(a, b) {
                var c = b - d(a);
                return c
            }

            function i(a, b) {
                b.css({display: "block"}), a.trigger("maxlength.shown")
            }

            function j(a, b) {
                b.css({display: "none"}), a.trigger("maxlength.hidden")
            }

            function k(a, c, d) {
                var e = "";
                return b.message ? e = "function" == typeof b.message ? b.message(a, c) : b.message.replace("%charsTyped%", d).replace("%charsRemaining%", c - d).replace("%charsTotal%", c) : (b.preText && (e += b.preText), e += b.showCharsTyped ? d : c - d, b.showMaxLength && (e += b.separator + c), b.postText && (e += b.postText)), e
            }

            function l(a, c, d, e) {
                e && (e.html(k(c.val(), d, d - a)), a > 0 ? g(c, b.threshold, d) ? i(c, e.removeClass(b.limitReachedClass).addClass(b.warningClass)) : j(c, e) : i(c, e.removeClass(b.warningClass).addClass(b.limitReachedClass))), b.customMaxAttribute && (0 > a ? c.addClass("overmax") : c.removeClass("overmax"))
            }

            function m(b) {
                var c = b[0];
                return a.extend({}, "function" == typeof c.getBoundingClientRect ? c.getBoundingClientRect() : {
                    width: c.offsetWidth,
                    height: c.offsetHeight
                }, b.offset())
            }

            function n(c, d) {
                var e = m(c);
                if ("function" === a.type(b.placement))return void b.placement(c, d, e);
                if (a.isPlainObject(b.placement))return void o(b.placement, d);
                var f = c.outerWidth(), g = d.outerWidth(), h = d.width(), i = d.height();
                switch (b.appendToParent && (e.top -= c.parent().offset().top, e.left -= c.parent().offset().left), b.placement) {
                    case"bottom":
                        d.css({top: e.top + e.height, left: e.left + e.width / 2 - h / 2});
                        break;
                    case"top":
                        d.css({top: e.top - i, left: e.left + e.width / 2 - h / 2});
                        break;
                    case"left":
                        d.css({top: e.top + e.height / 2 - i / 2, left: e.left - h});
                        break;
                    case"right":
                        d.css({top: e.top + e.height / 2 - i / 2, left: e.left + e.width});
                        break;
                    case"bottom-right":
                        d.css({top: e.top + e.height, left: e.left + e.width});
                        break;
                    case"top-right":
                        d.css({top: e.top - i, left: e.left + f});
                        break;
                    case"top-left":
                        d.css({top: e.top - i, left: e.left - g});
                        break;
                    case"bottom-left":
                        d.css({top: e.top + c.outerHeight(), left: e.left - g});
                        break;
                    case"centered-right":
                        d.css({top: e.top + i / 2, left: e.left + f - g - 3});
                        break;
                    case"bottom-right-inside":
                        d.css({top: e.top + e.height, left: e.left + e.width - g});
                        break;
                    case"top-right-inside":
                        d.css({top: e.top - i, left: e.left + f - g});
                        break;
                    case"top-left-inside":
                        d.css({top: e.top - i, left: e.left});
                        break;
                    case"bottom-left-inside":
                        d.css({top: e.top + c.outerHeight(), left: e.left})
                }
            }

            function o(c, d) {
                if (c && d) {
                    var e = ["top", "bottom", "left", "right", "position"], f = {};
                    a.each(e, function (a, c) {
                        var d = b.placement[c];
                        "undefined" != typeof d && (f[c] = d)
                    }), d.css(f)
                }
            }

            function p() {
                return "bottom-right-inside" === b.placement || "top-right-inside" === b.placement || "function" == typeof b.placement || b.message && "function" == typeof b.message
            }

            function q(a) {
                var c = a.attr("maxlength");
                if (b.customMaxAttribute) {
                    var d = a.attr(b.customMaxAttribute);
                    (!c || c > d) && (c = d)
                }
                return c || (c = a.attr("size")), c
            }

            var r = a("body"), s = {
                showOnReady: !1,
                alwaysShow: !1,
                threshold: 10,
                warningClass: "label label-success",
                limitReachedClass: "label label-important label-danger",
                separator: " / ",
                preText: "",
                postText: "",
                showMaxLength: !0,
                placement: "bottom",
                message: null,
                showCharsTyped: !0,
                validate: !1,
                utf8: !1,
                appendToParent: !1,
                twoCharLinebreak: !0,
                customMaxAttribute: null
            };
            return a.isFunction(b) && !c && (c = b, b = {}), b = a.extend(s, b), this.each(function () {
                function c() {
                    var c = k(g.val(), d, "0");
                    d = q(g), f || (f = a('<span class="bootstrap-maxlength"></span>').css({
                        display: "none",
                        position: "absolute",
                        whiteSpace: "nowrap",
                        zIndex: 1099
                    }).html(c)), g.is("textarea") && (g.data("maxlenghtsizex", g.outerWidth()), g.data("maxlenghtsizey", g.outerHeight()), g.mouseup(function () {
                        (g.outerWidth() !== g.data("maxlenghtsizex") || g.outerHeight() !== g.data("maxlenghtsizey")) && n(g, f), g.data("maxlenghtsizex", g.outerWidth()), g.data("maxlenghtsizey", g.outerHeight())
                    })), b.appendToParent ? (g.parent().append(f), g.parent().css("position", "relative")) : r.append(f);
                    var e = h(g, q(g));
                    l(e, g, d, f), n(g, f)
                }

                var d, f, g = a(this);
                a(window).resize(function () {
                    f && n(g, f)
                }), b.showOnReady ? g.ready(function () {
                    c()
                }) : g.focus(function () {
                    c()
                }), g.on("maxlength.reposition", function () {
                    n(g, f)
                }), g.on("destroyed", function () {
                    f && f.remove()
                }), g.on("blur", function () {
                    f && !b.showOnReady && f.remove()
                }), g.on("input", function () {
                    var a = q(g), c = h(g, a), i = !0;
                    return b.validate && 0 > c ? (e(g, a), i = !1) : l(c, g, d, f), p() && n(g, f), i
                })
            })
        }
    })
}(jQuery);


/*!
 * Datepicker for Bootstrap v1.5.1 (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : jQuery)
}(function (a, b) {
    function c() {
        return new Date(Date.UTC.apply(Date, arguments))
    }

    function d() {
        var a = new Date;
        return c(a.getFullYear(), a.getMonth(), a.getDate())
    }

    function e(a, b) {
        return a.getUTCFullYear() === b.getUTCFullYear() && a.getUTCMonth() === b.getUTCMonth() && a.getUTCDate() === b.getUTCDate()
    }

    function f(a) {
        return function () {
            return this[a].apply(this, arguments)
        }
    }

    function g(a) {
        return a && !isNaN(a.getTime())
    }

    function h(b, c) {
        function d(a, b) {
            return b.toLowerCase()
        }

        var e, f = a(b).data(), g = {}, h = new RegExp("^" + c.toLowerCase() + "([A-Z])");
        c = new RegExp("^" + c.toLowerCase());
        for (var i in f)c.test(i) && (e = i.replace(h, d), g[e] = f[i]);
        return g
    }

    function i(b) {
        var c = {};
        if (q[b] || (b = b.split("-")[0], q[b])) {
            var d = q[b];
            return a.each(p, function (a, b) {
                b in d && (c[b] = d[b])
            }), c
        }
    }

    var j = function () {
        var b = {
            get: function (a) {
                return this.slice(a)[0]
            }, contains: function (a) {
                for (var b = a && a.valueOf(), c = 0, d = this.length; d > c; c++)if (this[c].valueOf() === b)return c;
                return -1
            }, remove: function (a) {
                this.splice(a, 1)
            }, replace: function (b) {
                b && (a.isArray(b) || (b = [b]), this.clear(), this.push.apply(this, b))
            }, clear: function () {
                this.length = 0
            }, copy: function () {
                var a = new j;
                return a.replace(this), a
            }
        };
        return function () {
            var c = [];
            return c.push.apply(c, arguments), a.extend(c, b), c
        }
    }(), k = function (b, c) {
        a(b).data("datepicker", this), this._process_options(c), this.dates = new j, this.viewDate = this.o.defaultViewDate, this.focusDate = null, this.element = a(b), this.isInline = !1, this.isInput = this.element.is("input"), this.component = this.element.hasClass("date") ? this.element.find(".add-on, .input-group-addon, .btn") : !1, this.hasInput = this.component && this.element.find("input").length, this.component && 0 === this.component.length && (this.component = !1), this.picker = a(r.template), this._buildEvents(), this._attachEvents(), this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu"), this.o.rtl && this.picker.addClass("datepicker-rtl"), this.viewMode = this.o.startView, this.o.calendarWeeks && this.picker.find("thead .datepicker-title, tfoot .today, tfoot .clear").attr("colspan", function (a, b) {
            return parseInt(b) + 1
        }), this._allow_update = !1, this.setStartDate(this._o.startDate), this.setEndDate(this._o.endDate), this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled), this.setDaysOfWeekHighlighted(this.o.daysOfWeekHighlighted), this.setDatesDisabled(this.o.datesDisabled), this.fillDow(), this.fillMonths(), this._allow_update = !0, this.update(), this.showMode(), this.isInline && this.show()
    };
    k.prototype = {
        constructor: k, _process_options: function (b) {
            this._o = a.extend({}, this._o, b);
            var e = this.o = a.extend({}, this._o), f = e.language;
            switch (q[f] || (f = f.split("-")[0], q[f] || (f = o.language)), e.language = f, e.startView) {
                case 2:
                case"decade":
                    e.startView = 2;
                    break;
                case 1:
                case"year":
                    e.startView = 1;
                    break;
                default:
                    e.startView = 0
            }
            switch (e.minViewMode) {
                case 1:
                case"months":
                    e.minViewMode = 1;
                    break;
                case 2:
                case"years":
                    e.minViewMode = 2;
                    break;
                default:
                    e.minViewMode = 0
            }
            switch (e.maxViewMode) {
                case 0:
                case"days":
                    e.maxViewMode = 0;
                    break;
                case 1:
                case"months":
                    e.maxViewMode = 1;
                    break;
                default:
                    e.maxViewMode = 2
            }
            e.startView = Math.min(e.startView, e.maxViewMode), e.startView = Math.max(e.startView, e.minViewMode), e.multidate !== !0 && (e.multidate = Number(e.multidate) || !1, e.multidate !== !1 && (e.multidate = Math.max(0, e.multidate))), e.multidateSeparator = String(e.multidateSeparator), e.weekStart %= 7, e.weekEnd = (e.weekStart + 6) % 7;
            var g = r.parseFormat(e.format);
            if (e.startDate !== -(1 / 0) && (e.startDate ? e.startDate instanceof Date ? e.startDate = this._local_to_utc(this._zero_time(e.startDate)) : e.startDate = r.parseDate(e.startDate, g, e.language) : e.startDate = -(1 / 0)), e.endDate !== 1 / 0 && (e.endDate ? e.endDate instanceof Date ? e.endDate = this._local_to_utc(this._zero_time(e.endDate)) : e.endDate = r.parseDate(e.endDate, g, e.language) : e.endDate = 1 / 0), e.daysOfWeekDisabled = e.daysOfWeekDisabled || [], a.isArray(e.daysOfWeekDisabled) || (e.daysOfWeekDisabled = e.daysOfWeekDisabled.split(/[,\s]*/)), e.daysOfWeekDisabled = a.map(e.daysOfWeekDisabled, function (a) {
                    return parseInt(a, 10)
                }), e.daysOfWeekHighlighted = e.daysOfWeekHighlighted || [], a.isArray(e.daysOfWeekHighlighted) || (e.daysOfWeekHighlighted = e.daysOfWeekHighlighted.split(/[,\s]*/)), e.daysOfWeekHighlighted = a.map(e.daysOfWeekHighlighted, function (a) {
                    return parseInt(a, 10)
                }), e.datesDisabled = e.datesDisabled || [], !a.isArray(e.datesDisabled)) {
                var h = [];
                h.push(r.parseDate(e.datesDisabled, g, e.language)), e.datesDisabled = h
            }
            e.datesDisabled = a.map(e.datesDisabled, function (a) {
                return r.parseDate(a, g, e.language)
            });
            var i = String(e.orientation).toLowerCase().split(/\s+/g), j = e.orientation.toLowerCase();
            if (i = a.grep(i, function (a) {
                    return /^auto|left|right|top|bottom$/.test(a)
                }), e.orientation = {x: "auto", y: "auto"}, j && "auto" !== j)if (1 === i.length)switch (i[0]) {
                case"top":
                case"bottom":
                    e.orientation.y = i[0];
                    break;
                case"left":
                case"right":
                    e.orientation.x = i[0]
            } else j = a.grep(i, function (a) {
                return /^left|right$/.test(a)
            }), e.orientation.x = j[0] || "auto", j = a.grep(i, function (a) {
                return /^top|bottom$/.test(a)
            }), e.orientation.y = j[0] || "auto"; else;
            if (e.defaultViewDate) {
                var k = e.defaultViewDate.year || (new Date).getFullYear(), l = e.defaultViewDate.month || 0,
                    m = e.defaultViewDate.day || 1;
                e.defaultViewDate = c(k, l, m)
            } else e.defaultViewDate = d()
        }, _events: [], _secondaryEvents: [], _applyEvents: function (a) {
            for (var c, d, e, f = 0; f < a.length; f++)c = a[f][0], 2 === a[f].length ? (d = b, e = a[f][1]) : 3 === a[f].length && (d = a[f][1], e = a[f][2]), c.on(e, d)
        }, _unapplyEvents: function (a) {
            for (var c, d, e, f = 0; f < a.length; f++)c = a[f][0], 2 === a[f].length ? (e = b, d = a[f][1]) : 3 === a[f].length && (e = a[f][1], d = a[f][2]), c.off(d, e)
        }, _buildEvents: function () {
            var b = {
                keyup: a.proxy(function (b) {
                    -1 === a.inArray(b.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update()
                }, this), keydown: a.proxy(this.keydown, this), paste: a.proxy(this.paste, this)
            };
            this.o.showOnFocus === !0 && (b.focus = a.proxy(this.show, this)), this.isInput ? this._events = [[this.element, b]] : this.component && this.hasInput ? this._events = [[this.element.find("input"), b], [this.component, {click: a.proxy(this.show, this)}]] : this.element.is("div") ? this.isInline = !0 : this._events = [[this.element, {click: a.proxy(this.show, this)}]], this._events.push([this.element, "*", {
                blur: a.proxy(function (a) {
                    this._focused_from = a.target
                }, this)
            }], [this.element, {
                blur: a.proxy(function (a) {
                    this._focused_from = a.target
                }, this)
            }]), this.o.immediateUpdates && this._events.push([this.element, {
                "changeYear changeMonth": a.proxy(function (a) {
                    this.update(a.date)
                }, this)
            }]), this._secondaryEvents = [[this.picker, {click: a.proxy(this.click, this)}], [a(window), {resize: a.proxy(this.place, this)}], [a(document), {
                mousedown: a.proxy(function (a) {
                    this.element.is(a.target) || this.element.find(a.target).length || this.picker.is(a.target) || this.picker.find(a.target).length || this.picker.hasClass("datepicker-inline") || this.hide()
                }, this)
            }]]
        }, _attachEvents: function () {
            this._detachEvents(), this._applyEvents(this._events)
        }, _detachEvents: function () {
            this._unapplyEvents(this._events)
        }, _attachSecondaryEvents: function () {
            this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents)
        }, _detachSecondaryEvents: function () {
            this._unapplyEvents(this._secondaryEvents)
        }, _trigger: function (b, c) {
            var d = c || this.dates.get(-1), e = this._utc_to_local(d);
            this.element.trigger({
                type: b,
                date: e,
                dates: a.map(this.dates, this._utc_to_local),
                format: a.proxy(function (a, b) {
                    0 === arguments.length ? (a = this.dates.length - 1, b = this.o.format) : "string" == typeof a && (b = a, a = this.dates.length - 1), b = b || this.o.format;
                    var c = this.dates.get(a);
                    return r.formatDate(c, b, this.o.language)
                }, this)
            })
        }, show: function () {
            var b = this.component ? this.element.find("input") : this.element;
            if (!b.attr("readonly") || this.o.enableOnReadonly !== !1)return this.isInline || this.picker.appendTo(this.o.container), this.place(), this.picker.show(), this._attachSecondaryEvents(), this._trigger("show"), (window.navigator.msMaxTouchPoints || "ontouchstart" in document) && this.o.disableTouchKeyboard && a(this.element).blur(), this
        }, hide: function () {
            return this.isInline ? this : this.picker.is(":visible") ? (this.focusDate = null, this.picker.hide().detach(), this._detachSecondaryEvents(), this.viewMode = this.o.startView, this.showMode(), this.o.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this._trigger("hide"), this) : this
        }, remove: function () {
            return this.hide(), this._detachEvents(), this._detachSecondaryEvents(), this.picker.remove(), delete this.element.data().datepicker, this.isInput || delete this.element.data().date, this
        }, paste: function (b) {
            var c;
            if (b.originalEvent.clipboardData && b.originalEvent.clipboardData.types && -1 !== a.inArray("text/plain", b.originalEvent.clipboardData.types)) c = b.originalEvent.clipboardData.getData("text/plain"); else {
                if (!window.clipboardData)return;
                c = window.clipboardData.getData("Text")
            }
            this.setDate(c), this.update(), b.preventDefault()
        }, _utc_to_local: function (a) {
            return a && new Date(a.getTime() + 6e4 * a.getTimezoneOffset())
        }, _local_to_utc: function (a) {
            return a && new Date(a.getTime() - 6e4 * a.getTimezoneOffset())
        }, _zero_time: function (a) {
            return a && new Date(a.getFullYear(), a.getMonth(), a.getDate())
        }, _zero_utc_time: function (a) {
            return a && new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()))
        }, getDates: function () {
            return a.map(this.dates, this._utc_to_local)
        }, getUTCDates: function () {
            return a.map(this.dates, function (a) {
                return new Date(a)
            })
        }, getDate: function () {
            return this._utc_to_local(this.getUTCDate())
        }, getUTCDate: function () {
            var a = this.dates.get(-1);
            return "undefined" != typeof a ? new Date(a) : null
        }, clearDates: function () {
            var a;
            this.isInput ? a = this.element : this.component && (a = this.element.find("input")), a && a.val(""), this.update(), this._trigger("changeDate"), this.o.autoclose && this.hide()
        }, setDates: function () {
            var b = a.isArray(arguments[0]) ? arguments[0] : arguments;
            return this.update.apply(this, b), this._trigger("changeDate"), this.setValue(), this
        }, setUTCDates: function () {
            var b = a.isArray(arguments[0]) ? arguments[0] : arguments;
            return this.update.apply(this, a.map(b, this._utc_to_local)), this._trigger("changeDate"), this.setValue(), this
        }, setDate: f("setDates"), setUTCDate: f("setUTCDates"), setValue: function () {
            var a = this.getFormattedDate();
            return this.isInput ? this.element.val(a) : this.component && this.element.find("input").val(a), this
        }, getFormattedDate: function (c) {
            c === b && (c = this.o.format);
            var d = this.o.language;
            return a.map(this.dates, function (a) {
                return r.formatDate(a, c, d)
            }).join(this.o.multidateSeparator)
        }, setStartDate: function (a) {
            return this._process_options({startDate: a}), this.update(), this.updateNavArrows(), this
        }, setEndDate: function (a) {
            return this._process_options({endDate: a}), this.update(), this.updateNavArrows(), this
        }, setDaysOfWeekDisabled: function (a) {
            return this._process_options({daysOfWeekDisabled: a}), this.update(), this.updateNavArrows(), this
        }, setDaysOfWeekHighlighted: function (a) {
            return this._process_options({daysOfWeekHighlighted: a}), this.update(), this
        }, setDatesDisabled: function (a) {
            this._process_options({datesDisabled: a}), this.update(), this.updateNavArrows()
        }, place: function () {
            if (this.isInline)return this;
            var b = this.picker.outerWidth(), c = this.picker.outerHeight(), d = 10, e = a(this.o.container),
                f = e.width(), g = "body" === this.o.container ? a(document).scrollTop() : e.scrollTop(),
                h = e.offset(), i = [];
            this.element.parents().each(function () {
                var b = a(this).css("z-index");
                "auto" !== b && 0 !== b && i.push(parseInt(b))
            });
            var j = Math.max.apply(Math, i) + this.o.zIndexOffset,
                k = this.component ? this.component.parent().offset() : this.element.offset(),
                l = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1),
                m = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1), n = k.left - h.left,
                o = k.top - h.top;
            "body" !== this.o.container && (o += g), this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"), "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (n -= b - m)) : k.left < 0 ? (this.picker.addClass("datepicker-orient-left"), n -= k.left - d) : n + b > f ? (this.picker.addClass("datepicker-orient-right"), n += m - b) : this.picker.addClass("datepicker-orient-left");
            var p, q = this.o.orientation.y;
            if ("auto" === q && (p = -g + o - c, q = 0 > p ? "bottom" : "top"), this.picker.addClass("datepicker-orient-" + q), "top" === q ? o -= c + parseInt(this.picker.css("padding-top")) : o += l, this.o.rtl) {
                var r = f - (n + m);
                this.picker.css({top: o, right: r, zIndex: j})
            } else this.picker.css({top: o, left: n, zIndex: j});
            return this
        }, _allow_update: !0, update: function () {
            if (!this._allow_update)return this;
            var b = this.dates.copy(), c = [], d = !1;
            return arguments.length ? (a.each(arguments, a.proxy(function (a, b) {
                b instanceof Date && (b = this._local_to_utc(b)), c.push(b)
            }, this)), d = !0) : (c = this.isInput ? this.element.val() : this.element.data("date") || this.element.find("input").val(), c = c && this.o.multidate ? c.split(this.o.multidateSeparator) : [c], delete this.element.data().date), c = a.map(c, a.proxy(function (a) {
                return r.parseDate(a, this.o.format, this.o.language)
            }, this)), c = a.grep(c, a.proxy(function (a) {
                return !this.dateWithinRange(a) || !a
            }, this), !0), this.dates.replace(c), this.dates.length ? this.viewDate = new Date(this.dates.get(-1)) : this.viewDate < this.o.startDate ? this.viewDate = new Date(this.o.startDate) : this.viewDate > this.o.endDate ? this.viewDate = new Date(this.o.endDate) : this.viewDate = this.o.defaultViewDate, d ? this.setValue() : c.length && String(b) !== String(this.dates) && this._trigger("changeDate"), !this.dates.length && b.length && this._trigger("clearDate"), this.fill(), this.element.change(), this
        }, fillDow: function () {
            var a = this.o.weekStart, b = "<tr>";
            for (this.o.calendarWeeks && (this.picker.find(".datepicker-days .datepicker-switch").attr("colspan", function (a, b) {
                return parseInt(b) + 1
            }), b += '<th class="cw">&#160;</th>'); a < this.o.weekStart + 7;)b += '<th class="dow">' + q[this.o.language].daysMin[a++ % 7] + "</th>";
            b += "</tr>", this.picker.find(".datepicker-days thead").append(b)
        }, fillMonths: function () {
            for (var a = "", b = 0; 12 > b;)a += '<span class="month">' + q[this.o.language].monthsShort[b++] + "</span>";
            this.picker.find(".datepicker-months td").html(a)
        }, setRange: function (b) {
            b && b.length ? this.range = a.map(b, function (a) {
                return a.valueOf()
            }) : delete this.range, this.fill()
        }, getClassNames: function (b) {
            var c = [], d = this.viewDate.getUTCFullYear(), e = this.viewDate.getUTCMonth(), f = new Date;
            return b.getUTCFullYear() < d || b.getUTCFullYear() === d && b.getUTCMonth() < e ? c.push("old") : (b.getUTCFullYear() > d || b.getUTCFullYear() === d && b.getUTCMonth() > e) && c.push("new"), this.focusDate && b.valueOf() === this.focusDate.valueOf() && c.push("focused"), this.o.todayHighlight && b.getUTCFullYear() === f.getFullYear() && b.getUTCMonth() === f.getMonth() && b.getUTCDate() === f.getDate() && c.push("today"), -1 !== this.dates.contains(b) && c.push("active"), (!this.dateWithinRange(b) || this.dateIsDisabled(b)) && c.push("disabled"), -1 !== a.inArray(b.getUTCDay(), this.o.daysOfWeekHighlighted) && c.push("highlighted"), this.range && (b > this.range[0] && b < this.range[this.range.length - 1] && c.push("range"), -1 !== a.inArray(b.valueOf(), this.range) && c.push("selected"), b.valueOf() === this.range[0] && c.push("range-start"), b.valueOf() === this.range[this.range.length - 1] && c.push("range-end")), c
        }, fill: function () {
            var d, e = new Date(this.viewDate), f = e.getUTCFullYear(), g = e.getUTCMonth(),
                h = this.o.startDate !== -(1 / 0) ? this.o.startDate.getUTCFullYear() : -(1 / 0),
                i = this.o.startDate !== -(1 / 0) ? this.o.startDate.getUTCMonth() : -(1 / 0),
                j = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
                k = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
                l = q[this.o.language].today || q.en.today || "", m = q[this.o.language].clear || q.en.clear || "",
                n = q[this.o.language].titleFormat || q.en.titleFormat;
            if (!isNaN(f) && !isNaN(g)) {
                this.picker.find(".datepicker-days thead .datepicker-switch").text(r.formatDate(new c(f, g), n, this.o.language)), this.picker.find("tfoot .today").text(l).toggle(this.o.todayBtn !== !1), this.picker.find("tfoot .clear").text(m).toggle(this.o.clearBtn !== !1), this.picker.find("thead .datepicker-title").text(this.o.title).toggle("" !== this.o.title), this.updateNavArrows(), this.fillMonths();
                var o = c(f, g - 1, 28), p = r.getDaysInMonth(o.getUTCFullYear(), o.getUTCMonth());
                o.setUTCDate(p), o.setUTCDate(p - (o.getUTCDay() - this.o.weekStart + 7) % 7);
                var s = new Date(o);
                o.getUTCFullYear() < 100 && s.setUTCFullYear(o.getUTCFullYear()), s.setUTCDate(s.getUTCDate() + 42), s = s.valueOf();
                for (var t, u = []; o.valueOf() < s;) {
                    if (o.getUTCDay() === this.o.weekStart && (u.push("<tr>"), this.o.calendarWeeks)) {
                        var v = new Date(+o + (this.o.weekStart - o.getUTCDay() - 7) % 7 * 864e5),
                            w = new Date(Number(v) + (11 - v.getUTCDay()) % 7 * 864e5),
                            x = new Date(Number(x = c(w.getUTCFullYear(), 0, 1)) + (11 - x.getUTCDay()) % 7 * 864e5),
                            y = (w - x) / 864e5 / 7 + 1;
                        u.push('<td class="cw">' + y + "</td>")
                    }
                    if (t = this.getClassNames(o), t.push("day"), this.o.beforeShowDay !== a.noop) {
                        var z = this.o.beforeShowDay(this._utc_to_local(o));
                        z === b ? z = {} : "boolean" == typeof z ? z = {enabled: z} : "string" == typeof z && (z = {classes: z}), z.enabled === !1 && t.push("disabled"), z.classes && (t = t.concat(z.classes.split(/\s+/))), z.tooltip && (d = z.tooltip)
                    }
                    t = a.unique(t), u.push('<td class="' + t.join(" ") + '"' + (d ? ' title="' + d + '"' : "") + ">" + o.getUTCDate() + "</td>"), d = null, o.getUTCDay() === this.o.weekEnd && u.push("</tr>"), o.setUTCDate(o.getUTCDate() + 1)
                }
                this.picker.find(".datepicker-days tbody").empty().append(u.join(""));
                var A = q[this.o.language].monthsTitle || q.en.monthsTitle || "Months",
                    B = this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode < 2 ? A : f).end().find("span").removeClass("active");
                if (a.each(this.dates, function (a, b) {
                        b.getUTCFullYear() === f && B.eq(b.getUTCMonth()).addClass("active")
                    }), (h > f || f > j) && B.addClass("disabled"), f === h && B.slice(0, i).addClass("disabled"), f === j && B.slice(k + 1).addClass("disabled"), this.o.beforeShowMonth !== a.noop) {
                    var C = this;
                    a.each(B, function (b, c) {
                        if (!a(c).hasClass("disabled")) {
                            var d = new Date(f, b, 1), e = C.o.beforeShowMonth(d);
                            e === !1 && a(c).addClass("disabled")
                        }
                    })
                }
                u = "", f = 10 * parseInt(f / 10, 10);
                var D = this.picker.find(".datepicker-years").find(".datepicker-switch").text(f + "-" + (f + 9)).end().find("td");
                f -= 1;
                for (var E, F = a.map(this.dates, function (a) {
                    return a.getUTCFullYear()
                }), G = -1; 11 > G; G++) {
                    if (E = ["year"], d = null, -1 === G ? E.push("old") : 10 === G && E.push("new"), -1 !== a.inArray(f, F) && E.push("active"), (h > f || f > j) && E.push("disabled"), this.o.beforeShowYear !== a.noop) {
                        var H = this.o.beforeShowYear(new Date(f, 0, 1));
                        H === b ? H = {} : "boolean" == typeof H ? H = {enabled: H} : "string" == typeof H && (H = {classes: H}), H.enabled === !1 && E.push("disabled"), H.classes && (E = E.concat(H.classes.split(/\s+/))), H.tooltip && (d = H.tooltip)
                    }
                    u += '<span class="' + E.join(" ") + '"' + (d ? ' title="' + d + '"' : "") + ">" + f + "</span>", f += 1
                }
                D.html(u)
            }
        }, updateNavArrows: function () {
            if (this._allow_update) {
                var a = new Date(this.viewDate), b = a.getUTCFullYear(), c = a.getUTCMonth();
                switch (this.viewMode) {
                    case 0:
                        this.o.startDate !== -(1 / 0) && b <= this.o.startDate.getUTCFullYear() && c <= this.o.startDate.getUTCMonth() ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), this.o.endDate !== 1 / 0 && b >= this.o.endDate.getUTCFullYear() && c >= this.o.endDate.getUTCMonth() ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"});
                        break;
                    case 1:
                    case 2:
                        this.o.startDate !== -(1 / 0) && b <= this.o.startDate.getUTCFullYear() || this.o.maxViewMode < 2 ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), this.o.endDate !== 1 / 0 && b >= this.o.endDate.getUTCFullYear() || this.o.maxViewMode < 2 ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"})
                }
            }
        }, click: function (b) {
            b.preventDefault(), b.stopPropagation();
            var e, f, g, h = a(b.target).closest("span, td, th");
            if (1 === h.length)switch (h[0].nodeName.toLowerCase()) {
                case"th":
                    switch (h[0].className) {
                        case"datepicker-switch":
                            this.showMode(1);
                            break;
                        case"prev":
                        case"next":
                            var i = r.modes[this.viewMode].navStep * ("prev" === h[0].className ? -1 : 1);
                            switch (this.viewMode) {
                                case 0:
                                    this.viewDate = this.moveMonth(this.viewDate, i), this._trigger("changeMonth", this.viewDate);
                                    break;
                                case 1:
                                case 2:
                                    this.viewDate = this.moveYear(this.viewDate, i), 1 === this.viewMode && this._trigger("changeYear", this.viewDate)
                            }
                            this.fill();
                            break;
                        case"today":
                            this.showMode(-2);
                            var j = "linked" === this.o.todayBtn ? null : "view";
                            this._setDate(d(), j);
                            break;
                        case"clear":
                            this.clearDates()
                    }
                    break;
                case"span":
                    h.hasClass("disabled") || (this.viewDate.setUTCDate(1), h.hasClass("month") ? (g = 1, f = h.parent().find("span").index(h), e = this.viewDate.getUTCFullYear(), this.viewDate.setUTCMonth(f), this._trigger("changeMonth", this.viewDate), 1 === this.o.minViewMode ? (this._setDate(c(e, f, g)), this.showMode()) : this.showMode(-1)) : (g = 1, f = 0, e = parseInt(h.text(), 10) || 0, this.viewDate.setUTCFullYear(e), this._trigger("changeYear", this.viewDate), 2 === this.o.minViewMode && this._setDate(c(e, f, g)), this.showMode(-1)), this.fill());
                    break;
                case"td":
                    h.hasClass("day") && !h.hasClass("disabled") && (g = parseInt(h.text(), 10) || 1, e = this.viewDate.getUTCFullYear(), f = this.viewDate.getUTCMonth(), h.hasClass("old") ? 0 === f ? (f = 11, e -= 1) : f -= 1 : h.hasClass("new") && (11 === f ? (f = 0, e += 1) : f += 1), this._setDate(c(e, f, g)))
            }
            this.picker.is(":visible") && this._focused_from && a(this._focused_from).focus(), delete this._focused_from
        }, _toggle_multidate: function (a) {
            var b = this.dates.contains(a);
            if (a || this.dates.clear(), -1 !== b ? (this.o.multidate === !0 || this.o.multidate > 1 || this.o.toggleActive) && this.dates.remove(b) : this.o.multidate === !1 ? (this.dates.clear(), this.dates.push(a)) : this.dates.push(a), "number" == typeof this.o.multidate)for (; this.dates.length > this.o.multidate;)this.dates.remove(0)
        }, _setDate: function (a, b) {
            b && "date" !== b || this._toggle_multidate(a && new Date(a)), b && "view" !== b || (this.viewDate = a && new Date(a)), this.fill(), this.setValue(), b && "view" === b || this._trigger("changeDate");
            var c;
            this.isInput ? c = this.element : this.component && (c = this.element.find("input")), c && c.change(), !this.o.autoclose || b && "date" !== b || this.hide()
        }, moveDay: function (a, b) {
            var c = new Date(a);
            return c.setUTCDate(a.getUTCDate() + b), c
        }, moveWeek: function (a, b) {
            return this.moveDay(a, 7 * b)
        }, moveMonth: function (a, b) {
            if (!g(a))return this.o.defaultViewDate;
            if (!b)return a;
            var c, d, e = new Date(a.valueOf()), f = e.getUTCDate(), h = e.getUTCMonth(), i = Math.abs(b);
            if (b = b > 0 ? 1 : -1, 1 === i) d = -1 === b ? function () {
                return e.getUTCMonth() === h
            } : function () {
                return e.getUTCMonth() !== c
            }, c = h + b, e.setUTCMonth(c), (0 > c || c > 11) && (c = (c + 12) % 12); else {
                for (var j = 0; i > j; j++)e = this.moveMonth(e, b);
                c = e.getUTCMonth(), e.setUTCDate(f), d = function () {
                    return c !== e.getUTCMonth()
                }
            }
            for (; d();)e.setUTCDate(--f), e.setUTCMonth(c);
            return e
        }, moveYear: function (a, b) {
            return this.moveMonth(a, 12 * b)
        }, moveAvailableDate: function (a, b, c) {
            do {
                if (a = this[c](a, b), !this.dateWithinRange(a))return !1;
                c = "moveDay"
            } while (this.dateIsDisabled(a));
            return a
        }, weekOfDateIsDisabled: function (b) {
            return -1 !== a.inArray(b.getUTCDay(), this.o.daysOfWeekDisabled)
        }, dateIsDisabled: function (b) {
            return this.weekOfDateIsDisabled(b) || a.grep(this.o.datesDisabled, function (a) {
                    return e(b, a)
                }).length > 0
        }, dateWithinRange: function (a) {
            return a >= this.o.startDate && a <= this.o.endDate
        }, keydown: function (a) {
            if (!this.picker.is(":visible"))return void((40 === a.keyCode || 27 === a.keyCode) && (this.show(), a.stopPropagation()));
            var b, c, d = !1, e = this.focusDate || this.viewDate;
            switch (a.keyCode) {
                case 27:
                    this.focusDate ? (this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill()) : this.hide(), a.preventDefault(), a.stopPropagation();
                    break;
                case 37:
                case 38:
                case 39:
                case 40:
                    if (!this.o.keyboardNavigation || 7 === this.o.daysOfWeekDisabled.length)break;
                    b = 37 === a.keyCode || 38 === a.keyCode ? -1 : 1, a.ctrlKey ? (c = this.moveAvailableDate(e, b, "moveYear"), c && this._trigger("changeYear", this.viewDate)) : a.shiftKey ? (c = this.moveAvailableDate(e, b, "moveMonth"), c && this._trigger("changeMonth", this.viewDate)) : 37 === a.keyCode || 39 === a.keyCode ? c = this.moveAvailableDate(e, b, "moveDay") : this.weekOfDateIsDisabled(e) || (c = this.moveAvailableDate(e, b, "moveWeek")), c && (this.focusDate = this.viewDate = c, this.setValue(), this.fill(), a.preventDefault());
                    break;
                case 13:
                    if (!this.o.forceParse)break;
                    e = this.focusDate || this.dates.get(-1) || this.viewDate, this.o.keyboardNavigation && (this._toggle_multidate(e), d = !0), this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.setValue(), this.fill(), this.picker.is(":visible") && (a.preventDefault(), a.stopPropagation(), this.o.autoclose && this.hide());
                    break;
                case 9:
                    this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill(), this.hide()
            }
            if (d) {
                this.dates.length ? this._trigger("changeDate") : this._trigger("clearDate");
                var f;
                this.isInput ? f = this.element : this.component && (f = this.element.find("input")), f && f.change()
            }
        }, showMode: function (a) {
            a && (this.viewMode = Math.max(this.o.minViewMode, Math.min(this.o.maxViewMode, this.viewMode + a))), this.picker.children("div").hide().filter(".datepicker-" + r.modes[this.viewMode].clsName).show(), this.updateNavArrows()
        }
    };
    var l = function (b, c) {
        a(b).data("datepicker", this), this.element = a(b), this.inputs = a.map(c.inputs, function (a) {
            return a.jquery ? a[0] : a
        }), delete c.inputs, n.call(a(this.inputs), c).on("changeDate", a.proxy(this.dateUpdated, this)), this.pickers = a.map(this.inputs, function (b) {
            return a(b).data("datepicker")
        }), this.updateDates()
    };
    l.prototype = {
        updateDates: function () {
            this.dates = a.map(this.pickers, function (a) {
                return a.getUTCDate()
            }), this.updateRanges()
        }, updateRanges: function () {
            var b = a.map(this.dates, function (a) {
                return a.valueOf()
            });
            a.each(this.pickers, function (a, c) {
                c.setRange(b)
            })
        }, dateUpdated: function (b) {
            if (!this.updating) {
                this.updating = !0;
                var c = a(b.target).data("datepicker");
                if ("undefined" != typeof c) {
                    var d = c.getUTCDate(), e = a.inArray(b.target, this.inputs), f = e - 1, g = e + 1,
                        h = this.inputs.length;
                    if (-1 !== e) {
                        if (a.each(this.pickers, function (a, b) {
                                b.getUTCDate() || b.setUTCDate(d)
                            }), d < this.dates[f])for (; f >= 0 && d < this.dates[f];)this.pickers[f--].setUTCDate(d); else if (d > this.dates[g])for (; h > g && d > this.dates[g];)this.pickers[g++].setUTCDate(d);
                        this.updateDates(), delete this.updating
                    }
                }
            }
        }, remove: function () {
            a.map(this.pickers, function (a) {
                a.remove()
            }), delete this.element.data().datepicker
        }
    };
    var m = a.fn.datepicker, n = function (c) {
        var d = Array.apply(null, arguments);
        d.shift();
        var e;
        if (this.each(function () {
                var b = a(this), f = b.data("datepicker"), g = "object" == typeof c && c;
                if (!f) {
                    var j = h(this, "date"), m = a.extend({}, o, j, g), n = i(m.language), p = a.extend({}, o, n, j, g);
                    b.hasClass("input-daterange") || p.inputs ? (a.extend(p, {inputs: p.inputs || b.find("input").toArray()}), f = new l(this, p)) : f = new k(this, p), b.data("datepicker", f)
                }
                "string" == typeof c && "function" == typeof f[c] && (e = f[c].apply(f, d))
            }), e === b || e instanceof k || e instanceof l)return this;
        if (this.length > 1)throw new Error("Using only allowed for the collection of a single element (" + c + " function)");
        return e
    };
    a.fn.datepicker = n;
    var o = a.fn.datepicker.defaults = {
        autoclose: !1,
        beforeShowDay: a.noop,
        beforeShowMonth: a.noop,
        beforeShowYear: a.noop,
        calendarWeeks: !1,
        clearBtn: !1,
        toggleActive: !1,
        daysOfWeekDisabled: [],
        daysOfWeekHighlighted: [],
        datesDisabled: [],
        endDate: 1 / 0,
        forceParse: !0,
        format: "mm/dd/yyyy",
        keyboardNavigation: !0,
        language: "en",
        minViewMode: 0,
        maxViewMode: 2,
        multidate: !1,
        multidateSeparator: ",",
        orientation: "auto",
        rtl: !1,
        startDate: -(1 / 0),
        startView: 0,
        todayBtn: !1,
        todayHighlight: !1,
        weekStart: 0,
        disableTouchKeyboard: !1,
        enableOnReadonly: !0,
        showOnFocus: !0,
        zIndexOffset: 10,
        container: "body",
        immediateUpdates: !1,
        title: ""
    }, p = a.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"];
    a.fn.datepicker.Constructor = k;
    var q = a.fn.datepicker.dates = {
        en: {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today",
            clear: "Clear",
            titleFormat: "MM yyyy"
        }
    }, r = {
        modes: [{clsName: "days", navFnc: "Month", navStep: 1}, {
            clsName: "months",
            navFnc: "FullYear",
            navStep: 1
        }, {clsName: "years", navFnc: "FullYear", navStep: 10}],
        isLeapYear: function (a) {
            return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
        },
        getDaysInMonth: function (a, b) {
            return [31, r.isLeapYear(a) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][b]
        },
        validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
        nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,
        parseFormat: function (a) {
            if ("function" == typeof a.toValue && "function" == typeof a.toDisplay)return a;
            var b = a.replace(this.validParts, "\x00").split("\x00"), c = a.match(this.validParts);
            if (!b || !b.length || !c || 0 === c.length)throw new Error("Invalid date format.");
            return {separators: b, parts: c}
        },
        parseDate: function (e, f, g) {
            function h() {
                var a = this.slice(0, o[l].length), b = o[l].slice(0, a.length);
                return a.toLowerCase() === b.toLowerCase()
            }

            if (!e)return b;
            if (e instanceof Date)return e;
            if ("string" == typeof f && (f = r.parseFormat(f)), f.toValue)return f.toValue(e, f, g);
            var i, j, l, m, n = /([\-+]\d+)([dmwy])/, o = e.match(/([\-+]\d+)([dmwy])/g),
                p = {d: "moveDay", m: "moveMonth", w: "moveWeek", y: "moveYear"};
            if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(e)) {
                for (e = new Date, l = 0; l < o.length; l++)i = n.exec(o[l]), j = parseInt(i[1]), m = p[i[2]], e = k.prototype[m](e, j);
                return c(e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate())
            }
            o = e && e.match(this.nonpunctuation) || [], e = new Date;
            var s, t, u = {}, v = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"], w = {
                yyyy: function (a, b) {
                    return a.setUTCFullYear(b)
                }, yy: function (a, b) {
                    return a.setUTCFullYear(2e3 + b)
                }, m: function (a, b) {
                    if (isNaN(a))return a;
                    for (b -= 1; 0 > b;)b += 12;
                    for (b %= 12, a.setUTCMonth(b); a.getUTCMonth() !== b;)a.setUTCDate(a.getUTCDate() - 1);
                    return a
                }, d: function (a, b) {
                    return a.setUTCDate(b)
                }
            };
            w.M = w.MM = w.mm = w.m, w.dd = w.d, e = d();
            var x = f.parts.slice();
            if (o.length !== x.length && (x = a(x).filter(function (b, c) {
                    return -1 !== a.inArray(c, v)
                }).toArray()), o.length === x.length) {
                var y;
                for (l = 0, y = x.length; y > l; l++) {
                    if (s = parseInt(o[l], 10), i = x[l], isNaN(s))switch (i) {
                        case"MM":
                            t = a(q[g].months).filter(h), s = a.inArray(t[0], q[g].months) + 1;
                            break;
                        case"M":
                            t = a(q[g].monthsShort).filter(h), s = a.inArray(t[0], q[g].monthsShort) + 1
                    }
                    u[i] = s
                }
                var z, A;
                for (l = 0; l < v.length; l++)A = v[l], A in u && !isNaN(u[A]) && (z = new Date(e), w[A](z, u[A]), isNaN(z) || (e = z))
            }
            return e
        },
        formatDate: function (b, c, d) {
            if (!b)return "";
            if ("string" == typeof c && (c = r.parseFormat(c)), c.toDisplay)return c.toDisplay(b, c, d);
            var e = {
                d: b.getUTCDate(),
                D: q[d].daysShort[b.getUTCDay()],
                DD: q[d].days[b.getUTCDay()],
                m: b.getUTCMonth() + 1,
                M: q[d].monthsShort[b.getUTCMonth()],
                MM: q[d].months[b.getUTCMonth()],
                yy: b.getUTCFullYear().toString().substring(2),
                yyyy: b.getUTCFullYear()
            };
            e.dd = (e.d < 10 ? "0" : "") + e.d, e.mm = (e.m < 10 ? "0" : "") + e.m, b = [];
            for (var f = a.extend([], c.separators), g = 0, h = c.parts.length; h >= g; g++)f.length && b.push(f.shift()), b.push(e[c.parts[g]]);
            return b.join("")
        },
        headTemplate: '<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev"><i class="fa fa-angle-left"></i></th><th colspan="5" class="datepicker-switch"></th><th class="next"><i class="fa fa-angle-right"></i></th></tr></thead>',
        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
        footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'
    };
    r.template = '<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">' + r.headTemplate + "<tbody></tbody>" + r.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + r.headTemplate + r.contTemplate + r.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + r.headTemplate + r.contTemplate + r.footTemplate + "</table></div></div>", a.fn.datepicker.DPGlobal = r, a.fn.datepicker.noConflict = function () {
        return a.fn.datepicker = m, this
    }, a.fn.datepicker.version = "1.5.1", a(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function (b) {
        var c = a(this);
        c.data("datepicker") || (b.preventDefault(), n.call(c, "show"))
    }), a(function () {
        n.call(a('[data-provide="datepicker-inline"]'))
    })
});


/*! iCheck v1.0.2 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function (f) {
    function A(a, b, d) {
        var c = a[0], g = /er/.test(d) ? _indeterminate : /bl/.test(d) ? n : k, e = d == _update ? {
            checked: c[k],
            disabled: c[n],
            indeterminate: "true" == a.attr(_indeterminate) || "false" == a.attr(_determinate)
        } : c[g];
        if (/^(ch|di|in)/.test(d) && !e) x(a, g); else if (/^(un|en|de)/.test(d) && e) q(a, g); else if (d == _update)for (var f in e)e[f] ? x(a, f, !0) : q(a, f, !0); else if (!b || "toggle" == d) {
            if (!b) a[_callback]("ifClicked");
            e ? c[_type] !== r && q(a, g) : x(a, g)
        }
    }

    function x(a, b, d) {
        var c = a[0], g = a.parent(), e = b == k, u = b == _indeterminate,
            v = b == n, s = u ? _determinate : e ? y : "enabled", F = l(a, s + t(c[_type])), B = l(a, b + t(c[_type]));
        if (!0 !== c[b]) {
            if (!d && b == k && c[_type] == r && c.name) {
                var w = a.closest("form"), p = 'input[name="' + c.name + '"]', p = w.length ? w.find(p) : f(p);
                p.each(function () {
                    this !== c && f(this).data(m) && q(f(this), b)
                })
            }
            u ? (c[b] = !0, c[k] && q(a, k, "force")) : (d || (c[b] = !0), e && c[_indeterminate] && q(a, _indeterminate, !1));
            D(a, e, b, d)
        }
        c[n] && l(a, _cursor, !0) && g.find("." + C).css(_cursor, "default");
        g[_add](B || l(a, b) || "");
        g.attr("role") && !u && g.attr("aria-" + (v ? n : k), "true");
        g[_remove](F || l(a, s) || "")
    }

    function q(a, b, d) {
        var c = a[0], g = a.parent(), e = b == k, f = b == _indeterminate, m = b == n,
            s = f ? _determinate : e ? y : "enabled", q = l(a, s + t(c[_type])), r = l(a, b + t(c[_type]));
        if (!1 !== c[b]) {
            if (f || !d || "force" == d) c[b] = !1;
            D(a, e, s, d)
        }
        !c[n] && l(a, _cursor, !0) && g.find("." + C).css(_cursor, "pointer");
        g[_remove](r || l(a, b) || "");
        g.attr("role") && !f && g.attr("aria-" + (m ? n : k), "false");
        g[_add](q || l(a, s) || "")
    }

    function E(a, b) {
        if (a.data(m)) {
            a.parent().html(a.attr("style", a.data(m).s || ""));
            if (b) a[_callback](b);
            a.off(".i").unwrap();
            f(_label + '[for="' + a[0].id + '"]').add(a.closest(_label)).off(".i")
        }
    }

    function l(a, b, f) {
        if (a.data(m))return a.data(m).o[b + (f ? "" : "Class")]
    }

    function t(a) {
        return a.charAt(0).toUpperCase() + a.slice(1)
    }

    function D(a, b, f, c) {
        if (!c) {
            if (b) a[_callback]("ifToggled");
            a[_callback]("ifChanged")[_callback]("if" + t(f))
        }
    }

    var m = "iCheck", C = m + "-helper", r = "radio", k = "checked", y = "un" + k, n = "disabled";
    _determinate = "determinate";
    _indeterminate = "in" + _determinate;
    _update = "update";
    _type = "type";
    _click = "click";
    _touch = "touchbegin.i touchend.i";
    _add = "addClass";
    _remove = "removeClass";
    _callback = "trigger";
    _label = "label";
    _cursor = "cursor";
    _mobile = /ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);
    f.fn[m] = function (a, b) {
        var d = 'input[type="checkbox"], input[type="' + r + '"]', c = f(), g = function (a) {
            a.each(function () {
                var a = f(this);
                c = a.is(d) ? c.add(a) : c.add(a.find(d))
            })
        };
        if (/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a = a.toLowerCase(), g(this), c.each(function () {
            var c =
                f(this);
            "destroy" == a ? E(c, "ifDestroyed") : A(c, !0, a);
            f.isFunction(b) && b()
        });
        if ("object" != typeof a && a)return this;
        var e = f.extend({checkedClass: k, disabledClass: n, indeterminateClass: _indeterminate, labelHover: !0}, a),
            l = e.handle, v = e.hoverClass || "hover", s = e.focusClass || "focus", t = e.activeClass || "active",
            B = !!e.labelHover, w = e.labelHoverClass || "hover", p = ("" + e.increaseArea).replace("%", "") | 0;
        if ("checkbox" == l || l == r) d = 'input[type="' + l + '"]';
        -50 > p && (p = -50);
        g(this);
        return c.each(function () {
            var a = f(this);
            E(a);
            var c = this,
                b = c.id, g = -p + "%", d = 100 + 2 * p + "%", d = {
                    position: "absolute",
                    top: g,
                    left: g,
                    display: "block",
                    width: d,
                    height: d,
                    margin: 0,
                    padding: 0,
                    background: "#fff",
                    border: 0,
                    opacity: 0
                }, g = _mobile ? {position: "absolute", visibility: "hidden"} : p ? d : {position: "absolute", opacity: 0},
                l = "checkbox" == c[_type] ? e.checkboxClass || "icheckbox" : e.radioClass || "i" + r,
                z = f(_label + '[for="' + b + '"]').add(a.closest(_label)), u = !!e.aria,
                y = m + "-" + Math.random().toString(36).substr(2, 6),
                h = '<div class="' + l + '" ' + (u ? 'role="' + c[_type] + '" ' : "");
            u && z.each(function () {
                h +=
                    'aria-labelledby="';
                this.id ? h += this.id : (this.id = y, h += y);
                h += '"'
            });
            h = a.wrap(h + "/>")[_callback]("ifCreated").parent().append(e.insert);
            d = f('<ins class="' + C + '"/>').css(d).appendTo(h);
            a.data(m, {o: e, s: a.attr("style")}).css(g);
            e.inheritClass && h[_add](c.className || "");
            e.inheritID && b && h.attr("id", m + "-" + b);
            "static" == h.css("position") && h.css("position", "relative");
            A(a, !0, _update);
            if (z.length) z.on(_click + ".i mouseover.i mouseout.i " + _touch, function (b) {
                var d = b[_type], e = f(this);
                if (!c[n]) {
                    if (d == _click) {
                        if (f(b.target).is("a"))return;
                        A(a, !1, !0)
                    } else B && (/ut|nd/.test(d) ? (h[_remove](v), e[_remove](w)) : (h[_add](v), e[_add](w)));
                    if (_mobile) b.stopPropagation(); else return !1
                }
            });
            a.on(_click + ".i focus.i blur.i keyup.i keydown.i keypress.i", function (b) {
                var d = b[_type];
                b = b.keyCode;
                if (d == _click)return !1;
                if ("keydown" == d && 32 == b)return c[_type] == r && c[k] || (c[k] ? q(a, k) : x(a, k)), !1;
                if ("keyup" == d && c[_type] == r) !c[k] && x(a, k); else if (/us|ur/.test(d)) h["blur" == d ? _remove : _add](s)
            });
            d.on(_click + " mousedown mouseup mouseover mouseout " + _touch, function (b) {
                var d =
                    b[_type], e = /wn|up/.test(d) ? t : v;
                if (!c[n]) {
                    if (d == _click) A(a, !1, !0); else {
                        if (/wn|er|in/.test(d)) h[_add](e); else h[_remove](e + " " + t);
                        if (z.length && B && e == v) z[/ut|nd/.test(d) ? _remove : _add](w)
                    }
                    if (_mobile) b.stopPropagation(); else return !1
                }
            })
        })
    }
})(window.jQuery || window.Zepto);


/*!
 * jquery.inputmask.bundle.js
 * https://github.com/RobinHerbots/jquery.inputmask
 * Copyright (c) 2010 - 2016 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 3.3.4-45
 */
!function (a) {
    function b(c, d) {
        return this instanceof b ? (a.isPlainObject(c) ? d = c : (d = d || {}, d.alias = c), this.el = void 0, this.opts = a.extend(!0, {}, this.defaults, d), this.noMasksCache = d && void 0 !== d.definitions, this.userOptions = d || {}, this.events = {}, void e(this.opts.alias, d, this.opts)) : new b(c, d)
    }

    function c(a) {
        var b = document.createElement("input"), c = "on" + a, d = c in b;
        return d || (b.setAttribute(c, "return;"), d = "function" == typeof b[c]), b = null, d
    }

    function d(b, c) {
        var d = b.getAttribute("type"),
            e = "INPUT" === b.tagName && a.inArray(d, c.supportsInputType) !== -1 || b.isContentEditable || "TEXTAREA" === b.tagName;
        if (!e && "INPUT" === b.tagName) {
            var f = document.createElement("input");
            f.setAttribute("type", d), e = "text" === f.type, f = null
        }
        return e
    }

    function e(b, c, d) {
        var f = d.aliases[b];
        return f ? (f.alias && e(f.alias, void 0, d), a.extend(!0, d, f), a.extend(!0, d, c), !0) : (null === d.mask && (d.mask = b), !1)
    }

    function f(b, c, d) {
        function f(a, c) {
            c = void 0 !== c ? c : b.getAttribute("data-inputmask-" + a), null !== c && ("string" == typeof c && (0 === a.indexOf("on") ? c = window[c] : "false" === c ? c = !1 : "true" === c && (c = !0)), d[a] = c)
        }

        var g, h, i, j, k = b.getAttribute("data-inputmask");
        if (k && "" !== k && (k = k.replace(new RegExp("'", "g"), '"'), h = JSON.parse("{" + k + "}")), h) {
            i = void 0;
            for (j in h)if ("alias" === j.toLowerCase()) {
                i = h[j];
                break
            }
        }
        f("alias", i), d.alias && e(d.alias, d, c);
        for (g in c) {
            if (h) {
                i = void 0;
                for (j in h)if (j.toLowerCase() === g.toLowerCase()) {
                    i = h[j];
                    break
                }
            }
            f(g, i)
        }
        return a.extend(!0, c, d), c
    }

    function g(c, d) {
        function e(b) {
            function d(a, b, c, d) {
                this.matches = [], this.isGroup = a || !1, this.isOptional = b || !1, this.isQuantifier = c || !1, this.isAlternator = d || !1, this.quantifier = {
                    min: 1,
                    max: 1
                }
            }

            function e(b, d, e) {
                var f = c.definitions[d];
                e = void 0 !== e ? e : b.matches.length;
                var g = b.matches[e - 1];
                if (f && !r) {
                    f.placeholder = a.isFunction(f.placeholder) ? f.placeholder(c) : f.placeholder;
                    for (var h = f.prevalidator, i = h ? h.length : 0, j = 1; j < f.cardinality; j++) {
                        var k = i >= j ? h[j - 1] : [], l = k.validator, m = k.cardinality;
                        b.matches.splice(e++, 0, {
                            fn: l ? "string" == typeof l ? new RegExp(l) : new function () {
                                this.test = l
                            } : new RegExp("."),
                            cardinality: m ? m : 1,
                            optionality: b.isOptional,
                            newBlockMarker: void 0 === g || g.def !== (f.definitionSymbol || d),
                            casing: f.casing,
                            def: f.definitionSymbol || d,
                            placeholder: f.placeholder,
                            mask: d
                        }), g = b.matches[e - 1]
                    }
                    b.matches.splice(e++, 0, {
                        fn: f.validator ? "string" == typeof f.validator ? new RegExp(f.validator) : new function () {
                            this.test = f.validator
                        } : new RegExp("."),
                        cardinality: f.cardinality,
                        optionality: b.isOptional,
                        newBlockMarker: void 0 === g || g.def !== (f.definitionSymbol || d),
                        casing: f.casing,
                        def: f.definitionSymbol || d,
                        placeholder: f.placeholder,
                        mask: d
                    })
                } else b.matches.splice(e++, 0, {
                    fn: null,
                    cardinality: 0,
                    optionality: b.isOptional,
                    newBlockMarker: void 0 === g || g.def !== d,
                    casing: null,
                    def: c.staticDefinitionSymbol || d,
                    placeholder: void 0 !== c.staticDefinitionSymbol ? d : void 0,
                    mask: d
                }), r = !1
            }

            function f(a, b) {
                a.isGroup && (a.isGroup = !1, e(a, c.groupmarker.start, 0), b !== !0 && e(a, c.groupmarker.end))
            }

            function g(a, b, c, d) {
                b.matches.length > 0 && (void 0 === d || d) && (c = b.matches[b.matches.length - 1], f(c)), e(b, a)
            }

            function h() {
                if (t.length > 0) {
                    if (m = t[t.length - 1], g(k, m, o, !m.isAlternator), m.isAlternator) {
                        n = t.pop();
                        for (var a = 0; a < n.matches.length; a++)n.matches[a].isGroup = !1;
                        t.length > 0 ? (m = t[t.length - 1], m.matches.push(n)) : s.matches.push(n)
                    }
                } else g(k, s, o)
            }

            function i(a) {
                function b(a) {
                    return a === c.optionalmarker.start ? a = c.optionalmarker.end : a === c.optionalmarker.end ? a = c.optionalmarker.start : a === c.groupmarker.start ? a = c.groupmarker.end : a === c.groupmarker.end && (a = c.groupmarker.start), a
                }

                a.matches = a.matches.reverse();
                for (var d in a.matches) {
                    var e = parseInt(d);
                    if (a.matches[d].isQuantifier && a.matches[e + 1] && a.matches[e + 1].isGroup) {
                        var f = a.matches[d];
                        a.matches.splice(d, 1), a.matches.splice(e + 1, 0, f)
                    }
                    void 0 !== a.matches[d].matches ? a.matches[d] = i(a.matches[d]) : a.matches[d] = b(a.matches[d])
                }
                return a
            }

            for (var j, k, l, m, n, o, p, q = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g, r = !1, s = new d, t = [], u = []; j = q.exec(b);)if (k = j[0], r) h(); else switch (k.charAt(0)) {
                case c.escapeChar:
                    r = !0;
                    break;
                case c.optionalmarker.end:
                case c.groupmarker.end:
                    if (l = t.pop(), void 0 !== l)if (t.length > 0) {
                        if (m = t[t.length - 1], m.matches.push(l), m.isAlternator) {
                            n = t.pop();
                            for (var v = 0; v < n.matches.length; v++)n.matches[v].isGroup = !1;
                            t.length > 0 ? (m = t[t.length - 1], m.matches.push(n)) : s.matches.push(n)
                        }
                    } else s.matches.push(l); else h();
                    break;
                case c.optionalmarker.start:
                    t.push(new d((!1), (!0)));
                    break;
                case c.groupmarker.start:
                    t.push(new d((!0)));
                    break;
                case c.quantifiermarker.start:
                    var w = new d((!1), (!1), (!0));
                    k = k.replace(/[{}]/g, "");
                    var x = k.split(","), y = isNaN(x[0]) ? x[0] : parseInt(x[0]),
                        z = 1 === x.length ? y : isNaN(x[1]) ? x[1] : parseInt(x[1]);
                    if ("*" !== z && "+" !== z || (y = "*" === z ? 0 : 1), w.quantifier = {
                            min: y,
                            max: z
                        }, t.length > 0) {
                        var A = t[t.length - 1].matches;
                        j = A.pop(), j.isGroup || (p = new d((!0)), p.matches.push(j), j = p), A.push(j), A.push(w)
                    } else j = s.matches.pop(), j.isGroup || (p = new d((!0)), p.matches.push(j), j = p), s.matches.push(j), s.matches.push(w);
                    break;
                case c.alternatormarker:
                    t.length > 0 ? (m = t[t.length - 1], o = m.matches.pop()) : o = s.matches.pop(), o.isAlternator ? t.push(o) : (n = new d((!1), (!1), (!1), (!0)), n.matches.push(o), t.push(n));
                    break;
                default:
                    h()
            }
            for (; t.length > 0;)l = t.pop(), f(l, !0), s.matches.push(l);
            return s.matches.length > 0 && (o = s.matches[s.matches.length - 1], f(o), u.push(s)), c.numericInput && i(u[0]), u
        }

        function f(f, g) {
            if (null !== f && "" !== f) {
                if (1 === f.length && c.greedy === !1 && 0 !== c.repeat && (c.placeholder = ""), c.repeat > 0 || "*" === c.repeat || "+" === c.repeat) {
                    var h = "*" === c.repeat ? 0 : "+" === c.repeat ? 1 : c.repeat;
                    f = c.groupmarker.start + f + c.groupmarker.end + c.quantifiermarker.start + h + "," + c.repeat + c.quantifiermarker.end
                }
                var i;
                return void 0 === b.prototype.masksCache[f] || d === !0 ? (i = {
                    mask: f,
                    maskToken: e(f),
                    validPositions: {},
                    _buffer: void 0,
                    buffer: void 0,
                    tests: {},
                    metadata: g,
                    maskLength: void 0
                }, d !== !0 && (b.prototype.masksCache[c.numericInput ? f.split("").reverse().join("") : f] = i, i = a.extend(!0, {}, b.prototype.masksCache[c.numericInput ? f.split("").reverse().join("") : f]))) : i = a.extend(!0, {}, b.prototype.masksCache[c.numericInput ? f.split("").reverse().join("") : f]), i
            }
        }

        function g(a) {
            return a = a.toString()
        }

        var h;
        if (a.isFunction(c.mask) && (c.mask = c.mask(c)), a.isArray(c.mask)) {
            if (c.mask.length > 1) {
                c.keepStatic = null === c.keepStatic || c.keepStatic;
                var i = "(";
                return a.each(c.numericInput ? c.mask.reverse() : c.mask, function (b, c) {
                    i.length > 1 && (i += ")|("), i += g(void 0 === c.mask || a.isFunction(c.mask) ? c : c.mask)
                }), i += ")", f(i, c.mask)
            }
            c.mask = c.mask.pop()
        }
        return c.mask && (h = void 0 === c.mask.mask || a.isFunction(c.mask.mask) ? f(g(c.mask), c.mask) : f(g(c.mask.mask), c.mask)), h
    }

    function h(e, f, g) {
        function i(a, b, c) {
            b = b || 0;
            var d, e, f, h = [], i = 0, j = p();
            ja = void 0 !== ha ? ha.maxLength : void 0, ja === -1 && (ja = void 0);
            do a === !0 && n().validPositions[i] ? (f = n().validPositions[i], e = f.match, d = f.locator.slice(), h.push(c === !0 ? f.input : I(i, e))) : (f = s(i, d, i - 1), e = f.match, d = f.locator.slice(), (g.jitMasking === !1 || i < j || Number.isFinite(g.jitMasking) && g.jitMasking > i) && h.push(I(i, e))), i++; while ((void 0 === ja || i < ja) && (null !== e.fn || "" !== e.def) || b > i);
            return "" === h[h.length - 1] && h.pop(), n().maskLength = i + 1, h
        }

        function n() {
            return f
        }

        function o(a) {
            var b = n();
            b.buffer = void 0, a !== !0 && (b._buffer = void 0, b.validPositions = {}, b.p = 0)
        }

        function p(a, b, c) {
            var d = -1, e = -1, f = c || n().validPositions;
            void 0 === a && (a = -1);
            for (var g in f) {
                var h = parseInt(g);
                f[h] && (b || null !== f[h].match.fn) && (h <= a && (d = h), h >= a && (e = h))
            }
            return d !== -1 && a - d > 1 || e < a ? d : e
        }

        function q(b, c, d, e) {
            function f(a) {
                var b = n().validPositions[a];
                if (void 0 !== b && null === b.match.fn) {
                    var c = n().validPositions[a - 1], d = n().validPositions[a + 1];
                    return void 0 !== c && void 0 !== d
                }
                return !1
            }

            var h, i = b, j = a.extend(!0, {}, n().validPositions), k = !1;
            for (n().p = b, h = c - 1; h >= i; h--)void 0 !== n().validPositions[h] && (d === !0 || !f(h) && g.canClearPosition(n(), h, p(), e, g) !== !1) && delete n().validPositions[h];
            for (o(!0), h = i + 1; h <= p();) {
                for (; void 0 !== n().validPositions[i];)i++;
                var l = n().validPositions[i];
                if (h < i && (h = i + 1), void 0 === n().validPositions[h] && D(h) || void 0 !== l) h++; else {
                    var m = s(h);
                    k === !1 && j[i] && j[i].match.def === m.match.def ? (n().validPositions[i] = a.extend(!0, {}, j[i]), n().validPositions[i].input = m.input, delete n().validPositions[h], h++) : u(i, m.match.def) ? C(i, m.input || I(h), !0) !== !1 && (delete n().validPositions[h], h++, k = !0) : D(h) || (h++, i--), i++
                }
            }
            o(!0)
        }

        function r(a, b) {
            for (var c, d = a, e = p(), f = n().validPositions[e] || w(0)[0], h = void 0 !== f.alternation ? f.locator[f.alternation].toString().split(",") : [], i = 0; i < d.length && (c = d[i], !(c.match && (g.greedy && c.match.optionalQuantifier !== !0 || (c.match.optionality === !1 || c.match.newBlockMarker === !1) && c.match.optionalQuantifier !== !0) && (void 0 === f.alternation || f.alternation !== c.alternation || void 0 !== c.locator[f.alternation] && B(c.locator[f.alternation].toString().split(","), h))) || b === !0 && (null !== c.match.fn || /[0-9a-bA-Z]/.test(c.match.def))); i++);
            return c
        }

        function s(a, b, c) {
            return n().validPositions[a] || r(w(a, b ? b.slice() : b, c))
        }

        function t(a) {
            return n().validPositions[a] ? n().validPositions[a] : w(a)[0]
        }

        function u(a, b) {
            for (var c = !1, d = w(a), e = 0; e < d.length; e++)if (d[e].match && d[e].match.def === b) {
                c = !0;
                break
            }
            return c
        }

        function v(b, c) {
            var d, e;
            return (n().tests[b] || n().validPositions[b]) && a.each(n().tests[b] || [n().validPositions[b]], function (a, b) {
                var f = b.alternation ? b.locator[b.alternation].toString().indexOf(c) : -1;
                (void 0 === e || f < e) && f !== -1 && (d = b, e = f)
            }), d
        }

        function w(b, c, d) {
            function e(c, d, f, h) {
                function j(f, h, l) {
                    function q(b, c) {
                        var d = 0 === a.inArray(b, c.matches);
                        return d || a.each(c.matches, function (a, e) {
                            if (e.isQuantifier === !0 && (d = q(b, c.matches[a - 1])))return !1
                        }), d
                    }

                    function r(a, b) {
                        var c = v(a, b);
                        return c ? c.locator.slice(c.alternation + 1) : void 0
                    }

                    function s(a, c) {
                        return null === a.match.fn && null !== c.match.fn && c.match.fn.test(a.match.def, n(), b, !1, g, !1)
                    }

                    if (k > 1e4)throw"Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + n().mask;
                    if (k === b && void 0 === f.matches)return m.push({match: f, locator: h.reverse(), cd: p}), !0;
                    if (void 0 !== f.matches) {
                        if (f.isGroup && l !== f) {
                            if (f = j(c.matches[a.inArray(f, c.matches) + 1], h))return !0
                        } else if (f.isOptional) {
                            var t = f;
                            if (f = e(f, d, h, l)) {
                                if (i = m[m.length - 1].match, !q(i, t))return !0;
                                o = !0, k = b
                            }
                        } else if (f.isAlternator) {
                            var u, w = f, x = [], y = m.slice(), z = h.length, A = d.length > 0 ? d.shift() : -1;
                            if (A === -1 || "string" == typeof A) {
                                var B, C = k, D = d.slice(), E = [];
                                if ("string" == typeof A) E = A.split(","); else for (B = 0; B < w.matches.length; B++)E.push(B);
                                for (var F = 0; F < E.length; F++) {
                                    if (B = parseInt(E[F]), m = [], d = r(k, B) || D.slice(), f = j(w.matches[B] || c.matches[B], [B].concat(h), l) || f, f !== !0 && void 0 !== f && E[E.length - 1] < w.matches.length) {
                                        var G = a.inArray(f, c.matches) + 1;
                                        c.matches.length > G && (f = j(c.matches[G], [G].concat(h.slice(1, h.length)), l), f && (E.push(G.toString()), a.each(m, function (a, b) {
                                            b.alternation = h.length - 1
                                        })))
                                    }
                                    u = m.slice(), k = C, m = [];
                                    for (var H = 0; H < u.length; H++) {
                                        var I = u[H], J = !1;
                                        I.alternation = I.alternation || z;
                                        for (var K = 0; K < x.length; K++) {
                                            var L = x[K];
                                            if (("string" != typeof A || a.inArray(I.locator[I.alternation].toString(), E) !== -1) && (I.match.def === L.match.def || s(I, L))) {
                                                J = I.match.mask === L.match.mask, L.locator[I.alternation].toString().indexOf(I.locator[I.alternation]) === -1 && (L.locator[I.alternation] = L.locator[I.alternation] + "," + I.locator[I.alternation], L.alternation = I.alternation, null == I.match.fn && (L.na = L.na || I.locator[I.alternation].toString(), L.na.indexOf(I.locator[I.alternation]) === -1 && (L.na = L.na + "," + I.locator[I.alternation])));
                                                break
                                            }
                                        }
                                        J || x.push(I)
                                    }
                                }
                                "string" == typeof A && (x = a.map(x, function (b, c) {
                                    if (isFinite(c)) {
                                        var d, e = b.alternation, f = b.locator[e].toString().split(",");
                                        b.locator[e] = void 0, b.alternation = void 0;
                                        for (var g = 0; g < f.length; g++)d = a.inArray(f[g], E) !== -1, d && (void 0 !== b.locator[e] ? (b.locator[e] += ",", b.locator[e] += f[g]) : b.locator[e] = parseInt(f[g]), b.alternation = e);
                                        if (void 0 !== b.locator[e])return b
                                    }
                                })), m = y.concat(x), k = b, o = m.length > 0, d = D.slice()
                            } else f = j(w.matches[A] || c.matches[A], [A].concat(h), l);
                            if (f)return !0
                        } else if (f.isQuantifier && l !== c.matches[a.inArray(f, c.matches) - 1])for (var M = f, N = d.length > 0 ? d.shift() : 0; N < (isNaN(M.quantifier.max) ? N + 1 : M.quantifier.max) && k <= b; N++) {
                            var O = c.matches[a.inArray(M, c.matches) - 1];
                            if (f = j(O, [N].concat(h), O)) {
                                if (i = m[m.length - 1].match, i.optionalQuantifier = N > M.quantifier.min - 1, q(i, O)) {
                                    if (N > M.quantifier.min - 1) {
                                        o = !0, k = b;
                                        break
                                    }
                                    return !0
                                }
                                return !0
                            }
                        } else if (f = e(f, d, h, l))return !0
                    } else k++
                }

                for (var l = d.length > 0 ? d.shift() : 0; l < c.matches.length; l++)if (c.matches[l].isQuantifier !== !0) {
                    var q = j(c.matches[l], [l].concat(f), h);
                    if (q && k === b)return q;
                    if (k > b)break
                }
            }

            function f(b) {
                var c = [];
                return a.isArray(b) || (b = [b]), b.length > 0 && (void 0 === b[0].alternation ? (c = r(b.slice()).locator.slice(), 0 === c.length && (c = b[0].locator.slice())) : a.each(b, function (a, b) {
                    if ("" !== b.def)if (0 === c.length) c = b.locator.slice(); else for (var d = 0; d < c.length; d++)b.locator[d] && c[d].toString().indexOf(b.locator[d]) === -1 && (c[d] += "," + b.locator[d])
                })), c
            }

            function h(a) {
                return g.keepStatic && b > 0 && a.length > 1 + ("" === a[a.length - 1].match.def ? 1 : 0) && a[0].match.optionality !== !0 && a[0].match.optionalQuantifier !== !0 && null === a[0].match.fn && !/[0-9a-bA-Z]/.test(a[0].match.def) ? [r(a)] : a
            }

            var i, j = n().maskToken, k = c ? d : 0, l = c ? c.slice() : [0], m = [], o = !1, p = c ? c.join("") : "";
            if (b > -1) {
                if (void 0 === c) {
                    for (var q, s = b - 1; void 0 === (q = n().validPositions[s] || n().tests[s]) && s > -1;)s--;
                    void 0 !== q && s > -1 && (l = f(q), p = l.join(""), k = s)
                }
                if (n().tests[b] && n().tests[b][0].cd === p)return h(n().tests[b]);
                for (var t = l.shift(); t < j.length; t++) {
                    var u = e(j[t], l, [t]);
                    if (u && k === b || k > b)break
                }
            }
            return (0 === m.length || o) && m.push({
                match: {
                    fn: null,
                    cardinality: 0,
                    optionality: !0,
                    casing: null,
                    def: "",
                    placeholder: ""
                }, locator: [], cd: p
            }), void 0 !== c && n().tests[b] ? h(a.extend(!0, [], m)) : (n().tests[b] = a.extend(!0, [], m), h(n().tests[b]))
        }

        function x() {
            return void 0 === n()._buffer && (n()._buffer = i(!1, 1), void 0 === n().buffer && n()._buffer.slice()), n()._buffer
        }

        function y(a) {
            return void 0 !== n().buffer && a !== !0 || (n().buffer = i(!0, p(), !0)), n().buffer
        }

        function z(a, b, c) {
            var d;
            if (a === !0) o(), a = 0, b = c.length; else for (d = a; d < b; d++)delete n().validPositions[d];
            for (d = a; d < b; d++)o(!0), c[d] !== g.skipOptionalPartCharacter && C(d, c[d], !0, !0)
        }

        function A(a, c, d) {
            switch (g.casing || c.casing) {
                case"upper":
                    a = a.toUpperCase();
                    break;
                case"lower":
                    a = a.toLowerCase();
                    break;
                case"title":
                    var e = n().validPositions[d - 1];
                    a = 0 === d || e && e.input === String.fromCharCode(b.keyCode.SPACE) ? a.toUpperCase() : a.toLowerCase()
            }
            return a
        }

        function B(b, c) {
            for (var d = g.greedy ? c : c.slice(0, 1), e = !1, f = 0; f < b.length; f++)if (a.inArray(b[f], d) !== -1) {
                e = !0;
                break
            }
            return e
        }

        function C(c, d, e, f, h) {
            function i(a) {
                return ma ? a.begin - a.end > 1 || a.begin - a.end === 1 && g.insertMode : a.end - a.begin > 1 || a.end - a.begin === 1 && g.insertMode
            }

            function j(b, d, e) {
                var h = !1;
                return a.each(w(b), function (j, k) {
                    for (var l = k.match, r = d ? 1 : 0, s = "", t = l.cardinality; t > r; t--)s += G(b - (t - 1));
                    if (d && (s += d), y(!0), h = null != l.fn ? l.fn.test(s, n(), b, e, g, i(c)) : (d === l.def || d === g.skipOptionalPartCharacter) && "" !== l.def && {
                                c: l.placeholder || l.def,
                                pos: b
                            }, h !== !1) {
                        var u = void 0 !== h.c ? h.c : d;
                        u = u === g.skipOptionalPartCharacter && null === l.fn ? l.placeholder || l.def : u;
                        var v = b, w = y();
                        if (void 0 !== h.remove && (a.isArray(h.remove) || (h.remove = [h.remove]), a.each(h.remove.sort(function (a, b) {
                                return b - a
                            }), function (a, b) {
                                q(b, b + 1, !0)
                            })), void 0 !== h.insert && (a.isArray(h.insert) || (h.insert = [h.insert]), a.each(h.insert.sort(function (a, b) {
                                return a - b
                            }), function (a, b) {
                                C(b.pos, b.c, !0, f)
                            })), h.refreshFromBuffer) {
                            var x = h.refreshFromBuffer;
                            if (e = !0, z(x === !0 ? x : x.start, x.end, w), void 0 === h.pos && void 0 === h.c)return h.pos = p(), !1;
                            if (v = void 0 !== h.pos ? h.pos : b, v !== b)return h = a.extend(h, C(v, u, !0, f)), !1
                        } else if (h !== !0 && void 0 !== h.pos && h.pos !== b && (v = h.pos, z(b, v, y().slice()), v !== b))return h = a.extend(h, C(v, u, !0)), !1;
                        return (h === !0 || void 0 !== h.pos || void 0 !== h.c) && (j > 0 && o(!0), m(v, a.extend({}, k, {input: A(u, l, v)}), f, i(c)) || (h = !1), !1)
                    }
                }), h
            }

            function k(b, c, d) {
                var e, h, i, j, k, l, m, q, r = a.extend(!0, {}, n().validPositions), s = !1, t = p();
                for (j = n().validPositions[t]; t >= 0; t--)if (i = n().validPositions[t], i && void 0 !== i.alternation) {
                    if (e = t, h = n().validPositions[e].alternation, j.locator[i.alternation] !== i.locator[i.alternation])break;
                    j = i
                }
                if (void 0 !== h) {
                    q = parseInt(e);
                    var u = void 0 !== j.locator[j.alternation || h] ? j.locator[j.alternation || h] : m[0];
                    u.length > 0 && (u = u.split(",")[0]);
                    var v = n().validPositions[q], x = n().validPositions[q - 1];
                    a.each(w(q, x ? x.locator : void 0, q - 1), function (e, i) {
                        m = i.locator[h] ? i.locator[h].toString().split(",") : [];
                        for (var j = 0; j < m.length; j++) {
                            var t = [], w = 0, x = 0, y = !1;
                            if (u < m[j] && (void 0 === i.na || a.inArray(m[j], i.na.split(",")) === -1)) {
                                n().validPositions[q] = a.extend(!0, {}, i);
                                var z = n().validPositions[q].locator;
                                for (n().validPositions[q].locator[h] = parseInt(m[j]), null == i.match.fn ? (v.input !== i.match.def && (y = !0, v.generatedInput !== !0 && t.push(v.input)), x++, n().validPositions[q].generatedInput = !/[0-9a-bA-Z]/.test(i.match.def), n().validPositions[q].input = i.match.def) : n().validPositions[q].input = v.input, k = q + 1; k < p(void 0, !0) + 1; k++)l = n().validPositions[k], l && l.generatedInput !== !0 && /[0-9a-bA-Z]/.test(l.input) ? t.push(l.input) : k < b && w++, delete n().validPositions[k];
                                for (y && t[0] === i.match.def && t.shift(), o(!0), s = !0; t.length > 0;) {
                                    var A = t.shift();
                                    if (A !== g.skipOptionalPartCharacter && !(s = C(p(void 0, !0) + 1, A, !1, f, !0)))break
                                }
                                if (s) {
                                    n().validPositions[q].locator = z;
                                    var B = p(b) + 1;
                                    for (k = q + 1; k < p() + 1; k++)l = n().validPositions[k], (void 0 === l || null == l.match.fn) && k < b + (x - w) && x++;
                                    b += x - w, s = C(b > B ? B : b, c, d, f, !0)
                                }
                                if (s)return !1;
                                o(), n().validPositions = a.extend(!0, {}, r)
                            }
                        }
                    })
                }
                return s
            }

            function l(b, c) {
                var d = n().validPositions[c];
                if (d)for (var e = d.locator, f = e.length, g = b; g < c; g++)if (void 0 === n().validPositions[g] && !D(g, !0)) {
                    var h = w(g), i = h[0], j = -1;
                    a.each(h, function (a, b) {
                        for (var c = 0; c < f && (void 0 !== b.locator[c] && B(b.locator[c].toString().split(","), e[c].toString().split(","))); c++)j < c && (j = c, i = b)
                    }), m(g, a.extend({}, i, {input: i.match.placeholder || i.match.def}), !0)
                }
            }

            function m(b, c, d, e) {
                if (e || g.insertMode && void 0 !== n().validPositions[b] && void 0 === d) {
                    var f, h = a.extend(!0, {}, n().validPositions), i = p(void 0, !0);
                    for (f = b; f <= i; f++)delete n().validPositions[f];
                    n().validPositions[b] = a.extend(!0, {}, c);
                    var j, k = !0, l = n().validPositions, m = !1, q = n().maskLength;
                    for (f = j = b; f <= i; f++) {
                        var r = h[f];
                        if (void 0 !== r)for (var s = j; s < n().maskLength && (null == r.match.fn && l[f] && (l[f].match.optionalQuantifier === !0 || l[f].match.optionality === !0) || null != r.match.fn);) {
                            if (s++, m === !1 && h[s] && h[s].match.def === r.match.def) n().validPositions[s] = a.extend(!0, {}, h[s]), n().validPositions[s].input = r.input, t(s), j = s, k = !0; else if (u(s, r.match.def)) {
                                var v = C(s, r.input, !0, !0);
                                k = v !== !1, j = v.caret || v.insert ? p() : s, m = !0
                            } else k = r.generatedInput === !0;
                            if (n().maskLength < q && (n().maskLength = q), k)break
                        }
                        if (!k)break
                    }
                    if (!k)return n().validPositions = a.extend(!0, {}, h), o(!0), !1
                } else n().validPositions[b] = a.extend(!0, {}, c);
                return o(!0), !0
            }

            function t(b) {
                for (var c = b - 1; c > -1 && !n().validPositions[c]; c--);
                var d, e;
                for (c++; c < b; c++)void 0 === n().validPositions[c] && (g.jitMasking === !1 || g.jitMasking > c) && (e = w(c, s(c - 1).locator, c - 1).slice(), "" === e[e.length - 1].match.def && e.pop(), d = r(e), d && (d.match.def === g.radixPointDefinitionSymbol || !D(c, !0) || a.inArray(g.radixPoint, y()) < c && d.match.fn && d.match.fn.test(I(c), n(), c, !1, g)) && (x = j(c, d.match.placeholder || (null == d.match.fn ? d.match.def : "" !== I(c) ? I(c) : y()[c]), !0), x !== !1 && (n().validPositions[x.pos || c].generatedInput = !0)))
            }

            e = e === !0;
            var v = c;
            void 0 !== c.begin && (v = ma && !i(c) ? c.end : c.begin);
            var x = !1, F = a.extend(!0, {}, n().validPositions);
            if (t(v), i(c) && (Q(void 0, b.keyCode.DELETE, c), v = n().p), v < n().maskLength && (x = j(v, d, e), (!e || f === !0) && x === !1)) {
                var H = n().validPositions[v];
                if (!H || null !== H.match.fn || H.match.def !== d && d !== g.skipOptionalPartCharacter) {
                    if ((g.insertMode || void 0 === n().validPositions[E(v)]) && !D(v, !0)) {
                        var J = w(v).slice();
                        "" === J[J.length - 1].match.def && J.pop();
                        var K = r(J, !0);
                        K && null === K.match.fn && (K = K.match.placeholder || K.match.def, j(v, K, e), n().validPositions[v].generatedInput = !0);
                        for (var L = v + 1, M = E(v); L <= M; L++)if (x = j(L, d, e), x !== !1) {
                            l(v, void 0 !== x.pos ? x.pos : L), v = L;
                            break
                        }
                    }
                } else x = {caret: E(v)}
            }
            return x === !1 && g.keepStatic && !e && h !== !0 && (x = k(v, d, e)), x === !0 && (x = {pos: v}), a.isFunction(g.postValidation) && x !== !1 && !e && f !== !0 && (x = !!g.postValidation(y(!0), x, g) && x), void 0 === x.pos && (x.pos = v), x === !1 && (o(!0), n().validPositions = a.extend(!0, {}, F)), x
        }

        function D(a, b) {
            var c;
            if (b ? (c = s(a).match, "" === c.def && (c = t(a).match)) : c = t(a).match, null != c.fn)return c.fn;
            if (b !== !0 && a > -1) {
                var d = w(a);
                return d.length > 1 + ("" === d[d.length - 1].match.def ? 1 : 0)
            }
            return !1
        }

        function E(a, b) {
            var c = n().maskLength;
            if (a >= c)return c;
            for (var d = a; ++d < c && (b === !0 && (t(d).match.newBlockMarker !== !0 || !D(d)) || b !== !0 && !D(d)););
            return d
        }

        function F(a, b) {
            var c, d = a;
            if (d <= 0)return 0;
            for (; --d > 0 && (b === !0 && t(d).match.newBlockMarker !== !0 || b !== !0 && !D(d) && (c = w(d), c.length < 2 || 2 === c.length && "" === c[1].match.def)););
            return d
        }

        function G(a) {
            return void 0 === n().validPositions[a] ? I(a) : n().validPositions[a].input
        }

        function H(b, c, d, e, f) {
            if (e && a.isFunction(g.onBeforeWrite)) {
                var h = g.onBeforeWrite(e, c, d, g);
                if (h) {
                    if (h.refreshFromBuffer) {
                        var i = h.refreshFromBuffer;
                        z(i === !0 ? i : i.start, i.end, h.buffer || c), c = y(!0)
                    }
                    void 0 !== d && (d = void 0 !== h.caret ? h.caret : d)
                }
            }
            b.inputmask._valueSet(c.join("")), void 0 === d || void 0 !== e && "blur" === e.type ? ea(b, c, d) : L(b, d), f === !0 && (oa = !0, a(b).trigger("input"))
        }

        function I(a, b) {
            if (b = b || t(a).match, void 0 !== b.placeholder)return b.placeholder;
            if (null === b.fn) {
                if (a > -1 && void 0 === n().validPositions[a]) {
                    var c, d = w(a), e = [];
                    if (d.length > 1 + ("" === d[d.length - 1].match.def ? 1 : 0))for (var f = 0; f < d.length; f++)if (d[f].match.optionality !== !0 && d[f].match.optionalQuantifier !== !0 && (null === d[f].match.fn || void 0 === c || d[f].match.fn.test(c.match.def, n(), a, !0, g) !== !1) && (e.push(d[f]), null === d[f].match.fn && (c = d[f]), e.length > 1 && /[0-9a-bA-Z]/.test(e[0].match.def)))return g.placeholder.charAt(a % g.placeholder.length)
                }
                return b.def
            }
            return g.placeholder.charAt(a % g.placeholder.length)
        }

        function J(c, d, e, f, h, i) {
            function j() {
                var a = !1, b = x().slice(m, E(m)).join("").indexOf(l);
                if (b !== -1 && !D(m)) {
                    a = !0;
                    for (var c = x().slice(m, m + b), d = 0; d < c.length; d++)if (" " !== c[d]) {
                        a = !1;
                        break
                    }
                }
                return a
            }

            var k = f.slice(), l = "", m = 0, q = void 0;
            if (o(), n().p = E(-1), !e)if (g.autoUnmask !== !0) {
                var r = x().slice(0, E(-1)).join(""), t = k.join("").match(new RegExp("^" + b.escapeRegex(r), "g"));
                t && t.length > 0 && (k.splice(0, t.length * r.length), m = E(m))
            } else m = E(m);
            if (a.each(k, function (b, d) {
                    if (void 0 !== d) {
                        var f = new a.Event("keypress");
                        f.which = d.charCodeAt(0), l += d;
                        var h = p(void 0, !0), i = n().validPositions[h],
                            k = s(h + 1, i ? i.locator.slice() : void 0, h);
                        if (!j() || e || g.autoUnmask) {
                            var r = e ? b : null == k.match.fn && k.match.optionality && h + 1 < n().p ? h + 1 : n().p;
                            q = S.call(c, f, !0, !1, e, r), m = r + 1, l = ""
                        } else q = S.call(c, f, !0, !1, !0, h + 1);
                        if (!e && a.isFunction(g.onBeforeWrite) && (q = g.onBeforeWrite(f, y(), q.forwardPosition, g), q && q.refreshFromBuffer)) {
                            var t = q.refreshFromBuffer;
                            z(t === !0 ? t : t.start, t.end, q.buffer), o(!0), q.caret && (n().p = q.caret)
                        }
                    }
                }), d) {
                var u = void 0, v = p();
                document.activeElement === c && (h || q) && (u = L(c).begin, h && q === !1 && (u = E(p(u))), q && i !== !0 && (u < v + 1 || v === -1) && (u = g.numericInput && void 0 === q.caret ? F(q.forwardPosition) : q.forwardPosition)), H(c, y(), u, h || new a.Event("checkval"))
            }
        }

        function K(b) {
            if (b && void 0 === b.inputmask)return b.value;
            var c = [], d = n().validPositions;
            for (var e in d)d[e].match && null != d[e].match.fn && c.push(d[e].input);
            var f = 0 === c.length ? "" : (ma ? c.reverse() : c).join("");
            if (a.isFunction(g.onUnMask)) {
                var h = (ma ? y().slice().reverse() : y()).join("");
                f = g.onUnMask(h, f, g) || f
            }
            return f
        }

        function L(a, b, c, d) {
            function e(a) {
                if (d !== !0 && ma && "number" == typeof a && (!g.greedy || "" !== g.placeholder)) {
                    var b = y().join("").length;
                    a = b - a
                }
                return a
            }

            var f;
            if ("number" != typeof b)return a.setSelectionRange ? (b = a.selectionStart, c = a.selectionEnd) : window.getSelection ? (f = window.getSelection().getRangeAt(0), f.commonAncestorContainer.parentNode !== a && f.commonAncestorContainer !== a || (b = f.startOffset, c = f.endOffset)) : document.selection && document.selection.createRange && (f = document.selection.createRange(), b = 0 - f.duplicate().moveStart("character", -a.inputmask._valueGet().length), c = b + f.text.length), {
                begin: e(b),
                end: e(c)
            };
            b = e(b), c = e(c), c = "number" == typeof c ? c : b;
            var h = parseInt(((a.ownerDocument.defaultView || window).getComputedStyle ? (a.ownerDocument.defaultView || window).getComputedStyle(a, null) : a.currentStyle).fontSize) * c;
            if (a.scrollLeft = h > a.scrollWidth ? h : 0, j || g.insertMode !== !1 || b !== c || c++, a.setSelectionRange) a.selectionStart = b, a.selectionEnd = c; else if (window.getSelection) {
                if (f = document.createRange(), void 0 === a.firstChild || null === a.firstChild) {
                    var i = document.createTextNode("");
                    a.appendChild(i)
                }
                f.setStart(a.firstChild, b < a.inputmask._valueGet().length ? b : a.inputmask._valueGet().length), f.setEnd(a.firstChild, c < a.inputmask._valueGet().length ? c : a.inputmask._valueGet().length), f.collapse(!0);
                var k = window.getSelection();
                k.removeAllRanges(), k.addRange(f)
            } else a.createTextRange && (f = a.createTextRange(), f.collapse(!0), f.moveEnd("character", c), f.moveStart("character", b), f.select());
            ea(a, void 0, {begin: b, end: c})
        }

        function M(b) {
            var c, d, e = y(), f = e.length, g = p(), h = {}, i = n().validPositions[g],
                j = void 0 !== i ? i.locator.slice() : void 0;
            for (c = g + 1; c < e.length; c++)d = s(c, j, c - 1), j = d.locator.slice(), h[c] = a.extend(!0, {}, d);
            var k = i && void 0 !== i.alternation ? i.locator[i.alternation] : void 0;
            for (c = f - 1; c > g && (d = h[c], (d.match.optionality || d.match.optionalQuantifier || k && (k !== h[c].locator[i.alternation] && null != d.match.fn || null === d.match.fn && d.locator[i.alternation] && B(d.locator[i.alternation].toString().split(","), k.toString().split(",")) && "" !== w(c)[0].def)) && e[c] === I(c, d.match)); c--)f--;
            return b ? {l: f, def: h[f] ? h[f].match : void 0} : f
        }

        function N(a) {
            for (var b = M(), c = a.length - 1; c > b && !D(c); c--);
            return a.splice(b, c + 1 - b), a
        }

        function O(b) {
            if (a.isFunction(g.isComplete))return g.isComplete(b, g);
            if ("*" !== g.repeat) {
                var c = !1, d = M(!0), e = F(d.l);
                if (void 0 === d.def || d.def.newBlockMarker || d.def.optionality || d.def.optionalQuantifier) {
                    c = !0;
                    for (var f = 0; f <= e; f++) {
                        var h = s(f).match;
                        if (null !== h.fn && void 0 === n().validPositions[f] && h.optionality !== !0 && h.optionalQuantifier !== !0 || null === h.fn && b[f] !== I(f, h)) {
                            c = !1;
                            break
                        }
                    }
                }
                return c
            }
        }

        function P(b) {
            function c(b) {
                if (a.valHooks && (void 0 === a.valHooks[b] || a.valHooks[b].inputmaskpatch !== !0)) {
                    var c = a.valHooks[b] && a.valHooks[b].get ? a.valHooks[b].get : function (a) {
                        return a.value
                    }, d = a.valHooks[b] && a.valHooks[b].set ? a.valHooks[b].set : function (a, b) {
                        return a.value = b, a
                    };
                    a.valHooks[b] = {
                        get: function (a) {
                            if (a.inputmask) {
                                if (a.inputmask.opts.autoUnmask)return a.inputmask.unmaskedvalue();
                                var b = c(a);
                                return p(void 0, void 0, a.inputmask.maskset.validPositions) !== -1 || g.nullable !== !0 ? b : ""
                            }
                            return c(a)
                        }, set: function (b, c) {
                            var e, f = a(b);
                            return e = d(b, c), b.inputmask && f.trigger("setvalue"), e
                        }, inputmaskpatch: !0
                    }
                }
            }

            function d() {
                return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : p() !== -1 || g.nullable !== !0 ? document.activeElement === this && g.clearMaskOnLostFocus ? (ma ? N(y().slice()).reverse() : N(y().slice())).join("") : h.call(this) : "" : h.call(this)
            }

            function e(b) {
                i.call(this, b), this.inputmask && a(this).trigger("setvalue")
            }

            function f(b) {
                ra.on(b, "mouseenter", function (b) {
                    var c = a(this), d = this, e = d.inputmask._valueGet();
                    e !== y().join("") && c.trigger("setvalue")
                })
            }

            var h, i;
            if (!b.inputmask.__valueGet) {
                if (g.noValuePatching !== !0) {
                    if (Object.getOwnPropertyDescriptor) {
                        "function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" == typeof"test".__proto__ ? function (a) {
                            return a.__proto__
                        } : function (a) {
                            return a.constructor.prototype
                        });
                        var j = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(b), "value") : void 0;
                        j && j.get && j.set ? (h = j.get, i = j.set, Object.defineProperty(b, "value", {
                            get: d,
                            set: e,
                            configurable: !0
                        })) : "INPUT" !== b.tagName && (h = function () {
                                return this.textContent
                            }, i = function (a) {
                                this.textContent = a
                            }, Object.defineProperty(b, "value", {get: d, set: e, configurable: !0}))
                    } else document.__lookupGetter__ && b.__lookupGetter__("value") && (h = b.__lookupGetter__("value"), i = b.__lookupSetter__("value"), b.__defineGetter__("value", d), b.__defineSetter__("value", e));
                    b.inputmask.__valueGet = h, b.inputmask.__valueSet = i
                }
                b.inputmask._valueGet = function (a) {
                    return ma && a !== !0 ? h.call(this.el).split("").reverse().join("") : h.call(this.el)
                }, b.inputmask._valueSet = function (a, b) {
                    i.call(this.el, null === a || void 0 === a ? "" : b !== !0 && ma ? a.split("").reverse().join("") : a)
                }, void 0 === h && (h = function () {
                    return this.value
                }, i = function (a) {
                    this.value = a
                }, c(b.type), f(b))
            }
        }

        function Q(c, d, e, f) {
            function h() {
                if (g.keepStatic) {
                    for (var b = [], d = p(-1, !0), e = a.extend(!0, {}, n().validPositions), f = n().validPositions[d]; d >= 0; d--) {
                        var h = n().validPositions[d];
                        if (h) {
                            if (h.generatedInput !== !0 && /[0-9a-bA-Z]/.test(h.input) && b.push(h.input), delete n().validPositions[d], void 0 !== h.alternation && h.locator[h.alternation] !== f.locator[h.alternation])break;
                            f = h
                        }
                    }
                    if (d > -1)for (n().p = E(p(-1, !0)); b.length > 0;) {
                        var i = new a.Event("keypress");
                        i.which = b.pop().charCodeAt(0), S.call(c, i, !0, !1, !1, n().p)
                    } else n().validPositions = a.extend(!0, {}, e)
                }
            }

            if ((g.numericInput || ma) && (d === b.keyCode.BACKSPACE ? d = b.keyCode.DELETE : d === b.keyCode.DELETE && (d = b.keyCode.BACKSPACE), ma)) {
                var i = e.end;
                e.end = e.begin, e.begin = i
            }
            d === b.keyCode.BACKSPACE && (e.end - e.begin < 1 || g.insertMode === !1) ? (e.begin = F(e.begin), void 0 === n().validPositions[e.begin] || n().validPositions[e.begin].input !== g.groupSeparator && n().validPositions[e.begin].input !== g.radixPoint || e.begin--) : d === b.keyCode.DELETE && e.begin === e.end && (e.end = D(e.end, !0) ? e.end + 1 : E(e.end) + 1, void 0 === n().validPositions[e.begin] || n().validPositions[e.begin].input !== g.groupSeparator && n().validPositions[e.begin].input !== g.radixPoint || e.end++), q(e.begin, e.end, !1, f), f !== !0 && h();
            var j = p(e.begin, !0);
            j < e.begin ? n().p = E(j) : f !== !0 && (n().p = e.begin)
        }

        function R(d) {
            var e = this, f = a(e), h = d.keyCode, i = L(e);
            if (h === b.keyCode.BACKSPACE || h === b.keyCode.DELETE || l && h === b.keyCode.BACKSPACE_SAFARI || d.ctrlKey && h === b.keyCode.X && !c("cut")) d.preventDefault(), Q(e, h, i), H(e, y(!0), n().p, d, e.inputmask._valueGet() !== y().join("")), e.inputmask._valueGet() === x().join("") ? f.trigger("cleared") : O(y()) === !0 && f.trigger("complete"), g.showTooltip && (e.title = g.tooltip || n().mask); else if (h === b.keyCode.END || h === b.keyCode.PAGE_DOWN) {
                d.preventDefault();
                var j = E(p());
                g.insertMode || j !== n().maskLength || d.shiftKey || j--, L(e, d.shiftKey ? i.begin : j, j, !0)
            } else h === b.keyCode.HOME && !d.shiftKey || h === b.keyCode.PAGE_UP ? (d.preventDefault(), L(e, 0, d.shiftKey ? i.begin : 0, !0)) : (g.undoOnEscape && h === b.keyCode.ESCAPE || 90 === h && d.ctrlKey) && d.altKey !== !0 ? (J(e, !0, !1, ga.split("")), f.trigger("click")) : h !== b.keyCode.INSERT || d.shiftKey || d.ctrlKey ? g.tabThrough === !0 && h === b.keyCode.TAB ? (d.shiftKey === !0 ? (null === t(i.begin).match.fn && (i.begin = E(i.begin)), i.end = F(i.begin, !0), i.begin = F(i.end, !0)) : (i.begin = E(i.begin, !0), i.end = E(i.begin, !0), i.end < n().maskLength && i.end--), i.begin < n().maskLength && (d.preventDefault(), L(e, i.begin, i.end))) : d.shiftKey || (g.insertMode === !1 ? h === b.keyCode.RIGHT ? setTimeout(function () {
                    var a = L(e);
                    L(e, a.begin)
                }, 0) : h === b.keyCode.LEFT && setTimeout(function () {
                        var a = L(e);
                        L(e, ma ? a.begin + 1 : a.begin - 1)
                    }, 0) : setTimeout(function () {
                    ea(e)
                }, 0)) : (g.insertMode = !g.insertMode, L(e, g.insertMode || i.begin !== n().maskLength ? i.begin : i.begin - 1));
            g.onKeyDown.call(this, d, y(), L(e).begin, g), pa = a.inArray(h, g.ignorables) !== -1
        }

        function S(c, d, e, f, h) {
            var i = this, j = a(i), k = c.which || c.charCode || c.keyCode;
            if (!(d === !0 || c.ctrlKey && c.altKey) && (c.ctrlKey || c.metaKey || pa))return k === b.keyCode.ENTER && ga !== y().join("") && (ga = y().join(""), setTimeout(function () {
                j.trigger("change")
            }, 0)), !0;
            if (k) {
                46 === k && c.shiftKey === !1 && "," === g.radixPoint && (k = 44);
                var l, m = d ? {begin: h, end: h} : L(i), p = String.fromCharCode(k);
                n().writeOutBuffer = !0;
                var q = C(m, p, f);
                if (q !== !1 && (o(!0), l = void 0 !== q.caret ? q.caret : d ? q.pos + 1 : E(q.pos), n().p = l), e !== !1) {
                    var r = this;
                    if (setTimeout(function () {
                            g.onKeyValidation.call(r, k, q, g)
                        }, 0), n().writeOutBuffer && q !== !1) {
                        var s = y();
                        H(i, s, g.numericInput && void 0 === q.caret ? F(l) : l, c, d !== !0), d !== !0 && setTimeout(function () {
                            O(s) === !0 && j.trigger("complete")
                        }, 0)
                    }
                }
                if (g.showTooltip && (i.title = g.tooltip || n().mask), c.preventDefault(), d)return q.forwardPosition = l, q
            }
        }

        function T(b) {
            var c, d = this, e = b.originalEvent || b, f = a(d), h = d.inputmask._valueGet(!0), i = L(d);
            ma && (c = i.end, i.end = i.begin, i.begin = c);
            var j = h.substr(0, i.begin), k = h.substr(i.end, h.length);
            if (j === (ma ? x().reverse() : x()).slice(0, i.begin).join("") && (j = ""), k === (ma ? x().reverse() : x()).slice(i.end).join("") && (k = ""), ma && (c = j, j = k, k = c), window.clipboardData && window.clipboardData.getData) h = j + window.clipboardData.getData("Text") + k; else {
                if (!e.clipboardData || !e.clipboardData.getData)return !0;
                h = j + e.clipboardData.getData("text/plain") + k
            }
            var l = h;
            if (a.isFunction(g.onBeforePaste)) {
                if (l = g.onBeforePaste(h, g), l === !1)return b.preventDefault();
                l || (l = h)
            }
            return J(d, !1, !1, ma ? l.split("").reverse() : l.toString().split("")), H(d, y(), E(p()), b, ga !== y().join("")), O(y()) === !0 && f.trigger("complete"), b.preventDefault()
        }

        function U(c) {
            var d = this, e = d.inputmask._valueGet();
            if (y().join("") !== e) {
                var f = L(d);
                if (e = e.replace(new RegExp("(" + b.escapeRegex(x().join("")) + ")*"), ""), k) {
                    var g = e.replace(y().join(""), "");
                    if (1 === g.length) {
                        var h = new a.Event("keypress");
                        return h.which = g.charCodeAt(0), S.call(d, h, !0, !0, !1, n().validPositions[f.begin - 1] ? f.begin : f.begin - 1), !1
                    }
                }
                if (f.begin > e.length && (L(d, e.length), f = L(d)), y().length - e.length !== 1 || e.charAt(f.begin) === y()[f.begin] || e.charAt(f.begin + 1) === y()[f.begin] || D(f.begin)) {
                    for (var i = p() + 1, j = y().slice(i).join(""); null === e.match(b.escapeRegex(j) + "$");)j = j.slice(1);
                    e = e.replace(j, ""), e = e.split(""), J(d, !0, !1, e, c, f.begin < i), O(y()) === !0 && a(d).trigger("complete")
                } else c.keyCode = b.keyCode.BACKSPACE, R.call(d, c);
                c.preventDefault()
            }
        }

        function V(b) {
            var c = this, d = c.inputmask._valueGet();
            J(c, !0, !1, (a.isFunction(g.onBeforeMask) ? g.onBeforeMask(d, g) || d : d).split("")), ga = y().join(""), (g.clearMaskOnLostFocus || g.clearIncomplete) && c.inputmask._valueGet() === x().join("") && c.inputmask._valueSet("")
        }

        function W(a) {
            var b = this, c = b.inputmask._valueGet();
            g.showMaskOnFocus && (!g.showMaskOnHover || g.showMaskOnHover && "" === c) ? b.inputmask._valueGet() !== y().join("") && H(b, y(), E(p())) : qa === !1 && L(b, E(p())), g.positionCaretOnTab === !0 && setTimeout(function () {
                Y.apply(this, [a])
            }, 0), ga = y().join("")
        }

        function X(a) {
            var b = this;
            if (qa = !1, g.clearMaskOnLostFocus && document.activeElement !== b) {
                var c = y().slice(), d = b.inputmask._valueGet();
                d !== b.getAttribute("placeholder") && "" !== d && (p() === -1 && d === x().join("") ? c = [] : N(c), H(b, c))
            }
        }

        function Y(b) {
            function c(b) {
                if ("" !== g.radixPoint) {
                    var c = n().validPositions;
                    if (void 0 === c[b] || c[b].input === I(b)) {
                        if (b < E(-1))return !0;
                        var d = a.inArray(g.radixPoint, y());
                        if (d !== -1) {
                            for (var e in c)if (d < e && c[e].input !== I(e))return !1;
                            return !0
                        }
                    }
                }
                return !1
            }

            var d = this;
            setTimeout(function () {
                if (document.activeElement === d) {
                    var b = L(d);
                    if (b.begin === b.end)switch (g.positionCaretOnClick) {
                        case"none":
                            break;
                        case"radixFocus":
                            if (c(b.begin)) {
                                var e = a.inArray(g.radixPoint, y().join(""));
                                L(d, g.numericInput ? E(e) : e);
                                break
                            }
                        default:
                            var f = b.begin, h = p(f, !0), i = E(h);
                            if (f < i) L(d, D(f) || D(f - 1) ? f : E(f)); else {
                                var j = I(i);
                                ("" !== j && y()[i] !== j && t(i).match.optionalQuantifier !== !0 || !D(i, !0) && t(i).match.def === j) && (i = E(i)), L(d, i)
                            }
                    }
                }
            }, 0)
        }

        function Z(a) {
            var b = this;
            setTimeout(function () {
                L(b, 0, E(p()))
            }, 0)
        }

        function $(c) {
            var d = this, e = a(d), f = L(d), h = c.originalEvent || c, i = window.clipboardData || h.clipboardData,
                j = ma ? y().slice(f.end, f.begin) : y().slice(f.begin, f.end);
            i.setData("text", ma ? j.reverse().join("") : j.join("")), document.execCommand && document.execCommand("copy"), Q(d, b.keyCode.DELETE, f), H(d, y(), n().p, c, ga !== y().join("")), d.inputmask._valueGet() === x().join("") && e.trigger("cleared"), g.showTooltip && (d.title = g.tooltip || n().mask)
        }

        function _(b) {
            var c = a(this), d = this;
            if (d.inputmask) {
                var e = d.inputmask._valueGet(), f = y().slice();
                ga !== f.join("") && setTimeout(function () {
                    c.trigger("change"), ga = f.join("")
                }, 0), "" !== e && (g.clearMaskOnLostFocus && (p() === -1 && e === x().join("") ? f = [] : N(f)), O(f) === !1 && (setTimeout(function () {
                    c.trigger("incomplete")
                }, 0), g.clearIncomplete && (o(), f = g.clearMaskOnLostFocus ? [] : x().slice())), H(d, f, void 0, b))
            }
        }

        function aa(a) {
            var b = this;
            qa = !0, document.activeElement !== b && g.showMaskOnHover && b.inputmask._valueGet() !== y().join("") && H(b, y())
        }

        function ba(a) {
            ga !== y().join("") && ia.trigger("change"), g.clearMaskOnLostFocus && p() === -1 && ha.inputmask._valueGet && ha.inputmask._valueGet() === x().join("") && ha.inputmask._valueSet(""), g.removeMaskOnSubmit && (ha.inputmask._valueSet(ha.inputmask.unmaskedvalue(), !0), setTimeout(function () {
                H(ha, y())
            }, 0))
        }

        function ca(a) {
            setTimeout(function () {
                ia.trigger("setvalue")
            }, 0)
        }

        function da(b) {
            function c(a) {
                var c, d = document.createElement("span");
                for (var e in f)isNaN(e) && e.indexOf("font") !== -1 && (d.style[e] = f[e]);
                d.style.textTransform = f.textTransform, d.style.letterSpacing = f.letterSpacing, d.style.position = "absolute", d.style.height = "auto", d.style.width = "auto", d.style.visibility = "hidden", d.style.whiteSpace = "nowrap", document.body.appendChild(d);
                var g, h = b.inputmask._valueGet(), i = 0;
                for (c = 0, g = h.length; c <= g; c++) {
                    if (d.innerHTML += h.charAt(c) || "_", d.offsetWidth >= a) {
                        var j = a - i, k = d.offsetWidth - a;
                        d.innerHTML = h.charAt(c), j -= d.offsetWidth / 3, c = j < k ? c - 1 : c;
                        break
                    }
                    i = d.offsetWidth
                }
                return document.body.removeChild(d), c
            }

            function d() {
                ka.style.position = "absolute", ka.style.top = e.top + parseInt(f.borderTopWidth) + "px", ka.style.left = e.left + parseInt(f.borderLeftWidth) + "px", ka.style.width = parseInt(b.offsetWidth) - parseInt(f.paddingLeft) - parseInt(f.paddingRight) - parseInt(f.borderLeftWidth) - parseInt(f.borderRightWidth) + "px", ka.style.height = parseInt(b.offsetHeight) - parseInt(f.paddingTop) - parseInt(f.paddingBottom) - parseInt(f.borderTopWidth) - parseInt(f.borderBottomWidth) + "px", ka.style.lineHeight = ka.style.height, ka.style.border = ""
            }

            var e = a(b).position(), f = (b.ownerDocument.defaultView || window).getComputedStyle(b, null);
            b.parentNode;
            ka = document.createElement("div"), document.body.appendChild(ka);
            for (var g in f)isNaN(g) && "cssText" !== g && g.indexOf("webkit") == -1 && (ka.style[g] = f[g]);
            d(), a(window).on("resize", function (c) {
                e = a(b).position(), f = (b.ownerDocument.defaultView || window).getComputedStyle(b, null), d()
            }), a(ka).on("mouseenter", function (a) {
                aa.call(b, a)
            }), a(ka).on("mouseleave", function (a) {
                X.call(b, a)
            }), a(ka).on("click", function (d) {
                b.focus(), L(b, c(d.clientX)), a(b).trigger("click")
            })
        }

        function ea(a, b, c) {
            function d() {
                f || null !== i.fn && void 0 !== j.input ? f && null !== i.fn && void 0 !== j.input && (f = !1, e += "</span>") : (f = !0, e += "<span class='im-static''>")
            }

            if (void 0 !== ka) {
                b = b || y(), void 0 === c ? c = L(a) : void 0 === c.begin && (c = {begin: c, end: c});
                var e = "", f = !1;
                if ("" != b) {
                    var h, i, j, k = 0, l = p();
                    do k === c.begin && document.activeElement === a && (e += "<span class='im-caret' style='border-right-width: 1px;border-right-style: solid;'></span>"), n().validPositions[k] ? (j = n().validPositions[k], i = j.match, h = j.locator.slice(), d(), e += j.input) : (j = s(k, h, k - 1), i = j.match, h = j.locator.slice(), (g.jitMasking === !1 || k < l || Number.isFinite(g.jitMasking) && g.jitMasking > k) && (d(), e += I(k, i))), k++; while ((void 0 === ja || k < ja) && (null !== i.fn || "" !== i.def) || l > k)
                }
                ka.innerHTML = e
            }
        }

        function fa(b) {
            if (ha = b, ia = a(ha), g.showTooltip && (ha.title = g.tooltip || n().mask), ("rtl" === ha.dir || g.rightAlign) && (ha.style.textAlign = "right"), ("rtl" === ha.dir || g.numericInput) && (ha.dir = "ltr", ha.removeAttribute("dir"), ha.inputmask.isRTL = !0, ma = !0), g.colorMask === !0 && da(ha), m && (ha.hasOwnProperty("inputmode") && (ha.inputmode = g.inputmode, ha.setAttribute("inputmode", g.inputmode)), "rtfm" === g.androidHack && (g.colorMask !== !0 && da(ha), ha.type = "password")), ra.off(ha), P(ha), d(ha, g) && (ra.on(ha, "submit", ba), ra.on(ha, "reset", ca), ra.on(ha, "mouseenter", aa), ra.on(ha, "blur", _), ra.on(ha, "focus", W), ra.on(ha, "mouseleave", X), ra.on(ha, "click", Y), ra.on(ha, "dblclick", Z), ra.on(ha, "paste", T), ra.on(ha, "dragdrop", T), ra.on(ha, "drop", T), ra.on(ha, "cut", $), ra.on(ha, "complete", g.oncomplete), ra.on(ha, "incomplete", g.onincomplete), ra.on(ha, "cleared", g.oncleared), g.inputEventOnly !== !0 && (ra.on(ha, "keydown", R), ra.on(ha, "keypress", S)), ra.on(ha, "compositionstart", a.noop), ra.on(ha, "compositionupdate", a.noop), ra.on(ha, "compositionend", a.noop), ra.on(ha, "keyup", a.noop), ra.on(ha, "input", U)), ra.on(ha, "setvalue", V), x(), "" !== ha.inputmask._valueGet() || g.clearMaskOnLostFocus === !1 || document.activeElement === ha) {
                var c = a.isFunction(g.onBeforeMask) ? g.onBeforeMask(ha.inputmask._valueGet(), g) || ha.inputmask._valueGet() : ha.inputmask._valueGet();
                J(ha, !0, !1, c.split(""));
                var e = y().slice();
                ga = e.join(""), O(e) === !1 && g.clearIncomplete && o(), g.clearMaskOnLostFocus && document.activeElement !== ha && (p() === -1 ? e = [] : N(e)), H(ha, e), document.activeElement === ha && L(ha, E(p()))
            }
        }

        var ga, ha, ia, ja, ka, la, ma = !1, na = !1, oa = !1, pa = !1, qa = !0, ra = {
            on: function (c, d, e) {
                var f = function (c) {
                    if (void 0 === this.inputmask && "FORM" !== this.nodeName) {
                        var d = a.data(this, "_inputmask_opts");
                        d ? new b(d).mask(this) : ra.off(this)
                    } else {
                        if ("setvalue" === c.type || !(this.disabled || this.readOnly && !("keydown" === c.type && c.ctrlKey && 67 === c.keyCode || g.tabThrough === !1 && c.keyCode === b.keyCode.TAB))) {
                            switch (c.type) {
                                case"input":
                                    if (oa === !0)return oa = !1, c.preventDefault();
                                    break;
                                case"keydown":
                                    na = !1, oa = !1;
                                    break;
                                case"keypress":
                                    if (na === !0)return c.preventDefault();
                                    na = !0;
                                    break;
                                case"click":
                                    if (k || l) {
                                        var f = this, h = arguments;
                                        return setTimeout(function () {
                                            e.apply(f, h)
                                        }, 0), !1
                                    }
                            }
                            var i = e.apply(this, arguments);
                            return i === !1 && (c.preventDefault(), c.stopPropagation()), i
                        }
                        c.preventDefault()
                    }
                };
                c.inputmask.events[d] = c.inputmask.events[d] || [], c.inputmask.events[d].push(f), a.inArray(d, ["submit", "reset"]) !== -1 ? null != c.form && a(c.form).on(d, f) : a(c).on(d, f)
            }, off: function (b, c) {
                if (b.inputmask && b.inputmask.events) {
                    var d;
                    c ? (d = [], d[c] = b.inputmask.events[c]) : d = b.inputmask.events, a.each(d, function (c, d) {
                        for (; d.length > 0;) {
                            var e = d.pop();
                            a.inArray(c, ["submit", "reset"]) !== -1 ? null != b.form && a(b.form).off(c, e) : a(b).off(c, e)
                        }
                        delete b.inputmask.events[c]
                    })
                }
            }
        };
        if (void 0 !== e)switch (e.action) {
            case"isComplete":
                return ha = e.el, O(y());
            case"unmaskedvalue":
                return ha = e.el, void 0 !== ha && void 0 !== ha.inputmask ? (f = ha.inputmask.maskset, g = ha.inputmask.opts, ma = ha.inputmask.isRTL) : (la = e.value, g.numericInput && (ma = !0), la = (a.isFunction(g.onBeforeMask) ? g.onBeforeMask(la, g) || la : la).split(""), J(void 0, !1, !1, ma ? la.reverse() : la), a.isFunction(g.onBeforeWrite) && g.onBeforeWrite(void 0, y(), 0, g)), K(ha);
            case"mask":
                ha = e.el, f = ha.inputmask.maskset, g = ha.inputmask.opts, ma = ha.inputmask.isRTL, fa(ha);
                break;
            case"format":
                return g.numericInput && (ma = !0), la = (a.isFunction(g.onBeforeMask) ? g.onBeforeMask(e.value, g) || e.value : e.value).split(""), J(void 0, !1, !1, ma ? la.reverse() : la), a.isFunction(g.onBeforeWrite) && g.onBeforeWrite(void 0, y(), 0, g), e.metadata ? {
                    value: ma ? y().slice().reverse().join("") : y().join(""),
                    metadata: h({action: "getmetadata"}, f, g)
                } : ma ? y().slice().reverse().join("") : y().join("");
            case"isValid":
                g.numericInput && (ma = !0), e.value ? (la = e.value.split(""), J(void 0, !1, !0, ma ? la.reverse() : la)) : e.value = y().join("");
                for (var sa = y(), ta = M(), ua = sa.length - 1; ua > ta && !D(ua); ua--);
                return sa.splice(ta, ua + 1 - ta), O(sa) && e.value === y().join("");
            case"getemptymask":
                return x().join("");
            case"remove":
                ha = e.el, ia = a(ha), f = ha.inputmask.maskset, g = ha.inputmask.opts, ha.inputmask._valueSet(K(ha)), ra.off(ha);
                var va;
                Object.getOwnPropertyDescriptor && Object.getPrototypeOf ? (va = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(ha), "value"), va && ha.inputmask.__valueGet && Object.defineProperty(ha, "value", {
                    get: ha.inputmask.__valueGet,
                    set: ha.inputmask.__valueSet,
                    configurable: !0
                })) : document.__lookupGetter__ && ha.__lookupGetter__("value") && ha.inputmask.__valueGet && (ha.__defineGetter__("value", ha.inputmask.__valueGet), ha.__defineSetter__("value", ha.inputmask.__valueSet)), ha.inputmask = void 0;
                break;
            case"getmetadata":
                if (a.isArray(f.metadata)) {
                    for (var wa, xa = p(void 0, !0), ya = xa; ya >= 0; ya--)if (n().validPositions[ya] && void 0 !== n().validPositions[ya].alternation) {
                        wa = n().validPositions[ya].alternation;
                        break
                    }
                    return void 0 !== wa ? f.metadata[n().validPositions[ya].locator[wa]] : []
                }
                return f.metadata
        }
    }

    b.prototype = {
        defaults: {
            placeholder: "_",
            optionalmarker: {start: "[", end: "]"},
            quantifiermarker: {start: "{", end: "}"},
            groupmarker: {start: "(", end: ")"},
            alternatormarker: "|",
            escapeChar: "\\",
            mask: null,
            oncomplete: a.noop,
            onincomplete: a.noop,
            oncleared: a.noop,
            repeat: 0,
            greedy: !0,
            autoUnmask: !1,
            removeMaskOnSubmit: !1,
            clearMaskOnLostFocus: !0,
            insertMode: !0,
            clearIncomplete: !1,
            aliases: {},
            alias: null,
            onKeyDown: a.noop,
            onBeforeMask: null,
            onBeforePaste: function (b, c) {
                return a.isFunction(c.onBeforeMask) ? c.onBeforeMask(b, c) : b
            },
            onBeforeWrite: null,
            onUnMask: null,
            showMaskOnFocus: !0,
            showMaskOnHover: !0,
            onKeyValidation: a.noop,
            skipOptionalPartCharacter: " ",
            showTooltip: !1,
            tooltip: void 0,
            numericInput: !1,
            rightAlign: !1,
            undoOnEscape: !0,
            radixPoint: "",
            radixPointDefinitionSymbol: void 0,
            groupSeparator: "",
            keepStatic: null,
            positionCaretOnTab: !0,
            tabThrough: !1,
            supportsInputType: ["text", "tel", "password"],
            definitions: {
                9: {validator: "[0-9]", cardinality: 1, definitionSymbol: "*"},
                a: {validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", cardinality: 1, definitionSymbol: "*"},
                "*": {validator: "[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", cardinality: 1}
            },
            ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123],
            isComplete: null,
            canClearPosition: a.noop,
            postValidation: null,
            staticDefinitionSymbol: void 0,
            jitMasking: !1,
            nullable: !0,
            inputEventOnly: !1,
            noValuePatching: !1,
            positionCaretOnClick: "lvp",
            casing: null,
            inputmode: "verbatim",
            colorMask: !1,
            androidHack: !1
        }, masksCache: {}, mask: function (c) {
            var d = this;
            return "string" == typeof c && (c = document.getElementById(c) || document.querySelectorAll(c)), c = c.nodeName ? [c] : c, a.each(c, function (c, e) {
                var i = a.extend(!0, {}, d.opts);
                f(e, i, a.extend(!0, {}, d.userOptions));
                var j = g(i, d.noMasksCache);
                void 0 !== j && (void 0 !== e.inputmask && e.inputmask.remove(), e.inputmask = new b, e.inputmask.opts = i, e.inputmask.noMasksCache = d.noMasksCache, e.inputmask.userOptions = a.extend(!0, {}, d.userOptions), e.inputmask.el = e, e.inputmask.maskset = j, e.inputmask.isRTL = !1, a.data(e, "_inputmask_opts", i), h({
                    action: "mask",
                    el: e
                }))
            }), c && c[0] ? c[0].inputmask || this : this
        }, option: function (b, c) {
            return "string" == typeof b ? this.opts[b] : "object" == typeof b ? (a.extend(this.userOptions, b), this.el && c !== !0 && this.mask(this.el), this) : void 0
        }, unmaskedvalue: function (a) {
            return h({
                action: "unmaskedvalue",
                el: this.el,
                value: a
            }, this.el && this.el.inputmask ? this.el.inputmask.maskset : g(this.opts, this.noMasksCache), this.opts)
        }, remove: function () {
            if (this.el)return h({action: "remove", el: this.el}), this.el.inputmask = void 0, this.el
        }, getemptymask: function () {
            return h({action: "getemptymask"}, this.maskset || g(this.opts, this.noMasksCache), this.opts)
        }, hasMaskedValue: function () {
            return !this.opts.autoUnmask
        }, isComplete: function () {
            return h({action: "isComplete", el: this.el}, this.maskset || g(this.opts, this.noMasksCache), this.opts)
        }, getmetadata: function () {
            return h({action: "getmetadata"}, this.maskset || g(this.opts, this.noMasksCache), this.opts)
        }, isValid: function (a) {
            return h({action: "isValid", value: a}, this.maskset || g(this.opts, this.noMasksCache), this.opts)
        }, format: function (a, b) {
            return h({
                action: "format",
                value: a,
                metadata: b
            }, this.maskset || g(this.opts, this.noMasksCache), this.opts)
        }
    }, b.extendDefaults = function (c) {
        a.extend(!0, b.prototype.defaults, c)
    }, b.extendDefinitions = function (c) {
        a.extend(!0, b.prototype.defaults.definitions, c)
    }, b.extendAliases = function (c) {
        a.extend(!0, b.prototype.defaults.aliases, c)
    }, b.format = function (a, c, d) {
        return b(c).format(a, d)
    }, b.unmask = function (a, c) {
        return b(c).unmaskedvalue(a)
    }, b.isValid = function (a, c) {
        return b(c).isValid(a)
    }, b.remove = function (b) {
        a.each(b, function (a, b) {
            b.inputmask && b.inputmask.remove()
        })
    }, b.escapeRegex = function (a) {
        var b = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"];
        return a.replace(new RegExp("(\\" + b.join("|\\") + ")", "gim"), "\\$1")
    }, b.keyCode = {
        ALT: 18,
        BACKSPACE: 8,
        BACKSPACE_SAFARI: 127,
        CAPS_LOCK: 20,
        COMMA: 188,
        COMMAND: 91,
        COMMAND_LEFT: 91,
        COMMAND_RIGHT: 93,
        CONTROL: 17,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        INSERT: 45,
        LEFT: 37,
        MENU: 93,
        NUMPAD_ADD: 107,
        NUMPAD_DECIMAL: 110,
        NUMPAD_DIVIDE: 111,
        NUMPAD_ENTER: 108,
        NUMPAD_MULTIPLY: 106,
        NUMPAD_SUBTRACT: 109,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SHIFT: 16,
        SPACE: 32,
        TAB: 9,
        UP: 38,
        WINDOWS: 91,
        X: 88
    };
    var i = navigator.userAgent, j = /mobile/i.test(i), k = /iemobile/i.test(i), l = /iphone/i.test(i) && !k,
        m = /android/i.test(i) && !k;
    return window.Inputmask = b, b
}(jQuery), function (a, b) {
    return void 0 === a.fn.inputmask && (a.fn.inputmask = function (c, d) {
        var e, f = this[0];
        if (void 0 === d && (d = {}), "string" == typeof c)switch (c) {
            case"unmaskedvalue":
                return f && f.inputmask ? f.inputmask.unmaskedvalue() : a(f).val();
            case"remove":
                return this.each(function () {
                    this.inputmask && this.inputmask.remove()
                });
            case"getemptymask":
                return f && f.inputmask ? f.inputmask.getemptymask() : "";
            case"hasMaskedValue":
                return !(!f || !f.inputmask) && f.inputmask.hasMaskedValue();
            case"isComplete":
                return !f || !f.inputmask || f.inputmask.isComplete();
            case"getmetadata":
                return f && f.inputmask ? f.inputmask.getmetadata() : void 0;
            case"setvalue":
                a(f).val(d), f && void 0 === f.inputmask && a(f).triggerHandler("setvalue");
                break;
            case"option":
                if ("string" != typeof d)return this.each(function () {
                    if (void 0 !== this.inputmask)return this.inputmask.option(d)
                });
                if (f && void 0 !== f.inputmask)return f.inputmask.option(d);
                break;
            default:
                return d.alias = c, e = new b(d), this.each(function () {
                    e.mask(this)
                })
        } else {
            if ("object" == typeof c)return e = new b(c), void 0 === c.mask && void 0 === c.alias ? this.each(function () {
                return void 0 !== this.inputmask ? this.inputmask.option(c) : void e.mask(this)
            }) : this.each(function () {
                e.mask(this)
            });
            if (void 0 === c)return this.each(function () {
                e = new b(d), e.mask(this)
            })
        }
    }), a.fn.inputmask
}(jQuery, Inputmask), function (a, b) {
    return b.extendDefinitions({
        h: {
            validator: "[01][0-9]|2[0-3]",
            cardinality: 2,
            prevalidator: [{validator: "[0-2]", cardinality: 1}]
        },
        s: {validator: "[0-5][0-9]", cardinality: 2, prevalidator: [{validator: "[0-5]", cardinality: 1}]},
        d: {validator: "0[1-9]|[12][0-9]|3[01]", cardinality: 2, prevalidator: [{validator: "[0-3]", cardinality: 1}]},
        m: {validator: "0[1-9]|1[012]", cardinality: 2, prevalidator: [{validator: "[01]", cardinality: 1}]},
        y: {
            validator: "(19|20)\\d{2}",
            cardinality: 4,
            prevalidator: [{validator: "[12]", cardinality: 1}, {
                validator: "(19|20)",
                cardinality: 2
            }, {validator: "(19|20)\\d", cardinality: 3}]
        }
    }), b.extendAliases({
        "dd/mm/yyyy": {
            mask: "1/2/y",
            placeholder: "dd/mm/yyyy",
            regex: {
                val1pre: new RegExp("[0-3]"), val1: new RegExp("0[1-9]|[12][0-9]|3[01]"), val2pre: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|[12][0-9]|3[01])" + c + "[01])")
                }, val2: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|[12][0-9])" + c + "(0[1-9]|1[012]))|(30" + c + "(0[13-9]|1[012]))|(31" + c + "(0[13578]|1[02]))")
                }
            },
            leapday: "29/02/",
            separator: "/",
            yearrange: {minyear: 1900, maxyear: 2099},
            isInYearRange: function (a, b, c) {
                if (isNaN(a))return !1;
                var d = parseInt(a.concat(b.toString().slice(a.length))),
                    e = parseInt(a.concat(c.toString().slice(a.length)));
                return !isNaN(d) && (b <= d && d <= c) || !isNaN(e) && (b <= e && e <= c)
            },
            determinebaseyear: function (a, b, c) {
                var d = (new Date).getFullYear();
                if (a > d)return a;
                if (b < d) {
                    for (var e = b.toString().slice(0, 2), f = b.toString().slice(2, 4); b < e + c;)e--;
                    var g = e + f;
                    return a > g ? a : g
                }
                if (a <= d && d <= b) {
                    for (var h = d.toString().slice(0, 2); b < h + c;)h--;
                    var i = h + c;
                    return i < a ? a : i
                }
                return d
            },
            onKeyDown: function (c, d, e, f) {
                var g = a(this);
                if (c.ctrlKey && c.keyCode === b.keyCode.RIGHT) {
                    var h = new Date;
                    g.val(h.getDate().toString() + (h.getMonth() + 1).toString() + h.getFullYear().toString()), g.trigger("setvalue")
                }
            },
            getFrontValue: function (a, b, c) {
                for (var d = 0, e = 0, f = 0; f < a.length && "2" !== a.charAt(f); f++) {
                    var g = c.definitions[a.charAt(f)];
                    g ? (d += e, e = g.cardinality) : e++
                }
                return b.join("").substr(d, e)
            },
            definitions: {
                1: {
                    validator: function (a, b, c, d, e) {
                        var f = e.regex.val1.test(a);
                        return d || f || a.charAt(1) !== e.separator && "-./".indexOf(a.charAt(1)) === -1 || !(f = e.regex.val1.test("0" + a.charAt(0))) ? f : (b.buffer[c - 1] = "0", {
                            refreshFromBuffer: {
                                start: c - 1,
                                end: c
                            }, pos: c, c: a.charAt(0)
                        })
                    }, cardinality: 2, prevalidator: [{
                        validator: function (a, b, c, d, e) {
                            var f = a;
                            isNaN(b.buffer[c + 1]) || (f += b.buffer[c + 1]);
                            var g = 1 === f.length ? e.regex.val1pre.test(f) : e.regex.val1.test(f);
                            if (!d && !g) {
                                if (g = e.regex.val1.test(a + "0"))return b.buffer[c] = a, b.buffer[++c] = "0", {
                                    pos: c,
                                    c: "0"
                                };
                                if (g = e.regex.val1.test("0" + a))return b.buffer[c] = "0", c++, {pos: c}
                            }
                            return g
                        }, cardinality: 1
                    }]
                }, 2: {
                    validator: function (a, b, c, d, e) {
                        var f = e.getFrontValue(b.mask, b.buffer, e);
                        f.indexOf(e.placeholder[0]) !== -1 && (f = "01" + e.separator);
                        var g = e.regex.val2(e.separator).test(f + a);
                        if (!d && !g && (a.charAt(1) === e.separator || "-./".indexOf(a.charAt(1)) !== -1) && (g = e.regex.val2(e.separator).test(f + "0" + a.charAt(0))))return b.buffer[c - 1] = "0", {
                            refreshFromBuffer: {
                                start: c - 1,
                                end: c
                            }, pos: c, c: a.charAt(0)
                        };
                        if (e.mask.indexOf("2") === e.mask.length - 1 && g) {
                            var h = b.buffer.join("").substr(4, 4) + a;
                            if (h !== e.leapday)return !0;
                            var i = parseInt(b.buffer.join("").substr(0, 4), 10);
                            return i % 4 === 0 && (i % 100 !== 0 || i % 400 === 0)
                        }
                        return g
                    }, cardinality: 2, prevalidator: [{
                        validator: function (a, b, c, d, e) {
                            isNaN(b.buffer[c + 1]) || (a += b.buffer[c + 1]);
                            var f = e.getFrontValue(b.mask, b.buffer, e);
                            f.indexOf(e.placeholder[0]) !== -1 && (f = "01" + e.separator);
                            var g = 1 === a.length ? e.regex.val2pre(e.separator).test(f + a) : e.regex.val2(e.separator).test(f + a);
                            return d || g || !(g = e.regex.val2(e.separator).test(f + "0" + a)) ? g : (b.buffer[c] = "0", c++, {pos: c})
                        }, cardinality: 1
                    }]
                }, y: {
                    validator: function (a, b, c, d, e) {
                        if (e.isInYearRange(a, e.yearrange.minyear, e.yearrange.maxyear)) {
                            var f = b.buffer.join("").substr(0, 6);
                            if (f !== e.leapday)return !0;
                            var g = parseInt(a, 10);
                            return g % 4 === 0 && (g % 100 !== 0 || g % 400 === 0)
                        }
                        return !1
                    }, cardinality: 4, prevalidator: [{
                        validator: function (a, b, c, d, e) {
                            var f = e.isInYearRange(a, e.yearrange.minyear, e.yearrange.maxyear);
                            if (!d && !f) {
                                var g = e.determinebaseyear(e.yearrange.minyear, e.yearrange.maxyear, a + "0").toString().slice(0, 1);
                                if (f = e.isInYearRange(g + a, e.yearrange.minyear, e.yearrange.maxyear))return b.buffer[c++] = g.charAt(0), {pos: c};
                                if (g = e.determinebaseyear(e.yearrange.minyear, e.yearrange.maxyear, a + "0").toString().slice(0, 2), f = e.isInYearRange(g + a, e.yearrange.minyear, e.yearrange.maxyear))return b.buffer[c++] = g.charAt(0), b.buffer[c++] = g.charAt(1), {pos: c}
                            }
                            return f
                        }, cardinality: 1
                    }, {
                        validator: function (a, b, c, d, e) {
                            var f = e.isInYearRange(a, e.yearrange.minyear, e.yearrange.maxyear);
                            if (!d && !f) {
                                var g = e.determinebaseyear(e.yearrange.minyear, e.yearrange.maxyear, a).toString().slice(0, 2);
                                if (f = e.isInYearRange(a[0] + g[1] + a[1], e.yearrange.minyear, e.yearrange.maxyear))return b.buffer[c++] = g.charAt(1), {pos: c};
                                if (g = e.determinebaseyear(e.yearrange.minyear, e.yearrange.maxyear, a).toString().slice(0, 2), e.isInYearRange(g + a, e.yearrange.minyear, e.yearrange.maxyear)) {
                                    var h = b.buffer.join("").substr(0, 6);
                                    if (h !== e.leapday) f = !0; else {
                                        var i = parseInt(a, 10);
                                        f = i % 4 === 0 && (i % 100 !== 0 || i % 400 === 0)
                                    }
                                } else f = !1;
                                if (f)return b.buffer[c - 1] = g.charAt(0), b.buffer[c++] = g.charAt(1), b.buffer[c++] = a.charAt(0), {
                                    refreshFromBuffer: {
                                        start: c - 3,
                                        end: c
                                    }, pos: c
                                }
                            }
                            return f
                        }, cardinality: 2
                    }, {
                        validator: function (a, b, c, d, e) {
                            return e.isInYearRange(a, e.yearrange.minyear, e.yearrange.maxyear)
                        }, cardinality: 3
                    }]
                }
            },
            insertMode: !1,
            autoUnmask: !1
        },
        "mm/dd/yyyy": {
            placeholder: "mm/dd/yyyy", alias: "dd/mm/yyyy", regex: {
                val2pre: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[13-9]|1[012])" + c + "[0-3])|(02" + c + "[0-2])")
                }, val2: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|1[012])" + c + "(0[1-9]|[12][0-9]))|((0[13-9]|1[012])" + c + "30)|((0[13578]|1[02])" + c + "31)")
                }, val1pre: new RegExp("[01]"), val1: new RegExp("0[1-9]|1[012]")
            }, leapday: "02/29/", onKeyDown: function (c, d, e, f) {
                var g = a(this);
                if (c.ctrlKey && c.keyCode === b.keyCode.RIGHT) {
                    var h = new Date;
                    g.val((h.getMonth() + 1).toString() + h.getDate().toString() + h.getFullYear().toString()), g.trigger("setvalue")
                }
            }
        },
        "yyyy/mm/dd": {
            mask: "y/1/2",
            placeholder: "yyyy/mm/dd",
            alias: "mm/dd/yyyy",
            leapday: "/02/29",
            onKeyDown: function (c, d, e, f) {
                var g = a(this);
                if (c.ctrlKey && c.keyCode === b.keyCode.RIGHT) {
                    var h = new Date;
                    g.val(h.getFullYear().toString() + (h.getMonth() + 1).toString() + h.getDate().toString()), g.trigger("setvalue")
                }
            }
        },
        "dd.mm.yyyy": {
            mask: "1.2.y",
            placeholder: "dd.mm.yyyy",
            leapday: "29.02.",
            separator: ".",
            alias: "dd/mm/yyyy"
        },
        "dd-mm-yyyy": {
            mask: "1-2-y",
            placeholder: "dd-mm-yyyy",
            leapday: "29-02-",
            separator: "-",
            alias: "dd/mm/yyyy"
        },
        "mm.dd.yyyy": {
            mask: "1.2.y",
            placeholder: "mm.dd.yyyy",
            leapday: "02.29.",
            separator: ".",
            alias: "mm/dd/yyyy"
        },
        "mm-dd-yyyy": {
            mask: "1-2-y",
            placeholder: "mm-dd-yyyy",
            leapday: "02-29-",
            separator: "-",
            alias: "mm/dd/yyyy"
        },
        "yyyy.mm.dd": {
            mask: "y.1.2",
            placeholder: "yyyy.mm.dd",
            leapday: ".02.29",
            separator: ".",
            alias: "yyyy/mm/dd"
        },
        "yyyy-mm-dd": {
            mask: "y-1-2",
            placeholder: "yyyy-mm-dd",
            leapday: "-02-29",
            separator: "-",
            alias: "yyyy/mm/dd"
        },
        datetime: {
            mask: "1/2/y h:s",
            placeholder: "dd/mm/yyyy hh:mm",
            alias: "dd/mm/yyyy",
            regex: {
                hrspre: new RegExp("[012]"),
                hrs24: new RegExp("2[0-4]|1[3-9]"),
                hrs: new RegExp("[01][0-9]|2[0-4]"),
                ampm: new RegExp("^[a|p|A|P][m|M]"),
                mspre: new RegExp("[0-5]"),
                ms: new RegExp("[0-5][0-9]")
            },
            timeseparator: ":",
            hourFormat: "24",
            definitions: {
                h: {
                    validator: function (a, b, c, d, e) {
                        if ("24" === e.hourFormat && 24 === parseInt(a, 10))return b.buffer[c - 1] = "0", b.buffer[c] = "0", {
                            refreshFromBuffer: {
                                start: c - 1,
                                end: c
                            }, c: "0"
                        };
                        var f = e.regex.hrs.test(a);
                        if (!d && !f && (a.charAt(1) === e.timeseparator || "-.:".indexOf(a.charAt(1)) !== -1) && (f = e.regex.hrs.test("0" + a.charAt(0))))return b.buffer[c - 1] = "0", b.buffer[c] = a.charAt(0), c++, {
                            refreshFromBuffer: {
                                start: c - 2,
                                end: c
                            }, pos: c, c: e.timeseparator
                        };
                        if (f && "24" !== e.hourFormat && e.regex.hrs24.test(a)) {
                            var g = parseInt(a, 10);
                            return 24 === g ? (b.buffer[c + 5] = "a", b.buffer[c + 6] = "m") : (b.buffer[c + 5] = "p", b.buffer[c + 6] = "m"), g -= 12, g < 10 ? (b.buffer[c] = g.toString(), b.buffer[c - 1] = "0") : (b.buffer[c] = g.toString().charAt(1), b.buffer[c - 1] = g.toString().charAt(0)), {
                                refreshFromBuffer: {
                                    start: c - 1,
                                    end: c + 6
                                }, c: b.buffer[c]
                            }
                        }
                        return f
                    }, cardinality: 2, prevalidator: [{
                        validator: function (a, b, c, d, e) {
                            var f = e.regex.hrspre.test(a);
                            return d || f || !(f = e.regex.hrs.test("0" + a)) ? f : (b.buffer[c] = "0", c++, {pos: c})
                        }, cardinality: 1
                    }]
                }, s: {
                    validator: "[0-5][0-9]", cardinality: 2, prevalidator: [{
                        validator: function (a, b, c, d, e) {
                            var f = e.regex.mspre.test(a);
                            return d || f || !(f = e.regex.ms.test("0" + a)) ? f : (b.buffer[c] = "0", c++, {pos: c})
                        }, cardinality: 1
                    }]
                }, t: {
                    validator: function (a, b, c, d, e) {
                        return e.regex.ampm.test(a + "m")
                    }, casing: "lower", cardinality: 1
                }
            },
            insertMode: !1,
            autoUnmask: !1
        },
        datetime12: {mask: "1/2/y h:s t\\m", placeholder: "dd/mm/yyyy hh:mm xm", alias: "datetime", hourFormat: "12"},
        "mm/dd/yyyy hh:mm xm": {
            mask: "1/2/y h:s t\\m",
            placeholder: "mm/dd/yyyy hh:mm xm",
            alias: "datetime12",
            regex: {
                val2pre: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[13-9]|1[012])" + c + "[0-3])|(02" + c + "[0-2])")
                }, val2: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|1[012])" + c + "(0[1-9]|[12][0-9]))|((0[13-9]|1[012])" + c + "30)|((0[13578]|1[02])" + c + "31)")
                }, val1pre: new RegExp("[01]"), val1: new RegExp("0[1-9]|1[012]")
            },
            leapday: "02/29/",
            onKeyDown: function (c, d, e, f) {
                var g = a(this);
                if (c.ctrlKey && c.keyCode === b.keyCode.RIGHT) {
                    var h = new Date;
                    g.val((h.getMonth() + 1).toString() + h.getDate().toString() + h.getFullYear().toString()), g.trigger("setvalue")
                }
            }
        },
        "hh:mm t": {mask: "h:s t\\m", placeholder: "hh:mm xm", alias: "datetime", hourFormat: "12"},
        "h:s t": {mask: "h:s t\\m", placeholder: "hh:mm xm", alias: "datetime", hourFormat: "12"},
        "hh:mm:ss": {mask: "h:s:s", placeholder: "hh:mm:ss", alias: "datetime", autoUnmask: !1},
        "hh:mm": {mask: "h:s", placeholder: "hh:mm", alias: "datetime", autoUnmask: !1},
        date: {alias: "dd/mm/yyyy"},
        "mm/yyyy": {mask: "1/y", placeholder: "mm/yyyy", leapday: "donotuse", separator: "/", alias: "mm/dd/yyyy"},
        shamsi: {
            regex: {
                val2pre: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|1[012])" + c + "[0-3])")
                }, val2: function (a) {
                    var c = b.escapeRegex.call(this, a);
                    return new RegExp("((0[1-9]|1[012])" + c + "(0[1-9]|[12][0-9]))|((0[1-9]|1[012])" + c + "30)|((0[1-6])" + c + "31)")
                }, val1pre: new RegExp("[01]"), val1: new RegExp("0[1-9]|1[012]")
            },
            yearrange: {minyear: 1300, maxyear: 1499},
            mask: "y/1/2",
            leapday: "/12/30",
            placeholder: "yyyy/mm/dd",
            alias: "mm/dd/yyyy",
            clearIncomplete: !0
        }
    }), b
}(jQuery, Inputmask), function (a, b) {
    return b.extendDefinitions({
        A: {
            validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",
            cardinality: 1,
            casing: "upper"
        },
        "&": {validator: "[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", cardinality: 1, casing: "upper"},
        "#": {validator: "[0-9A-Fa-f]", cardinality: 1, casing: "upper"}
    }), b.extendAliases({
        url: {
            definitions: {i: {validator: ".", cardinality: 1}},
            mask: "(\\http://)|(\\http\\s://)|(ftp://)|(ftp\\s://)i{+}",
            insertMode: !1,
            autoUnmask: !1,
            inputmode: "url"
        },
        ip: {
            mask: "i[i[i]].i[i[i]].i[i[i]].i[i[i]]", definitions: {
                i: {
                    validator: function (a, b, c, d, e) {
                        return c - 1 > -1 && "." !== b.buffer[c - 1] ? (a = b.buffer[c - 1] + a, a = c - 2 > -1 && "." !== b.buffer[c - 2] ? b.buffer[c - 2] + a : "0" + a) : a = "00" + a, new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(a)
                    }, cardinality: 1
                }
            }, onUnMask: function (a, b, c) {
                return a
            }, inputmode: "numeric"
        },
        email: {
            mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
            greedy: !1,
            onBeforePaste: function (a, b) {
                return a = a.toLowerCase(), a.replace("mailto:", "")
            },
            definitions: {
                "*": {validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]", cardinality: 1, casing: "lower"},
                "-": {validator: "[0-9A-Za-z-]", cardinality: 1, casing: "lower"}
            },
            onUnMask: function (a, b, c) {
                return a
            },
            inputmode: "email"
        },
        mac: {mask: "##:##:##:##:##:##"},
        vin: {
            mask: "V{13}9{4}",
            definitions: {V: {validator: "[A-HJ-NPR-Za-hj-npr-z\\d]", cardinality: 1, casing: "upper"}},
            clearIncomplete: !0,
            autoUnmask: !0
        }
    }), b
}(jQuery, Inputmask), function (a, b) {
    return b.extendAliases({
        numeric: {
            mask: function (a) {
                function c(b) {
                    for (var c = "", d = 0; d < b.length; d++)c += a.definitions[b.charAt(d)] || a.optionalmarker.start === b.charAt(d) || a.optionalmarker.end === b.charAt(d) || a.quantifiermarker.start === b.charAt(d) || a.quantifiermarker.end === b.charAt(d) || a.groupmarker.start === b.charAt(d) || a.groupmarker.end === b.charAt(d) || a.alternatormarker === b.charAt(d) ? "\\" + b.charAt(d) : b.charAt(d);
                    return c
                }

                if (0 !== a.repeat && isNaN(a.integerDigits) && (a.integerDigits = a.repeat), a.repeat = 0, a.groupSeparator === a.radixPoint && ("." === a.radixPoint ? a.groupSeparator = "," : "," === a.radixPoint ? a.groupSeparator = "." : a.groupSeparator = ""), " " === a.groupSeparator && (a.skipOptionalPartCharacter = void 0), a.autoGroup = a.autoGroup && "" !== a.groupSeparator, a.autoGroup && ("string" == typeof a.groupSize && isFinite(a.groupSize) && (a.groupSize = parseInt(a.groupSize)), isFinite(a.integerDigits))) {
                    var d = Math.floor(a.integerDigits / a.groupSize), e = a.integerDigits % a.groupSize;
                    a.integerDigits = parseInt(a.integerDigits) + (0 === e ? d - 1 : d), a.integerDigits < 1 && (a.integerDigits = "*")
                }
                a.placeholder.length > 1 && (a.placeholder = a.placeholder.charAt(0)), "radixFocus" === a.positionCaretOnClick && "" === a.placeholder && a.integerOptional === !1 && (a.positionCaretOnClick = "lvp"), a.definitions[";"] = a.definitions["~"], a.definitions[";"].definitionSymbol = "~", a.numericInput === !0 && (a.positionCaretOnClick = "radixFocus" === a.positionCaretOnClick ? "lvp" : a.positionCaretOnClick, a.digitsOptional = !1, isNaN(a.digits) && (a.digits = 2), a.decimalProtect = !1);
                var f = "[+]";
                if (f += c(a.prefix), f += a.integerOptional === !0 ? "~{1," + a.integerDigits + "}" : "~{" + a.integerDigits + "}", void 0 !== a.digits) {
                    a.decimalProtect && (a.radixPointDefinitionSymbol = ":");
                    var g = a.digits.toString().split(",");
                    isFinite(g[0] && g[1] && isFinite(g[1])) ? f += (a.decimalProtect ? ":" : a.radixPoint) + ";{" + a.digits + "}" : (isNaN(a.digits) || parseInt(a.digits) > 0) && (f += a.digitsOptional ? "[" + (a.decimalProtect ? ":" : a.radixPoint) + ";{1," + a.digits + "}]" : (a.decimalProtect ? ":" : a.radixPoint) + ";{" + a.digits + "}")
                }
                return f += c(a.suffix), f += "[-]", a.greedy = !1, null !== a.min && (a.min = a.min.toString().replace(new RegExp(b.escapeRegex(a.groupSeparator), "g"), ""), "," === a.radixPoint && (a.min = a.min.replace(a.radixPoint, "."))), null !== a.max && (a.max = a.max.toString().replace(new RegExp(b.escapeRegex(a.groupSeparator), "g"), ""), "," === a.radixPoint && (a.max = a.max.replace(a.radixPoint, "."))), f
            },
            placeholder: "",
            greedy: !1,
            digits: "*",
            digitsOptional: !0,
            radixPoint: ".",
            positionCaretOnClick: "radixFocus",
            groupSize: 3,
            groupSeparator: "",
            autoGroup: !1,
            allowPlus: !0,
            allowMinus: !0,
            negationSymbol: {front: "-", back: ""},
            integerDigits: "+",
            integerOptional: !0,
            prefix: "",
            suffix: "",
            rightAlign: !0,
            decimalProtect: !0,
            min: null,
            max: null,
            step: 1,
            insertMode: !0,
            autoUnmask: !1,
            unmaskAsNumber: !1,
            inputmode: "numeric",
            postFormat: function (c, d, e) {
                e.numericInput === !0 && (c = c.reverse(), isFinite(d) && (d = c.join("").length - d - 1));
                var f, g;
                d = d >= c.length ? c.length - 1 : d < 0 ? 0 : d;
                var h = c[d], i = c.slice();
                h === e.groupSeparator && (i.splice(d--, 1), h = i[d]);
                var j = i.join("").match(new RegExp("^" + b.escapeRegex(e.negationSymbol.front)));
                j = null !== j && 1 === j.length, d > (j ? e.negationSymbol.front.length : 0) + e.prefix.length && d < i.length - e.suffix.length && (i[d] = "!");
                var k = i.join(""), l = k;
                if (j && (k = k.replace(new RegExp("^" + b.escapeRegex(e.negationSymbol.front)), ""), k = k.replace(new RegExp(b.escapeRegex(e.negationSymbol.back) + "$"), "")), k = k.replace(new RegExp(b.escapeRegex(e.suffix) + "$"), ""), k = k.replace(new RegExp("^" + b.escapeRegex(e.prefix)), ""), k.length > 0 && e.autoGroup || k.indexOf(e.groupSeparator) !== -1) {
                    var m = b.escapeRegex(e.groupSeparator);
                    k = k.replace(new RegExp(m, "g"), "");
                    var n = k.split(h === e.radixPoint ? "!" : e.radixPoint);
                    if (k = "" === e.radixPoint ? k : n[0], h !== e.negationSymbol.front && (k = k.replace("!", "?")), k.length > e.groupSize)for (var o = new RegExp("([-+]?[\\d?]+)([\\d?]{" + e.groupSize + "})"); o.test(k) && "" !== e.groupSeparator;)k = k.replace(o, "$1" + e.groupSeparator + "$2"), k = k.replace(e.groupSeparator + e.groupSeparator, e.groupSeparator);
                    k = k.replace("?", "!"), "" !== e.radixPoint && n.length > 1 && (k += (h === e.radixPoint ? "!" : e.radixPoint) + n[1])
                }
                k = e.prefix + k + e.suffix, j && (k = e.negationSymbol.front + k + e.negationSymbol.back);
                var p = l !== k;
                if (p)for (c.length = k.length, f = 0, g = k.length; f < g; f++)c[f] = k.charAt(f);
                var q = a.inArray("!", k);
                return q === -1 && (q = d), c[q] = h,
                    q = e.numericInput && isFinite(d) ? c.join("").length - q - 1 : q, e.numericInput && (c = c.reverse(), a.inArray(e.radixPoint, c) < q && c.join("").length - e.suffix.length !== q && (q -= 1)), {
                    pos: q,
                    refreshFromBuffer: p,
                    buffer: c,
                    isNegative: j
                }
            },
            onBeforeWrite: function (c, d, e, f) {
                var g;
                if (c && ("blur" === c.type || "checkval" === c.type || "keydown" === c.type)) {
                    var h = f.numericInput ? d.slice().reverse().join("") : d.join(""), i = h.replace(f.prefix, "");
                    i = i.replace(f.suffix, ""), i = i.replace(new RegExp(b.escapeRegex(f.groupSeparator), "g"), ""), "," === f.radixPoint && (i = i.replace(f.radixPoint, "."));
                    var j = i.match(new RegExp("[-" + b.escapeRegex(f.negationSymbol.front) + "]", "g"));
                    if (j = null !== j && 1 === j.length, i = i.replace(new RegExp("[-" + b.escapeRegex(f.negationSymbol.front) + "]", "g"), ""), i = i.replace(new RegExp(b.escapeRegex(f.negationSymbol.back) + "$"), ""), isNaN(f.placeholder) && (i = i.replace(new RegExp(b.escapeRegex(f.placeholder), "g"), "")), i = i === f.negationSymbol.front ? i + "0" : i, "" !== i && isFinite(i)) {
                        var k = parseFloat(i), l = j ? k * -1 : k;
                        if (null !== f.min && isFinite(f.min) && l < parseFloat(f.min) ? (k = Math.abs(f.min), j = f.min < 0, h = void 0) : null !== f.max && isFinite(f.max) && l > parseFloat(f.max) && (k = Math.abs(f.max), j = f.max < 0, h = void 0), i = k.toString().replace(".", f.radixPoint).split(""), isFinite(f.digits)) {
                            var m = a.inArray(f.radixPoint, i), n = a.inArray(f.radixPoint, h);
                            m === -1 && (i.push(f.radixPoint), m = i.length - 1);
                            for (var o = 1; o <= f.digits; o++)f.digitsOptional || void 0 !== i[m + o] && i[m + o] !== f.placeholder.charAt(0) ? n !== -1 && void 0 !== h[n + o] && (i[m + o] = i[m + o] || h[n + o]) : i[m + o] = "0";
                            i[i.length - 1] === f.radixPoint && delete i[i.length - 1]
                        }
                        if (k.toString() !== i && k.toString() + "." !== i || j)return i = (f.prefix + i.join("")).split(""), !j || 0 === k && "blur" === c.type || (i.unshift(f.negationSymbol.front), i.push(f.negationSymbol.back)), f.numericInput && (i = i.reverse()), g = f.postFormat(i, f.numericInput ? e : e - 1, f), g.buffer && (g.refreshFromBuffer = g.buffer.join("") !== d.join("")), g
                    }
                }
                if (f.autoGroup)return g = f.postFormat(d, f.numericInput ? e : e - 1, f), g.caret = e < (g.isNegative ? f.negationSymbol.front.length : 0) + f.prefix.length || e > g.buffer.length - (g.isNegative ? f.negationSymbol.back.length : 0) ? g.pos : g.pos + 1, g
            },
            regex: {
                integerPart: function (a) {
                    return new RegExp("[" + b.escapeRegex(a.negationSymbol.front) + "+]?\\d+")
                }, integerNPart: function (a) {
                    return new RegExp("[\\d" + b.escapeRegex(a.groupSeparator) + b.escapeRegex(a.placeholder.charAt(0)) + "]+")
                }
            },
            signHandler: function (a, b, c, d, e) {
                if (!d && e.allowMinus && "-" === a || e.allowPlus && "+" === a) {
                    var f = b.buffer.join("").match(e.regex.integerPart(e));
                    if (f && f[0].length > 0)return b.buffer[f.index] === ("-" === a ? "+" : e.negationSymbol.front) ? "-" === a ? "" !== e.negationSymbol.back ? {
                        pos: 0,
                        c: e.negationSymbol.front,
                        remove: 0,
                        caret: c,
                        insert: {pos: b.buffer.length - 1, c: e.negationSymbol.back}
                    } : {
                        pos: 0,
                        c: e.negationSymbol.front,
                        remove: 0,
                        caret: c
                    } : "" !== e.negationSymbol.back ? {
                        pos: 0,
                        c: "+",
                        remove: [0, b.buffer.length - 1],
                        caret: c
                    } : {
                        pos: 0,
                        c: "+",
                        remove: 0,
                        caret: c
                    } : b.buffer[0] === ("-" === a ? e.negationSymbol.front : "+") ? "-" === a && "" !== e.negationSymbol.back ? {
                        remove: [0, b.buffer.length - 1],
                        caret: c - 1
                    } : {remove: 0, caret: c - 1} : "-" === a ? "" !== e.negationSymbol.back ? {
                        pos: 0,
                        c: e.negationSymbol.front,
                        caret: c + 1,
                        insert: {pos: b.buffer.length, c: e.negationSymbol.back}
                    } : {pos: 0, c: e.negationSymbol.front, caret: c + 1} : {pos: 0, c: a, caret: c + 1}
                }
                return !1
            },
            radixHandler: function (b, c, d, e, f) {
                if (!e && f.numericInput !== !0 && b === f.radixPoint && void 0 !== f.digits && (isNaN(f.digits) || parseInt(f.digits) > 0)) {
                    var g = a.inArray(f.radixPoint, c.buffer), h = c.buffer.join("").match(f.regex.integerPart(f));
                    if (g !== -1 && c.validPositions[g])return c.validPositions[g - 1] ? {caret: g + 1} : {
                        pos: h.index,
                        c: h[0],
                        caret: g + 1
                    };
                    if (!h || "0" === h[0] && h.index + 1 !== d)return c.buffer[h ? h.index : d] = "0", {
                        pos: (h ? h.index : d) + 1,
                        c: f.radixPoint
                    }
                }
                return !1
            },
            leadingZeroHandler: function (b, c, d, e, f, g) {
                if (!e) {
                    var h = c.buffer.slice("");
                    if (h.splice(0, f.prefix.length), h.splice(h.length - f.suffix.length, f.suffix.length), f.numericInput === !0) {
                        var h = h.reverse(), i = h[0];
                        if ("0" === i && void 0 === c.validPositions[d - 1])return {pos: d, remove: h.length - 1}
                    } else {
                        d -= f.prefix.length;
                        var j = a.inArray(f.radixPoint, h),
                            k = h.slice(0, j !== -1 ? j : void 0).join("").match(f.regex.integerNPart(f));
                        if (k && (j === -1 || d <= j)) {
                            var l = j === -1 ? 0 : parseInt(h.slice(j + 1).join(""));
                            if (0 === k[0].indexOf("" !== f.placeholder ? f.placeholder.charAt(0) : "0") && (k.index + 1 === d || g !== !0 && 0 === l))return c.buffer.splice(k.index + f.prefix.length, 1), {
                                pos: k.index + f.prefix.length,
                                remove: k.index + f.prefix.length
                            };
                            if ("0" === b && d <= k.index && k[0] !== f.groupSeparator)return !1
                        }
                    }
                }
                return !0
            },
            definitions: {
                "~": {
                    validator: function (c, d, e, f, g, h) {
                        var i = g.signHandler(c, d, e, f, g);
                        if (!i && (i = g.radixHandler(c, d, e, f, g), !i && (i = f ? new RegExp("[0-9" + b.escapeRegex(g.groupSeparator) + "]").test(c) : new RegExp("[0-9]").test(c), i === !0 && (i = g.leadingZeroHandler(c, d, e, f, g, h), i === !0)))) {
                            var j = a.inArray(g.radixPoint, d.buffer);
                            i = j !== -1 && (g.digitsOptional === !1 || d.validPositions[e]) && g.numericInput !== !0 && e > j && !f ? {
                                pos: e,
                                remove: e
                            } : {pos: e}
                        }
                        return i
                    }, cardinality: 1
                }, "+": {
                    validator: function (a, b, c, d, e) {
                        var f = e.signHandler(a, b, c, d, e);
                        return !f && (d && e.allowMinus && a === e.negationSymbol.front || e.allowMinus && "-" === a || e.allowPlus && "+" === a) && (f = !(!d && "-" === a) || ("" !== e.negationSymbol.back ? {
                                pos: c,
                                c: "-" === a ? e.negationSymbol.front : "+",
                                caret: c + 1,
                                insert: {pos: b.buffer.length, c: e.negationSymbol.back}
                            } : {pos: c, c: "-" === a ? e.negationSymbol.front : "+", caret: c + 1})), f
                    }, cardinality: 1, placeholder: ""
                }, "-": {
                    validator: function (a, b, c, d, e) {
                        var f = e.signHandler(a, b, c, d, e);
                        return !f && d && e.allowMinus && a === e.negationSymbol.back && (f = !0), f
                    }, cardinality: 1, placeholder: ""
                }, ":": {
                    validator: function (a, c, d, e, f) {
                        var g = f.signHandler(a, c, d, e, f);
                        if (!g) {
                            var h = "[" + b.escapeRegex(f.radixPoint) + "]";
                            g = new RegExp(h).test(a), g && c.validPositions[d] && c.validPositions[d].match.placeholder === f.radixPoint && (g = {caret: d + 1})
                        }
                        return g ? {c: f.radixPoint} : g
                    }, cardinality: 1, placeholder: function (a) {
                        return a.radixPoint
                    }
                }
            },
            onUnMask: function (a, c, d) {
                if ("" === c && d.nullable === !0)return c;
                var e = a.replace(d.prefix, "");
                return e = e.replace(d.suffix, ""), e = e.replace(new RegExp(b.escapeRegex(d.groupSeparator), "g"), ""), d.unmaskAsNumber ? ("" !== d.radixPoint && e.indexOf(d.radixPoint) !== -1 && (e = e.replace(b.escapeRegex.call(this, d.radixPoint), ".")), Number(e)) : e
            },
            isComplete: function (a, c) {
                var d = a.join(""), e = a.slice();
                if (c.postFormat(e, 0, c), e.join("") !== d)return !1;
                var f = d.replace(c.prefix, "");
                return f = f.replace(c.suffix, ""), f = f.replace(new RegExp(b.escapeRegex(c.groupSeparator), "g"), ""), "," === c.radixPoint && (f = f.replace(b.escapeRegex(c.radixPoint), ".")), isFinite(f)
            },
            onBeforeMask: function (a, c) {
                if (c.numericInput === !0 && (a = a.split("").reverse().join("")), "" !== c.radixPoint && isFinite(a)) a = a.toString().replace(".", c.radixPoint); else {
                    var d = a.match(/,/g), e = a.match(/\./g);
                    e && d ? e.length > d.length ? (a = a.replace(/\./g, ""), a = a.replace(",", c.radixPoint)) : d.length > e.length ? (a = a.replace(/,/g, ""), a = a.replace(".", c.radixPoint)) : a = a.indexOf(".") < a.indexOf(",") ? a.replace(/\./g, "") : a = a.replace(/,/g, "") : a = a.replace(new RegExp(b.escapeRegex(c.groupSeparator), "g"), "")
                }
                if (0 === c.digits && (a.indexOf(".") !== -1 ? a = a.substring(0, a.indexOf(".")) : a.indexOf(",") !== -1 && (a = a.substring(0, a.indexOf(",")))), "" !== c.radixPoint && isFinite(c.digits) && a.indexOf(c.radixPoint) !== -1) {
                    var f = a.split(c.radixPoint), g = f[1].match(new RegExp("\\d*"))[0];
                    if (parseInt(c.digits) < g.toString().length) {
                        var h = Math.pow(10, parseInt(c.digits));
                        a = a.replace(b.escapeRegex(c.radixPoint), "."), a = Math.round(parseFloat(a) * h) / h, a = a.toString().replace(".", c.radixPoint)
                    }
                }
                return c.numericInput === !0 && (a = a.split("").reverse().join("")), a.toString()
            },
            canClearPosition: function (a, b, c, d, e) {
                var f = a.validPositions[b].input,
                    g = f !== e.radixPoint || null !== a.validPositions[b].match.fn && e.decimalProtect === !1 || isFinite(f) || b === c || f === e.groupSeparator || f === e.negationSymbol.front || f === e.negationSymbol.back;
                return g
            },
            onKeyDown: function (c, d, e, f) {
                var g = a(this);
                if (c.ctrlKey)switch (c.keyCode) {
                    case b.keyCode.UP:
                        g.val(parseFloat(this.inputmask.unmaskedvalue()) + parseInt(f.step)), g.trigger("setvalue");
                        break;
                    case b.keyCode.DOWN:
                        g.val(parseFloat(this.inputmask.unmaskedvalue()) - parseInt(f.step)), g.trigger("setvalue")
                }
            }
        },
        currency: {
            prefix: "$ ",
            groupSeparator: ",",
            alias: "numeric",
            placeholder: "0",
            autoGroup: !0,
            digits: 2,
            digitsOptional: !1,
            clearMaskOnLostFocus: !1
        },
        decimal: {alias: "numeric"},
        integer: {alias: "numeric", digits: 0, radixPoint: ""},
        percentage: {
            alias: "numeric",
            digits: 2,
            radixPoint: ".",
            placeholder: "0",
            autoGroup: !1,
            min: 0,
            max: 100,
            suffix: " %",
            allowPlus: !1,
            allowMinus: !1
        }
    }), b
}(jQuery, Inputmask), function (a, b) {
    return b.extendAliases({
        abstractphone: {
            countrycode: "", phoneCodes: [], mask: function (a) {
                a.definitions = {"#": a.definitions[9]};
                var b = a.phoneCodes.sort(function (a, b) {
                    var c = (a.mask || a).replace(/#/g, "9").replace(/[\+\(\)#-]/g, ""),
                        d = (b.mask || b).replace(/#/g, "9").replace(/[\+\(\)#-]/g, ""),
                        e = (a.mask || a).split("#")[0], f = (b.mask || b).split("#")[0];
                    return 0 === f.indexOf(e) ? -1 : 0 === e.indexOf(f) ? 1 : c.localeCompare(d)
                });
                return b
            }, keepStatic: !0, onBeforeMask: function (a, b) {
                var c = a.replace(/^0{1,2}/, "").replace(/[\s]/g, "");
                return (c.indexOf(b.countrycode) > 1 || c.indexOf(b.countrycode) === -1) && (c = "+" + b.countrycode + c), c
            }, onUnMask: function (a, b, c) {
                return b
            }, inputmode: "tel"
        }
    }), b
}(jQuery, Inputmask), function (a, b) {
    return b.extendAliases({
        Regex: {
            mask: "r",
            greedy: !1,
            repeat: "*",
            regex: null,
            regexTokens: null,
            tokenizer: /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,
            quantifierFilter: /[0-9]+[^,]/,
            isComplete: function (a, b) {
                return new RegExp(b.regex).test(a.join(""))
            },
            definitions: {
                r: {
                    validator: function (b, c, d, e, f) {
                        function g(a, b) {
                            this.matches = [], this.isGroup = a || !1, this.isQuantifier = b || !1, this.quantifier = {
                                min: 1,
                                max: 1
                            }, this.repeaterPart = void 0
                        }

                        function h() {
                            var a, b, c = new g, d = [];
                            for (f.regexTokens = []; a = f.tokenizer.exec(f.regex);)switch (b = a[0], b.charAt(0)) {
                                case"(":
                                    d.push(new g((!0)));
                                    break;
                                case")":
                                    k = d.pop(), d.length > 0 ? d[d.length - 1].matches.push(k) : c.matches.push(k);
                                    break;
                                case"{":
                                case"+":
                                case"*":
                                    var e = new g((!1), (!0));
                                    b = b.replace(/[{}]/g, "");
                                    var h = b.split(","), i = isNaN(h[0]) ? h[0] : parseInt(h[0]),
                                        j = 1 === h.length ? i : isNaN(h[1]) ? h[1] : parseInt(h[1]);
                                    if (e.quantifier = {min: i, max: j}, d.length > 0) {
                                        var l = d[d.length - 1].matches;
                                        a = l.pop(), a.isGroup || (k = new g((!0)), k.matches.push(a), a = k), l.push(a), l.push(e)
                                    } else a = c.matches.pop(), a.isGroup || (k = new g((!0)), k.matches.push(a), a = k), c.matches.push(a), c.matches.push(e);
                                    break;
                                default:
                                    d.length > 0 ? d[d.length - 1].matches.push(b) : c.matches.push(b)
                            }
                            c.matches.length > 0 && f.regexTokens.push(c)
                        }

                        function i(b, c) {
                            var d = !1;
                            c && (m += "(", o++);
                            for (var e = 0; e < b.matches.length; e++) {
                                var f = b.matches[e];
                                if (f.isGroup === !0) d = i(f, !0); else if (f.isQuantifier === !0) {
                                    var g = a.inArray(f, b.matches), h = b.matches[g - 1], k = m;
                                    if (isNaN(f.quantifier.max)) {
                                        for (; f.repeaterPart && f.repeaterPart !== m && f.repeaterPart.length > m.length && !(d = i(h, !0)););
                                        d = d || i(h, !0), d && (f.repeaterPart = m), m = k + f.quantifier.max
                                    } else {
                                        for (var l = 0, n = f.quantifier.max - 1; l < n && !(d = i(h, !0)); l++);
                                        m = k + "{" + f.quantifier.min + "," + f.quantifier.max + "}"
                                    }
                                } else if (void 0 !== f.matches)for (var p = 0; p < f.length && !(d = i(f[p], c)); p++); else {
                                    var q;
                                    if ("[" == f.charAt(0)) {
                                        q = m, q += f;
                                        for (var r = 0; r < o; r++)q += ")";
                                        var s = new RegExp("^(" + q + ")$");
                                        d = s.test(j)
                                    } else for (var t = 0, u = f.length; t < u; t++)if ("\\" !== f.charAt(t)) {
                                        q = m, q += f.substr(0, t + 1), q = q.replace(/\|$/, "");
                                        for (var r = 0; r < o; r++)q += ")";
                                        var s = new RegExp("^(" + q + ")$");
                                        if (d = s.test(j))break
                                    }
                                    m += f
                                }
                                if (d)break
                            }
                            return c && (m += ")", o--), d
                        }

                        var j, k, l = c.buffer.slice(), m = "", n = !1, o = 0;
                        null === f.regexTokens && h(), l.splice(d, 0, b), j = l.join("");
                        for (var p = 0; p < f.regexTokens.length; p++) {
                            var q = f.regexTokens[p];
                            if (n = i(q, q.isGroup))break
                        }
                        return n
                    }, cardinality: 1
                }
            }
        }
    }), b
}(jQuery, Inputmask);


/*!
 * bootstrap-fileinput v4.3.9
 * http://plugins.krajee.com/file-input
 *
 * Author: Kartik Visweswaran
 * Copyright: 2014 - 2017, Kartik Visweswaran, Krajee.com
 *
 * Licensed under the BSD 3-Clause
 * https://github.com/kartik-v/bootstrap-fileinput/blob/master/LICENSE.md
 */
!function (e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery)
}(function (e) {
    "use strict";
    e.fn.fileinputLocales = {}, e.fn.fileinputThemes = {};
    var i, t;
    i = {
        FRAMES: ".kv-preview-thumb",
        SORT_CSS: "file-sortable",
        STYLE_SETTING: 'style="width:{width};height:{height};"',
        OBJECT_PARAMS: '<param name="controller" value="true" />\n<param name="allowFullScreen" value="true" />\n<param name="allowScriptAccess" value="always" />\n<param name="autoPlay" value="false" />\n<param name="autoStart" value="false" />\n<param name="quality" value="high" />\n',
        DEFAULT_PREVIEW: '<div class="file-preview-other">\n<span class="{previewFileIconClass}">{previewFileIcon}</span>\n</div>',
        MODAL_ID: "kvFileinputModal",
        MODAL_EVENTS: ["show", "shown", "hide", "hidden", "loaded"],
        objUrl: window.URL || window.webkitURL,
        compare: function (e, i, t) {
            return void 0 !== e && (t ? e === i : e.match(i))
        },
        isIE: function (e) {
            if ("Microsoft Internet Explorer" !== navigator.appName)return !1;
            if (10 === e)return new RegExp("msie\\s" + e, "i").test(navigator.userAgent);
            var i, t = document.createElement("div");
            return t.innerHTML = "<!--[if IE " + e + "]> <i></i> <![endif]-->", i = t.getElementsByTagName("i").length, document.body.appendChild(t), t.parentNode.removeChild(t), i
        },
        initModal: function (i) {
            var t = e("body");
            t.length && i.appendTo(t)
        },
        isEmpty: function (i, t) {
            return void 0 === i || null === i || 0 === i.length || t && "" === e.trim(i)
        },
        isArray: function (e) {
            return Array.isArray(e) || "[object Array]" === Object.prototype.toString.call(e)
        },
        ifSet: function (e, i, t) {
            return t = t || "", i && "object" == typeof i && e in i ? i[e] : t
        },
        cleanArray: function (e) {
            return e instanceof Array || (e = []), e.filter(function (e) {
                return void 0 !== e && null !== e
            })
        },
        spliceArray: function (e, i) {
            var t, a = 0, r = [];
            if (!(e instanceof Array))return [];
            for (t = 0; t < e.length; t++)t !== i && (r[a] = e[t], a++);
            return r
        },
        getNum: function (e, i) {
            return i = i || 0, "number" == typeof e ? e : ("string" == typeof e && (e = parseFloat(e)), isNaN(e) ? i : e)
        },
        hasFileAPISupport: function () {
            return !(!window.File || !window.FileReader)
        },
        hasDragDropSupport: function () {
            var e = document.createElement("div");
            return !i.isIE(9) && (void 0 !== e.draggable || void 0 !== e.ondragstart && void 0 !== e.ondrop)
        },
        hasFileUploadSupport: function () {
            return i.hasFileAPISupport() && window.FormData
        },
        addCss: function (e, i) {
            e.removeClass(i).addClass(i)
        },
        getElement: function (t, a, r) {
            return i.isEmpty(t) || i.isEmpty(t[a]) ? r : e(t[a])
        },
        uniqId: function () {
            return Math.round((new Date).getTime() + 100 * Math.random())
        },
        htmlEncode: function (e) {
            return e.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;")
        },
        replaceTags: function (i, t) {
            var a = i;
            return t ? (e.each(t, function (e, i) {
                "function" == typeof i && (i = i()), a = a.split(e).join(i)
            }), a) : a
        },
        cleanMemory: function (e) {
            var t = e.is("img") ? e.attr("src") : e.find("source").attr("src");
            i.objUrl.revokeObjectURL(t)
        },
        findFileName: function (e) {
            var i = e.lastIndexOf("/");
            return -1 === i && (i = e.lastIndexOf("\\")), e.split(e.substring(i, i + 1)).pop()
        },
        checkFullScreen: function () {
            return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement
        },
        toggleFullScreen: function (e) {
            var t = document, a = t.documentElement;
            a && e && !i.checkFullScreen() ? a.requestFullscreen ? a.requestFullscreen() : a.msRequestFullscreen ? a.msRequestFullscreen() : a.mozRequestFullScreen ? a.mozRequestFullScreen() : a.webkitRequestFullscreen && a.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT) : t.exitFullscreen ? t.exitFullscreen() : t.msExitFullscreen ? t.msExitFullscreen() : t.mozCancelFullScreen ? t.mozCancelFullScreen() : t.webkitExitFullscreen && t.webkitExitFullscreen()
        },
        moveArray: function (e, i, t) {
            if (t >= e.length)for (var a = t - e.length; a-- + 1;)e.push(void 0);
            return e.splice(t, 0, e.splice(i, 1)[0]), e
        },
        cleanZoomCache: function (e) {
            var i = e.closest(".kv-zoom-cache-theme");
            i.length || (i = e.closest(".kv-zoom-cache")), i.remove()
        }
    }, t = function (t, a) {
        var r = this;
        r.$element = e(t), r._validate() && (r.isPreviewable = i.hasFileAPISupport(), r.isIE9 = i.isIE(9), r.isIE10 = i.isIE(10), r.isPreviewable || r.isIE9 ? (r._init(a), r._listen()) : r.$element.removeClass("file-loading"))
    }, t.prototype = {
        constructor: t, _init: function (t) {
            var a, r, n = this, o = n.$element;
            n.options = t, e.each(t, function (e, t) {
                switch (e) {
                    case"minFileCount":
                    case"maxFileCount":
                    case"maxFileSize":
                        n[e] = i.getNum(t);
                        break;
                    default:
                        n[e] = t
                }
            }), n.$form = o.closest("form"), n._initTemplateDefaults(), n.fileInputCleared = !1, n.fileBatchCompleted = !0, n.isPreviewable || (n.showPreview = !1), n.uploadFileAttr = i.isEmpty(o.attr("name")) ? "file_data" : o.attr("name"), n.reader = null, n.formdata = {}, n.clearStack(), n.uploadCount = 0, n.uploadStatus = {}, n.uploadLog = [], n.uploadAsyncCount = 0, n.loadedImages = [], n.totalImagesCount = 0, n.ajaxRequests = [], n.isError = !1, n.ajaxAborted = !1, n.cancelling = !1, r = n._getLayoutTemplate("progress"), n.progressTemplate = r.replace("{class}", n.progressClass), n.progressCompleteTemplate = r.replace("{class}", n.progressCompleteClass), n.progressErrorTemplate = r.replace("{class}", n.progressErrorClass), n.dropZoneEnabled = i.hasDragDropSupport() && n.dropZoneEnabled, n.isDisabled = o.attr("disabled") || o.attr("readonly"), n.isUploadable = i.hasFileUploadSupport() && !i.isEmpty(n.uploadUrl), n.isClickable = n.browseOnZoneClick && n.showPreview && (n.isUploadable && n.dropZoneEnabled || !i.isEmpty(n.defaultPreviewContent)), n.slug = "function" == typeof t.slugCallback ? t.slugCallback : n._slugDefault, n.mainTemplate = n.showCaption ? n._getLayoutTemplate("main1") : n._getLayoutTemplate("main2"), n.captionTemplate = n._getLayoutTemplate("caption"), n.previewGenericTemplate = n._getPreviewTemplate("generic"), n.resizeImage && (n.maxImageWidth || n.maxImageHeight) && (n.imageCanvas = document.createElement("canvas"), n.imageCanvasContext = n.imageCanvas.getContext("2d")), i.isEmpty(o.attr("id")) && o.attr("id", i.uniqId()), n.namespace = ".fileinput_" + o.attr("id").replace(/-/g, "_"), void 0 === n.$container ? n.$container = n._createContainer() : n._refreshContainer(), a = n.$container, n.$dropZone = a.find(".file-drop-zone"), n.$progress = a.find(".kv-upload-progress"), n.$btnUpload = a.find(".fileinput-upload"), n.$captionContainer = i.getElement(t, "elCaptionContainer", a.find(".file-caption")), n.$caption = i.getElement(t, "elCaptionText", a.find(".file-caption-name")), n.$previewContainer = i.getElement(t, "elPreviewContainer", a.find(".file-preview")), n.$preview = i.getElement(t, "elPreviewImage", a.find(".file-preview-thumbnails")), n.$previewStatus = i.getElement(t, "elPreviewStatus", a.find(".file-preview-status")), n.$errorContainer = i.getElement(t, "elErrorContainer", n.$previewContainer.find(".kv-fileinput-error")), i.isEmpty(n.msgErrorClass) || i.addCss(n.$errorContainer, n.msgErrorClass), n.$errorContainer.hide(), n.previewInitId = "preview-" + i.uniqId(), n._initPreviewCache(), n._initPreview(!0), n._initPreviewActions(), n._setFileDropZoneTitle(), o.removeClass("file-loading"), o.attr("disabled") && n.disable(), n._initZoom()
        }, _initTemplateDefaults: function () {
            var t, a, r, n, o, l, s, d, c, p, u, f, m, g, v, h, w, _, b, C, y, E, x, T, S, F, P, I, k, A, $, z, D, U,
                j = this;
            t = '{preview}\n<div class="kv-upload-progress hide"></div>\n<div class="input-group {class}">\n   {caption}\n   <div class="input-group-btn">\n       {remove}\n       {cancel}\n       {upload}\n       {browse}\n   </div>\n</div>', a = '{preview}\n<div class="kv-upload-progress hide"></div>\n{remove}\n{cancel}\n{upload}\n{browse}\n', r = '<div class="file-preview {class}">\n    {close}    <div class="{dropClass}">\n    <div class="file-preview-thumbnails">\n    </div>\n    <div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div>\n    <div class="kv-fileinput-error"></div>\n    </div>\n</div>', o = '<div class="close fileinput-remove">&times;</div>\n', n = '<i class="glyphicon glyphicon-file kv-caption-icon"></i>', l = '<div tabindex="500" class="form-control file-caption {class}">\n   <div class="file-caption-name"></div>\n</div>\n', s = '<button type="{type}" tabindex="500" title="{title}" class="{css}" {status}>{icon} {label}</button>', d = '<a href="{href}" tabindex="500" title="{title}" class="{css}" {status}>{icon} {label}</a>', c = '<div tabindex="500" class="{css}" {status}>{icon} {label}</div>', p = '<div id="' + i.MODAL_ID + '" class="file-zoom-dialog modal fade" tabindex="-1" aria-labelledby="' + i.MODAL_ID + 'Label"></div>', u = '<div class="modal-dialog modal-lg" role="document">\n  <div class="modal-content">\n    <div class="modal-header">\n      <div class="kv-zoom-actions pull-right">{toggleheader}{fullscreen}{borderless}{close}</div>\n      <h3 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h3>\n    </div>\n    <div class="modal-body">\n      <div class="floating-buttons"></div>\n      <div class="kv-zoom-body file-zoom-content"></div>\n{prev} {next}\n    </div>\n  </div>\n</div>\n', f = '<div class="progress">\n    <div class="{class}" role="progressbar" aria-valuenow="{percent}" aria-valuemin="0" aria-valuemax="100" style="width:{percent}%;">\n        {status}\n     </div>\n</div>', m = " <samp>({sizeText})</samp>", g = '<div class="file-thumbnail-footer">\n    <div class="file-footer-caption" title="{caption}">{caption}<br>{size}</div>\n    {progress} {actions}\n</div>', v = '<div class="file-upload-indicator" title="{indicatorTitle}">{indicator}</div>\n{drag}\n<div class="file-actions">\n    <div class="file-footer-buttons">\n        {upload} {delete} {zoom} {other}    </div>\n    <div class="clearfix"></div>\n</div>', h = '<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}" {dataUrl}{dataKey}>{removeIcon}</button>\n', w = '<button type="button" class="kv-file-upload {uploadClass}" title="{uploadTitle}">{uploadIcon}</button>', _ = '<button type="button" class="kv-file-zoom {zoomClass}" title="{zoomTitle}">{zoomIcon}</button>', b = '<span class="file-drag-handle {dragClass}" title="{dragTitle}">{dragIcon}</span>', C = '<div class="file-preview-frame {frameClass}" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}"', y = C + '><div class="kv-file-content">\n', E = C + ' title="{caption}"><div class="kv-file-content">\n', x = "</div>{footer}\n</div>\n", T = "{content}\n", S = '<div class="kv-preview-data file-preview-html" title="{caption}" ' + i.STYLE_SETTING + ">{data}</div>\n", F = '<img src="{data}" class="file-preview-image kv-preview-data" title="{caption}" alt="{caption}" ' + i.STYLE_SETTING + ">\n", P = '<textarea class="kv-preview-data file-preview-text" title="{caption}" readonly ' + i.STYLE_SETTING + ">{data}</textarea>\n", I = '<video class="kv-preview-data file-preview-video" width="{width}" height="{height}" controls>\n<source src="{data}" type="{type}">\n' + i.DEFAULT_PREVIEW + "\n</video>\n", k = '<div class="file-preview-audio"><audio class="kv-preview-data" controls>\n<source src="{data}" type="{type}">\n' + i.DEFAULT_PREVIEW + "\n</audio></div>\n", A = '<object class="kv-preview-data file-object" type="application/x-shockwave-flash" width="{width}" height="{height}" data="{data}">\n' + i.OBJECT_PARAMS + " " + i.DEFAULT_PREVIEW + "\n</object>\n", $ = '<object class="kv-preview-data file-object" data="{data}" type="{type}" width="{width}" height="{height}">\n<param name="movie" value="{caption}" />\n' + i.OBJECT_PARAMS + " " + i.DEFAULT_PREVIEW + "\n</object>\n", z = '<embed class="kv-preview-data" src="{data}" width="{width}" height="{height}" type="application/pdf">\n', D = '<div class="kv-preview-data file-preview-other-frame">\n' + i.DEFAULT_PREVIEW + "\n</div>\n", U = '<div class="kv-zoom-cache" style="display:none">{zoomContent}</div>', j.defaults = {
                layoutTemplates: {
                    main1: t,
                    main2: a,
                    preview: r,
                    close: o,
                    fileIcon: n,
                    caption: l,
                    modalMain: p,
                    modal: u,
                    progress: f,
                    size: m,
                    footer: g,
                    actions: v,
                    actionDelete: h,
                    actionUpload: w,
                    actionZoom: _,
                    actionDrag: b,
                    btnDefault: s,
                    btnLink: d,
                    btnBrowse: c,
                    zoomCache: U
                },
                previewMarkupTags: {tagBefore1: y, tagBefore2: E, tagAfter: x},
                previewContentTemplates: {
                    generic: T,
                    html: S,
                    image: F,
                    text: P,
                    video: I,
                    audio: k,
                    flash: A,
                    object: $,
                    pdf: z,
                    other: D
                },
                allowedPreviewTypes: ["image", "html", "text", "video", "audio", "flash", "pdf", "object"],
                previewTemplates: {},
                previewSettings: {
                    image: {width: "auto", height: "160px"},
                    html: {width: "213px", height: "160px"},
                    text: {width: "213px", height: "160px"},
                    video: {width: "213px", height: "160px"},
                    audio: {width: "213px", height: "80px"},
                    flash: {width: "213px", height: "160px"},
                    object: {width: "160px", height: "auto"},
                    pdf: {width: "160px", height: "160px"},
                    other: {width: "160px", height: "160px"}
                },
                previewZoomSettings: {
                    image: {width: "auto", height: "auto", "max-width": "100%", "max-height": "100%"},
                    html: {width: "100%", height: "100%", "min-height": "480px"},
                    text: {width: "100%", height: "100%", "min-height": "480px"},
                    video: {width: "auto", height: "100%", "max-width": "100%"},
                    audio: {width: "100%", height: "30px"},
                    flash: {width: "auto", height: "480px"},
                    object: {width: "auto", height: "100%", "min-height": "480px"},
                    pdf: {width: "100%", height: "100%", "min-height": "480px"},
                    other: {width: "auto", height: "100%", "min-height": "480px"}
                },
                fileTypeSettings: {
                    image: function (e, t) {
                        return i.compare(e, "image.*") || i.compare(t, /\.(gif|png|jpe?g)$/i)
                    }, html: function (e, t) {
                        return i.compare(e, "text/html") || i.compare(t, /\.(htm|html)$/i)
                    }, text: function (e, t) {
                        return i.compare(e, "text.*") || i.compare(t, /\.(xml|javascript)$/i) || i.compare(t, /\.(txt|md|csv|nfo|ini|json|php|js|css)$/i)
                    }, video: function (e, t) {
                        return i.compare(e, "video.*") && (i.compare(e, /(ogg|mp4|mp?g|mov|webm|3gp)$/i) || i.compare(t, /\.(og?|mp4|webm|mp?g|mov|3gp)$/i))
                    }, audio: function (e, t) {
                        return i.compare(e, "audio.*") && (i.compare(t, /(ogg|mp3|mp?g|wav)$/i) || i.compare(t, /\.(og?|mp3|mp?g|wav)$/i))
                    }, flash: function (e, t) {
                        return i.compare(e, "application/x-shockwave-flash", !0) || i.compare(t, /\.(swf)$/i)
                    }, pdf: function (e, t) {
                        return i.compare(e, "application/pdf", !0) || i.compare(t, /\.(pdf)$/i)
                    }, object: function () {
                        return !0
                    }, other: function () {
                        return !0
                    }
                },
                fileActionSettings: {
                    showRemove: !0,
                    showUpload: !0,
                    showZoom: !0,
                    showDrag: !0,
                    removeIcon: '<i class="glyphicon glyphicon-trash text-danger"></i>',
                    removeClass: "btn btn-xs btn-default",
                    removeTitle: "Remove file",
                    uploadIcon: '<i class="glyphicon glyphicon-upload text-info"></i>',
                    uploadClass: "btn btn-xs btn-default",
                    uploadTitle: "Upload file",
                    zoomIcon: '<i class="glyphicon glyphicon-zoom-in"></i>',
                    zoomClass: "btn btn-xs btn-default",
                    zoomTitle: "View Details",
                    dragIcon: '<i class="glyphicon glyphicon-menu-hamburger"></i>',
                    dragClass: "text-info",
                    dragTitle: "Move / Rearrange",
                    dragSettings: {},
                    indicatorNew: '<i class="glyphicon glyphicon-hand-down text-warning"></i>',
                    indicatorSuccess: '<i class="glyphicon glyphicon-ok-sign text-success"></i>',
                    indicatorError: '<i class="glyphicon glyphicon-exclamation-sign text-danger"></i>',
                    indicatorLoading: '<i class="glyphicon glyphicon-hand-up text-muted"></i>',
                    indicatorNewTitle: "Not uploaded yet",
                    indicatorSuccessTitle: "Uploaded",
                    indicatorErrorTitle: "Upload Error",
                    indicatorLoadingTitle: "Uploading ..."
                }
            }, e.each(j.defaults, function (i, t) {
                return "allowedPreviewTypes" === i ? void(void 0 === j.allowedPreviewTypes && (j.allowedPreviewTypes = t)) : void(j[i] = e.extend(!0, {}, t, j[i]))
            }), j._initPreviewTemplates()
        }, _initPreviewTemplates: function () {
            var t, a = this, r = a.defaults, n = a.previewMarkupTags, o = n.tagAfter;
            e.each(r.previewContentTemplates, function (e, r) {
                i.isEmpty(a.previewTemplates[e]) && (t = n.tagBefore2, "generic" !== e && "image" !== e && "html" !== e && "text" !== e || (t = n.tagBefore1), a.previewTemplates[e] = t + r + o)
            })
        }, _initPreviewCache: function () {
            var t = this;
            t.previewCache = {
                data: {}, init: function () {
                    var e = t.initialPreview;
                    e.length > 0 && !i.isArray(e) && (e = e.split(t.initialPreviewDelimiter)), t.previewCache.data = {
                        content: e,
                        config: t.initialPreviewConfig,
                        tags: t.initialPreviewThumbTags
                    }
                }, fetch: function () {
                    return t.previewCache.data.content.filter(function (e) {
                        return null !== e
                    })
                }, count: function (e) {
                    return t.previewCache.data && t.previewCache.data.content ? e ? t.previewCache.data.content.length : t.previewCache.fetch().length : 0
                }, get: function (a, r) {
                    var n, o, l, s, d, c, p, u = "init_" + a, f = t.previewCache.data, m = f.config[a],
                        g = f.content[a], v = t.previewInitId + "-" + u,
                        h = i.ifSet("previewAsData", m, t.initialPreviewAsData),
                        w = function (e, a, r, n, o, l, s, d, c) {
                            return d = " file-preview-initial " + i.SORT_CSS + (d ? " " + d : ""), t._generatePreviewTemplate(e, a, r, n, o, !1, null, d, l, s, c)
                        };
                    return g ? (r = void 0 === r ? !0 : r, l = i.ifSet("type", m, t.initialPreviewFileType || "generic"), d = i.ifSet("filename", m, i.ifSet("caption", m)), c = i.ifSet("filetype", m, l), s = t.previewCache.footer(a, r, m && m.size || null), p = i.ifSet("frameClass", m), n = h ? w(l, g, d, c, v, s, u, p) : w("generic", g, d, c, v, s, u, p, l).replace(/\{content}/g, f.content[a]), f.tags.length && f.tags[a] && (n = i.replaceTags(n, f.tags[a])), i.isEmpty(m) || i.isEmpty(m.frameAttr) || (o = e(document.createElement("div")).html(n), o.find(".file-preview-initial").attr(m.frameAttr), n = o.html(), o.remove()), n) : ""
                }, add: function (e, a, r, n) {
                    var o, l = t.previewCache.data;
                    return i.isArray(e) || (e = e.split(t.initialPreviewDelimiter)), n ? (o = l.content.push(e) - 1, l.config[o] = a, l.tags[o] = r) : (o = e.length - 1, l.content = e, l.config = a, l.tags = r), t.previewCache.data = l, o
                }, set: function (e, a, r, n) {
                    var o, l, s = t.previewCache.data;
                    if (e && e.length && (i.isArray(e) || (e = e.split(t.initialPreviewDelimiter)), l = e.filter(function (e) {
                            return null !== e
                        }), l.length)) {
                        if (void 0 === s.content && (s.content = []), void 0 === s.config && (s.config = []), void 0 === s.tags && (s.tags = []), n) {
                            for (o = 0; o < e.length; o++)e[o] && s.content.push(e[o]);
                            for (o = 0; o < a.length; o++)a[o] && s.config.push(a[o]);
                            for (o = 0; o < r.length; o++)r[o] && s.tags.push(r[o])
                        } else s.content = e, s.config = a, s.tags = r;
                        t.previewCache.data = s
                    }
                }, unset: function (e) {
                    var i = t.previewCache.count();
                    if (i) {
                        if (1 === i)return t.previewCache.data.content = [], t.previewCache.data.config = [], t.previewCache.data.tags = [], t.initialPreview = [], t.initialPreviewConfig = [], void(t.initialPreviewThumbTags = []);
                        t.previewCache.data.content[e] = null, t.previewCache.data.config[e] = null, t.previewCache.data.tags[e] = null
                    }
                }, out: function () {
                    var e, i, a = "", r = t.previewCache.count(!0);
                    if (0 === r)return {content: "", caption: ""};
                    for (i = 0; r > i; i++)a += t.previewCache.get(i);
                    return e = t._getMsgSelected(t.previewCache.count()), {content: a, caption: e}
                }, footer: function (e, a, r) {
                    var n = t.previewCache.data;
                    if (!n || !n.config || 0 === n.config.length || i.isEmpty(n.config[e]))return "";
                    a = void 0 === a ? !0 : a;
                    var o = n.config[e], l = i.ifSet("caption", o), s = "", d = i.ifSet("width", o, "auto"),
                        c = i.ifSet("url", o, !1), p = i.ifSet("key", o, null), u = t.fileActionSettings,
                        f = i.ifSet("showDelete", o, !0), m = i.ifSet("showZoom", o, u.showZoom),
                        g = i.ifSet("showDrag", o, u.showDrag), v = c === !1 && a;
                    return t.initialPreviewShowDelete && (s = t._renderFileActions(!1, f, m, g, v, c, p, !0)), t._getLayoutTemplate("footer").replace(/\{progress}/g, t._renderThumbProgress()).replace(/\{actions}/g, s).replace(/\{caption}/g, l).replace(/\{size}/g, t._getSize(r)).replace(/\{width}/g, d).replace(/\{indicator}/g, "").replace(/\{indicatorTitle}/g, "")
                }
            }, t.previewCache.init()
        }, _handler: function (e, i, t) {
            var a = this, r = a.namespace, n = i.split(" ").join(r + " ") + r;
            e && e.length && e.off(n).on(n, t)
        }, _log: function (e) {
            var i = this, t = i.$element.attr("id");
            t && (e = '"' + t + '": ' + e), "undefined" != typeof window.console.log ? window.console.log(e) : window.alert(e)
        }, _validate: function () {
            var e = this, i = "file" === e.$element.attr("type");
            return i || e._log('The input "type" must be set to "file" for initializing the "bootstrap-fileinput" plugin.'), i
        }, _errorsExist: function () {
            var i, t = this;
            return t.$errorContainer.find("li").length ? !0 : (i = e(document.createElement("div")).html(t.$errorContainer.html()), i.find("span.kv-error-close").remove(), i.find("ul").remove(), !!e.trim(i.text()).length)
        }, _errorHandler: function (e, i) {
            var t = this, a = e.target.error;
            a.code === a.NOT_FOUND_ERR ? t._showError(t.msgFileNotFound.replace("{name}", i)) : a.code === a.SECURITY_ERR ? t._showError(t.msgFileSecured.replace("{name}", i)) : a.code === a.NOT_READABLE_ERR ? t._showError(t.msgFileNotReadable.replace("{name}", i)) : a.code === a.ABORT_ERR ? t._showError(t.msgFilePreviewAborted.replace("{name}", i)) : t._showError(t.msgFilePreviewError.replace("{name}", i))
        }, _addError: function (e) {
            var i = this, t = i.$errorContainer;
            e && t.length && (t.html(i.errorCloseButton + e), i._handler(t.find(".kv-error-close"), "click", function () {
                t.fadeOut("slow")
            }))
        }, _resetErrors: function (e) {
            var i = this, t = i.$errorContainer;
            i.isError = !1, i.$container.removeClass("has-error"), t.html(""), e ? t.fadeOut("slow") : t.hide()
        }, _showFolderError: function (e) {
            var t, a = this, r = a.$errorContainer;
            e && (t = a.msgFoldersNotAllowed.replace(/\{n}/g, e), a._addError(t), i.addCss(a.$container, "has-error"), r.fadeIn(800), a._raise("filefoldererror", [e, t]))
        }, _showUploadError: function (e, t, a) {
            var r = this, n = r.$errorContainer, o = a || "fileuploaderror",
                l = t && t.id ? '<li data-file-id="' + t.id + '">' + e + "</li>" : "<li>" + e + "</li>";
            return 0 === n.find("ul").length ? r._addError("<ul>" + l + "</ul>") : n.find("ul").append(l), n.fadeIn(800), r._raise(o, [t, e]), r.$container.removeClass("file-input-new"), i.addCss(r.$container, "has-error"), !0
        }, _showError: function (e, t, a) {
            var r = this, n = r.$errorContainer, o = a || "fileerror";
            return t = t || {}, t.reader = r.reader, r._addError(e), n.fadeIn(800), r._raise(o, [t, e]), r.isUploadable || r._clearFileInput(), r.$container.removeClass("file-input-new"), i.addCss(r.$container, "has-error"), r.$btnUpload.attr("disabled", !0), !0
        }, _noFilesError: function (e) {
            var t = this, a = t.minFileCount > 1 ? t.filePlural : t.fileSingle,
                r = t.msgFilesTooLess.replace("{n}", t.minFileCount).replace("{files}", a), n = t.$errorContainer;
            t._addError(r), t.isError = !0, t._updateFileDetails(0), n.fadeIn(800), t._raise("fileerror", [e, r]), t._clearFileInput(), i.addCss(t.$container, "has-error")
        }, _parseError: function (i, t, a, r) {
            var n = this, o = e.trim(a + ""), l = "." === o.slice(-1) ? "" : ".",
                s = void 0 !== t.responseJSON && void 0 !== t.responseJSON.error ? t.responseJSON.error : t.responseText;
            return n.cancelling && n.msgUploadAborted && (o = n.msgUploadAborted), n.showAjaxErrorDetails && s ? (s = e.trim(s.replace(/\n\s*\n/g, "\n")), s = s.length > 0 ? "<pre>" + s + "</pre>" : "", o += l + s) : o += l, o === l && (o = n.msgAjaxError.replace("{operation}", i)), n.cancelling = !1, r ? "<b>" + r + ": </b>" + o : o
        }, _parseFileType: function (e) {
            var t, a, r, n, o = this, l = o.allowedPreviewTypes;
            for (n = 0; n < l.length; n++)if (r = l[n], t = o.fileTypeSettings[r], a = t(e.type, e.name) ? r : "", !i.isEmpty(a))return a;
            return "other"
        }, _getPreviewIcon: function (i) {
            var t, a = this, r = null;
            return i && i.indexOf(".") > -1 && (t = i.split(".").pop(), a.previewFileIconSettings && a.previewFileIconSettings[t] && (r = a.previewFileIconSettings[t]), a.previewFileExtSettings && e.each(a.previewFileExtSettings, function (e, i) {
                return a.previewFileIconSettings[e] && i(t) ? void(r = a.previewFileIconSettings[e]) : void 0
            })), r
        }, _parseFilePreviewIcon: function (e, i) {
            var t = this, a = t._getPreviewIcon(i) || t.previewFileIcon;
            return e.indexOf("{previewFileIcon}") > -1 && (e = e.replace(/\{previewFileIconClass}/g, t.previewFileIconClass).replace(/\{previewFileIcon}/g, a)), e
        }, _raise: function (i, t) {
            var a = this, r = e.Event(i);
            if (void 0 !== t ? a.$element.trigger(r, t) : a.$element.trigger(r), r.isDefaultPrevented() || r.result === !1)return !1;
            switch (i) {
                case"filebatchuploadcomplete":
                case"filebatchuploadsuccess":
                case"fileuploaded":
                case"fileclear":
                case"filecleared":
                case"filereset":
                case"fileerror":
                case"filefoldererror":
                case"fileuploaderror":
                case"filebatchuploaderror":
                case"filedeleteerror":
                case"filecustomerror":
                case"filesuccessremove":
                    break;
                default:
                    a.ajaxAborted = r.result
            }
            return !0
        }, _listenFullScreen: function (e) {
            var i, t, a = this, r = a.$modal;
            r && r.length && (i = r && r.find(".btn-fullscreen"), t = r && r.find(".btn-borderless"), i.length && t.length && (i.removeClass("active").attr("aria-pressed", "false"), t.removeClass("active").attr("aria-pressed", "false"), e ? i.addClass("active").attr("aria-pressed", "true") : t.addClass("active").attr("aria-pressed", "true"), r.hasClass("file-zoom-fullscreen") ? a._maximizeZoomDialog() : e ? a._maximizeZoomDialog() : t.removeClass("active").attr("aria-pressed", "false")))
        }, _listen: function () {
            var t, a = this, r = a.$element, n = a.$form, o = a.$container;
            a._handler(r, "change", e.proxy(a._change, a)), a.showBrowse && a._handler(a.$btnFile, "click", e.proxy(a._browse, a)), a._handler(o.find(".fileinput-remove:not([disabled])"), "click", e.proxy(a.clear, a)), a._handler(o.find(".fileinput-cancel"), "click", e.proxy(a.cancel, a)), a._initDragDrop(), a._handler(n, "reset", e.proxy(a.reset, a)), a.isUploadable || a._handler(n, "submit", e.proxy(a._submitForm, a)), a._handler(a.$container.find(".fileinput-upload"), "click", e.proxy(a._uploadClick, a)), a._handler(e(window), "resize", function () {
                a._listenFullScreen(screen.width === window.innerWidth && screen.height === window.innerHeight)
            }), t = "webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange", a._handler(e(document), t, function () {
                a._listenFullScreen(i.checkFullScreen())
            }), a._initClickable()
        }, _initClickable: function () {
            var t, a = this;
            a.isClickable && (t = a.isUploadable ? a.$dropZone : a.$preview.find(".file-default-preview"), i.addCss(t, "clickable"), t.attr("tabindex", -1), a._handler(t, "click", function (i) {
                var r = e(i.target);
                r.parents(".file-preview-thumbnails").length && !r.parents(".file-default-preview").length || (a.$element.trigger("click"), t.blur())
            }))
        }, _initDragDrop: function () {
            var i = this, t = i.$dropZone;
            i.isUploadable && i.dropZoneEnabled && i.showPreview && (i._handler(t, "dragenter dragover", e.proxy(i._zoneDragEnter, i)), i._handler(t, "dragleave", e.proxy(i._zoneDragLeave, i)), i._handler(t, "drop", e.proxy(i._zoneDrop, i)), i._handler(e(document), "dragenter dragover drop", i._zoneDragDropInit))
        }, _zoneDragDropInit: function (e) {
            e.stopPropagation(), e.preventDefault()
        }, _zoneDragEnter: function (t) {
            var a = this, r = e.inArray("Files", t.originalEvent.dataTransfer.types) > -1;
            return a._zoneDragDropInit(t), a.isDisabled || !r ? (t.originalEvent.dataTransfer.effectAllowed = "none", void(t.originalEvent.dataTransfer.dropEffect = "none")) : void i.addCss(a.$dropZone, "file-highlighted")
        }, _zoneDragLeave: function (e) {
            var i = this;
            i._zoneDragDropInit(e), i.isDisabled || i.$dropZone.removeClass("file-highlighted")
        }, _zoneDrop: function (e) {
            var t = this;
            e.preventDefault(), t.isDisabled || i.isEmpty(e.originalEvent.dataTransfer.files) || (t._change(e, "dragdrop"), t.$dropZone.removeClass("file-highlighted"))
        }, _uploadClick: function (e) {
            var t, a = this, r = a.$container.find(".fileinput-upload"),
                n = !r.hasClass("disabled") && i.isEmpty(r.attr("disabled"));
            if (!e || !e.isDefaultPrevented()) {
                if (!a.isUploadable)return void(n && "submit" !== r.attr("type") && (t = r.closest("form"), t.length && t.trigger("submit"), e.preventDefault()));
                e.preventDefault(), n && a.upload()
            }
        }, _submitForm: function () {
            var e = this, i = e.$element, t = i.get(0).files;
            return t && e.minFileCount > 0 && e._getFileCount(t.length) < e.minFileCount ? (e._noFilesError({}), !1) : !e._abort({})
        }, _clearPreview: function () {
            var t = this, a = t.$preview,
                r = t.showUploadedThumbs ? a.find(i.FRAMES + ":not(.file-preview-success)") : a.find(i.FRAMES);
            r.each(function () {
                var t = e(this);
                t.remove(), i.cleanZoomCache(a.find("#zoom-" + t.attr("id")))
            }), t.$preview.find(i.FRAMES).length && t.showPreview || t._resetUpload(), t._validateDefaultPreview()
        }, _initSortable: function () {
            var t, a = this, r = a.$preview, n = "." + i.SORT_CSS;
            window.KvSortable && 0 !== r.find(n).length && (t = {
                handle: ".drag-handle-init",
                dataIdAttr: "data-preview-id",
                draggable: n,
                onSort: function (t) {
                    var r, n, o = t.oldIndex, l = t.newIndex;
                    a.initialPreview = i.moveArray(a.initialPreview, o, l), a.initialPreviewConfig = i.moveArray(a.initialPreviewConfig, o, l), a.previewCache.init();
                    for (var s = 0; s < a.initialPreviewConfig.length; s++)null !== a.initialPreviewConfig[s] && (r = a.initialPreviewConfig[s].key, n = e(".kv-file-remove[data-key='" + r + "']").closest(i.FRAMES), n.attr("data-fileindex", "init_" + s).data("fileindex", "init_" + s));
                    a._raise("filesorted", {
                        previewId: e(t.item).attr("id"),
                        oldIndex: o,
                        newIndex: l,
                        stack: a.initialPreviewConfig
                    })
                }
            }, r.data("kvsortable") && r.kvsortable("destroy"), e.extend(!0, t, a.fileActionSettings.dragSettings), r.kvsortable(t))
        }, _initPreview: function (e) {
            var t, a = this, r = a.initialCaption || "";
            return a.previewCache.count() ? (t = a.previewCache.out(), r = e && a.initialCaption ? a.initialCaption : t.caption, a.$preview.html(t.content), a._setInitThumbAttr(), a._setCaption(r), a._initSortable(), void(i.isEmpty(t.content) || a.$container.removeClass("file-input-new"))) : (a._clearPreview(), void(e ? a._setCaption(r) : a._initCaption()))
        }, _getZoomButton: function (e) {
            var i = this, t = i.previewZoomButtonIcons[e], a = i.previewZoomButtonClasses[e],
                r = ' title="' + (i.previewZoomButtonTitles[e] || "") + '" ',
                n = r + ("close" === e ? ' data-dismiss="modal" aria-hidden="true"' : "");
            return "fullscreen" !== e && "borderless" !== e && "toggleheader" !== e || (n += ' data-toggle="button" aria-pressed="false" autocomplete="off"'), '<button type="button" class="' + a + " btn-" + e + '"' + n + ">" + t + "</button>"
        }, _getModalContent: function () {
            var e = this;
            return e._getLayoutTemplate("modal").replace(/\{heading}/g, e.msgZoomModalHeading).replace(/\{prev}/g, e._getZoomButton("prev")).replace(/\{next}/g, e._getZoomButton("next")).replace(/\{toggleheader}/g, e._getZoomButton("toggleheader")).replace(/\{fullscreen}/g, e._getZoomButton("fullscreen")).replace(/\{borderless}/g, e._getZoomButton("borderless")).replace(/\{close}/g, e._getZoomButton("close"))
        }, _listenModalEvent: function (e) {
            var t = this, a = t.$modal, r = function (e) {
                return {sourceEvent: e, previewId: a.data("previewId"), modal: a}
            };
            a.on(e + ".bs.modal", function (n) {
                var o = a.find(".btn-fullscreen"), l = a.find(".btn-borderless");
                t._raise("filezoom" + e, r(n)), "shown" === e && (l.removeClass("active").attr("aria-pressed", "false"), o.removeClass("active").attr("aria-pressed", "false"), a.hasClass("file-zoom-fullscreen") && (t._maximizeZoomDialog(), i.checkFullScreen() ? o.addClass("active").attr("aria-pressed", "true") : l.addClass("active").attr("aria-pressed", "true")))
            })
        }, _initZoom: function () {
            var t, a = this, r = a._getLayoutTemplate("modalMain"), n = "#" + i.MODAL_ID;
            a.showPreview && (a.$modal = e(n), a.$modal && a.$modal.length || (t = e(document.createElement("div")).html(r).insertAfter(a.$container), a.$modal = e("#" + i.MODAL_ID).insertBefore(t), t.remove()), i.initModal(a.$modal), a.$modal.html(a._getModalContent()), e.each(i.MODAL_EVENTS, function (e, i) {
                a._listenModalEvent(i)
            }))
        }, _initZoomButtons: function () {
            var t, a, r = this, n = r.$modal.data("previewId") || "", o = r.$preview, l = o.find(i.FRAMES).toArray(),
                s = l.length, d = r.$modal.find(".btn-prev"), c = r.$modal.find(".btn-next");
            return l.length < 2 ? (d.hide(), void c.hide()) : (d.show(), c.show(), void(s && (t = e(l[0]), a = e(l[s - 1]), d.removeAttr("disabled"), c.removeAttr("disabled"), t.length && t.attr("id") === n && d.attr("disabled", !0), a.length && a.attr("id") === n && c.attr("disabled", !0))))
        }, _maximizeZoomDialog: function () {
            var i = this, t = i.$modal, a = t.find(".modal-header:visible"), r = t.find(".modal-footer:visible"),
                n = t.find(".modal-body"), o = e(window).height(), l = 0;
            t.addClass("file-zoom-fullscreen"), a && a.length && (o -= a.outerHeight(!0)), r && r.length && (o -= r.outerHeight(!0)), n && n.length && (l = n.outerHeight(!0) - n.height(), o -= l), t.find(".kv-zoom-body").height(o)
        }, _resizeZoomDialog: function (e) {
            var t = this, a = t.$modal, r = a.find(".btn-fullscreen"), n = a.find(".btn-borderless");
            if (a.hasClass("file-zoom-fullscreen")) i.toggleFullScreen(!1), e ? r.hasClass("active") || (a.removeClass("file-zoom-fullscreen"), t._resizeZoomDialog(!0), n.hasClass("active") && n.removeClass("active").attr("aria-pressed", "false")) : r.hasClass("active") ? r.removeClass("active").attr("aria-pressed", "false") : (a.removeClass("file-zoom-fullscreen"), t.$modal.find(".kv-zoom-body").css("height", t.zoomModalHeight)); else {
                if (!e)return void t._maximizeZoomDialog();
                i.toggleFullScreen(!0)
            }
            a.focus()
        }, _setZoomContent: function (t, a) {
            var r, n, o, l, s, d, c, p, u, f, m = this, g = t.attr("id"), v = m.$modal, h = v.find(".btn-prev"),
                w = v.find(".btn-next"), _ = v.find(".btn-fullscreen"), b = v.find(".btn-borderless"),
                C = v.find(".btn-toggleheader"), y = m.$preview.find("#zoom-" + g);
            n = y.attr("data-template") || "generic", r = y.find(".kv-file-content"), o = r.length ? r.html() : "", u = t.data("caption") || "", f = t.data("size") || "", l = u + " " + f, v.find(".kv-zoom-title").html(l), s = v.find(".kv-zoom-body"), v.removeClass("kv-single-content"), a ? (p = s.clone().insertAfter(s), s.html(o).hide(), p.fadeOut("fast", function () {
                s.fadeIn("fast"), p.remove()
            })) : s.html(o), c = m.previewZoomSettings[n], c && (d = s.find(".kv-preview-data"), i.addCss(d, "file-zoom-detail"), e.each(c, function (e, i) {
                d.css(e, i), (d.attr("width") && "width" === e || d.attr("height") && "height" === e) && d.removeAttr(e)
            })), v.data("previewId", g), m._handler(h, "click", function () {
                m._zoomSlideShow("prev", g)
            }), m._handler(w, "click", function () {
                m._zoomSlideShow("next", g)
            }), m._handler(_, "click", function () {
                m._resizeZoomDialog(!0)
            }), m._handler(b, "click", function () {
                m._resizeZoomDialog(!1)
            }), m._handler(C, "click", function () {
                var e, i = v.find(".modal-header"), t = v.find(".modal-body .floating-buttons"),
                    a = i.find(".kv-zoom-actions"), r = function (e) {
                        var t = m.$modal.find(".kv-zoom-body"), a = m.zoomModalHeight;
                        v.hasClass("file-zoom-fullscreen") && (a = t.outerHeight(!0),
                        e || (a -= i.outerHeight(!0))), t.css("height", e ? a + e : a)
                    };
                i.is(":visible") ? (e = i.outerHeight(!0), i.slideUp("slow", function () {
                    a.find(".btn").appendTo(t), r(e)
                })) : (t.find(".btn").appendTo(a), i.slideDown("slow", function () {
                    r()
                })), v.focus()
            }), m._handler(v, "keydown", function (e) {
                var i = e.which || e.keyCode;
                37 !== i || h.attr("disabled") || m._zoomSlideShow("prev", g), 39 !== i || w.attr("disabled") || m._zoomSlideShow("next", g)
            })
        }, _zoomPreview: function (e) {
            var t, a = this, r = a.$modal;
            if (!e.length)throw"Cannot zoom to detailed preview!";
            i.initModal(r), r.html(a._getModalContent()), t = e.closest(i.FRAMES), a._setZoomContent(t), r.modal("show"), a._initZoomButtons()
        }, _zoomSlideShow: function (t, a) {
            var r, n, o, l = this, s = l.$modal.find(".kv-zoom-actions .btn-" + t),
                d = l.$preview.find(i.FRAMES).toArray(), c = d.length;
            if (!s.attr("disabled")) {
                for (n = 0; c > n; n++)if (e(d[n]).attr("id") === a) {
                    o = "prev" === t ? n - 1 : n + 1;
                    break
                }
                0 > o || o >= c || !d[o] || (r = e(d[o]), r.length && l._setZoomContent(r, !0), l._initZoomButtons(), l._raise("filezoom" + t, {
                    previewId: a,
                    modal: l.$modal
                }))
            }
        }, _initZoomButton: function () {
            var i = this;
            i.$preview.find(".kv-file-zoom").each(function () {
                var t = e(this);
                i._handler(t, "click", function () {
                    i._zoomPreview(t)
                })
            })
        }, _clearObjects: function (i) {
            i.find("video audio").each(function () {
                this.pause(), e(this).remove()
            }), i.find("img object div").each(function () {
                e(this).remove()
            })
        }, _clearFileInput: function () {
            var t, a, r, n = this, o = n.$element;
            n.fileInputCleared = !0, i.isEmpty(o.val()) || (n.isIE9 || n.isIE10 ? (t = o.closest("form"), a = e(document.createElement("form")), r = e(document.createElement("div")), o.before(r), t.length ? t.after(a) : r.after(a), a.append(o).trigger("reset"), r.before(o).remove(), a.remove()) : o.val(""))
        }, _resetUpload: function () {
            var e = this;
            e.uploadCache = {
                content: [],
                config: [],
                tags: [],
                append: !0
            }, e.uploadCount = 0, e.uploadStatus = {}, e.uploadLog = [], e.uploadAsyncCount = 0, e.loadedImages = [], e.totalImagesCount = 0, e.$btnUpload.removeAttr("disabled"), e._setProgress(0), i.addCss(e.$progress, "hide"), e._resetErrors(!1), e.ajaxAborted = !1, e.ajaxRequests = [], e._resetCanvas(), e.cacheInitialPreview = {}, e.overwriteInitial && (e.initialPreview = [], e.initialPreviewConfig = [], e.initialPreviewThumbTags = [], e.previewCache.data = {
                content: [],
                config: [],
                tags: []
            })
        }, _resetCanvas: function () {
            var e = this;
            e.canvas && e.imageCanvasContext && e.imageCanvasContext.clearRect(0, 0, e.canvas.width, e.canvas.height)
        }, _hasInitialPreview: function () {
            var e = this;
            return !e.overwriteInitial && e.previewCache.count()
        }, _resetPreview: function () {
            var e, i, t = this;
            t.previewCache.count() ? (e = t.previewCache.out(), t.$preview.html(e.content), t._setInitThumbAttr(), i = t.initialCaption ? t.initialCaption : e.caption, t._setCaption(i)) : (t._clearPreview(), t._initCaption()), t.showPreview && (t._initZoom(), t._initSortable())
        }, _clearDefaultPreview: function () {
            var e = this;
            e.$preview.find(".file-default-preview").remove()
        }, _validateDefaultPreview: function () {
            var e = this;
            e.showPreview && !i.isEmpty(e.defaultPreviewContent) && (e.$preview.html('<div class="file-default-preview">' + e.defaultPreviewContent + "</div>"), e.$container.removeClass("file-input-new"), e._initClickable())
        }, _resetPreviewThumbs: function (e) {
            var i, t = this;
            return e ? (t._clearPreview(), void t.clearStack()) : void(t._hasInitialPreview() ? (i = t.previewCache.out(), t.$preview.html(i.content), t._setInitThumbAttr(), t._setCaption(i.caption), t._initPreviewActions()) : t._clearPreview())
        }, _getLayoutTemplate: function (e) {
            var t = this, a = t.layoutTemplates[e];
            return i.isEmpty(t.customLayoutTags) ? a : i.replaceTags(a, t.customLayoutTags)
        }, _getPreviewTemplate: function (e) {
            var t = this, a = t.previewTemplates[e];
            return i.isEmpty(t.customPreviewTags) ? a : i.replaceTags(a, t.customPreviewTags)
        }, _getOutData: function (e, i, t) {
            var a = this;
            return e = e || {}, i = i || {}, t = t || a.filestack.slice(0) || {}, {
                form: a.formdata,
                files: t,
                filenames: a.filenames,
                filescount: a.getFilesCount(),
                extra: a._getExtraData(),
                response: i,
                reader: a.reader,
                jqXHR: e
            }
        }, _getMsgSelected: function (e) {
            var i = this, t = 1 === e ? i.fileSingle : i.filePlural;
            return e > 0 ? i.msgSelected.replace("{n}", e).replace("{files}", t) : i.msgNoFilesSelected
        }, _getThumbs: function (e) {
            return e = e || "", this.$preview.find(i.FRAMES + ":not(.file-preview-initial)" + e)
        }, _getExtraData: function (e, i) {
            var t = this, a = t.uploadExtraData;
            return "function" == typeof t.uploadExtraData && (a = t.uploadExtraData(e, i)), a
        }, _initXhr: function (e, i, t) {
            var a = this;
            return e.upload && e.upload.addEventListener("progress", function (e) {
                var r = 0, n = e.total, o = e.loaded || e.position;
                e.lengthComputable && (r = Math.floor(o / n * 100)), i ? a._setAsyncUploadStatus(i, r, t) : a._setProgress(r)
            }, !1), e
        }, _ajaxSubmit: function (i, t, a, r, n, o) {
            var l, s = this;
            s._raise("filepreajax", [n, o]) && (s._uploadExtra(n, o), l = e.extend(!0, {}, {
                xhr: function () {
                    var i = e.ajaxSettings.xhr();
                    return s._initXhr(i, n, s.getFileStack().length)
                },
                url: s.uploadUrl,
                type: "POST",
                dataType: "json",
                data: s.formdata,
                cache: !1,
                processData: !1,
                contentType: !1,
                beforeSend: i,
                success: t,
                complete: a,
                error: r
            }, s.ajaxSettings), s.ajaxRequests.push(e.ajax(l)))
        }, _mergeArray: function (e, t) {
            var a = this, r = i.cleanArray(a[e]), n = i.cleanArray(t);
            a[e] = r.concat(n)
        }, _initUploadSuccess: function (t, a, r) {
            var n, o, l, s, d, c, p, u, f, m = this;
            m.showPreview && "object" == typeof t && !e.isEmptyObject(t) && void 0 !== t.initialPreview && t.initialPreview.length > 0 && (m.hasInitData = !0, c = t.initialPreview || [], p = t.initialPreviewConfig || [], u = t.initialPreviewThumbTags || [], n = !(void 0 !== t.append && !t.append), c.length > 0 && !i.isArray(c) && (c = c.split(m.initialPreviewDelimiter)), m._mergeArray("initialPreview", c), m._mergeArray("initialPreviewConfig", p), m._mergeArray("initialPreviewThumbTags", u), void 0 !== a ? r ? (f = a.attr("data-fileindex"), m.uploadCache.content[f] = c[0], m.uploadCache.config[f] = p[0] || [], m.uploadCache.tags[f] = u[0] || [], m.uploadCache.append = n) : (l = m.previewCache.add(c, p[0], u[0], n), o = m.previewCache.get(l, !1), s = e(document.createElement("div")).html(o).hide().insertAfter(a), d = s.find(".kv-zoom-cache"), d && d.length && d.insertAfter(a), a.fadeOut("slow", function () {
                var e = s.find(".file-preview-frame");
                e && e.length && e.insertBefore(a).fadeIn("slow").css("display:inline-block"), m._initPreviewActions(), m._clearFileInput(), i.cleanZoomCache(m.$preview.find("#zoom-" + a.attr("id"))), a.remove(), s.remove(), m._initSortable()
            })) : (m.previewCache.set(c, p, u, n), m._initPreview(), m._initPreviewActions()))
        }, _initSuccessThumbs: function () {
            var t = this;
            t.showPreview && t._getThumbs(i.FRAMES + ".file-preview-success").each(function () {
                var a = e(this), r = t.$preview, n = a.find(".kv-file-remove");
                n.removeAttr("disabled"), t._handler(n, "click", function () {
                    var e = a.attr("id"), n = t._raise("filesuccessremove", [e, a.data("fileindex")]);
                    i.cleanMemory(a), n !== !1 && a.fadeOut("slow", function () {
                        i.cleanZoomCache(r.find("#zoom-" + e)), a.remove(), r.find(i.FRAMES).length || t.reset()
                    })
                })
            })
        }, _checkAsyncComplete: function () {
            var i, t, a = this;
            for (t = 0; t < a.filestack.length; t++)if (a.filestack[t] && (i = a.previewInitId + "-" + t, -1 === e.inArray(i, a.uploadLog)))return !1;
            return a.uploadAsyncCount === a.uploadLog.length
        }, _uploadExtra: function (i, t) {
            var a = this, r = a._getExtraData(i, t);
            0 !== r.length && e.each(r, function (e, i) {
                a.formdata.append(e, i)
            })
        }, _uploadSingle: function (t, a, r) {
            var n, o, l, s, d, c, p, u, f, m, g = this, v = g.getFileStack().length, h = new FormData,
                w = g.previewInitId + "-" + t, _ = g.filestack.length > 0 || !e.isEmptyObject(g.uploadExtraData),
                b = e("#" + w).find(".file-thumb-progress"), C = {id: w, index: t};
            g.formdata = h, g.showPreview && (o = e("#" + w + ":not(.file-preview-initial)"), s = o.find(".kv-file-upload"), d = o.find(".kv-file-remove"), b.removeClass("hide")), 0 === v || !_ || s && s.hasClass("disabled") || g._abort(C) || (m = function (e, i) {
                g.updateStack(e, void 0), g.uploadLog.push(i), g._checkAsyncComplete() && (g.fileBatchCompleted = !0)
            }, l = function () {
                var e, t, a, r = g.uploadCache, n = 0, o = g.cacheInitialPreview;
                g.fileBatchCompleted && (o && o.content && (n = o.content.length), setTimeout(function () {
                    if (g.showPreview) {
                        if (g.previewCache.set(r.content, r.config, r.tags, r.append), n) {
                            for (t = 0; t < r.content.length; t++)a = t + n, o.content[a] = r.content[t], o.config.length && (o.config[a] = r.config[t]), o.tags.length && (o.tags[a] = r.tags[t]);
                            g.initialPreview = i.cleanArray(o.content), g.initialPreviewConfig = i.cleanArray(o.config), g.initialPreviewThumbTags = i.cleanArray(o.tags)
                        } else g.initialPreview = r.content, g.initialPreviewConfig = r.config, g.initialPreviewThumbTags = r.tags;
                        g.cacheInitialPreview = {}, g.hasInitData && (g._initPreview(), g._initPreviewActions())
                    }
                    g.unlock(), g._clearFileInput(), e = g.$preview.find(".file-preview-initial"), g.uploadAsync && e.length && (i.addCss(e, i.SORT_CSS), g._initSortable()), g._raise("filebatchuploadcomplete", [g.filestack, g._getExtraData()]), g.uploadCount = 0, g.uploadStatus = {}, g.uploadLog = [], g._setProgress(101)
                }, 100))
            }, c = function (a) {
                n = g._getOutData(a), g.fileBatchCompleted = !1, g.showPreview && (o.hasClass("file-preview-success") || (g._setThumbStatus(o, "Loading"), i.addCss(o, "file-uploading")), s.attr("disabled", !0), d.attr("disabled", !0)), r || g.lock(), g._raise("filepreupload", [n, w, t]), e.extend(!0, C, n), g._abort(C) && (a.abort(), g._setProgressCancelled())
            }, p = function (a, l, d) {
                var c = g.showPreview && o.attr("id") ? o.attr("id") : w;
                n = g._getOutData(d, a), e.extend(!0, C, n), setTimeout(function () {
                    i.isEmpty(a) || i.isEmpty(a.error) ? (g.showPreview && (g._setThumbStatus(o, "Success"), s.hide(), g._initUploadSuccess(a, o, r), g._setProgress(101, b)), g._raise("fileuploaded", [n, c, t]), r ? m(t, c) : g.updateStack(t, void 0)) : (g._showUploadError(a.error, C), g._setPreviewError(o, t), r && m(t, c))
                }, 100)
            }, u = function () {
                setTimeout(function () {
                    g.showPreview && (s.removeAttr("disabled"), d.removeAttr("disabled"), o.removeClass("file-uploading")), r ? l() : (g.unlock(!1), g._clearFileInput()), g._initSuccessThumbs()
                }, 100)
            }, f = function (i, n, l) {
                var s = g.ajaxOperations.uploadThumb, d = g._parseError(s, i, l, r ? a[t].name : null);
                setTimeout(function () {
                    r && m(t, w), g.uploadStatus[w] = 100, g._setPreviewError(o, t), e.extend(!0, C, g._getOutData(i)), g._setProgress(101, b, g.msgAjaxProgressError.replace("{operation}", s)), g._showUploadError(d, C)
                }, 100)
            }, h.append(g.uploadFileAttr, a[t], g.filenames[t]), h.append("file_id", t), g._ajaxSubmit(c, p, u, f, w, t))
        }, _uploadBatch: function () {
            var t, a, r, n, o, l = this, s = l.filestack, d = s.length, c = {},
                p = l.filestack.length > 0 || !e.isEmptyObject(l.uploadExtraData);
            l.formdata = new FormData, 0 !== d && p && !l._abort(c) && (o = function () {
                e.each(s, function (e) {
                    l.updateStack(e, void 0)
                }), l._clearFileInput()
            }, t = function (t) {
                l.lock();
                var a = l._getOutData(t);
                l.showPreview && l._getThumbs().each(function () {
                    var t = e(this), a = t.find(".kv-file-upload"), r = t.find(".kv-file-remove");
                    t.hasClass("file-preview-success") || (l._setThumbStatus(t, "Loading"), i.addCss(t, "file-uploading")), a.attr("disabled", !0), r.attr("disabled", !0)
                }), l._raise("filebatchpreupload", [a]), l._abort(a) && (t.abort(), l._setProgressCancelled())
            }, a = function (t, a, r) {
                var n = l._getOutData(r, t), s = l._getThumbs(":not(.file-preview-error)"), d = 0,
                    c = i.isEmpty(t) || i.isEmpty(t.errorkeys) ? [] : t.errorkeys;
                i.isEmpty(t) || i.isEmpty(t.error) ? (l._raise("filebatchuploadsuccess", [n]), o(), l.showPreview ? (s.each(function () {
                    var i = e(this), t = i.find(".kv-file-upload");
                    i.find(".kv-file-upload").hide(), l._setThumbStatus(i, "Success"), i.removeClass("file-uploading"), t.removeAttr("disabled")
                }), l._initUploadSuccess(t)) : l.reset(), l._setProgress(101)) : (l.showPreview && (s.each(function () {
                    var i = e(this), t = i.find(".kv-file-remove"), a = i.find(".kv-file-upload");
                    return i.removeClass("file-uploading"), a.removeAttr("disabled"), t.removeAttr("disabled"), 0 === c.length ? void l._setPreviewError(i) : (-1 !== e.inArray(d, c) ? l._setPreviewError(i) : (i.find(".kv-file-upload").hide(), l._setThumbStatus(i, "Success"), l.updateStack(d, void 0)), void d++)
                }), l._initUploadSuccess(t)), l._showUploadError(t.error, n, "filebatchuploaderror"))
            }, n = function () {
                l.unlock(), l._initSuccessThumbs(), l._clearFileInput(), l._raise("filebatchuploadcomplete", [l.filestack, l._getExtraData()])
            }, r = function (i, t, a) {
                var r = l._getOutData(i), n = l.ajaxOperations.uploadBatch, o = l._parseError(n, i, a);
                l._showUploadError(o, r, "filebatchuploaderror"), l.uploadFileCount = d - 1, l.showPreview && (l._getThumbs().each(function () {
                    var i = e(this), t = i.attr("data-fileindex");
                    i.removeClass("file-uploading"), void 0 !== l.filestack[t] && l._setPreviewError(i)
                }), l._getThumbs().removeClass("file-uploading"), l._getThumbs(" .kv-file-upload").removeAttr("disabled"), l._getThumbs(" .kv-file-delete").removeAttr("disabled"), l._setProgress(101, l.$progress, l.msgAjaxProgressError.replace("{operation}", n)))
            }, e.each(s, function (e, t) {
                i.isEmpty(s[e]) || l.formdata.append(l.uploadFileAttr, t, l.filenames[e])
            }), l._ajaxSubmit(t, a, n, r))
        }, _uploadExtraOnly: function () {
            var e, t, a, r, n = this, o = {};
            n.formdata = new FormData, n._abort(o) || (e = function (e) {
                n.lock();
                var i = n._getOutData(e);
                n._raise("filebatchpreupload", [i]), n._setProgress(50), o.data = i, o.xhr = e, n._abort(o) && (e.abort(), n._setProgressCancelled())
            }, t = function (e, t, a) {
                var r = n._getOutData(a, e);
                i.isEmpty(e) || i.isEmpty(e.error) ? (n._raise("filebatchuploadsuccess", [r]), n._clearFileInput(), n._initUploadSuccess(e), n._setProgress(101)) : n._showUploadError(e.error, r, "filebatchuploaderror")
            }, a = function () {
                n.unlock(), n._clearFileInput(), n._raise("filebatchuploadcomplete", [n.filestack, n._getExtraData()])
            }, r = function (e, i, t) {
                var a = n._getOutData(e), r = n.ajaxOperations.uploadExtra, l = n._parseError(r, e, t);
                o.data = a, n._showUploadError(l, a, "filebatchuploaderror"), n._setProgress(101, n.$progress, n.msgAjaxProgressError.replace("{operation}", r))
            }, n._ajaxSubmit(e, t, a, r))
        }, _deleteFileIndex: function (t) {
            var a = this, r = t.attr("data-fileindex");
            "init_" === r.substring(0, 5) && (r = parseInt(r.replace("init_", "")), a.initialPreview = i.spliceArray(a.initialPreview, r), a.initialPreviewConfig = i.spliceArray(a.initialPreviewConfig, r), a.initialPreviewThumbTags = i.spliceArray(a.initialPreviewThumbTags, r), a.$preview.find(i.FRAMES).each(function () {
                var i = e(this), t = i.attr("data-fileindex");
                "init_" === t.substring(0, 5) && (t = parseInt(t.replace("init_", "")), t > r && (t--, i.attr("data-fileindex", "init_" + t)))
            }), a.uploadAsync && (a.cacheInitialPreview = a.getPreview()))
        }, _initFileActions: function () {
            var t = this, a = t.$preview;
            t.showPreview && (t._initZoomButton(), a.find(i.FRAMES + " .kv-file-remove").each(function () {
                var r, n, o, l, s = e(this), d = s.closest(i.FRAMES), c = d.attr("id"), p = d.attr("data-fileindex");
                t._handler(s, "click", function () {
                    return l = t._raise("filepreremove", [c, p]), l !== !1 && t._validateMinCount() ? (r = d.hasClass("file-preview-error"), i.cleanMemory(d), void d.fadeOut("slow", function () {
                        i.cleanZoomCache(a.find("#zoom-" + c)), t.updateStack(p, void 0), t._clearObjects(d), d.remove(), c && r && t.$errorContainer.find('li[data-file-id="' + c + '"]').fadeOut("fast", function () {
                            e(this).remove(), t._errorsExist() || t._resetErrors()
                        }), t._clearFileInput();
                        var l = t.getFileStack(!0), s = t.previewCache.count(), u = l.length,
                            f = t.showPreview && a.find(i.FRAMES).length;
                        0 !== u || 0 !== s || f ? (n = s + u, o = n > 1 ? t._getMsgSelected(n) : l[0] ? t._getFileNames()[0] : "", t._setCaption(o)) : t.reset(), t._raise("fileremoved", [c, p])
                    })) : !1
                })
            }), t.$preview.find(i.FRAMES + " .kv-file-upload").each(function () {
                var a = e(this);
                t._handler(a, "click", function () {
                    var e = a.closest(i.FRAMES), r = e.attr("data-fileindex");
                    e.hasClass("file-preview-error") || t._uploadSingle(r, t.filestack, !1)
                })
            }))
        }, _initPreviewActions: function () {
            var t = this, a = t.$preview, r = t.deleteExtraData || {}, n = i.FRAMES + " .kv-file-remove",
                o = function () {
                    var e = t.isUploadable ? t.previewCache.count() : t.$element.get(0).files.length;
                    0 !== a.find(n).length || e || (t.reset(), t.initialCaption = "")
                };
            t._initZoomButton(), a.find(n).each(function () {
                var n = e(this), l = n.data("url") || t.deleteUrl, s = n.data("key");
                if (!i.isEmpty(l) && void 0 !== s) {
                    var d, c, p, u, f = n.closest(i.FRAMES), m = t.previewCache.data, g = f.data("fileindex");
                    g = parseInt(g.replace("init_", "")), p = i.isEmpty(m.config) && i.isEmpty(m.config[g]) ? null : m.config[g], u = i.isEmpty(p) || i.isEmpty(p.extra) ? r : p.extra, "function" == typeof u && (u = u()), c = {
                        id: n.attr("id"),
                        key: s,
                        extra: u
                    }, d = e.extend(!0, {}, {
                        url: l,
                        type: "POST",
                        dataType: "json",
                        data: e.extend(!0, {}, {key: s}, u),
                        beforeSend: function (e) {
                            t.ajaxAborted = !1, t._raise("filepredelete", [s, e, u]), t.ajaxAborted ? e.abort() : (i.addCss(f, "file-uploading"), i.addCss(n, "disabled"))
                        },
                        success: function (e, r, l) {
                            var d, p;
                            return i.isEmpty(e) || i.isEmpty(e.error) ? (t.previewCache.init(), g = parseInt(f.data("fileindex").replace("init_", "")), t.previewCache.unset(g), d = t.previewCache.count(), p = d > 0 ? t._getMsgSelected(d) : "", t._deleteFileIndex(f), t._setCaption(p), t._raise("filedeleted", [s, l, u]), f.removeClass("file-uploading").addClass("file-deleted"), void f.fadeOut("slow", function () {
                                i.cleanZoomCache(a.find("#zoom-" + f.attr("id"))), t._clearObjects(f), f.remove(), o(), d || 0 !== t.getFileStack().length || (t._setCaption(""), t.reset())
                            })) : (c.jqXHR = l, c.response = e, t._showError(e.error, c, "filedeleteerror"), f.removeClass("file-uploading"), n.removeClass("disabled"), void o())
                        },
                        error: function (e, i, a) {
                            var r = t.ajaxOperations.deleteThumb, n = t._parseError(r, e, a);
                            c.jqXHR = e, c.response = {}, t._showError(n, c, "filedeleteerror"), f.removeClass("file-uploading"), o()
                        }
                    }, t.ajaxDeleteSettings), t._handler(n, "click", function () {
                        return t._validateMinCount() ? void e.ajax(d) : !1
                    })
                }
            })
        }, _hideFileIcon: function () {
            this.overwriteInitial && this.$captionContainer.find(".kv-caption-icon").hide()
        }, _showFileIcon: function () {
            this.$captionContainer.find(".kv-caption-icon").show()
        }, _getSize: function (i) {
            var t, a, r, n = this, o = parseFloat(i), l = n.fileSizeGetter;
            return e.isNumeric(i) && e.isNumeric(o) ? ("function" == typeof l ? r = l(o) : 0 === o ? r = "0.00 B" : (t = Math.floor(Math.log(o) / Math.log(1024)), a = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], r = 1 * (o / Math.pow(1024, t)).toFixed(2) + " " + a[t]), n._getLayoutTemplate("size").replace("{sizeText}", r)) : ""
        }, _generatePreviewTemplate: function (e, t, a, r, n, o, l, s, d, c, p) {
            var u, f = this, m = f.slug(a), g = "", v = f.previewSettings[e] || f.defaults.previewSettings[e],
                h = v && v.width ? v.width : "", w = v && v.height ? v.height : "",
                _ = d || f._renderFileFooter(m, l, i.isEmpty(h) ? "auto" : h, o), b = f._getPreviewIcon(a),
                C = b && f.preferIconicPreview, y = b && f.preferIconicZoomPreview, E = function (t, o, l, d) {
                    var u = l ? "zoom-" + n : n, g = f._getPreviewTemplate(t), v = (s || "") + " " + d;
                    return f.frameClass && (v = f.frameClass + " " + v), l && (v = v.replace(" " + i.SORT_CSS, "")), g = f._parseFilePreviewIcon(g, a), "text" === t && (o = i.htmlEncode(o)), g.replace(/\{previewId}/g, u).replace(/\{caption}/g, m).replace(/\{frameClass}/g, v).replace(/\{type}/g, r).replace(/\{fileindex}/g, c).replace(/\{width}/g, h).replace(/\{height}/g, w).replace(/\{footer}/g, _).replace(/\{data}/g, o).replace(/\{template}/g, p || e)
                };
            return c = c || n.slice(n.lastIndexOf("-") + 1), f.fileActionSettings.showZoom && (g = E(y ? "other" : e, t, !0, "kv-zoom-thumb")), g = "\n" + f._getLayoutTemplate("zoomCache").replace("{zoomContent}", g), u = E(C ? "other" : e, t, !1, "kv-preview-thumb"), u + g
        }, _previewDefault: function (t, a, r) {
            var n = this, o = n.$preview;
            if (n.showPreview) {
                var l, s = t ? t.name : "", d = t ? t.type : "", c = t.size || 0, p = n.slug(s),
                    u = r === !0 && !n.isUploadable, f = i.objUrl.createObjectURL(t);
                n._clearDefaultPreview(), l = n._generatePreviewTemplate("other", f, s, d, a, u, c), o.append("\n" + l), n._setThumbAttr(a, p, c), r === !0 && n.isUploadable && n._setThumbStatus(e("#" + a), "Error")
            }
        }, _previewFile: function (e, i, t, a, r) {
            if (this.showPreview) {
                var n, o = this, l = o._parseFileType(i), s = i ? i.name : "", d = o.slug(s), c = o.allowedPreviewTypes,
                    p = o.allowedPreviewMimeTypes, u = o.$preview, f = c && c.indexOf(l) >= 0, m = i.size || 0,
                    g = "text" === l || "html" === l || "image" === l ? t.target.result : r,
                    v = p && -1 !== p.indexOf(i.type);
                "html" === l && o.purifyHtml && window.DOMPurify && (g = window.DOMPurify.sanitize(g)), f || v ? (n = o._generatePreviewTemplate(l, g, s, i.type, a, !1, m), o._clearDefaultPreview(), u.append("\n" + n), o._validateImage(a, d, i.type)) : o._previewDefault(i, a), o._setThumbAttr(a, d, m), o._initSortable()
            }
        }, _setThumbAttr: function (i, t, a) {
            var r = this, n = e("#" + i);
            n.length && (a = a && a > 0 ? r._getSize(a) : "", n.data({caption: t, size: a}))
        }, _setInitThumbAttr: function () {
            var e, t, a, r, n = this, o = n.previewCache.data, l = n.previewCache.count(!0);
            if (0 !== l)for (var s = 0; l > s; s++)e = o.config[s], r = n.previewInitId + "-init_" + s, t = i.ifSet("caption", e, i.ifSet("filename", e)), a = i.ifSet("size", e), n._setThumbAttr(r, t, a)
        }, _slugDefault: function (e) {
            return i.isEmpty(e) ? "" : String(e).replace(/[\-\[\]\/\{}:;#%=\(\)\*\+\?\\\^\$\|<>&"']/g, "_")
        }, _readFiles: function (t) {
            this.reader = new FileReader;
            var a, r = this, n = r.$element, o = r.$preview, l = r.reader, s = r.$previewContainer,
                d = r.$previewStatus, c = r.msgLoading, p = r.msgProgress, u = r.previewInitId, f = t.length,
                m = r.fileTypeSettings, g = r.filestack.length, v = r.allowedFileTypes, h = v ? v.length : 0,
                w = r.allowedFileExtensions, _ = i.isEmpty(w) ? "" : w.join(", "),
                b = r.maxFilePreviewSize && parseFloat(r.maxFilePreviewSize), C = o.length && (!b || isNaN(b)),
                y = function (i, n, o, l) {
                    var s = e.extend(!0, {}, r._getOutData({}, {}, t), {id: o, index: l}),
                        d = {id: o, index: l, file: n, files: t};
                    return r._previewDefault(n, o, !0), r.isUploadable && (r.addToStack(void 0), setTimeout(function () {
                        a(l + 1)
                    }, 100)), r._initFileActions(), r.removeFromPreviewOnError && e("#" + o).remove(), r.isUploadable ? r._showUploadError(i, s) : r._showError(i, d)
                };
            r.loadedImages = [], r.totalImagesCount = 0, e.each(t, function (e, i) {
                var t = r.fileTypeSettings.image;
                t && t(i.type) && r.totalImagesCount++
            }), a = function (e) {
                if (i.isEmpty(n.attr("multiple")) && (f = 1), e >= f)return r.isUploadable && r.filestack.length > 0 ? r._raise("filebatchselected", [r.getFileStack()]) : r._raise("filebatchselected", [t]), s.removeClass("file-thumb-loading"), void d.html("");
                var E, x, T, S, F, P, I, k, A, $, z = g + e, D = u + "-" + z, U = t[e],
                    j = U.name ? r.slug(U.name) : "", L = (U.size || 0) / 1e3, R = "", M = i.objUrl.createObjectURL(U),
                    O = 0, Z = "";
                if (h > 0)for (S = 0; h > S; S++)k = v[S], A = r.msgFileTypes[k] || k, Z += 0 === S ? A : ", " + A;
                if (j === !1)return void a(e + 1);
                if (0 === j.length)return F = r.msgInvalidFileName.replace("{name}", i.htmlEncode(U.name)), void(r.isError = y(F, U, D, e));
                if (i.isEmpty(w) || (R = new RegExp("\\.(" + w.join("|") + ")$", "i")), T = L.toFixed(2), r.maxFileSize > 0 && L > r.maxFileSize)return F = r.msgSizeTooLarge.replace("{name}", j).replace("{size}", T).replace("{maxSize}", r.maxFileSize), void(r.isError = y(F, U, D, e));
                if (null !== r.minFileSize && L <= i.getNum(r.minFileSize))return F = r.msgSizeTooSmall.replace("{name}", j).replace("{size}", T).replace("{minSize}", r.minFileSize), void(r.isError = y(F, U, D, e));
                if (!i.isEmpty(v) && i.isArray(v)) {
                    for (S = 0; S < v.length; S += 1)P = v[S], $ = m[P], O += $ && "function" == typeof $ && $(U.type, U.name) ? 1 : 0;
                    if (0 === O)return F = r.msgInvalidFileType.replace("{name}", j).replace("{types}", Z), void(r.isError = y(F, U, D, e))
                }
                return 0 !== O || i.isEmpty(w) || !i.isArray(w) || i.isEmpty(R) || (I = i.compare(j, R), O += i.isEmpty(I) ? 0 : I.length, 0 !== O) ? r.showPreview ? !C && L > b ? (r.addToStack(U), s.addClass("file-thumb-loading"), r._previewDefault(U, D), r._initFileActions(), r._updateFileDetails(f), void a(e + 1)) : (o.length && void 0 !== FileReader ? (d.html(c.replace("{index}", e + 1).replace("{files}", f)), s.addClass("file-thumb-loading"), l.onerror = function (e) {
                    r._errorHandler(e, j)
                }, l.onload = function (i) {
                    r._previewFile(e, U, i, D, M), r._initFileActions()
                }, l.onloadend = function () {
                    F = p.replace("{index}", e + 1).replace("{files}", f).replace("{percent}", 50).replace("{name}", j), setTimeout(function () {
                        d.html(F), r._updateFileDetails(f), a(e + 1)
                    }, 100), r._raise("fileloaded", [U, D, e, l])
                }, l.onprogress = function (i) {
                    if (i.lengthComputable) {
                        var t = i.loaded / i.total * 100, a = Math.ceil(t);
                        F = p.replace("{index}", e + 1).replace("{files}", f).replace("{percent}", a).replace("{name}", j), setTimeout(function () {
                            d.html(F)
                        }, 100)
                    }
                }, E = m.text, x = m.image, E(U.type, j) ? l.readAsText(U, r.textEncoding) : x(U.type, j) ? l.readAsDataURL(U) : l.readAsArrayBuffer(U)) : (r._previewDefault(U, D), setTimeout(function () {
                    a(e + 1), r._updateFileDetails(f)
                }, 100), r._raise("fileloaded", [U, D, e, l])), void r.addToStack(U)) : (r.addToStack(U), setTimeout(function () {
                    a(e + 1)
                }, 100), void r._raise("fileloaded", [U, D, e, l])) : (F = r.msgInvalidFileExtension.replace("{name}", j).replace("{extensions}", _), void(r.isError = y(F, U, D, e)))
            }, a(0), r._updateFileDetails(f, !1)
        }, _updateFileDetails: function (e) {
            var t = this, a = t.$element, r = t.getFileStack(),
                n = i.isIE(9) && i.findFileName(a.val()) || a[0].files[0] && a[0].files[0].name || r.length && r[0].name || "",
                o = t.slug(n), l = t.isUploadable ? r.length : e, s = t.previewCache.count() + l,
                d = l > 1 ? t._getMsgSelected(s) : o;
            t.isError ? (t.$previewContainer.removeClass("file-thumb-loading"), t.$previewStatus.html(""), t.$captionContainer.find(".kv-caption-icon").hide()) : t._showFileIcon(), t._setCaption(d, t.isError), t.$container.removeClass("file-input-new file-input-ajax-new"), 1 === arguments.length && t._raise("fileselect", [e, o]), t.previewCache.count() && t._initPreviewActions()
        }, _setThumbStatus: function (e, i) {
            var t = this;
            if (t.showPreview) {
                var a = "indicator" + i, r = a + "Title", n = "file-preview-" + i.toLowerCase(),
                    o = e.find(".file-upload-indicator"), l = t.fileActionSettings;
                e.removeClass("file-preview-success file-preview-error file-preview-loading"), "Error" === i && e.find(".kv-file-upload").attr("disabled", !0), "Success" === i && (e.find(".file-drag-handle").remove(), o.css("margin-left", 0)), o.html(l[a]), o.attr("title", l[r]), e.addClass(n)
            }
        }, _setProgressCancelled: function () {
            var e = this;
            e._setProgress(101, e.$progress, e.msgCancelled)
        }, _setProgress: function (e, t, a) {
            var r, n, o = this, l = Math.min(e, 100), s = o.progressUploadThreshold,
                d = 100 >= e ? o.progressTemplate : o.progressCompleteTemplate,
                c = 100 > l ? o.progressTemplate : a ? o.progressErrorTemplate : d;
            t = t || o.$progress, i.isEmpty(c) || (s && l > s && 100 >= e ? r = c.replace(/\{percent}/g, s).replace(/\{status}/g, o.msgUploadThreshold) : (n = e > 100 ? o.msgUploadEnd : l + "%", r = c.replace(/\{percent}/g, l).replace(/\{status}/g, n)), t.html(r), a && t.find('[role="progressbar"]').html(a))
        }, _setFileDropZoneTitle: function () {
            var e, t = this, a = t.$container.find(".file-drop-zone"), r = t.dropZoneTitle;
            t.isClickable && (e = i.isEmpty(t.$element.attr("multiple")) ? t.fileSingle : t.filePlural, r += t.dropZoneClickTitle.replace("{files}", e)), a.find("." + t.dropZoneTitleClass).remove(), t.isUploadable && t.showPreview && 0 !== a.length && !(t.getFileStack().length > 0) && t.dropZoneEnabled && (0 === a.find(i.FRAMES).length && i.isEmpty(t.defaultPreviewContent) && a.prepend('<div class="' + t.dropZoneTitleClass + '">' + r + "</div>"), t.$container.removeClass("file-input-new"), i.addCss(t.$container, "file-input-ajax-new"))
        }, _setAsyncUploadStatus: function (i, t, a) {
            var r = this, n = 0;
            r._setProgress(t, e("#" + i).find(".file-thumb-progress")), r.uploadStatus[i] = t, e.each(r.uploadStatus, function (e, i) {
                n += i
            }), r._setProgress(Math.floor(n / a))
        }, _validateMinCount: function () {
            var e = this, i = e.isUploadable ? e.getFileStack().length : e.$element.get(0).files.length;
            return e.validateInitialCount && e.minFileCount > 0 && e._getFileCount(i - 1) < e.minFileCount ? (e._noFilesError({}), !1) : !0
        }, _getFileCount: function (e) {
            var i = this, t = 0;
            return i.validateInitialCount && !i.overwriteInitial && (t = i.previewCache.count(), e += t), e
        }, _getFileName: function (e) {
            return e && e.name ? this.slug(e.name) : void 0
        }, _getFileNames: function (e) {
            var i = this;
            return i.filenames.filter(function (i) {
                return e ? void 0 !== i : void 0 !== i && null !== i
            })
        }, _setPreviewError: function (e, i, t) {
            var a = this;
            void 0 !== i && a.updateStack(i, t), a.removeFromPreviewOnError ? e.remove() : a._setThumbStatus(e, "Error")
        }, _checkDimensions: function (e, t, a, r, n, o, l) {
            var s, d, c, p, u = this, f = "Small" === t ? "min" : "max", m = u[f + "Image" + o];
            !i.isEmpty(m) && a.length && (c = a[0], d = "Width" === o ? c.naturalWidth || c.width : c.naturalHeight || c.height, p = "Small" === t ? d >= m : m >= d, p || (s = u["msgImage" + o + t].replace("{name}", n).replace("{size}", m), u._showUploadError(s, l), u._setPreviewError(r, e, null)))
        }, _validateImage: function (e, i, t) {
            var a, r, n, o = this, l = o.$preview, s = l.find("#" + e), d = s.attr("data-fileindex"), c = s.find("img");
            i = i || "Untitled", c.length && o._handler(c, "load", function () {
                r = s.width(), n = l.width(), r > n && (c.css("width", "100%"), s.css("width", "97%")), a = {
                    ind: d,
                    id: e
                }, o._checkDimensions(d, "Small", c, s, i, "Width", a), o._checkDimensions(d, "Small", c, s, i, "Height", a), o.resizeImage || (o._checkDimensions(d, "Large", c, s, i, "Width", a), o._checkDimensions(d, "Large", c, s, i, "Height", a)), o._raise("fileimageloaded", [e]), o.loadedImages.push({
                    ind: d,
                    img: c,
                    thumb: s,
                    pid: e,
                    typ: t
                }), o._validateAllImages()
            })
        }, _validateAllImages: function () {
            var e, i = this, t = {val: 0}, a = i.loadedImages.length;
            if (a === i.totalImagesCount && (i._raise("fileimagesloaded"), i.resizeImage))for (e = 0; e < i.loadedImages.length; e++)i._getResizedImage(i.loadedImages[e], t, a)
        }, _getResizedImage: function (i, t, a) {
            var r, n, o, l, s = this, d = e(i.img)[0], c = d.naturalWidth, p = d.naturalHeight, u = 1,
                f = s.maxImageWidth || c, m = s.maxImageHeight || p, g = !(!c || !p), v = s.imageCanvas,
                h = s.imageCanvasContext, w = i.typ, _ = i.pid, b = i.ind, C = i.thumb;
            if (o = function (e, i, t) {
                    s.isUploadable ? s._showUploadError(e, i, t) : s._showError(e, i, t), s._setPreviewError(C, b)
                }, (!s.filestack[b] || !g || f >= c && m >= p) && (g && s.filestack[b] && s._raise("fileimageresized", [_, b]), t.val++, t.val === a && s._raise("fileimagesresized"), !g))return void o(s.msgImageResizeError, {
                id: _,
                index: b
            }, "fileimageresizeerror");
            w = w || s.resizeDefaultImageType, r = c > f, n = p > m, u = "width" === s.resizePreference ? r ? f / c : n ? m / p : 1 : n ? m / p : r ? f / c : 1, s._resetCanvas(), c *= u, p *= u, v.width = c, v.height = p;
            try {
                h.drawImage(d, 0, 0, c, p), v.toBlob(function (e) {
                    s.filestack[b] = e, s._raise("fileimageresized", [_, b]), t.val++, t.val === a && s._raise("fileimagesresized", [void 0, void 0]), e instanceof Blob || o(s.msgImageResizeError, {
                        id: _,
                        index: b
                    }, "fileimageresizeerror")
                }, w, s.resizeQuality)
            } catch (y) {
                t.val++, t.val === a && s._raise("fileimagesresized", [void 0, void 0]), l = s.msgImageResizeException.replace("{errors}", y.message), o(l, {
                    id: _,
                    index: b
                }, "fileimageresizeexception")
            }
        }, _initBrowse: function (e) {
            var i = this;
            i.showBrowse ? (i.$btnFile = e.find(".btn-file"), i.$btnFile.append(i.$element)) : i.$element.hide()
        }, _initCaption: function () {
            var e = this, t = e.initialCaption || "";
            return e.overwriteInitial || i.isEmpty(t) ? (e.$caption.html(""), !1) : (e._setCaption(t), !0)
        }, _setCaption: function (t, a) {
            var r, n, o, l, s = this, d = s.getFileStack();
            if (s.$caption.length) {
                if (a) r = e("<div>" + s.msgValidationError + "</div>").text(), o = d.length, l = o ? 1 === o && d[0] ? s._getFileNames()[0] : s._getMsgSelected(o) : s._getMsgSelected(s.msgNo), n = '<span class="' + s.msgValidationErrorClass + '">' + s.msgValidationErrorIcon + (i.isEmpty(t) ? l : t) + "</span>"; else {
                    if (i.isEmpty(t))return;
                    r = e("<div>" + t + "</div>").text(), n = s._getLayoutTemplate("fileIcon") + r
                }
                s.$caption.html(n), s.$caption.attr("title", r), s.$captionContainer.find(".file-caption-ellipsis").attr("title", r)
            }
        }, _createContainer: function () {
            var i = this,
                t = e(document.createElement("div")).attr({"class": "file-input file-input-new"}).html(i._renderMain());
            return i.$element.before(t), i._initBrowse(t), i.theme && t.addClass("theme-" + i.theme), t
        }, _refreshContainer: function () {
            var e = this, i = e.$container;
            i.before(e.$element), i.html(e._renderMain()), e._initBrowse(i)
        }, _renderMain: function () {
            var e = this, i = e.isUploadable && e.dropZoneEnabled ? " file-drop-zone" : "file-drop-disabled",
                t = e.showClose ? e._getLayoutTemplate("close") : "",
                a = e.showPreview ? e._getLayoutTemplate("preview").replace(/\{class}/g, e.previewClass).replace(/\{dropClass}/g, i) : "",
                r = e.isDisabled ? e.captionClass + " file-caption-disabled" : e.captionClass,
                n = e.captionTemplate.replace(/\{class}/g, r + " kv-fileinput-caption");
            return e.mainTemplate.replace(/\{class}/g, e.mainClass + (!e.showBrowse && e.showCaption ? " no-browse" : "")).replace(/\{preview}/g, a).replace(/\{close}/g, t).replace(/\{caption}/g, n).replace(/\{upload}/g, e._renderButton("upload")).replace(/\{remove}/g, e._renderButton("remove")).replace(/\{cancel}/g, e._renderButton("cancel")).replace(/\{browse}/g, e._renderButton("browse"))
        }, _renderButton: function (e) {
            var t = this, a = t._getLayoutTemplate("btnDefault"), r = t[e + "Class"], n = t[e + "Title"],
                o = t[e + "Icon"], l = t[e + "Label"], s = t.isDisabled ? " disabled" : "", d = "button";
            switch (e) {
                case"remove":
                    if (!t.showRemove)return "";
                    break;
                case"cancel":
                    if (!t.showCancel)return "";
                    r += " hide";
                    break;
                case"upload":
                    if (!t.showUpload)return "";
                    t.isUploadable && !t.isDisabled ? a = t._getLayoutTemplate("btnLink").replace("{href}", t.uploadUrl) : d = "submit";
                    break;
                case"browse":
                    if (!t.showBrowse)return "";
                    a = t._getLayoutTemplate("btnBrowse");
                    break;
                default:
                    return ""
            }
            return r += "browse" === e ? " btn-file" : " fileinput-" + e + " fileinput-" + e + "-button", i.isEmpty(l) || (l = ' <span class="' + t.buttonLabelClass + '">' + l + "</span>"), a.replace("{type}", d).replace("{css}", r).replace("{title}", n).replace("{status}", s).replace("{icon}", o).replace("{label}", l)
        }, _renderThumbProgress: function () {
            var e = this;
            return '<div class="file-thumb-progress hide">' + e.progressTemplate.replace(/\{percent}/g, "0").replace(/\{status}/g, e.msgUploadBegin) + "</div>"
        }, _renderFileFooter: function (e, t, a, r) {
            var n, o = this, l = o.fileActionSettings, s = l.showRemove, d = l.showDrag, c = l.showUpload,
                p = l.showZoom, u = o._getLayoutTemplate("footer"), f = r ? l.indicatorError : l.indicatorNew,
                m = r ? l.indicatorErrorTitle : l.indicatorNewTitle;
            return t = o._getSize(t), n = o.isUploadable ? u.replace(/\{actions}/g, o._renderFileActions(c, s, p, d, !1, !1, !1)).replace(/\{caption}/g, e).replace(/\{size}/g, t).replace(/\{width}/g, a).replace(/\{progress}/g, o._renderThumbProgress()).replace(/\{indicator}/g, f).replace(/\{indicatorTitle}/g, m) : u.replace(/\{actions}/g, o._renderFileActions(!1, !1, p, d, !1, !1, !1)).replace(/\{caption}/g, e).replace(/\{size}/g, t).replace(/\{width}/g, a).replace(/\{progress}/g, "").replace(/\{indicator}/g, f).replace(/\{indicatorTitle}/g, m),
                n = i.replaceTags(n, o.previewThumbTags)
        }, _renderFileActions: function (e, i, t, a, r, n, o, l) {
            if (!(e || i || t || a))return "";
            var s, d = this, c = n === !1 ? "" : ' data-url="' + n + '"', p = o === !1 ? "" : ' data-key="' + o + '"',
                u = "", f = "", m = "", g = "", v = d._getLayoutTemplate("actions"), h = d.fileActionSettings,
                w = d.otherActionButtons.replace(/\{dataKey}/g, p), _ = r ? h.removeClass + " disabled" : h.removeClass;
            return i && (u = d._getLayoutTemplate("actionDelete").replace(/\{removeClass}/g, _).replace(/\{removeIcon}/g, h.removeIcon).replace(/\{removeTitle}/g, h.removeTitle).replace(/\{dataUrl}/g, c).replace(/\{dataKey}/g, p)), e && (f = d._getLayoutTemplate("actionUpload").replace(/\{uploadClass}/g, h.uploadClass).replace(/\{uploadIcon}/g, h.uploadIcon).replace(/\{uploadTitle}/g, h.uploadTitle)), t && (m = d._getLayoutTemplate("actionZoom").replace(/\{zoomClass}/g, h.zoomClass).replace(/\{zoomIcon}/g, h.zoomIcon).replace(/\{zoomTitle}/g, h.zoomTitle)), a && l && (s = "drag-handle-init " + h.dragClass, g = d._getLayoutTemplate("actionDrag").replace(/\{dragClass}/g, s).replace(/\{dragTitle}/g, h.dragTitle).replace(/\{dragIcon}/g, h.dragIcon)), v.replace(/\{delete}/g, u).replace(/\{upload}/g, f).replace(/\{zoom}/g, m).replace(/\{drag}/g, g).replace(/\{other}/g, w)
        }, _browse: function (e) {
            var i = this;
            i._raise("filebrowse"), e && e.isDefaultPrevented() || (i.isError && !i.isUploadable && i.clear(), i.$captionContainer.focus())
        }, _change: function (t) {
            var a = this, r = a.$element;
            if (!a.isUploadable && i.isEmpty(r.val()) && a.fileInputCleared)return void(a.fileInputCleared = !1);
            a.fileInputCleared = !1;
            var n, o, l, s, d, c, p = arguments.length > 1, u = a.isUploadable, f = 0,
                m = p ? t.originalEvent.dataTransfer.files : r.get(0).files, g = a.filestack.length,
                v = i.isEmpty(r.attr("multiple")), h = v && g > 0, w = 0, _ = function (i, t, r, n) {
                    var o = e.extend(!0, {}, a._getOutData({}, {}, m), {id: r, index: n}),
                        l = {id: r, index: n, file: t, files: m};
                    return a.isUploadable ? a._showUploadError(i, o) : a._showError(i, l)
                };
            if (a.reader = null, a._resetUpload(), a._hideFileIcon(), a.isUploadable && a.$container.find(".file-drop-zone ." + a.dropZoneTitleClass).remove(), p)for (n = []; m[f];)s = m[f], s.type || s.size % 4096 !== 0 ? n.push(s) : w++, f++; else n = void 0 === t.target.files ? t.target && t.target.value ? [{name: t.target.value.replace(/^.+\\/, "")}] : [] : t.target.files;
            if (i.isEmpty(n) || 0 === n.length)return u || a.clear(), a._showFolderError(w), void a._raise("fileselectnone");
            if (a._resetErrors(), c = n.length, l = a._getFileCount(a.isUploadable ? a.getFileStack().length + c : c), a.maxFileCount > 0 && l > a.maxFileCount) {
                if (!a.autoReplace || c > a.maxFileCount)return d = a.autoReplace && c > a.maxFileCount ? c : l, o = a.msgFilesTooMany.replace("{m}", a.maxFileCount).replace("{n}", d), a.isError = _(o, null, null, null), a.$captionContainer.find(".kv-caption-icon").hide(), a._setCaption("", !0), void a.$container.removeClass("file-input-new file-input-ajax-new");
                l > a.maxFileCount && a._resetPreviewThumbs(u)
            } else!u || h ? (a._resetPreviewThumbs(!1), h && a.clearStack()) : !u || 0 !== g || a.previewCache.count() && !a.overwriteInitial || a._resetPreviewThumbs(!0);
            a.isPreviewable ? a._readFiles(n) : a._updateFileDetails(1), a._showFolderError(w)
        }, _abort: function (i) {
            var t, a = this;
            return a.ajaxAborted && "object" == typeof a.ajaxAborted && void 0 !== a.ajaxAborted.message ? (t = e.extend(!0, {}, a._getOutData(), i), t.abortData = a.ajaxAborted.data || {}, t.abortMessage = a.ajaxAborted.message, a.cancel(), a._setProgress(101, a.$progress, a.msgCancelled), a._showUploadError(a.ajaxAborted.message, t, "filecustomerror"), !0) : !1
        }, _resetFileStack: function () {
            var i = this, t = 0, a = [], r = [];
            i._getThumbs().each(function () {
                var n = e(this), o = n.attr("data-fileindex"), l = i.filestack[o];
                -1 !== o && (void 0 !== l ? (a[t] = l, r[t] = i._getFileName(l), n.attr({
                    id: i.previewInitId + "-" + t,
                    "data-fileindex": t
                }), t++) : n.attr({"data-fileindex": "-1"}))
            }), i.filestack = a, i.filenames = r
        }, clearStack: function () {
            var e = this;
            return e.filestack = [], e.filenames = [], e.$element
        }, updateStack: function (e, i) {
            var t = this;
            return t.filestack[e] = i, t.filenames[e] = t._getFileName(i), t.$element
        }, addToStack: function (e) {
            var i = this;
            return i.filestack.push(e), i.filenames.push(i._getFileName(e)), i.$element
        }, getFileStack: function (e) {
            var i = this;
            return i.filestack.filter(function (i) {
                return e ? void 0 !== i : void 0 !== i && null !== i
            })
        }, getFilesCount: function () {
            var e = this, i = e.isUploadable ? e.getFileStack().length : e.$element.get(0).files.length;
            return e._getFileCount(i)
        }, lock: function () {
            var e = this;
            return e._resetErrors(), e.disable(), e.showRemove && i.addCss(e.$container.find(".fileinput-remove"), "hide"), e.showCancel && e.$container.find(".fileinput-cancel").removeClass("hide"), e._raise("filelock", [e.filestack, e._getExtraData()]), e.$element
        }, unlock: function (e) {
            var t = this;
            return void 0 === e && (e = !0), t.enable(), t.showCancel && i.addCss(t.$container.find(".fileinput-cancel"), "hide"), t.showRemove && t.$container.find(".fileinput-remove").removeClass("hide"), e && t._resetFileStack(), t._raise("fileunlock", [t.filestack, t._getExtraData()]), t.$element
        }, cancel: function () {
            var i, t = this, a = t.ajaxRequests, r = a.length;
            if (r > 0)for (i = 0; r > i; i += 1)t.cancelling = !0, a[i].abort();
            return t._setProgressCancelled(), t._getThumbs().each(function () {
                var i = e(this), a = i.attr("data-fileindex");
                i.removeClass("file-uploading"), void 0 !== t.filestack[a] && (i.find(".kv-file-upload").removeClass("disabled").removeAttr("disabled"), i.find(".kv-file-remove").removeClass("disabled").removeAttr("disabled")), t.unlock()
            }), t.$element
        }, clear: function () {
            var t, a = this;
            if (a._raise("fileclear"))return a.$btnUpload.removeAttr("disabled"), a._getThumbs().find("video,audio,img").each(function () {
                i.cleanMemory(e(this))
            }), a._resetUpload(), a.clearStack(), a._clearFileInput(), a._resetErrors(!0), a._hasInitialPreview() ? (a._showFileIcon(), a._resetPreview(), a._initPreviewActions(), a.$container.removeClass("file-input-new")) : (a._getThumbs().each(function () {
                a._clearObjects(e(this))
            }), a.isUploadable && (a.previewCache.data = {}), a.$preview.html(""), t = !a.overwriteInitial && a.initialCaption.length > 0 ? a.initialCaption : "", a.$caption.html(t), a.$caption.attr("title", ""), i.addCss(a.$container, "file-input-new"), a._validateDefaultPreview()), 0 === a.$container.find(i.FRAMES).length && (a._initCaption() || a.$captionContainer.find(".kv-caption-icon").hide()), a._hideFileIcon(), a._raise("filecleared"), a.$captionContainer.focus(), a._setFileDropZoneTitle(), a.$element
        }, reset: function () {
            var e = this;
            if (e._raise("filereset"))return e._resetPreview(), e.$container.find(".fileinput-filename").text(""), i.addCss(e.$container, "file-input-new"), (e.$preview.find(i.FRAMES).length || e.isUploadable && e.dropZoneEnabled) && e.$container.removeClass("file-input-new"), e._setFileDropZoneTitle(), e.clearStack(), e.formdata = {}, e.$element
        }, disable: function () {
            var e = this;
            return e.isDisabled = !0, e._raise("filedisabled"), e.$element.attr("disabled", "disabled"), e.$container.find(".kv-fileinput-caption").addClass("file-caption-disabled"), e.$container.find(".btn-file, .fileinput-remove, .fileinput-upload, .file-preview-frame button").attr("disabled", !0), e._initDragDrop(), e.$element
        }, enable: function () {
            var e = this;
            return e.isDisabled = !1, e._raise("fileenabled"), e.$element.removeAttr("disabled"), e.$container.find(".kv-fileinput-caption").removeClass("file-caption-disabled"), e.$container.find(".btn-file, .fileinput-remove, .fileinput-upload, .file-preview-frame button").removeAttr("disabled"), e._initDragDrop(), e.$element
        }, upload: function () {
            var t, a, r, n = this, o = n.getFileStack().length, l = {}, s = !e.isEmptyObject(n._getExtraData());
            if (n.isUploadable && !n.isDisabled) {
                if (n.minFileCount > 0 && n._getFileCount(o) < n.minFileCount)return void n._noFilesError(l);
                if (n._resetUpload(), 0 === o && !s)return void n._showUploadError(n.msgUploadEmpty);
                if (n.$progress.removeClass("hide"), n.uploadCount = 0, n.uploadStatus = {}, n.uploadLog = [], n.lock(), n._setProgress(2), 0 === o && s)return void n._uploadExtraOnly();
                if (r = n.filestack.length, n.hasInitData = !1, !n.uploadAsync)return n._uploadBatch(), n.$element;
                for (a = n._getOutData(), n._raise("filebatchpreupload", [a]), n.fileBatchCompleted = !1, n.uploadCache = {
                    content: [],
                    config: [],
                    tags: [],
                    append: !0
                }, n.uploadAsyncCount = n.getFileStack().length, t = 0; r > t; t++)n.uploadCache.content[t] = null, n.uploadCache.config[t] = null, n.uploadCache.tags[t] = null;
                for (n.$preview.find(".file-preview-initial").removeClass(i.SORT_CSS), n._initSortable(), n.cacheInitialPreview = n.getPreview(), t = 0; r > t; t++)void 0 !== n.filestack[t] && n._uploadSingle(t, n.filestack, !0)
            }
        }, destroy: function () {
            var i = this, t = i.$form, a = i.$container, r = i.$element, n = i.namespace;
            return e(document).off(n), e(window).off(n), t && t.length && t.off(n), r.insertBefore(a).off(n).removeData(), a.off().remove(), r
        }, refresh: function (i) {
            var t = this, a = t.$element;
            return i = i ? e.extend(!0, {}, t.options, i) : t.options, t.destroy(), a.fileinput(i), a.val() && a.trigger("change.fileinput"), a
        }, zoom: function (t) {
            var a = this, r = e("#" + t), n = a.$modal;
            return r.length ? (i.initModal(n), n.html(a._getModalContent()), a._setZoomContent(r), n.modal("show"), void a._initZoomButtons()) : void a._log('Cannot zoom to detailed preview! Invalid frame with id: "' + t + '".')
        }, getPreview: function () {
            var e = this;
            return {content: e.initialPreview, config: e.initialPreviewConfig, tags: e.initialPreviewThumbTags}
        }
    }, e.fn.fileinput = function (a) {
        if (i.hasFileAPISupport() || i.isIE(9)) {
            var r = Array.apply(null, arguments), n = [];
            switch (r.shift(), this.each(function () {
                var o, l = e(this), s = l.data("fileinput"), d = "object" == typeof a && a,
                    c = d.theme || l.data("theme"), p = {}, u = {},
                    f = d.language || l.data("language") || e.fn.fileinput.defaults.language || "en";
                s || (c && (u = e.fn.fileinputThemes[c] || {}), "en" === f || i.isEmpty(e.fn.fileinputLocales[f]) || (p = e.fn.fileinputLocales[f] || {}), o = e.extend(!0, {}, e.fn.fileinput.defaults, u, e.fn.fileinputLocales.en, p, d, l.data()), s = new t(this, o), l.data("fileinput", s)), "string" == typeof a && n.push(s[a].apply(s, r))
            }), n.length) {
                case 0:
                    return this;
                case 1:
                    return n[0];
                default:
                    return n
            }
        }
    }, e.fn.fileinput.defaults = {
        language: "en",
        showCaption: !0,
        showBrowse: !0,
        showPreview: !0,
        showRemove: !0,
        showUpload: !0,
        showCancel: !0,
        showClose: !0,
        showUploadedThumbs: !0,
        browseOnZoneClick: !1,
        autoReplace: !1,
        previewClass: "",
        captionClass: "",
        frameClass: "krajee-default",
        mainClass: "file-caption-main",
        mainTemplate: null,
        purifyHtml: !0,
        fileSizeGetter: null,
        initialCaption: "",
        initialPreview: [],
        initialPreviewDelimiter: "*$$*",
        initialPreviewAsData: !1,
        initialPreviewFileType: "image",
        initialPreviewConfig: [],
        initialPreviewThumbTags: [],
        previewThumbTags: {},
        initialPreviewShowDelete: !0,
        removeFromPreviewOnError: !1,
        deleteUrl: "",
        deleteExtraData: {},
        overwriteInitial: !0,
        previewZoomButtonIcons: {
            prev: '<i class="glyphicon glyphicon-triangle-left"></i>',
            next: '<i class="glyphicon glyphicon-triangle-right"></i>',
            toggleheader: '<i class="glyphicon glyphicon-resize-vertical"></i>',
            fullscreen: '<i class="glyphicon glyphicon-fullscreen"></i>',
            borderless: '<i class="glyphicon glyphicon-resize-full"></i>',
            close: '<i class="glyphicon glyphicon-remove"></i>'
        },
        previewZoomButtonClasses: {
            prev: "btn btn-navigate",
            next: "btn btn-navigate",
            toggleheader: "btn btn-default btn-header-toggle",
            fullscreen: "btn btn-default",
            borderless: "btn btn-default",
            close: "btn btn-default"
        },
        preferIconicPreview: !1,
        preferIconicZoomPreview: !1,
        allowedPreviewTypes: void 0,
        allowedPreviewMimeTypes: null,
        allowedFileTypes: null,
        allowedFileExtensions: null,
        defaultPreviewContent: null,
        customLayoutTags: {},
        customPreviewTags: {},
        previewFileIcon: '<i class="glyphicon glyphicon-file"></i>',
        previewFileIconClass: "file-other-icon",
        previewFileIconSettings: {},
        previewFileExtSettings: {},
        buttonLabelClass: "hidden-xs",
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>&nbsp;',
        browseClass: "btn btn-primary",
        removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
        removeClass: "btn btn-default",
        cancelIcon: '<i class="glyphicon glyphicon-ban-circle"></i>',
        cancelClass: "btn btn-default",
        uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
        uploadClass: "btn btn-default",
        uploadUrl: null,
        uploadAsync: !0,
        uploadExtraData: {},
        zoomModalHeight: 480,
        minImageWidth: null,
        minImageHeight: null,
        maxImageWidth: null,
        maxImageHeight: null,
        resizeImage: !1,
        resizePreference: "width",
        resizeQuality: .92,
        resizeDefaultImageType: "image/jpeg",
        minFileSize: 0,
        maxFileSize: 0,
        maxFilePreviewSize: 25600,
        minFileCount: 0,
        maxFileCount: 0,
        validateInitialCount: !1,
        msgValidationErrorClass: "text-danger",
        msgValidationErrorIcon: '<i class="glyphicon glyphicon-exclamation-sign"></i> ',
        msgErrorClass: "file-error-message",
        progressThumbClass: "progress-bar progress-bar-success progress-bar-striped active",
        progressClass: "progress-bar progress-bar-success progress-bar-striped active",
        progressCompleteClass: "progress-bar progress-bar-success",
        progressErrorClass: "progress-bar progress-bar-danger",
        progressUploadThreshold: 99,
        previewFileType: "image",
        elCaptionContainer: null,
        elCaptionText: null,
        elPreviewContainer: null,
        elPreviewImage: null,
        elPreviewStatus: null,
        elErrorContainer: null,
        errorCloseButton: '<span class="close kv-error-close">&times;</span>',
        slugCallback: null,
        dropZoneEnabled: !0,
        dropZoneTitleClass: "file-drop-zone-title",
        fileActionSettings: {},
        otherActionButtons: "",
        textEncoding: "UTF-8",
        ajaxSettings: {},
        ajaxDeleteSettings: {},
        showAjaxErrorDetails: !0
    }, e.fn.fileinputLocales.en = {
        fileSingle: "file",
        filePlural: "files",
        browseLabel: "Browse &hellip;",
        removeLabel: "Remove",
        removeTitle: "Clear selected files",
        cancelLabel: "Cancel",
        cancelTitle: "Abort ongoing upload",
        uploadLabel: "Upload",
        uploadTitle: "Upload selected files",
        msgNo: "No",
        msgNoFilesSelected: "No files selected",
        msgCancelled: "Cancelled",
        msgZoomModalHeading: "Detailed Preview",
        msgSizeTooSmall: 'File "{name}" (<b>{size} KB</b>) is too small and must be larger than <b>{minSize} KB</b>.',
        msgSizeTooLarge: 'File "{name}" (<b>{size} KB</b>) exceeds maximum allowed upload size of <b>{maxSize} KB</b>.',
        msgFilesTooLess: "You must select at least <b>{n}</b> {files} to upload.",
        msgFilesTooMany: "Number of files selected for upload <b>({n})</b> exceeds maximum allowed limit of <b>{m}</b>.",
        msgFileNotFound: 'File "{name}" not found!',
        msgFileSecured: 'Security restrictions prevent reading the file "{name}".',
        msgFileNotReadable: 'File "{name}" is not readable.',
        msgFilePreviewAborted: 'File preview aborted for "{name}".',
        msgFilePreviewError: 'An error occurred while reading the file "{name}".',
        msgInvalidFileName: 'Invalid or unsupported characters in file name "{name}".',
        msgInvalidFileType: 'Invalid type for file "{name}". Only "{types}" files are supported.',
        msgInvalidFileExtension: 'Invalid extension for file "{name}". Only "{extensions}" files are supported.',
        msgFileTypes: {
            image: "image",
            html: "HTML",
            text: "text",
            video: "video",
            audio: "audio",
            flash: "flash",
            pdf: "PDF",
            object: "object"
        },
        msgUploadAborted: "The file upload was aborted",
        msgUploadThreshold: "Processing...",
        msgUploadBegin: "Initializing...",
        msgUploadEnd: "Done",
        msgUploadEmpty: "No valid data available for upload.",
        msgValidationError: "Validation Error",
        msgLoading: "Loading file {index} of {files} &hellip;",
        msgProgress: "Loading file {index} of {files} - {name} - {percent}% completed.",
        msgSelected: "{n} {files} selected",
        msgFoldersNotAllowed: "Drag & drop files only! {n} folder(s) dropped were skipped.",
        msgImageWidthSmall: 'Width of image file "{name}" must be at least {size} px.',
        msgImageHeightSmall: 'Height of image file "{name}" must be at least {size} px.',
        msgImageWidthLarge: 'Width of image file "{name}" cannot exceed {size} px.',
        msgImageHeightLarge: 'Height of image file "{name}" cannot exceed {size} px.',
        msgImageResizeError: "Could not get the image dimensions to resize.",
        msgImageResizeException: "Error while resizing the image.<pre>{errors}</pre>",
        msgAjaxError: "Something went wrong with the {operation} operation. Please try again later!",
        msgAjaxProgressError: "{operation} failed",
        ajaxOperations: {
            deleteThumb: "file delete",
            uploadThumb: "file upload",
            uploadBatch: "batch file upload",
            uploadExtra: "form data upload"
        },
        dropZoneTitle: "Drag & drop files here &hellip;",
        dropZoneClickTitle: "<br>(or click to select {files})",
        previewZoomButtonTitles: {
            prev: "View previous file",
            next: "View next file",
            toggleheader: "Toggle header",
            fullscreen: "Toggle full screen",
            borderless: "Toggle borderless mode",
            close: "Close detailed preview"
        }
    }, e.fn.fileinput.Constructor = t, e(document).ready(function () {
        var i = e("input.file[type=file]");
        i.length && i.fileinput()
    })
});


/* =========================================================
 * bootstrap-datetimepicker.js
 * =========================================================
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Improvements by Sébastien Malot
 * Improvements by Yun Lai
 * Improved by Keenthemes for Bootstrap 3.0 Support
 * Project URL : http://www.malot.fr/bootstrap-datetimepicker
 */
!function (t) {
    function e() {
        return new Date(Date.UTC.apply(Date, arguments))
    }

    var i = function (e, i) {
        var a = this;
        this.element = t(e), this.language = i.language || this.element.data("date-language") || "en", this.language = this.language in n ? this.language : "en", this.isRTL = n[this.language].rtl || "rtl" == t("body").css("direction"), this.formatType = i.formatType || this.element.data("format-type") || "standard", this.format = s.parseFormat(i.format || this.element.data("date-format") || n[this.language].format || s.getDefaultFormat(this.formatType, "input"), this.formatType), this.isInline = !1, this.isVisible = !1, this.isInput = this.element.is("input"), this.component = this.element.is(".date") ? this.element.find(".date-set").parent() : !1, this.componentReset = this.element.is(".date") ? this.element.find(".date-reset").parent() : !1, this.hasInput = this.component && this.element.find("input").length, this.component && 0 === this.component.length && (this.component = !1), this.linkField = i.linkField || this.element.data("link-field") || !1, this.linkFormat = s.parseFormat(i.linkFormat || this.element.data("link-format") || s.getDefaultFormat(this.formatType, "link"), this.formatType), this.minuteStep = i.minuteStep || this.element.data("minute-step") || 5, this.pickerPosition = i.pickerPosition || this.element.data("picker-position") || "bottom-right", this.showMeridian = i.showMeridian || this.element.data("show-meridian") || !1, this.initialDate = i.initialDate || new Date, this._attachEvents(), this.formatViewType = "datetime", "formatViewType" in i ? this.formatViewType = i.formatViewType : "formatViewType" in this.element.data() && (this.formatViewType = this.element.data("formatViewType")), this.minView = 0, "minView" in i ? this.minView = i.minView : "minView" in this.element.data() && (this.minView = this.element.data("min-view")), this.minView = s.convertViewMode(this.minView), this.maxView = s.modes.length - 1, "maxView" in i ? this.maxView = i.maxView : "maxView" in this.element.data() && (this.maxView = this.element.data("max-view")), this.maxView = s.convertViewMode(this.maxView), this.wheelViewModeNavigation = !1, "wheelViewModeNavigation" in i ? this.wheelViewModeNavigation = i.wheelViewModeNavigation : "wheelViewModeNavigation" in this.element.data() && (this.wheelViewModeNavigation = this.element.data("view-mode-wheel-navigation")), this.wheelViewModeNavigationInverseDirection = !1, "wheelViewModeNavigationInverseDirection" in i ? this.wheelViewModeNavigationInverseDirection = i.wheelViewModeNavigationInverseDirection : "wheelViewModeNavigationInverseDirection" in this.element.data() && (this.wheelViewModeNavigationInverseDirection = this.element.data("view-mode-wheel-navigation-inverse-dir")), this.wheelViewModeNavigationDelay = 100, "wheelViewModeNavigationDelay" in i ? this.wheelViewModeNavigationDelay = i.wheelViewModeNavigationDelay : "wheelViewModeNavigationDelay" in this.element.data() && (this.wheelViewModeNavigationDelay = this.element.data("view-mode-wheel-navigation-delay")), this.startViewMode = 2, "startView" in i ? this.startViewMode = i.startView : "startView" in this.element.data() && (this.startViewMode = this.element.data("start-view")), this.startViewMode = s.convertViewMode(this.startViewMode), this.viewMode = this.startViewMode, this.viewSelect = this.minView, "viewSelect" in i ? this.viewSelect = i.viewSelect : "viewSelect" in this.element.data() && (this.viewSelect = this.element.data("view-select")), this.viewSelect = s.convertViewMode(this.viewSelect), this.forceParse = !0, "forceParse" in i ? this.forceParse = i.forceParse : "dateForceParse" in this.element.data() && (this.forceParse = this.element.data("date-force-parse")), this.picker = t(s.template).appendTo(this.isInline ? this.element : "body").on({
            click: t.proxy(this.click, this),
            mousedown: t.proxy(this.mousedown, this)
        }), this.wheelViewModeNavigation && (t.fn.mousewheel ? this.picker.on({mousewheel: t.proxy(this.mousewheel, this)}) : console.log("Mouse Wheel event is not supported. Please include the jQuery Mouse Wheel plugin before enabling this option")), this.isInline ? this.picker.addClass("datetimepicker-inline") : this.picker.addClass("datetimepicker-dropdown-" + this.pickerPosition + " dropdown-menu"), this.isRTL && (this.picker.addClass("datetimepicker-rtl"), this.picker.find(".prev i, .next i").toggleClass("fa-arrow-left fa-arrow-right")), t(document).on("mousedown", function (e) {
            0 === t(e.target).closest(".datetimepicker").length && a.hide()
        }), this.autoclose = !1, "autoclose" in i ? this.autoclose = i.autoclose : "dateAutoclose" in this.element.data() && (this.autoclose = this.element.data("date-autoclose")), this.keyboardNavigation = !0, "keyboardNavigation" in i ? this.keyboardNavigation = i.keyboardNavigation : "dateKeyboardNavigation" in this.element.data() && (this.keyboardNavigation = this.element.data("date-keyboard-navigation")), this.todayBtn = i.todayBtn || this.element.data("date-today-btn") || !1, this.todayHighlight = i.todayHighlight || this.element.data("date-today-highlight") || !1, this.weekStart = (i.weekStart || this.element.data("date-weekstart") || n[this.language].weekStart || 0) % 7, this.weekEnd = (this.weekStart + 6) % 7, this.startDate = -1 / 0, this.endDate = 1 / 0, this.daysOfWeekDisabled = [], this.setStartDate(i.startDate || this.element.data("date-startdate")), this.setEndDate(i.endDate || this.element.data("date-enddate")), this.setDaysOfWeekDisabled(i.daysOfWeekDisabled || this.element.data("date-days-of-week-disabled")), this.fillDow(), this.fillMonths(), this.update(), this.showMode(), this.isInline && this.show()
    };
    i.prototype = {
        constructor: i, _events: [], _attachEvents: function () {
            this._detachEvents(), this.isInput ? this._events = [[this.element, {
                focus: t.proxy(this.show, this),
                keyup: t.proxy(this.update, this),
                keydown: t.proxy(this.keydown, this)
            }]] : this.component && this.hasInput ? (this._events = [[this.element.find("input"), {
                focus: t.proxy(this.show, this),
                keyup: t.proxy(this.update, this),
                keydown: t.proxy(this.keydown, this)
            }], [this.component, {click: t.proxy(this.show, this)}]], this.componentReset && this._events.push([this.componentReset, {click: t.proxy(this.reset, this)}])) : this.element.is("div") ? this.isInline = !0 : this._events = [[this.element, {click: t.proxy(this.show, this)}]];
            for (var e, i, n = 0; n < this._events.length; n++)e = this._events[n][0], i = this._events[n][1], e.on(i)
        }, _detachEvents: function () {
            for (var t, e, i = 0; i < this._events.length; i++)t = this._events[i][0], e = this._events[i][1], t.off(e);
            this._events = []
        }, show: function (e) {
            this.picker.show(), this.height = this.component ? this.component.outerHeight() : this.element.outerHeight(), this.forceParse && this.update(), this.place(), t(window).on("resize", t.proxy(this.place, this)), e && (e.stopPropagation(), e.preventDefault()), this.isVisible = !0, this.element.trigger({
                type: "show",
                date: this.date
            })
        }, hide: function () {
            this.isVisible && (this.isInline || (this.picker.hide(), t(window).off("resize", this.place), this.viewMode = this.startViewMode, this.showMode(), this.isInput || t(document).off("mousedown", this.hide), this.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this.isVisible = !1, this.element.trigger({
                type: "hide",
                date: this.date
            })))
        }, remove: function () {
            this._detachEvents(), this.picker.remove(), delete this.picker, delete this.element.data().datetimepicker
        }, getDate: function () {
            var t = this.getUTCDate();
            return new Date(t.getTime() + 6e4 * t.getTimezoneOffset())
        }, getUTCDate: function () {
            return this.date
        }, setDate: function (t) {
            this.setUTCDate(new Date(t.getTime() - 6e4 * t.getTimezoneOffset()))
        }, setUTCDate: function (t) {
            t >= this.startDate && t <= this.endDate ? (this.date = t, this.setValue(), this.viewDate = this.date, this.fill()) : this.element.trigger({
                type: "outOfRange",
                date: t,
                startDate: this.startDate,
                endDate: this.endDate
            })
        }, setFormat: function (t) {
            this.format = s.parseFormat(t, this.formatType);
            var e;
            this.isInput ? e = this.element : this.component && (e = this.element.find("input")), e && e.val() && this.setValue()
        }, setValue: function () {
            var e = this.getFormattedDate();
            this.isInput ? this.element.val(e) : (this.component && this.element.find("input").val(e), this.element.data("date", e)), this.linkField && t("#" + this.linkField).val(this.getFormattedDate(this.linkFormat))
        }, getFormattedDate: function (t) {
            return void 0 == t && (t = this.format), s.formatDate(this.date, t, this.language, this.formatType)
        }, setStartDate: function (t) {
            this.startDate = t || -1 / 0, this.startDate !== -1 / 0 && (this.startDate = s.parseDate(this.startDate, this.format, this.language, this.formatType)), this.update(), this.updateNavArrows()
        }, setEndDate: function (t) {
            this.endDate = t || 1 / 0, 1 / 0 !== this.endDate && (this.endDate = s.parseDate(this.endDate, this.format, this.language, this.formatType)), this.update(), this.updateNavArrows()
        }, setDaysOfWeekDisabled: function (e) {
            this.daysOfWeekDisabled = e || [], t.isArray(this.daysOfWeekDisabled) || (this.daysOfWeekDisabled = this.daysOfWeekDisabled.split(/,\s*/)), this.daysOfWeekDisabled = t.map(this.daysOfWeekDisabled, function (t) {
                return parseInt(t, 10)
            }), this.update(), this.updateNavArrows()
        }, place: function () {
            if (!this.isInline) {
                var e, i, n, s = parseInt(this.element.parents().filter(function () {
                        return "auto" != t(this).css("z-index")
                    }).first().css("z-index")) + 10;
                this.component ? (e = this.component.offset(), n = e.left, ("bottom-left" == this.pickerPosition || "top-left" == this.pickerPosition) && (n += this.component.outerWidth() - this.picker.outerWidth())) : (e = this.element.offset(), n = e.left), i = "top-left" == this.pickerPosition || "top-right" == this.pickerPosition ? e.top - this.picker.outerHeight() : e.top + this.height, this.picker.css({
                    top: i,
                    left: n,
                    zIndex: s
                })
            }
        }, update: function () {
            var t, e = !1;
            arguments && arguments.length && ("string" == typeof arguments[0] || arguments[0] instanceof Date) ? (t = arguments[0], e = !0) : t = this.element.data("date") || (this.isInput ? this.element.val() : this.element.find("input").val()) || this.initialDate, t || (t = new Date, e = !1), this.date = s.parseDate(t, this.format, this.language, this.formatType), e && this.setValue(), this.viewDate = this.date < this.startDate ? new Date(this.startDate) : this.date > this.endDate ? new Date(this.endDate) : new Date(this.date), this.fill()
        }, fillDow: function () {
            for (var t = this.weekStart, e = "<tr>"; t < this.weekStart + 7;)e += '<th class="dow">' + n[this.language].daysMin[t++ % 7] + "</th>";
            e += "</tr>", this.picker.find(".datetimepicker-days thead").append(e)
        }, fillMonths: function () {
            for (var t = "", e = 0; 12 > e;)t += '<span class="month">' + n[this.language].monthsShort[e++] + "</span>";
            this.picker.find(".datetimepicker-months td").html(t)
        }, fill: function () {
            if (null != this.date && null != this.viewDate) {
                var i = new Date(this.viewDate), a = i.getUTCFullYear(), o = i.getUTCMonth(), r = i.getUTCDate(),
                    l = i.getUTCHours(), c = i.getUTCMinutes(),
                    d = this.startDate !== -1 / 0 ? this.startDate.getUTCFullYear() : -1 / 0,
                    h = this.startDate !== -1 / 0 ? this.startDate.getUTCMonth() : -1 / 0,
                    u = 1 / 0 !== this.endDate ? this.endDate.getUTCFullYear() : 1 / 0,
                    p = 1 / 0 !== this.endDate ? this.endDate.getUTCMonth() : 1 / 0,
                    f = new e(this.date.getUTCFullYear(), this.date.getUTCMonth(), this.date.getUTCDate()).valueOf(),
                    g = new Date;
                if (this.picker.find(".datetimepicker-days thead th:eq(1)").text(n[this.language].months[o] + " " + a), "time" == this.formatViewType) {
                    var v = l % 12 ? l % 12 : 12, m = (10 > v ? "0" : "") + v, b = (10 > c ? "0" : "") + c,
                        w = n[this.language].meridiem[12 > l ? 0 : 1];
                    this.picker.find(".datetimepicker-hours thead th:eq(1)").text(m + ":" + b + " " + w.toUpperCase()), this.picker.find(".datetimepicker-minutes thead th:eq(1)").text(m + ":" + b + " " + w.toUpperCase())
                } else this.picker.find(".datetimepicker-hours thead th:eq(1)").text(r + " " + n[this.language].months[o] + " " + a), this.picker.find(".datetimepicker-minutes thead th:eq(1)").text(r + " " + n[this.language].months[o] + " " + a);
                this.picker.find("tfoot th.today").text(n[this.language].today).toggle(this.todayBtn !== !1), this.updateNavArrows(), this.fillMonths();
                var y = e(a, o - 1, 28, 0, 0, 0, 0), x = s.getDaysInMonth(y.getUTCFullYear(), y.getUTCMonth());
                y.setUTCDate(x), y.setUTCDate(x - (y.getUTCDay() - this.weekStart + 7) % 7);
                var T = new Date(y);
                T.setUTCDate(T.getUTCDate() + 42), T = T.valueOf();
                for (var C, S = []; y.valueOf() < T;)y.getUTCDay() == this.weekStart && S.push("<tr>"), C = "", y.getUTCFullYear() < a || y.getUTCFullYear() == a && y.getUTCMonth() < o ? C += " old" : (y.getUTCFullYear() > a || y.getUTCFullYear() == a && y.getUTCMonth() > o) && (C += " new"), this.todayHighlight && y.getUTCFullYear() == g.getFullYear() && y.getUTCMonth() == g.getMonth() && y.getUTCDate() == g.getDate() && (C += " today"), y.valueOf() == f && (C += " active"), (y.valueOf() + 864e5 <= this.startDate || y.valueOf() > this.endDate || -1 !== t.inArray(y.getUTCDay(), this.daysOfWeekDisabled)) && (C += " disabled"), S.push('<td class="day' + C + '">' + y.getUTCDate() + "</td>"), y.getUTCDay() == this.weekEnd && S.push("</tr>"), y.setUTCDate(y.getUTCDate() + 1);
                this.picker.find(".datetimepicker-days tbody").empty().append(S.join("")), S = [];
                for (var k = "", _ = "", $ = "", D = 0; 24 > D; D++) {
                    var I = e(a, o, r, D);
                    C = "", I.valueOf() + 36e5 <= this.startDate || I.valueOf() > this.endDate ? C += " disabled" : l == D && (C += " active"), this.showMeridian && 2 == n[this.language].meridiem.length ? (_ = 12 > D ? n[this.language].meridiem[0] : n[this.language].meridiem[1], _ != $ && ("" != $ && S.push("</fieldset>"), S.push('<fieldset class="hour"><legend>' + _.toUpperCase() + "</legend>")), $ = _, k = D % 12 ? D % 12 : 12, S.push('<span class="hour' + C + " hour_" + (12 > D ? "am" : "pm") + '">' + k + "</span>"), 23 == D && S.push("</fieldset>")) : (k = D + ":00", S.push('<span class="hour' + C + '">' + k + "</span>"))
                }
                this.picker.find(".datetimepicker-hours td").html(S.join("")), S = [], k = "", _ = "", $ = "";
                for (var D = 0; 60 > D; D += this.minuteStep) {
                    var I = e(a, o, r, l, D, 0);
                    C = "", I.valueOf() < this.startDate || I.valueOf() > this.endDate ? C += " disabled" : Math.floor(c / this.minuteStep) == Math.floor(D / this.minuteStep) && (C += " active"), this.showMeridian && 2 == n[this.language].meridiem.length ? (_ = 12 > l ? n[this.language].meridiem[0] : n[this.language].meridiem[1], _ != $ && ("" != $ && S.push("</fieldset>"), S.push('<fieldset class="minute"><legend>' + _.toUpperCase() + "</legend>")), $ = _, k = l % 12 ? l % 12 : 12, S.push('<span class="minute' + C + '">' + k + ":" + (10 > D ? "0" + D : D) + "</span>"), 59 == D && S.push("</fieldset>")) : (k = D + ":00", S.push('<span class="minute' + C + '">' + l + ":" + (10 > D ? "0" + D : D) + "</span>"))
                }
                this.picker.find(".datetimepicker-minutes td").html(S.join(""));
                var M = this.date.getUTCFullYear(),
                    E = this.picker.find(".datetimepicker-months").find("th:eq(1)").text(a).end().find("span").removeClass("active");
                M == a && E.eq(this.date.getUTCMonth()).addClass("active"), (d > a || a > u) && E.addClass("disabled"), a == d && E.slice(0, h).addClass("disabled"), a == u && E.slice(p + 1).addClass("disabled"), S = "", a = 10 * parseInt(a / 10, 10);
                var A = this.picker.find(".datetimepicker-years").find("th:eq(1)").text(a + "-" + (a + 9)).end().find("td");
                a -= 1;
                for (var D = -1; 11 > D; D++)S += '<span class="year' + (-1 == D || 10 == D ? " old" : "") + (M == a ? " active" : "") + (d > a || a > u ? " disabled" : "") + '">' + a + "</span>", a += 1;
                A.html(S), this.place()
            }
        }, updateNavArrows: function () {
            var t = new Date(this.viewDate), e = t.getUTCFullYear(), i = t.getUTCMonth(), n = t.getUTCDate(),
                s = t.getUTCHours();
            switch (this.viewMode) {
                case 0:
                    this.startDate !== -1 / 0 && e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() && n <= this.startDate.getUTCDate() && s <= this.startDate.getUTCHours() ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), 1 / 0 !== this.endDate && e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() && n >= this.endDate.getUTCDate() && s >= this.endDate.getUTCHours() ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"});
                    break;
                case 1:
                    this.startDate !== -1 / 0 && e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() && n <= this.startDate.getUTCDate() ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), 1 / 0 !== this.endDate && e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() && n >= this.endDate.getUTCDate() ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"});
                    break;
                case 2:
                    this.startDate !== -1 / 0 && e <= this.startDate.getUTCFullYear() && i <= this.startDate.getUTCMonth() ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), 1 / 0 !== this.endDate && e >= this.endDate.getUTCFullYear() && i >= this.endDate.getUTCMonth() ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"});
                    break;
                case 3:
                case 4:
                    this.startDate !== -1 / 0 && e <= this.startDate.getUTCFullYear() ? this.picker.find(".prev").css({visibility: "hidden"}) : this.picker.find(".prev").css({visibility: "visible"}), 1 / 0 !== this.endDate && e >= this.endDate.getUTCFullYear() ? this.picker.find(".next").css({visibility: "hidden"}) : this.picker.find(".next").css({visibility: "visible"})
            }
        }, mousewheel: function (e) {
            if (e.preventDefault(), e.stopPropagation(), !this.wheelPause) {
                this.wheelPause = !0;
                var i = e.originalEvent, n = i.wheelDelta, s = n > 0 ? 1 : 0 === n ? 0 : -1;
                this.wheelViewModeNavigationInverseDirection && (s = -s), this.showMode(s), setTimeout(t.proxy(function () {
                    this.wheelPause = !1
                }, this), this.wheelViewModeNavigationDelay)
            }
        }, click: function (i) {
            i.stopPropagation(), i.preventDefault();
            var n = t(i.target).closest("span, td, th, legend");
            if (1 == n.length) {
                if (n.is(".disabled"))return this.element.trigger({
                    type: "outOfRange",
                    date: this.viewDate,
                    startDate: this.startDate,
                    endDate: this.endDate
                }), void 0;
                switch (n[0].nodeName.toLowerCase()) {
                    case"th":
                        switch (n[0].className) {
                            case"switch":
                                this.showMode(1);
                                break;
                            case"prev":
                            case"next":
                                var a = s.modes[this.viewMode].navStep * ("prev" == n[0].className ? -1 : 1);
                                switch (this.viewMode) {
                                    case 0:
                                        this.viewDate = this.moveHour(this.viewDate, a);
                                        break;
                                    case 1:
                                        this.viewDate = this.moveDate(this.viewDate, a);
                                        break;
                                    case 2:
                                        this.viewDate = this.moveMonth(this.viewDate, a);
                                        break;
                                    case 3:
                                    case 4:
                                        this.viewDate = this.moveYear(this.viewDate, a)
                                }
                                this.fill();
                                break;
                            case"today":
                                var o = new Date;
                                o = e(o.getFullYear(), o.getMonth(), o.getDate(), o.getHours(), o.getMinutes(), o.getSeconds(), 0), this.viewMode = this.startViewMode, this.showMode(0), this._setDate(o), this.fill(), this.autoclose && this.hide()
                        }
                        break;
                    case"span":
                        if (!n.is(".disabled")) {
                            var r = this.viewDate.getUTCFullYear(), l = this.viewDate.getUTCMonth(),
                                c = this.viewDate.getUTCDate(), d = this.viewDate.getUTCHours(),
                                h = this.viewDate.getUTCMinutes(), u = this.viewDate.getUTCSeconds();
                            if (n.is(".month") ? (this.viewDate.setUTCDate(1), l = n.parent().find("span").index(n), c = this.viewDate.getUTCDate(), this.viewDate.setUTCMonth(l), this.element.trigger({
                                    type: "changeMonth",
                                    date: this.viewDate
                                }), this.viewSelect >= 3 && this._setDate(e(r, l, c, d, h, u, 0))) : n.is(".year") ? (this.viewDate.setUTCDate(1), r = parseInt(n.text(), 10) || 0, this.viewDate.setUTCFullYear(r), this.element.trigger({
                                    type: "changeYear",
                                    date: this.viewDate
                                }), this.viewSelect >= 4 && this._setDate(e(r, l, c, d, h, u, 0))) : n.is(".hour") ? (d = parseInt(n.text(), 10) || 0, (n.hasClass("hour_am") || n.hasClass("hour_pm")) && (12 == d && n.hasClass("hour_am") ? d = 0 : 12 != d && n.hasClass("hour_pm") && (d += 12)), this.viewDate.setUTCHours(d), this.element.trigger({
                                    type: "changeHour",
                                    date: this.viewDate
                                }), this.viewSelect >= 1 && this._setDate(e(r, l, c, d, h, u, 0))) : n.is(".minute") && (h = parseInt(n.text().substr(n.text().indexOf(":") + 1), 10) || 0, this.viewDate.setUTCMinutes(h), this.element.trigger({
                                        type: "changeMinute",
                                        date: this.viewDate
                                    }), this.viewSelect >= 0 && this._setDate(e(r, l, c, d, h, u, 0))), 0 != this.viewMode) {
                                var p = this.viewMode;
                                this.showMode(-1), this.fill(), p == this.viewMode && this.autoclose && this.hide()
                            } else this.fill(), this.autoclose && this.hide()
                        }
                        break;
                    case"td":
                        if (n.is(".day") && !n.is(".disabled")) {
                            var c = parseInt(n.text(), 10) || 1, r = this.viewDate.getUTCFullYear(),
                                l = this.viewDate.getUTCMonth(), d = this.viewDate.getUTCHours(),
                                h = this.viewDate.getUTCMinutes(), u = this.viewDate.getUTCSeconds();
                            n.is(".old") ? 0 === l ? (l = 11, r -= 1) : l -= 1 : n.is(".new") && (11 == l ? (l = 0, r += 1) : l += 1), this.viewDate.setUTCFullYear(r), this.viewDate.setUTCMonth(l), this.viewDate.setUTCDate(c), this.element.trigger({
                                type: "changeDay",
                                date: this.viewDate
                            }), this.viewSelect >= 2 && this._setDate(e(r, l, c, d, h, u, 0))
                        }
                        var p = this.viewMode;
                        this.showMode(-1), this.fill(), p == this.viewMode && this.autoclose && this.hide()
                }
            }
        }, _setDate: function (t, e) {
            e && "date" != e || (this.date = t), e && "view" != e || (this.viewDate = t), this.fill(), this.setValue();
            var i;
            this.isInput ? i = this.element : this.component && (i = this.element.find("input")), i && (i.change(), this.autoclose && (!e || "date" == e)), this.element.trigger({
                type: "changeDate",
                date: this.date
            })
        }, moveMinute: function (t, e) {
            if (!e)return t;
            var i = new Date(t.valueOf());
            return i.setUTCMinutes(i.getUTCMinutes() + e * this.minuteStep), i
        }, moveHour: function (t, e) {
            if (!e)return t;
            var i = new Date(t.valueOf());
            return i.setUTCHours(i.getUTCHours() + e), i
        }, moveDate: function (t, e) {
            if (!e)return t;
            var i = new Date(t.valueOf());
            return i.setUTCDate(i.getUTCDate() + e), i
        }, moveMonth: function (t, e) {
            if (!e)return t;
            var i, n, s = new Date(t.valueOf()), a = s.getUTCDate(), o = s.getUTCMonth(), r = Math.abs(e);
            if (e = e > 0 ? 1 : -1, 1 == r) n = -1 == e ? function () {
                return s.getUTCMonth() == o
            } : function () {
                return s.getUTCMonth() != i
            }, i = o + e, s.setUTCMonth(i), (0 > i || i > 11) && (i = (i + 12) % 12); else {
                for (var l = 0; r > l; l++)s = this.moveMonth(s, e);
                i = s.getUTCMonth(), s.setUTCDate(a), n = function () {
                    return i != s.getUTCMonth()
                }
            }
            for (; n();)s.setUTCDate(--a), s.setUTCMonth(i);
            return s
        }, moveYear: function (t, e) {
            return this.moveMonth(t, 12 * e)
        }, dateWithinRange: function (t) {
            return t >= this.startDate && t <= this.endDate
        }, keydown: function (t) {
            if (this.picker.is(":not(:visible)"))return 27 == t.keyCode && this.show(), void 0;
            var e, i, n, s = !1;
            switch (t.keyCode) {
                case 27:
                    this.hide(), t.preventDefault();
                    break;
                case 37:
                case 39:
                    if (!this.keyboardNavigation)break;
                    e = 37 == t.keyCode ? -1 : 1, viewMode = this.viewMode, t.ctrlKey ? viewMode += 2 : t.shiftKey && (viewMode += 1), 4 == viewMode ? (i = this.moveYear(this.date, e), n = this.moveYear(this.viewDate, e)) : 3 == viewMode ? (i = this.moveMonth(this.date, e), n = this.moveMonth(this.viewDate, e)) : 2 == viewMode ? (i = this.moveDate(this.date, e), n = this.moveDate(this.viewDate, e)) : 1 == viewMode ? (i = this.moveHour(this.date, e), n = this.moveHour(this.viewDate, e)) : 0 == viewMode && (i = this.moveMinute(this.date, e), n = this.moveMinute(this.viewDate, e)), this.dateWithinRange(i) && (this.date = i, this.viewDate = n, this.setValue(), this.update(), t.preventDefault(), s = !0);
                    break;
                case 38:
                case 40:
                    if (!this.keyboardNavigation)break;
                    e = 38 == t.keyCode ? -1 : 1, viewMode = this.viewMode, t.ctrlKey ? viewMode += 2 : t.shiftKey && (viewMode += 1), 4 == viewMode ? (i = this.moveYear(this.date, e), n = this.moveYear(this.viewDate, e)) : 3 == viewMode ? (i = this.moveMonth(this.date, e), n = this.moveMonth(this.viewDate, e)) : 2 == viewMode ? (i = this.moveDate(this.date, 7 * e), n = this.moveDate(this.viewDate, 7 * e)) : 1 == viewMode ? this.showMeridian ? (i = this.moveHour(this.date, 6 * e), n = this.moveHour(this.viewDate, 6 * e)) : (i = this.moveHour(this.date, 4 * e), n = this.moveHour(this.viewDate, 4 * e)) : 0 == viewMode && (i = this.moveMinute(this.date, 4 * e), n = this.moveMinute(this.viewDate, 4 * e)), this.dateWithinRange(i) && (this.date = i, this.viewDate = n, this.setValue(), this.update(), t.preventDefault(), s = !0);
                    break;
                case 13:
                    if (0 != this.viewMode) {
                        var a = this.viewMode;
                        this.showMode(-1), this.fill(), a == this.viewMode && this.autoclose && this.hide()
                    } else this.fill(), this.autoclose && this.hide();
                    t.preventDefault();
                    break;
                case 9:
                    this.hide()
            }
            if (s) {
                var o;
                this.isInput ? o = this.element : this.component && (o = this.element.find("input")), o && o.change(), this.element.trigger({
                    type: "changeDate",
                    date: this.date
                })
            }
        }, showMode: function (t) {
            if (t) {
                var e = Math.max(0, Math.min(s.modes.length - 1, this.viewMode + t));
                e >= this.minView && e <= this.maxView && (this.element.trigger({
                    type: "changeMode",
                    date: this.viewDate,
                    oldViewMode: this.viewMode,
                    newViewMode: e
                }), this.viewMode = e)
            }
            this.picker.find(">div").hide().filter(".datetimepicker-" + s.modes[this.viewMode].clsName).css("display", "block"), this.updateNavArrows()
        }, reset: function () {
            this._setDate(null, "date")
        }
    }, t.fn.datetimepicker = function (e) {
        var n = Array.apply(null, arguments);
        return n.shift(), this.each(function () {
            var s = t(this), a = s.data("datetimepicker"), o = "object" == typeof e && e;
            a || s.data("datetimepicker", a = new i(this, t.extend({}, t.fn.datetimepicker.defaults, o))), "string" == typeof e && "function" == typeof a[e] && a[e].apply(a, n)
        })
    }, t.fn.datetimepicker.defaults = {}, t.fn.datetimepicker.Constructor = i;
    var n = t.fn.datetimepicker.dates = {
        en: {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            meridiem: ["am", "pm"],
            suffix: ["st", "nd", "rd", "th"],
            today: "Today"
        }
    }, s = {
        modes: [{clsName: "minutes", navFnc: "Hours", navStep: 1}, {
            clsName: "hours",
            navFnc: "Date",
            navStep: 1
        }, {clsName: "days", navFnc: "Month", navStep: 1}, {
            clsName: "months",
            navFnc: "FullYear",
            navStep: 1
        }, {clsName: "years", navFnc: "FullYear", navStep: 10}],
        isLeapYear: function (t) {
            return 0 === t % 4 && 0 !== t % 100 || 0 === t % 400
        },
        getDaysInMonth: function (t, e) {
            return [31, s.isLeapYear(t) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][e]
        },
        getDefaultFormat: function (t, e) {
            if ("standard" == t)return "input" == e ? "yyyy-mm-dd hh:ii" : "yyyy-mm-dd hh:ii:ss";
            if ("php" == t)return "input" == e ? "Y-m-d H:i" : "Y-m-d H:i:s";
            throw new Error("Invalid format type.")
        },
        validParts: function (t) {
            if ("standard" == t)return /hh?|HH?|p|P|ii?|ss?|dd?|DD?|mm?|MM?|yy(?:yy)?/g;
            if ("php" == t)return /[dDjlNwzFmMnStyYaABgGhHis]/g;
            throw new Error("Invalid format type.")
        },
        nonpunctuation: /[^ -\/:-@\[-`{-~\t\n\rTZ]+/g,
        parseFormat: function (t, e) {
            var i = t.replace(this.validParts(e), "\0").split("\0"), n = t.match(this.validParts(e));
            if (!i || !i.length || !n || 0 == n.length)throw new Error("Invalid date format.");
            return {separators: i, parts: n}
        },
        parseDate: function (s, a, o, r) {
            if (s instanceof Date) {
                var l = new Date(s.valueOf() - 6e4 * s.getTimezoneOffset());
                return l.setMilliseconds(0), l
            }
            if (/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(s) && (a = this.parseFormat("yyyy-mm-dd", r)), /^\d{4}\-\d{1,2}\-\d{1,2}[T ]\d{1,2}\:\d{1,2}$/.test(s) && (a = this.parseFormat("yyyy-mm-dd hh:ii", r)), /^\d{4}\-\d{1,2}\-\d{1,2}[T ]\d{1,2}\:\d{1,2}\:\d{1,2}[Z]{0,1}$/.test(s) && (a = this.parseFormat("yyyy-mm-dd hh:ii:ss", r)), /^[-+]\d+[dmwy]([\s,]+[-+]\d+[dmwy])*$/.test(s)) {
                var c, d, h = /([-+]\d+)([dmwy])/, u = s.match(/([-+]\d+)([dmwy])/g);
                s = new Date;
                for (var p = 0; p < u.length; p++)switch (c = h.exec(u[p]), d = parseInt(c[1]), c[2]) {
                    case"d":
                        s.setUTCDate(s.getUTCDate() + d);
                        break;
                    case"m":
                        s = i.prototype.moveMonth.call(i.prototype, s, d);
                        break;
                    case"w":
                        s.setUTCDate(s.getUTCDate() + 7 * d);
                        break;
                    case"y":
                        s = i.prototype.moveYear.call(i.prototype, s, d)
                }
                return e(s.getUTCFullYear(), s.getUTCMonth(), s.getUTCDate(), s.getUTCHours(), s.getUTCMinutes(), s.getUTCSeconds(), 0)
            }
            var f, g, c, u = s && s.match(this.nonpunctuation) || [], s = new Date(0, 0, 0, 0, 0, 0, 0), v = {},
                m = ["hh", "h", "ii", "i", "ss", "s", "yyyy", "yy", "M", "MM", "m", "mm", "D", "DD", "d", "dd", "H", "HH", "p", "P"],
                b = {
                    hh: function (t, e) {
                        return t.setUTCHours(e)
                    }, h: function (t, e) {
                        return t.setUTCHours(e)
                    }, HH: function (t, e) {
                        return t.setUTCHours(12 == e ? 0 : e)
                    }, H: function (t, e) {
                        return t.setUTCHours(12 == e ? 0 : e)
                    }, ii: function (t, e) {
                        return t.setUTCMinutes(e)
                    }, i: function (t, e) {
                        return t.setUTCMinutes(e)
                    }, ss: function (t, e) {
                        return t.setUTCSeconds(e)
                    }, s: function (t, e) {
                        return t.setUTCSeconds(e)
                    }, yyyy: function (t, e) {
                        return t.setUTCFullYear(e)
                    }, yy: function (t, e) {
                        return t.setUTCFullYear(2e3 + e)
                    }, m: function (t, e) {
                        for (e -= 1; 0 > e;)e += 12;
                        for (e %= 12, t.setUTCMonth(e); t.getUTCMonth() != e;)t.setUTCDate(t.getUTCDate() - 1);
                        return t
                    }, d: function (t, e) {
                        return t.setUTCDate(e)
                    }, p: function (t, e) {
                        return t.setUTCHours(1 == e ? t.getUTCHours() + 12 : t.getUTCHours())
                    }
                };
            if (b.M = b.MM = b.mm = b.m, b.dd = b.d, b.P = b.p, s = e(s.getFullYear(), s.getMonth(), s.getDate(), s.getHours(), s.getMinutes(), s.getSeconds()), u.length == a.parts.length) {
                for (var p = 0, w = a.parts.length; w > p; p++) {
                    if (f = parseInt(u[p], 10), c = a.parts[p], isNaN(f))switch (c) {
                        case"MM":
                            g = t(n[o].months).filter(function () {
                                var t = this.slice(0, u[p].length), e = u[p].slice(0, t.length);
                                return t == e
                            }), f = t.inArray(g[0], n[o].months) + 1;
                            break;
                        case"M":
                            g = t(n[o].monthsShort).filter(function () {
                                var t = this.slice(0, u[p].length), e = u[p].slice(0, t.length);
                                return t == e
                            }), f = t.inArray(g[0], n[o].monthsShort) + 1;
                            break;
                        case"p":
                        case"P":
                            f = t.inArray(u[p].toLowerCase(), n[o].meridiem)
                    }
                    v[c] = f
                }
                for (var y, p = 0; p < m.length; p++)y = m[p], y in v && !isNaN(v[y]) && b[y](s, v[y])
            }
            return s
        },
        formatDate: function (e, i, a, o) {
            if (null == e)return "";
            var r;
            if ("standard" == o) r = {
                yy: e.getUTCFullYear().toString().substring(2),
                yyyy: e.getUTCFullYear(),
                m: e.getUTCMonth() + 1,
                M: n[a].monthsShort[e.getUTCMonth()],
                MM: n[a].months[e.getUTCMonth()],
                d: e.getUTCDate(),
                D: n[a].daysShort[e.getUTCDay()],
                DD: n[a].days[e.getUTCDay()],
                p: 2 == n[a].meridiem.length ? n[a].meridiem[e.getUTCHours() < 12 ? 0 : 1] : "",
                h: e.getUTCHours(),
                i: e.getUTCMinutes(),
                s: e.getUTCSeconds()
            }, r.H = 0 == r.h % 12 ? 12 : r.h % 12, r.HH = (r.H < 10 ? "0" : "") + r.H, r.P = r.p.toUpperCase(), r.hh = (r.h < 10 ? "0" : "") + r.h, r.ii = (r.i < 10 ? "0" : "") + r.i, r.ss = (r.s < 10 ? "0" : "") + r.s, r.dd = (r.d < 10 ? "0" : "") + r.d, r.mm = (r.m < 10 ? "0" : "") + r.m; else {
                if ("php" != o)throw new Error("Invalid format type.");
                r = {
                    y: e.getUTCFullYear().toString().substring(2),
                    Y: e.getUTCFullYear(),
                    F: n[a].months[e.getUTCMonth()],
                    M: n[a].monthsShort[e.getUTCMonth()],
                    n: e.getUTCMonth() + 1,
                    t: s.getDaysInMonth(e.getUTCFullYear(), e.getUTCMonth()),
                    j: e.getUTCDate(),
                    l: n[a].days[e.getUTCDay()],
                    D: n[a].daysShort[e.getUTCDay()],
                    w: e.getUTCDay(),
                    N: 0 == e.getUTCDay() ? 7 : e.getUTCDay(),
                    S: e.getUTCDate() % 10 <= n[a].suffix.length ? n[a].suffix[e.getUTCDate() % 10 - 1] : "",
                    a: 2 == n[a].meridiem.length ? n[a].meridiem[e.getUTCHours() < 12 ? 0 : 1] : "",
                    g: 0 == e.getUTCHours() % 12 ? 12 : e.getUTCHours() % 12,
                    G: e.getUTCHours(),
                    i: e.getUTCMinutes(),
                    s: e.getUTCSeconds()
                }, r.m = (r.n < 10 ? "0" : "") + r.n, r.d = (r.j < 10 ? "0" : "") + r.j, r.A = r.a.toString().toUpperCase(), r.h = (r.g < 10 ? "0" : "") + r.g, r.H = (r.G < 10 ? "0" : "") + r.G, r.i = (r.i < 10 ? "0" : "") + r.i, r.s = (r.s < 10 ? "0" : "") + r.s
            }
            for (var e = [], l = t.extend([], i.separators), c = 0, d = i.parts.length; d > c; c++)l.length && e.push(l.shift()), e.push(r[i.parts[c]]);
            return e.join("")
        },
        convertViewMode: function (t) {
            switch (t) {
                case 4:
                case"decade":
                    t = 4;
                    break;
                case 3:
                case"year":
                    t = 3;
                    break;
                case 2:
                case"month":
                    t = 2;
                    break;
                case 1:
                case"day":
                    t = 1;
                    break;
                case 0:
                case"hour":
                    t = 0
            }
            return t
        },
        headTemplate: '<thead><tr><th class="prev"><i class="fa fa-angle-left"/></th><th colspan="5" class="switch"></th><th class="next"><i class="fa fa-angle-right"/></th></tr></thead>',
        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
        footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr></tfoot>'
    };
    s.template = '<div class="datetimepicker"><div class="datetimepicker-minutes"><table class=" table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + "</table>" + "</div>" + '<div class="datetimepicker-hours">' + '<table class=" table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + "</table>" + "</div>" + '<div class="datetimepicker-days">' + '<table class=" table-condensed">' + s.headTemplate + "<tbody></tbody>" + s.footTemplate + "</table>" + "</div>" + '<div class="datetimepicker-months">' + '<table class="table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + "</table>" + "</div>" + '<div class="datetimepicker-years">' + '<table class="table-condensed">' + s.headTemplate + s.contTemplate + s.footTemplate + "</table>" + "</div>" + "</div>", t.fn.datetimepicker.DPGlobal = s, t.fn.datetimepicker.noConflict = function () {
        return t.fn.datetimepicker = old, this
    }, t(document).on("focus.datetimepicker.data-api click.datetimepicker.data-api", '[data-provide="datetimepicker"]', function (e) {
        var i = t(this);
        i.data("datetimepicker") || (e.preventDefault(), i.datetimepicker("show"))
    }), t(function () {
        t('[data-provide="datetimepicker-inline"]').datetimepicker()
    })
}(window.jQuery);


/*!
 * Timepicker Component for Twitter Bootstrap
 *
 * Copyright 2013 Joris de Wit and bootstrap-timepicker contributors
 *
 * Contributors https://github.com/jdewit/bootstrap-timepicker/graphs/contributors
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
!function (t, i, e) {
    "use strict";
    var s = function (i, e) {
        this.widget = "", this.$element = t(i), this.defaultTime = e.defaultTime, this.disableFocus = e.disableFocus, this.disableMousewheel = e.disableMousewheel, this.isOpen = e.isOpen, this.minuteStep = e.minuteStep, this.modalBackdrop = e.modalBackdrop, this.orientation = e.orientation, this.secondStep = e.secondStep, this.snapToStep = e.snapToStep, this.showInputs = e.showInputs, this.showMeridian = e.showMeridian, this.showSeconds = e.showSeconds, this.template = e.template, this.appendWidgetTo = e.appendWidgetTo, this.showWidgetOnAddonClick = e.showWidgetOnAddonClick, this.icons = e.icons, this.maxHours = e.maxHours, this.explicitMode = e.explicitMode, this.handleDocumentClick = function (t) {
            var i = t.data.scope;
            i.$element.parent().find(t.target).length || i.$widget.is(t.target) || i.$widget.find(t.target).length || i.hideWidget()
        }, this._init()
    };
    s.prototype = {
        constructor: s, _init: function () {
            var i = this;
            this.showWidgetOnAddonClick && this.$element.parent().hasClass("input-group") && this.$element.parent().hasClass("bootstrap-timepicker") ? (this.$element.parent(".input-group.bootstrap-timepicker").find(".input-group-addon").on({"click.timepicker": t.proxy(this.showWidget, this)}), this.$element.on({
                "focus.timepicker": t.proxy(this.highlightUnit, this),
                "click.timepicker": t.proxy(this.highlightUnit, this),
                "keydown.timepicker": t.proxy(this.elementKeydown, this),
                "blur.timepicker": t.proxy(this.blurElement, this),
                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
            })) : this.template ? this.$element.on({
                "focus.timepicker": t.proxy(this.showWidget, this),
                "click.timepicker": t.proxy(this.showWidget, this),
                "blur.timepicker": t.proxy(this.blurElement, this),
                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
            }) : this.$element.on({
                "focus.timepicker": t.proxy(this.highlightUnit, this),
                "click.timepicker": t.proxy(this.highlightUnit, this),
                "keydown.timepicker": t.proxy(this.elementKeydown, this),
                "blur.timepicker": t.proxy(this.blurElement, this),
                "mousewheel.timepicker DOMMouseScroll.timepicker": t.proxy(this.mousewheel, this)
            }), this.template !== !1 ? this.$widget = t(this.getTemplate()).on("click", t.proxy(this.widgetClick, this)) : this.$widget = !1, this.showInputs && this.$widget !== !1 && this.$widget.find("input").each(function () {
                t(this).on({
                    "click.timepicker": function () {
                        t(this).select()
                    }, "keydown.timepicker": t.proxy(i.widgetKeydown, i), "keyup.timepicker": t.proxy(i.widgetKeyup, i)
                })
            }), this.setDefaultTime(this.defaultTime)
        }, blurElement: function () {
            this.highlightedUnit = null, this.updateFromElementVal()
        }, clear: function () {
            this.hour = "", this.minute = "", this.second = "", this.meridian = "", this.$element.val("")
        }, decrementHour: function () {
            if (this.showMeridian)if (1 === this.hour) this.hour = 12; else {
                if (12 === this.hour)return this.hour--, this.toggleMeridian();
                if (0 === this.hour)return this.hour = 11, this.toggleMeridian();
                this.hour--
            } else this.hour <= 0 ? this.hour = this.maxHours - 1 : this.hour--
        }, decrementMinute: function (t) {
            var i;
            i = t ? this.minute - t : this.minute - this.minuteStep, 0 > i ? (this.decrementHour(), this.minute = i + 60) : this.minute = i
        }, decrementSecond: function () {
            var t = this.second - this.secondStep;
            0 > t ? (this.decrementMinute(!0), this.second = t + 60) : this.second = t
        }, elementKeydown: function (t) {
            switch (t.which) {
                case 9:
                    if (t.shiftKey) {
                        if ("hour" === this.highlightedUnit) {
                            this.hideWidget();
                            break
                        }
                        this.highlightPrevUnit()
                    } else {
                        if (this.showMeridian && "meridian" === this.highlightedUnit || this.showSeconds && "second" === this.highlightedUnit || !this.showMeridian && !this.showSeconds && "minute" === this.highlightedUnit) {
                            this.hideWidget();
                            break
                        }
                        this.highlightNextUnit()
                    }
                    t.preventDefault(), this.updateFromElementVal();
                    break;
                case 27:
                    this.updateFromElementVal();
                    break;
                case 37:
                    t.preventDefault(), this.highlightPrevUnit(), this.updateFromElementVal();
                    break;
                case 38:
                    switch (t.preventDefault(), this.highlightedUnit) {
                        case"hour":
                            this.incrementHour(), this.highlightHour();
                            break;
                        case"minute":
                            this.incrementMinute(), this.highlightMinute();
                            break;
                        case"second":
                            this.incrementSecond(), this.highlightSecond();
                            break;
                        case"meridian":
                            this.toggleMeridian(), this.highlightMeridian()
                    }
                    this.update();
                    break;
                case 39:
                    t.preventDefault(), this.highlightNextUnit(), this.updateFromElementVal();
                    break;
                case 40:
                    switch (t.preventDefault(), this.highlightedUnit) {
                        case"hour":
                            this.decrementHour(), this.highlightHour();
                            break;
                        case"minute":
                            this.decrementMinute(), this.highlightMinute();
                            break;
                        case"second":
                            this.decrementSecond(), this.highlightSecond();
                            break;
                        case"meridian":
                            this.toggleMeridian(), this.highlightMeridian()
                    }
                    this.update()
            }
        }, getCursorPosition: function () {
            var t = this.$element.get(0);
            if ("selectionStart" in t)return t.selectionStart;
            if (e.selection) {
                t.focus();
                var i = e.selection.createRange(), s = e.selection.createRange().text.length;
                return i.moveStart("character", -t.value.length), i.text.length - s
            }
        }, getTemplate: function () {
            var t, i, e, s, h, n;
            switch (this.showInputs ? (i = '<input type="text" class="bootstrap-timepicker-hour" maxlength="2"/>', e = '<input type="text" class="bootstrap-timepicker-minute" maxlength="2"/>', s = '<input type="text" class="bootstrap-timepicker-second" maxlength="2"/>', h = '<input type="text" class="bootstrap-timepicker-meridian" maxlength="2"/>') : (i = '<span class="bootstrap-timepicker-hour"></span>', e = '<span class="bootstrap-timepicker-minute"></span>', s = '<span class="bootstrap-timepicker-second"></span>', h = '<span class="bootstrap-timepicker-meridian"></span>'), n = '<table><tr><td><a href="#" data-action="incrementHour"><span class="' + this.icons.up + '"></span></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><span class="' + this.icons.up + '"></span></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><span class="' + this.icons.up + '"></span></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><span class="' + this.icons.up + '"></span></a></td>' : "") + "</tr><tr><td>" + i + '</td> <td class="separator">:</td><td>' + e + "</td> " + (this.showSeconds ? '<td class="separator">:</td><td>' + s + "</td>" : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td>' + h + "</td>" : "") + '</tr><tr><td><a href="#" data-action="decrementHour"><span class="' + this.icons.down + '"></span></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><span class="' + this.icons.down + '"></span></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><span class="' + this.icons.down + '"></span></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><span class="' + this.icons.down + '"></span></a></td>' : "") + "</tr></table>", this.template) {
                case"modal":
                    t = '<div class="bootstrap-timepicker-widget modal hide fade in" data-backdrop="' + (this.modalBackdrop ? "true" : "false") + '"><div class="modal-header"><a href="#" class="close" data-dismiss="modal">&times;</a><h3>Pick a Time</h3></div><div class="modal-content">' + n + '</div><div class="modal-footer"><a href="#" class="btn btn-primary" data-dismiss="modal">OK</a></div></div>';
                    break;
                case"dropdown":
                    t = '<div class="bootstrap-timepicker-widget dropdown-menu">' + n + "</div>"
            }
            return t
        }, getTime: function () {
            return "" === this.hour ? "" : this.hour + ":" + (1 === this.minute.toString().length ? "0" + this.minute : this.minute) + (this.showSeconds ? ":" + (1 === this.second.toString().length ? "0" + this.second : this.second) : "") + (this.showMeridian ? " " + this.meridian : "")
        }, hideWidget: function () {
            this.isOpen !== !1 && (this.$element.trigger({
                type: "hide.timepicker",
                time: {
                    value: this.getTime(),
                    hours: this.hour,
                    minutes: this.minute,
                    seconds: this.second,
                    meridian: this.meridian
                }
            }), "modal" === this.template && this.$widget.modal ? this.$widget.modal("hide") : this.$widget.removeClass("open"), t(e).off("mousedown.timepicker, touchend.timepicker", this.handleDocumentClick), this.isOpen = !1, this.$widget.detach())
        }, highlightUnit: function () {
            this.position = this.getCursorPosition(), this.position >= 0 && this.position <= 2 ? this.highlightHour() : this.position >= 3 && this.position <= 5 ? this.highlightMinute() : this.position >= 6 && this.position <= 8 ? this.showSeconds ? this.highlightSecond() : this.highlightMeridian() : this.position >= 9 && this.position <= 11 && this.highlightMeridian()
        }, highlightNextUnit: function () {
            switch (this.highlightedUnit) {
                case"hour":
                    this.highlightMinute();
                    break;
                case"minute":
                    this.showSeconds ? this.highlightSecond() : this.showMeridian ? this.highlightMeridian() : this.highlightHour();
                    break;
                case"second":
                    this.showMeridian ? this.highlightMeridian() : this.highlightHour();
                    break;
                case"meridian":
                    this.highlightHour()
            }
        }, highlightPrevUnit: function () {
            switch (this.highlightedUnit) {
                case"hour":
                    this.showMeridian ? this.highlightMeridian() : this.showSeconds ? this.highlightSecond() : this.highlightMinute();
                    break;
                case"minute":
                    this.highlightHour();
                    break;
                case"second":
                    this.highlightMinute();
                    break;
                case"meridian":
                    this.showSeconds ? this.highlightSecond() : this.highlightMinute()
            }
        }, highlightHour: function () {
            var t = this.$element.get(0), i = this;
            this.highlightedUnit = "hour", t.setSelectionRange && setTimeout(function () {
                i.hour < 10 ? t.setSelectionRange(0, 1) : t.setSelectionRange(0, 2)
            }, 0)
        }, highlightMinute: function () {
            var t = this.$element.get(0), i = this;
            this.highlightedUnit = "minute", t.setSelectionRange && setTimeout(function () {
                i.hour < 10 ? t.setSelectionRange(2, 4) : t.setSelectionRange(3, 5)
            }, 0)
        }, highlightSecond: function () {
            var t = this.$element.get(0), i = this;
            this.highlightedUnit = "second", t.setSelectionRange && setTimeout(function () {
                i.hour < 10 ? t.setSelectionRange(5, 7) : t.setSelectionRange(6, 8)
            }, 0)
        }, highlightMeridian: function () {
            var t = this.$element.get(0), i = this;
            this.highlightedUnit = "meridian", t.setSelectionRange && (this.showSeconds ? setTimeout(function () {
                i.hour < 10 ? t.setSelectionRange(8, 10) : t.setSelectionRange(9, 11)
            }, 0) : setTimeout(function () {
                i.hour < 10 ? t.setSelectionRange(5, 7) : t.setSelectionRange(6, 8)
            }, 0))
        }, incrementHour: function () {
            if (this.showMeridian) {
                if (11 === this.hour)return this.hour++, this.toggleMeridian();
                12 === this.hour && (this.hour = 0)
            }
            return this.hour === this.maxHours - 1 ? void(this.hour = 0) : void this.hour++
        }, incrementMinute: function (t) {
            var i;
            i = t ? this.minute + t : this.minute + this.minuteStep - this.minute % this.minuteStep, i > 59 ? (this.incrementHour(), this.minute = i - 60) : this.minute = i
        }, incrementSecond: function () {
            var t = this.second + this.secondStep - this.second % this.secondStep;
            t > 59 ? (this.incrementMinute(!0), this.second = t - 60) : this.second = t
        }, mousewheel: function (i) {
            if (!this.disableMousewheel) {
                i.preventDefault(), i.stopPropagation();
                var e = i.originalEvent.wheelDelta || -i.originalEvent.detail, s = null;
                switch ("mousewheel" === i.type ? s = -1 * i.originalEvent.wheelDelta : "DOMMouseScroll" === i.type && (s = 40 * i.originalEvent.detail), s && (i.preventDefault(), t(this).scrollTop(s + t(this).scrollTop())), this.highlightedUnit) {
                    case"minute":
                        e > 0 ? this.incrementMinute() : this.decrementMinute(), this.highlightMinute();
                        break;
                    case"second":
                        e > 0 ? this.incrementSecond() : this.decrementSecond(), this.highlightSecond();
                        break;
                    case"meridian":
                        this.toggleMeridian(), this.highlightMeridian();
                        break;
                    default:
                        e > 0 ? this.incrementHour() : this.decrementHour(), this.highlightHour()
                }
                return !1
            }
        }, changeToNearestStep: function (t, i) {
            return t % i === 0 ? t : Math.round(t % i / i) ? (t + (i - t % i)) % 60 : t - t % i
        }, place: function () {
            if (!this.isInline) {
                var e = this.$widget.outerWidth(), s = this.$widget.outerHeight(), h = 10, n = t(i).width(),
                    o = t(i).height(), a = t(i).scrollTop(), r = parseInt(this.$element.parents().filter(function () {
                            return "auto" !== t(this).css("z-index")
                        }).first().css("z-index"), 10) + 10,
                    d = this.component ? this.component.parent().offset() : this.$element.offset(),
                    c = this.component ? this.component.outerHeight(!0) : this.$element.outerHeight(!1),
                    l = this.component ? this.component.outerWidth(!0) : this.$element.outerWidth(!1), u = d.left,
                    p = d.top;
                this.$widget.removeClass("timepicker-orient-top timepicker-orient-bottom timepicker-orient-right timepicker-orient-left"), "auto" !== this.orientation.x ? (this.$widget.addClass("timepicker-orient-" + this.orientation.x), "right" === this.orientation.x && (u -= e - l)) : (this.$widget.addClass("timepicker-orient-left"), d.left < 0 ? u -= d.left - h : d.left + e > n && (u = n - e - h));
                var m, g, f = this.orientation.y;
                "auto" === f && (m = -a + d.top - s, g = a + o - (d.top + c + s), f = Math.max(m, g) === g ? "top" : "bottom"), this.$widget.addClass("timepicker-orient-" + f), "top" === f ? p += c : p -= s + parseInt(this.$widget.css("padding-top"), 10), this.$widget.css({
                    top: p,
                    left: u,
                    zIndex: r
                })
            }
        }, remove: function () {
            t("document").off(".timepicker"), this.$widget && this.$widget.remove(), delete this.$element.data().timepicker
        }, setDefaultTime: function (t) {
            if (this.$element.val()) this.updateFromElementVal(); else if ("current" === t) {
                var i = new Date, e = i.getHours(), s = i.getMinutes(), h = i.getSeconds(), n = "AM";
                0 !== h && (h = Math.ceil(i.getSeconds() / this.secondStep) * this.secondStep, 60 === h && (s += 1, h = 0)), 0 !== s && (s = Math.ceil(i.getMinutes() / this.minuteStep) * this.minuteStep, 60 === s && (e += 1, s = 0)), this.showMeridian && (0 === e ? e = 12 : e >= 12 ? (e > 12 && (e -= 12), n = "PM") : n = "AM"), this.hour = e, this.minute = s, this.second = h, this.meridian = n, this.update()
            } else t === !1 ? (this.hour = 0, this.minute = 0, this.second = 0, this.meridian = "AM") : this.setTime(t)
        }, setTime: function (t, i) {
            if (!t)return void this.clear();
            var e, s, h, n, o, a;
            if ("object" == typeof t && t.getMonth) h = t.getHours(), n = t.getMinutes(), o = t.getSeconds(), this.showMeridian && (a = "AM", h > 12 && (a = "PM", h %= 12), 12 === h && (a = "PM")); else {
                if (e = (/a/i.test(t) ? 1 : 0) + (/p/i.test(t) ? 2 : 0), e > 2)return void this.clear();
                if (s = t.replace(/[^0-9\:]/g, "").split(":"), h = s[0] ? s[0].toString() : s.toString(), this.explicitMode && h.length > 2 && h.length % 2 !== 0)return void this.clear();
                n = s[1] ? s[1].toString() : "", o = s[2] ? s[2].toString() : "", h.length > 4 && (o = h.slice(-2), h = h.slice(0, -2)), h.length > 2 && (n = h.slice(-2), h = h.slice(0, -2)), n.length > 2 && (o = n.slice(-2), n = n.slice(0, -2)), h = parseInt(h, 10), n = parseInt(n, 10), o = parseInt(o, 10), isNaN(h) && (h = 0), isNaN(n) && (n = 0), isNaN(o) && (o = 0), o > 59 && (o = 59), n > 59 && (n = 59), h >= this.maxHours && (h = this.maxHours - 1), this.showMeridian ? (h > 12 && (e = 2, h -= 12), e || (e = 1), 0 === h && (h = 12), a = 1 === e ? "AM" : "PM") : 12 > h && 2 === e ? h += 12 : h >= this.maxHours ? h = this.maxHours - 1 : (0 > h || 12 === h && 1 === e) && (h = 0)
            }
            this.hour = h, this.snapToStep ? (this.minute = this.changeToNearestStep(n, this.minuteStep), this.second = this.changeToNearestStep(o, this.secondStep)) : (this.minute = n, this.second = o), this.meridian = a, this.update(i)
        }, showWidget: function () {
            this.isOpen || this.$element.is(":disabled") || (this.$widget.appendTo(this.appendWidgetTo), t(e).on("mousedown.timepicker, touchend.timepicker", {scope: this}, this.handleDocumentClick), this.$element.trigger({
                type: "show.timepicker",
                time: {
                    value: this.getTime(),
                    hours: this.hour,
                    minutes: this.minute,
                    seconds: this.second,
                    meridian: this.meridian
                }
            }), this.place(), this.disableFocus && this.$element.blur(), "" === this.hour && (this.defaultTime ? this.setDefaultTime(this.defaultTime) : this.setTime("0:0:0")), "modal" === this.template && this.$widget.modal ? this.$widget.modal("show").on("hidden", t.proxy(this.hideWidget, this)) : this.isOpen === !1 && this.$widget.addClass("open"), this.isOpen = !0)
        }, toggleMeridian: function () {
            this.meridian = "AM" === this.meridian ? "PM" : "AM"
        }, update: function (t) {
            this.updateElement(), t || this.updateWidget(), this.$element.trigger({
                type: "changeTime.timepicker",
                time: {
                    value: this.getTime(),
                    hours: this.hour,
                    minutes: this.minute,
                    seconds: this.second,
                    meridian: this.meridian
                }
            })
        }, updateElement: function () {
            this.$element.val(this.getTime()).change()
        }, updateFromElementVal: function () {
            this.setTime(this.$element.val())
        }, updateWidget: function () {
            if (this.$widget !== !1) {
                var t = this.hour, i = 1 === this.minute.toString().length ? "0" + this.minute : this.minute,
                    e = 1 === this.second.toString().length ? "0" + this.second : this.second;
                this.showInputs ? (this.$widget.find("input.bootstrap-timepicker-hour").val(t), this.$widget.find("input.bootstrap-timepicker-minute").val(i), this.showSeconds && this.$widget.find("input.bootstrap-timepicker-second").val(e), this.showMeridian && this.$widget.find("input.bootstrap-timepicker-meridian").val(this.meridian)) : (this.$widget.find("span.bootstrap-timepicker-hour").text(t), this.$widget.find("span.bootstrap-timepicker-minute").text(i), this.showSeconds && this.$widget.find("span.bootstrap-timepicker-second").text(e), this.showMeridian && this.$widget.find("span.bootstrap-timepicker-meridian").text(this.meridian))
            }
        }, updateFromWidgetInputs: function () {
            if (this.$widget !== !1) {
                var t = this.$widget.find("input.bootstrap-timepicker-hour").val() + ":" + this.$widget.find("input.bootstrap-timepicker-minute").val() + (this.showSeconds ? ":" + this.$widget.find("input.bootstrap-timepicker-second").val() : "") + (this.showMeridian ? this.$widget.find("input.bootstrap-timepicker-meridian").val() : "");
                this.setTime(t, !0)
            }
        }, widgetClick: function (i) {
            i.stopPropagation(), i.preventDefault();
            var e = t(i.target), s = e.closest("a").data("action");
            s && this[s](), this.update(), e.is("input") && e.get(0).setSelectionRange(0, 2)
        }, widgetKeydown: function (i) {
            var e = t(i.target), s = e.attr("class").replace("bootstrap-timepicker-", "");
            switch (i.which) {
                case 9:
                    if (i.shiftKey) {
                        if ("hour" === s)return this.hideWidget()
                    } else if (this.showMeridian && "meridian" === s || this.showSeconds && "second" === s || !this.showMeridian && !this.showSeconds && "minute" === s)return this.hideWidget();
                    break;
                case 27:
                    this.hideWidget();
                    break;
                case 38:
                    switch (i.preventDefault(), s) {
                        case"hour":
                            this.incrementHour();
                            break;
                        case"minute":
                            this.incrementMinute();
                            break;
                        case"second":
                            this.incrementSecond();
                            break;
                        case"meridian":
                            this.toggleMeridian()
                    }
                    this.setTime(this.getTime()), e.get(0).setSelectionRange(0, 2);
                    break;
                case 40:
                    switch (i.preventDefault(), s) {
                        case"hour":
                            this.decrementHour();
                            break;
                        case"minute":
                            this.decrementMinute();
                            break;
                        case"second":
                            this.decrementSecond();
                            break;
                        case"meridian":
                            this.toggleMeridian()
                    }
                    this.setTime(this.getTime()), e.get(0).setSelectionRange(0, 2)
            }
        }, widgetKeyup: function (t) {
            (65 === t.which || 77 === t.which || 80 === t.which || 46 === t.which || 8 === t.which || t.which >= 48 && t.which <= 57 || t.which >= 96 && t.which <= 105) && this.updateFromWidgetInputs()
        }
    }, t.fn.timepicker = function (i) {
        var e = Array.apply(null, arguments);
        return e.shift(), this.each(function () {
            var h = t(this), n = h.data("timepicker"), o = "object" == typeof i && i;
            n || h.data("timepicker", n = new s(this, t.extend({}, t.fn.timepicker.defaults, o, t(this).data()))), "string" == typeof i && n[i].apply(n, e)
        })
    }, t.fn.timepicker.defaults = {
        defaultTime: "current",
        disableFocus: !1,
        disableMousewheel: !1,
        isOpen: !1,
        minuteStep: 15,
        modalBackdrop: !1,
        orientation: {x: "auto", y: "auto"},
        secondStep: 15,
        snapToStep: !1,
        showSeconds: !1,
        showInputs: !0,
        showMeridian: !0,
        template: "dropdown",
        appendWidgetTo: "body",
        showWidgetOnAddonClick: !0,
        icons: {up: "fa fa-angle-up", down: "fa fa-angle-down"},
        maxHours: 24,
        explicitMode: !1
    }, t.fn.timepicker.Constructor = s, t(e).on("focus.timepicker.data-api click.timepicker.data-api", '[data-provide="timepicker"]', function (i) {
        var e = t(this);
        e.data("timepicker") || (i.preventDefault(), e.timepicker())
    })
}(jQuery, window, document);