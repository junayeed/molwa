/****************************************************/
/****** Datatables managed **************************/
/****************************************************/
var TableDatatablesManaged = function () {
    var a = function () {
            var a = $("#table_list_view");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                bStateSave: !0,
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                pagingType: "bootstrap_full_number",
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }, {
                    className: "dt-right"
                }],
                order: [
                    [1, "asc"]
                ]
            }), jQuery("#table_list_view_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? ($(this).prop("checked", !0), $(this).parents("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).parents("tr").removeClass("active"))
                })
            }), a.on("change", "tbody tr .checkboxes", function () {
                $(this).parents("tr").toggleClass("active")
            })
        },
        b = function () {
            var a = $("#table_list_view_2");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                bStateSave: !1,
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                pagingType: "bootstrap_full_number",
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }, {
                    className: "dt-right"
                }],
                order: [
                    [1, "asc"]
                ],
                initComplete: function () {
                    this.api().column(1).every(function () {
                        var a = this,
                            b = $('<select class="form-control input-sm"><option value="">Select</option></select>').appendTo($(a.footer()).empty()).on("change", function () {
                                var b = $.fn.dataTable.util.escapeRegex($(this).val());
                                a.search(b ? "^" + b + "$" : "", !0, !1).draw()
                            });
                        a.data().unique().sort().each(function (a, c) {
                            b.append('<option value="' + a + '">' + a + "</option>")
                        })
                    })
                }
            }), jQuery("#table_list_view_2_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? ($(this).prop("checked", !0), $(this).parents("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).parents("tr").removeClass("active"))
                })
            }), a.on("change", "tbody tr .checkboxes", function () {
                $(this).parents("tr").toggleClass("active")
            })
        },
        c = function () {
            var a = $("#sample_2");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                bStateSave: !0,
                pagingType: "bootstrap_extended",
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }],
                order: [
                    [1, "asc"]
                ]
            }), jQuery("#sample_2_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? $(this).prop("checked", !0) : $(this).prop("checked", !1)
                })
            })
        },
        d = function () {
            var a = $("#sample_3");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                bStateSave: !0,
                lengthMenu: [
                    [6, 10, 15, 20, -1],
                    [6, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }],
                order: [
                    [1, "asc"]
                ]
            }), jQuery("#sample_3_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? $(this).prop("checked", !0) : $(this).prop("checked", !1)
                })
            })
        },
        e = function () {
            var a = $("#sample_4");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                bStateSave: !0,
                lengthMenu: [
                    [6, 10, 15, 20, -1],
                    [6, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }],
                order: [
                    [1, "asc"]
                ]
            }), jQuery("#sample_4_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? $(this).prop("checked", !0) : $(this).prop("checked", !1)
                })
            })
        },
        f = function () {
            var a = $("#sample_5");
            a.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    infoEmpty: "No records found",
                    infoFiltered: "(filtered1 from _MAX_ total records)",
                    lengthMenu: "Show _MENU_",
                    search: "Search:",
                    zeroRecords: "No matching records found",
                    paginate: {
                        previous: "Prev",
                        next: "Next",
                        last: "Last",
                        first: "First"
                    }
                },
                footerCallback: function (a, b, c, d, e) {
                    var f = this.api(),
                        g = function (a) {
                            return "string" == typeof a ? 1 * a.replace(/[\$,]/g, "") : "number" == typeof a ? a : 0
                        };
                    total = f.column(3).data().reduce(function (a, b) {
                        return g(a) + g(b)
                    }, 0), pageTotal = f.column(3, {
                        page: "current"
                    }).data().reduce(function (a, b) {
                        return g(a) + g(b)
                    }, 0), $(f.column(3).footer()).html("$" + pageTotal + " ( $" + total + " total)")
                },
                bStateSave: !0,
                lengthMenu: [
                    [6, 10, 15, 20, -1],
                    [6, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                columnDefs: [{
                    orderable: !1,
                    targets: [0]
                }, {
                    searchable: !1,
                    targets: [0]
                }],
                order: [
                    [1, "asc"]
                ]
            }), jQuery("#sample_5_wrapper"), a.find(".group-checkable").change(function () {
                var a = jQuery(this).attr("data-set"),
                    b = jQuery(this).is(":checked");
                jQuery(a).each(function () {
                    b ? $(this).prop("checked", !0) : $(this).prop("checked", !1)
                })
            })
        };
    return {
        init: function () {
            jQuery().dataTable && (a(), b(), c(), d(), e(), f())
        }
    }
}();
!1 === App.isAngularJsApp() && jQuery(document).ready(function () {
    TableDatatablesManaged.init()
});


/****************************************************/
/****** components-bootstrap-maxlength **************/
/****************************************************/
var ComponentsBootstrapMaxlength = function () {

    var handleBootstrapMaxlength = function () {
        // $('#maxlength_defaultconfig').maxlength({
        //     limitReachedClass: "label label-danger",
        // })

        // $('#maxlength_thresholdconfig').maxlength({
        //     limitReachedClass: "label label-danger",
        //     threshold: 20
        // });

        // $('#maxlength_alloptions').maxlength({
        //     alwaysShow: true,
        //     warningClass: "label label-success",
        //     limitReachedClass: "label label-danger",
        //     separator: ' out of ',
        //     preText: 'You typed ',
        //     postText: ' chars available.',
        //     validate: true
        // });

        // $('#maxlength_textarea').maxlength({
        //     limitReachedClass: "label label-danger",
        //     alwaysShow: true,
        //     // placement: 'bottom-left'
        // });

        // $('#maxlength_placement').maxlength({
        //     limitReachedClass: "label label-danger",
        //     alwaysShow: true,
        //     placement: App.isRTL() ? 'top-right' : 'top-left'
        // });
        // $('#sub1').maxlength({
        //     limitReachedClass: "label label-danger",
        //     alwaysShow: true,
        // });
        var calcMaxlengthHolder = document.getElementsByClassName("calc-maxlength");
        for (var i = 0; i < calcMaxlengthHolder.length; i++) {
            $(calcMaxlengthHolder[i]).maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            handleBootstrapMaxlength();
        }
    };

}();

jQuery(document).ready(function () {
    ComponentsBootstrapMaxlength.init();
});


/****************************************************/
/****** components-date-time-pickers ****************/
/****************************************************/
var ComponentsDateTimePickers = function () {

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: 'yyyy/mm/dd'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */

        // Workaround to fix datepicker position on window scroll
        $(document).scroll(function () {
            $('#form_modal2 .date-picker').datepicker('place'); //#modal is the id of the modal
        });
    }

    var handleToDatePickers = function () {

        if (jQuery().datepicker) {
            $('.to-date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: '-yyyy/mm/dd'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */

        // Workaround to fix datepicker position on window scroll
        $(document).scroll(function () {
            $('#form_modal2 .date-picker').datepicker('place'); //#modal is the id of the modal
        });
    }

    var handleFromDatePickers = function () {

        if (jQuery().datepicker) {
            $('.from-date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: 'd MM yyyy -'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */

        // Workaround to fix datepicker position on window scroll
        $(document).scroll(function () {
            $('#form_modal2 .date-picker').datepicker('place'); //#modal is the id of the modal
        });
    }

    var handleTimePickers = function () {

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5,
                defaultTime: false
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });

            // Workaround to fix timepicker position on window scroll
            $(document).scroll(function () {
                $('#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24').timepicker('place'); //#modal is the id of the modal
            });
        }
    }

    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        }

        $('#defaultrange').daterangepicker({
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'MM/DD/YYYY',
                separator: ' to ',
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                minDate: '01/01/2012',
                maxDate: '12/31/2018',
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
            }
        );

        $('#defaultrange_modal').daterangepicker({
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'MM/DD/YYYY',
                separator: ' to ',
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2018',
            },
            function (start, end) {
                $('#defaultrange_modal input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );

        // this is very important fix when daterangepicker is used in modal. in modal when daterange picker is opened and mouse clicked anywhere bootstrap modal removes the modal-open class from the body element.
        // so the below code will fix this issue.
        $('#defaultrange_modal').on('click', function () {
            if ($('#daterangepicker_modal').is(":visible") && $('body').hasClass("modal-open") == false) {
                $('body').addClass("modal-open");
            }
        });

        $('#reportrange').daterangepicker({
                opens: (App.isRTL() ? 'left' : 'right'),
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                //minDate: '01/01/2012',
                //maxDate: '12/31/2014',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                buttonClasses: ['btn'],
                applyClass: 'green',
                cancelClass: 'default',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Apply',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom Range',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    }

    var handleDatetimePicker = function () {

        if (!jQuery().datetimepicker) {
            return;
        }

        $(".form_datetime").datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            format: "yyyy/mm/dd - hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });

        $(".form_advance_datetime").datetimepicker({
            isRTL: App.isRTL(),
            format: "dd MM yyyy - hh:ii",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });

        $(".form_meridian_datetime").datetimepicker({
            isRTL: App.isRTL(),
            format: "dd MM yyyy - HH:ii P",
            showMeridian: true,
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            todayBtn: true
        });

        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal

        // Workaround to fix datetimepicker position on window scroll
        $(document).scroll(function () {
            $('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
        });
    }

    var handleClockfaceTimePickers = function () {

        if (!jQuery().clockface) {
            return;
        }

        $('.clockface_1').clockface();

        $('#clockface_2').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2').clockface('toggle');
        });

        $('#clockface_2_modal').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_modal_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2_modal').clockface('toggle');
        });

        $('.clockface_3').clockface({
            format: 'H:mm'
        }).clockface('show', '14:30');

        // Workaround to fix clockface position on window scroll
        $(document).scroll(function () {
            $('#form_modal5 .clockface_1, #form_modal5 #clockface_2_modal').clockface('place'); //#modal is the id of the modal
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDatePickers();
            handleToDatePickers();
            handleFromDatePickers();
            handleTimePickers();
            handleDatetimePicker();
            handleDateRangePickers();
            handleClockfaceTimePickers();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        ComponentsDateTimePickers.init();
    });
}


/****************************************************/
/****** form-input-mask *****************************/
/****************************************************/
var FormInputMask = function () {

    var handleInputMasks = function () {


        $(".mask-acct-tender").inputmask({
            "mask": "99.99.9999.999.99.999.99.9999"
            //placeholder: "" // remove underscores from the input mask
        }); //specifying options only
        $(".confiscated-item-quantity").inputmask({
            "mask": "9999999999",
            placeholder: ""
        });
        $(".num-2digit").inputmask({
            "mask": "99",
            placeholder: ""
        });
        $(".num-3digit").inputmask({
            "mask": "999",
            placeholder: ""
        });
        $(".num-4digit").inputmask({
            "mask": "9999",
            placeholder: ""
        });
        $(".num-5digit").inputmask({
            "mask": "99999",
            placeholder: ""
        });
        $(".num-6digit").inputmask({
            "mask": "999999",
            placeholder: ""
        });
        $(".num-7digit").inputmask({
            "mask": "9999999",
            placeholder: ""
        });
        $(".num-8digit").inputmask({
            "mask": "99999999",
            placeholder: ""
        });
        $(".num-9digit").inputmask({
            "mask": "999999999",
            placeholder: ""
        });
        $(".num-10digit").inputmask({
            "mask": "9999999999",
            placeholder: ""
        });
        $(".num-11digit").inputmask({
            "mask": "99999999999",
            placeholder: ""
        });
        $(".num-15digit").inputmask({
            "mask": "999999999999999",
            placeholder: ""
        });


        $("#mask_date").inputmask("d/m/y", {
            autoUnmask: true
        }); //direct mask        
        $("#mask_date1").inputmask("d/m/y", {
            "placeholder": "*"
        }); //change the placeholder
        $("#mask_date2").inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        }); //multi-char placeholder
        $("#mask_phone").inputmask("mask", {
            "mask": "(999) 999-9999"
        }); //specifying fn & options
        $("#mask_number").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        }); // ~ mask "9" or mask "99" or ... mask "9999999999"
        $("#mask_decimal").inputmask('decimal', {
            rightAlignNumerics: false
        }); //disables the right alignment of the decimal input
        $("#mask_currency").inputmask('€ 999.999.999,99', {
            numericInput: true
        }); //123456  =>  € ___.__1.234,56

        $("#mask_currency2").inputmask('€ 999,999,999.99', {
            numericInput: true,
            rightAlignNumerics: false,
            greedy: false
        }); //123456  =>  € ___.__1.234,56
        $("#mask_ssn").inputmask("999-99-9999", {
            placeholder: " ",
            clearMaskOnLostFocus: true
        }); //default
    }

    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        FormInputMask.init(); // init metronic core componets
    });
}


/****************************************************/
/****** Bootstrap Multi Select **********************/
/****************************************************/

var ComponentsBootstrapMultiselect = function () {

    return {
        //main function to initiate the module
        init: function () {
            $('.mt-multiselect').each(function () {
                var btn_class = $(this).attr('class');
                var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false;
                var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false;
                var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false;
                var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false;
                var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false;
                var width = ($(this).data('width')) ? $(this).data('width') : '';
                var height = ($(this).data('height')) ? $(this).data('height') : '';
                var filter = ($(this).data('filter')) ? $(this).data('filter') : false;

                // advanced functions
                var onchange_function = function (option, checked, select) {
                    alert('Changed option ' + $(option).val() + '.');
                }
                var dropdownshow_function = function (event) {
                    alert('Dropdown shown.');
                }
                var dropdownhide_function = function (event) {
                    alert('Dropdown Hidden.');
                }

                // init advanced functions
                var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
                var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
                var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

                // template functions
                // init variables
                var li_template;
                if ($(this).attr('multiple')) {
                    li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
                } else {
                    li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
                }

                // init multiselect
                $(this).multiselect({
                    enableClickableOptGroups: clickable_groups,
                    enableCollapsibleOptGroups: collapse_groups,
                    disableIfEmpty: true,
                    enableFiltering: filter,
                    includeSelectAllOption: select_all,
                    dropRight: drop_right,
                    buttonWidth: width,
                    maxHeight: height,
                    onChange: onchange,
                    onDropdownShow: dropdownshow,
                    onDropdownHide: dropdownhide,
                    buttonClass: btn_class,
                    //optionClass: function(element) { return "mt-checkbox"; },
                    //optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
                    /*templates: {
                     li: li_template,
                     }*/
                });
            });

        }
    };

}();

jQuery(document).ready(function () {
    ComponentsBootstrapMultiselect.init();
});