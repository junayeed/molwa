$(document).ready(function (){

    //FormValidation.init(); //Init the form valition

    $(".input-daterange").datepicker({
        isRTL: false,
        format: 'dd-mm-yyyy',
        autoclose:true,
        language: 'bn'
    });

    $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd-mm-yyyy',autoclose:!0});

    calculateDateDiff();  // Calcualate and populate date difference for Edit view

    $('#country').multiSelect({
        afterSelect: function(country){
            add2Map(country);
        },
        afterDeselect: function(country){
            removeFromMap(country);
        }
    });

    $('#ms-country').css('width', '220%');

    getParticipantDetails($('#employees').val(), 'foreign_training_participants');

    Highcharts.mapChart('container', {
        chart: {
            width: 1000,
            height: 550,
            plotBorderWidth: 1,
            plotBorderColor: '#ffffff',
            borderColor: "#ffffff",
            plotBackgroundColor: '#FFFFff',
            map: 'custom/world',
        },

        title: {
            text: "" ,
            enabled: false
        },

        credits: {
            enabled: false
        },
        exporting:{
            enabled: true
        },
        legend: {
            enabled: false
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        series: [{
            data: [],
            mapData: Highcharts.maps['custom/world'],
            joinBy: ['iso-a2', 'code']
        }]
    });
    $('#container').highcharts().series[0].addPoint({code: 'BD'});


    $.each(selected_country_iso, function(key, val){
        $('#container').highcharts().series[0].addPoint({code: val});
    });
});

function add2Map(country) {
    $.each(country_list, function (key, val){
        if (val.id == country) {
            $('#container').highcharts().series[0].addPoint({code: val.iso});
        }
    });
}

function removeFromMap(country) {
    //$('#container').highcharts().series[0]
}

$('body').on('hide', '.date-picker', function() {
    calculateDateDiff();
});

$('.add_participants').on('click', function(){
    var batch_id = $(this).data('batch_id');

    getParticipantList(batch_id);
});

var employees = [];

$('body').on('click', '.attachment_view', function(){
    var modal_title      = $(this).data('modal_title');
    var modal_iframe_src = $(this).data('ref');

    $("#attachment_view_modal_title").text(modal_title);
    $("#attachment_iframe").attr("src", '\\' + modal_iframe_src);
});

$('body').on('ifChanged', '.employee_id', function(event) {
    var employee_id     = event.target.value;

    if( event.target.checked ) {
        employees.push(employee_id);

    }
    else {
        var index = employees.indexOf(employee_id);
        if (index !== -1) {
            employees.splice(index, 1);
        }
    }

    $('#employees').val(employees);
});

$('body').on('click', '.btn_participant_close', function(){
    $('#participant_modal').modal('hide');

    getParticipantDetails($('#employees').val(), 'foreign_training_participants');
    //window.location = '/batch';
});

$('.participant_list').on('click', function(){
    getParticipantDetails($(this).data('employees'), 'foreign_training_participants_list');
    //window.location = '/batch';
});

String.prototype.getDigitBanglaFromEnglish = function() {
    var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
    var retStr = this;

    for (var x in finalEnlishToBanglaNumber) {
        retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
    }

    return retStr;
};

function calculateDateDiff() {
    var start_date = $('#start_date').val();
    var end_date   = $('#end_date').val();

    if ( start_date) {
        //var start_date = new Date(start_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
        var start_date = new Date(start_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    }

    if ( end_date ) {
        var end_date = new Date(end_date.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    }

    var diff =  Math.floor(( end_date - start_date ) / 86400000);

    $('#batch_days_duration').text((diff+1).toString().getDigitBanglaFromEnglish() + ' দিন');
}

function getParticipantList() {
    var designation    = $('#designation').val() ;
    var employee_class = $('#employee_class').val();
    var office         = $('#office').val();

    $.ajax({
        type: "GET",
        url: "/ForeignTraining/getParticipantList/",
        data: {designation: designation, employee_class: employee_class, office: office},
        dataType: 'html',
        success: function (res) {
            $('#participant_details').html(res);
        }
    });
}

function getParticipantDetails(employees, destElem) {
    $.ajax({
        type: "GET",
        url: "/ForeignTraining/getParticipantDetails/"+employees,
        //data: {employees: employees},
        dataType: 'html',
        success: function (res) {
            $('#'+destElem).html(res);

        }
    });
}