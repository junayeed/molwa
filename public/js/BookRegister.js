$(".exploder").click(function(){

    //$(this).toggleClass("btn-success btn-danger");

    $(this).children("span").toggleClass("fa-plus fa-minus");

    $(this).closest("tr").next("tr").toggleClass("hide");

    if($(this).closest("tr").next("tr").hasClass("hide")){
        $(this).closest("tr").next("tr").children("td").slideUp();
    }
    else{
        $(this).closest("tr").next("tr").children("td").slideDown(250);
    }
});

/*
$(document).ready(function (){

    FormValidation.init(); //Init the form valition

    var batch_trainer = $("#books_repeater").repeater({
        initEmpty:!1,
        show:function(){ $(this).slideDown(); },
        hide:function(e){
            //confirm("আপনি কি নিশ্চিত আপনি এই বই টির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e);
            if ( confirm("আপনি কি নিশ্চিত আপনি এই বই টির তথ্য মুছে ফেলতে চান?") ) {
                $(this).slideUp(e);
                setTimeout(function(){
                    return calculateTotalBookDistribution();
                }, 50);
            }

        }
    });

    // Repeater Set data starts here
    if ( book_dis_array.length ) { // SET DATA FOR TRAINER
        var items = {};
        $.each(book_dis_array, function (key, val) {
            items[key] = {book_id: val.book_id, writer: val.writer, dist_book_copy: val.book_distributed};

            batch_trainer.setList(items);
        });

        var i = 0;
        /!*while($("input[name='books_data["+i+"][trainer_type][]']").length) {
            if(!$("input[name='books_data["+i+"][trainer_type][]']").is(":checked")){
                $("input[name='books_data["+i+"][trainer_type][]']").closest('#trainer_type_div').find('#trainer_type_lbl').html('অন্য অফিস');
                $("input[name='books_data["+i+"][trainer_type][]']").parent().next().closest('#own_office_div').find('#own_office').hide();
                $("input[name='books_data["+i+"][trainer_type][]']").parent().next().closest('#own_office_div').find('#other_office').show();
            }
            i++;
        }*!/
    }

    calculateTotalBookDistribution();

    $(".session_start_time,.session_end_time").timepicker({autoclose:!0, minuteStep:15, showSeconds:0, showMeridian:!1, defaultTime: '' });
    $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd/mm/yyyy',autoclose:!0});
});

$('body').on('change', '.book_distribution', function(){
    var book = $.ajax({
        type: "GET",
        async: false,
        url: "/BookDistribution/getBookDetails",
        data: {book_id: $(this).val()},
        dataType: 'json',
        success: function (res) { return res; }
    });

    $(this).closest('#book_name').closest('#books_details_div').find('#writer_div').html(book.responseJSON.writer);
    $(this).closest('#book_name').closest('#books_details_div').find('#publisher_div').html(book.responseJSON.publisher_bn);
    $(this).closest('#book_name').closest('#books_details_div').find('#no_copy_div').html(book.responseJSON.no_copy);
    $(this).closest('#book_name').closest('#books_details_div').find('#dist_book_copy_div').find('#dist_book_copy').val('');
});

$('body').on('change', '.no_book_copy', function() {
    var dist_book_copy = $(this).val() * 1;

    book_id = $(this).parent().parent().find('#book_id').val() ;

    var book = $.ajax({
        type: "GET",
        async: false,
        url: "/BookDistribution/getBookDetails",
        data: {book_id: book_id},
        dataType: 'json',
        success: function (res) { return res; }
    });

    $(this).closest('#dist_book_copy_div').closest('#books_details_div').find('#no_copy_div').html(book.responseJSON.no_copy);
    var available_book_copy = $(this).closest('#dist_book_copy_div').closest('#books_details_div').find('#no_copy_div').text() * 1;

    $(this).closest('#dist_book_copy_div').closest('#books_details_div').find('#no_copy_div').text(available_book_copy-dist_book_copy);

    calculateTotalBookDistribution();
});

function calculateTotalBookDistribution() {
    var total_book_distribution = 0;

    $('.no_book_copy').each(function() {
        total_book_distribution += $(this).val()*1;
    });

    $('#total_book_distribution').html(total_book_distribution + "\n" + "কপি");
}

var FormValidation = function () {
    var batch_form = function() {
        var form = $('#batch_form');
        var error = $('.alert-danger', form);

        $("#batch_form").validate(
            {
                errorElement : 'span', //---- default input error message container
                errorClass   : 'help-block help-block-error', //---- default input error message class
                focusInvalid : false, //---- do not focus the last invalid input
                ignore       : "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'batch_size'           : "required",
                        'training_venue'       : "required",
                        'training_title'       : "required",
                        'batch_start_date'     : "required",
                        'batch_end_date'       : "required",
                        'batch_start_time'     : "required",
                        'batch_end_time'       : "required",
                        'batch_status'         : "required",
                        'training_type'        : "required",
                    },
                messages: { },
                errorPlacement: function() { return true; },  // will remove the error ("This field is required") messahe
                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /!*submitHandler : function()
                {
                    return false;
                }*!/

            });
    };
    return {
        //---- main function to initiate the module
        init: function () {
            batch_form();
        }
    };
}();*/
