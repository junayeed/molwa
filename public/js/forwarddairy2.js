$(document).ready(function (){

    FormValidation.init();

    $('#dc_office').hide();
    $('#uno_office').hide();
    $('#ministry').hide();
    $('#recipient_ministry_div').hide();
    $('#subject_type').select2();
    $('#sf_uno_office').select2();

    $('#sf_dc_office').hide();
    $('#sf_uno_office').next(".select2-container").hide();//$('#sf_uno_office').hide();
    $('#sf_ministry').hide();

    $('.date-picker').datepicker({
        autoclose: true,
        isRTL: Metronic.isRTL(),
        format: "dd/mm/yyyy"
    });



    $("#has_interim_order").bootstrapSwitch('state', true);
    $("#has_rule_nisi").bootstrapSwitch('state', true);


    $('#has_interim_order').on('switchChange.bootstrapSwitch', function() {

        if ($("#has_interim_order").bootstrapSwitch('state')) {
            $('#interim_order_div').show();
        }
        else {
            $('#interim_order_div').hide();
        }
    });

    $('#has_rule_nisi').on('switchChange.bootstrapSwitch', function() {

        if ($("#has_rule_nisi").bootstrapSwitch(('state'))) {
            $('#rule_nisi_div').show();
        }
        else {
            $('#rule_nisi_div').hide();
        }
    });

    $('#case_status').on('change', function(){
        if(this.value == 6) { // if the status Necessary action required that is nothing to deal with MOLWA, case send to concern Ministry
            $('#sf_details_div').hide();
            $('#recipient_ministry_div').show();
        }
        else {
            $('#sf_details_div').show();
            $('#recipient_ministry_div').hide();
        }
    });
});

$('body').on('keyup', '#writ_no', function(){
    $('.writ_no').text(this.value);
});

$('#sf_recipient').on('change', function(){
    if ($(this).val() == 6 ) // show the district list for DC office
    {
        $('#sf_dc_office').show();
        $('#sf_uno_office').next(".select2-container").hide(); //$('#sf_uno_office').hide();
        $('#sf_ministry').hide();
    }
    else if ($(this).val() == 7) // show Upzilla list for UNO office
    {
        $('#sf_uno_office').next(".select2-container").show() ;//$('#sf_uno_office').show();
        $('#sf_dc_office').hide();
        $('#sf_ministry').hide();
    }
    else if ( $(this).val() == 3 ) // show Ministry List
    {
        $('#sf_ministry').show();
        $('#sf_dc_office').hide();
        $('#sf_uno_office').next(".select2-container").hide(); //$('#sf_uno_office').hide();
    }
    else // hide Ministry, District and Upzilla list
    {
        $('#sf_dc_office').hide();
        $('#sf_uno_office').next(".select2-container").hide(); //$('#sf_uno_office').hide();
        $('#sf_ministry').hide();
    }
});

$('#implementing_authority').on('change', function(){
    if ($(this).val() == 6 ) // show the district list for DC office
    {
        $('#dc_office').show();
        $('#uno_office').hide();
        $('#ministry').hide();
    }
    else if ($(this).val() == 7) // show Upzilla list for UNO office
    {
        $('#uno_office').show();
        $('#dc_office').hide();
        $('#ministry').hide();
    }
    else if ( $(this).val() == 8 ) // show Ministry List
    {
        $('#ministry').show();
        $('#dc_office').hide();
        $('#uno_office').hide();
    }
    else // hide Ministry, District and Upzilla list
    {
        $('#dc_office').hide();
        $('#uno_office').hide();
        $('#ministry').hide();
    }
});

$('.btn-save').on('click', function (){

    /*if ($('#case_status').val() == 3) // if the case status is SF Received
    {
        $("#sf_data_container .validate_sf_receive_date").each(function(){
            $(this).rules("add", {required: true});
        });
    }*/

    // if has_interim_order is checked then all the dynamic fileds in Interim Order section is required
    if($('#has_interim_order').is(':checked')) {
        $('.validateImpAuth').each(function () {
            $(this).rules("add", {required: true});
        });
    }
    //otherwise remove the rules
    else {
        $('.validateImpAuth').each(function () {
            $(this).rules('remove', 'required');
        });
    }
});

$('#case_no').on('change', function(){
    var case_no = $(this).val();

    if ( !case_no) {
        return;
    }

    $.ajax({
        type: "GET",
        url: "/checkDuplicateCaseNo",
        data: {case_no: case_no},
        dataType: 'JSON',
        success: function (res) {
            alert(res);
        }
    });
});

var FormValidation = function () {
    var addUserForm = function() {
        var form = $('#case_details_form');
        var error = $('.alert-danger', form);

        $("#case_details_form").validate(
            {
                errorElement: 'span', //---- default input error message container
                errorClass: 'help-block help-block-error', //---- default input error message class
                focusInvalid: false, //---- do not focus the last invalid input
                ignore: "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'case_no'                 : "required",
                        'case_type'               : "required",
                        //'file_no'                 : "required",
                        'case_receive_date'       : "required",
                        'parties_name'            : "required",
                        'subject_type'            : "required",
                        'case_subject'            : "required",
                        'case_status'             : "required",
                        // if the has_interim_order is checked
                        'interim_order_date'      : { required: '#has_interim_order:checked' },
                        'deadline'                : { required: '#has_interim_order:checked' },
                        'order_details'           : { required: '#has_interim_order:checked' },
                        // if the has_rule_nisi is checked
                        'rule_nisi_order_date'    : { required: '#has_rule_nisi:checked'},
                        'rule_nisi_order_details' : { required: '#has_rule_nisi:checked'},
                        'sf_issue_memo'           : {
                            required:{ depend: function() {
                                    return $('#case_status').val() === '2';
                                }}
                        }

                    },
                messages: { },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /*submitHandler : function()
                {
                    return false;
                }*/
            });
    }
    return {
        //---- main function to initiate the module
        init: function () {
            addUserForm();
        }
    };
}();