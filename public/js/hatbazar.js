$(document).ready(function (){

    FormValidation.init(); //Init the form valition
    $('#division_div,#district_div,#upazilla_div,#city_corporation_div,#dc_office_div,#pourosova_div').hide();

    $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd/mm/yyyy', autoclose: !0});

    updateOfficeTypeDD( $('#office_type') );

    var division = $('#division').val();
    //alert('Division = ' + division)
    if( division ) {
        getDistrictList(division);
    }



});

var FormValidation = function () {
    var hatbazar_form = function() {
        var form = $('#hatbazar_form');
        var error = $('.alert-danger', form);

        $("#hatbazar_form").validate(
            {
                errorElement : 'span', //---- default input error message container
                errorClass   : 'help-block help-block-error', //---- default input error message class
                focusInvalid : false, //---- do not focus the last invalid input
                ignore       : "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'office_type'         : "required",
                        'division'            : {required: { depends: function(element) { if ( $('#office_type').val() == 1 ){ return true; } else{ return false; } } } },
                        'district'            : {required: { depends: function(element) { if ( $('#office_type').val() == 1 ){ return true; } else{ return false; } } } },
                        'upazilla'            : {required: { depends: function(element) { if ( $('#office_type').val() == 1 ){ return true; } else{ return false; } } } },
                        'city_corporation'    : {required: { depends: function(element) { if ( $('#office_type').val() == 4 ){ return true; } else{ return false; } } } },
                        'dc_office'           : {required: { depends: function(element) { if ( $('#office_type').val() == 2 ){ return true; } else{ return false; } } } },
                        'pourosova'           : {required: { depends: function(element) { if ( $('#office_type').val() == 3 ){ return true; } else{ return false; } } } },
                        'bengali_year'        : "required",
                        'cheque_bank'         : "required",
                        'cheque_amount'       : "required",
                        'cheque_number'       : "required",
                        'cheque_date'         : "required",
                        'deposite_ac_number'  : "required",
                        'deposite_date'       : "required",
                        'deposite_book_no'    : "required",
                        'deposite_inst_no'    : "required",
                    },
                messages: { },
                errorPlacement: function() { return true; },  // will remove the error ("This field is required") messahe
                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /*submitHandler : function()
                {
                    return false;
                }*/

            });
    }
    return {
        //---- main function to initiate the module
        init: function () {
            hatbazar_form();
        }
    };
}();

$('body').on('change', '#office_type', function(){
    updateOfficeTypeDD(this);
});

function updateOfficeTypeDD(th) {
    if ( $(th).val() == 1 ) { // UNO Office
        $('#division_div,#district_div,#upazilla_div').show();
        $('#city_corporation_div,#dc_office_div,#pourosova_div').hide();
    }
    else if ( $(th).val() == 2 ){  // DC Office
        $('#division_div,#district_div,#upazilla_div,#city_corporation_div,#pourosova_div').hide();
        $('#dc_office_div').show();
    }
    else if ( $(th).val() == 3 ){  // Pourosova
        $('#upazilla_div,#city_corporation_div,#dc_office_div').hide();
        $('#division_div,#district_div,#pourosova_div').show();
    }
    else if ( $(th).val() == 4 ){  // City Corporation Office
        $('#division_div,#district_div,#upazilla_div,#dc_office_div,#pourosova_div').hide();
        $('#city_corporation_div').show();
    }
}

String.prototype.getDigitBanglaFromEnglish = function() {
    var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
    var retStr = this;

    for (var x in finalEnlishToBanglaNumber) {
        retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
    }

    return retStr;
};

$('body').on('change', '#division', function(){
    var division = $(this).val();
    if ( division ) {
        getDistrictList(division);
    }
});

function getDistrictList(division) {

    var office_type  = $('#office_type').val();
    $.ajax({
        type: "GET",
        url: "/getDistrictList",
        data: {division: division},
        dataType: 'JSON',
        success: function (data) {
            $('#district,#upazilla').empty();
            $("#district").append(new Option(" - জেলা বাছাই করুন - ", 0) );
            $('#upazilla').append(new Option(" - উপজেলা বাছাই করুন - ", 0) );
            $.each(data, function(key, item){
                $("#district").append(new Option(item, key) );
            });
        },
        complete: function(){
            var district_val = $('#district_val').val();
            if ( district_val ){
                $('#district').val(district_val);
                getUpazillaList(district_val, office_type);
            }
        }
    });
}

$('body').on('change', '#district', function(){
    var district     = $(this).val();
    var office_type  = $('#office_type').val();

    if ( district ) {
        getUpazillaList(district, office_type);
    }
});

function getUpazillaList(district, office_type) {
    if (office_type == 1) {
        elem_id = 'upazilla';
        elem_text = ' - উপজেলা বাছাই করুন - ';
    }
    else if (office_type == 3) {
        elem_id = 'pourosova';
        elem_text = ' - পৌরসভা বাছাই করুন - ';
    }

    $.ajax({
        type: "GET",
        url: "/getUpazillaList",
        data: {district: district, office_type: office_type},
        dataType: 'JSON',
        success: function (data) {
            $('#'+elem_id).empty();
            $('#'+elem_id).append(new Option(elem_text, 0) );
            $.each(data, function(key, item){
                $('#'+elem_id).append(new Option(item, key) );
            });
        },
        complete: function () {
            var upazilla_val = $('#upazilla_val').val();
            //alert('Upazilla = ' + upazilla_val);
            if (upazilla_val) {
                $('#'+elem_id).val(upazilla_val);
            }
        }
    });


}