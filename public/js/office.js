$(document).ready(function (){
    FormValidation.init(); //Init the form valition
});

var FormValidation = function () {
    var office_form = function() {
        var form = $('#office_form');
        var error = $('.alert-danger', form);

        $("#office_form").validate(
            {
                errorElement : 'span', //---- default input error message container
                errorClass   : 'help-block help-block-error', //---- default input error message class
                focusInvalid : false, //---- do not focus the last invalid input
                ignore       : "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'office_name_bn'      : "required",
                        'office_name_en'      : "required",
                        'office_status'       : "required",
                    },
                messages: { },
                errorPlacement: function() { return true; },  // will remove the error ("This field is required") messahe
                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /*submitHandler : function()
                {
                    return false;
                }*/

            });
    }
    return {
        //---- main function to initiate the module
        init: function () {
            office_form();
        }
    };
}();