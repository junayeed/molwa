$(document).ready(function (){

    FormValidation.init(); //Init the form valition

    $('#other_office').hide();

    $(".input-daterange").datepicker({
        isRTL: false,
        format: 'dd/mm/yyyy',
        autoclose:true,
        language: 'bn'
    });

    $("#batch_start_time,#batch_end_time").timepicker({
        autoclose:!0,
        minuteStep:30,
        showSeconds:!1,
        defaultTime: '',
        showMeridian:!1 //or timeFormat: 'h:mm p',
    });

    calculateDateDiff();  // Calcualate and populate date difference for Edit view

    var batch_trainer = $("#batch_session_repeater").repeater({
        initEmpty:!1,
        show:function(){ $(this).slideDown(); },
        hide:function(e){ confirm("আপনি কি নিশ্চিত আপনি এই বিবরণটির তথ্য মুছে ফেলতে চান?")&&$(this).slideUp(e) }
    });

    // Repeater Set data starts here
    if ( trainer_array.length ) { // SET DATA FOR TRAINER
        var items = {};
        $.each(trainer_array, function (key, val) {
            items[key] = {session_topic: val.session_topic, employee_id: val.employee_id, session_start_time: val.session_start_time,
                          session_end_time: val.session_end_time, trainer_type: val.trainer_type, trainer_name: val.trainer_name,
                          trainer_office: val.trainer_office, trainer_email: val.trainer_email, trainer_cellphone: val.trainer_cellphone,
                          session_date: val.session_date};

            batch_trainer.setList(items);
        });

        var i = 0;

        while($("input[name='batch_session_data["+i+"][trainer_type][]']").length) {
            if(!$("input[name='batch_session_data["+i+"][trainer_type][]']").is(":checked")){
                $("input[name='batch_session_data["+i+"][trainer_type][]']").closest('#trainer_type_div').find('#trainer_type_lbl').html('অন্য অফিস');
                $("input[name='batch_session_data["+i+"][trainer_type][]']").parent().next().closest('#own_office_div').find('#own_office').hide();
                $("input[name='batch_session_data["+i+"][trainer_type][]']").parent().next().closest('#own_office_div').find('#other_office').show();
            }
            i++;
        }
    }

    /*$('#include_weekend').iCheck({
        checkboxClass: 'icheckbox_square-red',
        increaseArea: '40%' // optional
    });*/

    $(".session_start_time,.session_end_time").timepicker({autoclose:!0, minuteStep:15, showSeconds:0, showMeridian:!1, defaultTime: '' });
    $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd/mm/yyyy',autoclose:!0});

    loadAPAPerformanceIndex($('#apa_activities').val(), apa_performance_index_val);
});

$('body').on('click', '#send_sms_btn', function (){
    var sms_text  = $('#sms_text').val();
    var batch_id  = $('#id').val();

    $.ajax({
        type: "GET",
        url: "/batch/sendSMS/",
        data: {batch_id: batch_id, sms_text: sms_text},
        dataType: 'html',
        success: function (res) {
            //$('#participant_details').html(res);
            var data                 = JSON.parse(res);
            var total_participants   = data.total;
            var participant_counter  = 0;

            $.each(data.reply, function (key, val) {
                setTimeout(function(){
                    $('.sms_sent_number').text(++participant_counter);
                    $('.sms_sending').data('easyPieChart').update(participant_counter/total_participants*100);
                    $('.feeds').append('<li><div class="col1"><div class="desc">' + key + ' ... ' + val + '</div></div></li>');
                }, participant_counter * 10000000);
            });
        }
    });
});

$('body').on('click', '.attachment_view', function(){
    var modal_title      = $(this).data('modal_title');
    var modal_iframe_src = $(this).data('ref');

    $("#attachment_view_modal_title").text(modal_title);
    $("#attachment_iframe").attr("src", '\\' + modal_iframe_src);
});

$('body').on('click', '.btn-cancel', function(){
    window.location = '/batch';
});

$('body').on('click', '.add_new_session', function(){
    //$(this).closest('#home_property_details_div').remove();

    $('.session_start_time').each(function ()
    {
        $(".session_start_time,.session_end_time").timepicker({ autoclose:!0, minuteStep:15, showSeconds:0, showMeridian:!1 /*or timeFormat: 'h:mm p', */});

        $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd/mm/yyyy', autoclose:!0});
    });
});

$('body').on('change', '.session_start_time,.session_end_time', function() {
    var start_time = $(this).val();
    var end_time   = $(this).val();

    //$('#batch_time_length').text(getTimeDifference(start_time, end_time).getDigitBanglaFromEnglish());
});


$('body').on('click', '.delete_session', function(){
    $(this).closest('#batch_session_details_div').remove();
});

$('body').on('click', '.trainer_type', function(){
    if ($(this).is(":checked")) {
        $(this).closest('#trainer_type_div').find('#trainer_type_lbl').html('নিজ অফিস');
        $(this).parent().next().closest('#own_office_div').find('#own_office').show();
        $(this).parent().next().closest('#own_office_div').find('#other_office').hide();
    }
    else {
        $(this).closest('#trainer_type_div').find('#trainer_type_lbl').html('অন্য অফিস');
        $(this).parent().next().closest('#own_office_div').find('#own_office').hide();
        $(this).parent().next().closest('#own_office_div').find('#other_office').show();
    }
});

/*$('body').on('ifChanged', '#include_weekend', function(){
    calculateDateDiff();
});*/

$('body').on('change', '#apa_activities', function(){
    loadAPAPerformanceIndex($(this).val(), '');
});

function loadAPAPerformanceIndex(apa_activities, apa_performance_index_val) {

    if(apa_activities === "2.2") {
        $('#apa_performance_index').empty();
        $("#apa_performance_index").append(new Option(" - কর্মসম্পাদন সূচক বাছাই করুন - ", "0") );
        $("#apa_performance_index").append(new Option("[২.২.১] প্রকল্প ব্যবস্থাপনা সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল", "2.2.1") );
        $("#apa_performance_index").append(new Option("[২.২.২] ই-জিপি সংক্রান্ত প্রশিক্ষণ প্রদানের মাধ্যমে প্রশিক্ষিত জনবল", "2.2.2") );
    }
    else if (apa_activities === "2.5") {
        $('#apa_performance_index').empty();
        $("#apa_performance_index").append(new Option(" - কর্মসম্পাদন সূচক বাছাই করুন - ", "0") );
        $("#apa_performance_index").append(new Option("[২.৫.১] কারিগরি দক্ষতা বৃদ্ধির লক্ষ্যে আয়োজিত প্রশিক্ষণে অংশগ্রহণকারী", "2.5.1") );
        $("#apa_performance_index").append(new Option("[২.৫.২] ১০ম গ্রেড ও তদুর্ধ্ব প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ", "2.5.2") );
        $("#apa_performance_index").append(new Option("[২.৫.৩] ১১তম - ২০তম গ্রেডের প্রত্যেক কর্মচারীকে প্রদত্ত প্রশিক্ষণ", "2.5.3") );
        $("#apa_performance_index").append(new Option("[২.৫.৪] সমসাময়িক বিষয় নিয়ে লার্নিং সেশন আয়োজিত", "2.5.4") );
    }

    $("#apa_performance_index").val(apa_performance_index_val);
}

$('body').on('change', '#include_weekend', function(){
    calculateDateDiff();
});

$('body').on('hide', '.date-picker', function() {
    calculateDateDiff();
});

var FormValidation = function () {
    var batch_form = function() {
        var form = $('#batch_form');
        var error = $('.alert-danger', form);

        $("#batch_form").validate(
            {
                errorElement : 'span', //---- default input error message container
                errorClass   : 'help-block help-block-error', //---- default input error message class
                focusInvalid : false, //---- do not focus the last invalid input
                ignore       : "",  //---- validate all fields including form hidden input
                rules:
                    {
                        'batch_size'           : "required",
                        'training_venue'       : "required",
                        'training_title'       : "required",
                        'batch_start_date'     : "required",
                        'batch_end_date'       : "required",
                        'batch_start_time'     : "required",
                        'batch_end_time'       : "required",
                        'batch_status'         : "required",
                        'training_type'        : "required",
                    },
                messages: { },
                errorPlacement: function() { return true; },  // will remove the error ("This field is required") messahe
                invalidHandler: function (event, validator) { //display error alert on form submit
                    error.show();
                    Metronic.scrollTo(error, -200);
                    error.delay(6000).fadeOut(2000);
                },
                highlight: function (element)
                { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element)
                { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label)
                {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                /*submitHandler : function()
                {
                    return false;
                }*/

            });
    }
    return {
        //---- main function to initiate the module
        init: function () {
            batch_form();
        }
    };
}();




String.prototype.getDigitBanglaFromEnglish = function() {
    var finalEnlishToBanglaNumber={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯'};
    var retStr = this;

    for (var x in finalEnlishToBanglaNumber) {
        retStr = retStr.replace(new RegExp(x, 'g'), finalEnlishToBanglaNumber[x]);
    }

    return retStr;
};

function calculateDateDiff() {
    var start_date = $('#batch_start_date').val();
    var end_date   = $('#batch_end_date').val();

    if ( start_date) {
        var start_date = new Date(start_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
    }

    if ( end_date ) {
        var end_date = new Date(end_date.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
    }

    var diff =  Math.floor(( end_date - start_date ) / 86400000);

    //alert($('#include_weekend').is(':checked'));
    //alert( $('#include_weekend').iCheck('check').val()) ;

    if ( !$('#include_weekend').is(':checked')){
        // check whether there is any weekend
        while (start_date <= end_date) {
            var day = start_date.getDay();
            isWeekend = (day === 6) || (day === 5);
            if (isWeekend) { // if weekend decrease the day difference by 1
                diff--;
            }
            start_date.setDate(start_date.getDate() + 1);
        }
    }

    $('#batch_days_duration').text((diff+1).toString().getDigitBanglaFromEnglish() + ' দিন');
}

function timeStringToMins(s) {
    s = s.split(':');
    s[0] = /m$/i.test(s[1]) && s[0] == 12? 0 : s[0];
    return s[0]*60 + parseInt(s[1]) + (/pm$/i.test(s[1])? 720 : 0);
}

// Return difference between two times in hh:mm[am/pm] format as hh:mm
function getTimeDifference(t0, t1) {
    // Small helper function to padd single digits
    function z(n){return (n<10?'0':'') + n;}

    // Get difference in minutes
    var diff = timeStringToMins(t1) - timeStringToMins(t0);

    // Format difference as hh:mm and return
    return   z(diff/60 | 0) + ':' + z(diff % 60);
}

$('body').on('change', '#batch_start_time,#batch_end_time', function() {
    var start_time = $('#batch_start_time').val();
    var end_time   = $('#batch_end_time').val();
    //var diff       = new Date(end_time) - new Date( start_time);
    if (start_time && end_time) {
        $('#batch_time_length').text(getTimeDifference(start_time, end_time).getDigitBanglaFromEnglish());
    }
});


$('body').on('shown.bs.modal', '#participant_modal', function () {
    /*$('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        increaseArea: '40%' // optional
    });*/
});

$('body').on('ifChanged', '.employee_id', function(event) {
    var employee_id     = event.target.value;
    var emp_profile_id  = $(this).data('emp_profile_id');
    var batch_id        = $(this).data('batch_id');

    if( event.target.checked ) {
        addBatchParticipant(batch_id, employee_id, emp_profile_id);
    }
    else {
        removeBatchPartipant(batch_id, employee_id)
    }
});

function addBatchParticipant(batch_id, employee_id, emp_profile_id) {
    $.ajax({
        type: "GET",
        url: "batch/saveBatchParticipant/",
        data: {batch_id: batch_id, employee_id: employee_id, emp_profile_id: emp_profile_id},
        dataType: 'JSON',
        success: function (res) {
            //$('#participant_details').html(res);
        }
    });
}

function removeBatchPartipant(batch_id, employee_id) {
    $.ajax({
        type: "GET",
        url: "batch/removeBatchParticipant/",
        data: {batch_id: batch_id, employee_id: employee_id},
        dataType: 'JSON',
        success: function (res) {
            //$('#participant_details').html(res);
        }
    });
}

$('.add_participants').on('click', function(){
    var batch_id = $(this).data('batch_id');

    getParticipantDetails(batch_id)
});

$('body').on('click', '.btn_participant_close', function(){
    $('#participant_modal').modal('hide');
    window.location = '/batch';
});

$('body').on('change', '#designation,#employee_class,#office', function(){
    var batch_id = $(this).data('batch_id');
    getParticipantDetails(batch_id)
});

function getParticipantDetails(batch_id) {
    var designation    = $('#designation').val() ;
    var employee_class = $('#employee_class').val();
    var office         = $('#office').val();

    $.ajax({
        type: "GET",
        url: "batch/getParticipantDetails/",
        data: {batch_id: batch_id, designation: designation, employee_class: employee_class, office: office},
        dataType: 'html',
        success: function (res) {
            $('#participant_details').html(res);
        }
    });
}