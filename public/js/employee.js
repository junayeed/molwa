$(document).ready(function (){

    $(".date-picker").datepicker({rtl:false,orientation:"left", format: 'dd-mm-yyyy',autoclose:!0});

    //var myvalues = [0.0, 4.5, 6.0, 3, 9, 3, 5, 6, 9, 3, 5, 5];
    $("#sparkline_bar5").sparkline(months_val, {
        type: 'bar',
        width: '150',
        barWidth: 12,
        height: '65',
        barColor: '#35aa47',
        negBarColor: '#e02222',
        tooltipFormat: '{{offset:offset}}: {{value:val}}',
        tooltipValueLookups: {
            'offset': {
                 0: 'জুলাই',
                 1: 'আগস্ট',
                 2: 'সেপ্টেম্বর',
                 3: 'অক্টোবর',
                 4: 'নভেম্বর',
                 5: 'ডিসেম্বর',
                 6: 'জানুয়ারি',
                 7: 'ফেব্রুয়ারি',
                 8: 'মার্চ',
                 9: 'এপ্রিল',
                10: 'মে',
                11: 'জুন'
            }
        },
    });
});

$('body').on('change', '#dob', function(){

    var dob = $("#dob").datepicker("getDate");

    //var prl_date        = new Date(dob.getFullYear() + 58, dob.getMonth(), dob.getDate()-1);
    //var retirement_date = new Date(dob.getFullYear() + 59, dob.getMonth(), dob.getDate()-1).toLocaleString().split(',')[0];

   var dob_array = $("#dob").val().toLocaleString().split('-');
   var prl_date = (dob_array[0]*1-1) + '-' + dob_array[1] + '-' + (dob_array[2]*1+58);
   var retirement_date = (dob_array[0]*1-1) + '-' + dob_array[1] + '-' + (dob_array[2]*1+59);

    //alert(prl_day + '-' + prl_month + '-' + prl_year);
    
    $('#prl_date').val(prl_date);
    $('#retirement_date').val(retirement_date);
});

$('.foreign_training_view').on('click', function(){
    var employee_id = $(this).data('employee_id');

    getEmployeeForeignTrainingDetails(employee_id);
});

function getEmployeeForeignTrainingDetails(employee_id) {
    $.ajax({
        type: "GET",
        url: "/employee/getEmployeeForeignTrainingDetails/" + employee_id,
        //data: {employee_id: employee_id},
        dataType: 'html',
        success: function (res) {
            $('#foreign_training_details').html(res);

        }
    });
}