﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2017 at 06:11 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gmis`
--

-- --------------------------------------------------------

--
-- Table structure for table `indicators`
--

DROP TABLE IF EXISTS `indicators`;
CREATE TABLE `indicators` (
  `id` int(10) UNSIGNED NOT NULL,
  `serial` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicator_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicator_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `indicator_title_bn` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `indicators`
--

INSERT INTO `indicators` (`id`, `serial`, `indicator_title`, `indicator_status`, `created_at`, `updated_at`, `indicator_title_bn`, `main_area`) VALUES
(1, '4.1', 'Office building', 1, NULL, NULL, 'অফিস ভবন', 1),
(2, '4.2', 'Upazila parishad bhaban', 1, NULL, NULL, 'উপজেলা পরিষদ ভবন', 1),
(3, '4.3', 'UP complex', 1, NULL, NULL, 'ইউপি কমপ্লেক্স', 1),
(4, '4.4', 'Community center', 1, NULL, NULL, 'কমিউনিটি সেন্টার', 1),
(5, '4.5', 'School building', 1, NULL, NULL, 'বিদ্যালয় ভবন', 1),
(6, '4.6', 'Surround shelter center', 1, NULL, NULL, 'ঘূর্নীঝড় আশ্রয় কেন্দ্র', 1),
(7, '4.7', 'Multipurpose buildings', 1, NULL, NULL, 'বহুমূখী ভবন', 1),
(8, '4.8', 'Bus or launch terminal', 1, NULL, NULL, 'বাস বা লঞ্চ টার্মিনাল', 1),
(9, '4.9', 'Market', 1, NULL, NULL, 'মার্কেট', 1),
(10, '4.10', 'Other (if any)', 1, NULL, NULL, 'অন্যান্য (যদি থাকে)', 1),
(11, '5.1.1', 'Manpower of different offices of LGED (1st class officer)', 1, NULL, NULL, 'এলজিইডি\'র বিভিন্ন দপ্তরের জনবল (১ম শ্রেণীর কর্মকর্তা)', 2),
(12, '5.1.1', 'Manpower of different offices of LGED (2nd class officer)', 1, NULL, NULL, 'এলজিইডি\'র বিভিন্ন দপ্তরের জনবল (২য়  শ্রেণীর কর্মকর্তা)', 2),
(13, '5.1.1', 'Manpower of different offices of LGED (3rd Class employee)', 1, NULL, NULL, 'এলজিইডি\'র বিভিন্ন দপ্তরের জনবল (৩য়  শ্রেণীর কর্মচারী)', 2),
(14, '5.1.1', 'Manpower of different offices of LGED (4th Class Employee)', 1, NULL, NULL, 'এলজিইডি\'র বিভিন্ন দপ্তরের জনবল (৪র্থ  শ্রেণীর কর্মচারী)', 2),
(15, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Road)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - সড়ক)', 2),
(16, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Bridge/Culvert)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - ব্রিজ/কালভার্ট)', 2),
(17, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Office building)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - অফিস ভবন)', 2),
(18, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Upazila complex)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - উপজেলা কমপ্লেক্স)', 2),
(19, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Union parishad complex)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - ইউনিয়ন পরিষদ কমপ্লেক্স)', 2),
(20, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Market)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - মার্কেট)', 2),
(21, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Cyclone shelter center)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - ঘূর্ণিঝড় আশ্রয় কেন্দ্র)', 2),
(22, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Community Center)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - কমিউনিটি সেন্টার)', 2),
(23, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - School building)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - বিদ্যালয় ভবন)', 2),
(24, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Bus terminal)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - বাস টার্মিনাল)', 2),
(25, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Lunch terminal)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - লঞ্চ টার্মিনাল)', 2),
(26, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Public Toilet)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - গণ শৌচাগার)', 2),
(27, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Auditorium)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - অডিটরিয়াম)', 2),
(28, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Soil cut / road maintenance)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - মাটি কাটা / সড়ক রক্ষনাবেক্ষণ)', 2),
(29, '5.1.2', 'Employment generated through development activities (Employment in construction work through contractors - Other (If any))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (ঠিকাদারের মাধ্যমে নির্মাণ কাজে সৃষ্ট কর্মসংস্থান - অন্যান্য (যদি থাকে))', 2),
(30, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Contract labor party (LCS))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - চুক্তিবদ্ধ শ্রমিক দল (এলসিএস))', 2),
(31, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Project implementation committee (PIC))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - প্রকল্প বাস্তবায়ন কমিটি (পিআইসি))', 2),
(32, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Water resources management co-operative society (Prabas))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - পানি সম্পদ ব্যবস্থাপনা সমবায় সমিতি (পাবসস))', 2),
(33, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - The PUBS preservation committee)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - পাবসস রক্ষ্ণাবেক্ষণ কমিটি)', 2),
(34, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Scheme implementation committee (SEC))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - স্কীম বাস্তবায়ন কমিটি (এসাইসি))', 2),
(35, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Community development committee (CDC))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - কমিউনিটি ডেভেলপমেন্ট কমিটি (সিডিসি))', 2),
(36, '', 'Employment generated through development activities (Employment generated by the organized group - CDC Clusters)', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - সিডিসি ক্লাষ্টার)', 2),
(37, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Ward level committee (WLCC))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - ওয়ার্ড লেভেল কমিটি (ডব্লিউএলসিসি))', 2),
(38, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Town level committee (TLCC))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - টাউন লেভেল কমিটি (টিএলসিসি))', 2),
(39, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Community based institution (CBO))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - কমিউনিটি ভিত্তিক প্রতিষ্ঠান (সিবিও))', 2),
(40, '5.1.2', 'Employment generated through development activities (Employment generated by the organized group - Other (if any))', 1, NULL, NULL, 'উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থান (সংঘবদ্ধ দলের মাধ্যমে বাস্তবায়িত কাজের সৃষ্ট কর্মসংস্থান - অন্যান্য (যদি থাকে))', 2),
(41, '5.1.3', 'Self-employment (Cattle / poultry husbendary)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (গবাদি পশু/হাঁস/মুরগী পালন)', 2),
(42, '5.1.3', 'Self-employment (Sewing)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (সেলাই)', 2),
(43, '5.1.3', 'Self-employment (Parlar)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (পার্লার)', 2),
(44, '5.1.3', 'Self-employment (Nursing)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (নার্সিং)', 2),
(45, '5.1.3', 'Self-employment (Fishery farming)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (মৎস চাষ)', 2),
(46, '5.1.3', 'Self-employment (Vegetables cultivation)', 1, NULL, NULL, 'আত্মকর্মসংস্থান (শাকশব্জি চাষ)', 2),
(47, '5.1.3', 'Self-employment (Handcraft (Block / Batik))', 1, NULL, NULL, 'আত্মকর্মসংস্থান (হস্ত শিল্প (ব্লক/বাটিক))', 2),
(48, '5.1.3', 'Self-employment (Other (if any))', 1, NULL, NULL, 'আত্মকর্মসংস্থান (অন্যান্য (যদি থাকে))', 2),
(49, '5.2.1', 'Work environment (Supporting facilities in the official field - Assistance in the headquarter level)', 1, NULL, NULL, 'কর্মপরিবেশ ( দাপ্তরিক ক্ষেত্রের সহায়ক সুবিধা - সদর দপ্ত্রর পর্যায়ে সহায়ক সুবিধা)', 2),
(50, '5.2.1', 'Work environment (Supporting facilities in the official field - Assistance in the departmental level)', 1, NULL, NULL, 'কর্মপরিবেশ ( দাপ্তরিক ক্ষেত্রের সহায়ক সুবিধা - বিভাগীয় পর্যায়ে সহায়ক সুবিধা)', 2),
(51, '5.2.1', 'Work environment (Supporting facilities in the official field - Assistance in the region level)', 1, NULL, NULL, 'কর্মপরিবেশ ( দাপ্তরিক ক্ষেত্রের সহায়ক সুবিধা - অঞ্চল পর্যায়ে সহায়ক সুবিধা)', 2),
(52, '5.2.1', 'Work environment (Supporting facilities in the official field - Assistance at district level)', 1, NULL, NULL, 'কর্মপরিবেশ ( দাপ্তরিক ক্ষেত্রের সহায়ক সুবিধা - জেলা পর্যায়ে সহায়ক সুবিধা)', 2),
(53, '5.2.1', 'Work environment (Supporting facilities in the official field - Assistant at upazila level)', 1, NULL, NULL, 'কর্মপরিবেশ ( দাপ্তরিক ক্ষেত্রের সহায়ক সুবিধা - উপজেলা পর্যায়ে সহায়ক সুবিধা)', 2),
(54, '5.2.1', 'Work environment (Assistant at employment generated through development activities - Through the contractor)', 1, NULL, NULL, 'কর্মপরিবেশ ( উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থানের ক্ষেত্রে প্রদেয় সহায়ক সুবিধা - ঠিকাদারে মাধ্যমে)', 2),
(55, '5.2.1', 'Work environment (Assistant at employment generated through development activities - Through the contracted labor party)', 1, NULL, NULL, 'কর্মপরিবেশ ( উন্নয়ন কর্মকান্ডের মাধ্যমে সৃষ্ট কর্মসংস্থানের ক্ষেত্রে প্রদেয় সহায়ক সুবিধা - চুক্তিবদ্ধ শ্রমিক দলের মাধ্যমে)', 2),
(56, '5.2.2', 'Internal monitoring and resolution committee for physical and mental torture at workplace', 1, NULL, NULL, 'কর্মক্ষেত্রে শারিরীক ও মানসিক নির্যাতনের জন্য অভ্যন্তরীণ পর্যবেক্ষণ ও নিরসন কমিটি IRRC', 2),
(57, '6.1', 'Skills enhancing training', 1, NULL, NULL, 'দক্ষতা বৃদ্ধিমূলক প্রশিক্ষণ', 3),
(58, '6.2', 'Employment training (Self-employed training)', 1, NULL, NULL, 'কর্মসংস্থানমূলক প্রশিক্ষণ ( আত্মকর্মসংস্থান মূলক প্রশিক্ষণ)', 3),
(59, '6.2', 'Employment training (Training of trainee)', 1, NULL, NULL, 'কর্মসংস্থানমূলক প্রশিক্ষণ (শিক্ষানবীস প্রশিক্ষণ)', 3),
(60, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Man power employment committee meeting)', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (জন শক্তি নিয়োগ কমিটির সভা)', 4),
(61, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Promotion committee meeting)', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (পদোন্নতি কমিটির সভা)', 4),
(62, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Offer Assessment Committee meeting)', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (প্রস্তাব মূল্যায়ন কমিটির সভা)', 4),
(63, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Gender Development Forum / Committee Meeting)', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (জেন্ডার উন্নয়ন ফোরাম/কমিটির সভা)', 4),
(64, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Successful recruitment committee meeting)', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (সুফলভোগী নিয়োগ কমিটির সভা)\r\n', 4),
(65, '7.1', 'The number of decisions taken at the level of LGED officials / employees (Other (if any))', 1, NULL, NULL, 'এলজিইডির কর্মকর্তা/কর্মচারীদের পর্যায়ে সিদ্ধান্ত গ্রহনের সংখ্যা (অন্যান্য (যদি থাকে))\r\n', 4),
(66, '7.2', 'The number of participants taking part in the decision-making stage (LCS meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (এলসিএস এর সভা)', 4),
(67, '7.2', 'The number of participants taking part in the decision-making stage (Bill Users\' Meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (বিল ব্যবহারকারীর সভা)', 4),
(68, '7.2', 'The number of participants taking part in the decision-making stage (Debt committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (ঋণ কমিটির সভা)', 4),
(69, '7.2', 'The number of participants taking part in the decision-making stage (Market development committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (বাজার উন্নয়ন কমিটির সভা)', 4),
(70, '7.2', 'The number of participants taking part in the decision-making stage (Pabus\'s management committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (পাবসস ব্যবস্থাপনা কমিটির সভা)', 4),
(71, '7.2', 'The number of participants taking part in the decision-making stage (Pabus\'s management sub-committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (পাবসস ব্যবস্থাপনা উপ-কমিটির সভা)', 4),
(72, '7.2', 'The number of participants taking part in the decision-making stage (Urban development committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (নগর উন্নয়ন কমিটির সভা)', 4),
(73, '7.2', 'The number of participants taking part in the decision-making stage (City coordination committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (নগর সমন্বয় কমিটির সভা)', 4),
(74, '7.2', 'The number of participants taking part in the decision-making stage (Women and child abuse, dowry, acid throwing and child marriage prevention committee meeting)', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (নারী ও শিশু নির্যাতন, যৌতুক, এসিড নিক্ষেপ ও বাল্য বিবাহ প্রতিরোধ কমিটির সভা)', 4),
(75, '7.2', 'The number of participants taking part in the decision-making stage (Community Development Committee (CDC))', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (কমিউনিটি ডেভেলপমেন্ট কমিটি (সিডিসি))', 4),
(76, '7.2', 'The number of participants taking part in the decision-making stage (Other (if any))', 1, NULL, NULL, 'সুফলভোগী পর্যায়ে সিদ্ধান্ত গ্রহনে অংশ গ্রহনের সংখ্যা (অন্যান্য (যদি থাকে))', 4),
(77, '8.1', 'Headquarters level', 1, NULL, NULL, 'সদর দপ্তর পর্যায়ে', 5),
(78, '8.2', 'Division level', 1, NULL, NULL, 'বিভাগ পর্যায়ে', 5),
(79, '8.3', 'Zone level', 1, NULL, NULL, 'অঞ্চল পর্যায়ে', 5),
(80, '8.4', 'District level', 1, NULL, NULL, 'জেলা পর্যায়ে', 5),
(81, '8.5', 'Upazila level', 1, NULL, NULL, 'উপজেলা পর্যায়ে', 5),
(82, '9.1', 'According to the budget area allocated in gender related matters', 1, NULL, NULL, 'জেন্ডার সংক্রান্ত বিষয়ে বরাদ্দকৃত বাজেট ক্ষেত্র অনুযায়ী', 6),
(83, '9.2', 'Money spent on gender related training', 1, NULL, NULL, 'জেন্ডার সংক্রান্ত বিষয়ে প্রশিক্ষনের জন্য ব্যয়িত অর্থ', 6),
(84, '9.3', 'The amount of money spent on all training', 1, NULL, NULL, 'সকল প্রশিক্ষনে ব্যয়িত অর্থের পরিমান', 6),
(85, '9.4', 'The money spent on the maintenance of the workers in the maintenance sector', 1, NULL, NULL, 'রক্ষণাবেক্ষণ খাতে শ্রমিকের অনুকূলে ব্যয়িত অর্থ', 6),
(86, '9.5', 'Budget allocated for survey or evaluation', 1, NULL, NULL, 'সমীক্ষা বা মূল্যায়নের জন্য বরাদ্দকৃত বাজেট', 6),
(87, '9.6', 'Budget allocated for institutionalization', 1, NULL, NULL, 'প্রতিষ্ঠানিকরনের জন্য বরাদ্দকৃত বাজেট', 6),
(88, '9.7', 'Other (if any)', 1, NULL, NULL, 'অন্যান্য (যদি থাকে)', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mainareas`
--

DROP TABLE IF EXISTS `mainareas`;
CREATE TABLE `mainareas` (
  `id` int(10) UNSIGNED NOT NULL,
  `mainarea_en` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Main Area name in English',
  `mainarea_bn` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Main Area name in Bangla',
  `status` tinyint(1) NOT NULL COMMENT '1 means active 0 means inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mainareas`
--

INSERT INTO `mainareas` (`id`, `mainarea_en`, `mainarea_bn`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Infrastructure development', 'অবকাঠামো উন্নয়ন', 1, NULL, NULL),
(2, 'Employment and Environment', 'কর্মসংস্থান ও পরিবেশ', 1, NULL, NULL),
(3, 'Training', 'প্রশিক্ষণ', 1, NULL, NULL),
(4, 'Participation', 'অংশ গ্রহণ', 1, NULL, NULL),
(5, 'Empowerment', 'ক্ষমতায়ন', 1, NULL, NULL),
(6, 'Financing', 'অর্থায়ন', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_indicators`
--

DROP TABLE IF EXISTS `sub_indicators`;
CREATE TABLE `sub_indicators` (
  `id` int(10) UNSIGNED NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `serial` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_title_en` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_title_bn` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_status` tinyint(1) NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_indicators`
--

INSERT INTO `sub_indicators` (`id`, `indicator_id`, `serial`, `sub_indicator_title_en`, `sub_indicator_title_bn`, `sub_indicator_status`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(2, 1, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(3, 1, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(4, 1, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(5, 2, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(6, 2, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(7, 2, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(8, 2, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(9, 3, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(10, 3, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(11, 3, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(12, 3, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(13, 4, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(14, 4, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(15, 4, '', 'Private Corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(16, 4, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(17, 5, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(18, 5, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(19, 5, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(20, 5, '', 'Private corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(21, 5, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(22, 6, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(23, 6, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(24, 6, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(25, 6, '', 'Private corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(26, 6, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(27, 7, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(28, 7, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(29, 7, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(30, 7, '', 'Private corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(31, 7, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(32, 8, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(33, 8, '', 'Ticket Counter', 'টিকিট কাউন্টার', 1, 0, NULL, NULL),
(34, 8, '', 'Restroom', 'বিশ্রামাগার', 1, 0, NULL, NULL),
(35, 8, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(36, 8, '', 'Private corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(37, 8, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(38, 9, '', 'Toilets', 'শৌঁচাগার (টয়লেট)', 1, 0, NULL, NULL),
(39, 9, '', 'Place of prayer', 'প্রার্থনার স্থান', 1, 0, NULL, NULL),
(40, 9, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(41, 9, '', 'Private corner', 'প্রাইভেট কর্ণার', 1, 0, NULL, NULL),
(42, 9, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(43, 10, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(44, 11, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(45, 11, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(46, 12, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(47, 12, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(48, 13, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(49, 13, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(50, 14, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(51, 14, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(52, 15, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(53, 16, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(54, 17, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(55, 18, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(56, 19, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(57, 20, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(58, 21, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(59, 22, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(60, 23, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(61, 24, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(62, 25, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(63, 26, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(64, 27, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(65, 28, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(66, 29, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(67, 30, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(68, 31, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(69, 32, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(70, 33, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(71, 34, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(72, 35, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(73, 36, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(74, 37, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(75, 38, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(76, 39, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(77, 40, '', 'Employment', 'কর্মসংস্থান', 1, 0, NULL, NULL),
(78, 41, '', 'Savings (money)', 'সঞ্চয় (টাকা)', 1, 0, NULL, NULL),
(79, 42, '', 'Storekeeper (number)', 'সঞ্চয়কারীর (সংখ্যা)', 1, 0, NULL, NULL),
(80, 43, '', 'Donation amount (money)', 'অনুদানের পরিমান (টাকা)', 1, 0, NULL, NULL),
(81, 44, '', 'Number of donor recipients', 'অনুদান গ্রহনকারীর সংখ্যা', 1, 0, NULL, NULL),
(82, 45, '', 'Microcredit amount (money)', 'ক্ষুদ্র ঋনের পরিমান (টাকা)', 1, 0, NULL, NULL),
(83, 46, '', 'Micro credit holder', 'ক্ষুদ্র ঋন গ্রহিতা', 1, 0, NULL, NULL),
(84, 47, '', 'Number of donor recipients', 'অনুদান গ্রহনকারীর সংখ্যা', 1, 0, NULL, NULL),
(85, 48, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(86, 49, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(87, 49, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(88, 49, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(89, 49, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(90, 49, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(91, 49, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(92, 49, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(93, 50, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(94, 50, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(95, 50, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(96, 50, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(97, 50, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(98, 50, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(99, 50, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(100, 51, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(101, 51, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(102, 51, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(103, 51, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(104, 51, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(105, 51, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(106, 51, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(107, 52, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(108, 52, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(109, 52, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(110, 52, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(111, 52, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(112, 52, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(113, 52, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(114, 53, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(115, 53, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(116, 53, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(117, 53, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(118, 53, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(119, 53, '', 'Children day care center', 'শিশু দিবাযত্ন কেন্দ্র', 1, 0, NULL, NULL),
(120, 53, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(121, 54, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(122, 54, '', 'Prayer arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(123, 54, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(124, 54, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(125, 54, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(126, 54, '', 'Child care system', 'শিশু যত্নের ব্যবস্থা', 1, 0, NULL, NULL),
(127, 54, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(128, 55, '', 'Toilets', 'শৌঁচাগার', 1, 0, NULL, NULL),
(129, 55, '', 'Prayer Arrangement', 'প্রার্থনার ব্যবস্থা', 1, 0, NULL, NULL),
(130, 55, '', 'Residential arrangement', 'আবাসিক ব্যবস্থা', 1, 0, NULL, NULL),
(131, 55, '', 'Transport (car)', 'যাতায়াত (গাড়ী)', 1, 0, NULL, NULL),
(132, 55, '', 'First aid', 'প্রাথমিক চিকিৎসা ব্যবস্থা', 1, 0, NULL, NULL),
(133, 55, '', 'Child care system', 'শিশু যত্নের ব্যবস্থা', 1, 0, NULL, NULL),
(134, 55, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(135, 56, '', 'IRRC Committee member number', 'IRRC কমিটির সদস্য সংখ্যা', 1, 0, NULL, NULL),
(136, 56, '', 'IRRC complaints number received in the committee', 'IRRC কমিটিতে প্রাপ্ত অভিযোগ সংখ্যা', 1, 0, NULL, NULL),
(137, 56, '', 'IRRC number of complaints and counseling received in the committee', 'IRRC কমিটিতে প্রাপ্ত অভিযোগ নিস্পত্তি ও কাউন্সিলিং এর সংখ্যা', 1, 0, NULL, NULL),
(138, 57, '', 'Higher degree', 'উচ্চতর ডিগ্রী', 1, 0, NULL, NULL),
(139, 57, '', 'Technical training', 'কারিগরী প্রশিক্ষণ', 1, 0, NULL, NULL),
(140, 57, '', 'ICT / computer training', 'আইসিটি/কম্পিউটার বিষয়ক প্রশিক্ষণ', 1, 0, NULL, NULL),
(141, 57, '', 'Financial management training', 'আর্থিক ব্যবস্থাপনা প্রশিক্ষণ', 1, 0, NULL, NULL),
(142, 57, '', 'General management training', 'সাধারণ ব্যবস্থাপনা প্রশিক্ষণ', 1, 0, NULL, NULL),
(143, 57, '', 'Gender and developmental training', 'জেন্ডার ও উন্নয়ন বিষয়ক প্রশিক্ষণ', 1, 0, NULL, NULL),
(144, 57, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(145, 58, '', 'Cattle / poultry husbendary', 'গবাদী পশু/হাঁস/মুরগী পালন প্রশিক্ষণ', 1, 0, NULL, NULL),
(146, 58, '', 'Sewing training', 'সেলাই প্রশিক্ষণ', 1, 0, NULL, NULL),
(147, 58, '', 'Parlor training', 'পার্লার প্রশিক্ষণ', 1, 0, NULL, NULL),
(148, 58, '', 'Nursing training', 'নার্সিং প্রশিক্ষণ', 1, 0, NULL, NULL),
(149, 58, '', 'Fisheries farming training', 'মৎস্য চাষ প্রশিক্ষণ', 1, 0, NULL, NULL),
(150, 58, '', 'Vegetables farming training', 'শাকসবজি চাষ প্রশিক্ষণ', 1, 0, NULL, NULL),
(151, 58, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(152, 59, '', 'Sewing training', 'সেলাই প্রশিক্ষণ', 1, 0, NULL, NULL),
(153, 59, '', 'Parlor training', 'পার্লার প্রশিক্ষণ', 1, 0, NULL, NULL),
(154, 59, '', 'Nursing training', 'নার্সিং প্রশিক্ষণ', 1, 0, NULL, NULL),
(155, 59, '', 'Factory training', 'কারখানায় প্রশিক্ষণ', 1, 0, NULL, NULL),
(156, 59, '', 'Other (if any)', 'অন্যান্য (যদি থাকে)', 1, 0, NULL, NULL),
(157, 77, '', '(Head of the Office Revenue)', '(অফিস প্রধান রাজস্ব)', 1, 0, NULL, NULL),
(158, 77, '', '(Head of the Office Development)', '(অফিস প্রধান উন্নয়ন)', 1, 0, NULL, NULL),
(159, 78, '', '(Head of the Office Revenue)', '(অফিস প্রধান রাজস্ব)', 1, 0, NULL, NULL),
(160, 78, '', '(Head of the Office Development)', '(অফিস প্রধান উন্নয়ন)', 1, 0, NULL, NULL),
(161, 79, '', '(Head of the Office Revenue)', '(অফিস প্রধান রাজস্ব)', 1, 0, NULL, NULL),
(162, 79, '', '(Head of the Office Development)', '(অফিস প্রধান উন্নয়ন)', 1, 0, NULL, NULL),
(163, 80, '', '(Head of the Office Revenue)', '(অফিস প্রধান রাজস্ব)', 1, 0, NULL, NULL),
(164, 80, '', '(Head of the Office Development)', '(অফিস প্রধান উন্নয়ন)', 1, 0, NULL, NULL),
(165, 81, '', '(Head of the Office Revenue)', '(অফিস প্রধান রাজস্ব)', 1, 0, NULL, NULL),
(166, 81, '', '(Head of the Office Development)', '(অফিস প্রধান উন্নয়ন)', 1, 0, NULL, NULL),
(167, 82, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(168, 82, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(169, 83, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(170, 83, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(171, 84, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(172, 84, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(173, 85, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(174, 85, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(175, 86, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(176, 86, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(177, 87, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(178, 87, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL),
(179, 88, '', 'Revenue', 'রাজস্ব', 1, 0, NULL, NULL),
(180, 88, '', 'Development', 'উন্নয়ন', 1, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `indicators`
--
ALTER TABLE `indicators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainareas`
--
ALTER TABLE `mainareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_indicators`
--
ALTER TABLE `sub_indicators`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `indicators`
--
ALTER TABLE `indicators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `mainareas`
--
ALTER TABLE `mainareas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sub_indicators`
--
ALTER TABLE `sub_indicators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
