-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2017 at 10:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_datas`
--

CREATE TABLE `application_datas` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `main_area_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `sub_indicator_id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `woman` int(11) DEFAULT NULL,
  `man` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `percent` decimal(10,2) DEFAULT NULL,
  `remarks` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application_datas`
--

INSERT INTO `application_datas` (`id`, `application_id`, `upazila_id`, `district_id`, `main_area_id`, `indicator_id`, `sub_indicator_id`, `amount`, `woman`, `man`, `total`, `percent`, `remarks`, `created_at`, `updated_at`) VALUES
(59, 17, 286, 57, 1, 1, 1, 3, 3, 3, 6, '50.00', NULL, '2017-10-05 20:28:46', '2017-10-05 20:30:23'),
(60, 17, 286, 57, 1, 1, 2, 1, 3, 3, 6, '50.00', NULL, '2017-10-05 20:32:46', '2017-10-05 20:32:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application_datas`
--
ALTER TABLE `application_datas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application_datas`
--
ALTER TABLE `application_datas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
