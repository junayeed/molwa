-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2017 at 11:54 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_bangla` varchar(255) DEFAULT NULL,
  `month` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `district_id`, `upazila_id`, `name`, `name_bangla`, `month`, `year`, `status`, `date`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 'Application 2017 January-June', 'Application 2017 January-June Bangla', '1', 2017, '1', '2017-09-20', '2017-09-19 12:44:59', '2017-09-19 19:17:26'),
(2, 0, 0, 'Application 2017July-December', 'Application 2017July-December Bangla', '2', 2017, '1', '2017-09-20', '2017-09-19 12:48:24', '2017-09-19 18:55:19'),
(3, 0, 0, 'Application 2016 January-June', 'Application 2016 January-June BN', '1', 2016, '1', '2017-09-19', '2017-09-19 12:56:53', '2017-09-19 12:56:53'),
(5, 0, 0, 'Application 2017 January-June--', 'Application 2017 January-June Bangla', '1', 2017, '1', '2017-09-20', '2017-09-19 19:02:37', '2017-09-19 19:02:37'),
(6, NULL, NULL, 'Application 2017 January-Juneasdf', 'Application 2017 January-June Bangla', '1', 2017, '1', NULL, '2017-09-19 19:25:15', '2017-09-19 19:25:15'),
(8, 2, 151, 'Application 2017 January-Jun', 'Application 2017 January-June Bangla', '1', 2017, '1', '2017-09-20', '2017-09-19 19:26:51', '2017-09-19 19:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `application_datas`
--

CREATE TABLE `application_datas` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `upazila_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `sub_indicator_id` int(11) NOT NULL,
  `amount_in_indicator` int(11) DEFAULT NULL,
  `woman` int(11) DEFAULT NULL,
  `man` int(11) DEFAULT NULL,
  `remarks` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application_datas`
--

INSERT INTO `application_datas` (`id`, `application_id`, `upazila_id`, `district_id`, `sub_indicator_id`, `amount_in_indicator`, `woman`, `man`, `remarks`, `created_at`, `updated_at`) VALUES
(3, 1, 189, 6, 1, 1, 2, 3, '6', '2017-09-21 21:50:53', '2017-09-21 21:50:53'),
(4, 1, 189, 6, 2, 7, 8, 9, '12', '2017-09-21 21:50:53', '2017-09-21 21:50:53');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `district_tbl`
--

CREATE TABLE `district_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `dis_division_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dis_bn_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dis_lat` double(15,8) NOT NULL,
  `dis_lon` double(15,8) NOT NULL,
  `dis_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district_tbl`
--

INSERT INTO `district_tbl` (`id`, `dis_division_id`, `name`, `dis_bn_name`, `dis_lat`, `dis_lon`, `dis_website`, `created_at`, `updated_at`) VALUES
(1, '3', 'Dhaka', 'ঢাকা', 23.71152530, 90.41114510, 'www.dhaka.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(2, '3', 'Faridpur', 'ফরিদপুর', 23.60708220, 89.84294060, 'www.faridpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(3, '3', 'Gazipur', 'গাজীপুর', 24.00228580, 90.42642830, 'www.gazipur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(4, '3', 'Gopalganj', 'গোপালগঞ্জ', 23.00508570, 89.82660590, 'www.gopalganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(5, '3', 'Jamalpur', 'জামালপুর', 24.93753300, 89.93777500, 'www.jamalpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(6, '3', 'Kishoreganj', 'কিশোরগঞ্জ', 24.44493700, 90.77657500, 'www.kishoreganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(7, '3', 'Madaripur', 'মাদারীপুর', 23.16410200, 90.18968050, 'www.madaripur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(8, '3', 'Manikganj', 'মানিকগঞ্জ', 0.00000000, 0.00000000, 'www.manikganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(9, '3', 'Munshiganj', 'মুন্সিগঞ্জ', 0.00000000, 0.00000000, 'www.munshiganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(10, '3', 'Mymensingh', 'ময়মনসিং', 0.00000000, 0.00000000, 'www.mymensingh.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(11, '3', 'Narayanganj', 'নারায়াণগঞ্জ', 23.63366000, 90.49648200, 'www.narayanganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(12, '3', 'Narsingdi', 'নরসিংদী', 23.93223300, 90.71541000, 'www.narsingdi.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(13, '3', 'Netrokona', 'নেত্রকোনা', 24.87095500, 90.72788700, 'www.netrokona.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(14, '3', 'Rajbari', 'রাজবাড়ি', 23.75743050, 89.64446650, 'www.rajbari.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(15, '3', 'Shariatpur', 'শরীয়তপুর', 0.00000000, 0.00000000, 'www.shariatpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(16, '3', 'Sherpur', 'শেরপুর', 25.02049330, 90.01529660, 'www.sherpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(17, '3', 'Tangail', 'টাঙ্গাইল', 0.00000000, 0.00000000, 'www.tangail.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(18, '5', 'Bogra', 'বগুড়া', 24.84652280, 89.37775500, 'www.bogra.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(19, '5', 'Joypurhat', 'জয়পুরহাট', 0.00000000, 0.00000000, 'www.joypurhat.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(20, '5', 'Naogaon', 'নওগাঁ', 0.00000000, 0.00000000, 'www.naogaon.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(21, '5', 'Natore', 'নাটোর', 24.42055600, 89.00028200, 'www.natore.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(22, '5', 'Nawabganj', 'নবাবগঞ্জ', 24.59650340, 88.27751220, 'www.chapainawabganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(23, '5', 'Pabna', 'পাবনা', 23.99852400, 89.23364500, 'www.pabna.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(24, '5', 'Rajshahi', 'রাজশাহী', 0.00000000, 0.00000000, 'www.rajshahi.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(25, '5', 'Sirajgonj', 'সিরাজগঞ্জ', 24.45339780, 89.70068150, 'www.sirajganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(26, '6', 'Dinajpur', 'দিনাজপুর', 25.62170610, 88.63545040, 'www.dinajpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(27, '6', 'Gaibandha', 'গাইবান্ধা', 25.32875100, 89.52808800, 'www.gaibandha.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(28, '6', 'Kurigram', 'কুড়িগ্রাম', 25.80544500, 89.63617400, 'www.kurigram.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(29, '6', 'Lalmonirhat', 'লালমনিরহাট', 0.00000000, 0.00000000, 'www.lalmonirhat.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(30, '6', 'Nilphamari', 'নীলফামারী', 25.93179400, 88.85600600, 'www.nilphamari.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(31, '6', 'Panchagarh', 'পঞ্চগড়', 26.34110000, 88.55416060, 'www.panchagarh.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(32, '6', 'Rangpur', 'রংপুর', 25.75580960, 89.24446200, 'www.rangpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(33, '6', 'Thakurgaon', 'ঠাকুরগাঁও', 26.03369450, 88.46168340, 'www.thakurgaon.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(34, '1', 'Barguna', 'বরগুনা', 0.00000000, 0.00000000, 'www.barguna.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(35, '1', 'Barisal', 'বরিশাল', 0.00000000, 0.00000000, 'www.barisal.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(36, '1', 'Bhola', 'ভোলা', 22.68592300, 90.64817900, 'www.bhola.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(37, '1', 'Jhalokati', 'ঝালকাঠি', 0.00000000, 0.00000000, 'www.jhalakathi.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(38, '1', 'Patuakhali', 'পটুয়াখালী', 22.35963160, 90.32987120, 'www.patuakhali.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(39, '1', 'Pirojpur', 'পিরোজপুর', 0.00000000, 0.00000000, 'www.pirojpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(40, '2', 'Bandarban', 'বান্দরবান', 22.19532750, 92.21837730, 'www.bandarban.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(41, '2', 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 23.95709040, 91.11192860, 'www.brahmanbaria.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(42, '2', 'Chandpur', 'চাঁদপুর', 23.23325850, 90.67129120, 'www.chandpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(43, '2', 'Chittagong', 'চট্টগ্রাম', 22.33510900, 91.83407300, 'www.chittagong.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(44, '2', 'Comilla', 'কুমিল্লা', 23.46827470, 91.17881350, 'www.comilla.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(45, '2', 'Cox''s Bazar', 'কক্স বাজার', 0.00000000, 0.00000000, 'www.coxsbazar.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(46, '2', 'Feni', 'ফেনী', 23.02323100, 91.38408440, 'www.feni.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(47, '2', 'Khagrachari', 'খাগড়াছড়ি', 23.11928500, 91.98466300, 'www.khagrachhari.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(48, '2', 'Lakshmipur', 'লক্ষ্মীপুর', 22.94247700, 90.84118400, 'www.lakshmipur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(49, '2', 'Noakhali', 'নোয়াখালী', 22.86956300, 91.09939800, 'www.noakhali.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(50, '2', 'Rangamati', 'রাঙ্গামাটি', 0.00000000, 0.00000000, 'www.rangamati.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(51, '7', 'Habiganj', 'হবিগঞ্জ', 24.37494500, 91.41553000, 'www.habiganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(52, '7', 'Maulvibazar', 'মৌলভীবাজার', 24.48293400, 91.77741700, 'www.moulvibazar.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(53, '7', 'Sunamganj', 'সুনামগঞ্জ', 25.06580420, 91.39501150, 'www.sunamganj.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(54, '7', 'Sylhet', 'সিলেট', 24.88979560, 91.86978940, 'www.sylhet.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(55, '4', 'Bagerhat', 'বাগেরহাট', 22.65156800, 89.78593800, 'www.bagerhat.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(56, '4', 'Chuadanga', 'চুয়াডাঙ্গা', 23.64019610, 88.84184100, 'www.chuadanga.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(57, '4', 'Jessore', 'যশোর', 23.16643000, 89.20811260, 'www.jessore.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(58, '4', 'Jhenaidah', 'ঝিনাইদহ', 23.54481760, 89.15392130, 'www.jhenaidah.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(59, '4', 'Khulna', 'খুলনা', 22.81577400, 89.56867900, 'www.khulna.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(60, '4', 'Kushtia', 'কুষ্টিয়া', 23.90125800, 89.12048200, 'www.kushtia.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(61, '4', 'Magura', 'মাগুরা', 23.48733700, 89.41995600, 'www.magura.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(62, '4', 'Meherpur', 'মেহেরপুর', 23.76221300, 88.63182100, 'www.meherpur.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(63, '4', 'Narail', 'নড়াইল', 23.17253400, 89.51267200, 'www.narail.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20'),
(64, '4', 'Satkhira', 'সাতক্ষীরা', 0.00000000, 0.00000000, 'www.satkhira.gov.bd', '2015-09-13 04:33:27', '2015-09-13 04:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `division_tbl`
--

CREATE TABLE `division_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `div_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `div_bn_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `division_tbl`
--

INSERT INTO `division_tbl` (`id`, `div_name`, `div_bn_name`, `created_at`, `updated_at`) VALUES
(1, 'Barisal', 'বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Chittagong', 'চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Dhaka', 'ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Khulna', 'খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Rajshahi', 'রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Rangpur', 'রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Sylhet', 'সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `indicators`
--

CREATE TABLE `indicators` (
  `id` int(10) UNSIGNED NOT NULL,
  `serial` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicator_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `indicator_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `indicator_title_bn` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `indicators`
--

INSERT INTO `indicators` (`id`, `serial`, `indicator_title`, `indicator_status`, `created_at`, `updated_at`, `indicator_title_bn`, `main_area`) VALUES
(1, '1', 'Indicator 1-1', 1, '2017-09-19 11:28:32', '2017-09-19 11:28:32', 'Indicator 1 Bangla', 1),
(2, '2', 'Indicator 1-2', 1, '2017-09-19 11:29:00', '2017-09-19 11:29:00', 'Indicator 2 bangla', 1),
(3, '3', 'Indicator 1-3', 1, '2017-09-19 11:29:23', '2017-09-19 11:29:23', 'Indicator 3 Bangla', 1),
(4, '1', 'Indicator 2-1', 1, '2017-09-19 11:29:48', '2017-09-19 11:29:48', 'Indicator 1 Bangla', 2),
(5, '2', 'Indicator 2-2', 1, '2017-09-19 11:30:19', '2017-09-19 11:30:19', 'Indicator 2 bangla', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mainareas`
--

CREATE TABLE `mainareas` (
  `id` int(10) UNSIGNED NOT NULL,
  `mainarea_en` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Main Area name in English',
  `mainarea_bn` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Main Area name in Bangla',
  `status` tinyint(1) NOT NULL COMMENT '1 means active 0 means inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mainareas`
--

INSERT INTO `mainareas` (`id`, `mainarea_en`, `mainarea_bn`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Main Area', 'Main Area Bangla', 1, '2017-09-19 11:27:13', '2017-09-19 11:27:13'),
(2, 'Main Area 2', 'Main Area 2 bangla', 1, '2017-09-19 11:27:34', '2017-09-19 11:27:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_08_28_181717_create_upazilas_table', 1),
(4, '2017_08_28_182237_create_districts_table', 1),
(5, '2017_08_28_183501_create_user_groups_table', 1),
(6, '2017_08_31_121837_user_group', 1),
(7, '2017_09_11_002210_create_mainareas_table', 1),
(8, '2017_09_12_003920_create_indicators_table', 1),
(9, '2017_09_12_004441_create_sub_indicators_table', 1),
(10, '2017_09_12_061311_add_indicator_title_bn_to_indicators', 1),
(11, '2017_09_16_023000_add_main_area_to_incators', 1),
(12, '2017_09_17_000947_add_serial_to_sub_indicators_table', 1),
(13, '2017_09_17_001651_add_amount_to_sub_indicators_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_indicators`
--

CREATE TABLE `sub_indicators` (
  `id` int(10) UNSIGNED NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `serial` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_title_en` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_title_bn` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_indicator_status` tinyint(1) NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_indicators`
--

INSERT INTO `sub_indicators` (`id`, `indicator_id`, `serial`, `sub_indicator_title_en`, `sub_indicator_title_bn`, `sub_indicator_status`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'Sub Indicator 1', 'Sub Indicator 1 bangla', 1, 4, '2017-09-19 11:31:09', '2017-09-19 11:31:09'),
(2, 1, '2', 'Sub Indicator 2', 'Sub Indicator 2 Bangla', 1, 5, '2017-09-19 11:31:49', '2017-09-19 11:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `upazilas`
--

CREATE TABLE `upazilas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upazila_tbl`
--

CREATE TABLE `upazila_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `upa_district_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upa_bn_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `upazila_tbl`
--

INSERT INTO `upazila_tbl` (`id`, `upa_district_id`, `name`, `upa_bn_name`, `created_at`, `updated_at`) VALUES
(1, '34', 'Amtali Upazila', 'আমতলী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '34', 'Bamna Upazila', 'বামনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '34', 'Barguna Sadar Upazila', 'বরগুনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '34', 'Betagi Upazila', 'বেতাগি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '34', 'Patharghata Upazila', 'পাথরঘাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '34', 'Taltali Upazila', 'তালতলী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '35', 'Muladi Upazila', 'মুলাদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '35', 'Babuganj Upazila', 'বাবুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '35', 'Agailjhara Upazila', 'আগাইলঝরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '35', 'Barisal Sadar Upazila', 'বরিশাল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '35', 'Bakerganj Upazila', 'বাকেরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '35', 'Banaripara Upazila', 'বানাড়িপারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '35', 'Gaurnadi Upazila', 'গৌরনদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '35', 'Hizla Upazila', 'হিজলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '35', 'Mehendiganj Upazila', 'মেহেদিগঞ্জ ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '35', 'Wazirpur Upazila', 'ওয়াজিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '36', 'Bhola Sadar Upazila', 'ভোলা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '36', 'Burhanuddin Upazila', 'বুরহানউদ্দিন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '36', 'Char Fasson Upazila', 'চর ফ্যাশন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '36', 'Daulatkhan Upazila', 'দৌলতখান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '36', 'Lalmohan Upazila', 'লালমোহন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '36', 'Manpura Upazila', 'মনপুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '36', 'Tazumuddin Upazila', 'তাজুমুদ্দিন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '37', 'Jhalokati Sadar Upazila', 'ঝালকাঠি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '37', 'Kathalia Upazila', 'কাঁঠালিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '37', 'Nalchity Upazila', 'নালচিতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '37', 'Rajapur Upazila', 'রাজাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '38', 'Bauphal Upazila', 'বাউফল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '38', 'Dashmina Upazila', 'দশমিনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '38', 'Galachipa Upazila', 'গলাচিপা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '38', 'Kalapara Upazila', 'কালাপারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '38', 'Mirzaganj Upazila', 'মির্জাগঞ্জ ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '38', 'Patuakhali Sadar Upazila', 'পটুয়াখালী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '38', 'Dumki Upazila', 'ডুমকি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '38', 'Rangabali Upazila', 'রাঙ্গাবালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '39', 'Bhandaria', 'ভ্যান্ডারিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '39', 'Kaukhali', 'কাউখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '39', 'Mathbaria', 'মাঠবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '39', 'Nazirpur', 'নাজিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '39', 'Nesarabad', 'নেসারাবাদ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '39', 'Pirojpur Sadar', 'পিরোজপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '39', 'Zianagar', 'জিয়ানগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '40', 'Bandarban Sadar', 'বান্দরবন সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '40', 'Thanchi', 'থানচি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '40', 'Lama', 'লামা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '40', 'Naikhongchhari', 'নাইখংছড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '40', 'Ali kadam', 'আলী কদম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '40', 'Rowangchhari', 'রউয়াংছড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '40', 'Ruma', 'রুমা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '41', 'Brahmanbaria Sadar Upazila', 'ব্রাহ্মণবাড়িয়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '41', 'Ashuganj Upazila', 'আশুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '41', 'Nasirnagar Upazila', 'নাসির নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '41', 'Nabinagar Upazila', 'নবীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '41', 'Sarail Upazila', 'সরাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '41', 'Shahbazpur Town', 'শাহবাজপুর টাউন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '41', 'Kasba Upazila', 'কসবা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '41', 'Akhaura Upazila', 'আখাউরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '41', 'Bancharampur Upazila', 'বাঞ্ছারামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '41', 'Bijoynagar Upazila', 'বিজয় নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '42', 'Chandpur Sadar', 'চাঁদপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '42', 'Faridganj', 'ফরিদগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '42', 'Haimchar', 'হাইমচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '42', 'Haziganj', 'হাজীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '42', 'Kachua', 'কচুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, '42', 'Matlab Uttar', 'মতলব উত্তর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, '42', 'Matlab Dakkhin', 'মতলব দক্ষিণ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, '42', 'Shahrasti', 'শাহরাস্তি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, '43', 'Anwara Upazila', 'আনোয়ারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, '43', 'Banshkhali Upazila', 'বাশখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, '43', 'Boalkhali Upazila', 'বোয়ালখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, '43', 'Chandanaish Upazila', 'চন্দনাইশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, '43', 'Fatikchhari Upazila', 'ফটিকছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, '43', 'Hathazari Upazila', 'হাঠহাজারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, '43', 'Lohagara Upazila', 'লোহাগারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, '43', 'Mirsharai Upazila', 'মিরসরাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, '43', 'Patiya Upazila', 'পটিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, '43', 'Rangunia Upazila', 'রাঙ্গুনিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, '43', 'Raozan Upazila', 'রাউজান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, '43', 'Sandwip Upazila', 'সন্দ্বীপ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, '43', 'Satkania Upazila', 'সাতকানিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, '43', 'Sitakunda Upazila', 'সীতাকুণ্ড', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, '44', 'Barura Upazila', 'বড়ুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, '44', 'Brahmanpara Upazila', 'ব্রাহ্মণপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, '44', 'Burichong Upazila', 'বুড়িচং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, '44', 'Chandina Upazila', 'চান্দিনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, '44', 'Chauddagram Upazila', 'চৌদ্দগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, '44', 'Daudkandi Upazila', 'দাউদকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, '44', 'Debidwar Upazila', 'দেবীদ্বার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, '44', 'Homna Upazila', 'হোমনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, '44', 'Comilla Sadar Upazila', 'কুমিল্লা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, '44', 'Laksam Upazila', 'লাকসাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, '44', 'Monohorgonj Upazila', 'মনোহরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, '44', 'Meghna Upazila', 'মেঘনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, '44', 'Muradnagar Upazila', 'মুরাদনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, '44', 'Nangalkot Upazila', 'নাঙ্গালকোট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, '44', 'Comilla Sadar South Upazila', 'কুমিল্লা সদর দক্ষিণ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, '44', 'Titas Upazila', 'তিতাস', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, '45', 'Chakaria Upazila', 'চকরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, '45', 'Chakaria Upazila', 'চকরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, '45', 'Cox''s Bazar Sadar Upazila', 'কক্স বাজার সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, '45', 'Kutubdia Upazila', 'কুতুবদিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, '45', 'Maheshkhali Upazila', 'মহেশখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, '45', 'Ramu Upazila', 'রামু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, '45', 'Teknaf Upazila', 'টেকনাফ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, '45', 'Ukhia Upazila', 'উখিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, '45', 'Pekua Upazila', 'পেকুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, '46', 'Feni Sadar', 'ফেনী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, '46', 'Chagalnaiya', 'ছাগল নাইয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, '46', 'Daganbhyan', 'দাগানভিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, '46', 'Parshuram', 'পরশুরাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, '46', 'Fhulgazi', 'ফুলগাজি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, '46', 'Sonagazi', 'সোনাগাজি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, '47', 'Dighinala Upazila', 'দিঘিনালা ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, '47', 'Khagrachhari Upazila', 'খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, '47', 'Lakshmichhari Upazila', 'লক্ষ্মীছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, '47', 'Mahalchhari Upazila', 'মহলছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, '47', 'Manikchhari Upazila', 'মানিকছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, '47', 'Matiranga Upazila', 'মাটিরাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, '47', 'Panchhari Upazila', 'পানছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, '47', 'Ramgarh Upazila', 'রামগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, '48', 'Lakshmipur Sadar Upazila', 'লক্ষ্মীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, '48', 'Raipur Upazila', 'রায়পুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, '48', 'Ramganj Upazila', 'রামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, '48', 'Ramgati Upazila', 'রামগতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, '48', 'Komol Nagar Upazila', 'কমল নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, '49', 'Noakhali Sadar Upazila', 'নোয়াখালী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, '49', 'Begumganj Upazila', 'বেগমগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, '49', 'Chatkhil Upazila', 'চাটখিল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, '49', 'Companyganj Upazila', 'কোম্পানীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, '49', 'Shenbag Upazila', 'শেনবাগ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, '49', 'Hatia Upazila', 'হাতিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, '49', 'Kobirhat Upazila', 'কবিরহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, '49', 'Sonaimuri Upazila', 'সোনাইমুরি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, '49', 'Suborno Char Upazila', 'সুবর্ণ চর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, '50', 'Rangamati Sadar Upazila', 'রাঙ্গামাটি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, '50', 'Belaichhari Upazila', 'বেলাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, '50', 'Bagaichhari Upazila', 'বাঘাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, '50', 'Barkal Upazila', 'বরকল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, '50', 'Juraichhari Upazila', 'জুরাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, '50', 'Rajasthali Upazila', 'রাজাস্থলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, '50', 'Kaptai Upazila', 'কাপ্তাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, '50', 'Langadu Upazila', 'লাঙ্গাডু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, '50', 'Nannerchar Upazila', 'নান্নেরচর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, '50', 'Kaukhali Upazila', 'কাউখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, '1', 'Dhamrai Upazila', 'ধামরাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, '1', 'Dohar Upazila', 'দোহার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, '1', 'Keraniganj Upazila', 'কেরানীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, '1', 'Nawabganj Upazila', 'নবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, '1', 'Savar Upazila', 'সাভার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, '2', 'Faridpur Sadar Upazila', 'ফরিদপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, '2', 'Boalmari Upazila', 'বোয়ালমারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, '2', 'Alfadanga Upazila', 'আলফাডাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, '2', 'Madhukhali Upazila', 'মধুখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, '2', 'Bhanga Upazila', 'ভাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, '2', 'Nagarkanda Upazila', 'নগরকান্ড', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, '2', 'Charbhadrasan Upazila', 'চরভদ্রাসন ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, '2', 'Sadarpur Upazila', 'সদরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, '2', 'Shaltha Upazila', 'শালথা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, '3', 'Gazipur Sadar-Joydebpur', 'গাজীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, '3', 'Kaliakior', 'কালিয়াকৈর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, '3', 'Kapasia', 'কাপাসিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, '3', 'Sripur', 'শ্রীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, '3', 'Kaliganj', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, '3', 'Tongi', 'টঙ্গি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, '4', 'Gopalganj Sadar Upazila', 'গোপালগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, '4', 'Kashiani Upazila', 'কাশিয়ানি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, '4', 'Kotalipara Upazila', 'কোটালিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, '4', 'Muksudpur Upazila', 'মুকসুদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, '4', 'Tungipara Upazila', 'টুঙ্গিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, '5', 'Dewanganj Upazila', 'দেওয়ানগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, '5', 'Baksiganj Upazila', 'বকসিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, '5', 'Islampur Upazila', 'ইসলামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, '5', 'Jamalpur Sadar Upazila', 'জামালপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, '5', 'Madarganj Upazila', 'মাদারগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, '5', 'Melandaha Upazila', 'মেলানদাহা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, '5', 'Sarishabari Upazila', 'সরিষাবাড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, '5', 'Narundi Police I.C', 'নারুন্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, '6', 'Astagram Upazila', 'অষ্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, '6', 'Bajitpur Upazila', 'বাজিতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, '6', 'Bhairab Upazila', 'ভৈরব', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, '6', 'Hossainpur Upazila', 'হোসেনপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, '6', 'Itna Upazila', 'ইটনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, '6', 'Karimganj Upazila', 'করিমগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, '6', 'Katiadi Upazila', 'কতিয়াদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, '6', 'Kishoreganj Sadar Upazila', 'কিশোরগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, '6', 'Kuliarchar Upazila', 'কুলিয়ারচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, '6', 'Mithamain Upazila', 'মিঠামাইন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, '6', 'Nikli Upazila', 'নিকলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, '6', 'Pakundia Upazila', 'পাকুন্ডা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, '6', 'Tarail Upazila', 'তাড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, '7', 'Madaripur Sadar', 'মাদারীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, '7', 'Kalkini', 'কালকিনি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, '7', 'Rajoir', 'রাজইর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, '7', 'Shibchar', 'শিবচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, '8', 'Manikganj Sadar Upazila', 'মানিকগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, '8', 'Singair Upazila', 'সিঙ্গাইর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, '8', 'Shibalaya Upazila', 'শিবালয়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, '8', 'Saturia Upazila', 'সাঠুরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, '8', 'Harirampur Upazila', 'হরিরামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, '8', 'Ghior Upazila', 'ঘিওর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, '8', 'Daulatpur Upazila', 'দৌলতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, '9', 'Lohajang Upazila', 'লোহাজং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, '9', 'Sreenagar Upazila', 'শ্রীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, '9', 'Munshiganj Sadar Upazila', 'মুন্সিগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, '9', 'Sirajdikhan Upazila', 'সিরাজদিখান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, '9', 'Tongibari Upazila', 'টঙ্গিবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, '9', 'Gazaria Upazila', 'গজারিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, '10', 'Bhaluka', 'ভালুকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, '10', 'Trishal', 'ত্রিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, '10', 'Haluaghat', 'হালুয়াঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, '10', 'Muktagachha', 'মুক্তাগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, '10', 'Dhobaura', 'ধবারুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, '10', 'Fulbaria', 'ফুলবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, '10', 'Gaffargaon', 'গফরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, '10', 'Gauripur', 'গৌরিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, '10', 'Ishwarganj', 'ঈশ্বরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, '10', 'Mymensingh Sadar', 'ময়মনসিং সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, '10', 'Nandail', 'নন্দাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, '10', 'Phulpur', 'ফুলপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, '11', 'Araihazar Upazila', 'আড়াইহাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, '11', 'Sonargaon Upazila', 'সোনারগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, '11', 'Bandar', 'বান্দার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, '11', 'Naryanganj Sadar Upazila', 'নারায়ানগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, '11', 'Rupganj Upazila', 'রূপগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, '11', 'Siddirgonj Upazila', 'সিদ্ধিরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, '12', 'Belabo Upazila', 'বেলাবো', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, '12', 'Monohardi Upazila', 'মনোহরদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, '12', 'Narsingdi Sadar Upazila', 'নরসিংদী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, '12', 'Palash Upazila', 'পলাশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, '12', 'Raipura Upazila, Narsingdi', 'রায়পুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, '12', 'Shibpur Upazila', 'শিবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, '13', 'Kendua Upazilla', 'কেন্দুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, '13', 'Atpara Upazilla', 'আটপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, '13', 'Barhatta Upazilla', 'বরহাট্টা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, '13', 'Durgapur Upazilla', 'দুর্গাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, '13', 'Kalmakanda Upazilla', 'কলমাকান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, '13', 'Madan Upazilla', 'মদন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, '13', 'Mohanganj Upazilla', 'মোহনগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, '13', 'Netrakona-S Upazilla', 'নেত্রকোনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, '13', 'Purbadhala Upazilla', 'পূর্বধলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, '13', 'Khaliajuri Upazilla', 'খালিয়াজুরি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, '14', 'Baliakandi Upazila', 'বালিয়াকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, '14', 'Goalandaghat Upazila', 'গোয়ালন্দ ঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, '14', 'Pangsha Upazila', 'পাংশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, '14', 'Kalukhali Upazila', 'কালুখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, '14', 'Rajbari Sadar Upazila', 'রাজবাড়ি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, '15', 'Shariatpur Sadar -Palong', 'শরীয়তপুর সদর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, '15', 'Damudya Upazila', 'দামুদিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, '15', 'Naria Upazila', 'নড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, '15', 'Jajira Upazila', 'জাজিরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, '15', 'Bhedarganj Upazila', 'ভেদারগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, '15', 'Gosairhat Upazila', 'গোসাইর হাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, '16', 'Jhenaigati Upazila', 'ঝিনাইগাতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, '16', 'Nakla Upazila', 'নাকলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, '16', 'Nalitabari Upazila', 'নালিতাবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, '16', 'Sherpur Sadar Upazila', 'শেরপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, '16', 'Sreebardi Upazila', 'শ্রীবরদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, '17', 'Tangail Sadar Upazila', 'টাঙ্গাইল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, '17', 'Sakhipur Upazila', 'সখিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, '17', 'Basail Upazila', 'বসাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, '17', 'Madhupur Upazila', 'মধুপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, '17', 'Ghatail Upazila', 'ঘাটাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, '17', 'Kalihati Upazila', 'কালিহাতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, '17', 'Nagarpur Upazila', 'নগরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, '17', 'Mirzapur Upazila', 'মির্জাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, '17', 'Gopalpur Upazila', 'গোপালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, '17', 'Delduar Upazila', 'দেলদুয়ার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, '17', 'Bhuapur Upazila', 'ভুয়াপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, '17', 'Dhanbari Upazila', 'ধানবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, '55', 'Bagerhat Sadar Upazila', 'বাগেরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, '55', 'Chitalmari Upazila', 'চিতলমাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, '55', 'Fakirhat Upazila', 'ফকিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, '55', 'Kachua Upazila', 'কচুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, '55', 'Mollahat Upazila', 'মোল্লাহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, '55', 'Mongla Upazila', 'মংলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, '55', 'Morrelganj Upazila', 'মরেলগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, '55', 'Rampal Upazila', 'রামপাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, '55', 'Sarankhola Upazila', 'স্মরণখোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, '56', 'Damurhuda Upazila', 'দামুরহুদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, '56', 'Chuadanga-S Upazila', 'চুয়াডাঙ্গা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, '56', 'Jibannagar Upazila', 'জীবন নগর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, '56', 'Alamdanga Upazila', 'আলমডাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, '57', 'Abhaynagar Upazila', 'অভয়নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, '57', 'Keshabpur Upazila', 'কেশবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, '57', 'Bagherpara Upazila', 'বাঘের পাড়া ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, '57', 'Jessore Sadar Upazila', 'যশোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, '57', 'Chaugachha Upazila', 'চৌগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, '57', 'Manirampur Upazila', 'মনিরামপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, '57', 'Jhikargachha Upazila', 'ঝিকরগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, '57', 'Sharsha Upazila', 'সারশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, '58', 'Jhenaidah Sadar Upazila', 'ঝিনাইদহ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, '58', 'Maheshpur Upazila', 'মহেশপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, '58', 'Kaliganj Upazila', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, '58', 'Kotchandpur Upazila', 'কোট চাঁদপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, '58', 'Shailkupa Upazila', 'শৈলকুপা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, '58', 'Harinakunda Upazila', 'হাড়িনাকুন্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, '59', 'Terokhada Upazila', 'তেরোখাদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, '59', 'Batiaghata Upazila', 'বাটিয়াঘাটা ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, '59', 'Dacope Upazila', 'ডাকপে', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, '59', 'Dumuria Upazila', 'ডুমুরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, '59', 'Dighalia Upazila', 'দিঘলিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, '59', 'Koyra Upazila', 'কয়ড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, '59', 'Paikgachha Upazila', 'পাইকগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, '59', 'Phultala Upazila', 'ফুলতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, '59', 'Rupsa Upazila', 'রূপসা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, '60', 'Kushtia Sadar', 'কুষ্টিয়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, '60', 'Kumarkhali', 'কুমারখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, '60', 'Daulatpur', 'দৌলতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, '60', 'Mirpur', 'মিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, '60', 'Bheramara', 'ভেরামারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, '60', 'Khoksa', 'খোকসা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, '61', 'Magura Sadar Upazila', 'মাগুরা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, '61', 'Mohammadpur Upazila', 'মোহাম্মাদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, '61', 'Shalikha Upazila', 'শালিখা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, '61', 'Sreepur Upazila', 'শ্রীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, '62', 'angni Upazila', 'আংনি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, '62', 'Mujib Nagar Upazila', 'মুজিব নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, '62', 'Meherpur-S Upazila', 'মেহেরপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, '63', 'Narail-S Upazilla', 'নড়াইল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, '63', 'Lohagara Upazilla', 'লোহাগাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, '63', 'Kalia Upazilla', 'কালিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, '64', 'Satkhira Sadar Upazila', 'সাতক্ষীরা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, '64', 'Assasuni Upazila', 'আসসাশুনি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, '64', 'Debhata Upazila', 'দেভাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, '64', 'Tala Upazila', 'তালা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, '64', 'Kalaroa Upazila', 'কলরোয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, '64', 'Kaliganj Upazila', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, '64', 'Shyamnagar Upazila', 'শ্যামনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, '18', 'Adamdighi', 'আদমদিঘী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, '18', 'Bogra Sadar', 'বগুড়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, '18', 'Sherpur', 'শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, '18', 'Dhunat', 'ধুনট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, '18', 'Dhupchanchia', 'দুপচাচিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, '18', 'Gabtali', 'গাবতলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, '18', 'Kahaloo', 'কাহালু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, '18', 'Nandigram', 'নন্দিগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, '18', 'Sahajanpur', 'শাহজাহানপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, '18', 'Sariakandi', 'সারিয়াকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, '18', 'Shibganj', 'শিবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, '18', 'Sonatala', 'সোনাতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, '19', 'Joypurhat S', 'জয়পুরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, '19', 'Akkelpur', 'আক্কেলপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, '19', 'Kalai', 'কালাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, '19', 'Khetlal', 'খেতলাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, '19', 'Panchbibi', 'পাঁচবিবি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, '20', 'Naogaon Sadar Upazila', 'নওগাঁ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, '20', 'Mohadevpur Upazila', 'মহাদেবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, '20', 'Manda Upazila', 'মান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, '20', 'Niamatpur Upazila', 'নিয়ামতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, '20', 'Atrai Upazila', 'আত্রাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, '20', 'Raninagar Upazila', 'রাণীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, '20', 'Patnitala Upazila', 'পত্নীতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, '20', 'Dhamoirhat Upazila', 'ধামইরহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, '20', 'Sapahar Upazila', 'সাপাহার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, '20', 'Porsha Upazila', 'পোরশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, '20', 'Badalgachhi Upazila', 'বদলগাছি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, '21', 'Natore Sadar Upazila', 'নাটোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, '21', 'Baraigram Upazila', 'বড়াইগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, '21', 'Bagatipara Upazila', 'বাগাতিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, '21', 'Lalpur Upazila', 'লালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, '21', 'Natore Sadar Upazila', 'নাটোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, '21', 'Baraigram Upazila', 'বড়াই গ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, '22', 'Bholahat Upazila', 'ভোলাহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, '22', 'Gomastapur Upazila', 'গোমস্তাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, '22', 'Nachole Upazila', 'নাচোল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, '22', 'Nawabganj Sadar Upazila', 'নবাবগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, '22', 'Shibganj Upazila', 'শিবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, '23', 'Atgharia Upazila', 'আটঘরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, '23', 'Bera Upazila', 'বেড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, '23', 'Bhangura Upazila', 'ভাঙ্গুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, '23', 'Chatmohar Upazila', 'চাটমোহর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, '23', 'Faridpur Upazila', 'ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, '23', 'Ishwardi Upazila', 'ঈশ্বরদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, '23', 'Pabna Sadar Upazila', 'পাবনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, '23', 'Santhia Upazila', 'সাথিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, '23', 'Sujanagar Upazila', 'সুজানগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, '24', 'Bagha', 'বাঘা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, '24', 'Bagmara', 'বাগমারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, '24', 'Charghat', 'চারঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, '24', 'Durgapur', 'দুর্গাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, '24', 'Godagari', 'গোদাগারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, '24', 'Mohanpur', 'মোহনপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, '24', 'Paba', 'পবা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, '24', 'Puthia', 'পুঠিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, '24', 'Tanore', 'তানোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, '25', 'Sirajganj Sadar Upazila', 'সিরাজগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, '25', 'Belkuchi Upazila', 'বেলকুচি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, '25', 'Chauhali Upazila', 'চৌহালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, '25', 'Kamarkhanda Upazila', 'কামারখান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, '25', 'Kazipur Upazila', 'কাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, '25', 'Raiganj Upazila', 'রায়গঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, '25', 'Shahjadpur Upazila', 'শাহজাদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, '25', 'Tarash Upazila', 'তারাশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, '25', 'Ullahpara Upazila', 'উল্লাপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, '26', 'Birampur Upazila', 'বিরামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, '26', 'Birganj', 'বীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, '26', 'Biral Upazila', 'বিড়াল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, '26', 'Bochaganj Upazila', 'বোচাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, '26', 'Chirirbandar Upazila', 'চিরিরবন্দর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, '26', 'Phulbari Upazila', 'ফুলবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, '26', 'Ghoraghat Upazila', 'ঘোড়াঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, '26', 'Hakimpur Upazila', 'হাকিমপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, '26', 'Kaharole Upazila', 'কাহারোল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, '26', 'Khansama Upazila', 'খানসামা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, '26', 'Dinajpur Sadar Upazila', 'দিনাজপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, '26', 'Nawabganj', 'নবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, '26', 'Parbatipur Upazila', 'পার্বতীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, '27', 'Fulchhari', 'ফুলছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, '27', 'Gaibandha sadar', 'গাইবান্ধা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, '27', 'Gobindaganj', 'গোবিন্দগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, '27', 'Palashbari', 'পলাশবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, '27', 'Sadullapur', 'সাদুল্যাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, '27', 'Saghata', 'সাঘাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, '27', 'Sundarganj', 'সুন্দরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, '28', 'Kurigram Sadar', 'কুড়িগ্রাম সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, '28', 'Nageshwari', 'নাগেশ্বরী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, '28', 'Bhurungamari', 'ভুরুঙ্গামারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, '28', 'Phulbari', 'ফুলবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, '28', 'Rajarhat', 'রাজারহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, '28', 'Ulipur', 'উলিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, '28', 'Chilmari', 'চিলমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, '28', 'Rowmari', 'রউমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, '28', 'Char Rajibpur', 'চর রাজিবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, '29', 'Lalmanirhat Sadar', 'লালমনিরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, '29', 'Aditmari', 'আদিতমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, '29', 'Kaliganj', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, '29', 'Hatibandha', 'হাতিবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, '29', 'Patgram', 'পাটগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, '30', 'Nilphamari Sadar', 'নীলফামারী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, '30', 'Saidpur', 'সৈয়দপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, '30', 'Jaldhaka', 'জলঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, '30', 'Kishoreganj', 'কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, '30', 'Domar', 'ডোমার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, '30', 'Dimla', 'ডিমলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, '31', 'Panchagarh Sadar', 'পঞ্চগড় সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, '31', 'Debiganj', 'দেবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, '31', 'Boda', 'বোদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, '31', 'Atwari', 'আটোয়ারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, '31', 'Tetulia', 'তেতুলিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, '32', 'Badarganj', 'বদরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, '32', 'Mithapukur', 'মিঠাপুকুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, '32', 'Gangachara', 'গঙ্গাচরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, '32', 'Kaunia', 'কাউনিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, '32', 'Rangpur Sadar', 'রংপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, '32', 'Pirgachha', 'পীরগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, '32', 'Pirganj', 'পীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, '32', 'Taraganj', 'তারাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, '33', 'Thakurgaon Sadar Upazila', 'ঠাকুরগাঁও সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, '33', 'Pirganj Upazila', 'পীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, '33', 'Baliadangi Upazila', 'বালিয়াডাঙ্গি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, '33', 'Haripur Upazila', 'হরিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, '33', 'Ranisankail Upazila', 'রাণীসংকইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, '51', 'Ajmiriganj', 'আজমিরিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, '51', 'Baniachang', 'বানিয়াচং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, '51', 'Bahubal', 'বাহুবল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, '51', 'Chunarughat', 'চুনারুঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, '51', 'Habiganj Sadar', 'হবিগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, '51', 'Lakhai', 'লাক্ষাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, '51', 'Madhabpur', 'মাধবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, '51', 'Nabiganj', 'নবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, '51', 'Shaistagonj Upazila', 'শায়েস্তাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, '52', 'Moulvibazar Sadar', 'মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, '52', 'Barlekha', 'বড়লেখা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, '52', 'Juri', 'জুড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, '52', 'Kamalganj', 'কামালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, '52', 'Kulaura', 'কুলাউরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, '52', 'Rajnagar', 'রাজনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, '52', 'Sreemangal', 'শ্রীমঙ্গল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, '53', 'Bishwamvarpur', 'বিসশম্ভারপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, '53', 'Chhatak', 'ছাতক', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, '53', 'Derai', 'দেড়াই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, '53', 'Dharampasha', 'ধরমপাশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, '53', 'Dowarabazar', 'দোয়ারাবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, '53', 'Jagannathpur', 'জগন্নাথপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, '53', 'Jamalganj', 'জামালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, '53', 'Sulla', 'সুল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, '53', 'Sunamganj Sadar', 'সুনামগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, '53', 'Shanthiganj', 'শান্তিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, '53', 'Tahirpur', 'তাহিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, '54', 'Sylhet Sadar', 'সিলেট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, '54', 'Beanibazar', 'বেয়ানিবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, '54', 'Bishwanath', 'বিশ্বনাথ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(483, '54', 'Dakshin Surma Upazila', 'দক্ষিণ সুরমা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(484, '54', 'Balaganj', 'বালাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(485, '54', 'Companiganj', 'কোম্পানিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, '54', 'Fenchuganj', 'ফেঞ্চুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, '54', 'Golapganj', 'গোলাপগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, '54', 'Gowainghat', 'গোয়াইনঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, '54', 'Jaintiapur', 'জয়ন্তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, '54', 'Kanaighat', 'কানাইঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, '54', 'Zakiganj', 'জাকিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, '54', 'Nobigonj', 'নবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_level` int(11) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `upazila` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `user_level`, `district`, `upazila`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.gmis', 4, 6, 189, '$2y$10$qr9YOUt.bJHMoZJ.7jLowuBA6cGfNLQ9XO9gPPfnk7FxrwBt4veS.', 'zSagvqNUrOew6hNnP1CooDKIBWjfx4o2qsPwgOOVgbrsqkyLfDku0GyNFjza', '2017-08-27 14:50:14', '2017-08-28 13:55:50'),
(4, 'Alamin', 'alamin@gmail.com', 2, 6, 189, '$2y$10$CGDZPURnY.mbpDFDu320dukF7AN/8uMywwi8qayU4Wg8eIue.NdOq', NULL, '2017-08-28 13:10:25', '2017-08-28 13:10:25'),
(5, 'Alamin', 'alamin2@gmail.com', 2, 2, 151, '$2y$10$Vgvj36Frrg97Daqm2jvrh.ITuxD/qJpWygwzjKEH2zejV1GGg5VUG', NULL, '2017-08-28 13:35:48', '2017-08-28 13:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2017-08-26 18:58:06', '2017-08-28 18:34:19'),
(2, 'Head Office', '2017-08-26 18:58:25', '2017-08-28 18:34:15'),
(3, 'District', '2017-08-26 18:58:41', '2017-08-28 18:34:32'),
(4, 'Upazila', '2017-08-26 18:58:41', '2017-08-28 18:34:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `application_datas`
--
ALTER TABLE `application_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district_tbl`
--
ALTER TABLE `district_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division_tbl`
--
ALTER TABLE `division_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indicators`
--
ALTER TABLE `indicators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainareas`
--
ALTER TABLE `mainareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sub_indicators`
--
ALTER TABLE `sub_indicators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upazila_tbl`
--
ALTER TABLE `upazila_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `application_datas`
--
ALTER TABLE `application_datas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `district_tbl`
--
ALTER TABLE `district_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `division_tbl`
--
ALTER TABLE `division_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `indicators`
--
ALTER TABLE `indicators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mainareas`
--
ALTER TABLE `mainareas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `sub_indicators`
--
ALTER TABLE `sub_indicators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `upazilas`
--
ALTER TABLE `upazilas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upazila_tbl`
--
ALTER TABLE `upazila_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=493;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
