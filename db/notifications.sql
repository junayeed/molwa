-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2017 at 12:06 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gimsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=unread, 1=read',
  `remarks` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `date`, `from_id`, `to_id`, `application_id`, `status`, `remarks`, `created_at`, `updated_at`) VALUES
(1, '2017-10-04', 7, 6, 15, 1, 'remarksssssssssssssssssssssssssss', '2017-10-03 19:34:23', '2017-10-03 20:32:44'),
(2, '2017-10-04', 7, 6, 16, 1, 'comments goes here', '2017-10-03 20:45:16', '2017-10-03 20:45:56'),
(3, '2017-10-04', 6, 4, 16, 1, 'send to center...', '2017-10-03 20:57:10', '2017-10-03 20:57:37'),
(4, '2017-10-04', 6, 7, 15, 1, 'back to operator, data not correct. Please check out.', '2017-10-03 21:05:14', '2017-10-03 21:08:32'),
(5, '2017-10-04', 7, 6, 15, 1, 'corrected', '2017-10-03 21:10:15', '2017-10-03 21:10:37'),
(6, '2017-10-04', 6, 4, 15, 1, 'corrected data. please check it', '2017-10-03 21:10:52', '2017-10-03 21:12:58'),
(7, '2017-10-04', 4, 6, 15, 1, 'back from center to reviewer', '2017-10-03 21:20:36', '2017-10-03 21:26:32'),
(8, '2017-10-04', 4, 6, 16, 0, 'asdfasdfasdf', '2017-10-03 21:22:56', '2017-10-03 21:22:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
